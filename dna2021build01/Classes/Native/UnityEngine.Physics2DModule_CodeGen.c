﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.PhysicsScene2D::ToString()
extern void PhysicsScene2D_ToString_m6F48AC6CE0D8540FCE4914ABB78ED0BAF0D83CBE_AdjustorThunk ();
// 0x00000002 System.Int32 UnityEngine.PhysicsScene2D::GetHashCode()
extern void PhysicsScene2D_GetHashCode_mB1C0E9E977ACCBF0AA0D266E5851B4D778354467_AdjustorThunk ();
// 0x00000003 System.Boolean UnityEngine.PhysicsScene2D::Equals(System.Object)
extern void PhysicsScene2D_Equals_mA91E96FDE086CF876D4D469CBFF0D43400C834E8_AdjustorThunk ();
// 0x00000004 System.Boolean UnityEngine.PhysicsScene2D::Equals(UnityEngine.PhysicsScene2D)
extern void PhysicsScene2D_Equals_mAA6F413AD3CDDD052496FAF69C34A45CA25D4293_AdjustorThunk ();
// 0x00000005 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void PhysicsScene2D_Raycast_m8A048506EDDC5C968DB55584FBF650DAB3BCB987_AdjustorThunk ();
// 0x00000006 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_mFA61658E024A98E2A7BC1B6965E9E5537DCA2DB8_AdjustorThunk ();
// 0x00000007 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_Internal_mB24F5D2B6967C70371484EA703E16346DBFD0718 ();
// 0x00000008 System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_Raycast_mE0460FE0CEE7076962DC2983A7B0DBB757DB133A_AdjustorThunk ();
// 0x00000009 System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_m40B8BDD4BE4D95E3826334DA2A8E31EBAD7B6E8D ();
// 0x0000000A System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_Raycast_mC5642256C2119435B87AD87ED30086973D9F3A24_AdjustorThunk ();
// 0x0000000B System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_m4D2446707FAC9EC36975B8119616F30BB724EA09 ();
// 0x0000000C System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersection(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit2D[],System.Int32)
extern void PhysicsScene2D_GetRayIntersection_m2DB850378F1910BFC62243A1A33D8B17738882EC_AdjustorThunk ();
// 0x0000000D System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_m1A9DC1520B80AF8444C38FEEDEB40EDD405805A5 ();
// 0x0000000E System.Void UnityEngine.PhysicsScene2D::Raycast_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern void PhysicsScene2D_Raycast_Internal_Injected_m197B563F302D9E7C336EE7BB0A356F6785F1584A ();
// 0x0000000F System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_Injected_mC5FDF82692390ECAB17CF821D25349A66B3C8143 ();
// 0x00000010 System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_Injected_mD8495122B2F8BD1194E83CA20DFC005D414C707B ();
// 0x00000011 System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_m74194745127DA849411A2191EE7C52EB07BB21A9 ();
// 0x00000012 UnityEngine.PhysicsScene2D UnityEngine.Physics2D::get_defaultPhysicsScene()
extern void Physics2D_get_defaultPhysicsScene_m2C9DA4DFAFB71332EC48E50CCB16275441CADE84 ();
// 0x00000013 System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern void Physics2D_get_queriesHitTriggers_m8BB98B1754A86777B4D58A4F28F63E8EC77B031B ();
// 0x00000014 UnityEngine.Vector2 UnityEngine.Physics2D::ClosestPoint(UnityEngine.Vector2,UnityEngine.Collider2D)
extern void Physics2D_ClosestPoint_mEB0BEB6CA76EAB36CDD68B27396FC4434B9E145C ();
// 0x00000015 UnityEngine.Vector2 UnityEngine.Physics2D::ClosestPoint_Collider(UnityEngine.Vector2,UnityEngine.Collider2D)
extern void Physics2D_ClosestPoint_Collider_mA4C6B483EC3FF0BD8FA070CEB2E54BB18019BE0D ();
// 0x00000016 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Physics2D_Raycast_mD22D6BC52ACAB22598A720525B3840C019842FFC ();
// 0x00000017 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Physics2D_Raycast_m468BF2D74BED92728533EA2108830C44ED93A0EF ();
// 0x00000018 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void Physics2D_Raycast_mBEB66E9AA034BD0AE1B1C99DF872247B0131CBDD ();
// 0x00000019 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern void Physics2D_Raycast_mB43742B1077F487D1458388C5B11EE46D73533C0 ();
// 0x0000001A UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern void Physics2D_Raycast_m4803AD692674FEE7EE269A6170AD5CEFEA6D3D78 ();
// 0x0000001B System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void Physics2D_Raycast_m0C22B1CACFA7E2A16D731B6E2D9D2ABC0666CCCE ();
// 0x0000001C System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_Raycast_m8678AB161A71C09D7606299D194A90BA814BA543 ();
// 0x0000001D System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>,System.Single)
extern void Physics2D_Raycast_m940284F559A12F0594CF6E1A20583F7EA67E8645 ();
// 0x0000001E UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern void Physics2D_GetRayIntersectionAll_mBD650C3EA6E692CE3E1255B6EAADF659307012D8 ();
// 0x0000001F UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern void Physics2D_GetRayIntersectionAll_mACC24DD73E1388C1DF86847390B89A5B0223F03A ();
// 0x00000020 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_m04BCAB03333B049C48BE61036E512E12A5FBD053 ();
// 0x00000021 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_m4E68866CF4A79A58DBF4B8A355D3EEE62BEF6612 ();
// 0x00000022 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[])
extern void Physics2D_GetRayIntersectionNonAlloc_m5F1AF31EEB67FE97AD2C40C102914371C2E825F0 ();
// 0x00000023 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_GetRayIntersectionNonAlloc_m1A638894F08E9F401C7161D02171805B4897B51E ();
// 0x00000024 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionNonAlloc_m3817EA2CC7B95C89683ACE0E433D6D4C6735CA0A ();
// 0x00000025 System.Void UnityEngine.Physics2D::.cctor()
extern void Physics2D__cctor_mC0D622F2EAF13BF0513DB2969E50EEC5631CDBFC ();
// 0x00000026 System.Void UnityEngine.Physics2D::ClosestPoint_Collider_Injected(UnityEngine.Vector2&,UnityEngine.Collider2D,UnityEngine.Vector2&)
extern void Physics2D_ClosestPoint_Collider_Injected_m22D1E7A7C38BA70B0D8A30689FA4F420E7271BF1 ();
// 0x00000027 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_Injected_m8B627D4448B34665FC8BCF560EE851152FE2D15A ();
// 0x00000028 System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern void ContactFilter2D_CheckConsistency_m0E1FC7D646C418F545F778197348F97ADA5409A2_AdjustorThunk ();
// 0x00000029 System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern void ContactFilter2D_SetLayerMask_mECEE981A09393F1097555D46449ED7CA4D8659E6_AdjustorThunk ();
// 0x0000002A System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern void ContactFilter2D_SetDepth_mF4AB9C380EDC3726D58734010BD90AD7D36ABDB0_AdjustorThunk ();
// 0x0000002B UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern void ContactFilter2D_CreateLegacyFilter_mA52A1C54BA7C4A49094B172BE7FA6044EF346A51 ();
// 0x0000002C System.Void UnityEngine.ContactFilter2D::CheckConsistency_Injected(UnityEngine.ContactFilter2D&)
extern void ContactFilter2D_CheckConsistency_Injected_m4640AA8896FEFE90396C5B47C4FE07930DA918BE ();
// 0x0000002D UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern void Collision2D_get_collider_m8767ED466970D214201582C37D90F2CD4BEFD6D2 ();
// 0x0000002E UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern void Collision2D_get_rigidbody_m7344D69B114D326F6866C0F02E5152CBE5C8B5BB ();
// 0x0000002F UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern void Collision2D_get_gameObject_m209F9F15585DE3F9270E0D9BFB050950AD301A5F ();
// 0x00000030 UnityEngine.Vector2 UnityEngine.Collision2D::get_relativeVelocity()
extern void Collision2D_get_relativeVelocity_mB688E6CEB859646997EC23D156133021CD7E8562 ();
// 0x00000031 UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern void RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098_AdjustorThunk ();
// 0x00000032 UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern void RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4_AdjustorThunk ();
// 0x00000033 System.Single UnityEngine.RaycastHit2D::get_distance()
extern void RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F_AdjustorThunk ();
// 0x00000034 UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern void RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976_AdjustorThunk ();
// 0x00000035 UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern void Rigidbody2D_get_position_m68CB3236D19D7472ABDE1F5A5A9BD924595361B8 ();
// 0x00000036 System.Single UnityEngine.Rigidbody2D::get_rotation()
extern void Rigidbody2D_get_rotation_mAF8F2E151EF82D8CF48DEDC17FE6882C2A67AF5C ();
// 0x00000037 System.Void UnityEngine.Rigidbody2D::MovePosition(UnityEngine.Vector2)
extern void Rigidbody2D_MovePosition_mD0572DDD77FD07D4D03B0A44CF8AD050AE9C42E5 ();
// 0x00000038 System.Void UnityEngine.Rigidbody2D::MoveRotation(UnityEngine.Quaternion)
extern void Rigidbody2D_MoveRotation_m195F548521220A6317F3B2C74EC05C44CD8F0BDB ();
// 0x00000039 System.Void UnityEngine.Rigidbody2D::MoveRotation_Quaternion(UnityEngine.Quaternion)
extern void Rigidbody2D_MoveRotation_Quaternion_m598121073EE8F7BEB72C128765405EE6E6FCBABC ();
// 0x0000003A UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern void Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1 ();
// 0x0000003B System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern void Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83 ();
// 0x0000003C System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern void Rigidbody2D_set_angularVelocity_mEA0807B27FFA3A839397575C2696F6B15693C599 ();
// 0x0000003D System.Boolean UnityEngine.Rigidbody2D::get_useAutoMass()
extern void Rigidbody2D_get_useAutoMass_mAD148425BF012EE1A741DA1DC6CB91D4C2451BD5 ();
// 0x0000003E System.Single UnityEngine.Rigidbody2D::get_mass()
extern void Rigidbody2D_get_mass_mD217EC45743AB52C6555287A0FF765BC7F6002E9 ();
// 0x0000003F System.Single UnityEngine.Rigidbody2D::get_drag()
extern void Rigidbody2D_get_drag_mF1D04791BB75BF1973F31F37BAAD5C6AD844FAE0 ();
// 0x00000040 System.Void UnityEngine.Rigidbody2D::set_drag(System.Single)
extern void Rigidbody2D_set_drag_m21BC07E7CDA33CA228A6F00A1DBF6E3D72CF5C62 ();
// 0x00000041 UnityEngine.RigidbodyType2D UnityEngine.Rigidbody2D::get_bodyType()
extern void Rigidbody2D_get_bodyType_m3C4E5E37E357E2D8AA862B6BEE14ADA4FD5BED89 ();
// 0x00000042 System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
extern void Rigidbody2D_set_bodyType_m239CDB6FFA033FD3B5BAC061A3F96DC9264D9900 ();
// 0x00000043 System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
extern void Rigidbody2D_get_isKinematic_mAE51BC33F3534009C53385BCA47A19D27DD9DE46 ();
// 0x00000044 System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern void Rigidbody2D_set_isKinematic_mA7711684E1E1E25FA7C1A1FF297B6E45DFD03BEE ();
// 0x00000045 System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2)
extern void Rigidbody2D_AddForce_mFE4658C0AE6643026A7CE7452857CA37DB687436 ();
// 0x00000046 System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern void Rigidbody2D_AddForce_m09E1A2E24DABA5BBC613E35772AE2C1C35C6E40C ();
// 0x00000047 System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single)
extern void Rigidbody2D_AddTorque_m740EC80534311898F5D1A8D7EAC91597BC93D08F ();
// 0x00000048 System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)
extern void Rigidbody2D_AddTorque_m3869C3D10CA85297FA6A1101C410F132E2696FD8 ();
// 0x00000049 System.Void UnityEngine.Rigidbody2D::.ctor()
extern void Rigidbody2D__ctor_mEADC6DF3297B27BF8DAE647A5D3384CA80E005FB ();
// 0x0000004A System.Void UnityEngine.Rigidbody2D::get_position_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_position_Injected_m7684DCAD6874A8D0A8560A052FEF85EF48280F73 ();
// 0x0000004B System.Void UnityEngine.Rigidbody2D::MovePosition_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_MovePosition_Injected_mA8D44ABECD0D47CD832619650FFB176F658B878A ();
// 0x0000004C System.Void UnityEngine.Rigidbody2D::MoveRotation_Quaternion_Injected(UnityEngine.Quaternion&)
extern void Rigidbody2D_MoveRotation_Quaternion_Injected_mA629DD8B53AE5E7C68CAA6C4E65E1AAA9575F280 ();
// 0x0000004D System.Void UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_velocity_Injected_mFB508815764F2895389C2B75002D3D7A33630B3C ();
// 0x0000004E System.Void UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_set_velocity_Injected_m8DCAEDCB8C0165DCEC87B70089C7B4EA41FEB73E ();
// 0x0000004F System.Void UnityEngine.Rigidbody2D::AddForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern void Rigidbody2D_AddForce_Injected_m41B8B49B88A28334BE679D3E9D0A44B242CC0897 ();
// 0x00000050 System.Single UnityEngine.Collider2D::get_density()
extern void Collider2D_get_density_m3127E283B4E96F595E6289509FAE3093012BE838 ();
// 0x00000051 System.Void UnityEngine.Collider2D::set_density(System.Single)
extern void Collider2D_set_density_m061D9893DEC0B69CEABEB19E18FB158D549E4DF5 ();
// 0x00000052 System.Boolean UnityEngine.Collider2D::get_isTrigger()
extern void Collider2D_get_isTrigger_m72C2C32959124D4FB91A83B56E5D7D5204B87E48 ();
// 0x00000053 System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern void Collider2D_set_isTrigger_m15514794FC3D677B712080672EAEEA0CE2C3A20B ();
// 0x00000054 System.Boolean UnityEngine.Collider2D::get_usedByEffector()
extern void Collider2D_get_usedByEffector_mAE7196767E57C0F8B3ED696670D6D081D1F34428 ();
// 0x00000055 System.Void UnityEngine.Collider2D::set_usedByEffector(System.Boolean)
extern void Collider2D_set_usedByEffector_mD34C0E0F5D92A74C62662F6680195FAF84AC464A ();
// 0x00000056 UnityEngine.Vector2 UnityEngine.Collider2D::get_offset()
extern void Collider2D_get_offset_mCB3DEFB9ACB05211320B8406B01F089EF7F8788D ();
// 0x00000057 System.Void UnityEngine.Collider2D::set_offset(UnityEngine.Vector2)
extern void Collider2D_set_offset_mC752F6CA4C47C5543538BA3BC9B04A3D267AAC6C ();
// 0x00000058 UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern void Collider2D_get_attachedRigidbody_m7BBA6D4F834B78D334349066540EA4DDDE4F0308 ();
// 0x00000059 UnityEngine.PhysicsMaterial2D UnityEngine.Collider2D::get_sharedMaterial()
extern void Collider2D_get_sharedMaterial_m3ACC07DAF13F704AB415DE8CA29DD135DDD16A97 ();
// 0x0000005A System.Void UnityEngine.Collider2D::set_sharedMaterial(UnityEngine.PhysicsMaterial2D)
extern void Collider2D_set_sharedMaterial_m87995DCC0B7F3742C383B18412FF3B581850F6C8 ();
// 0x0000005B System.Boolean UnityEngine.Collider2D::OverlapPoint(UnityEngine.Vector2)
extern void Collider2D_OverlapPoint_m3B1A14E8A3A937789EDDDFD64715D31FF7C213AC ();
// 0x0000005C UnityEngine.Vector2 UnityEngine.Collider2D::ClosestPoint(UnityEngine.Vector2)
extern void Collider2D_ClosestPoint_m370EB3FED9628BB69219CB70842E81D91766495A ();
// 0x0000005D System.Void UnityEngine.Collider2D::get_offset_Injected(UnityEngine.Vector2&)
extern void Collider2D_get_offset_Injected_mFBEEA538206330D2D4C62A04DF339332596C0F97 ();
// 0x0000005E System.Void UnityEngine.Collider2D::set_offset_Injected(UnityEngine.Vector2&)
extern void Collider2D_set_offset_Injected_m98CB990275094E1261101B082C51DDD634CA0A79 ();
// 0x0000005F System.Boolean UnityEngine.Collider2D::OverlapPoint_Injected(UnityEngine.Vector2&)
extern void Collider2D_OverlapPoint_Injected_mF16C156692D7E6DC2D6884FC2C62FEF6C4462404 ();
// 0x00000060 UnityEngine.Vector2 UnityEngine.BoxCollider2D::get_size()
extern void BoxCollider2D_get_size_m6230015317115D9BED5C61A4EDAC013C8A7664E1 ();
// 0x00000061 System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
extern void BoxCollider2D_set_size_mFA630C4BD41F786D208C1C0382A7FCF7868BA489 ();
// 0x00000062 System.Void UnityEngine.BoxCollider2D::get_size_Injected(UnityEngine.Vector2&)
extern void BoxCollider2D_get_size_Injected_m6E18F627969D38CF513DB4CF680ACD51810F74CD ();
// 0x00000063 System.Void UnityEngine.BoxCollider2D::set_size_Injected(UnityEngine.Vector2&)
extern void BoxCollider2D_set_size_Injected_mB787996D8E2E7919FE975BC435677115539A53A0 ();
// 0x00000064 System.Int32 UnityEngine.PolygonCollider2D::GetTotalPointCount()
extern void PolygonCollider2D_GetTotalPointCount_m3DB7B6E36C685F81F4A7DC503DD93E8F32347877 ();
// 0x00000065 UnityEngine.Vector2[] UnityEngine.PolygonCollider2D::get_points()
extern void PolygonCollider2D_get_points_m65A26A2D193E9F2C0D517635B46190FF8DBD9AF2 ();
// 0x00000066 System.Void UnityEngine.PolygonCollider2D::set_points(UnityEngine.Vector2[])
extern void PolygonCollider2D_set_points_m07965D584971AE013C355E34939C4EAD6365674E ();
// 0x00000067 System.Int32 UnityEngine.PolygonCollider2D::get_pathCount()
extern void PolygonCollider2D_get_pathCount_m1EF971CEECBD2A4E152B45DE2DD840F209F5517E ();
// 0x00000068 UnityEngine.Vector2[] UnityEngine.PolygonCollider2D::GetPath(System.Int32)
extern void PolygonCollider2D_GetPath_m297DB4C5B3449F88817A40EB48638197A395AB77 ();
// 0x00000069 UnityEngine.Vector2[] UnityEngine.PolygonCollider2D::GetPath_Internal(System.Int32)
extern void PolygonCollider2D_GetPath_Internal_m4E382AC9221B7FB9E28C8F7C4E768A1BEF4D651F ();
// 0x0000006A System.Void UnityEngine.PolygonCollider2D::SetPath(System.Int32,UnityEngine.Vector2[])
extern void PolygonCollider2D_SetPath_mE0B11473C90B4F848A146DD08DC5EC23004E99CA ();
// 0x0000006B System.Void UnityEngine.PolygonCollider2D::SetPath_Internal(System.Int32,UnityEngine.Vector2[])
extern void PolygonCollider2D_SetPath_Internal_m74F981E2C411B7DA32620FD26AB792843FDA2843 ();
// 0x0000006C System.Int32 UnityEngine.CompositeCollider2D::get_pathCount()
extern void CompositeCollider2D_get_pathCount_m9C39D6C9F24A12D4A4B6FD8207ED03889F28A733 ();
// 0x0000006D System.Int32 UnityEngine.CompositeCollider2D::get_pointCount()
extern void CompositeCollider2D_get_pointCount_m6A86D8430FD6E3A35A758DE79FF013D109D3D5FA ();
// 0x0000006E System.Int32 UnityEngine.CompositeCollider2D::GetPath(System.Int32,UnityEngine.Vector2[])
extern void CompositeCollider2D_GetPath_m247D0D285AA721DC9F0FE893A26C80E37B614FF9 ();
// 0x0000006F System.Int32 UnityEngine.CompositeCollider2D::GetPathArray_Internal(System.Int32,UnityEngine.Vector2[])
extern void CompositeCollider2D_GetPathArray_Internal_mC86A10BCF91A3036158C5D1192F0A215A7452FBD ();
// 0x00000070 System.Void UnityEngine.PhysicsMaterial2D::.ctor()
extern void PhysicsMaterial2D__ctor_mF489E9A0EDAEB106D5ADFF8D993AC303027A4E1F ();
// 0x00000071 System.Void UnityEngine.PhysicsMaterial2D::Create_Internal(UnityEngine.PhysicsMaterial2D,System.String)
extern void PhysicsMaterial2D_Create_Internal_mAEAC61ECFDF867806837B2740DD002755B9B56D0 ();
// 0x00000072 System.Single UnityEngine.PhysicsMaterial2D::get_bounciness()
extern void PhysicsMaterial2D_get_bounciness_m66F58C507B1DE148A37B83973A3C7C9A9FF97FBB ();
// 0x00000073 System.Void UnityEngine.PhysicsMaterial2D::set_bounciness(System.Single)
extern void PhysicsMaterial2D_set_bounciness_mFBDE7F27F8E84F72136EF3680F1C596D2A250542 ();
// 0x00000074 System.Single UnityEngine.PhysicsMaterial2D::get_friction()
extern void PhysicsMaterial2D_get_friction_mB4BDC722612DDDFBEDE630BAEE79FE97E9FAA1E9 ();
// 0x00000075 System.Void UnityEngine.PhysicsMaterial2D::set_friction(System.Single)
extern void PhysicsMaterial2D_set_friction_mDE66AFCA112E831AF8143A26FC603C623AE2A60F ();
static Il2CppMethodPointer s_methodPointers[117] = 
{
	PhysicsScene2D_ToString_m6F48AC6CE0D8540FCE4914ABB78ED0BAF0D83CBE_AdjustorThunk,
	PhysicsScene2D_GetHashCode_mB1C0E9E977ACCBF0AA0D266E5851B4D778354467_AdjustorThunk,
	PhysicsScene2D_Equals_mA91E96FDE086CF876D4D469CBFF0D43400C834E8_AdjustorThunk,
	PhysicsScene2D_Equals_mAA6F413AD3CDDD052496FAF69C34A45CA25D4293_AdjustorThunk,
	PhysicsScene2D_Raycast_m8A048506EDDC5C968DB55584FBF650DAB3BCB987_AdjustorThunk,
	PhysicsScene2D_Raycast_mFA61658E024A98E2A7BC1B6965E9E5537DCA2DB8_AdjustorThunk,
	PhysicsScene2D_Raycast_Internal_mB24F5D2B6967C70371484EA703E16346DBFD0718,
	PhysicsScene2D_Raycast_mE0460FE0CEE7076962DC2983A7B0DBB757DB133A_AdjustorThunk,
	PhysicsScene2D_RaycastArray_Internal_m40B8BDD4BE4D95E3826334DA2A8E31EBAD7B6E8D,
	PhysicsScene2D_Raycast_mC5642256C2119435B87AD87ED30086973D9F3A24_AdjustorThunk,
	PhysicsScene2D_RaycastList_Internal_m4D2446707FAC9EC36975B8119616F30BB724EA09,
	PhysicsScene2D_GetRayIntersection_m2DB850378F1910BFC62243A1A33D8B17738882EC_AdjustorThunk,
	PhysicsScene2D_GetRayIntersectionArray_Internal_m1A9DC1520B80AF8444C38FEEDEB40EDD405805A5,
	PhysicsScene2D_Raycast_Internal_Injected_m197B563F302D9E7C336EE7BB0A356F6785F1584A,
	PhysicsScene2D_RaycastArray_Internal_Injected_mC5FDF82692390ECAB17CF821D25349A66B3C8143,
	PhysicsScene2D_RaycastList_Internal_Injected_mD8495122B2F8BD1194E83CA20DFC005D414C707B,
	PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_m74194745127DA849411A2191EE7C52EB07BB21A9,
	Physics2D_get_defaultPhysicsScene_m2C9DA4DFAFB71332EC48E50CCB16275441CADE84,
	Physics2D_get_queriesHitTriggers_m8BB98B1754A86777B4D58A4F28F63E8EC77B031B,
	Physics2D_ClosestPoint_mEB0BEB6CA76EAB36CDD68B27396FC4434B9E145C,
	Physics2D_ClosestPoint_Collider_mA4C6B483EC3FF0BD8FA070CEB2E54BB18019BE0D,
	Physics2D_Raycast_mD22D6BC52ACAB22598A720525B3840C019842FFC,
	Physics2D_Raycast_m468BF2D74BED92728533EA2108830C44ED93A0EF,
	Physics2D_Raycast_mBEB66E9AA034BD0AE1B1C99DF872247B0131CBDD,
	Physics2D_Raycast_mB43742B1077F487D1458388C5B11EE46D73533C0,
	Physics2D_Raycast_m4803AD692674FEE7EE269A6170AD5CEFEA6D3D78,
	Physics2D_Raycast_m0C22B1CACFA7E2A16D731B6E2D9D2ABC0666CCCE,
	Physics2D_Raycast_m8678AB161A71C09D7606299D194A90BA814BA543,
	Physics2D_Raycast_m940284F559A12F0594CF6E1A20583F7EA67E8645,
	Physics2D_GetRayIntersectionAll_mBD650C3EA6E692CE3E1255B6EAADF659307012D8,
	Physics2D_GetRayIntersectionAll_mACC24DD73E1388C1DF86847390B89A5B0223F03A,
	Physics2D_GetRayIntersectionAll_m04BCAB03333B049C48BE61036E512E12A5FBD053,
	Physics2D_GetRayIntersectionAll_Internal_m4E68866CF4A79A58DBF4B8A355D3EEE62BEF6612,
	Physics2D_GetRayIntersectionNonAlloc_m5F1AF31EEB67FE97AD2C40C102914371C2E825F0,
	Physics2D_GetRayIntersectionNonAlloc_m1A638894F08E9F401C7161D02171805B4897B51E,
	Physics2D_GetRayIntersectionNonAlloc_m3817EA2CC7B95C89683ACE0E433D6D4C6735CA0A,
	Physics2D__cctor_mC0D622F2EAF13BF0513DB2969E50EEC5631CDBFC,
	Physics2D_ClosestPoint_Collider_Injected_m22D1E7A7C38BA70B0D8A30689FA4F420E7271BF1,
	Physics2D_GetRayIntersectionAll_Internal_Injected_m8B627D4448B34665FC8BCF560EE851152FE2D15A,
	ContactFilter2D_CheckConsistency_m0E1FC7D646C418F545F778197348F97ADA5409A2_AdjustorThunk,
	ContactFilter2D_SetLayerMask_mECEE981A09393F1097555D46449ED7CA4D8659E6_AdjustorThunk,
	ContactFilter2D_SetDepth_mF4AB9C380EDC3726D58734010BD90AD7D36ABDB0_AdjustorThunk,
	ContactFilter2D_CreateLegacyFilter_mA52A1C54BA7C4A49094B172BE7FA6044EF346A51,
	ContactFilter2D_CheckConsistency_Injected_m4640AA8896FEFE90396C5B47C4FE07930DA918BE,
	Collision2D_get_collider_m8767ED466970D214201582C37D90F2CD4BEFD6D2,
	Collision2D_get_rigidbody_m7344D69B114D326F6866C0F02E5152CBE5C8B5BB,
	Collision2D_get_gameObject_m209F9F15585DE3F9270E0D9BFB050950AD301A5F,
	Collision2D_get_relativeVelocity_mB688E6CEB859646997EC23D156133021CD7E8562,
	RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098_AdjustorThunk,
	RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4_AdjustorThunk,
	RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F_AdjustorThunk,
	RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976_AdjustorThunk,
	Rigidbody2D_get_position_m68CB3236D19D7472ABDE1F5A5A9BD924595361B8,
	Rigidbody2D_get_rotation_mAF8F2E151EF82D8CF48DEDC17FE6882C2A67AF5C,
	Rigidbody2D_MovePosition_mD0572DDD77FD07D4D03B0A44CF8AD050AE9C42E5,
	Rigidbody2D_MoveRotation_m195F548521220A6317F3B2C74EC05C44CD8F0BDB,
	Rigidbody2D_MoveRotation_Quaternion_m598121073EE8F7BEB72C128765405EE6E6FCBABC,
	Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1,
	Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83,
	Rigidbody2D_set_angularVelocity_mEA0807B27FFA3A839397575C2696F6B15693C599,
	Rigidbody2D_get_useAutoMass_mAD148425BF012EE1A741DA1DC6CB91D4C2451BD5,
	Rigidbody2D_get_mass_mD217EC45743AB52C6555287A0FF765BC7F6002E9,
	Rigidbody2D_get_drag_mF1D04791BB75BF1973F31F37BAAD5C6AD844FAE0,
	Rigidbody2D_set_drag_m21BC07E7CDA33CA228A6F00A1DBF6E3D72CF5C62,
	Rigidbody2D_get_bodyType_m3C4E5E37E357E2D8AA862B6BEE14ADA4FD5BED89,
	Rigidbody2D_set_bodyType_m239CDB6FFA033FD3B5BAC061A3F96DC9264D9900,
	Rigidbody2D_get_isKinematic_mAE51BC33F3534009C53385BCA47A19D27DD9DE46,
	Rigidbody2D_set_isKinematic_mA7711684E1E1E25FA7C1A1FF297B6E45DFD03BEE,
	Rigidbody2D_AddForce_mFE4658C0AE6643026A7CE7452857CA37DB687436,
	Rigidbody2D_AddForce_m09E1A2E24DABA5BBC613E35772AE2C1C35C6E40C,
	Rigidbody2D_AddTorque_m740EC80534311898F5D1A8D7EAC91597BC93D08F,
	Rigidbody2D_AddTorque_m3869C3D10CA85297FA6A1101C410F132E2696FD8,
	Rigidbody2D__ctor_mEADC6DF3297B27BF8DAE647A5D3384CA80E005FB,
	Rigidbody2D_get_position_Injected_m7684DCAD6874A8D0A8560A052FEF85EF48280F73,
	Rigidbody2D_MovePosition_Injected_mA8D44ABECD0D47CD832619650FFB176F658B878A,
	Rigidbody2D_MoveRotation_Quaternion_Injected_mA629DD8B53AE5E7C68CAA6C4E65E1AAA9575F280,
	Rigidbody2D_get_velocity_Injected_mFB508815764F2895389C2B75002D3D7A33630B3C,
	Rigidbody2D_set_velocity_Injected_m8DCAEDCB8C0165DCEC87B70089C7B4EA41FEB73E,
	Rigidbody2D_AddForce_Injected_m41B8B49B88A28334BE679D3E9D0A44B242CC0897,
	Collider2D_get_density_m3127E283B4E96F595E6289509FAE3093012BE838,
	Collider2D_set_density_m061D9893DEC0B69CEABEB19E18FB158D549E4DF5,
	Collider2D_get_isTrigger_m72C2C32959124D4FB91A83B56E5D7D5204B87E48,
	Collider2D_set_isTrigger_m15514794FC3D677B712080672EAEEA0CE2C3A20B,
	Collider2D_get_usedByEffector_mAE7196767E57C0F8B3ED696670D6D081D1F34428,
	Collider2D_set_usedByEffector_mD34C0E0F5D92A74C62662F6680195FAF84AC464A,
	Collider2D_get_offset_mCB3DEFB9ACB05211320B8406B01F089EF7F8788D,
	Collider2D_set_offset_mC752F6CA4C47C5543538BA3BC9B04A3D267AAC6C,
	Collider2D_get_attachedRigidbody_m7BBA6D4F834B78D334349066540EA4DDDE4F0308,
	Collider2D_get_sharedMaterial_m3ACC07DAF13F704AB415DE8CA29DD135DDD16A97,
	Collider2D_set_sharedMaterial_m87995DCC0B7F3742C383B18412FF3B581850F6C8,
	Collider2D_OverlapPoint_m3B1A14E8A3A937789EDDDFD64715D31FF7C213AC,
	Collider2D_ClosestPoint_m370EB3FED9628BB69219CB70842E81D91766495A,
	Collider2D_get_offset_Injected_mFBEEA538206330D2D4C62A04DF339332596C0F97,
	Collider2D_set_offset_Injected_m98CB990275094E1261101B082C51DDD634CA0A79,
	Collider2D_OverlapPoint_Injected_mF16C156692D7E6DC2D6884FC2C62FEF6C4462404,
	BoxCollider2D_get_size_m6230015317115D9BED5C61A4EDAC013C8A7664E1,
	BoxCollider2D_set_size_mFA630C4BD41F786D208C1C0382A7FCF7868BA489,
	BoxCollider2D_get_size_Injected_m6E18F627969D38CF513DB4CF680ACD51810F74CD,
	BoxCollider2D_set_size_Injected_mB787996D8E2E7919FE975BC435677115539A53A0,
	PolygonCollider2D_GetTotalPointCount_m3DB7B6E36C685F81F4A7DC503DD93E8F32347877,
	PolygonCollider2D_get_points_m65A26A2D193E9F2C0D517635B46190FF8DBD9AF2,
	PolygonCollider2D_set_points_m07965D584971AE013C355E34939C4EAD6365674E,
	PolygonCollider2D_get_pathCount_m1EF971CEECBD2A4E152B45DE2DD840F209F5517E,
	PolygonCollider2D_GetPath_m297DB4C5B3449F88817A40EB48638197A395AB77,
	PolygonCollider2D_GetPath_Internal_m4E382AC9221B7FB9E28C8F7C4E768A1BEF4D651F,
	PolygonCollider2D_SetPath_mE0B11473C90B4F848A146DD08DC5EC23004E99CA,
	PolygonCollider2D_SetPath_Internal_m74F981E2C411B7DA32620FD26AB792843FDA2843,
	CompositeCollider2D_get_pathCount_m9C39D6C9F24A12D4A4B6FD8207ED03889F28A733,
	CompositeCollider2D_get_pointCount_m6A86D8430FD6E3A35A758DE79FF013D109D3D5FA,
	CompositeCollider2D_GetPath_m247D0D285AA721DC9F0FE893A26C80E37B614FF9,
	CompositeCollider2D_GetPathArray_Internal_mC86A10BCF91A3036158C5D1192F0A215A7452FBD,
	PhysicsMaterial2D__ctor_mF489E9A0EDAEB106D5ADFF8D993AC303027A4E1F,
	PhysicsMaterial2D_Create_Internal_mAEAC61ECFDF867806837B2740DD002755B9B56D0,
	PhysicsMaterial2D_get_bounciness_m66F58C507B1DE148A37B83973A3C7C9A9FF97FBB,
	PhysicsMaterial2D_set_bounciness_mFBDE7F27F8E84F72136EF3680F1C596D2A250542,
	PhysicsMaterial2D_get_friction_mB4BDC722612DDDFBEDE630BAEE79FE97E9FAA1E9,
	PhysicsMaterial2D_set_friction_mDE66AFCA112E831AF8143A26FC603C623AE2A60F,
};
static const int32_t s_InvokerIndices[117] = 
{
	14,
	10,
	9,
	1377,
	1378,
	1379,
	1380,
	1381,
	1382,
	1381,
	1382,
	1383,
	1384,
	1385,
	1386,
	1386,
	1387,
	1388,
	49,
	1389,
	1389,
	1390,
	1391,
	1392,
	1393,
	1394,
	1395,
	1396,
	1396,
	1397,
	1398,
	1399,
	1400,
	1401,
	1402,
	1403,
	3,
	1404,
	1405,
	23,
	1406,
	1037,
	1407,
	17,
	14,
	14,
	14,
	1046,
	1046,
	1046,
	670,
	14,
	1046,
	670,
	1047,
	1226,
	1226,
	1046,
	1047,
	292,
	95,
	670,
	670,
	292,
	10,
	32,
	95,
	31,
	1047,
	1408,
	292,
	1409,
	23,
	6,
	6,
	6,
	6,
	6,
	64,
	670,
	292,
	95,
	31,
	95,
	31,
	1046,
	1047,
	14,
	14,
	26,
	1074,
	1410,
	6,
	6,
	782,
	1046,
	1047,
	6,
	6,
	10,
	14,
	26,
	10,
	34,
	34,
	62,
	62,
	10,
	10,
	1411,
	1411,
	23,
	121,
	670,
	292,
	670,
	292,
};
extern const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule = 
{
	"UnityEngine.Physics2DModule.dll",
	117,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
