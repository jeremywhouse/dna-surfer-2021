﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void BlueCoinActivator::Start()
extern void BlueCoinActivator_Start_m0F238FD4275D6F2E0DEFA4C09804BE24B9851178 ();
// 0x00000002 System.Void BlueCoinActivator::Update()
extern void BlueCoinActivator_Update_mBA2F05EF2A4D37DF4C37DC6061E31BC558899970 ();
// 0x00000003 System.Void BlueCoinActivator::CheckCoins()
extern void BlueCoinActivator_CheckCoins_m525D5AC5E9FC0611807ABCA39DFA5F0975321FBB ();
// 0x00000004 System.Void BlueCoinActivator::CheckCombo()
extern void BlueCoinActivator_CheckCombo_mE8DD0E6CDD6C27892D9C7A114980D0666D3C7BE4 ();
// 0x00000005 System.Void BlueCoinActivator::.ctor()
extern void BlueCoinActivator__ctor_m02F1523DCF4481CCF43FC1244570C205A65DEE47 ();
// 0x00000006 System.Void DnaEater::Start()
extern void DnaEater_Start_m4B334D21181542A02ED5A784D9C2BC8F3FE2049A ();
// 0x00000007 System.Void DnaEater::Awake()
extern void DnaEater_Awake_m30A4C0DF09CA4394A9481959D7BA5C40F246CC5C ();
// 0x00000008 System.Void DnaEater::Update()
extern void DnaEater_Update_m05B606042E8A6DD63924826540417A8DCE8DF948 ();
// 0x00000009 System.Void DnaEater::UpdateAccelleration()
extern void DnaEater_UpdateAccelleration_m509536AB3DF3751860623708F8FC40BCCFF31819 ();
// 0x0000000A System.Void DnaEater::Reset()
extern void DnaEater_Reset_m4478500CA01C757C662B0FE44618FEFB748FA06C ();
// 0x0000000B System.Void DnaEater::CatchUp()
extern void DnaEater_CatchUp_mBCE208D516E8BD2FA58988AE9E03B5EF499C19CD ();
// 0x0000000C System.Void DnaEater::.ctor()
extern void DnaEater__ctor_m244F03631AB082CC71C584877974D0FA4B5E657C ();
// 0x0000000D System.Void DnaEater::.cctor()
extern void DnaEater__cctor_mF1A51388B6A1857C7FC3F65F168A0832BBABBDF5 ();
// 0x0000000E System.Void DnaInfo::Start()
extern void DnaInfo_Start_m69B4BEAF65BABFA9C7D645670A01F152A36FFADE ();
// 0x0000000F System.Void DnaInfo::Update()
extern void DnaInfo_Update_mD3574A42A39A0E00D7500A0C5E1FAAD6D634558D ();
// 0x00000010 System.Void DnaInfo::.ctor()
extern void DnaInfo__ctor_mAC35537741A4533C14DDE340CEC3277F04421F05 ();
// 0x00000011 System.Void DriftScript::Start()
extern void DriftScript_Start_mC0B4E5ECB165811EF4DB3FB52437A746384215D5 ();
// 0x00000012 System.Void DriftScript::Awake()
extern void DriftScript_Awake_m14E893C28259A32A7BF84A80A6555D7C052AC71F ();
// 0x00000013 System.Void DriftScript::Update()
extern void DriftScript_Update_mA7F0EFAB0064BE6A6732715751B5BC24BD1D7875 ();
// 0x00000014 System.Void DriftScript::UpdateDrift()
extern void DriftScript_UpdateDrift_mB4DAA483A395C492EA120B35BEF906521A801F59 ();
// 0x00000015 System.Void DriftScript::.ctor()
extern void DriftScript__ctor_m568DC12B68F45D1F7E6086D519C19F0339BDD868 ();
// 0x00000016 System.Void MultiLayerTouch::OnEnable()
extern void MultiLayerTouch_OnEnable_m18ABECD79B51E17A47C99FDDBD8DE36071558D03 ();
// 0x00000017 System.Void MultiLayerTouch::OnDestroy()
extern void MultiLayerTouch_OnDestroy_mDDBC581BD0A967C81F58BE8AFC2973BBBA605AC7 ();
// 0x00000018 System.Void MultiLayerTouch::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void MultiLayerTouch_On_TouchDown_m6C55CE20CD5D15742DA48368138904FC16BEA440 ();
// 0x00000019 System.Void MultiLayerTouch::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void MultiLayerTouch_On_TouchUp_m0B85E5D32DAEBD9A5057E84B1B91721E63A9D15E ();
// 0x0000001A System.Void MultiLayerTouch::.ctor()
extern void MultiLayerTouch__ctor_mBCF2164FF90D51374CB31A1E941071A3489E9671 ();
// 0x0000001B System.Void MultiLayerUI::SetAutoSelect(System.Boolean)
extern void MultiLayerUI_SetAutoSelect_m361FCB01D6F2A7F5989FA346EBAB59DC2D7A8AB7 ();
// 0x0000001C System.Void MultiLayerUI::SetAutoUpdate(System.Boolean)
extern void MultiLayerUI_SetAutoUpdate_m165800131C4964DB5BB7E301F09A71E96CE8DEC4 ();
// 0x0000001D System.Void MultiLayerUI::Layer1(System.Boolean)
extern void MultiLayerUI_Layer1_mBF42373F089EBFA64EF745FE616DAA2575DC4D3C ();
// 0x0000001E System.Void MultiLayerUI::Layer2(System.Boolean)
extern void MultiLayerUI_Layer2_m5D4B226E371E3D788682F29CA3DB006284A669CD ();
// 0x0000001F System.Void MultiLayerUI::Layer3(System.Boolean)
extern void MultiLayerUI_Layer3_m196EA62000B97225FE9913286BD9BD875CF28DB9 ();
// 0x00000020 System.Void MultiLayerUI::.ctor()
extern void MultiLayerUI__ctor_m3B167593F50A91D4E7805891FF7F6408D36A526E ();
// 0x00000021 System.Void MultiCameraTouch::OnEnable()
extern void MultiCameraTouch_OnEnable_m8F6536E4A6BE03D7F559BC1259D63B0D2C2FA852 ();
// 0x00000022 System.Void MultiCameraTouch::OnDestroy()
extern void MultiCameraTouch_OnDestroy_m3102045E143234B9BCDEE95598D4C92BDC87F46A ();
// 0x00000023 System.Void MultiCameraTouch::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void MultiCameraTouch_On_TouchDown_mA9D78A14AA46D3DD202222F3579F92F2E57B9865 ();
// 0x00000024 System.Void MultiCameraTouch::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void MultiCameraTouch_On_TouchUp_m14DC3BC21990DC31420276D32AC25B1F49D30B07 ();
// 0x00000025 System.Void MultiCameraTouch::.ctor()
extern void MultiCameraTouch__ctor_mF021A85E26ADFB48FA07A2CA0E929E6751A4E1CA ();
// 0x00000026 System.Void MultiCameraUI::AddCamera2(System.Boolean)
extern void MultiCameraUI_AddCamera2_m7266009223CE15DC1BE8517D12BCE99702DB88D9 ();
// 0x00000027 System.Void MultiCameraUI::AddCamera3(System.Boolean)
extern void MultiCameraUI_AddCamera3_mE24FA9A3999F37CEC164070A64F2EDF84542EAC5 ();
// 0x00000028 System.Void MultiCameraUI::AddCamera(UnityEngine.Camera,System.Boolean)
extern void MultiCameraUI_AddCamera_m6DD1C72806928B2845529C34018DEB6BC1EFF5EC ();
// 0x00000029 System.Void MultiCameraUI::.ctor()
extern void MultiCameraUI__ctor_mB0829FAE56541297AF990D22FD121280C4730B9B ();
// 0x0000002A System.Void CubeSelect::OnEnable()
extern void CubeSelect_OnEnable_m38FF3BDB5F99FADEAB1A239B95AC22DB4111EEE7 ();
// 0x0000002B System.Void CubeSelect::OnDestroy()
extern void CubeSelect_OnDestroy_m7579A2F75BCA70E772B1E7A59EA6AB64143000EA ();
// 0x0000002C System.Void CubeSelect::Start()
extern void CubeSelect_Start_m63DF08877C35602F8D359684E5CCED1FD837F152 ();
// 0x0000002D System.Void CubeSelect::On_SimpleTap(HedgehogTeam.EasyTouch.Gesture)
extern void CubeSelect_On_SimpleTap_m8D59A3851F70A44762FB189271A458E2C1CAD820 ();
// 0x0000002E System.Void CubeSelect::ResteColor()
extern void CubeSelect_ResteColor_m9F79E704CD139A9C583E9A8CBF7E36019B6AEB4B ();
// 0x0000002F System.Void CubeSelect::.ctor()
extern void CubeSelect__ctor_m4856483EB7019362767791BB01AB963BE12D8594 ();
// 0x00000030 System.Void RTSCamera::OnEnable()
extern void RTSCamera_OnEnable_m9D7E68DFA0F5C2DA6C47478287900158625E7454 ();
// 0x00000031 System.Void RTSCamera::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern void RTSCamera_On_Twist_m6B4B1DAC2F2DE6CF48B1410CBE5C0EC078351DEA ();
// 0x00000032 System.Void RTSCamera::OnDestroy()
extern void RTSCamera_OnDestroy_m041494B68E6EA892ED6CE2098A7A3D55842A01E9 ();
// 0x00000033 System.Void RTSCamera::On_Drag(HedgehogTeam.EasyTouch.Gesture)
extern void RTSCamera_On_Drag_mE0A13F5EF8F9C904F5B1E84E3A703A6B590E291A ();
// 0x00000034 System.Void RTSCamera::On_Swipe(HedgehogTeam.EasyTouch.Gesture)
extern void RTSCamera_On_Swipe_mA5AFE6113E1EAF9D03F028FF1945648072EAF88B ();
// 0x00000035 System.Void RTSCamera::On_Pinch(HedgehogTeam.EasyTouch.Gesture)
extern void RTSCamera_On_Pinch_m57944272EC15749D9858BF7CB64E1EB623980AD2 ();
// 0x00000036 System.Void RTSCamera::.ctor()
extern void RTSCamera__ctor_m648CF44113CAE61AA43CA417786095D38F8A814A ();
// 0x00000037 System.Void Ball::.ctor()
extern void Ball__ctor_mCF954B56503D505E1FC04F582DD66B086098B67A ();
// 0x00000038 System.Void BallRunPlayer::OnEnable()
extern void BallRunPlayer_OnEnable_m5F3703E9764FB560B60E4DB30C81C4B980BB72F0 ();
// 0x00000039 System.Void BallRunPlayer::OnDestroy()
extern void BallRunPlayer_OnDestroy_m1172907FFC96014959E9FBE7B3D79A870716D125 ();
// 0x0000003A System.Void BallRunPlayer::Start()
extern void BallRunPlayer_Start_m37EBD7254B861A2EEEFC924C11A54014EB2EA4F8 ();
// 0x0000003B System.Void BallRunPlayer::Update()
extern void BallRunPlayer_Update_m72D95FE55CE3A4F62B89F7825BDD721791A3A28F ();
// 0x0000003C System.Void BallRunPlayer::OnCollision()
extern void BallRunPlayer_OnCollision_m9BB037D5C3C510927BF7B99998B505823628C5F8 ();
// 0x0000003D System.Void BallRunPlayer::On_SwipeEnd(HedgehogTeam.EasyTouch.Gesture)
extern void BallRunPlayer_On_SwipeEnd_mE332D52086C9FEC4148B13CF71C15703EB2D8E6D ();
// 0x0000003E System.Void BallRunPlayer::StartGame()
extern void BallRunPlayer_StartGame_m7710E645847D54F29F20B4D66D331C81ED973B3E ();
// 0x0000003F System.Void BallRunPlayer::.ctor()
extern void BallRunPlayer__ctor_mE0553449C98AFC29BE54AC628371194C63EB236C ();
// 0x00000040 System.Void ThirdPersonCamera::Start()
extern void ThirdPersonCamera_Start_mD22E58CB725E8543D94ACDB4482FC7F9BBE0413F ();
// 0x00000041 System.Void ThirdPersonCamera::LateUpdate()
extern void ThirdPersonCamera_LateUpdate_m288A921D34C2A80C295C66FAAD1CC7B10E180766 ();
// 0x00000042 System.Void ThirdPersonCamera::.ctor()
extern void ThirdPersonCamera__ctor_m2815DED4895A74874732CAF0A1DC9B12685B4C3A ();
// 0x00000043 System.Void LoadExamples::LoadExample(System.String)
extern void LoadExamples_LoadExample_m906BE36371874F3AF3866877F97B9840FACCB230 ();
// 0x00000044 System.Void LoadExamples::.ctor()
extern void LoadExamples__ctor_mC9BFFCB3C15EF18DD5660891CC852E37B1108998 ();
// 0x00000045 System.Void FingerTouch::OnEnable()
extern void FingerTouch_OnEnable_m01912F265459CF43B54DE69F4F86B8C299B943BA ();
// 0x00000046 System.Void FingerTouch::OnDestroy()
extern void FingerTouch_OnDestroy_m7E0E44AD716806E9031DC93C12683D40B8E89681 ();
// 0x00000047 System.Void FingerTouch::On_Drag(HedgehogTeam.EasyTouch.Gesture)
extern void FingerTouch_On_Drag_m69724AC042F6F549CE7235A0E194B0093D2CC1D5 ();
// 0x00000048 System.Void FingerTouch::On_Swipe(HedgehogTeam.EasyTouch.Gesture)
extern void FingerTouch_On_Swipe_mDE31BDE9A42F50006C454999847CBF149C08DC65 ();
// 0x00000049 System.Void FingerTouch::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern void FingerTouch_On_TouchStart_mABE14F92A47920D285F05A5701230DBD75D17DFD ();
// 0x0000004A System.Void FingerTouch::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void FingerTouch_On_TouchUp_mA92E0B105DA7DE69FE71F7B2DA6882F4767238CF ();
// 0x0000004B System.Void FingerTouch::InitTouch(System.Int32)
extern void FingerTouch_InitTouch_mEF8867656598C4B1F0DD568F40E84A96FFF435B6 ();
// 0x0000004C System.Void FingerTouch::On_DoubleTap(HedgehogTeam.EasyTouch.Gesture)
extern void FingerTouch_On_DoubleTap_m53329B3640EDBE72D372D85AABE4D9A02BB719F4 ();
// 0x0000004D System.Void FingerTouch::.ctor()
extern void FingerTouch__ctor_m8390586A711BF6B7CEA88940442365D858FFADBB ();
// 0x0000004E System.Void MutliFingersScreenTouch::OnEnable()
extern void MutliFingersScreenTouch_OnEnable_m3593020B513B382D67162637E82DC40E822E2995 ();
// 0x0000004F System.Void MutliFingersScreenTouch::OnDestroy()
extern void MutliFingersScreenTouch_OnDestroy_m0FBD8033CE1A4837B3D03779B008983A4FCAD382 ();
// 0x00000050 System.Void MutliFingersScreenTouch::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern void MutliFingersScreenTouch_On_TouchStart_m1B224F6C8DD6EE96BAD4FF8083BCDDAD41A45A3E ();
// 0x00000051 System.Void MutliFingersScreenTouch::.ctor()
extern void MutliFingersScreenTouch__ctor_m96C172021FEF3C41F3C6EBADD28EA970B810079A ();
// 0x00000052 System.Void DoubleTapMe::OnEnable()
extern void DoubleTapMe_OnEnable_m872EE06ADDEB105D79B13487525D37FAC45F846B ();
// 0x00000053 System.Void DoubleTapMe::OnDisable()
extern void DoubleTapMe_OnDisable_m76037CB13D45135541CE24679AF93D04636A2D4E ();
// 0x00000054 System.Void DoubleTapMe::OnDestroy()
extern void DoubleTapMe_OnDestroy_m30BEE6402DE0A038BA43447364D42AF959D656B1 ();
// 0x00000055 System.Void DoubleTapMe::UnsubscribeEvent()
extern void DoubleTapMe_UnsubscribeEvent_m8E50FA7F72C4CC2D796678D2515E75E1B73203EE ();
// 0x00000056 System.Void DoubleTapMe::On_DoubleTap(HedgehogTeam.EasyTouch.Gesture)
extern void DoubleTapMe_On_DoubleTap_m45EED23522CB529540FC7E7110B5485ADEA21875 ();
// 0x00000057 System.Void DoubleTapMe::.ctor()
extern void DoubleTapMe__ctor_mA878A175EF0D361BC359DC6E46EC26843EEF1A8C ();
// 0x00000058 System.Void DragMe::OnEnable()
extern void DragMe_OnEnable_m437FFD62DFEDBD11D4BB2629D6F0C75A3F20164B ();
// 0x00000059 System.Void DragMe::OnDisable()
extern void DragMe_OnDisable_mA84B75EE1AC36B8EDAE6EB094161FC9892C87BB3 ();
// 0x0000005A System.Void DragMe::OnDestroy()
extern void DragMe_OnDestroy_m3FB91C6A43D2969A41CB07A25B957BA03CADD467 ();
// 0x0000005B System.Void DragMe::UnsubscribeEvent()
extern void DragMe_UnsubscribeEvent_m39998A2771826394CB19EA547669720820881FC4 ();
// 0x0000005C System.Void DragMe::Start()
extern void DragMe_Start_m06CB9ACAB54F91A8D02F8AE7EB2A3C223F46836B ();
// 0x0000005D System.Void DragMe::On_DragStart(HedgehogTeam.EasyTouch.Gesture)
extern void DragMe_On_DragStart_m1E0014AE56BDB3E1657D7F6B33A6846D667DF780 ();
// 0x0000005E System.Void DragMe::On_Drag(HedgehogTeam.EasyTouch.Gesture)
extern void DragMe_On_Drag_m853C3A8F075E36E1037FA4BF1C092C95F8AEFB00 ();
// 0x0000005F System.Void DragMe::On_DragEnd(HedgehogTeam.EasyTouch.Gesture)
extern void DragMe_On_DragEnd_m237E31706F6BDE3790BEE1696C9885852FBA8F49 ();
// 0x00000060 System.Void DragMe::RandomColor()
extern void DragMe_RandomColor_mD433EA64B058D5F12BA04D08E51987E26747A20E ();
// 0x00000061 System.Void DragMe::.ctor()
extern void DragMe__ctor_mCC3B3FE01046220C730DF75620531B954379BC6C ();
// 0x00000062 System.Void LongTapMe::OnEnable()
extern void LongTapMe_OnEnable_mEF7776B7B6D770186604AE635C483B25F17D9DD8 ();
// 0x00000063 System.Void LongTapMe::OnDisable()
extern void LongTapMe_OnDisable_m3796F96D0FD4CD23894425C9AE37643C91BA99BD ();
// 0x00000064 System.Void LongTapMe::OnDestroy()
extern void LongTapMe_OnDestroy_mBFB30DECA7DE75FD8AF54F760ADAF62EB7291E31 ();
// 0x00000065 System.Void LongTapMe::UnsubscribeEvent()
extern void LongTapMe_UnsubscribeEvent_m52AFC82592F269EE203BBC3F24B9BE4C52D9302B ();
// 0x00000066 System.Void LongTapMe::Start()
extern void LongTapMe_Start_m51AA4A13FBEEB976829FE7C8B337017292DEE6B8 ();
// 0x00000067 System.Void LongTapMe::On_LongTapStart(HedgehogTeam.EasyTouch.Gesture)
extern void LongTapMe_On_LongTapStart_m582CB5DBB71356BCA87BACE1DCA5DF703FA803CA ();
// 0x00000068 System.Void LongTapMe::On_LongTap(HedgehogTeam.EasyTouch.Gesture)
extern void LongTapMe_On_LongTap_m498E5EB14213C4AF10113A3D72AF08D7E90880D1 ();
// 0x00000069 System.Void LongTapMe::On_LongTapEnd(HedgehogTeam.EasyTouch.Gesture)
extern void LongTapMe_On_LongTapEnd_m0506B87C0198871EC64CB7DEDB8159323792A469 ();
// 0x0000006A System.Void LongTapMe::RandomColor()
extern void LongTapMe_RandomColor_mC9EB633FE9F73577D22DF2018EA87212D675393F ();
// 0x0000006B System.Void LongTapMe::.ctor()
extern void LongTapMe__ctor_m6496AF92C1B15512436B2D1F4359D15AC872B5DD ();
// 0x0000006C System.Void Swipe::OnEnable()
extern void Swipe_OnEnable_m701C4E847A4ACB5E1FB8A205508F09D4056CD329 ();
// 0x0000006D System.Void Swipe::OnDisable()
extern void Swipe_OnDisable_mADBE444EF54E865F15B42EC4E0EA7D39F8739D04 ();
// 0x0000006E System.Void Swipe::OnDestroy()
extern void Swipe_OnDestroy_mE2A66E32FFC35413BD9DBBD8E367CA841505F17E ();
// 0x0000006F System.Void Swipe::UnsubscribeEvent()
extern void Swipe_UnsubscribeEvent_m810C4A8E1B2F56C8EFB12AF25CE91B132C6314F2 ();
// 0x00000070 System.Void Swipe::On_SwipeStart(HedgehogTeam.EasyTouch.Gesture)
extern void Swipe_On_SwipeStart_m62DD6BD2A7B3D8011D11E8059CA0FA31C13D0432 ();
// 0x00000071 System.Void Swipe::On_Swipe(HedgehogTeam.EasyTouch.Gesture)
extern void Swipe_On_Swipe_m452120CE24AABC459A73658269655FE7812D0F35 ();
// 0x00000072 System.Void Swipe::On_SwipeEnd(HedgehogTeam.EasyTouch.Gesture)
extern void Swipe_On_SwipeEnd_m342AD97C1B11E4D8F15D7BE76B6D34F68074AB69 ();
// 0x00000073 System.Void Swipe::.ctor()
extern void Swipe__ctor_mEDA6F2DED2025C742AA2029AD8896DB65C3146E5 ();
// 0x00000074 System.Void TapMe::OnEnable()
extern void TapMe_OnEnable_mBA52DAF5AC43235EDC05D2E7D7C2E9A98A14D2BE ();
// 0x00000075 System.Void TapMe::OnDisable()
extern void TapMe_OnDisable_m9D5D587ED8C43F95C9D56B2DD366B6D1C7E43A61 ();
// 0x00000076 System.Void TapMe::OnDestroy()
extern void TapMe_OnDestroy_m0AEA6D037CDEAF9C4344EB4B67F5F038A8EA0F6B ();
// 0x00000077 System.Void TapMe::UnsubscribeEvent()
extern void TapMe_UnsubscribeEvent_m67DF957DE130A08F2D1E89EE78FBB699B06E5F0F ();
// 0x00000078 System.Void TapMe::On_SimpleTap(HedgehogTeam.EasyTouch.Gesture)
extern void TapMe_On_SimpleTap_m4A36A3515E9CC944DA57E96AD8A397BA8BA9E21D ();
// 0x00000079 System.Void TapMe::.ctor()
extern void TapMe__ctor_m7D6391ED786D5E92CC61307825E313FF91EF631A ();
// 0x0000007A System.Void TouchMe::OnEnable()
extern void TouchMe_OnEnable_m019FE783263BF7C692E304362C259C907D36A30F ();
// 0x0000007B System.Void TouchMe::OnDisable()
extern void TouchMe_OnDisable_m524C4771561149FB7218ADD0217274B09872B9A7 ();
// 0x0000007C System.Void TouchMe::OnDestroy()
extern void TouchMe_OnDestroy_m4C8EBF7F62CE31A52905377B14D0BB4B8C691824 ();
// 0x0000007D System.Void TouchMe::UnsubscribeEvent()
extern void TouchMe_UnsubscribeEvent_m31432F200DF166090AD601C0ECA684EDC872DF88 ();
// 0x0000007E System.Void TouchMe::Start()
extern void TouchMe_Start_mBCD3921FEBDF16C5D0078764FE8ECBD09169C258 ();
// 0x0000007F System.Void TouchMe::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern void TouchMe_On_TouchStart_m5D5F18D145B06E61E82CCF2308C6D34E60BFED2C ();
// 0x00000080 System.Void TouchMe::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void TouchMe_On_TouchDown_mBB5C13F97D2B25A6ECD4E533363567EB2B467CED ();
// 0x00000081 System.Void TouchMe::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void TouchMe_On_TouchUp_mF9F8108BF9727D348733472F38FE17AA24FABF88 ();
// 0x00000082 System.Void TouchMe::RandomColor()
extern void TouchMe_RandomColor_m5270C32B484B8DAB294E808117526C492EE18AD2 ();
// 0x00000083 System.Void TouchMe::.ctor()
extern void TouchMe__ctor_mCF17EBB3CE54F25951D5C5C2EC0F9296987E957B ();
// 0x00000084 System.Void PinchMe::OnEnable()
extern void PinchMe_OnEnable_m169A20FD5C16579821FDD3E54F3F495BEBAB1899 ();
// 0x00000085 System.Void PinchMe::OnDisable()
extern void PinchMe_OnDisable_m6989D877303244F2CAC32135CE20CAFB40A227C0 ();
// 0x00000086 System.Void PinchMe::OnDestroy()
extern void PinchMe_OnDestroy_m6D243F569C1937DF68D34734978B88F33D614990 ();
// 0x00000087 System.Void PinchMe::UnsubscribeEvent()
extern void PinchMe_UnsubscribeEvent_mF39844375DEEF16C725171545F3865DCB149FE5E ();
// 0x00000088 System.Void PinchMe::Start()
extern void PinchMe_Start_mEA98E2DBF3ECEBA71FC7BA8998CCEF96B9B938E1 ();
// 0x00000089 System.Void PinchMe::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void PinchMe_On_TouchStart2Fingers_m858F891D07436DE3EA0EE8D3FC114D3B5D9C033C ();
// 0x0000008A System.Void PinchMe::On_PinchIn(HedgehogTeam.EasyTouch.Gesture)
extern void PinchMe_On_PinchIn_mCEEBEDA7A82C3A1F314D974E43FF893E61167925 ();
// 0x0000008B System.Void PinchMe::On_PinchOut(HedgehogTeam.EasyTouch.Gesture)
extern void PinchMe_On_PinchOut_mDC5A08B5C92C4EB20EA3985643A9181F7E295302 ();
// 0x0000008C System.Void PinchMe::On_PinchEnd(HedgehogTeam.EasyTouch.Gesture)
extern void PinchMe_On_PinchEnd_mE08FBCCED6AC8474F1C50A997638A77E109B5605 ();
// 0x0000008D System.Void PinchMe::.ctor()
extern void PinchMe__ctor_mC148E8F1114DEF247041BFE6ED3F981AEA686461 ();
// 0x0000008E System.Void TooglePickMethodUI::SetPickMethod2Finger(System.Boolean)
extern void TooglePickMethodUI_SetPickMethod2Finger_mCC5D850796239A401E4A04E6FAA17111869EA930 ();
// 0x0000008F System.Void TooglePickMethodUI::SetPickMethod2Averager(System.Boolean)
extern void TooglePickMethodUI_SetPickMethod2Averager_m296B8E0802288A26941A226A9FC89DAC4C834E96 ();
// 0x00000090 System.Void TooglePickMethodUI::.ctor()
extern void TooglePickMethodUI__ctor_mB1B383DAF1018561DAD7C1184D45FB4FFCD3E663 ();
// 0x00000091 System.Void TwistMe::OnEnable()
extern void TwistMe_OnEnable_mC088E661BE5C529F317B51AA0B78AEC4ACFCB869 ();
// 0x00000092 System.Void TwistMe::OnDisable()
extern void TwistMe_OnDisable_mD7F7DDD38EF945CB620ED4DFC10A349530986022 ();
// 0x00000093 System.Void TwistMe::OnDestroy()
extern void TwistMe_OnDestroy_mB8A5F5FEF51E82322ECEF08B1B82E3967062DB3E ();
// 0x00000094 System.Void TwistMe::UnsubscribeEvent()
extern void TwistMe_UnsubscribeEvent_m7B4BCF9A689F41186C0AA9F958796037608C755F ();
// 0x00000095 System.Void TwistMe::Start()
extern void TwistMe_Start_mC6A0F64168BF753E5AE9E3FB1D020E5FE655EB68 ();
// 0x00000096 System.Void TwistMe::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwistMe_On_TouchStart2Fingers_mFFF75A1B207027E1C605C279618B75A31E2FFA44 ();
// 0x00000097 System.Void TwistMe::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern void TwistMe_On_Twist_mE89C6E729B06C376A893C3715B174FBAB9FB40F1 ();
// 0x00000098 System.Void TwistMe::On_TwistEnd(HedgehogTeam.EasyTouch.Gesture)
extern void TwistMe_On_TwistEnd_m424E01AD6EF2387A76FABFF95CBAC054AA48C24A ();
// 0x00000099 System.Void TwistMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwistMe_On_Cancel2Fingers_mFD08D8ACA10F6A61B5455F67F7AB12162450DC7A ();
// 0x0000009A System.Void TwistMe::.ctor()
extern void TwistMe__ctor_m06744BE7272661D1B810646B384755C1A4509651 ();
// 0x0000009B System.Void TwoDoubleTapMe::OnEnable()
extern void TwoDoubleTapMe_OnEnable_m2B11FBB5777441BE991146F63778A1A570E1D1F6 ();
// 0x0000009C System.Void TwoDoubleTapMe::OnDisable()
extern void TwoDoubleTapMe_OnDisable_mE0C828F1C208572A1D74337F9DAAE4003E04FDFF ();
// 0x0000009D System.Void TwoDoubleTapMe::OnDestroy()
extern void TwoDoubleTapMe_OnDestroy_m0537A4F20B25F0CCB1235D7297D264944A1230A7 ();
// 0x0000009E System.Void TwoDoubleTapMe::UnsubscribeEvent()
extern void TwoDoubleTapMe_UnsubscribeEvent_mB7E8FD6ECF721C15D9228E09C7D32984FC069657 ();
// 0x0000009F System.Void TwoDoubleTapMe::On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoDoubleTapMe_On_DoubleTap2Fingers_mD4B615169C0127E93616D25DDC9A9D0D738F4F5D ();
// 0x000000A0 System.Void TwoDoubleTapMe::.ctor()
extern void TwoDoubleTapMe__ctor_mD2BCCE139F26114852238D88404541A3B48BD474 ();
// 0x000000A1 System.Void TwoDragMe::OnEnable()
extern void TwoDragMe_OnEnable_m4D9291A4F42D0E4CCAD0A5E0A38C9530559F97BE ();
// 0x000000A2 System.Void TwoDragMe::OnDisable()
extern void TwoDragMe_OnDisable_m5B906CF331E3D147D171A6BFE12EB394BC26E7E8 ();
// 0x000000A3 System.Void TwoDragMe::OnDestroy()
extern void TwoDragMe_OnDestroy_mD49987B836652C20657BE68EB543623FB3095834 ();
// 0x000000A4 System.Void TwoDragMe::UnsubscribeEvent()
extern void TwoDragMe_UnsubscribeEvent_m8528DD558E23FE55EC234587C2578D42840D507C ();
// 0x000000A5 System.Void TwoDragMe::Start()
extern void TwoDragMe_Start_m8299AE6AFFA99BDA8D0F51E23B9EF81A3D7C4BA2 ();
// 0x000000A6 System.Void TwoDragMe::On_DragStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoDragMe_On_DragStart2Fingers_m8245A5D5CCC2328E5E9A85EEFDC15E93A0AA0DC5 ();
// 0x000000A7 System.Void TwoDragMe::On_Drag2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoDragMe_On_Drag2Fingers_m3D2B364638B7B17314181E863ED647082AE4BB88 ();
// 0x000000A8 System.Void TwoDragMe::On_DragEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7 ();
// 0x000000A9 System.Void TwoDragMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoDragMe_On_Cancel2Fingers_m08BF27F6834261DCC9C1DD6D02B890FB3E7DADEB ();
// 0x000000AA System.Void TwoDragMe::RandomColor()
extern void TwoDragMe_RandomColor_mF539871B48852F1E6407AC872917E58CAE62C031 ();
// 0x000000AB System.Void TwoDragMe::.ctor()
extern void TwoDragMe__ctor_mAB9F5C5DEE23B4AFA27EE46FC1A9DE277C27A44E ();
// 0x000000AC System.Void TwoLongTapMe::OnEnable()
extern void TwoLongTapMe_OnEnable_m24553C57C72307C05864F880DE2E06DF759B5179 ();
// 0x000000AD System.Void TwoLongTapMe::OnDisable()
extern void TwoLongTapMe_OnDisable_m8A597E272F4A4DCA984CF98175BD334EF6D90D4C ();
// 0x000000AE System.Void TwoLongTapMe::OnDestroy()
extern void TwoLongTapMe_OnDestroy_m834BA7E34436FBD3F1168C92BC41D8C806CE4DDC ();
// 0x000000AF System.Void TwoLongTapMe::UnsubscribeEvent()
extern void TwoLongTapMe_UnsubscribeEvent_mD6FD63B41BD44D2F2EA9B2F8AD3A0147A13F332E ();
// 0x000000B0 System.Void TwoLongTapMe::Start()
extern void TwoLongTapMe_Start_m25B7567F5274044250351D028F3413A15B68DB13 ();
// 0x000000B1 System.Void TwoLongTapMe::On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoLongTapMe_On_LongTapStart2Fingers_mC7B363620D7F512BBE58F081E55C91DE7CB043FB ();
// 0x000000B2 System.Void TwoLongTapMe::On_LongTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoLongTapMe_On_LongTap2Fingers_mABA3459A3EF0C678FC271172441CCD50269B35A0 ();
// 0x000000B3 System.Void TwoLongTapMe::On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA ();
// 0x000000B4 System.Void TwoLongTapMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoLongTapMe_On_Cancel2Fingers_mF5F2AB348A2221D96B083F7492F673145E660BDA ();
// 0x000000B5 System.Void TwoLongTapMe::RandomColor()
extern void TwoLongTapMe_RandomColor_m2EC75C8E459CEEF167AC03C733FBD6C8661C034F ();
// 0x000000B6 System.Void TwoLongTapMe::.ctor()
extern void TwoLongTapMe__ctor_m6BAB410EEAFED8FB06624FC8E66EA9506601D434 ();
// 0x000000B7 System.Void TwoSwipe::OnEnable()
extern void TwoSwipe_OnEnable_mD01720E2E3D1FB0F5F6485FDE2E89087724160D2 ();
// 0x000000B8 System.Void TwoSwipe::OnDisable()
extern void TwoSwipe_OnDisable_m072399D0FB362E3F9BAD878F4F9D30A5BDDD48A5 ();
// 0x000000B9 System.Void TwoSwipe::OnDestroy()
extern void TwoSwipe_OnDestroy_m1F1BB31AECD941C287714260116BDC48312A0020 ();
// 0x000000BA System.Void TwoSwipe::UnsubscribeEvent()
extern void TwoSwipe_UnsubscribeEvent_m621E8D3873D00BD409B92D713D525AAD377B45DE ();
// 0x000000BB System.Void TwoSwipe::On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoSwipe_On_SwipeStart2Fingers_m996F4113B4E14D998D290641321014E90F14FD79 ();
// 0x000000BC System.Void TwoSwipe::On_Swipe2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoSwipe_On_Swipe2Fingers_mFB811F96A67015B4782939BE6FEDE357D9A69895 ();
// 0x000000BD System.Void TwoSwipe::On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoSwipe_On_SwipeEnd2Fingers_mDB441678D2EEBB257D550C07493925FDCE7985A0 ();
// 0x000000BE System.Void TwoSwipe::.ctor()
extern void TwoSwipe__ctor_mBD0137AA0BA8843547F3646F7B7D27C26DF5B255 ();
// 0x000000BF System.Void TwoTapMe::OnEnable()
extern void TwoTapMe_OnEnable_mE11A33FC435C98E2DD8CAC8417E8CD0C181CBF8C ();
// 0x000000C0 System.Void TwoTapMe::OnDisable()
extern void TwoTapMe_OnDisable_m090FCAF372A993258677033A9FA146AAC24DB1D8 ();
// 0x000000C1 System.Void TwoTapMe::OnDestroy()
extern void TwoTapMe_OnDestroy_mC61C7535ACB2FCC296C68513E7907F72F7BC7201 ();
// 0x000000C2 System.Void TwoTapMe::UnsubscribeEvent()
extern void TwoTapMe_UnsubscribeEvent_m507D3A6FC8FEBB6E4E54A4AEBC9EB6D2EE960C59 ();
// 0x000000C3 System.Void TwoTapMe::On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoTapMe_On_SimpleTap2Fingers_mFD1FDE5B180135498BB293517C2E60FB82869AAF ();
// 0x000000C4 System.Void TwoTapMe::RandomColor()
extern void TwoTapMe_RandomColor_m9B8DADD685E5A235EDBD1CADBEDB76469B8EBDA7 ();
// 0x000000C5 System.Void TwoTapMe::.ctor()
extern void TwoTapMe__ctor_mCBA3A52FFCF5F4A399A4CE9AD115DBBF3E8BC095 ();
// 0x000000C6 System.Void TwoTouchMe::OnEnable()
extern void TwoTouchMe_OnEnable_mEADF58E4D9A10CE69524FAC5F05EEB0445DD6540 ();
// 0x000000C7 System.Void TwoTouchMe::OnDisable()
extern void TwoTouchMe_OnDisable_mAA27BCB9D49F63DE42CD442377ACD14743F30E82 ();
// 0x000000C8 System.Void TwoTouchMe::OnDestroy()
extern void TwoTouchMe_OnDestroy_m154CB1223709052D3CF256B51017D289036F770E ();
// 0x000000C9 System.Void TwoTouchMe::UnsubscribeEvent()
extern void TwoTouchMe_UnsubscribeEvent_mC4B6CE024DF6A020633D5A68813AB1A4DF9BCEEA ();
// 0x000000CA System.Void TwoTouchMe::Start()
extern void TwoTouchMe_Start_m29E68CF505FCC00AF0CF622AF832E2AE063D1DEC ();
// 0x000000CB System.Void TwoTouchMe::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoTouchMe_On_TouchStart2Fingers_m80A5B62BDEA9EBB501870D3515BEA904FE6D05F7 ();
// 0x000000CC System.Void TwoTouchMe::On_TouchDown2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoTouchMe_On_TouchDown2Fingers_m5F2DBFD060A4B609ADEE268FB1A00B7F6EA5FCFE ();
// 0x000000CD System.Void TwoTouchMe::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06 ();
// 0x000000CE System.Void TwoTouchMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void TwoTouchMe_On_Cancel2Fingers_mC1A9360BED415B5C6B1C965B678E2BE310C12511 ();
// 0x000000CF System.Void TwoTouchMe::RandomColor()
extern void TwoTouchMe_RandomColor_m085E3A052F8C8A608915FAD9944ADE5DB04FE322 ();
// 0x000000D0 System.Void TwoTouchMe::.ctor()
extern void TwoTouchMe__ctor_mEB426FAC62BB58413573FBD998C5E81F89AE94BC ();
// 0x000000D1 System.Void ETWindow::OnEnable()
extern void ETWindow_OnEnable_mB5D997AA20BC14C1B49B8EF371844C5EFE5E2BE7 ();
// 0x000000D2 System.Void ETWindow::OnDestroy()
extern void ETWindow_OnDestroy_m97C3EF2E32CD74B617F0C3A2F60CAC4B72691AEE ();
// 0x000000D3 System.Void ETWindow::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern void ETWindow_On_TouchStart_mF9298CD7FEA2EA435C477B1720FBC5CBB3FA7690 ();
// 0x000000D4 System.Void ETWindow::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void ETWindow_On_TouchDown_mC980459D3E1017A72584FF41998E9C038E8D071F ();
// 0x000000D5 System.Void ETWindow::.ctor()
extern void ETWindow__ctor_m74F0467482D00E06A07E72F73F5D6F9D0E198486 ();
// 0x000000D6 System.Void GlobalEasyTouchEvent::OnEnable()
extern void GlobalEasyTouchEvent_OnEnable_mF64F27C95AD693F4BA6D8A651B204D8541F22892 ();
// 0x000000D7 System.Void GlobalEasyTouchEvent::OnDestroy()
extern void GlobalEasyTouchEvent_OnDestroy_m642CAB87ED75B3736AA952EA27A8E118C57FE3A1 ();
// 0x000000D8 System.Void GlobalEasyTouchEvent::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void GlobalEasyTouchEvent_On_TouchDown_mCAA00AEF37E23149523C6E28C48A56EC406E77B6 ();
// 0x000000D9 System.Void GlobalEasyTouchEvent::On_OverUIElement(HedgehogTeam.EasyTouch.Gesture)
extern void GlobalEasyTouchEvent_On_OverUIElement_mC8EC40CD57C00D761986053300D206D880C5D8BA ();
// 0x000000DA System.Void GlobalEasyTouchEvent::On_UIElementTouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void GlobalEasyTouchEvent_On_UIElementTouchUp_mA2848C78736BAD880D4D0D836F365AA060B1A4F5 ();
// 0x000000DB System.Void GlobalEasyTouchEvent::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void GlobalEasyTouchEvent_On_TouchUp_mC4CAC1719E04B1BB2CEE6F58E51BF2E365A427C3 ();
// 0x000000DC System.Void GlobalEasyTouchEvent::.ctor()
extern void GlobalEasyTouchEvent__ctor_mF96541D57730508FB87233D4B8D723D771D83558 ();
// 0x000000DD System.Void UICompatibility::SetCompatibility(System.Boolean)
extern void UICompatibility_SetCompatibility_m469CB267AC36BBA0FB46FAA5DF5FE93BAC664ED9 ();
// 0x000000DE System.Void UICompatibility::.ctor()
extern void UICompatibility__ctor_mE0E38E77CBFDEA33A1E3632FBE804F6064E03A73 ();
// 0x000000DF System.Void UIWindow::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void UIWindow_OnDrag_m0C823B17A262F9D6294E0015EB1627EE246FD591 ();
// 0x000000E0 System.Void UIWindow::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void UIWindow_OnPointerDown_m58491AECE96A6331EC5C91614A1F78126F189AB0 ();
// 0x000000E1 System.Void UIWindow::.ctor()
extern void UIWindow__ctor_m789C7BB02C86B2D6627A8A13CC4BBECF9D0207E8 ();
// 0x000000E2 System.Void UIDrag::OnEnable()
extern void UIDrag_OnEnable_m53F3D1A1B1141E2A30F7633C81224DEAE59EE620 ();
// 0x000000E3 System.Void UIDrag::OnDestroy()
extern void UIDrag_OnDestroy_mDBA34F76DF1D49FC4EAB15E75C912C9B6D1A419B ();
// 0x000000E4 System.Void UIDrag::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern void UIDrag_On_TouchStart_m65ECE69393864CB1BAE99547E38BB74FCDF26A90 ();
// 0x000000E5 System.Void UIDrag::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void UIDrag_On_TouchDown_mDA6137F376EA0D51188523F0EB7F12A57862AFEF ();
// 0x000000E6 System.Void UIDrag::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void UIDrag_On_TouchUp_mB610AEAAE2080078E5035964592074FED1AA317E ();
// 0x000000E7 System.Void UIDrag::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void UIDrag_On_TouchStart2Fingers_m7E4609C89923E3E57F4F319306A870E6B8003A62 ();
// 0x000000E8 System.Void UIDrag::On_TouchDown2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void UIDrag_On_TouchDown2Fingers_m1D3686F31D71771CF4C04E43A1AD64C0200FF598 ();
// 0x000000E9 System.Void UIDrag::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void UIDrag_On_TouchUp2Fingers_m9835E0ACCB3C9E978D8A642C5EEDCC8C91689ACC ();
// 0x000000EA System.Void UIDrag::.ctor()
extern void UIDrag__ctor_mA0B84C6372221FB61265BCDA320B577BE40C2A4B ();
// 0x000000EB System.Void UIPinch::OnEnable()
extern void UIPinch_OnEnable_m63DC2215BEC9FB17C5D4C3859FE58565DA8E3239 ();
// 0x000000EC System.Void UIPinch::OnDestroy()
extern void UIPinch_OnDestroy_mB910E8CCBD3760031F31C9EAA2B20F0E8CDF62EE ();
// 0x000000ED System.Void UIPinch::On_Pinch(HedgehogTeam.EasyTouch.Gesture)
extern void UIPinch_On_Pinch_m4EE743B766948A9807B7A008DE1EDB1D7219750C ();
// 0x000000EE System.Void UIPinch::.ctor()
extern void UIPinch__ctor_m13DB91E62E69AAFDAE6272E07AD753438853B548 ();
// 0x000000EF System.Void UITwist::OnEnable()
extern void UITwist_OnEnable_m197511549C6C8217FDE1FA2264F5A0A25B1F0FC2 ();
// 0x000000F0 System.Void UITwist::OnDestroy()
extern void UITwist_OnDestroy_m183699EEB9681FC54940FB2114819D819E5B05DB ();
// 0x000000F1 System.Void UITwist::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern void UITwist_On_Twist_mE02A43AECF2B05D0C6DD523F15CB8DE872575D63 ();
// 0x000000F2 System.Void UITwist::.ctor()
extern void UITwist__ctor_mE6B86357BEEF6CE575A51F4163A2263C9489ACC5 ();
// 0x000000F3 System.Void RTS_NewSyntaxe::Start()
extern void RTS_NewSyntaxe_Start_m7952442576B46EC4B8B1F36373AA360DC6AD9C3E ();
// 0x000000F4 System.Void RTS_NewSyntaxe::Update()
extern void RTS_NewSyntaxe_Update_m646B814BD0D64888F94452B9F8E3A1C7E7B33B09 ();
// 0x000000F5 System.Void RTS_NewSyntaxe::ResteColor()
extern void RTS_NewSyntaxe_ResteColor_m02294425582808D35870A04643F9740EA2E46124 ();
// 0x000000F6 System.Void RTS_NewSyntaxe::.ctor()
extern void RTS_NewSyntaxe__ctor_m9A3837AAE85A4A62F5C41885CCCFFFF00C33BFD5 ();
// 0x000000F7 System.Void SimpleActionExample::Start()
extern void SimpleActionExample_Start_m320F01A2C4A35902AE14A720EC03E9976D03B688 ();
// 0x000000F8 System.Void SimpleActionExample::ChangeColor(HedgehogTeam.EasyTouch.Gesture)
extern void SimpleActionExample_ChangeColor_m3F2078AFB928FFE24E82778AD1A27F6FE6C42145 ();
// 0x000000F9 System.Void SimpleActionExample::TimePressed(HedgehogTeam.EasyTouch.Gesture)
extern void SimpleActionExample_TimePressed_mE13BBA99D2DAC7FD390EB1602BD1DE39872A7E1A ();
// 0x000000FA System.Void SimpleActionExample::DisplaySwipeAngle(HedgehogTeam.EasyTouch.Gesture)
extern void SimpleActionExample_DisplaySwipeAngle_m71627B252382DA87916DC91CB6763AA5DCEAF12D ();
// 0x000000FB System.Void SimpleActionExample::ChangeText(System.String)
extern void SimpleActionExample_ChangeText_m324A813064BD45D1EED2F3A0DEAF1E7203540C3F ();
// 0x000000FC System.Void SimpleActionExample::ResetScale()
extern void SimpleActionExample_ResetScale_m917CFAD531B9464E384DDCC95C6F3E6793F6D24C ();
// 0x000000FD System.Void SimpleActionExample::RandomColor()
extern void SimpleActionExample_RandomColor_m646B92284C1C46EF2D6FFFA5777895A4A0B30B31 ();
// 0x000000FE System.Void SimpleActionExample::.ctor()
extern void SimpleActionExample__ctor_mB16BE5538D9BE220D2D569230C499C7C576DDBE3 ();
// 0x000000FF System.Void ETCSetDirectActionTransform::Start()
extern void ETCSetDirectActionTransform_Start_m787EC67F4B8F298F397A5EAE7881AF0593B01891 ();
// 0x00000100 System.Void ETCSetDirectActionTransform::.ctor()
extern void ETCSetDirectActionTransform__ctor_m68B806C234EB7A1ED71327AAC437510FF18AE34F ();
// 0x00000101 System.Void ButtonInputUI::Update()
extern void ButtonInputUI_Update_m37F26A6E34E20DCDA49F963CBDB0748BB31C42C7 ();
// 0x00000102 System.Collections.IEnumerator ButtonInputUI::ClearText(UnityEngine.UI.Text)
extern void ButtonInputUI_ClearText_m190A836B685076A4057231633C733728B5824ECD ();
// 0x00000103 System.Void ButtonInputUI::SetSwipeIn(System.Boolean)
extern void ButtonInputUI_SetSwipeIn_m9C2B071D39C3B27E129212281F658AB35CA9212A ();
// 0x00000104 System.Void ButtonInputUI::SetSwipeOut(System.Boolean)
extern void ButtonInputUI_SetSwipeOut_m4478A95FE1B1B7EA5CC4DB6ECED6BE5740045559 ();
// 0x00000105 System.Void ButtonInputUI::setTimePush(System.Boolean)
extern void ButtonInputUI_setTimePush_mF1D0BAE7ABCD052E2A3E7571433B3F6E916FBA15 ();
// 0x00000106 System.Void ButtonInputUI::.ctor()
extern void ButtonInputUI__ctor_m9A933B71935FA104C17FC0CEE382D6873ABFA953 ();
// 0x00000107 System.Void ButtonUIEvent::Down()
extern void ButtonUIEvent_Down_m7B37FBBBCDE726B1CDAEAF7B6135FF0ACD4B4C19 ();
// 0x00000108 System.Void ButtonUIEvent::Up()
extern void ButtonUIEvent_Up_m38C58DE413B3DB6D5FFD1AA75D4BE41ADBC6ED1F ();
// 0x00000109 System.Void ButtonUIEvent::Press()
extern void ButtonUIEvent_Press_m37A8B89B6553F1A3B5275DFA130A4743993577F7 ();
// 0x0000010A System.Void ButtonUIEvent::PressValue(System.Single)
extern void ButtonUIEvent_PressValue_m253AA571B4BB196C6D977C931A343BD0307472B9 ();
// 0x0000010B System.Collections.IEnumerator ButtonUIEvent::ClearText(UnityEngine.UI.Text)
extern void ButtonUIEvent_ClearText_m9A37084EF52E33F49DD49D5DAD2A96D73D8FF9A6 ();
// 0x0000010C System.Void ButtonUIEvent::.ctor()
extern void ButtonUIEvent__ctor_m8351A9DADA01FCD65B9D7DEC5403815274106676 ();
// 0x0000010D System.Void ControlUIEvent::Update()
extern void ControlUIEvent_Update_m355F2AFDFEAE0B244B81B06483491215C2DFCACF ();
// 0x0000010E System.Void ControlUIEvent::MoveStart()
extern void ControlUIEvent_MoveStart_m5FE3AFA1DB231EDE515FC390CD29FE65C002B680 ();
// 0x0000010F System.Void ControlUIEvent::Move(UnityEngine.Vector2)
extern void ControlUIEvent_Move_m59A9D5E093DC0924DAF4E7EDD3B90C790A544ABC ();
// 0x00000110 System.Void ControlUIEvent::MoveSpeed(UnityEngine.Vector2)
extern void ControlUIEvent_MoveSpeed_m0B56C4FF9583F0CB9AE875ACB5AC306180488F65 ();
// 0x00000111 System.Void ControlUIEvent::MoveEnd()
extern void ControlUIEvent_MoveEnd_m21A02FF1675D31FABDB6F20DAEFB3F2408167643 ();
// 0x00000112 System.Void ControlUIEvent::TouchStart()
extern void ControlUIEvent_TouchStart_mCDE2D3554A6CEFB7252E8D0FA9B6C209A2231892 ();
// 0x00000113 System.Void ControlUIEvent::TouchUp()
extern void ControlUIEvent_TouchUp_m5CD3169D487B66AEC27D1F5CF1A6FF28C86B8B3A ();
// 0x00000114 System.Void ControlUIEvent::DownRight()
extern void ControlUIEvent_DownRight_mE61123CB414ACE01CEE7B37BF4AE06C673339103 ();
// 0x00000115 System.Void ControlUIEvent::DownDown()
extern void ControlUIEvent_DownDown_m5F88D1AED44DF493ABE0CA3700D611D7110141F3 ();
// 0x00000116 System.Void ControlUIEvent::DownLeft()
extern void ControlUIEvent_DownLeft_mB52C736B52DCD4677A2ACADE0B5CD8F40208B925 ();
// 0x00000117 System.Void ControlUIEvent::DownUp()
extern void ControlUIEvent_DownUp_mEDA5F6357A53AC3BF2401AE2778F4690999D9E0E ();
// 0x00000118 System.Void ControlUIEvent::Right()
extern void ControlUIEvent_Right_m03F3EA8631583EFAD860AEB9D54527F3134F2E92 ();
// 0x00000119 System.Void ControlUIEvent::Down()
extern void ControlUIEvent_Down_m60CEB122C0322ABF30884110E011085885EBFB8F ();
// 0x0000011A System.Void ControlUIEvent::Left()
extern void ControlUIEvent_Left_mB740ABB49DC269F76167D97041457C6B3839AFD3 ();
// 0x0000011B System.Void ControlUIEvent::Up()
extern void ControlUIEvent_Up_mBE430BF8A8D0C2B55BEFE052E4D25E5B006DE6A1 ();
// 0x0000011C System.Collections.IEnumerator ControlUIEvent::ClearText(UnityEngine.UI.Text)
extern void ControlUIEvent_ClearText_mC2A10439EE2B50D983EA96004841B0B040A2873A ();
// 0x0000011D System.Void ControlUIEvent::.ctor()
extern void ControlUIEvent__ctor_m54824CAAC9B5EC4670995E620CD688C85B1F9361 ();
// 0x0000011E System.Void ControlUIInput::Update()
extern void ControlUIInput_Update_mCED806C8DBE5CBE5348FB78A17242F742DDDDB10 ();
// 0x0000011F System.Collections.IEnumerator ControlUIInput::ClearText(UnityEngine.UI.Text)
extern void ControlUIInput_ClearText_m7A57694C041703FD828F146991BD5E788AF75B77 ();
// 0x00000120 System.Void ControlUIInput::.ctor()
extern void ControlUIInput__ctor_mED47F31F89F8E9AA22BB7D7D64D694881E82068D ();
// 0x00000121 System.Void DPadParameterUI::SetClassicalInertia(System.Boolean)
extern void DPadParameterUI_SetClassicalInertia_mC56C691C6A1A05518078612A9BBE5E270F8D9020 ();
// 0x00000122 System.Void DPadParameterUI::SetTimePushInertia(System.Boolean)
extern void DPadParameterUI_SetTimePushInertia_m271E2EBA9B61407C1EA61BA75088A135D1B0B601 ();
// 0x00000123 System.Void DPadParameterUI::SetClassicalTwoAxesCount()
extern void DPadParameterUI_SetClassicalTwoAxesCount_m75493FF83663DA7225F11C6746ECA00D73EF7A9C ();
// 0x00000124 System.Void DPadParameterUI::SetClassicalFourAxesCount()
extern void DPadParameterUI_SetClassicalFourAxesCount_m2FF4FF6F06E2B27ED21329098FEA100F6BA04E08 ();
// 0x00000125 System.Void DPadParameterUI::SetTimePushTwoAxesCount()
extern void DPadParameterUI_SetTimePushTwoAxesCount_m902D3B36E4B41FF116E5D32A4875BA6A6A06BC14 ();
// 0x00000126 System.Void DPadParameterUI::SetTimePushFourAxesCount()
extern void DPadParameterUI_SetTimePushFourAxesCount_m827638037CE107FF7ECFC88419149A91DB516DD0 ();
// 0x00000127 System.Void DPadParameterUI::.ctor()
extern void DPadParameterUI__ctor_mE2E796422671F940FF2F5BC2CC89043577E6A9F1 ();
// 0x00000128 System.Void FPSPlayerControl::Awake()
extern void FPSPlayerControl_Awake_mAB80AF9A3CB050EF07935F1F242C71E17809FA67 ();
// 0x00000129 System.Void FPSPlayerControl::Update()
extern void FPSPlayerControl_Update_m70AC5CF9E2C826BAEE0B696783E85F8FBF03E194 ();
// 0x0000012A System.Void FPSPlayerControl::MoveStart()
extern void FPSPlayerControl_MoveStart_m0A5946E0C4AD7AE31DF3D458183F0B6ED7B41689 ();
// 0x0000012B System.Void FPSPlayerControl::MoveStop()
extern void FPSPlayerControl_MoveStop_m42847790894454BF4A28F27AD75515D410301E4E ();
// 0x0000012C System.Void FPSPlayerControl::GunFire()
extern void FPSPlayerControl_GunFire_m355430928DD1EE8A7CF4B261245C74E41B19C618 ();
// 0x0000012D System.Void FPSPlayerControl::TouchPadSwipe(System.Boolean)
extern void FPSPlayerControl_TouchPadSwipe_mEB376362C5147F593A8F9B4E1F448A3E1E007197 ();
// 0x0000012E System.Collections.IEnumerator FPSPlayerControl::Flash()
extern void FPSPlayerControl_Flash_m29B371AED745F7D1234A6ED2562C7C7405E2E0CE ();
// 0x0000012F System.Collections.IEnumerator FPSPlayerControl::Reload()
extern void FPSPlayerControl_Reload_m92254D38E482EC998BCCD83FC2497DBC4FA3C398 ();
// 0x00000130 System.Void FPSPlayerControl::.ctor()
extern void FPSPlayerControl__ctor_mEAECB3EFDE0DBBBBF74AC6E8F82E6123DF5607FF ();
// 0x00000131 System.Void ImpactEffect::Start()
extern void ImpactEffect_Start_m2851D6A982D3328CBC3EDFC6E37883DA3EB6DD03 ();
// 0x00000132 System.Void ImpactEffect::Update()
extern void ImpactEffect_Update_m27A802F075CBB32E69A30ED17B331A3D37B06064 ();
// 0x00000133 System.Void ImpactEffect::.ctor()
extern void ImpactEffect__ctor_m8A2F621CE36895E5D1E98186031ADC199994B7CF ();
// 0x00000134 System.Void AxisXUi::ActivateAxisX(System.Boolean)
extern void AxisXUi_ActivateAxisX_m2CD03246D94408476A6519DA8C3C3B3A19DABB96 ();
// 0x00000135 System.Void AxisXUi::InvertedAxisX(System.Boolean)
extern void AxisXUi_InvertedAxisX_m2CACCF93264FBB63D38D7DC00A2E5611694F7FD3 ();
// 0x00000136 System.Void AxisXUi::DeadAxisX(System.Single)
extern void AxisXUi_DeadAxisX_m611EF3AE6005B4D73E93BEF548D70A5FED5E7546 ();
// 0x00000137 System.Void AxisXUi::SpeedAxisX(System.Single)
extern void AxisXUi_SpeedAxisX_mD3A276A758E8556C3870D520CCD5FFF91F2B6CFE ();
// 0x00000138 System.Void AxisXUi::IsInertiaX(System.Boolean)
extern void AxisXUi_IsInertiaX_mA7454219216A27467B2C73A36B9B6379FB67FCBD ();
// 0x00000139 System.Void AxisXUi::InertiaSpeedX(System.Single)
extern void AxisXUi_InertiaSpeedX_mDFB8D410260146841B07FFAE4AD4B6E3FF583D95 ();
// 0x0000013A System.Void AxisXUi::ActivateAxisY(System.Boolean)
extern void AxisXUi_ActivateAxisY_mC6CF5790DFF354E7EFEF0036E9D2D499D41987ED ();
// 0x0000013B System.Void AxisXUi::InvertedAxisY(System.Boolean)
extern void AxisXUi_InvertedAxisY_m4A693E3BEAEA2ED92C37724CC17D6B0F01B1FD14 ();
// 0x0000013C System.Void AxisXUi::DeadAxisY(System.Single)
extern void AxisXUi_DeadAxisY_m21A272462C85CF2B165A58782F3C5AE585FA2565 ();
// 0x0000013D System.Void AxisXUi::SpeedAxisY(System.Single)
extern void AxisXUi_SpeedAxisY_mE53C5CA0509E084511BCC33BD2A0845D51CE305A ();
// 0x0000013E System.Void AxisXUi::IsInertiaY(System.Boolean)
extern void AxisXUi_IsInertiaY_mF97C07B12CDE7AB761EC560735785947DAB7F691 ();
// 0x0000013F System.Void AxisXUi::InertiaSpeedY(System.Single)
extern void AxisXUi_InertiaSpeedY_m287AFE40BA971185350CEA95E35A911924EA4C6C ();
// 0x00000140 System.Void AxisXUi::.ctor()
extern void AxisXUi__ctor_m77FEB3F0312C697AAFEC4491B3DD141343F880E3 ();
// 0x00000141 System.Void LoadLevelScript::LoadMainMenu()
extern void LoadLevelScript_LoadMainMenu_m70C1C19730191EFBE232CD94CEAC04D233FD5F51 ();
// 0x00000142 System.Void LoadLevelScript::LoadJoystickEvent()
extern void LoadLevelScript_LoadJoystickEvent_mDF330EC86C89AE3CA5382B8B466D3BC43F2D36F8 ();
// 0x00000143 System.Void LoadLevelScript::LoadJoysticParameter()
extern void LoadLevelScript_LoadJoysticParameter_mD34065AB8E652E00811231FDAE32F0D56A3BAF67 ();
// 0x00000144 System.Void LoadLevelScript::LoadDPadEvent()
extern void LoadLevelScript_LoadDPadEvent_m87EBE9820724F667C4FD34697642E6E81B021481 ();
// 0x00000145 System.Void LoadLevelScript::LoadDPadClassicalTime()
extern void LoadLevelScript_LoadDPadClassicalTime_m67AAE44F180BEBCC06C907B8ECE180A5A217E833 ();
// 0x00000146 System.Void LoadLevelScript::LoadTouchPad()
extern void LoadLevelScript_LoadTouchPad_m6E330A7B07F8D9B891ABEB72361F872BDC8A50FE ();
// 0x00000147 System.Void LoadLevelScript::LoadButton()
extern void LoadLevelScript_LoadButton_mE19C26B4CD1260C366A0FC51A3C0F676B25CDFAA ();
// 0x00000148 System.Void LoadLevelScript::LoadFPS()
extern void LoadLevelScript_LoadFPS_mDAF10CD6D6EC9476D4D26E46F7CFA565896FF521 ();
// 0x00000149 System.Void LoadLevelScript::LoadThird()
extern void LoadLevelScript_LoadThird_m6408479A75EDD4139A3003161056D0A785707DB2 ();
// 0x0000014A System.Void LoadLevelScript::LoadThirddungeon()
extern void LoadLevelScript_LoadThirddungeon_mC8C19490D26AA28BF522FB776B82A5FE0E4C8938 ();
// 0x0000014B System.Void LoadLevelScript::.ctor()
extern void LoadLevelScript__ctor_m6A3AD55CE73FC214BE20B4FA990C4C5C9DDA4162 ();
// 0x0000014C System.Void TouchPadUIEvent::TouchDown()
extern void TouchPadUIEvent_TouchDown_m38CA3BCC215E0DC95B158C8D3BC97225745612EC ();
// 0x0000014D System.Void TouchPadUIEvent::TouchEvt(UnityEngine.Vector2)
extern void TouchPadUIEvent_TouchEvt_mF9416496C893E0C4E328770513D50B3CC0E77890 ();
// 0x0000014E System.Void TouchPadUIEvent::TouchUp()
extern void TouchPadUIEvent_TouchUp_m591BBF133B55261F98EAC73AEC2EB8F548914682 ();
// 0x0000014F System.Collections.IEnumerator TouchPadUIEvent::ClearText(UnityEngine.UI.Text)
extern void TouchPadUIEvent_ClearText_m2F56C2CD146F74128BF43D33E6FEC5D879E28374 ();
// 0x00000150 System.Void TouchPadUIEvent::.ctor()
extern void TouchPadUIEvent__ctor_m3E1BD8B6B27E6C46D8010981FB3EDEE2210A562B ();
// 0x00000151 System.Void CharacterAnimation::Start()
extern void CharacterAnimation_Start_mEB7C3AD926CC597F6A8FD95735DF7B22D8FF5E06 ();
// 0x00000152 System.Void CharacterAnimation::LateUpdate()
extern void CharacterAnimation_LateUpdate_m123CBF299A51B1C3BF2413B9CDA5F37AAABCE424 ();
// 0x00000153 System.Void CharacterAnimation::.ctor()
extern void CharacterAnimation__ctor_m02CB4BD3E568E4A191B854F1916BFFFEC376CBEB ();
// 0x00000154 System.Void CharacterAnimationDungeon::Start()
extern void CharacterAnimationDungeon_Start_mF1A5028ABD6C34021DFDD9F44AEE2C494A25B9CE ();
// 0x00000155 System.Void CharacterAnimationDungeon::LateUpdate()
extern void CharacterAnimationDungeon_LateUpdate_mC7DA1C93E92A47DEC60BA592AF97C79AFAA0A15E ();
// 0x00000156 System.Void CharacterAnimationDungeon::.ctor()
extern void CharacterAnimationDungeon__ctor_m3679F4E917AA81FAC54B7F0E57F02E783E134436 ();
// 0x00000157 System.Void SliderText::SetText(System.Single)
extern void SliderText_SetText_mFC24AC2F4A8A089AE8251C97A0F7925F6971990B ();
// 0x00000158 System.Void SliderText::.ctor()
extern void SliderText__ctor_mFC54ECE8C5F931E8DE1196A6395B8E2B7E7E8742 ();
// 0x00000159 UnityEngine.RectTransform ComponentExtensions::rectTransform(UnityEngine.Component)
extern void ComponentExtensions_rectTransform_mF088C3344E063DCA92A493483D0BD87F77DAF423 ();
// 0x0000015A System.Single ComponentExtensions::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void ComponentExtensions_Remap_mC4490DF86B8396580E71E523AC4D690A068A0075 ();
// 0x0000015B System.Void ETCArea::.ctor()
extern void ETCArea__ctor_m6E0601AC796B33B11564FF2E95097E2B550A3C59 ();
// 0x0000015C System.Void ETCArea::Awake()
extern void ETCArea_Awake_m9B60FB2516B3218F290FF9B44724604DFD04160F ();
// 0x0000015D System.Void ETCArea::ApplyPreset(ETCArea_AreaPreset)
extern void ETCArea_ApplyPreset_m6ABAE8611FBEB423D4581216CD5B9EC28C92A000 ();
// 0x0000015E UnityEngine.Transform ETCAxis::get_directTransform()
extern void ETCAxis_get_directTransform_m81A3C04FA49EB7F28D4E29AD240E639578DA37E1 ();
// 0x0000015F System.Void ETCAxis::set_directTransform(UnityEngine.Transform)
extern void ETCAxis_set_directTransform_m49C57210686A1E7D958315C6DBEB0F819C0EB184 ();
// 0x00000160 System.Void ETCAxis::.ctor(System.String)
extern void ETCAxis__ctor_m05B005D0EC30FCB465468B4A2787B57ABE1B5436 ();
// 0x00000161 System.Void ETCAxis::InitAxis()
extern void ETCAxis_InitAxis_m1068815F1388637523F3C91D223D11B5E8565163 ();
// 0x00000162 System.Void ETCAxis::UpdateAxis(System.Single,System.Boolean,ETCBase_ControlType,System.Boolean)
extern void ETCAxis_UpdateAxis_m0402F3C31F4BFDD0351CE8586858595DC2720AA6 ();
// 0x00000163 System.Void ETCAxis::UpdateButton()
extern void ETCAxis_UpdateButton_mC242858FC4CA302E0A2FCACB1BD26889974C1C1F ();
// 0x00000164 System.Void ETCAxis::ResetAxis()
extern void ETCAxis_ResetAxis_mFFCD5D21A0C6155E4610782B479ADC72ECD977DF ();
// 0x00000165 System.Void ETCAxis::DoDirectAction()
extern void ETCAxis_DoDirectAction_m36FDFC94ADF1E16A46A1A3CEF96E9E725A2A4F3D ();
// 0x00000166 System.Void ETCAxis::DoGravity()
extern void ETCAxis_DoGravity_m220F56A3B663AF2E0268D406600B2BE4D7B55EA9 ();
// 0x00000167 System.Void ETCAxis::ComputAxisValue(System.Single,ETCBase_ControlType,System.Boolean,System.Boolean)
extern void ETCAxis_ComputAxisValue_m1676411BB915BF9F2E02FDBFD6D82E8D1FCCA0FF ();
// 0x00000168 UnityEngine.Vector3 ETCAxis::GetInfluencedAxis()
extern void ETCAxis_GetInfluencedAxis_mA98AF53336C3C9053188E085BCC989EFF3B5921A ();
// 0x00000169 System.Single ETCAxis::GetAngle()
extern void ETCAxis_GetAngle_mE1666B74F75D34558B26CEB22530BE605B22CEA3 ();
// 0x0000016A System.Void ETCAxis::DoAutoStabilisation()
extern void ETCAxis_DoAutoStabilisation_m44449B1C5961AB6B5CB37A4516B8D147BC7ECA7F ();
// 0x0000016B System.Void ETCAxis::DoAngleLimitation()
extern void ETCAxis_DoAngleLimitation_mE3A4668835BD4EC44969794B3748575B2464CC70 ();
// 0x0000016C System.Void ETCAxis::InitDeadCurve()
extern void ETCAxis_InitDeadCurve_m792C52B4C1DC170EB2744EB38BF1386F2E434777 ();
// 0x0000016D ETCBase_RectAnchor ETCBase::get_anchor()
extern void ETCBase_get_anchor_m5264815ABD81F40475A94DD8F22200966A6F5671 ();
// 0x0000016E System.Void ETCBase::set_anchor(ETCBase_RectAnchor)
extern void ETCBase_set_anchor_mAC12D428C2AD7202847E23C1874419F6D2DB8F37 ();
// 0x0000016F UnityEngine.Vector2 ETCBase::get_anchorOffet()
extern void ETCBase_get_anchorOffet_m45F590C803B5428363B2C991E4A5D6DD3F29EC23 ();
// 0x00000170 System.Void ETCBase::set_anchorOffet(UnityEngine.Vector2)
extern void ETCBase_set_anchorOffet_mFB2A1DD8C7A014BA8F4C75995DE57432D1DD0414 ();
// 0x00000171 System.Boolean ETCBase::get_visible()
extern void ETCBase_get_visible_m046CD11BFF4A77021DA78B68311DEE6EA0EE5AD3 ();
// 0x00000172 System.Void ETCBase::set_visible(System.Boolean)
extern void ETCBase_set_visible_m00B2525EFBB28BC9EC0BA0280AB4F989B64C88EC ();
// 0x00000173 System.Boolean ETCBase::get_activated()
extern void ETCBase_get_activated_mDC5DD764F14B57B3BE726B3F322B7D3F05CF3C5C ();
// 0x00000174 System.Void ETCBase::set_activated(System.Boolean)
extern void ETCBase_set_activated_m8D4CAE0E6EDDDBA21BB0521497A000C6D89F4945 ();
// 0x00000175 System.Void ETCBase::Awake()
extern void ETCBase_Awake_mCE1D1FF2791F9453A6FCAD89188A875A905AC7FE ();
// 0x00000176 System.Void ETCBase::Start()
extern void ETCBase_Start_m2B249AA08A8782BB725C038EA32E3080505A1BAD ();
// 0x00000177 System.Void ETCBase::OnEnable()
extern void ETCBase_OnEnable_m72AC9287FD616176EDAA65852FA20CA0EEC67F50 ();
// 0x00000178 System.Void ETCBase::OnDisable()
extern void ETCBase_OnDisable_m003F87BCA1B9AC396423E49B7F66A582EC2AD131 ();
// 0x00000179 System.Void ETCBase::OnDestroy()
extern void ETCBase_OnDestroy_mB8AB4D7B7F75A76F8A7BE3C4DAD08B7AB2D15AE5 ();
// 0x0000017A System.Void ETCBase::Update()
extern void ETCBase_Update_m71F89C1AE64F58D8F069C1BBF2E26221A0EFBF48 ();
// 0x0000017B System.Void ETCBase::FixedUpdate()
extern void ETCBase_FixedUpdate_mF87BEE97E4E5B698A2180F0581BD23B66A0492A1 ();
// 0x0000017C System.Void ETCBase::LateUpdate()
extern void ETCBase_LateUpdate_m010C5F0B0D7244A84AB0CD66C1E154D7BAAB6E1B ();
// 0x0000017D System.Void ETCBase::UpdateControlState()
extern void ETCBase_UpdateControlState_m2AACCB711D87DAF6AE02DE8C53BBC50C8456391F ();
// 0x0000017E System.Void ETCBase::SetVisible(System.Boolean)
extern void ETCBase_SetVisible_mD93DBB871AA408D8E755201768ED21D08CCF0582 ();
// 0x0000017F System.Void ETCBase::SetActivated()
extern void ETCBase_SetActivated_mDA97ED22B2F86E7B52D95830D16E74D297968D7A ();
// 0x00000180 System.Void ETCBase::SetAnchorPosition()
extern void ETCBase_SetAnchorPosition_m9A2C462A0F74849219E5F68DA9949A3634510939 ();
// 0x00000181 UnityEngine.GameObject ETCBase::GetFirstUIElement(UnityEngine.Vector2)
extern void ETCBase_GetFirstUIElement_m376A97048D47A0C5194ADB7001A370200BD28831 ();
// 0x00000182 System.Void ETCBase::CameraSmoothFollow()
extern void ETCBase_CameraSmoothFollow_m9C1431B401B4187E232F804760F5DF6D3C29CD33 ();
// 0x00000183 System.Void ETCBase::CameraFollow()
extern void ETCBase_CameraFollow_m392819CCD54211D83B191F849FF8669F1178D2C2 ();
// 0x00000184 System.Collections.IEnumerator ETCBase::UpdateVirtualControl()
extern void ETCBase_UpdateVirtualControl_m907E0DF0C53C330B03D59E3B6DE63DDB45DC8D98 ();
// 0x00000185 System.Collections.IEnumerator ETCBase::FixedUpdateVirtualControl()
extern void ETCBase_FixedUpdateVirtualControl_m60E3C0834D3EC40F0C7B83A11C0EF561EAB5493C ();
// 0x00000186 System.Void ETCBase::DoActionBeforeEndOfFrame()
extern void ETCBase_DoActionBeforeEndOfFrame_mE6A885346C696EC5E4E2D8BE47BA629B62DBA252 ();
// 0x00000187 System.Void ETCBase::.ctor()
extern void ETCBase__ctor_mF5E1256874ADE07E24860A3AE37994F3C9896998 ();
// 0x00000188 System.Void ETCButton::.ctor()
extern void ETCButton__ctor_mDBCBF361BA3766C531D73494236CC8D3225E606B ();
// 0x00000189 System.Void ETCButton::Awake()
extern void ETCButton_Awake_m807702E4D5A199DA8C053D281B83DDB09EF9E7AB ();
// 0x0000018A System.Void ETCButton::Start()
extern void ETCButton_Start_mDC30B8F1D2BE8074F9B6001AB0DE1A62DF4FDB3F ();
// 0x0000018B System.Void ETCButton::UpdateControlState()
extern void ETCButton_UpdateControlState_mA71E1F59EAD20D3A86A770017D62F2F1BB6F2D27 ();
// 0x0000018C System.Void ETCButton::DoActionBeforeEndOfFrame()
extern void ETCButton_DoActionBeforeEndOfFrame_m5D1DB6621F5E041C61A8D7E0EA460273DB1F013E ();
// 0x0000018D System.Void ETCButton::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void ETCButton_OnPointerEnter_m16B469510021FD83EB55541ABED20F94A09ABA58 ();
// 0x0000018E System.Void ETCButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ETCButton_OnPointerDown_mA71620E4158C4D6B0D0C339A85A84D63975D48EC ();
// 0x0000018F System.Void ETCButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ETCButton_OnPointerUp_m52E471EB0E1F532AFBCFF9334B122D0E1086C4D7 ();
// 0x00000190 System.Void ETCButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void ETCButton_OnPointerExit_m664902F20FEBDCC099D3C3081D1F7ECB7C550864 ();
// 0x00000191 System.Void ETCButton::UpdateButton()
extern void ETCButton_UpdateButton_mD63ED06B84CBEB19FE795CA549C285FA905FEAE3 ();
// 0x00000192 System.Void ETCButton::SetVisible(System.Boolean)
extern void ETCButton_SetVisible_mBD6F54158DE6FA5F99F5E6F0DC11FF89EA121E8F ();
// 0x00000193 System.Void ETCButton::ApllyState()
extern void ETCButton_ApllyState_mF3F1BEFB29227DB99C1EE9C80E76F079B874FF73 ();
// 0x00000194 System.Void ETCButton::SetActivated()
extern void ETCButton_SetActivated_m9B473F029A5CAA3754D59983882EF6E19F950734 ();
// 0x00000195 System.Void ETCDPad::.ctor()
extern void ETCDPad__ctor_m97BDDD28C2742574EC6ED78D5C931F5F736F4392 ();
// 0x00000196 System.Void ETCDPad::Start()
extern void ETCDPad_Start_m8A07615643ECFAE141BB529DB55BE35E08C6FEE3 ();
// 0x00000197 System.Void ETCDPad::UpdateControlState()
extern void ETCDPad_UpdateControlState_mA5B36B51EAF18060E8B29D8F19BA08F5452F5A09 ();
// 0x00000198 System.Void ETCDPad::DoActionBeforeEndOfFrame()
extern void ETCDPad_DoActionBeforeEndOfFrame_m47CF72131BF5D8179A2867CBA79A4A6CCE92858C ();
// 0x00000199 System.Void ETCDPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ETCDPad_OnPointerDown_mC4CFAE85F02ED3A3CF753260C40EAF32FA2C8E84 ();
// 0x0000019A System.Void ETCDPad::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ETCDPad_OnDrag_mD0C2CDC27E4FD0A47842FAB474F8E859E85B1A2A ();
// 0x0000019B System.Void ETCDPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ETCDPad_OnPointerUp_m32BDE0C555AD90C4E343E34716FC068B7A189A64 ();
// 0x0000019C System.Void ETCDPad::UpdateDPad()
extern void ETCDPad_UpdateDPad_mE8C0C87F0967C24558FC0300A2846FDE4F2B3863 ();
// 0x0000019D System.Void ETCDPad::SetVisible(System.Boolean)
extern void ETCDPad_SetVisible_mF862A5CE3E82BB669815AED0FAB76DD51F4785C1 ();
// 0x0000019E System.Void ETCDPad::SetActivated()
extern void ETCDPad_SetActivated_m7AD745D8C9215CE49888A55E96E02EE407E454B0 ();
// 0x0000019F System.Void ETCDPad::GetTouchDirection(UnityEngine.Vector2,UnityEngine.Camera)
extern void ETCDPad_GetTouchDirection_m6EF6C79B797101B8C95F67FEBAB3029ABE14D92C ();
// 0x000001A0 ETCInput ETCInput::get_instance()
extern void ETCInput_get_instance_m9CD838DA676B36D5D4EF832B07636948D6403B9D ();
// 0x000001A1 System.Void ETCInput::RegisterControl(ETCBase)
extern void ETCInput_RegisterControl_m5A312482B738B12625AC973666B756DA229C8FCE ();
// 0x000001A2 System.Void ETCInput::UnRegisterControl(ETCBase)
extern void ETCInput_UnRegisterControl_m5146D877CEFA00F2703C93EEBF2FB29D08F96F64 ();
// 0x000001A3 System.Void ETCInput::Create()
extern void ETCInput_Create_m2A4EA6298D8A95F2CA185032CB29FA50D8469F32 ();
// 0x000001A4 System.Void ETCInput::Register(ETCBase)
extern void ETCInput_Register_m5B70537D14FB364A3EF577FDFF37B151A0389BF9 ();
// 0x000001A5 System.Void ETCInput::UnRegister(ETCBase)
extern void ETCInput_UnRegister_m85AB76BD77FDFC41C9C96B4443F4EB119F11D831 ();
// 0x000001A6 System.Void ETCInput::SetControlVisible(System.String,System.Boolean)
extern void ETCInput_SetControlVisible_m04ECA8319B242A471F7E7A6657F9C70DF4344904 ();
// 0x000001A7 System.Boolean ETCInput::GetControlVisible(System.String)
extern void ETCInput_GetControlVisible_m1F6869013CFCD0444D92D5CBE9E5AE621B4019DF ();
// 0x000001A8 System.Void ETCInput::SetControlActivated(System.String,System.Boolean)
extern void ETCInput_SetControlActivated_mE08E99365F8AECC13FBCE3EB384B504F08D66E12 ();
// 0x000001A9 System.Boolean ETCInput::GetControlActivated(System.String)
extern void ETCInput_GetControlActivated_mF26D37234A7D9EBCC6EC99632B120DE65AE68AE3 ();
// 0x000001AA System.Void ETCInput::SetControlSwipeIn(System.String,System.Boolean)
extern void ETCInput_SetControlSwipeIn_m29223017D1C737CEA2C15A5A940B7CEB9AF4CD3D ();
// 0x000001AB System.Boolean ETCInput::GetControlSwipeIn(System.String)
extern void ETCInput_GetControlSwipeIn_m8163C29B98F8A06483CFB4E60C3C5D24E4C37072 ();
// 0x000001AC System.Void ETCInput::SetControlSwipeOut(System.String,System.Boolean)
extern void ETCInput_SetControlSwipeOut_mAF89FB4658C1AC1316426A180947F0315F7BC8CC ();
// 0x000001AD System.Boolean ETCInput::GetControlSwipeOut(System.String,System.Boolean)
extern void ETCInput_GetControlSwipeOut_m96E2165FF278FF43FFC15D013803C11C77011882 ();
// 0x000001AE System.Void ETCInput::SetDPadAxesCount(System.String,ETCBase_DPadAxis)
extern void ETCInput_SetDPadAxesCount_mFBB8C56E671E60AAE43CE19BC3A9AF711A8F7200 ();
// 0x000001AF ETCBase_DPadAxis ETCInput::GetDPadAxesCount(System.String)
extern void ETCInput_GetDPadAxesCount_mF5A3ACB2C943FB2641148F4BD6BCFFDE242C0241 ();
// 0x000001B0 ETCJoystick ETCInput::GetControlJoystick(System.String)
extern void ETCInput_GetControlJoystick_m6C99396CE1F3A2C71F9F73E8D58C396698532B1C ();
// 0x000001B1 ETCDPad ETCInput::GetControlDPad(System.String)
extern void ETCInput_GetControlDPad_m3512F8CE59B2B996AB0F075E78E78830337BA41D ();
// 0x000001B2 ETCTouchPad ETCInput::GetControlTouchPad(System.String)
extern void ETCInput_GetControlTouchPad_mA9CB8A16617247BD3E9BE0D1FED961BC74908FBD ();
// 0x000001B3 ETCButton ETCInput::GetControlButton(System.String)
extern void ETCInput_GetControlButton_m5DBBF10FB44304BF294096A2F652BE1EE2B05AF1 ();
// 0x000001B4 System.Void ETCInput::SetControlSprite(System.String,UnityEngine.Sprite,UnityEngine.Color)
extern void ETCInput_SetControlSprite_mFB0FD1BE5689E49BDE7CDC1874B094D7C880905B ();
// 0x000001B5 System.Void ETCInput::SetJoystickThumbSprite(System.String,UnityEngine.Sprite,UnityEngine.Color)
extern void ETCInput_SetJoystickThumbSprite_m8FBD3CA4680E92652185387742005CEF2C2BEBE2 ();
// 0x000001B6 System.Void ETCInput::SetButtonSprite(System.String,UnityEngine.Sprite,UnityEngine.Sprite,UnityEngine.Color)
extern void ETCInput_SetButtonSprite_m1E7E0CD5680F81B989FE9F057068DB1E097EEADE ();
// 0x000001B7 System.Void ETCInput::SetAxisSpeed(System.String,System.Single)
extern void ETCInput_SetAxisSpeed_m63501F05D8D4885D11742A14F1D07ED21C1F2FBF ();
// 0x000001B8 System.Void ETCInput::SetAxisGravity(System.String,System.Single)
extern void ETCInput_SetAxisGravity_m7C5938D18630E9AA1C6963088B47E600E8682F23 ();
// 0x000001B9 System.Void ETCInput::SetTurnMoveSpeed(System.String,System.Single)
extern void ETCInput_SetTurnMoveSpeed_m59EF3FD689B58D1F829BC23356DBE40A5748A69B ();
// 0x000001BA System.Void ETCInput::ResetAxis(System.String)
extern void ETCInput_ResetAxis_m9D287427DBB3A0D3695555790124B6DBA14E7684 ();
// 0x000001BB System.Void ETCInput::SetAxisEnabled(System.String,System.Boolean)
extern void ETCInput_SetAxisEnabled_mDE0FE8B3CE9AB70F46F22C04BA87D5C66EE73B96 ();
// 0x000001BC System.Boolean ETCInput::GetAxisEnabled(System.String)
extern void ETCInput_GetAxisEnabled_mA16336CC21EDCABEADB2154526751506C9263609 ();
// 0x000001BD System.Void ETCInput::SetAxisInverted(System.String,System.Boolean)
extern void ETCInput_SetAxisInverted_m16A56E626768ACBC225985E0D8D2D614266D6670 ();
// 0x000001BE System.Boolean ETCInput::GetAxisInverted(System.String)
extern void ETCInput_GetAxisInverted_mDE1DCC32119799008EB2A187FD6EB33536F9D875 ();
// 0x000001BF System.Void ETCInput::SetAxisDeadValue(System.String,System.Single)
extern void ETCInput_SetAxisDeadValue_m3BBD1F5390E9B9858ACFD84AD38EA02765634C83 ();
// 0x000001C0 System.Single ETCInput::GetAxisDeadValue(System.String)
extern void ETCInput_GetAxisDeadValue_m74B7B48696128E592B4C2BA85280F6C3A8F50EF0 ();
// 0x000001C1 System.Void ETCInput::SetAxisSensitivity(System.String,System.Single)
extern void ETCInput_SetAxisSensitivity_m952C10DEF9B92D0ACF0A485999BA5A3F619162B5 ();
// 0x000001C2 System.Single ETCInput::GetAxisSensitivity(System.String)
extern void ETCInput_GetAxisSensitivity_m0A0F96810A806F5FB2B6BB10249C94C31EED638F ();
// 0x000001C3 System.Void ETCInput::SetAxisThreshold(System.String,System.Single)
extern void ETCInput_SetAxisThreshold_m3AE1F48AF73A6FCC3E3006CE2FA7CA2473811EC0 ();
// 0x000001C4 System.Single ETCInput::GetAxisThreshold(System.String)
extern void ETCInput_GetAxisThreshold_mBDEF6F2E9EF3D1C26F4AD74B6F328BD0D1CF21E6 ();
// 0x000001C5 System.Void ETCInput::SetAxisInertia(System.String,System.Boolean)
extern void ETCInput_SetAxisInertia_m6E2BB2406F83ECD09A0EF725643CF5F930D90AB9 ();
// 0x000001C6 System.Boolean ETCInput::GetAxisInertia(System.String)
extern void ETCInput_GetAxisInertia_m43EA4C9D21F4834AD8C1D7889092D7C5AFEBCFC3 ();
// 0x000001C7 System.Void ETCInput::SetAxisInertiaSpeed(System.String,System.Single)
extern void ETCInput_SetAxisInertiaSpeed_m37761EBA43DDC2B437D40A51473D809EA6B3DFBF ();
// 0x000001C8 System.Single ETCInput::GetAxisInertiaSpeed(System.String)
extern void ETCInput_GetAxisInertiaSpeed_m9C41E015DDCD969DF91CDF46559A78BF4861DABF ();
// 0x000001C9 System.Void ETCInput::SetAxisInertiaThreshold(System.String,System.Single)
extern void ETCInput_SetAxisInertiaThreshold_mAB8B9C58A8D9CCD89CC2231993D4E3F4AA640A84 ();
// 0x000001CA System.Single ETCInput::GetAxisInertiaThreshold(System.String)
extern void ETCInput_GetAxisInertiaThreshold_m2D15A8B708B54F04D07AC458A85EA28667B98779 ();
// 0x000001CB System.Void ETCInput::SetAxisAutoStabilization(System.String,System.Boolean)
extern void ETCInput_SetAxisAutoStabilization_m0798915581DC1E2A20F6FCECF5A1286ACD8BBB85 ();
// 0x000001CC System.Boolean ETCInput::GetAxisAutoStabilization(System.String)
extern void ETCInput_GetAxisAutoStabilization_mC8CD3D0FAC37F07BC7397149394B4E5C5AD2CEAB ();
// 0x000001CD System.Void ETCInput::SetAxisAutoStabilizationSpeed(System.String,System.Single)
extern void ETCInput_SetAxisAutoStabilizationSpeed_m42CCBC09FEA25A3CBF7593DBF4D9F0FCBEEA263E ();
// 0x000001CE System.Single ETCInput::GetAxisAutoStabilizationSpeed(System.String)
extern void ETCInput_GetAxisAutoStabilizationSpeed_m8EFA0AA2ECB938DC1FD12BFEFF99D497C3005AFE ();
// 0x000001CF System.Void ETCInput::SetAxisAutoStabilizationThreshold(System.String,System.Single)
extern void ETCInput_SetAxisAutoStabilizationThreshold_m72E94D1E811AB0F70A4FFB0BDC338533EB2706BD ();
// 0x000001D0 System.Single ETCInput::GetAxisAutoStabilizationThreshold(System.String)
extern void ETCInput_GetAxisAutoStabilizationThreshold_mDBC9BC599618FB0E9A297478BB4A34EDA86EBF35 ();
// 0x000001D1 System.Void ETCInput::SetAxisClampRotation(System.String,System.Boolean)
extern void ETCInput_SetAxisClampRotation_mBCE056504DEFA7B03B10BF7E55B2B42E49DA4427 ();
// 0x000001D2 System.Boolean ETCInput::GetAxisClampRotation(System.String)
extern void ETCInput_GetAxisClampRotation_m3377294703CA91A7596B9371D7B616E974261D14 ();
// 0x000001D3 System.Void ETCInput::SetAxisClampRotationValue(System.String,System.Single,System.Single)
extern void ETCInput_SetAxisClampRotationValue_m362EE5C71D25A7EF4A13C5DB767666A0ECA099A3 ();
// 0x000001D4 System.Void ETCInput::SetAxisClampRotationMinValue(System.String,System.Single)
extern void ETCInput_SetAxisClampRotationMinValue_m0525F1D62D4329E5FFCF9637A49192323C272F77 ();
// 0x000001D5 System.Void ETCInput::SetAxisClampRotationMaxValue(System.String,System.Single)
extern void ETCInput_SetAxisClampRotationMaxValue_m63B4CAE7DFAABD121B974FF0C7AC8EF7A7424570 ();
// 0x000001D6 System.Single ETCInput::GetAxisClampRotationMinValue(System.String)
extern void ETCInput_GetAxisClampRotationMinValue_mEA94004611E6D5F022B059B1F4CEBC7B4F17D7B0 ();
// 0x000001D7 System.Single ETCInput::GetAxisClampRotationMaxValue(System.String)
extern void ETCInput_GetAxisClampRotationMaxValue_mEEFEF0A44A5FA003FB31149F2CED90BDC73E71B4 ();
// 0x000001D8 System.Void ETCInput::SetAxisDirecTransform(System.String,UnityEngine.Transform)
extern void ETCInput_SetAxisDirecTransform_mFF6445BE90A9B01D595E558C89D505728EDC5D0A ();
// 0x000001D9 UnityEngine.Transform ETCInput::GetAxisDirectTransform(System.String)
extern void ETCInput_GetAxisDirectTransform_m7277917738E219C869139A5E1A17290153F5BE70 ();
// 0x000001DA System.Void ETCInput::SetAxisDirectAction(System.String,ETCAxis_DirectAction)
extern void ETCInput_SetAxisDirectAction_m9525E9D86622A6DCB36BE17EDF409AB735A40E1A ();
// 0x000001DB ETCAxis_DirectAction ETCInput::GetAxisDirectAction(System.String)
extern void ETCInput_GetAxisDirectAction_m77A172EF33A9DF3C31C7076359479D78249DE9D4 ();
// 0x000001DC System.Void ETCInput::SetAxisAffectedAxis(System.String,ETCAxis_AxisInfluenced)
extern void ETCInput_SetAxisAffectedAxis_m4765B52F252C8C3EB877294E5EDF17DB5FB703AE ();
// 0x000001DD ETCAxis_AxisInfluenced ETCInput::GetAxisAffectedAxis(System.String)
extern void ETCInput_GetAxisAffectedAxis_m55EC2EF12AFBFBB501083EAB2913F2EE4F6C74CB ();
// 0x000001DE System.Void ETCInput::SetAxisOverTime(System.String,System.Boolean)
extern void ETCInput_SetAxisOverTime_mDCA45348296470D71D43DC64C11A484032E5C4FA ();
// 0x000001DF System.Boolean ETCInput::GetAxisOverTime(System.String)
extern void ETCInput_GetAxisOverTime_m23ECB76E72C76AAB7033CD6A925DED52DB59F9ED ();
// 0x000001E0 System.Void ETCInput::SetAxisOverTimeStep(System.String,System.Single)
extern void ETCInput_SetAxisOverTimeStep_mD82A2C2EA6B8F1D247527A7B19D5DC222C15354D ();
// 0x000001E1 System.Single ETCInput::GetAxisOverTimeStep(System.String)
extern void ETCInput_GetAxisOverTimeStep_mBE2196554DFA81502C40621890C9695AA493C6CF ();
// 0x000001E2 System.Void ETCInput::SetAxisOverTimeMaxValue(System.String,System.Single)
extern void ETCInput_SetAxisOverTimeMaxValue_m9721597310E47BA6FD8F67D1398B8B7CB295E650 ();
// 0x000001E3 System.Single ETCInput::GetAxisOverTimeMaxValue(System.String)
extern void ETCInput_GetAxisOverTimeMaxValue_m364858C44036139AD6FA89E5D8ED8B595C8B1319 ();
// 0x000001E4 System.Single ETCInput::GetAxis(System.String)
extern void ETCInput_GetAxis_mD1ABC6B4866AD2318D3BF3800F1E4A205D905ADC ();
// 0x000001E5 System.Single ETCInput::GetAxisSpeed(System.String)
extern void ETCInput_GetAxisSpeed_m46050DB636DAF7F2C08C1E7CACC8D04FB723251B ();
// 0x000001E6 System.Boolean ETCInput::GetAxisDownUp(System.String)
extern void ETCInput_GetAxisDownUp_mCACCA58C011AA1AE66525FE9E63E8DE7FA98E760 ();
// 0x000001E7 System.Boolean ETCInput::GetAxisDownDown(System.String)
extern void ETCInput_GetAxisDownDown_mA48179A5A7296C155D8A1591C061C6E0FA329626 ();
// 0x000001E8 System.Boolean ETCInput::GetAxisDownRight(System.String)
extern void ETCInput_GetAxisDownRight_m8A02D4A250D9C998003461D4916EC0168A21A821 ();
// 0x000001E9 System.Boolean ETCInput::GetAxisDownLeft(System.String)
extern void ETCInput_GetAxisDownLeft_m5B51CB9F33C048A3B76BEB9FAF3D1B7D0D3D840F ();
// 0x000001EA System.Boolean ETCInput::GetAxisPressedUp(System.String)
extern void ETCInput_GetAxisPressedUp_mB4C7810CE00C2060AEC708DE8FBD7F3C81E56430 ();
// 0x000001EB System.Boolean ETCInput::GetAxisPressedDown(System.String)
extern void ETCInput_GetAxisPressedDown_m8321645C4928A00278F489108888EB84C67F7394 ();
// 0x000001EC System.Boolean ETCInput::GetAxisPressedRight(System.String)
extern void ETCInput_GetAxisPressedRight_m7A5451CB3AF12D5BF36C5AF9D2DEE0B9918F64E2 ();
// 0x000001ED System.Boolean ETCInput::GetAxisPressedLeft(System.String)
extern void ETCInput_GetAxisPressedLeft_mEFD4326F4B09CB976DEB246C6798C0F80D70B7F0 ();
// 0x000001EE System.Boolean ETCInput::GetButtonDown(System.String)
extern void ETCInput_GetButtonDown_mB743B8B6226FD9243F1258749D868B37BB5EC711 ();
// 0x000001EF System.Boolean ETCInput::GetButton(System.String)
extern void ETCInput_GetButton_m3349D8FA6F91C5324B786798D7D0361C776DAE73 ();
// 0x000001F0 System.Boolean ETCInput::GetButtonUp(System.String)
extern void ETCInput_GetButtonUp_m9F75E915B9C474B9F594855FF400E29CB0E0ED89 ();
// 0x000001F1 System.Single ETCInput::GetButtonValue(System.String)
extern void ETCInput_GetButtonValue_mD0958F928622F132F946EC03B75DB88364055863 ();
// 0x000001F2 System.Void ETCInput::RegisterAxis(ETCAxis)
extern void ETCInput_RegisterAxis_m60EA1B99DA52D8EC63C46B0026C9EE72901E6EB0 ();
// 0x000001F3 System.Void ETCInput::UnRegisterAxis(ETCAxis)
extern void ETCInput_UnRegisterAxis_mE6E1E5B9B81B599ED62B78DF5540C303AA5C5288 ();
// 0x000001F4 System.Void ETCInput::.ctor()
extern void ETCInput__ctor_m46BDD6FFC063F4766C996CF78EE6F388BDFCCD52 ();
// 0x000001F5 System.Void ETCInput::.cctor()
extern void ETCInput__cctor_m14DE50B9CF55516919E133AC30BC2038B3D84B3E ();
// 0x000001F6 System.Boolean ETCJoystick::get_IsNoReturnThumb()
extern void ETCJoystick_get_IsNoReturnThumb_m84B853A7A18F39C2E9FBAF9BC15767540BD56C24 ();
// 0x000001F7 System.Void ETCJoystick::set_IsNoReturnThumb(System.Boolean)
extern void ETCJoystick_set_IsNoReturnThumb_m7F2BDF26BC8029A5F3CAC31A1A647BC65E6798DA ();
// 0x000001F8 System.Boolean ETCJoystick::get_IsNoOffsetThumb()
extern void ETCJoystick_get_IsNoOffsetThumb_m853FBAA23A336D03F40A59335CD126AE050166E1 ();
// 0x000001F9 System.Void ETCJoystick::set_IsNoOffsetThumb(System.Boolean)
extern void ETCJoystick_set_IsNoOffsetThumb_m5C479CEEA3597C5C7CFFAED750DB17F7CADF9492 ();
// 0x000001FA System.Void ETCJoystick::.ctor()
extern void ETCJoystick__ctor_m04BA229A817ACF68AA5200D9D8FB5C6CBCE12FE7 ();
// 0x000001FB System.Void ETCJoystick::Awake()
extern void ETCJoystick_Awake_m504ACE49B1B4247E4D3D74428C7D4F4FA15EE875 ();
// 0x000001FC System.Void ETCJoystick::Start()
extern void ETCJoystick_Start_m6F8C4F4F4458D5DCB09F491E90A0B51694F3AC75 ();
// 0x000001FD System.Void ETCJoystick::Update()
extern void ETCJoystick_Update_m882C84F88EE338A7C762D674F1612A0AB03926D4 ();
// 0x000001FE System.Void ETCJoystick::LateUpdate()
extern void ETCJoystick_LateUpdate_mD248E35D70897A64A7E1EC0B51FDF9E574075ED3 ();
// 0x000001FF System.Void ETCJoystick::InitCameraLookAt()
extern void ETCJoystick_InitCameraLookAt_m6970412ED7F2273B4FDAA0E233E3086F5B9720BF ();
// 0x00000200 System.Void ETCJoystick::UpdateControlState()
extern void ETCJoystick_UpdateControlState_mE7F9B2FC5C19AEFBF85C96C5722DC554ACBA2FFF ();
// 0x00000201 System.Void ETCJoystick::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void ETCJoystick_OnPointerEnter_m8F54FE6621158782B02BBBD3B57D4CB2B109A752 ();
// 0x00000202 System.Void ETCJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ETCJoystick_OnPointerDown_m6373FDB24BCAE0530E06355C026A6C45BE3251B5 ();
// 0x00000203 System.Void ETCJoystick::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void ETCJoystick_OnBeginDrag_m2F59226A2B693F1A80C736196D85F41687F16E0C ();
// 0x00000204 System.Void ETCJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ETCJoystick_OnDrag_m91351068301671B274DBF7D93465372420792BE9 ();
// 0x00000205 System.Void ETCJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ETCJoystick_OnPointerUp_mBF67451AEB565B394F72D79170CC17446CE30590 ();
// 0x00000206 System.Void ETCJoystick::OnUp(System.Boolean)
extern void ETCJoystick_OnUp_mE3400B8C1C7E81D1229E8A5F1625666A3565B68F ();
// 0x00000207 System.Void ETCJoystick::DoActionBeforeEndOfFrame()
extern void ETCJoystick_DoActionBeforeEndOfFrame_m028B30685DD59C8040713A206EABA4B9C98A825C ();
// 0x00000208 System.Void ETCJoystick::UpdateJoystick()
extern void ETCJoystick_UpdateJoystick_mA530F42E68897F7CE8FBA5A98E2432A0E58C6466 ();
// 0x00000209 System.Boolean ETCJoystick::isTouchOverJoystickArea(UnityEngine.Vector2&,UnityEngine.Vector2&)
extern void ETCJoystick_isTouchOverJoystickArea_mBACB572D3CDACDAB0221E331B0571D4523C073E7 ();
// 0x0000020A System.Boolean ETCJoystick::isScreenPointOverArea(UnityEngine.Vector2,UnityEngine.Vector2&)
extern void ETCJoystick_isScreenPointOverArea_m8ADDC7A67135E8C1F059FE39FDE31A6A527DC971 ();
// 0x0000020B System.Int32 ETCJoystick::GetTouchCount()
extern void ETCJoystick_GetTouchCount_mDDA817D693DBA91C66A960024415B8441EB98C91 ();
// 0x0000020C System.Single ETCJoystick::GetRadius()
extern void ETCJoystick_GetRadius_m32A263333C7EDC4F7D56492F61A109EC5339522C ();
// 0x0000020D System.Void ETCJoystick::SetActivated()
extern void ETCJoystick_SetActivated_m624984BFEA0EEFCBCEBA9BF6E99FC6228D400BE8 ();
// 0x0000020E System.Void ETCJoystick::SetVisible(System.Boolean)
extern void ETCJoystick_SetVisible_m7E09B2DEDD5F5A7318C208B42C999A2E3C1F65FA ();
// 0x0000020F System.Void ETCJoystick::DoTurnAndMove()
extern void ETCJoystick_DoTurnAndMove_mF8DA95ED4027E29812E16E80E9B536B1E082E799 ();
// 0x00000210 System.Void ETCJoystick::InitCurve()
extern void ETCJoystick_InitCurve_mBAC68F5B60055E282C50F4518E5245FBC0B720CC ();
// 0x00000211 System.Void ETCJoystick::InitTurnMoveCurve()
extern void ETCJoystick_InitTurnMoveCurve_m8FDDF3006B95932DD2EDD41BB2CA89139637DA13 ();
// 0x00000212 System.Void ETCTouchPad::.ctor()
extern void ETCTouchPad__ctor_mF1B52FA1E1EE26111A1FDD7A34957E5FFD0D3C44 ();
// 0x00000213 System.Void ETCTouchPad::Awake()
extern void ETCTouchPad_Awake_mC14785D63F7A1DBE25F260FAFC26BE95D500583F ();
// 0x00000214 System.Void ETCTouchPad::OnEnable()
extern void ETCTouchPad_OnEnable_m21D562361A1FAD07E141BCD6530404BFF20913B2 ();
// 0x00000215 System.Void ETCTouchPad::Start()
extern void ETCTouchPad_Start_m4D328BEDA29D50FF43148B0A2A73AA1A59514DDD ();
// 0x00000216 System.Void ETCTouchPad::UpdateControlState()
extern void ETCTouchPad_UpdateControlState_m59AD53E74B1A43FAA9DDE142B26AC18B98CAB66C ();
// 0x00000217 System.Void ETCTouchPad::DoActionBeforeEndOfFrame()
extern void ETCTouchPad_DoActionBeforeEndOfFrame_mE7C2E8F0CF0D2A53E995965F88E33C3C211F658D ();
// 0x00000218 System.Void ETCTouchPad::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void ETCTouchPad_OnPointerEnter_m832FC3D243110D1062F053A9EA4C444DFD6B4D89 ();
// 0x00000219 System.Void ETCTouchPad::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void ETCTouchPad_OnBeginDrag_mE2E5817B425D3C3CA05E349316614933DC75A4D6 ();
// 0x0000021A System.Void ETCTouchPad::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ETCTouchPad_OnDrag_m75C168E7D3915E6937582B88D737F75D9A4C3B17 ();
// 0x0000021B System.Void ETCTouchPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ETCTouchPad_OnPointerDown_m65A43C1580C460E876D6BF2A14F658826CC62E4D ();
// 0x0000021C System.Void ETCTouchPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ETCTouchPad_OnPointerUp_mD8AC432E1B1F3E51E48B4DB265D09BAF6161289D ();
// 0x0000021D System.Void ETCTouchPad::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void ETCTouchPad_OnPointerExit_mF7EC1FEB98948C8822F4C2264D2322D0CE6B2225 ();
// 0x0000021E System.Void ETCTouchPad::UpdateTouchPad()
extern void ETCTouchPad_UpdateTouchPad_m8D4DF59BA07000F3C50B2C37C8439A589E839B4E ();
// 0x0000021F System.Void ETCTouchPad::SetVisible(System.Boolean)
extern void ETCTouchPad_SetVisible_m7B36BF9D750C564AC5062A0D7DDA9CC665EA4B55 ();
// 0x00000220 System.Void ETCTouchPad::SetActivated()
extern void ETCTouchPad_SetActivated_mBB984EA6A0EE8E4FCF30A4FCDDDCD8CC94D65D34 ();
// 0x00000221 System.Void RotationScript::Awake()
extern void RotationScript_Awake_m4CABA81305AEF3AFD1FA121FA5A7767610C4B97E ();
// 0x00000222 System.Void RotationScript::Start()
extern void RotationScript_Start_mC6CD41E0258CD8FE249FB37C3AFA53A531F1CB53 ();
// 0x00000223 System.Void RotationScript::Update()
extern void RotationScript_Update_m1176401A78CD79210300333FBC316561D9D7E6E9 ();
// 0x00000224 System.Void RotationScript::UpdateConstantRotation()
extern void RotationScript_UpdateConstantRotation_mE94400C61A5087D5CD7D06E7E35197503696AABC ();
// 0x00000225 System.Void RotationScript::.ctor()
extern void RotationScript__ctor_m38376ACF6849CB119931A0DAE31FE22CAD48D0BD ();
// 0x00000226 System.Void CinemachineController::Awake()
extern void CinemachineController_Awake_m29EB2B6090A1C02A403861E8326CD15550BB94F3 ();
// 0x00000227 System.Void CinemachineController::Start()
extern void CinemachineController_Start_m08177BEED6AB5F76422297F6D241FEDC5306D86C ();
// 0x00000228 System.Void CinemachineController::Update()
extern void CinemachineController_Update_m6CD69FA671B31AE6454622A476271C856518FB60 ();
// 0x00000229 System.Void CinemachineController::UpdateFrameTimer()
extern void CinemachineController_UpdateFrameTimer_m9C2DB0B7176BB97FEAB93091D26EA53F2EEDB946 ();
// 0x0000022A System.Void CinemachineController::FixedUpdate()
extern void CinemachineController_FixedUpdate_m6586501D4F8DCBDCAC90DE196ECEAA4440C2B4CE ();
// 0x0000022B System.Void CinemachineController::UpdateCameraShake()
extern void CinemachineController_UpdateCameraShake_m72F1C3B5EE5B5B2D8C8057B1DAFBA4E5BE29E9C4 ();
// 0x0000022C System.Void CinemachineController::.ctor()
extern void CinemachineController__ctor_m2B6986946B1977B6FE7810557291323377A81626 ();
// 0x0000022D System.Void Collectible::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Collectible_OnTriggerEnter2D_m0E7AB7E7FCD8F36E89AEDFAA299588E0E6984AA3 ();
// 0x0000022E System.Void Collectible::.ctor()
extern void Collectible__ctor_mA82F242807011F3F131A51F9697889BF5B9C299B ();
// 0x0000022F System.Void CollectibleManager::Spawn(UnityEngine.Vector2)
extern void CollectibleManager_Spawn_mDCBA20E523FFD9D4C7DBC46CD4414AD5C50ADD71 ();
// 0x00000230 System.Void CollectibleManager::Awake()
extern void CollectibleManager_Awake_mE35FFD8380DFB797C6BF4DFD4EA4DCA5831B28B6 ();
// 0x00000231 System.Void CollectibleManager::Start()
extern void CollectibleManager_Start_mA60880CF54FEAEDB7104F4E9BB5C4302F17A492B ();
// 0x00000232 System.Void CollectibleManager::Update()
extern void CollectibleManager_Update_m38DBB1F41C6345FF9EDF5D8CD599C3707F75D655 ();
// 0x00000233 System.Void CollectibleManager::UpdateSpawn()
extern void CollectibleManager_UpdateSpawn_m79329A2C9906A7DD31CA1713366E255587FFB614 ();
// 0x00000234 System.Void CollectibleManager::.ctor()
extern void CollectibleManager__ctor_mAA385632EA37E0B39327A2607145DF2DFF36B7DE ();
// 0x00000235 System.Void DNACollider::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void DNACollider_OnTriggerEnter2D_m40161A31E989C7FFFCA7F4F6B5402258F31843C1 ();
// 0x00000236 System.Void DNACollider::OnTriggerStay2D(UnityEngine.Collider2D)
extern void DNACollider_OnTriggerStay2D_m3D10D593C3570BE9A80A4168F630C04083873ABF ();
// 0x00000237 System.Void DNACollider::OnTriggerExit2D(UnityEngine.Collider2D)
extern void DNACollider_OnTriggerExit2D_m0733C1A3358DDA0C78A2056960ACB93E4FD41695 ();
// 0x00000238 PlayerController DNACollider::GetPlayer(UnityEngine.Collider2D)
extern void DNACollider_GetPlayer_mC2ED7D9D623EDD2215F21698845B8CDC448CD45A ();
// 0x00000239 System.Void DNACollider::.ctor()
extern void DNACollider__ctor_m2CB1A118C437FB40C4551E25CDAC4546173B8BB3 ();
// 0x0000023A System.Void DNAStrandManager::Start()
extern void DNAStrandManager_Start_m536D70BC1EBD8015385051D43F3C40D178283248 ();
// 0x0000023B System.Void DNAStrandManager::StartDNAStrand()
extern void DNAStrandManager_StartDNAStrand_mA67AAD76D0ADFE679F7DAA455F8BEE423EA80FB1 ();
// 0x0000023C System.Void DNAStrandManager::ScoreIncrement()
extern void DNAStrandManager_ScoreIncrement_m04A36165A2EC86896C59BEB6881EB833380E8320 ();
// 0x0000023D System.Void DNAStrandManager::SpawnNext()
extern void DNAStrandManager_SpawnNext_m1D5B0B2C347720E17FD8007F6C4FDA9E1D98BA58 ();
// 0x0000023E System.Collections.Generic.List`1<UnityEngine.Collider2D> DNAStrandManager::GetTrackGuides(System.Int32)
extern void DNAStrandManager_GetTrackGuides_m082E5E49709E9C40E064785030766B280A143B51 ();
// 0x0000023F System.Void DNAStrandManager::ActivateWalls(System.Boolean)
extern void DNAStrandManager_ActivateWalls_m0A459CBD5625E78D21D8BA837B85948D1FD31984 ();
// 0x00000240 System.Void DNAStrandManager::Reset()
extern void DNAStrandManager_Reset_m8084C40FFAC60D17B93D2AA5FD8703093D491952 ();
// 0x00000241 System.Void DNAStrandManager::populateDictionary()
extern void DNAStrandManager_populateDictionary_m22F5860567BC8C4ECDBDE99CC205644ADA09C569 ();
// 0x00000242 System.Void DNAStrandManager::Awake()
extern void DNAStrandManager_Awake_mE64B80623352245DA2CE201F3AB4987B6DCFFDA9 ();
// 0x00000243 System.Void DNAStrandManager::.ctor()
extern void DNAStrandManager__ctor_m412EA62A47AAB1E9DFFA107BFEC80BB91DD5AC0B ();
// 0x00000244 System.Boolean FinishScreen::Visible()
extern void FinishScreen_Visible_m5AAC449F98E3FF787A72802727C8958A8025590A ();
// 0x00000245 System.Void FinishScreen::TryAgain()
extern void FinishScreen_TryAgain_m071DA818EAD58C808E7FA04B969B455651882042 ();
// 0x00000246 System.Void FinishScreen::Win()
extern void FinishScreen_Win_m93C353B68584F1327A39B45EC4054AE152609567 ();
// 0x00000247 System.Void FinishScreen::Lose()
extern void FinishScreen_Lose_m0C3AEB0595AB6E34D91AE679A6EFFB2808E1C9CB ();
// 0x00000248 System.Void FinishScreen::Continue()
extern void FinishScreen_Continue_m04769CB19C9FE2C0A3FDB8363291484A0BC6E72F ();
// 0x00000249 System.Void FinishScreen::Awake()
extern void FinishScreen_Awake_m827778628FA7734D79BAA288752BBD8807B1356C ();
// 0x0000024A System.Void FinishScreen::Start()
extern void FinishScreen_Start_mC4BD4981443BED530FBE51B38FD5340D4C2D3E6A ();
// 0x0000024B System.Void FinishScreen::StorePlayer1Score(System.Single)
extern void FinishScreen_StorePlayer1Score_m4EFB9CCF8F51EC581BF211D6999BE47EA841C9C5 ();
// 0x0000024C System.Void FinishScreen::StorePlayer2Score(System.Single)
extern void FinishScreen_StorePlayer2Score_m4DBA859485443D89234F68122B169E3825D2493E ();
// 0x0000024D System.Void FinishScreen::DisplayScores()
extern void FinishScreen_DisplayScores_m9063871C7739EBDD904F52C11FF005404D86DDEE ();
// 0x0000024E System.String FinishScreen::FormatScore(System.Single)
extern void FinishScreen_FormatScore_mB50AF1B6ED34DC45A2DB39588926197BA266470C ();
// 0x0000024F System.Void FinishScreen::.ctor()
extern void FinishScreen__ctor_m5652F46083815215658CE8C5D8CADCD9ECBB919B ();
// 0x00000250 System.Void HUD::SetIsMultiplayer(System.Boolean)
extern void HUD_SetIsMultiplayer_mF8F4A47C57883D44CE28CDD458E4EB52C83206B4 ();
// 0x00000251 System.Void HUD::setLives(System.Int32)
extern void HUD_setLives_m41EDC8B3B7EFAC6B839918D4CE94E6F98EDE83AF ();
// 0x00000252 System.Void HUD::SetEnabled(System.Int32,HUD_Player,System.Boolean)
extern void HUD_SetEnabled_mAA9C06B49868DEC0D0198857696F0135C402D187 ();
// 0x00000253 System.Void HUD::SetHealth(System.Int32,System.Int32)
extern void HUD_SetHealth_m9B311A06EF8ACE70901A44C9596C6BA2FD60665C ();
// 0x00000254 System.Void HUD::SetName(System.String,System.Int32)
extern void HUD_SetName_m897B1B5664EF2E48885B91653F8E172ED2479A94 ();
// 0x00000255 System.Void HUD::SetScore(System.Int32,System.Int32)
extern void HUD_SetScore_m31BD982AF4A3B2CAF9D89692F336F0C7D450E1E5 ();
// 0x00000256 System.Void HUD::SetBoost(System.Single,System.Int32)
extern void HUD_SetBoost_mE7E43957FD3F8A29F546B40F80D279D6DE463C35 ();
// 0x00000257 System.Void HUD::SetBlue(System.Single,System.Int32)
extern void HUD_SetBlue_mD147932CC2790A0ED7D5991D764AEF7CEE39CD28 ();
// 0x00000258 HUD_Player HUD::GetPlayer(System.Int32)
extern void HUD_GetPlayer_m455A9C7D6FAD83F454A068427C15347D8CD64B3D ();
// 0x00000259 System.Void HUD::Awake()
extern void HUD_Awake_mDF77764DC1D74224C4AF276B610B8F33E2E7A800 ();
// 0x0000025A System.Void HUD::Start()
extern void HUD_Start_m4BD3E094BF1DE8D743C70396024691DD0B0BB275 ();
// 0x0000025B System.Void HUD::Update()
extern void HUD_Update_mD5BD3824D8D13B56D9E833D398B957345025951F ();
// 0x0000025C System.Void HUD::ActivateNextLevelText(System.String)
extern void HUD_ActivateNextLevelText_mF16AAD3C3AB525DD095F37F83808F894EAF7FDDA ();
// 0x0000025D System.Void HUD::ActivateWipeoutText()
extern void HUD_ActivateWipeoutText_m62C15B074F8CEC5037F7D87E0D15D6126188DC74 ();
// 0x0000025E System.Void HUD::.ctor()
extern void HUD__ctor_m579520AFA3C6D8555D07E83C9EB3F140D8323CAE ();
// 0x0000025F System.String NameGenerator::Name()
extern void NameGenerator_Name_m1DB4502AA0866B1682414DF243F75D143440878B ();
// 0x00000260 System.Void NameGenerator::.ctor()
extern void NameGenerator__ctor_m01231912A2381310BAA7442A95066502823A8B8F ();
// 0x00000261 System.Void NameGenerator::.cctor()
extern void NameGenerator__cctor_m452E4B6CFC2FE555C029F911022FA1828DC25B43 ();
// 0x00000262 System.Void NewBehaviourScript::Start()
extern void NewBehaviourScript_Start_mDB573B0B04591BBF1CDC10C7C835851EBF8D17F2 ();
// 0x00000263 System.Void NewBehaviourScript::Update()
extern void NewBehaviourScript_Update_mAA0BE51F329DE556FA585E93DD1B9CF6D917A85C ();
// 0x00000264 System.Void NewBehaviourScript::.ctor()
extern void NewBehaviourScript__ctor_mC87DFFB91971C9C20A9487A744F5E68D74FB05FE ();
// 0x00000265 System.Int32 PlayerController::get_currentHealth()
extern void PlayerController_get_currentHealth_mA238FB34861E13044FF51984B124B6633AE1A17C ();
// 0x00000266 System.Void PlayerController::set_currentHealth(System.Int32)
extern void PlayerController_set_currentHealth_m12746B486DA77A161EAA05F3CAA2D18120762CFF ();
// 0x00000267 System.Int32 PlayerController::get_currentLives()
extern void PlayerController_get_currentLives_m6CA3C8E9DA3BC58036F06237C1F208496C5CE3F5 ();
// 0x00000268 System.Void PlayerController::set_currentLives(System.Int32)
extern void PlayerController_set_currentLives_mBAED406A54F6ABCAAC7724394AE1B58211569ED9 ();
// 0x00000269 System.Int32 PlayerController::get_playerNumber()
extern void PlayerController_get_playerNumber_m0A518CA5E2AB88B6183C2CBBBF8B86719706CD9F ();
// 0x0000026A System.Void PlayerController::set_playerNumber(System.Int32)
extern void PlayerController_set_playerNumber_mC39BB990846E9F677C2F7239C8247F6C9C56E464 ();
// 0x0000026B System.String PlayerController::get_playerName()
extern void PlayerController_get_playerName_mCD3F35E59BFFCBFB6011C24ED603A1A457EED4D5 ();
// 0x0000026C System.Void PlayerController::set_playerName(System.String)
extern void PlayerController_set_playerName_m5C9797B9533C8F4C40C188C7C8972EE1A257485E ();
// 0x0000026D System.Void PlayerController::Collect(Collectible)
extern void PlayerController_Collect_m1C88129453F8202810D59FA8AD10528D163855C0 ();
// 0x0000026E System.Void PlayerController::exitLevel()
extern void PlayerController_exitLevel_m1DC20642A604B23CE776238A888520CEA634D149 ();
// 0x0000026F System.Void PlayerController::SetHealth(System.Int32)
extern void PlayerController_SetHealth_mD18B26D0341F433B7D2F239614E62F355EF54C82 ();
// 0x00000270 System.Void PlayerController::ChangeHealth(System.Int32)
extern void PlayerController_ChangeHealth_m23D95E59ED18ACBB6BCD9CB4DA5CDCF9E5117868 ();
// 0x00000271 System.Void PlayerController::Awake()
extern void PlayerController_Awake_m118C1D205A374B627E1EC963E5837E23CF554573 ();
// 0x00000272 System.Void PlayerController::Start()
extern void PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF ();
// 0x00000273 System.Void PlayerController::Reset(System.Boolean)
extern void PlayerController_Reset_m902A8C9D413CFF880212AB09556DC0D5EA73D9E2 ();
// 0x00000274 System.Void PlayerController::Update()
extern void PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33 ();
// 0x00000275 System.Void PlayerController::setRotation(System.Single)
extern void PlayerController_setRotation_m83D5251B477A34971416C904BFA04C9F4D70AE37 ();
// 0x00000276 System.Void PlayerController::pressLeftButton()
extern void PlayerController_pressLeftButton_mB96AF0C4F7FA64577407F6613E58879AF83B78A8 ();
// 0x00000277 System.Void PlayerController::pressUseAllBoost()
extern void PlayerController_pressUseAllBoost_mEAC5B562BB6C2E5994F283662EB31A7552F4A9B8 ();
// 0x00000278 System.Void PlayerController::releaseLeftButton()
extern void PlayerController_releaseLeftButton_mC75C45BD0C5EF4AD1385B69DB860F4F8FC4B0066 ();
// 0x00000279 System.Void PlayerController::pressRightButton()
extern void PlayerController_pressRightButton_m978D262A8970A8F5E5984062CFA631E7A52D3350 ();
// 0x0000027A System.Void PlayerController::releaseRightButton()
extern void PlayerController_releaseRightButton_m166C4B33D5BEBCEA59082E34FA8EA41EA187C1F2 ();
// 0x0000027B System.Void PlayerController::UpdateInDistress()
extern void PlayerController_UpdateInDistress_m2FB1DBE2FEF0F1E858770BC23AFDBD81541644B3 ();
// 0x0000027C System.Void PlayerController::UpdateMirror()
extern void PlayerController_UpdateMirror_mBE5DD28C09E2D38A2DF182CC5D925EBFE6B95BF3 ();
// 0x0000027D System.Void PlayerController::UpdateBoost()
extern void PlayerController_UpdateBoost_m366F78E6D8D96B1C7754197D21BFE14F4D3E0433 ();
// 0x0000027E System.Void PlayerController::InDistress()
extern void PlayerController_InDistress_m6F33784BF1F546379B6DA11AE0B34DFE727AF3BA ();
// 0x0000027F System.Void PlayerController::EndDistress()
extern void PlayerController_EndDistress_m6DEEABCB09434A19B426739EE48A68A761D268C6 ();
// 0x00000280 System.Void PlayerController::SetAvailableBoost(System.Single)
extern void PlayerController_SetAvailableBoost_mEA40B368E919A15342F04C4747035BB2084B9A7E ();
// 0x00000281 System.Void PlayerController::SetAvailableBlue(System.Single)
extern void PlayerController_SetAvailableBlue_m434A4FBFC1CAA4F0506AE12D045D774C3E26855E ();
// 0x00000282 System.Void PlayerController::UpdateSailRotation()
extern void PlayerController_UpdateSailRotation_mE0796C61BAD4FE23ACB067B54616D17D24104CE6 ();
// 0x00000283 System.Void PlayerController::UpdateAccelleration()
extern void PlayerController_UpdateAccelleration_mFFE774CC3FA7EEADB02B9B0BD9E6952FE3B0DEFC ();
// 0x00000284 System.Void PlayerController::UpdateTryAgain()
extern void PlayerController_UpdateTryAgain_mA889B13776CCAFF8A027857A4364670A7D575A59 ();
// 0x00000285 System.Void PlayerController::UITryAgain()
extern void PlayerController_UITryAgain_m4D3020B2D2E91EDC8FFE842434C2E3961F58AC81 ();
// 0x00000286 System.Void PlayerController::LoadMenu()
extern void PlayerController_LoadMenu_mB41AADDB5C5AFE2E615CF41B5B903C143504425A ();
// 0x00000287 System.Void PlayerController::turnOnVaccine()
extern void PlayerController_turnOnVaccine_m776592BB3D73144737DB57460D3539E3C1BB4DE2 ();
// 0x00000288 System.Boolean PlayerController::getVaccine()
extern void PlayerController_getVaccine_mB4C46F5CE4881DA40F5D4C96E108686869CA7AA0 ();
// 0x00000289 System.Void PlayerController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlayerController_OnTriggerEnter2D_m4BFC3BB96B7F16C5F0E6A7BE6795149716478A74 ();
// 0x0000028A System.Void PlayerController::OnTriggerExit2D(UnityEngine.Collider2D)
extern void PlayerController_OnTriggerExit2D_m8E5A01B8FA14061A9E824E3B4AA6E702BC32A870 ();
// 0x0000028B System.Void PlayerController::SetDrag(System.String,System.Single)
extern void PlayerController_SetDrag_m56ED9520C66BE349A979B845F15BCA955EDA58CC ();
// 0x0000028C System.Void PlayerController::RestoreDrag(System.String)
extern void PlayerController_RestoreDrag_m5C7B2EE4DBA75736A7C154A176533E78E0B1220D ();
// 0x0000028D System.Boolean PlayerController::InputButton(System.String)
extern void PlayerController_InputButton_mA163344F554E819C5D4AFA8DE26C4609200D96E3 ();
// 0x0000028E System.Single PlayerController::InputAxisRaw(System.String)
extern void PlayerController_InputAxisRaw_m412FC50032AE0B387781F445E17D4B8FD9381A42 ();
// 0x0000028F System.Void PlayerController::.ctor()
extern void PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60 ();
// 0x00000290 System.Void PlayerController::.cctor()
extern void PlayerController__cctor_mC7F091C693834BDDD5C2F37F8A8AF92350083357 ();
// 0x00000291 System.Void MainMenuTurnSpeedSlider::Start()
extern void MainMenuTurnSpeedSlider_Start_m85074A3BE0B6BAF3B4D5E53007EEB4DA0367DD55 ();
// 0x00000292 System.Void MainMenuTurnSpeedSlider::UpdateTurnSpeed()
extern void MainMenuTurnSpeedSlider_UpdateTurnSpeed_mCB502E2E8ED9B243B223FDF0C2BA1A345ACDF4F6 ();
// 0x00000293 System.Void MainMenuTurnSpeedSlider::OnDisable()
extern void MainMenuTurnSpeedSlider_OnDisable_m1E3D3F70FBB5088FE86F51250F2BCE0553DE79F2 ();
// 0x00000294 System.Void MainMenuTurnSpeedSlider::.ctor()
extern void MainMenuTurnSpeedSlider__ctor_m08D32BEC6F07B4063AC04E707AC573706C4830EC ();
// 0x00000295 UnityEngine.Vector2 ScoreGateController::StartPosition(System.Int32,System.Boolean)
extern void ScoreGateController_StartPosition_m09C3F74DDF482C45F159699F63E3CDF9CB6F632E ();
// 0x00000296 System.Void ScoreGateController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ScoreGateController_OnTriggerEnter2D_m3E3F7616F2755E61FBCBE824F89A1468B2197939 ();
// 0x00000297 System.Void ScoreGateController::Awake()
extern void ScoreGateController_Awake_m4A2A6EC6E31227C8053104A9EE5879170B9814B1 ();
// 0x00000298 System.Void ScoreGateController::.ctor()
extern void ScoreGateController__ctor_m2810289E215BD50F41D8E4D8BF0F4571D06C289A ();
// 0x00000299 System.Void ScoreManager::Boost()
extern void ScoreManager_Boost_m98871E98B616CA1F9C2E25455F6E0537786FCE71 ();
// 0x0000029A System.Void ScoreManager::CollectCoin(System.Int32)
extern void ScoreManager_CollectCoin_mAB62D8EBAB72C53F6BD31C89E430CEDBA8E7C790 ();
// 0x0000029B System.Void ScoreManager::PassScoreGate(System.Int32)
extern void ScoreManager_PassScoreGate_mA754233986BB162B4BC12D660D69128269C6F644 ();
// 0x0000029C System.Void ScoreManager::Reset()
extern void ScoreManager_Reset_m23F7E7B0959CAF8614B9DFC1AC0CAF2015C42483 ();
// 0x0000029D System.Void ScoreManager::Awake()
extern void ScoreManager_Awake_mC9372E5D9A3EEB1FC42D08450E813A6E01B3AC8F ();
// 0x0000029E System.Void ScoreManager::Start()
extern void ScoreManager_Start_m1BE9724ADAC2945CE6A29AB672FBB5DBE5BE96B5 ();
// 0x0000029F System.Void ScoreManager::Update()
extern void ScoreManager_Update_m7C0C17E3C017FDFB4915824C59DEC5832A0A166F ();
// 0x000002A0 System.Void ScoreManager::UpdatePeriodicScore(System.Int32,System.Single,System.Int32,System.Single&)
extern void ScoreManager_UpdatePeriodicScore_m1EBF757319C58F1CF4FF8234FC24853011DDB13A ();
// 0x000002A1 System.Void ScoreManager::UpdateScoreText(System.Int32)
extern void ScoreManager_UpdateScoreText_m084C614C74288678E1D996CC280577D89B472EF8 ();
// 0x000002A2 System.Single ScoreManager::GetScore(System.Int32)
extern void ScoreManager_GetScore_mB5A1018BE3EF9F9E774E91CE8AB3E2A310ABE624 ();
// 0x000002A3 System.Void ScoreManager::resetCurrentLevel()
extern void ScoreManager_resetCurrentLevel_m9FBF4ACFFCB546330C5CB54D38A60FDBC0FB37FD ();
// 0x000002A4 System.Void ScoreManager::checkCompleteLevel(System.Int32)
extern void ScoreManager_checkCompleteLevel_mE2FEEA2D3FC6072D028C0C5FAA99354E96EBDA8F ();
// 0x000002A5 System.Void ScoreManager::.ctor()
extern void ScoreManager__ctor_mFA7EC3F474306DB06EAA63B19DEFA71F4E00B9E4 ();
// 0x000002A6 System.Void SerializableDictionary`2::OnBeforeSerialize()
// 0x000002A7 System.Void SerializableDictionary`2::OnAfterDeserialize()
// 0x000002A8 System.Void SerializableDictionary`2::.ctor()
// 0x000002A9 System.Void TargetFrameRate::Start()
extern void TargetFrameRate_Start_mB12212D0850E0C5C8153A58608E170DC2FD7B463 ();
// 0x000002AA System.Void TargetFrameRate::Update()
extern void TargetFrameRate_Update_m685D371061CD7405EE649C669BDF67E77ACCD70C ();
// 0x000002AB System.Void TargetFrameRate::.ctor()
extern void TargetFrameRate__ctor_m549E35628533F771593486A4A0A696F66A4ECFE6 ();
// 0x000002AC System.Void API_Demo::Update()
extern void API_Demo_Update_mA48F15C091DF17F2437C1896D4D4EE09D93F150E ();
// 0x000002AD System.Void API_Demo::OnGUI()
extern void API_Demo_OnGUI_mB78DC161C2DC70637D87AC05891E566A07F9FD69 ();
// 0x000002AE System.Void API_Demo::Sens(System.String)
extern void API_Demo_Sens_m3394080DFD1B35661AD2106641F657F6E798621C ();
// 0x000002AF System.Void API_Demo::Axes(System.String)
extern void API_Demo_Axes_m58AE74947AE5BC050A9A4EC8C8E38620C61B0657 ();
// 0x000002B0 System.Void API_Demo::SetGuiStyle(System.String)
extern void API_Demo_SetGuiStyle_m960A735C08E4F30F8B8BE09076B5E907F59763B4 ();
// 0x000002B1 System.Single API_Demo::customSlider(System.String,System.Single,System.Single,System.Single)
extern void API_Demo_customSlider_m77455CD2F153C037F6A6E8931B41AD1544D9ADB6 ();
// 0x000002B2 System.Void API_Demo::.ctor()
extern void API_Demo__ctor_mF3C2961812A81D482E0862748198AE8A7DFBCC82 ();
// 0x000002B3 System.Void mainmenu::Start()
extern void mainmenu_Start_mDC81297C63144BAFB8DD830BC92F36D98A5AFB51 ();
// 0x000002B4 System.Void mainmenu::Update()
extern void mainmenu_Update_mBFD24A0C2525F5F195934592A61148E37F2D6354 ();
// 0x000002B5 System.Void mainmenu::loadOnePlayerGameIntro1()
extern void mainmenu_loadOnePlayerGameIntro1_m65D0F4C6598A5F7153F2CE5318DB1FA507EDB60A ();
// 0x000002B6 System.Void mainmenu::loadOnePlayerGameIntro2()
extern void mainmenu_loadOnePlayerGameIntro2_m4F09CF443D3295D3F483D9377046A0EFD5E19F36 ();
// 0x000002B7 System.Void mainmenu::loadOnePlayerGame()
extern void mainmenu_loadOnePlayerGame_mEAA5970F5ECB291E05CADEA609FFFD38E7E01616 ();
// 0x000002B8 System.Void mainmenu::loadOnePlayerGameEasy()
extern void mainmenu_loadOnePlayerGameEasy_m117369FF13EF8FF89BD8FA57261CA49F14B4D033 ();
// 0x000002B9 System.Void mainmenu::loadOnePlayerGameHard()
extern void mainmenu_loadOnePlayerGameHard_mB790347924211A7EE3EA17FA6FCACCB4710879AC ();
// 0x000002BA System.Void mainmenu::loadTwoPlayerGameIntro1()
extern void mainmenu_loadTwoPlayerGameIntro1_mF1899FED779D2EB45DC09478286AB041A41398C5 ();
// 0x000002BB System.Void mainmenu::loadTwoPlayerGameIntro2()
extern void mainmenu_loadTwoPlayerGameIntro2_m5BA59B3C13EF7C8ED914FDC4CDB2747860BAC4F6 ();
// 0x000002BC System.Void mainmenu::loadTwoPlayerGame()
extern void mainmenu_loadTwoPlayerGame_mBE4368A48A1E92E6D170AB618DFA134AFD8C680B ();
// 0x000002BD System.Void mainmenu::.ctor()
extern void mainmenu__ctor_m49FD1EAC3055FBC9885EABB53E715B12EBD0936B ();
// 0x000002BE System.Void rotatorScript::Start()
extern void rotatorScript_Start_mF1D5803F10BA8630B167DBE734FE76EF412DF196 ();
// 0x000002BF System.Void rotatorScript::Update()
extern void rotatorScript_Update_mD54FE3F99E6E2AF94F9F0D7E255124AD0FC70505 ();
// 0x000002C0 System.Void rotatorScript::.ctor()
extern void rotatorScript__ctor_m92241A9C114B4535C466686F17D85A9E18D9E856 ();
// 0x000002C1 System.Void TouchControlsKit.ApplyMethodAttribute::.ctor()
extern void ApplyMethodAttribute__ctor_m296E6B09B992262C44F658D22169D62A1023EE7C ();
// 0x000002C2 System.Void TouchControlsKit.LabelAttribute::.ctor(System.String)
extern void LabelAttribute__ctor_mD9492F23B8F3EA05C8A1CEE399B4B4BBF3A95E28 ();
// 0x000002C3 System.Void TouchControlsKit.LogicLabelAttribute::.ctor(System.String,System.String,System.String)
extern void LogicLabelAttribute__ctor_m1B723C7AD45E2D8CF0133350055706C0793D3135 ();
// 0x000002C4 UnityEngine.Camera TouchControlsKit.GuiCamera::get_getCamera()
extern void GuiCamera_get_getCamera_mD1310EB3B41BAD6C3564D91469DE70229D834A26 ();
// 0x000002C5 System.Void TouchControlsKit.GuiCamera::set_getCamera(UnityEngine.Camera)
extern void GuiCamera_set_getCamera_m140E54BE2DF0723C0B0429736E50375A55768D33 ();
// 0x000002C6 UnityEngine.Transform TouchControlsKit.GuiCamera::get_getTransform()
extern void GuiCamera_get_getTransform_mC266F3006763DE9A88FD00036FAE6DEFF88A0D5E ();
// 0x000002C7 System.Void TouchControlsKit.GuiCamera::set_getTransform(UnityEngine.Transform)
extern void GuiCamera_set_getTransform_mA92D4DCD74CEC008CC9F6AB57FFE3225ED1004AF ();
// 0x000002C8 System.Void TouchControlsKit.GuiCamera::Awake()
extern void GuiCamera_Awake_m18A1476EB47D6B9815495648C6A3F8532190A38C ();
// 0x000002C9 UnityEngine.Vector2 TouchControlsKit.GuiCamera::ScreenToWorldPoint(UnityEngine.Vector2)
extern void GuiCamera_ScreenToWorldPoint_m2F019D2787CB5F7C11C6CD6BDCB5AD37A2C094BE ();
// 0x000002CA System.Void TouchControlsKit.GuiCamera::.ctor()
extern void GuiCamera__ctor_m1741142A36BECC6603E9BD910C255B0B1E530A8B ();
// 0x000002CB System.Single TouchControlsKit.Axis::get_value()
extern void Axis_get_value_m8A180723D48678894264701038F0BBC343640E47 ();
// 0x000002CC System.Void TouchControlsKit.Axis::set_value(System.Single)
extern void Axis_set_value_mA9C3192DE8A1A2C3FD706608FCB46A84D31C7CAF ();
// 0x000002CD System.Void TouchControlsKit.Axis::SetValue(System.Single)
extern void Axis_SetValue_m3F99072C1B9A593EC334B0B8AFCCEAC2647F009D ();
// 0x000002CE System.Void TouchControlsKit.Axis::.ctor()
extern void Axis__ctor_mE874128554829D17C871B1C9FBB775B223F3DAC6 ();
// 0x000002CF System.Boolean TouchControlsKit.AxesBasedController::get_ShowTouchZone()
extern void AxesBasedController_get_ShowTouchZone_mF6912DF0B3EAADCA9294654E8B02A38B7E4FA642 ();
// 0x000002D0 System.Void TouchControlsKit.AxesBasedController::set_ShowTouchZone(System.Boolean)
extern void AxesBasedController_set_ShowTouchZone_m84782525F3DF56A5369944505AC5B2543EF8A91A ();
// 0x000002D1 System.Void TouchControlsKit.AxesBasedController::OnApplyShowTouchZone()
extern void AxesBasedController_OnApplyShowTouchZone_mFE45672990F5DFC0BCC7D5E0E709ED3736EE2E1D ();
// 0x000002D2 System.Void TouchControlsKit.AxesBasedController::OnApplyActiveColors()
extern void AxesBasedController_OnApplyActiveColors_mF43C93B90202125B134048E9A726F0D19513451E ();
// 0x000002D3 System.Void TouchControlsKit.AxesBasedController::OnApplyVisible()
extern void AxesBasedController_OnApplyVisible_m75F854BEA8B3F7668ED5A4CB45A14BB2A9468C7B ();
// 0x000002D4 System.Void TouchControlsKit.AxesBasedController::ResetAxes()
extern void AxesBasedController_ResetAxes_m600249A15F091FB2ABD5DE3C3B5B815252C7E9E9 ();
// 0x000002D5 System.Void TouchControlsKit.AxesBasedController::SetAxes(UnityEngine.Vector2)
extern void AxesBasedController_SetAxes_m5F89BE7F50AFB265ED0B7B57F95553918BE86AAB ();
// 0x000002D6 System.Void TouchControlsKit.AxesBasedController::SetAxes(System.Single,System.Single)
extern void AxesBasedController_SetAxes_m36D06AD499732F417BF513AF37FD0B6217A08162 ();
// 0x000002D7 System.Collections.IEnumerator TouchControlsKit.AxesBasedController::SmoothAxisX(System.Single)
extern void AxesBasedController_SmoothAxisX_mCA175E2DA84EF66DB7E9987ACB64A039589E115D ();
// 0x000002D8 System.Collections.IEnumerator TouchControlsKit.AxesBasedController::SmoothAxisY(System.Single)
extern void AxesBasedController_SmoothAxisY_m4696B49D9184B972CE889D538D38CF12129B7C0E ();
// 0x000002D9 System.Void TouchControlsKit.AxesBasedController::ControlReset()
extern void AxesBasedController_ControlReset_m665C1235C98F939BA6C6E97BC07D4542A86F53A7 ();
// 0x000002DA System.Void TouchControlsKit.AxesBasedController::.ctor()
extern void AxesBasedController__ctor_m75882BEF560E81CE2B035326BBF2BD4D1B5FED4B ();
// 0x000002DB UnityEngine.Color TouchControlsKit.ControllerBase::GetActiveColor(UnityEngine.Color)
extern void ControllerBase_GetActiveColor_m2CB5BA88547CDAC0499A9895385EC383C370028B ();
// 0x000002DC System.Boolean TouchControlsKit.ControllerBase::get_isEnable()
extern void ControllerBase_get_isEnable_m56794FD2588BC750EE510C45791B39EAC6B7DBDF ();
// 0x000002DD System.Void TouchControlsKit.ControllerBase::set_isEnable(System.Boolean)
extern void ControllerBase_set_isEnable_mA52409A32CE8E73258115CDE1B1CB701A2DB6976 ();
// 0x000002DE System.Void TouchControlsKit.ControllerBase::OnApplyEnable()
extern void ControllerBase_OnApplyEnable_mA7B37761C439CBB21C7995099D465F64CED934D2 ();
// 0x000002DF System.Boolean TouchControlsKit.ControllerBase::get_isActive()
extern void ControllerBase_get_isActive_mD0A90584871B5973215E37D5F64AA6A55F8AB34A ();
// 0x000002E0 System.Void TouchControlsKit.ControllerBase::set_isActive(System.Boolean)
extern void ControllerBase_set_isActive_mF26CE3D98D6C5814E080B4DBCDD1ED6CBC294C12 ();
// 0x000002E1 System.Void TouchControlsKit.ControllerBase::OnApplyActive()
extern void ControllerBase_OnApplyActive_m8C6F1C13686045A65BD2FEC4C668F8397B0653BF ();
// 0x000002E2 System.Void TouchControlsKit.ControllerBase::OnApplyActiveColors()
extern void ControllerBase_OnApplyActiveColors_m038B03133484419441CAE25B4BAF9AB46D50A28E ();
// 0x000002E3 System.Boolean TouchControlsKit.ControllerBase::get_isVisible()
extern void ControllerBase_get_isVisible_m699AE27604C54E112C49F4AA90A5CEF45AD37603 ();
// 0x000002E4 System.Void TouchControlsKit.ControllerBase::set_isVisible(System.Boolean)
extern void ControllerBase_set_isVisible_m9B7C67302D5849B757D233658818F0CC54025012 ();
// 0x000002E5 System.Void TouchControlsKit.ControllerBase::OnApplyVisible()
extern void ControllerBase_OnApplyVisible_m4D19B6D3E6B8E6898FAD67F27E6EE9AA2D5DE478 ();
// 0x000002E6 System.Void TouchControlsKit.ControllerBase::OnAwake()
extern void ControllerBase_OnAwake_m87AECBBDA5C4EDB74EE5C5AD159FA5DCB5CC95F0 ();
// 0x000002E7 System.Void TouchControlsKit.ControllerBase::Update()
extern void ControllerBase_Update_mE98C96F4F96C299946D0DCD2FC71295B3445EC75 ();
// 0x000002E8 System.Void TouchControlsKit.ControllerBase::LateUpdate()
extern void ControllerBase_LateUpdate_m1814B073344C4E51FE4865342BC20CFAE6C4A108 ();
// 0x000002E9 System.Void TouchControlsKit.ControllerBase::FixedUpdate()
extern void ControllerBase_FixedUpdate_m595260AAB40C9651900D0BC54BCA45566B0D28DA ();
// 0x000002EA System.Void TouchControlsKit.ControllerBase::OnDisable()
extern void ControllerBase_OnDisable_m633C0E158F84CF63F5F503E612BB2A1224C52C0A ();
// 0x000002EB System.Void TouchControlsKit.ControllerBase::UpdateTouchPhase()
extern void ControllerBase_UpdateTouchPhase_mBE7297F1E53CCAF44DBF5E3E091545F785273E00 ();
// 0x000002EC System.Void TouchControlsKit.ControllerBase::UpdatePosition(UnityEngine.Vector2)
extern void ControllerBase_UpdatePosition_mE31505EF07B5196E431D3D1258510A35343F6F2A ();
// 0x000002ED System.Void TouchControlsKit.ControllerBase::ControlReset()
extern void ControllerBase_ControlReset_m8F1BFC5D9CEB4FE2C37A0F106591828776BCB940 ();
// 0x000002EE System.Void TouchControlsKit.ControllerBase::.ctor()
extern void ControllerBase__ctor_m0B6BDD4E5E0A33736B860D03BA42A53D9B2875F4 ();
// 0x000002EF System.Boolean TouchControlsKit.TCKButton::get_isPRESSED()
extern void TCKButton_get_isPRESSED_mA4DFE14807C4B9BE7A9A4F25C646694E9F23E679 ();
// 0x000002F0 System.Boolean TouchControlsKit.TCKButton::get_isDOWN()
extern void TCKButton_get_isDOWN_m21A6B739592B2435994E4EB044AA73E3764FBF66 ();
// 0x000002F1 System.Boolean TouchControlsKit.TCKButton::get_isUP()
extern void TCKButton_get_isUP_m030F8B6F0133E4810B1618BD727B8CB8B02A9066 ();
// 0x000002F2 System.Boolean TouchControlsKit.TCKButton::get_isCLICK()
extern void TCKButton_get_isCLICK_m1C74D4394D82F7D66EABDCF71A336BB0A4DA1A24 ();
// 0x000002F3 System.Void TouchControlsKit.TCKButton::UpdatePosition(UnityEngine.Vector2)
extern void TCKButton_UpdatePosition_m27C44C72A75DB8383D88CBA43107DAAE8B9C11DC ();
// 0x000002F4 System.Void TouchControlsKit.TCKButton::ButtonDown()
extern void TCKButton_ButtonDown_m570FC008BCF8D2399B22E0FECE272FE61B94A37E ();
// 0x000002F5 System.Void TouchControlsKit.TCKButton::ButtonUp()
extern void TCKButton_ButtonUp_mC2FF788E70391697A3B5786119C61256BE09024A ();
// 0x000002F6 System.Void TouchControlsKit.TCKButton::ControlReset()
extern void TCKButton_ControlReset_m87ACE3AAB4B3C226CB35DD298B6DE70BE2345F13 ();
// 0x000002F7 System.Void TouchControlsKit.TCKButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void TCKButton_OnPointerDown_m65B3ADA6DB1AA89E941F4861EF30613024E1862D ();
// 0x000002F8 System.Void TouchControlsKit.TCKButton::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void TCKButton_OnDrag_m8C23768914FD492008AE0AA4C36CCA1781725AFF ();
// 0x000002F9 System.Void TouchControlsKit.TCKButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TCKButton_OnPointerExit_m7BDEFF040A89BF7FE6731DE0A6D86C9DEE3CBBDF ();
// 0x000002FA System.Void TouchControlsKit.TCKButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TCKButton_OnPointerUp_mF1E05D892C07763F697F88ED2AC153901C7477BD ();
// 0x000002FB System.Void TouchControlsKit.TCKButton::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TCKButton_OnPointerClick_mAF0418C5F2B97C569EDF9BB81F6ABF21820B6A3A ();
// 0x000002FC System.Void TouchControlsKit.TCKButton::.ctor()
extern void TCKButton__ctor_mFFE37E008FC815B74E901998681ABD89F78A61A1 ();
// 0x000002FD System.Boolean TouchControlsKit.TCKJoystick::get_IsStatic()
extern void TCKJoystick_get_IsStatic_m5B3625BD29E3FC9A178D06D24CF798A27B1F5CD1 ();
// 0x000002FE System.Void TouchControlsKit.TCKJoystick::set_IsStatic(System.Boolean)
extern void TCKJoystick_set_IsStatic_mC73AACE1ECE0C68D13E58CD5CDFDCBC6A7387ADE ();
// 0x000002FF System.Void TouchControlsKit.TCKJoystick::OnAwake()
extern void TCKJoystick_OnAwake_m7DE43BC6180AB47CF910E336854ADD29E960084E ();
// 0x00000300 System.Void TouchControlsKit.TCKJoystick::OnApplyEnable()
extern void TCKJoystick_OnApplyEnable_mDC783B43E065AF3E1D757B90BC953A2B1AE43E4E ();
// 0x00000301 System.Void TouchControlsKit.TCKJoystick::OnApplyActiveColors()
extern void TCKJoystick_OnApplyActiveColors_m9DF053D0512882C707811A5CCBE79AA8F5D6F10F ();
// 0x00000302 System.Void TouchControlsKit.TCKJoystick::OnApplyVisible()
extern void TCKJoystick_OnApplyVisible_mF404470D980B6BE05CFCDE8D935851EA4269F664 ();
// 0x00000303 System.Void TouchControlsKit.TCKJoystick::UpdatePosition(UnityEngine.Vector2)
extern void TCKJoystick_UpdatePosition_mD675788CEA9548B749E0ECFA5625BEAD47BC5E1B ();
// 0x00000304 System.Void TouchControlsKit.TCKJoystick::UpdateCurrentPosition(UnityEngine.Vector2)
extern void TCKJoystick_UpdateCurrentPosition_m8D6125168FC3E2C0CDF929EA803A095EF2AAEEFA ();
// 0x00000305 System.Void TouchControlsKit.TCKJoystick::UpdateJoystickPosition()
extern void TCKJoystick_UpdateJoystickPosition_m7D5690C2864EE3F0F9EB69C7283343AC6BCF4883 ();
// 0x00000306 System.Void TouchControlsKit.TCKJoystick::UpdateTransparencyAndPosition(UnityEngine.Vector2)
extern void TCKJoystick_UpdateTransparencyAndPosition_m64AFC17AED6D297ABC0A3497C39206BD5F093DBD ();
// 0x00000307 System.Collections.IEnumerator TouchControlsKit.TCKJoystick::SmoothReturnRun()
extern void TCKJoystick_SmoothReturnRun_m263550D1BDB70F0BFC8B5B90895B48FE49216FE3 ();
// 0x00000308 System.Void TouchControlsKit.TCKJoystick::ControlReset()
extern void TCKJoystick_ControlReset_m07DADE3A97D5C42F18114D337F5EDCAA7CA50EB1 ();
// 0x00000309 System.Void TouchControlsKit.TCKJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void TCKJoystick_OnPointerDown_mD65A2BB5AC11011C3932B63A3057D7E744BEBF3B ();
// 0x0000030A System.Void TouchControlsKit.TCKJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void TCKJoystick_OnDrag_m36F50B1D84852A80E6DA8E6E1B57230A9371AE2B ();
// 0x0000030B System.Void TouchControlsKit.TCKJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TCKJoystick_OnPointerUp_mC80EA08686365ABA1B809D9CA68E3B3EB33B19C2 ();
// 0x0000030C System.Void TouchControlsKit.TCKJoystick::.ctor()
extern void TCKJoystick__ctor_m38C752E89491800B77D12AC6D715B62A29FAD0F4 ();
// 0x0000030D System.Void TouchControlsKit.TCKTouchpad::OnApplyVisible()
extern void TCKTouchpad_OnApplyVisible_m68D102D28CC07E3F760B8E8D7EF162C5B5F4EE91 ();
// 0x0000030E System.Void TouchControlsKit.TCKTouchpad::UpdatePosition(UnityEngine.Vector2)
extern void TCKTouchpad_UpdatePosition_m72C696F70529BA924120454A549D35C4ECB697E0 ();
// 0x0000030F System.Void TouchControlsKit.TCKTouchpad::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TCKTouchpad_OnPointerEnter_m79FDA1AEBFEE77BF2923D962B8D2E9B2DCFC6E3D ();
// 0x00000310 System.Void TouchControlsKit.TCKTouchpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void TCKTouchpad_OnPointerDown_mC5C4A0E20F4F91F3F5AC254CF68501E58A4B73AC ();
// 0x00000311 System.Void TouchControlsKit.TCKTouchpad::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void TCKTouchpad_OnDrag_m131BD1ED57EF4214598C04B630EEE18E33E05DC7 ();
// 0x00000312 System.Collections.IEnumerator TouchControlsKit.TCKTouchpad::UpdateEndPosition(UnityEngine.Vector2)
extern void TCKTouchpad_UpdateEndPosition_m4699CED0A47160E2F2CF20A3BF638526CD8A4A28 ();
// 0x00000313 System.Void TouchControlsKit.TCKTouchpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TCKTouchpad_OnPointerUp_mD7BD2FAAF8075E36CE7688B6B5D8EF204B554B05 ();
// 0x00000314 System.Void TouchControlsKit.TCKTouchpad::.ctor()
extern void TCKTouchpad__ctor_m01C8D7C078753D740131521A19A1443177C698B1 ();
// 0x00000315 TouchControlsKit.TCKInput TouchControlsKit.TCKInput::get_instance()
extern void TCKInput_get_instance_m030CC4E39694281D15E16BFD91AC3E39F8F34EE7 ();
// 0x00000316 System.Boolean TouchControlsKit.TCKInput::get_isActive()
extern void TCKInput_get_isActive_mB0854A8A1424EC25A137877789B7A84984A10DB8 ();
// 0x00000317 TouchControlsKit.ControllerBase[] TouchControlsKit.TCKInput::get_allControllers()
extern void TCKInput_get_allControllers_mB8B54A73D6237F0BD4317BD68A712DE350CE29BB ();
// 0x00000318 TouchControlsKit.AxesBasedController[] TouchControlsKit.TCKInput::get_abControllers()
extern void TCKInput_get_abControllers_m968AF442F344D721A36024F5EC9944F7003700F2 ();
// 0x00000319 TouchControlsKit.TCKButton[] TouchControlsKit.TCKInput::get_buttons()
extern void TCKInput_get_buttons_mD997ED98A213FD3F5B2D71E2B086C2C5C1C2C1A0 ();
// 0x0000031A System.Void TouchControlsKit.TCKInput::CheckUIEventSystem()
extern void TCKInput_CheckUIEventSystem_m49FCEB202FD99332483F1E8FF5F033814B6330E8 ();
// 0x0000031B System.Void TouchControlsKit.TCKInput::Awake()
extern void TCKInput_Awake_m03D7FCFC1DE7E3B863C707F7887C395465CE363A ();
// 0x0000031C System.Void TouchControlsKit.TCKInput::OnDisable()
extern void TCKInput_OnDisable_m9B4921019B6E1C30F1927DD3DBAE82C0C7ED0BE3 ();
// 0x0000031D System.Void TouchControlsKit.TCKInput::OnDestroy()
extern void TCKInput_OnDestroy_m5C364EBC121976FE23800995469455D9069D7CAD ();
// 0x0000031E System.Void TouchControlsKit.TCKInput::OnReset()
extern void TCKInput_OnReset_m9AC14D3ADC913F55F66B2D18567B6294E0189F90 ();
// 0x0000031F System.Void TouchControlsKit.TCKInput::InitControllers()
extern void TCKInput_InitControllers_mFAD19EB00743F5CA7B797D5381BF36E9F0116B56 ();
// 0x00000320 T[] TouchControlsKit.TCKInput::GetControllers()
// 0x00000321 System.Void TouchControlsKit.TCKInput::SetActive(System.Boolean)
extern void TCKInput_SetActive_m046D9BEB11E5831368E45EABB49626803DE7EDC7 ();
// 0x00000322 System.Boolean TouchControlsKit.TCKInput::CheckController(System.String)
extern void TCKInput_CheckController_mDEBFF59D19166F6C83A88274DACFAFFD06E2D79B ();
// 0x00000323 System.Boolean TouchControlsKit.TCKInput::GetControllerEnable(System.String)
extern void TCKInput_GetControllerEnable_m0B445D902AD39DFCCED919F3B631A2DE4AEB288A ();
// 0x00000324 System.Void TouchControlsKit.TCKInput::SetControllerEnable(System.String,System.Boolean)
extern void TCKInput_SetControllerEnable_mE4E30714BDEB2AF5660CA789F2055EC08C387293 ();
// 0x00000325 System.Boolean TouchControlsKit.TCKInput::GetControllerActive(System.String)
extern void TCKInput_GetControllerActive_m25A474DDE8495E39CE7AD89345628C1EC9DABD0A ();
// 0x00000326 System.Void TouchControlsKit.TCKInput::SetControllerActive(System.String,System.Boolean)
extern void TCKInput_SetControllerActive_mF7FAE357420251729189095A29A499ECE8E291C3 ();
// 0x00000327 System.Boolean TouchControlsKit.TCKInput::GetControllerVisible(System.String)
extern void TCKInput_GetControllerVisible_mF6F1782CA79EEC05185064117C62261EF8DE444C ();
// 0x00000328 System.Void TouchControlsKit.TCKInput::SetControllerVisible(System.String,System.Boolean)
extern void TCKInput_SetControllerVisible_m203486B41CD0D023A542E44E05C8A3FC6FB527F0 ();
// 0x00000329 System.Single TouchControlsKit.TCKInput::GetAxis(System.String,TouchControlsKit.EAxisType)
extern void TCKInput_GetAxis_m7DA7A595A620007C5E853EDF56E91B90548339A5 ();
// 0x0000032A UnityEngine.Vector2 TouchControlsKit.TCKInput::GetAxis(System.String)
extern void TCKInput_GetAxis_m373AE58DE90BC946C5CB7F6DFF282F6B1CA51E1D ();
// 0x0000032B System.Boolean TouchControlsKit.TCKInput::GetAxisEnable(System.String,TouchControlsKit.EAxisType)
extern void TCKInput_GetAxisEnable_mD76609AB0E109C5FFD43B99BE200A9FF3D0AED40 ();
// 0x0000032C System.Void TouchControlsKit.TCKInput::SetAxisEnable(System.String,TouchControlsKit.EAxisType,System.Boolean)
extern void TCKInput_SetAxisEnable_mBBEF1E459DEDFCAD886E75AFC5FBF72E17EBCF26 ();
// 0x0000032D System.Boolean TouchControlsKit.TCKInput::GetAxisInverse(System.String,TouchControlsKit.EAxisType)
extern void TCKInput_GetAxisInverse_m0ECF046E09BF703808619FB8D343D3154FEBC3C1 ();
// 0x0000032E System.Void TouchControlsKit.TCKInput::SetAxisInverse(System.String,TouchControlsKit.EAxisType,System.Boolean)
extern void TCKInput_SetAxisInverse_mE51F4371C1EBD3E0F25C0608548F7553522461B4 ();
// 0x0000032F System.Single TouchControlsKit.TCKInput::GetSensitivity(System.String)
extern void TCKInput_GetSensitivity_mAA20F74A07855CF1A9C89D1F045F84ADAC0E4B35 ();
// 0x00000330 System.Void TouchControlsKit.TCKInput::SetSensitivity(System.String,System.Single)
extern void TCKInput_SetSensitivity_m8B5F806113CD0D0E912EE3B64458F03F01DDA141 ();
// 0x00000331 System.Void TouchControlsKit.TCKInput::ShowingTouchZone(System.Boolean)
extern void TCKInput_ShowingTouchZone_mB13BE0DDF92EA9D293E3D91ADA74546DEC9B8E69 ();
// 0x00000332 System.Boolean TouchControlsKit.TCKInput::GetAction(System.String,TouchControlsKit.EActionEvent)
extern void TCKInput_GetAction_mA1F43D192959984AD33026D098E23C48426847FD ();
// 0x00000333 System.Boolean TouchControlsKit.TCKInput::GetButtonDown(System.String)
extern void TCKInput_GetButtonDown_m5C88CFC3680E74B374CD427CF926E362A95706CE ();
// 0x00000334 System.Boolean TouchControlsKit.TCKInput::GetButton(System.String)
extern void TCKInput_GetButton_mFE6B58D6D9CEC14C05F2A5412909BE9191E40619 ();
// 0x00000335 System.Boolean TouchControlsKit.TCKInput::GetButtonUp(System.String)
extern void TCKInput_GetButtonUp_m1FDDED653FBFC4E5E68AB602E33435A2A46A9AF7 ();
// 0x00000336 System.Boolean TouchControlsKit.TCKInput::GetButtonClick(System.String)
extern void TCKInput_GetButtonClick_m54F0EBF08356BFC68A3C9BFB672FE2F17D18E156 ();
// 0x00000337 TouchControlsKit.EUpdateMode TouchControlsKit.TCKInput::GetEUpdateType(System.String)
extern void TCKInput_GetEUpdateType_m1BB8FE16AF72ABE36A406E4D8B3AB9A0476AFF5D ();
// 0x00000338 System.Void TouchControlsKit.TCKInput::SetEUpdateType(System.String,TouchControlsKit.EUpdateMode)
extern void TCKInput_SetEUpdateType_m7011876453070543E6FE8C059CD138B825B3CFB3 ();
// 0x00000339 TouchControlsKit.ETouchPhase TouchControlsKit.TCKInput::GetTouchPhase(System.String)
extern void TCKInput_GetTouchPhase_m4F83327BD54711D7E28E592BAA1662B44A33709A ();
// 0x0000033A System.Void TouchControlsKit.TCKInput::.ctor()
extern void TCKInput__ctor_m154DE606DD19AA21C9B3203950456F85478CE8FC ();
// 0x0000033B System.Void Examples.FirstPersonExample::Awake()
extern void FirstPersonExample_Awake_m6C15FF8324901B0B702EAC836B31FD87DFBB7D58 ();
// 0x0000033C System.Void Examples.FirstPersonExample::Update()
extern void FirstPersonExample_Update_m1B331750FAC2CFAEE38A0D70A57E570614B2BD11 ();
// 0x0000033D System.Void Examples.FirstPersonExample::FixedUpdate()
extern void FirstPersonExample_FixedUpdate_m7128A17A4DD2E89D27F07F13D32EA3276782D489 ();
// 0x0000033E System.Void Examples.FirstPersonExample::Jumping()
extern void FirstPersonExample_Jumping_mE106FC87A791E25766AA160E908D2225EC39E20E ();
// 0x0000033F System.Void Examples.FirstPersonExample::PlayerMovement(System.Single,System.Single)
extern void FirstPersonExample_PlayerMovement_m95729B8554AB8EF472C4F8BA514FA53DF64BB103 ();
// 0x00000340 System.Void Examples.FirstPersonExample::PlayerRotation(System.Single,System.Single)
extern void FirstPersonExample_PlayerRotation_mDDD7E1E02FED0B7167CFDBA2D916D712083DFCB0 ();
// 0x00000341 System.Void Examples.FirstPersonExample::PlayerFiring()
extern void FirstPersonExample_PlayerFiring_mDC36CEA670704AC16ABC765A4A1C20ACD8CA31C5 ();
// 0x00000342 System.Void Examples.FirstPersonExample::PlayerClicked()
extern void FirstPersonExample_PlayerClicked_m1455834C7F8577A19A72968DA0141EAB4D898C64 ();
// 0x00000343 System.Void Examples.FirstPersonExample::.ctor()
extern void FirstPersonExample__ctor_m914B8FBD2E47B35F74AEF776D7C468C4DFAD21E0 ();
// 0x00000344 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::Start()
extern void EasyTouchTrigger_Start_m711DFFD49A39C76354E4AF884837832A1FAC3BCB ();
// 0x00000345 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::OnEnable()
extern void EasyTouchTrigger_OnEnable_m61552C4FDC8D6A8384BF2B56D8141256D78E256C ();
// 0x00000346 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::OnDisable()
extern void EasyTouchTrigger_OnDisable_mBEAE9160ECC230A6A6D10A114E2FEFA429F62164 ();
// 0x00000347 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::OnDestroy()
extern void EasyTouchTrigger_OnDestroy_mCEE4817A00E1F63EE8CD74C3057BD5D64DF6C313 ();
// 0x00000348 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::SubscribeEasyTouchEvent()
extern void EasyTouchTrigger_SubscribeEasyTouchEvent_mF19355C4A4AE28D2B2930CA92AB7102BE6CC36E5 ();
// 0x00000349 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::UnsubscribeEasyTouchEvent()
extern void EasyTouchTrigger_UnsubscribeEasyTouchEvent_m00F8875F7E098744A212C95E03935A8E3CE4FB84 ();
// 0x0000034A System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_TouchStart_mBE2F314F30DD09D28331EFACD56014ECA46171DF ();
// 0x0000034B System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_TouchDown_mFA13DC8CFE69F4EFAFDD8B8644A392E3A6A790AE ();
// 0x0000034C System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_TouchUp_mF120AE26739BAF7E77BD7792A5E258CAC4AD322A ();
// 0x0000034D System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_SimpleTap(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_SimpleTap_mA709D5B7D1FF7B076F0B9CAE9148460F16CC26C7 ();
// 0x0000034E System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_DoubleTap(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_DoubleTap_m8020924B2DEA68CE37CA4C4EF3C6A44C9155E1C3 ();
// 0x0000034F System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_LongTapStart(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_LongTapStart_mC3B18CE1C5FDB5CA27D67D256CBAD63A585BB4ED ();
// 0x00000350 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_LongTap(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_LongTap_m1FFCEC9E68BF2DB70077EE6B7A774295EFE1B38C ();
// 0x00000351 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_LongTapEnd(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_LongTapEnd_mC07993994E30CAB63359AAF898FA6CCD985637AA ();
// 0x00000352 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_SwipeStart(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_SwipeStart_m8AB6F05618EDFB931DAB501FF85A8A1C4836AAEB ();
// 0x00000353 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_Swipe(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_Swipe_mEA3686120C7684059B41962F531022F563893554 ();
// 0x00000354 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_SwipeEnd(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_SwipeEnd_mEDC32271BCBE699F24D0D8BC1751AE3D49C941BA ();
// 0x00000355 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_DragStart(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_DragStart_m740DDE3CADF433592E2543C296C2E1AAC167B136 ();
// 0x00000356 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_Drag(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_Drag_mE960E83E9511C4F8D40791C4F3A686920E7F7D66 ();
// 0x00000357 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_DragEnd(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_DragEnd_m107B02A41EF56A62A0C509B2BF0EDB48D46B9943 ();
// 0x00000358 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_Cancel(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_Cancel_mCF059F25609C7CA53A18B1960EC54DE470347566 ();
// 0x00000359 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_TouchStart2Fingers_mED9EA412466D921C37EFA898F4759305A9E2D736 ();
// 0x0000035A System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_TouchDown2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_TouchDown2Fingers_m8F2990E73692F51FC49078389BC315996A286C33 ();
// 0x0000035B System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_TouchUp2Fingers_m21A93D70D6987B1B1A8C90986941EF50359E9CD5 ();
// 0x0000035C System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_LongTapStart2Fingers_m3FAECD3D400D66AB6E1BF18C12E6ADDBC2901940 ();
// 0x0000035D System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_LongTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_LongTap2Fingers_m566D7816F5C5EEC8B6E4F6D9E9D76BA9624C2AF5 ();
// 0x0000035E System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_LongTapEnd2Fingers_m9C2146DA7EEAA78E1C186D9B6DDEC43FEB05BBF2 ();
// 0x0000035F System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_DragStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_DragStart2Fingers_m5D3CA4E9B6EE613ED550BC376F2A932351AC21ED ();
// 0x00000360 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_Drag2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_Drag2Fingers_mDD2C39858E72756AA231459AD9A479B0C26FE16B ();
// 0x00000361 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_DragEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_DragEnd2Fingers_mF0FE2C21B1F41F2AB15457AABE5A13EECE725D7E ();
// 0x00000362 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_SwipeStart2Fingers_m5EB76F195BEE887E27AA52708A78FD80CF5E7759 ();
// 0x00000363 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_Swipe2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_Swipe2Fingers_mD40D225719BD87605F38B1A66E8C59FE711D037C ();
// 0x00000364 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_SwipeEnd2Fingers_mDC4B51476CB2A65561FB2A6A8F6BAE3EEE1F8260 ();
// 0x00000365 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_Twist_m7AF44FE52517312C014F59771DACD2CEA79A0C15 ();
// 0x00000366 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_TwistEnd(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_TwistEnd_mEA5A698D69E659F04D39D87B199354E2E14E9290 ();
// 0x00000367 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_Pinch(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_Pinch_m3814E9FC11484E90010510744268AC64BD6E7C22 ();
// 0x00000368 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_PinchOut(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_PinchOut_mD91A4A271C923EE6E87669FE37A21C1C77B4B7BF ();
// 0x00000369 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_PinchIn(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_PinchIn_m6CC6C986D08EF95022ACB866E52ED99174D43CEC ();
// 0x0000036A System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_PinchEnd(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_PinchEnd_mCFCC92D92A354C94DD502FA424E7CCF19ED8BB2A ();
// 0x0000036B System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_SimpleTap2Fingers_mBFD74A5641EE4F36A419928838C02E70C593FF9C ();
// 0x0000036C System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_DoubleTap2Fingers_m37FF870B26017DA5B5CD016157478D80F32BFAF2 ();
// 0x0000036D System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_UIElementTouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_UIElementTouchUp_m59E963F792D0AFF69007370FCF7C515C4EA6D7B7 ();
// 0x0000036E System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::On_OverUIElement(HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_On_OverUIElement_m55B286E7442D7115F6831D91020D050A48233D19 ();
// 0x0000036F System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::AddTrigger(HedgehogTeam.EasyTouch.EasyTouch_EvtType)
extern void EasyTouchTrigger_AddTrigger_m27E4FACCF1F692C9BB24972C8D8C7AF713FF9274 ();
// 0x00000370 System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger::SetTriggerEnable(System.String,System.Boolean)
extern void EasyTouchTrigger_SetTriggerEnable_mFBD931A7FAEE6F6B256999BD7D8D30D5C647F453 ();
// 0x00000371 System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger::GetTriggerEnable(System.String)
extern void EasyTouchTrigger_GetTriggerEnable_m58C9F8ECE31BB90311A556DF5D6D24C31EDB730A ();
// 0x00000372 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::TriggerScheduler(HedgehogTeam.EasyTouch.EasyTouch_EvtType,HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouchTrigger_TriggerScheduler_m551750B30661A2EBA9C1773AAE26CCC49643E701 ();
// 0x00000373 System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger::IsRecevier4(HedgehogTeam.EasyTouch.EasyTouch_EvtType)
extern void EasyTouchTrigger_IsRecevier4_mD4544DF3B62C6F4C3F85AAE327B86DC588DBC934 ();
// 0x00000374 HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver HedgehogTeam.EasyTouch.EasyTouchTrigger::GetTrigger(System.String)
extern void EasyTouchTrigger_GetTrigger_mD10D58C6EFE45AAB1246166E71D26DDACA6B8595 ();
// 0x00000375 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger::.ctor()
extern void EasyTouchTrigger__ctor_m48F9CE5A67418AEFF9D62CE1ADAD4C2D4041D457 ();
// 0x00000376 System.Void HedgehogTeam.EasyTouch.QuickBase::Awake()
extern void QuickBase_Awake_mE45ACA3A836B9CC9530195AC03E482CCD539A7C8 ();
// 0x00000377 System.Void HedgehogTeam.EasyTouch.QuickBase::Start()
extern void QuickBase_Start_mF31E321C7867827B90A454B84FB63589E202A30A ();
// 0x00000378 System.Void HedgehogTeam.EasyTouch.QuickBase::OnEnable()
extern void QuickBase_OnEnable_m229AB2AD87DA9C451DFDAC41F3C3068AD0850817 ();
// 0x00000379 System.Void HedgehogTeam.EasyTouch.QuickBase::OnDisable()
extern void QuickBase_OnDisable_mE54B0741A8E06383C7FC0274951978E27546CB8B ();
// 0x0000037A UnityEngine.Vector3 HedgehogTeam.EasyTouch.QuickBase::GetInfluencedAxis()
extern void QuickBase_GetInfluencedAxis_mB28CC2EB2CF21EBB99A59735697F66140B079F26 ();
// 0x0000037B System.Void HedgehogTeam.EasyTouch.QuickBase::DoDirectAction(System.Single)
extern void QuickBase_DoDirectAction_m4BD5573E2186CC349D4F7666270262488B53DA0A ();
// 0x0000037C System.Void HedgehogTeam.EasyTouch.QuickBase::EnabledQuickComponent(System.String)
extern void QuickBase_EnabledQuickComponent_m9F479F1B7CBC49F63710F9DC0AA6C10DC0AD8C69 ();
// 0x0000037D System.Void HedgehogTeam.EasyTouch.QuickBase::DisabledQuickComponent(System.String)
extern void QuickBase_DisabledQuickComponent_m7433D42D2ED635019EAAC10C97F6EC22342C0D53 ();
// 0x0000037E System.Void HedgehogTeam.EasyTouch.QuickBase::DisabledAllSwipeExcepted(System.String)
extern void QuickBase_DisabledAllSwipeExcepted_m663F0B4F908235073A9DC24B6E25C55099B2F3DA ();
// 0x0000037F System.Void HedgehogTeam.EasyTouch.QuickBase::.ctor()
extern void QuickBase__ctor_m72EF095BC9624BC5B806F8F0B9185937031156FF ();
// 0x00000380 System.Void HedgehogTeam.EasyTouch.QuickDrag::.ctor()
extern void QuickDrag__ctor_m15AD2A187771F3D204E48C81B499DF94E3B705CF ();
// 0x00000381 System.Void HedgehogTeam.EasyTouch.QuickDrag::OnEnable()
extern void QuickDrag_OnEnable_mA71556956F1B88874865242FEBCD2BFE5EA4112E ();
// 0x00000382 System.Void HedgehogTeam.EasyTouch.QuickDrag::OnDisable()
extern void QuickDrag_OnDisable_mB271040344F7972AE80F25949138F31818ABA3C2 ();
// 0x00000383 System.Void HedgehogTeam.EasyTouch.QuickDrag::OnDestroy()
extern void QuickDrag_OnDestroy_mFFEFC1FD3F705845B642B40089AADD3AD27E7C57 ();
// 0x00000384 System.Void HedgehogTeam.EasyTouch.QuickDrag::UnsubscribeEvent()
extern void QuickDrag_UnsubscribeEvent_m78A55B08AC3B7219AA07DFBE1F6D8C8DFE2DCEF8 ();
// 0x00000385 System.Void HedgehogTeam.EasyTouch.QuickDrag::OnCollisionEnter()
extern void QuickDrag_OnCollisionEnter_m9525B1CC52B5685C97DF6FB798BF0285BA999455 ();
// 0x00000386 System.Void HedgehogTeam.EasyTouch.QuickDrag::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern void QuickDrag_On_TouchStart_m2AF99AA9A7F5AE904811439AA8BC1C0BC0967F8D ();
// 0x00000387 System.Void HedgehogTeam.EasyTouch.QuickDrag::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void QuickDrag_On_TouchDown_m1B0D67493BF0D8A6299FBF6A9BE798D83B456BBE ();
// 0x00000388 System.Void HedgehogTeam.EasyTouch.QuickDrag::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void QuickDrag_On_TouchUp_m3DFFC62B95EE1C8AFC8BE2E6E0BDA17599F07D83 ();
// 0x00000389 System.Void HedgehogTeam.EasyTouch.QuickDrag::On_DragStart(HedgehogTeam.EasyTouch.Gesture)
extern void QuickDrag_On_DragStart_m8BD3CF68062A4FB9A9AF96E973F6D60535F707E6 ();
// 0x0000038A System.Void HedgehogTeam.EasyTouch.QuickDrag::On_Drag(HedgehogTeam.EasyTouch.Gesture)
extern void QuickDrag_On_Drag_mB9EE5722E773F2DBDE43F27254C5C4DAB706CBE2 ();
// 0x0000038B System.Void HedgehogTeam.EasyTouch.QuickDrag::On_DragEnd(HedgehogTeam.EasyTouch.Gesture)
extern void QuickDrag_On_DragEnd_mCE34654E155520FC30304ADA65516B48433E2ED7 ();
// 0x0000038C UnityEngine.Vector3 HedgehogTeam.EasyTouch.QuickDrag::GetPositionAxes(UnityEngine.Vector3)
extern void QuickDrag_GetPositionAxes_mB7B7BFF16546E8AEFD8D89F9210CB70B10CBF4D4 ();
// 0x0000038D System.Void HedgehogTeam.EasyTouch.QuickDrag::StopDrag()
extern void QuickDrag_StopDrag_m6873FF5D48404A5F6A274E2CB07FB482647AA581 ();
// 0x0000038E System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist::.ctor()
extern void QuickEnterOverExist__ctor_m45745191648DF5CA2B6CB59D10C880D474C78A9C ();
// 0x0000038F System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist::Awake()
extern void QuickEnterOverExist_Awake_mD43B550F7674D42063A0210EE6524EDBFCAB772A ();
// 0x00000390 System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist::OnEnable()
extern void QuickEnterOverExist_OnEnable_m543C5F88B64EDAE0003ED0F3F7D7ABE118D373D3 ();
// 0x00000391 System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist::OnDisable()
extern void QuickEnterOverExist_OnDisable_m51E0DBA91617D2CD42EC515AD791EFA0F0EA54ED ();
// 0x00000392 System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist::OnDestroy()
extern void QuickEnterOverExist_OnDestroy_m9E657D19F7F90868E03B73C893853C6789FB8330 ();
// 0x00000393 System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist::UnsubscribeEvent()
extern void QuickEnterOverExist_UnsubscribeEvent_mED5FE96400B9B6419788501B95D5D1DAC536F6F6 ();
// 0x00000394 System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern void QuickEnterOverExist_On_TouchDown_m7B2694710E65BC8B0003053152ED229837255FCF ();
// 0x00000395 System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern void QuickEnterOverExist_On_TouchUp_m6B5864F962785D029F6B7D65BCAA3570350BF52C ();
// 0x00000396 System.Void HedgehogTeam.EasyTouch.QuickLongTap::.ctor()
extern void QuickLongTap__ctor_mAFE9C84FDE30871F077AA01A7D7742D0C8244E3C ();
// 0x00000397 System.Void HedgehogTeam.EasyTouch.QuickLongTap::Update()
extern void QuickLongTap_Update_m169423D6A4B878EC9B751B540697AB14F33DC842 ();
// 0x00000398 System.Void HedgehogTeam.EasyTouch.QuickLongTap::DoAction(HedgehogTeam.EasyTouch.Gesture)
extern void QuickLongTap_DoAction_m6B871E605256A82D282ED7DF9CB9216FDD71CB40 ();
// 0x00000399 System.Boolean HedgehogTeam.EasyTouch.QuickLongTap::IsOverMe(HedgehogTeam.EasyTouch.Gesture)
extern void QuickLongTap_IsOverMe_m84C139027D1AFE6C71025BBA2758C15341E097E3 ();
// 0x0000039A System.Void HedgehogTeam.EasyTouch.QuickPinch::.ctor()
extern void QuickPinch__ctor_m8F793548382B7D49449030BC5455A433EDCC145B ();
// 0x0000039B System.Void HedgehogTeam.EasyTouch.QuickPinch::OnEnable()
extern void QuickPinch_OnEnable_m36023D422F5AD602BB485B09AF1AE0502703426D ();
// 0x0000039C System.Void HedgehogTeam.EasyTouch.QuickPinch::OnDisable()
extern void QuickPinch_OnDisable_mB0B2879F18F4965FABBC2886A33F6051159BF56C ();
// 0x0000039D System.Void HedgehogTeam.EasyTouch.QuickPinch::OnDestroy()
extern void QuickPinch_OnDestroy_mADFAC4A8B8C943B9034D053E0DE5155073F4679E ();
// 0x0000039E System.Void HedgehogTeam.EasyTouch.QuickPinch::UnsubscribeEvent()
extern void QuickPinch_UnsubscribeEvent_m8897B207A1EC343B14999DA19A1D55DA333B8966 ();
// 0x0000039F System.Void HedgehogTeam.EasyTouch.QuickPinch::On_Pinch(HedgehogTeam.EasyTouch.Gesture)
extern void QuickPinch_On_Pinch_mC647D6DBDA1B9C3419486F24552D60CBB024AB25 ();
// 0x000003A0 System.Void HedgehogTeam.EasyTouch.QuickPinch::On_PinchIn(HedgehogTeam.EasyTouch.Gesture)
extern void QuickPinch_On_PinchIn_mEF4525419DB887CF149FEEA839B12C77AE169CE7 ();
// 0x000003A1 System.Void HedgehogTeam.EasyTouch.QuickPinch::On_PinchOut(HedgehogTeam.EasyTouch.Gesture)
extern void QuickPinch_On_PinchOut_mAC09D4C91014F598924F31D33C2191B737B2FD4B ();
// 0x000003A2 System.Void HedgehogTeam.EasyTouch.QuickPinch::On_PichEnd(HedgehogTeam.EasyTouch.Gesture)
extern void QuickPinch_On_PichEnd_m7E45F14CE8D8513B2E9DB00DFA0A5E1A86CA8F3D ();
// 0x000003A3 System.Void HedgehogTeam.EasyTouch.QuickPinch::DoAction(HedgehogTeam.EasyTouch.Gesture)
extern void QuickPinch_DoAction_m4A3D63434FB0C6B37F3C928120A2BC6264F3A158 ();
// 0x000003A4 System.Void HedgehogTeam.EasyTouch.QuickSwipe::.ctor()
extern void QuickSwipe__ctor_m103A8C68349269611D91BDFF1E6AD2E35294EF50 ();
// 0x000003A5 System.Void HedgehogTeam.EasyTouch.QuickSwipe::OnEnable()
extern void QuickSwipe_OnEnable_mB29973394673BE23B0D859FE0C9BE23842815D25 ();
// 0x000003A6 System.Void HedgehogTeam.EasyTouch.QuickSwipe::OnDisable()
extern void QuickSwipe_OnDisable_m039EA35198A34D51ED7DD595973B70E48ED3FCD5 ();
// 0x000003A7 System.Void HedgehogTeam.EasyTouch.QuickSwipe::OnDestroy()
extern void QuickSwipe_OnDestroy_m8306B7AC738B5A00D4979889A877E1F24403E678 ();
// 0x000003A8 System.Void HedgehogTeam.EasyTouch.QuickSwipe::UnsubscribeEvent()
extern void QuickSwipe_UnsubscribeEvent_m8A5BDA871105F3001EED8C9F16CA44EACCB8EAFB ();
// 0x000003A9 System.Void HedgehogTeam.EasyTouch.QuickSwipe::On_Swipe(HedgehogTeam.EasyTouch.Gesture)
extern void QuickSwipe_On_Swipe_mBB46FA3C72D3E3686FFC2E190B23157255A721FA ();
// 0x000003AA System.Void HedgehogTeam.EasyTouch.QuickSwipe::On_SwipeEnd(HedgehogTeam.EasyTouch.Gesture)
extern void QuickSwipe_On_SwipeEnd_m6283D9F39D34244AA746DDE7DDA4768D6355BF79 ();
// 0x000003AB System.Void HedgehogTeam.EasyTouch.QuickSwipe::On_DragEnd(HedgehogTeam.EasyTouch.Gesture)
extern void QuickSwipe_On_DragEnd_m20EB1EEC594291E9A239C24CC69B4A19D411AD23 ();
// 0x000003AC System.Void HedgehogTeam.EasyTouch.QuickSwipe::On_Drag(HedgehogTeam.EasyTouch.Gesture)
extern void QuickSwipe_On_Drag_m3A19FFE4C70EC9AB043CBE8447D0FC5D7509B135 ();
// 0x000003AD System.Boolean HedgehogTeam.EasyTouch.QuickSwipe::isRightDirection(HedgehogTeam.EasyTouch.Gesture)
extern void QuickSwipe_isRightDirection_m97109BB53C5F213D861165089E721339447D5EAA ();
// 0x000003AE System.Void HedgehogTeam.EasyTouch.QuickSwipe::DoAction(HedgehogTeam.EasyTouch.Gesture)
extern void QuickSwipe_DoAction_mD88BF66FA14D6275559DEB52F8C8F67FA8CF7874 ();
// 0x000003AF System.Void HedgehogTeam.EasyTouch.QuickTap::.ctor()
extern void QuickTap__ctor_m1387DACA0C92859479CDA62FC7A5B871EE75E8B1 ();
// 0x000003B0 System.Void HedgehogTeam.EasyTouch.QuickTap::Update()
extern void QuickTap_Update_m68EE78BE4DA5D6A3F0EA62DB9F825FB8CAEB4D09 ();
// 0x000003B1 System.Void HedgehogTeam.EasyTouch.QuickTap::DoAction(HedgehogTeam.EasyTouch.Gesture)
extern void QuickTap_DoAction_m1D43666F9A68BE70722D7E301E8EA6B775827B85 ();
// 0x000003B2 System.Void HedgehogTeam.EasyTouch.QuickTouch::.ctor()
extern void QuickTouch__ctor_mFA53C0A1FE6D0E1A69282A21228C8BAEF819EE6B ();
// 0x000003B3 System.Void HedgehogTeam.EasyTouch.QuickTouch::Update()
extern void QuickTouch_Update_m52C4F9B55578F6495AFFB9E945BBAE1B5AB02512 ();
// 0x000003B4 System.Void HedgehogTeam.EasyTouch.QuickTouch::DoAction(HedgehogTeam.EasyTouch.Gesture)
extern void QuickTouch_DoAction_mF1E1FC42435E76F97BF5992B6035A19F8137F64D ();
// 0x000003B5 System.Boolean HedgehogTeam.EasyTouch.QuickTouch::IsOverMe(HedgehogTeam.EasyTouch.Gesture)
extern void QuickTouch_IsOverMe_m4E984CB8B62A7045B5AC6AC016DFEDBEF867D251 ();
// 0x000003B6 System.Void HedgehogTeam.EasyTouch.QuickTwist::.ctor()
extern void QuickTwist__ctor_m124B147FCFB74267018F7730FC130CF3E7A1A0D4 ();
// 0x000003B7 System.Void HedgehogTeam.EasyTouch.QuickTwist::OnEnable()
extern void QuickTwist_OnEnable_m150C4CF1C506FF577C99254C13A2792F39DC292D ();
// 0x000003B8 System.Void HedgehogTeam.EasyTouch.QuickTwist::OnDisable()
extern void QuickTwist_OnDisable_mFB6F2B148F78C5200E7B62B45B844970ED5090F4 ();
// 0x000003B9 System.Void HedgehogTeam.EasyTouch.QuickTwist::OnDestroy()
extern void QuickTwist_OnDestroy_m8C74910DB2918A0DEBD24F0B6E74F25F6DB68B17 ();
// 0x000003BA System.Void HedgehogTeam.EasyTouch.QuickTwist::UnsubscribeEvent()
extern void QuickTwist_UnsubscribeEvent_mFD4B62BD703DFDA7197EDADA6126B560C2AC4064 ();
// 0x000003BB System.Void HedgehogTeam.EasyTouch.QuickTwist::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern void QuickTwist_On_Twist_mC5A58E17BE4D2B1ECC30ACCB61CC34FA2F3F1783 ();
// 0x000003BC System.Void HedgehogTeam.EasyTouch.QuickTwist::On_TwistEnd(HedgehogTeam.EasyTouch.Gesture)
extern void QuickTwist_On_TwistEnd_m9ECEF22E448E9998BD32E1BC07F9F64F2CE8FBB0 ();
// 0x000003BD System.Boolean HedgehogTeam.EasyTouch.QuickTwist::IsRightRotation(HedgehogTeam.EasyTouch.Gesture)
extern void QuickTwist_IsRightRotation_m54783811E44FF06BF0E73A843FFB051B120024A9 ();
// 0x000003BE System.Void HedgehogTeam.EasyTouch.QuickTwist::DoAction(HedgehogTeam.EasyTouch.Gesture)
extern void QuickTwist_DoAction_mFD7D4EF117BF67A7D578868CE4F0551EED6F39BC ();
// 0x000003BF HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.BaseFinger::GetGesture()
extern void BaseFinger_GetGesture_mF7B1F71FAAA164D2923860675827C788AC97797F ();
// 0x000003C0 System.Void HedgehogTeam.EasyTouch.BaseFinger::.ctor()
extern void BaseFinger__ctor_mD0C79ED74523CE5A7CF4B1989FB0BCC0DC424477 ();
// 0x000003C1 System.Void HedgehogTeam.EasyTouch.ECamera::.ctor(UnityEngine.Camera,System.Boolean)
extern void ECamera__ctor_mAEB9941B81F98EDEDE6B3B959E70ED4682D974BB ();
// 0x000003C2 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Cancel(HedgehogTeam.EasyTouch.EasyTouch_TouchCancelHandler)
extern void EasyTouch_add_On_Cancel_m4D5145DA0A8EA343EB856F5E0DCF407613027C59 ();
// 0x000003C3 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Cancel(HedgehogTeam.EasyTouch.EasyTouch_TouchCancelHandler)
extern void EasyTouch_remove_On_Cancel_m262C3A4930C3BFFC5360906272C90E5095EE91A4 ();
// 0x000003C4 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Cancel2Fingers(HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler)
extern void EasyTouch_add_On_Cancel2Fingers_mB18C68401E44AEE9AE6445D30BE84635112D939B ();
// 0x000003C5 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Cancel2Fingers(HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler)
extern void EasyTouch_remove_On_Cancel2Fingers_m22C78177398F197A13E7B33A6F752EFA0D548335 ();
// 0x000003C6 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchStart(HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler)
extern void EasyTouch_add_On_TouchStart_m97F8CDF7FB22E6FB7515E5BB618C41D675A03BFA ();
// 0x000003C7 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchStart(HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler)
extern void EasyTouch_remove_On_TouchStart_mC872F67C12E8BEC695AF6214936B10D6BB8AA8DC ();
// 0x000003C8 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchDown(HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler)
extern void EasyTouch_add_On_TouchDown_m2D2D5E25BB869F6A6B394F3566EEBE4BAD522EEC ();
// 0x000003C9 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchDown(HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler)
extern void EasyTouch_remove_On_TouchDown_m700E4BBE8FFA99A81B81C8E89F213F1EC410A19C ();
// 0x000003CA System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchUp(HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler)
extern void EasyTouch_add_On_TouchUp_mA1891CE45733A36181535ABFE611B6FFC194FC6A ();
// 0x000003CB System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchUp(HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler)
extern void EasyTouch_remove_On_TouchUp_m0BBFA87933DF34D98AB73029DE067840F76F0E03 ();
// 0x000003CC System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SimpleTap(HedgehogTeam.EasyTouch.EasyTouch_SimpleTapHandler)
extern void EasyTouch_add_On_SimpleTap_m1B25B3C1E31A7094B98F01BBEA5B1F45F49E4579 ();
// 0x000003CD System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SimpleTap(HedgehogTeam.EasyTouch.EasyTouch_SimpleTapHandler)
extern void EasyTouch_remove_On_SimpleTap_mA776F5C495F3EBA34F60D4307AE0758E5CA54FA3 ();
// 0x000003CE System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DoubleTap(HedgehogTeam.EasyTouch.EasyTouch_DoubleTapHandler)
extern void EasyTouch_add_On_DoubleTap_m0A710EFE4C192FB6F656C93333CA349B23B337D4 ();
// 0x000003CF System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DoubleTap(HedgehogTeam.EasyTouch.EasyTouch_DoubleTapHandler)
extern void EasyTouch_remove_On_DoubleTap_m44A74B353A123E9B3F05E2A872E4058761170769 ();
// 0x000003D0 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTapStart(HedgehogTeam.EasyTouch.EasyTouch_LongTapStartHandler)
extern void EasyTouch_add_On_LongTapStart_mF8AD6896FC1ED129681A0A50F51078241D5DA6D5 ();
// 0x000003D1 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTapStart(HedgehogTeam.EasyTouch.EasyTouch_LongTapStartHandler)
extern void EasyTouch_remove_On_LongTapStart_m700757D9AD133688F0CFE9FE80AE78FEDF9A82C1 ();
// 0x000003D2 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTap(HedgehogTeam.EasyTouch.EasyTouch_LongTapHandler)
extern void EasyTouch_add_On_LongTap_mA903F36D7BFFB45C746DAC633242938A61BF2F2E ();
// 0x000003D3 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTap(HedgehogTeam.EasyTouch.EasyTouch_LongTapHandler)
extern void EasyTouch_remove_On_LongTap_m0325ADBA5E88F4CF9CD718882B58FDC74E705EC3 ();
// 0x000003D4 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTapEnd(HedgehogTeam.EasyTouch.EasyTouch_LongTapEndHandler)
extern void EasyTouch_add_On_LongTapEnd_mD65B26DBF12E1397C495C3DDBB69983B66366CDC ();
// 0x000003D5 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTapEnd(HedgehogTeam.EasyTouch.EasyTouch_LongTapEndHandler)
extern void EasyTouch_remove_On_LongTapEnd_m8E8330E95476B3849943009570935DE70CFD8952 ();
// 0x000003D6 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DragStart(HedgehogTeam.EasyTouch.EasyTouch_DragStartHandler)
extern void EasyTouch_add_On_DragStart_mF0B097955BA74D6610087D5C46BBDE92B4BB6B98 ();
// 0x000003D7 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DragStart(HedgehogTeam.EasyTouch.EasyTouch_DragStartHandler)
extern void EasyTouch_remove_On_DragStart_m92AE8749EB717A9A116196D70E09C7036A21B6CA ();
// 0x000003D8 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Drag(HedgehogTeam.EasyTouch.EasyTouch_DragHandler)
extern void EasyTouch_add_On_Drag_mA55E17CD90BD1075069EF21247F1BCB0B10471CB ();
// 0x000003D9 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Drag(HedgehogTeam.EasyTouch.EasyTouch_DragHandler)
extern void EasyTouch_remove_On_Drag_m40E4B7188D26CE7D4B6D22C708A8182A0DB2707B ();
// 0x000003DA System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DragEnd(HedgehogTeam.EasyTouch.EasyTouch_DragEndHandler)
extern void EasyTouch_add_On_DragEnd_m3E9D4182FE045CD275F2B7290CFD077732031CB1 ();
// 0x000003DB System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DragEnd(HedgehogTeam.EasyTouch.EasyTouch_DragEndHandler)
extern void EasyTouch_remove_On_DragEnd_mFA68FDC4D3AF626D991B96184935B6B6C4B28F0C ();
// 0x000003DC System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeStart(HedgehogTeam.EasyTouch.EasyTouch_SwipeStartHandler)
extern void EasyTouch_add_On_SwipeStart_mAA683134929C0EE754C0D14C977F153325F85511 ();
// 0x000003DD System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeStart(HedgehogTeam.EasyTouch.EasyTouch_SwipeStartHandler)
extern void EasyTouch_remove_On_SwipeStart_mE5FD140158400800623D7F8E39E2FED30D555626 ();
// 0x000003DE System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Swipe(HedgehogTeam.EasyTouch.EasyTouch_SwipeHandler)
extern void EasyTouch_add_On_Swipe_m2576D5A79E42B54FC3696626945275237DFD4B95 ();
// 0x000003DF System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Swipe(HedgehogTeam.EasyTouch.EasyTouch_SwipeHandler)
extern void EasyTouch_remove_On_Swipe_m318B258AD60224EBF8DD2ED6C5D5256C7DD911C1 ();
// 0x000003E0 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeEnd(HedgehogTeam.EasyTouch.EasyTouch_SwipeEndHandler)
extern void EasyTouch_add_On_SwipeEnd_m542B113825441F578D5D86E8E53338D92FEBA493 ();
// 0x000003E1 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeEnd(HedgehogTeam.EasyTouch.EasyTouch_SwipeEndHandler)
extern void EasyTouch_remove_On_SwipeEnd_mE4232BD4DF53D33D2B9869B165372B30E0BBBF2A ();
// 0x000003E2 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler)
extern void EasyTouch_add_On_TouchStart2Fingers_mD03C23CBCDA52A3698559EE3676449F23D9D0FC0 ();
// 0x000003E3 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler)
extern void EasyTouch_remove_On_TouchStart2Fingers_m0EB5073AE67ADE0392A32028AA8EE2142E7EFC5F ();
// 0x000003E4 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchDown2Fingers(HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler)
extern void EasyTouch_add_On_TouchDown2Fingers_mCEC9C49CDAC504B09D7E9BCDE555469C3DC7EBF8 ();
// 0x000003E5 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchDown2Fingers(HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler)
extern void EasyTouch_remove_On_TouchDown2Fingers_m8FC281BAD18B3C3C2B9A87424BB7573424CF122A ();
// 0x000003E6 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchUp2Fingers(HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler)
extern void EasyTouch_add_On_TouchUp2Fingers_m039A663F83056984EA1F7C82CE246C3040CF6556 ();
// 0x000003E7 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchUp2Fingers(HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler)
extern void EasyTouch_remove_On_TouchUp2Fingers_m6E4F3B68FA23F90751EC9D4E5E90FF58ED717122 ();
// 0x000003E8 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler)
extern void EasyTouch_add_On_SimpleTap2Fingers_mC19305DAB55E6BCFB82C8FB6E4123255B82E7738 ();
// 0x000003E9 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler)
extern void EasyTouch_remove_On_SimpleTap2Fingers_m1FF34C9A9AEDAAD4145E7DE230A27009F229D680 ();
// 0x000003EA System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler)
extern void EasyTouch_add_On_DoubleTap2Fingers_mA847C67B0A30C81BC9A12D4FD3046C8F094047E1 ();
// 0x000003EB System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler)
extern void EasyTouch_remove_On_DoubleTap2Fingers_m4D6E01A1404E74A7C937B777B442320A9AD139BD ();
// 0x000003EC System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler)
extern void EasyTouch_add_On_LongTapStart2Fingers_mD6EAB281C23078CDBB179CF030DF06364730DCA2 ();
// 0x000003ED System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler)
extern void EasyTouch_remove_On_LongTapStart2Fingers_mC01DA2731180F0C2282F828900820B9DA907D1AB ();
// 0x000003EE System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler)
extern void EasyTouch_add_On_LongTap2Fingers_mEEBDCAA62C01A97A50392545C95A826662D0B109 ();
// 0x000003EF System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler)
extern void EasyTouch_remove_On_LongTap2Fingers_m45F4803E90895FAE4523272CA320188C07360A06 ();
// 0x000003F0 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler)
extern void EasyTouch_add_On_LongTapEnd2Fingers_mD897971A4E5DA92088145FF69C32BCA7C5F16BE5 ();
// 0x000003F1 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler)
extern void EasyTouch_remove_On_LongTapEnd2Fingers_m3BCFD86E017A40A1E7CF6F187EC582A0402428CF ();
// 0x000003F2 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Twist(HedgehogTeam.EasyTouch.EasyTouch_TwistHandler)
extern void EasyTouch_add_On_Twist_mE80E52A3010E98705B3319F87DDC34706601239A ();
// 0x000003F3 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Twist(HedgehogTeam.EasyTouch.EasyTouch_TwistHandler)
extern void EasyTouch_remove_On_Twist_mF6873B2804131E986AC0C40F390EC4E487A466D5 ();
// 0x000003F4 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TwistEnd(HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler)
extern void EasyTouch_add_On_TwistEnd_m46230202EA91287F5E52C55CDD43F0611B8C0166 ();
// 0x000003F5 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TwistEnd(HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler)
extern void EasyTouch_remove_On_TwistEnd_m27D30CE3446F715A85B737E2F6B9825D43DEB8C7 ();
// 0x000003F6 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Pinch(HedgehogTeam.EasyTouch.EasyTouch_PinchHandler)
extern void EasyTouch_add_On_Pinch_m3AB86DB5A8558B4BA19968EE71452F82CACA6352 ();
// 0x000003F7 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Pinch(HedgehogTeam.EasyTouch.EasyTouch_PinchHandler)
extern void EasyTouch_remove_On_Pinch_m0144B1E20FF8546AFF793BBDF6794007FD2E4BE1 ();
// 0x000003F8 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_PinchIn(HedgehogTeam.EasyTouch.EasyTouch_PinchInHandler)
extern void EasyTouch_add_On_PinchIn_m54185BB2A30BE59E7293FF8080109CD9F0F8D369 ();
// 0x000003F9 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_PinchIn(HedgehogTeam.EasyTouch.EasyTouch_PinchInHandler)
extern void EasyTouch_remove_On_PinchIn_m2633DDA06984988206E09C2A20B4753F16EAA81E ();
// 0x000003FA System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_PinchOut(HedgehogTeam.EasyTouch.EasyTouch_PinchOutHandler)
extern void EasyTouch_add_On_PinchOut_mD86FF1D1540B2E52258D90B6125BB2C5F2A36787 ();
// 0x000003FB System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_PinchOut(HedgehogTeam.EasyTouch.EasyTouch_PinchOutHandler)
extern void EasyTouch_remove_On_PinchOut_m3C345E56635517A6E1473EE206A2310CCFC435B3 ();
// 0x000003FC System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_PinchEnd(HedgehogTeam.EasyTouch.EasyTouch_PinchEndHandler)
extern void EasyTouch_add_On_PinchEnd_mCE516A6547AE177F70D6CC019679865ECBD7B750 ();
// 0x000003FD System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_PinchEnd(HedgehogTeam.EasyTouch.EasyTouch_PinchEndHandler)
extern void EasyTouch_remove_On_PinchEnd_mC95CE1CCF992C035FD5F11F1EC580B002E71213B ();
// 0x000003FE System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DragStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler)
extern void EasyTouch_add_On_DragStart2Fingers_mDE91C6A5A0E98EF2EFCB655FAE2FBAA27DA3BB17 ();
// 0x000003FF System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DragStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler)
extern void EasyTouch_remove_On_DragStart2Fingers_mEBEF7A5C1788FB0D372C873EE7D3CD04C514689E ();
// 0x00000400 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Drag2Fingers(HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler)
extern void EasyTouch_add_On_Drag2Fingers_mA7EB90CBF64A6EE2D985AC3DB16795DAD80E5113 ();
// 0x00000401 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Drag2Fingers(HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler)
extern void EasyTouch_remove_On_Drag2Fingers_mC6D908A3893EF1F9E966C8AA502FD786B859D8C2 ();
// 0x00000402 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DragEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler)
extern void EasyTouch_add_On_DragEnd2Fingers_mE2E7397978E3B634279BDC927E61CB9B92887B0F ();
// 0x00000403 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DragEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler)
extern void EasyTouch_remove_On_DragEnd2Fingers_m36A33E27ABD55E19913DA5C8FB24BA6CE4DE00D4 ();
// 0x00000404 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler)
extern void EasyTouch_add_On_SwipeStart2Fingers_mD066B4DF4DED0C2A5ECDC0B63E2E23F85970DF6B ();
// 0x00000405 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler)
extern void EasyTouch_remove_On_SwipeStart2Fingers_mC014048396BCDB5E414C8CB51E2FAE989CD3FCDA ();
// 0x00000406 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Swipe2Fingers(HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler)
extern void EasyTouch_add_On_Swipe2Fingers_m29F2B727DF6BB6F5DA51998D5F391A46CAB590AC ();
// 0x00000407 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Swipe2Fingers(HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler)
extern void EasyTouch_remove_On_Swipe2Fingers_mE50D63E2CB409D988CF47532116E799801F7C753 ();
// 0x00000408 System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler)
extern void EasyTouch_add_On_SwipeEnd2Fingers_mACCBCCA09EE9D34F296C454A4A7BB284E623A456 ();
// 0x00000409 System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler)
extern void EasyTouch_remove_On_SwipeEnd2Fingers_m52A6FE0854E1376170C0128AD1240929AF3CE3AD ();
// 0x0000040A System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_EasyTouchIsReady(HedgehogTeam.EasyTouch.EasyTouch_EasyTouchIsReadyHandler)
extern void EasyTouch_add_On_EasyTouchIsReady_mF99BA5EC1FF2B801A27773C810930177677050CB ();
// 0x0000040B System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_EasyTouchIsReady(HedgehogTeam.EasyTouch.EasyTouch_EasyTouchIsReadyHandler)
extern void EasyTouch_remove_On_EasyTouchIsReady_m10DC1059571E752245E5AE35A75BB92DD96EF451 ();
// 0x0000040C System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_OverUIElement(HedgehogTeam.EasyTouch.EasyTouch_OverUIElementHandler)
extern void EasyTouch_add_On_OverUIElement_m3A660E8E80D2918A54B419709040A8644C4DC195 ();
// 0x0000040D System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_OverUIElement(HedgehogTeam.EasyTouch.EasyTouch_OverUIElementHandler)
extern void EasyTouch_remove_On_OverUIElement_m872CF9AFEA620D714F9FD66AAF1685FDBB1E55DF ();
// 0x0000040E System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_UIElementTouchUp(HedgehogTeam.EasyTouch.EasyTouch_UIElementTouchUpHandler)
extern void EasyTouch_add_On_UIElementTouchUp_mF003C43244A08052314C00E9889D43598A9E6AA6 ();
// 0x0000040F System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_UIElementTouchUp(HedgehogTeam.EasyTouch.EasyTouch_UIElementTouchUpHandler)
extern void EasyTouch_remove_On_UIElementTouchUp_m796074782B0C81C05662957C991B7FA77F46F4DB ();
// 0x00000410 HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch::get_instance()
extern void EasyTouch_get_instance_m4EC32DCD4E6D3467A303A43E0BAA580C89C62943 ();
// 0x00000411 HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.EasyTouch::get_current()
extern void EasyTouch_get_current_m8C25C504633E944652E3554BE2990F60975047D3 ();
// 0x00000412 System.Void HedgehogTeam.EasyTouch.EasyTouch::.ctor()
extern void EasyTouch__ctor_m824044C7397F0A86844B20D3167923B7AB57020F ();
// 0x00000413 System.Void HedgehogTeam.EasyTouch.EasyTouch::OnEnable()
extern void EasyTouch_OnEnable_m4F120C46FCEA5247A811CD29DC492D1736F68815 ();
// 0x00000414 System.Void HedgehogTeam.EasyTouch.EasyTouch::Awake()
extern void EasyTouch_Awake_mD613403936D28738DC2DFA5697D9289340D54F13 ();
// 0x00000415 System.Void HedgehogTeam.EasyTouch.EasyTouch::Start()
extern void EasyTouch_Start_m92693B8343FE8A80CE5C3F9E012DDF885F9059E9 ();
// 0x00000416 System.Void HedgehogTeam.EasyTouch.EasyTouch::Init()
extern void EasyTouch_Init_m4D6ABC5FE7775C2D015DDB12BC1ED4ABFFF3055C ();
// 0x00000417 System.Void HedgehogTeam.EasyTouch.EasyTouch::OnDrawGizmos()
extern void EasyTouch_OnDrawGizmos_m683D503BF36A525A68CF90FE155842EA867C7C55 ();
// 0x00000418 System.Void HedgehogTeam.EasyTouch.EasyTouch::Update()
extern void EasyTouch_Update_mBF4D18A7FFABBF9FFEB4EC2BC0EE96E893D64610 ();
// 0x00000419 System.Void HedgehogTeam.EasyTouch.EasyTouch::LateUpdate()
extern void EasyTouch_LateUpdate_m4207884C8CD599EA280F89A49C3F019EC3D5B696 ();
// 0x0000041A System.Void HedgehogTeam.EasyTouch.EasyTouch::UpdateTouches(System.Boolean,System.Int32)
extern void EasyTouch_UpdateTouches_m99BA926BA28D6316F033DCCE4082068288F8014F ();
// 0x0000041B System.Void HedgehogTeam.EasyTouch.EasyTouch::ResetTouches()
extern void EasyTouch_ResetTouches_m4442CCA6BCE69F9FF5C3032B18754EA1DEE3EBC0 ();
// 0x0000041C System.Void HedgehogTeam.EasyTouch.EasyTouch::OneFinger(System.Int32)
extern void EasyTouch_OneFinger_m806337BECEFA4DC518E18003CAD5EC8A62CE7841 ();
// 0x0000041D System.Collections.IEnumerator HedgehogTeam.EasyTouch.EasyTouch::SingleOrDouble(System.Int32)
extern void EasyTouch_SingleOrDouble_m996FA47004CD12E785054B3C38C4B1AA630B9145 ();
// 0x0000041E System.Void HedgehogTeam.EasyTouch.EasyTouch::CreateGesture(System.Int32,HedgehogTeam.EasyTouch.EasyTouch_EvtType,HedgehogTeam.EasyTouch.Finger,HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection,System.Single,UnityEngine.Vector2)
extern void EasyTouch_CreateGesture_mEE109C3593F2E4C759A65EB6D6316D275DC951A5 ();
// 0x0000041F System.Void HedgehogTeam.EasyTouch.EasyTouch::TwoFinger()
extern void EasyTouch_TwoFinger_m9A8625AB639E6925EFAD3A5CD425F7DB9B76ACFA ();
// 0x00000420 System.Void HedgehogTeam.EasyTouch.EasyTouch::DetectPinch(System.Single)
extern void EasyTouch_DetectPinch_m6650DFA2E65849D969D5123B9C7A62A1D427BBCA ();
// 0x00000421 System.Void HedgehogTeam.EasyTouch.EasyTouch::DetecTwist(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void EasyTouch_DetecTwist_m80B77AAE6D2BEB920C203CFDB75C8F5BFF3E4DEA ();
// 0x00000422 System.Void HedgehogTeam.EasyTouch.EasyTouch::CreateStateEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch_GestureType,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean,System.Single,System.Single,System.Single)
extern void EasyTouch_CreateStateEnd2Fingers_m85DCDF49B29F87CF4976583CCAA020660D7A929A ();
// 0x00000423 System.Collections.IEnumerator HedgehogTeam.EasyTouch.EasyTouch::SingleOrDouble2Fingers()
extern void EasyTouch_SingleOrDouble2Fingers_mD214C9F818629F80E85A1FF8FB52EDAF00ECEBAF ();
// 0x00000424 System.Void HedgehogTeam.EasyTouch.EasyTouch::CreateGesture2Finger(HedgehogTeam.EasyTouch.EasyTouch_EvtType,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection,System.Single,UnityEngine.Vector2,System.Single,System.Single,System.Single)
extern void EasyTouch_CreateGesture2Finger_m85C3576FF6EBE5F0712DF9B89B82CEC1254C83C4 ();
// 0x00000425 System.Int32 HedgehogTeam.EasyTouch.EasyTouch::GetTwoFinger(System.Int32)
extern void EasyTouch_GetTwoFinger_mD7E6D39FE16064E58F38CDA8E3DDCF7F07FEEBB2 ();
// 0x00000426 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetTwoFingerPickedObject()
extern void EasyTouch_GetTwoFingerPickedObject_m0D0A8EDFFB1F511AD62493A2709B899C8B69A8CB ();
// 0x00000427 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetTwoFingerPickedUIElement()
extern void EasyTouch_GetTwoFingerPickedUIElement_m4CC3B79E8EEE4A8E27961B7CE992FD64D520D641 ();
// 0x00000428 System.Void HedgehogTeam.EasyTouch.EasyTouch::RaiseEvent(HedgehogTeam.EasyTouch.EasyTouch_EvtType,HedgehogTeam.EasyTouch.Gesture)
extern void EasyTouch_RaiseEvent_m6D8729C26586C4869FA9B065AAB64C703810B01F ();
// 0x00000429 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetPickedGameObject(HedgehogTeam.EasyTouch.Finger,System.Boolean)
extern void EasyTouch_GetPickedGameObject_mB62261DA4CF662819F8F6769FCC0B1EF9D5A895C ();
// 0x0000042A System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetGameObjectAt(UnityEngine.Vector2,UnityEngine.Camera,System.Boolean)
extern void EasyTouch_GetGameObjectAt_m64D43562BBF1119733A60012FB154D784CDC95AA ();
// 0x0000042B HedgehogTeam.EasyTouch.EasyTouch_SwipeDirection HedgehogTeam.EasyTouch.EasyTouch::GetSwipe(UnityEngine.Vector2,UnityEngine.Vector2)
extern void EasyTouch_GetSwipe_m78BDEFB22EDEA16F70306BBC477ECB8DB6D6A678 ();
// 0x0000042C System.Boolean HedgehogTeam.EasyTouch.EasyTouch::FingerInTolerance(HedgehogTeam.EasyTouch.Finger)
extern void EasyTouch_FingerInTolerance_m5E7C26AF42914EC105AE3E77E20AE1C503580C95 ();
// 0x0000042D System.Boolean HedgehogTeam.EasyTouch.EasyTouch::IsTouchOverNGui(UnityEngine.Vector2,System.Boolean)
extern void EasyTouch_IsTouchOverNGui_mCD9F37AC010E5F2FEDC9D14F490761278F575555 ();
// 0x0000042E HedgehogTeam.EasyTouch.Finger HedgehogTeam.EasyTouch.EasyTouch::GetFinger(System.Int32)
extern void EasyTouch_GetFinger_m31FC29DC8EC219128C62F9E56EE050BF4AD0790B ();
// 0x0000042F System.Boolean HedgehogTeam.EasyTouch.EasyTouch::IsScreenPositionOverUI(UnityEngine.Vector2)
extern void EasyTouch_IsScreenPositionOverUI_mA9C4CE91D3E71A51291724D773918E9B7497D625 ();
// 0x00000430 UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouch::GetFirstUIElementFromCache()
extern void EasyTouch_GetFirstUIElementFromCache_mA1CFF57052EE8934FF6E7F02767BE2F18652F463 ();
// 0x00000431 UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouch::GetFirstUIElement(UnityEngine.Vector2)
extern void EasyTouch_GetFirstUIElement_m57BFC575A199CADEEA6956B38DD8B359EE779BFB ();
// 0x00000432 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::IsFingerOverUIElement(System.Int32)
extern void EasyTouch_IsFingerOverUIElement_mC9476A13C2709A91FEE5EE78493BE3B49ED17867 ();
// 0x00000433 UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouch::GetCurrentPickedUIElement(System.Int32,System.Boolean)
extern void EasyTouch_GetCurrentPickedUIElement_mB74BE964259134F74D391B761B9B9B8D6305167B ();
// 0x00000434 UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouch::GetCurrentPickedObject(System.Int32,System.Boolean)
extern void EasyTouch_GetCurrentPickedObject_mD4CC0C83EF24E46C2C20A86D2A1E91524965131F ();
// 0x00000435 UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouch::GetGameObjectAt(UnityEngine.Vector2,System.Boolean)
extern void EasyTouch_GetGameObjectAt_mE60FDEE97CC006E13E3745D79F10AB85E7CDA5EB ();
// 0x00000436 System.Int32 HedgehogTeam.EasyTouch.EasyTouch::GetTouchCount()
extern void EasyTouch_GetTouchCount_mE6679BE58C7B97EB10AA3E9A46A90E262E5DA8C7 ();
// 0x00000437 System.Void HedgehogTeam.EasyTouch.EasyTouch::ResetTouch(System.Int32)
extern void EasyTouch_ResetTouch_mB3E1E99369B9B6066AEC529E6E88582540FC193A ();
// 0x00000438 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnabled(System.Boolean)
extern void EasyTouch_SetEnabled_m85A9A6BF4391266CA761756271CD1E55258189A8 ();
// 0x00000439 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetEnabled()
extern void EasyTouch_GetEnabled_m6DAC0A4CD5D581F18C9C4C65C2A3F2BD6AB55871 ();
// 0x0000043A System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnableUIDetection(System.Boolean)
extern void EasyTouch_SetEnableUIDetection_m64F49C1FDE241F2108DECF8A33FE16C58EBE6CB4 ();
// 0x0000043B System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetEnableUIDetection()
extern void EasyTouch_GetEnableUIDetection_m2BB2C11AEA3BE7F4FFDB5B2C72692FB880C74C5E ();
// 0x0000043C System.Void HedgehogTeam.EasyTouch.EasyTouch::SetUICompatibily(System.Boolean)
extern void EasyTouch_SetUICompatibily_m9DC3C352B33E7DF4B4DAED2FB5C49D6AEA94551E ();
// 0x0000043D System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetUIComptability()
extern void EasyTouch_GetUIComptability_m78DC33E3DC502174F63540717F6279A7FB98F6CE ();
// 0x0000043E System.Void HedgehogTeam.EasyTouch.EasyTouch::SetAutoUpdateUI(System.Boolean)
extern void EasyTouch_SetAutoUpdateUI_m531462A1FD65B2D14A99498CF59FB1564B651315 ();
// 0x0000043F System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetAutoUpdateUI()
extern void EasyTouch_GetAutoUpdateUI_m75A374651F6D9A960D8415E2E7B28EFB92A5F20A ();
// 0x00000440 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetNGUICompatibility(System.Boolean)
extern void EasyTouch_SetNGUICompatibility_m51F6DAE53AFC53F2B490CCD1F954A57424DBA560 ();
// 0x00000441 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetNGUICompatibility()
extern void EasyTouch_GetNGUICompatibility_m3936DBC6E3571FB3FFC253B7CA939B01818E8C3E ();
// 0x00000442 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnableAutoSelect(System.Boolean)
extern void EasyTouch_SetEnableAutoSelect_mBC19F466F18F576FACF7BE176C22ADFB4C4A73E3 ();
// 0x00000443 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetEnableAutoSelect()
extern void EasyTouch_GetEnableAutoSelect_mFFDA2D74DD630D0F2480DB805E1C81F57A0B2F0F ();
// 0x00000444 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetAutoUpdatePickedObject(System.Boolean)
extern void EasyTouch_SetAutoUpdatePickedObject_mC8D1679142EE30BD01C7823CCF2732E5BB362A3B ();
// 0x00000445 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetAutoUpdatePickedObject()
extern void EasyTouch_GetAutoUpdatePickedObject_mDF7C4BE1F832633FA734BCFEC4A58E8028BCF222 ();
// 0x00000446 System.Void HedgehogTeam.EasyTouch.EasyTouch::Set3DPickableLayer(UnityEngine.LayerMask)
extern void EasyTouch_Set3DPickableLayer_mCA629B084DCD7313141549CF0C2FD0FC5FDFED08 ();
// 0x00000447 UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::Get3DPickableLayer()
extern void EasyTouch_Get3DPickableLayer_m249C0E5A794FFEAE5FE2F4CC57D09E8FE93DBDD4 ();
// 0x00000448 System.Void HedgehogTeam.EasyTouch.EasyTouch::AddCamera(UnityEngine.Camera,System.Boolean)
extern void EasyTouch_AddCamera_m0057E4A6D6BD214077BF5A3F262EB873121676A3 ();
// 0x00000449 System.Void HedgehogTeam.EasyTouch.EasyTouch::RemoveCamera(UnityEngine.Camera)
extern void EasyTouch_RemoveCamera_m6075E81E6429E1E3F94AAC7955384126F2FEB415 ();
// 0x0000044A UnityEngine.Camera HedgehogTeam.EasyTouch.EasyTouch::GetCamera(System.Int32)
extern void EasyTouch_GetCamera_m4B1FD6D6C5AA8797F4B104F9DAE393194BB407B5 ();
// 0x0000044B System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnable2DCollider(System.Boolean)
extern void EasyTouch_SetEnable2DCollider_m3CED9249618DD25AC476D21995F118A21EB01256 ();
// 0x0000044C System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetEnable2DCollider()
extern void EasyTouch_GetEnable2DCollider_m6FCFC0EA3C1CBCACD2B5C9544B038484DC61AE81 ();
// 0x0000044D System.Void HedgehogTeam.EasyTouch.EasyTouch::Set2DPickableLayer(UnityEngine.LayerMask)
extern void EasyTouch_Set2DPickableLayer_m724E88D020966DBC275B4BC2CB4AEEF10BDD2F8A ();
// 0x0000044E UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::Get2DPickableLayer()
extern void EasyTouch_Get2DPickableLayer_m97DEA9089481FA9510012506DE87AAC3E0F0EC9B ();
// 0x0000044F System.Void HedgehogTeam.EasyTouch.EasyTouch::SetGesturePriority(HedgehogTeam.EasyTouch.EasyTouch_GesturePriority)
extern void EasyTouch_SetGesturePriority_m4378BBF308B3A17CF3BCA736392E03C88C183FA7 ();
// 0x00000450 HedgehogTeam.EasyTouch.EasyTouch_GesturePriority HedgehogTeam.EasyTouch.EasyTouch::GetGesturePriority()
extern void EasyTouch_GetGesturePriority_m838B2DD8C6D1FB57C6DA91ACC946C7F68F5E92AF ();
// 0x00000451 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetStationaryTolerance(System.Single)
extern void EasyTouch_SetStationaryTolerance_mDD24F3EEB7F9A0E1AA8CC994650380057084DD00 ();
// 0x00000452 System.Single HedgehogTeam.EasyTouch.EasyTouch::GetStationaryTolerance()
extern void EasyTouch_GetStationaryTolerance_m1823E6257577CB4FD1292E492E1165E3583FC032 ();
// 0x00000453 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetLongTapTime(System.Single)
extern void EasyTouch_SetLongTapTime_mD1398D865EBB7E57540388CE4AA1DC11B4741EE3 ();
// 0x00000454 System.Single HedgehogTeam.EasyTouch.EasyTouch::GetlongTapTime()
extern void EasyTouch_GetlongTapTime_mCEE03CE98A2FBCA016587DD572AA4D604A41CB32 ();
// 0x00000455 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetDoubleTapTime(System.Single)
extern void EasyTouch_SetDoubleTapTime_m3E41B76C100E04E9A0A3F26730DF1FBB3F14C953 ();
// 0x00000456 System.Single HedgehogTeam.EasyTouch.EasyTouch::GetDoubleTapTime()
extern void EasyTouch_GetDoubleTapTime_mB3D692A98C275C754F8B32E0C4D3DEB98BCF2F27 ();
// 0x00000457 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetDoubleTapMethod(HedgehogTeam.EasyTouch.EasyTouch_DoubleTapDetection)
extern void EasyTouch_SetDoubleTapMethod_m19F2B51AC054659EAC6FC1D55C2BBFF5B52781D6 ();
// 0x00000458 HedgehogTeam.EasyTouch.EasyTouch_DoubleTapDetection HedgehogTeam.EasyTouch.EasyTouch::GetDoubleTapMethod()
extern void EasyTouch_GetDoubleTapMethod_m3B741A2D2B448D128D64C4A70B5E96FF77746447 ();
// 0x00000459 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetSwipeTolerance(System.Single)
extern void EasyTouch_SetSwipeTolerance_m6FCB6CA33E7EAEFF7A9CA80D87BF326DA51E9F62 ();
// 0x0000045A System.Single HedgehogTeam.EasyTouch.EasyTouch::GetSwipeTolerance()
extern void EasyTouch_GetSwipeTolerance_mEA04C3331CD6516FFEA21A924EE2C9F38FBB0E10 ();
// 0x0000045B System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnable2FingersGesture(System.Boolean)
extern void EasyTouch_SetEnable2FingersGesture_mF8D1C4BB1782A80D57E3FFE35BCCB1D25E162E28 ();
// 0x0000045C System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetEnable2FingersGesture()
extern void EasyTouch_GetEnable2FingersGesture_m7FA8C9DF21ED2B7B800202E9597182FA681D3324 ();
// 0x0000045D System.Void HedgehogTeam.EasyTouch.EasyTouch::SetTwoFingerPickMethod(HedgehogTeam.EasyTouch.EasyTouch_TwoFingerPickMethod)
extern void EasyTouch_SetTwoFingerPickMethod_m5A0222B380E3DF3FB832BEAC13A7B697D8A014D2 ();
// 0x0000045E HedgehogTeam.EasyTouch.EasyTouch_TwoFingerPickMethod HedgehogTeam.EasyTouch.EasyTouch::GetTwoFingerPickMethod()
extern void EasyTouch_GetTwoFingerPickMethod_m487A50CB29623E9D140F881262E0779F3A2FB9F5 ();
// 0x0000045F System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnablePinch(System.Boolean)
extern void EasyTouch_SetEnablePinch_m97FE6AF8236A6A86E84A75A2EFEA216A26172102 ();
// 0x00000460 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetEnablePinch()
extern void EasyTouch_GetEnablePinch_m85E34EA117DA386F71BF49AB6D991E66AB7CC4AE ();
// 0x00000461 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetMinPinchLength(System.Single)
extern void EasyTouch_SetMinPinchLength_m262017B03981C629A93B0CD7FDFB9FD99D5FF6EB ();
// 0x00000462 System.Single HedgehogTeam.EasyTouch.EasyTouch::GetMinPinchLength()
extern void EasyTouch_GetMinPinchLength_m4B9B224EA407F22AA6085BBD72E9A1FC0D8D6617 ();
// 0x00000463 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnableTwist(System.Boolean)
extern void EasyTouch_SetEnableTwist_m5F025CF421BE62026A1836F9122901D3AB90CC6D ();
// 0x00000464 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetEnableTwist()
extern void EasyTouch_GetEnableTwist_m28E312D968424062299B918E84FE7E09D306A029 ();
// 0x00000465 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetMinTwistAngle(System.Single)
extern void EasyTouch_SetMinTwistAngle_mBD36DD5F5278251CE74A5C11995C68709B0CACBB ();
// 0x00000466 System.Single HedgehogTeam.EasyTouch.EasyTouch::GetMinTwistAngle()
extern void EasyTouch_GetMinTwistAngle_mAA34620E29A3E8FDC3805D4BA3441544ACC9500A ();
// 0x00000467 System.Boolean HedgehogTeam.EasyTouch.EasyTouch::GetSecondeFingerSimulation()
extern void EasyTouch_GetSecondeFingerSimulation_m6DD72025E65BDCC8769705F3FB2354BA037C54DB ();
// 0x00000468 System.Void HedgehogTeam.EasyTouch.EasyTouch::SetSecondFingerSimulation(System.Boolean)
extern void EasyTouch_SetSecondFingerSimulation_mB4E1807A4CF6DD6C9A2D0296D9774DBF0F629326 ();
// 0x00000469 System.Int32 HedgehogTeam.EasyTouch.EasyTouchInput::TouchCount()
extern void EasyTouchInput_TouchCount_m1BE53940BF325EE91B5BF7D7E33BE40284B4655D ();
// 0x0000046A System.Int32 HedgehogTeam.EasyTouch.EasyTouchInput::getTouchCount(System.Boolean)
extern void EasyTouchInput_getTouchCount_m3892F5EA856BF840F9848FD06D1EC2112EF03C29 ();
// 0x0000046B HedgehogTeam.EasyTouch.Finger HedgehogTeam.EasyTouch.EasyTouchInput::GetMouseTouch(System.Int32,HedgehogTeam.EasyTouch.Finger)
extern void EasyTouchInput_GetMouseTouch_m9CC7DE29A1CC986941E8EAE140A26BE6811D556B ();
// 0x0000046C UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::GetSecondFingerPosition()
extern void EasyTouchInput_GetSecondFingerPosition_mC68C5BF9B51EA991DBD88B46B076144006FEF927 ();
// 0x0000046D UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::GetPointerPosition(System.Int32)
extern void EasyTouchInput_GetPointerPosition_m09D05776571C3CFFB7A45C577FF15F82532E3A4E ();
// 0x0000046E UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::GetPinchTwist2Finger(System.Boolean)
extern void EasyTouchInput_GetPinchTwist2Finger_m7567D733B56A48BD4978E713C2728FFE97398ACD ();
// 0x0000046F UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::GetComplex2finger()
extern void EasyTouchInput_GetComplex2finger_m4FAD9FD89F22A0D00C39AD3EA5B0B05991347E62 ();
// 0x00000470 System.Void HedgehogTeam.EasyTouch.EasyTouchInput::.ctor()
extern void EasyTouchInput__ctor_m31B43CF5C38D9E3FF2850632217F1C97611A0127 ();
// 0x00000471 System.Void HedgehogTeam.EasyTouch.Finger::.ctor()
extern void Finger__ctor_mB66F5DA2B6C04E7D0FEB9013FDAFAA5F38FFE941 ();
// 0x00000472 System.Object HedgehogTeam.EasyTouch.Gesture::Clone()
extern void Gesture_Clone_mE8AAE298832ABE6C48BC21498002F3140AF95F60 ();
// 0x00000473 UnityEngine.Vector3 HedgehogTeam.EasyTouch.Gesture::GetTouchToWorldPoint(System.Single)
extern void Gesture_GetTouchToWorldPoint_mCEBE61ED58EC849C47A2C093A7BD71DA650BE72F ();
// 0x00000474 UnityEngine.Vector3 HedgehogTeam.EasyTouch.Gesture::GetTouchToWorldPoint(UnityEngine.Vector3)
extern void Gesture_GetTouchToWorldPoint_m976B9A97EA151D764514B372966D71E653072BB4 ();
// 0x00000475 System.Single HedgehogTeam.EasyTouch.Gesture::GetSwipeOrDragAngle()
extern void Gesture_GetSwipeOrDragAngle_m2F365EE39BDBCE136ABD4084BFA17E62AD9EEF85 ();
// 0x00000476 UnityEngine.Vector2 HedgehogTeam.EasyTouch.Gesture::NormalizedPosition()
extern void Gesture_NormalizedPosition_m6C18796533A4088AAA243C4266568FBD563B1EBA ();
// 0x00000477 System.Boolean HedgehogTeam.EasyTouch.Gesture::IsOverUIElement()
extern void Gesture_IsOverUIElement_mA687B34D83B4834C16CF0844039D647DF8F3384F ();
// 0x00000478 System.Boolean HedgehogTeam.EasyTouch.Gesture::IsOverRectTransform(UnityEngine.RectTransform,UnityEngine.Camera)
extern void Gesture_IsOverRectTransform_m88CA1E99841A4FA33F5F26D0984E285CA2F79CC0 ();
// 0x00000479 UnityEngine.GameObject HedgehogTeam.EasyTouch.Gesture::GetCurrentFirstPickedUIElement(System.Boolean)
extern void Gesture_GetCurrentFirstPickedUIElement_m67F46284647CF6C76E8355AC5BA745CD85DF7FE7 ();
// 0x0000047A UnityEngine.GameObject HedgehogTeam.EasyTouch.Gesture::GetCurrentPickedObject(System.Boolean)
extern void Gesture_GetCurrentPickedObject_m7A51EB9A59A805659444032D910226D63AAB2D83 ();
// 0x0000047B System.Void HedgehogTeam.EasyTouch.Gesture::.ctor()
extern void Gesture__ctor_mE9DC73D42CDA0F9A3ECE9F864350B3133C4CD0A0 ();
// 0x0000047C System.Void HedgehogTeam.EasyTouch.TwoFingerGesture::ClearPickedObjectData()
extern void TwoFingerGesture_ClearPickedObjectData_mA3EF8042C8B3FE7BF585E5A19A0145E18D60E6B2 ();
// 0x0000047D System.Void HedgehogTeam.EasyTouch.TwoFingerGesture::ClearPickedUIData()
extern void TwoFingerGesture_ClearPickedUIData_m0B7DF8262E9E5FCC9B320E614722E87937D7ABC0 ();
// 0x0000047E System.Void HedgehogTeam.EasyTouch.TwoFingerGesture::.ctor()
extern void TwoFingerGesture__ctor_mC9619CF4B4951F2BF145D4F0B5915023DEA16368 ();
// 0x0000047F System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::WaitForFrame()
extern void Coroutines_WaitForFrame_mA0AA42FFF3F9A573764679D1558E5F7C2B5BA79F ();
// 0x00000480 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::WaitForSeconds(System.Single)
extern void Coroutines_WaitForSeconds_m0EB5337B82053D9EE204EC319FF70952975D4A5F ();
// 0x00000481 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::WaitUntil(Btkalman.Unity.Util.Val`1<System.Boolean>)
extern void Coroutines_WaitUntil_mC7921F8BFDBDDC27E830B8D41D7BEB6B07EE07F7 ();
// 0x00000482 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::Action(System.Action)
extern void Coroutines_Action_m2A6B8EFFEE0878FD1B00B495DD28DEF095171525 ();
// 0x00000483 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::WaitForButtonDown(System.String)
extern void Coroutines_WaitForButtonDown_m27C67642D70FC23F11F74074F2074C978AC2A879 ();
// 0x00000484 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::WaitForButtonUp(System.String)
extern void Coroutines_WaitForButtonUp_mBB542898CAFB3EC6AEDBA163637101E408899BE5 ();
// 0x00000485 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::WaitForButtonPress(System.String,Btkalman.Unity.Util.Val`1<System.Boolean>)
extern void Coroutines_WaitForButtonPress_m0328F1F1D5F0DAC5DF50E655D45493788501F806 ();
// 0x00000486 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::Race(Btkalman.Unity.Util.Val`1<System.Boolean>,System.Collections.IEnumerator[])
extern void Coroutines_Race_m76670B85A403903E28908F6A71AA9DB2B005944C ();
// 0x00000487 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::Race(System.Collections.IEnumerator[])
extern void Coroutines_Race_m4EA8E23EB30A16F8D8024C1219A3C0E2E0DB9F73 ();
// 0x00000488 System.Collections.IEnumerator Btkalman.Unity.Util.Coroutines::Chain(System.Collections.IEnumerator[])
extern void Coroutines_Chain_m87F3222221A696E5E8256CE31FC8B4665A6E6F49 ();
// 0x00000489 System.Void Btkalman.Unity.Util.Coroutines::.ctor()
extern void Coroutines__ctor_m3600C6EDA1DEF571CA83FAAB82053DE042F7C954 ();
// 0x0000048A System.Void Btkalman.Unity.Util.Coroutines::.cctor()
extern void Coroutines__cctor_m7E9488B6E6D7CA7A7A248D614DA3230200C9BAEA ();
// 0x0000048B System.Single Btkalman.Unity.Util.Floats::Quantize(System.Single,System.Single)
extern void Floats_Quantize_m0F30246CB323B5BE433CF457B05A5B6518F2E6F4 ();
// 0x0000048C System.Int32 Btkalman.Unity.Util.Floats::Compare(System.Single,System.Single)
extern void Floats_Compare_mF0B65F4315217757FB0AE141CA27B2674B9094B9 ();
// 0x0000048D System.Void Btkalman.Unity.Util.Floats::.ctor()
extern void Floats__ctor_mB50F61E0C5D86218B7714BAA33A10860019DB820 ();
// 0x0000048E T Btkalman.Unity.Util.GameObjects::GetComponentWithTagInChildren(UnityEngine.GameObject,System.String)
// 0x0000048F T Btkalman.Unity.Util.GameObjects::GetComponentWithTagInChildren(UnityEngine.Component,System.String)
// 0x00000490 System.Collections.Generic.List`1<UnityEngine.GameObject> Btkalman.Unity.Util.GameObjects::GetObjectsWithTagInDescendents(UnityEngine.GameObject,System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void GameObjects_GetObjectsWithTagInDescendents_m639E6A23D7DE8CF1CCCE2DEA45C172A3D63D5C25 ();
// 0x00000491 System.Void Btkalman.Unity.Util.GameObjects::.ctor()
extern void GameObjects__ctor_mABD7C458DD3D0CF5798EE1FCFA0CAE6E7ADAEBF0 ();
// 0x00000492 System.Void Btkalman.Unity.Util.Singleton::Awake(T,T&)
// 0x00000493 System.Void Btkalman.Unity.Util.Singleton::.ctor()
extern void Singleton__ctor_mEFAE1584B158DD2EDF56FD7FA2520380E04F6798 ();
// 0x00000494 System.Void Btkalman.Unity.Util.Val`1::.ctor()
// 0x00000495 System.Void Btkalman.Unity.Util.Val`1::.ctor(E)
// 0x00000496 System.String Btkalman.Unity.Util.Val`1::ToString()
// 0x00000497 UnityEngine.Vector3 Btkalman.Unity.Util.Vectors::Abs(UnityEngine.Vector3)
extern void Vectors_Abs_m0E280CD0217D2561AC52F7B3E2026168D0A3EFC4 ();
// 0x00000498 UnityEngine.Vector2 Btkalman.Unity.Util.Vectors::Round(UnityEngine.Vector2)
extern void Vectors_Round_m86C6EB0C43E45E3B504C3D3730737E8D7BD7FB72 ();
// 0x00000499 UnityEngine.Vector2 Btkalman.Unity.Util.Vectors::Quantize(UnityEngine.Vector2,System.Single)
extern void Vectors_Quantize_mD25AD1802EF1A9DE187A6071CE3B858455A147B9 ();
// 0x0000049A System.Int32 Btkalman.Unity.Util.Vectors::Compare(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vectors_Compare_m735595D7F044159571B9FED2766B00E5B166A1E3 ();
// 0x0000049B UnityEngine.Vector3 Btkalman.Unity.Util.Vectors::X(System.Single,UnityEngine.Vector3)
extern void Vectors_X_mBC6C45C90C043138CE002EE5BD3842FF2708D4B0 ();
// 0x0000049C UnityEngine.Vector3 Btkalman.Unity.Util.Vectors::X(System.Single)
extern void Vectors_X_m114ACA3723318C8B5760D60B9758A86379A97772 ();
// 0x0000049D UnityEngine.Vector3 Btkalman.Unity.Util.Vectors::Y(System.Single,UnityEngine.Vector3)
extern void Vectors_Y_m386845081C8B59C3FAEA849DA586BEB37555A2DF ();
// 0x0000049E UnityEngine.Vector3 Btkalman.Unity.Util.Vectors::Y(System.Single)
extern void Vectors_Y_mF7FA97E2AFDBA3B22BE9047539907F8EC3F6941D ();
// 0x0000049F UnityEngine.Vector3 Btkalman.Unity.Util.Vectors::Z(System.Single,UnityEngine.Vector3)
extern void Vectors_Z_mB1BD638C3825FE966A6CDE53E1723248FF937D33 ();
// 0x000004A0 UnityEngine.Vector3 Btkalman.Unity.Util.Vectors::Z(System.Single)
extern void Vectors_Z_m2A6C68EE929324CA95C49B04FC6D9500D9FC53E2 ();
// 0x000004A1 System.Void Btkalman.Unity.Util.Vectors::.ctor()
extern void Vectors__ctor_mC65DA9C5CBDC990DF6FB6C2A58AF886CAABFCC8C ();
// 0x000004A2 Btkalman.Cinemachine.CinemachineReset_Before Btkalman.Cinemachine.CinemachineReset::BeforeReset(System.Int32)
extern void CinemachineReset_BeforeReset_m1E3A111E56CCB75928CA2FAACCE7BA6DF0A75912 ();
// 0x000004A3 System.Void Btkalman.Cinemachine.CinemachineReset::Reset(Btkalman.Cinemachine.CinemachineReset_Before)
extern void CinemachineReset_Reset_mF6D533C9094AF28176AC6B745E19512CE67BC9CA ();
// 0x000004A4 System.Void Btkalman.Cinemachine.CinemachineReset::.ctor()
extern void CinemachineReset__ctor_m3B44817769546897220DDEFEAB667177E4012008 ();
// 0x000004A5 System.Void Btkalman.Cinemachine.CinemachineScroller::PostPipelineStageCallback(Cinemachine.CinemachineVirtualCameraBase,Cinemachine.CinemachineCore_Stage,Cinemachine.CameraState&,System.Single)
extern void CinemachineScroller_PostPipelineStageCallback_m6F21765AA9A398E0FC2834760A387DD40686FD6F ();
// 0x000004A6 System.Void Btkalman.Cinemachine.CinemachineScroller::OnCinemachineReset()
extern void CinemachineScroller_OnCinemachineReset_m7DB11DEC9E1A5446A09CC8D720D2A17D2CC501AA ();
// 0x000004A7 System.Void Btkalman.Cinemachine.CinemachineScroller::.ctor()
extern void CinemachineScroller__ctor_mEE57F9389BD7C0E144E862FDD691D0D525BC68F1 ();
// 0x000004A8 System.Void Btkalman.Cinemachine.CinemachineCameraLock::Start()
extern void CinemachineCameraLock_Start_mF1D7757EF3895E18A56E1715EA241CB5AB0269D2 ();
// 0x000004A9 System.Void Btkalman.Cinemachine.CinemachineCameraLock::PostPipelineStageCallback(Cinemachine.CinemachineVirtualCameraBase,Cinemachine.CinemachineCore_Stage,Cinemachine.CameraState&,System.Single)
extern void CinemachineCameraLock_PostPipelineStageCallback_m97B81EE4E9221DE7B4D5730D425A259438498346 ();
// 0x000004AA System.Void Btkalman.Cinemachine.CinemachineCameraLock::UpdateOrthoSize()
extern void CinemachineCameraLock_UpdateOrthoSize_m82B66A05455A15D8E1BD7B944148849EF3D9D498 ();
// 0x000004AB System.Void Btkalman.Cinemachine.CinemachineCameraLock::.ctor()
extern void CinemachineCameraLock__ctor_m4F7F997570161ACFBC5B3E93CC5970619354201F ();
// 0x000004AC System.Void Btkalman.Cinemachine.CinemachineCameraLocked::Start()
extern void CinemachineCameraLocked_Start_m85F94BBA8C742604B0A44C4A884AE9E997EF7CAD ();
// 0x000004AD System.Void Btkalman.Cinemachine.CinemachineCameraLocked::PostPipelineStageCallback(Cinemachine.CinemachineVirtualCameraBase,Cinemachine.CinemachineCore_Stage,Cinemachine.CameraState&,System.Single)
extern void CinemachineCameraLocked_PostPipelineStageCallback_m202090129AA16464C7E32E18919B4237A6960D67 ();
// 0x000004AE System.Void Btkalman.Cinemachine.CinemachineCameraLocked::UpdateOrthoSize()
extern void CinemachineCameraLocked_UpdateOrthoSize_m6695B5590EEC0C7554E72E100A3CFB5D6D2C325D ();
// 0x000004AF System.Void Btkalman.Cinemachine.CinemachineCameraLocked::.ctor()
extern void CinemachineCameraLocked__ctor_m0F96ABCD2B982793B344193C0107412594ECE104 ();
// 0x000004B0 System.Void ButtonInputUI_<ClearText>d__5::.ctor(System.Int32)
extern void U3CClearTextU3Ed__5__ctor_mDA3220E75EEEAFEF440DA9015714488192941848 ();
// 0x000004B1 System.Void ButtonInputUI_<ClearText>d__5::System.IDisposable.Dispose()
extern void U3CClearTextU3Ed__5_System_IDisposable_Dispose_mA8D07EBAC38D595050B720D9F663A6BF6D9E3ABA ();
// 0x000004B2 System.Boolean ButtonInputUI_<ClearText>d__5::MoveNext()
extern void U3CClearTextU3Ed__5_MoveNext_m95C2615DEB29A7A6A97033C17350C8E87F7457B0 ();
// 0x000004B3 System.Object ButtonInputUI_<ClearText>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClearTextU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCAE7B5E5A0EE41BE4C8FD7EAB527B097529D94AC ();
// 0x000004B4 System.Void ButtonInputUI_<ClearText>d__5::System.Collections.IEnumerator.Reset()
extern void U3CClearTextU3Ed__5_System_Collections_IEnumerator_Reset_m9F6AE8BA6C20DFB7E83D36EB397AD89F4DBA93D0 ();
// 0x000004B5 System.Object ButtonInputUI_<ClearText>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CClearTextU3Ed__5_System_Collections_IEnumerator_get_Current_m2BBFD4DC767989F6178CB2ACE66AFB2D1483E027 ();
// 0x000004B6 System.Void ButtonUIEvent_<ClearText>d__8::.ctor(System.Int32)
extern void U3CClearTextU3Ed__8__ctor_m955896B0FE659D09A669EC42B9442CEDC8438D5C ();
// 0x000004B7 System.Void ButtonUIEvent_<ClearText>d__8::System.IDisposable.Dispose()
extern void U3CClearTextU3Ed__8_System_IDisposable_Dispose_m6008363FC8044B7FF650806EC12DBDB547DA5164 ();
// 0x000004B8 System.Boolean ButtonUIEvent_<ClearText>d__8::MoveNext()
extern void U3CClearTextU3Ed__8_MoveNext_m75AEE5D44B18C9D823159ACBD50C82622657AE65 ();
// 0x000004B9 System.Object ButtonUIEvent_<ClearText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClearTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDC2547C244C668B08D127C720A77AD3608133F9 ();
// 0x000004BA System.Void ButtonUIEvent_<ClearText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CClearTextU3Ed__8_System_Collections_IEnumerator_Reset_m0D14AD5A2B1F9A696B73EB5D8B72F30242AD3AB0 ();
// 0x000004BB System.Object ButtonUIEvent_<ClearText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CClearTextU3Ed__8_System_Collections_IEnumerator_get_Current_m996F24410DC74B3B5E2AD6AB3BDE35AABDE8EACD ();
// 0x000004BC System.Void ControlUIEvent_<ClearText>d__33::.ctor(System.Int32)
extern void U3CClearTextU3Ed__33__ctor_m5A0365988303938ACAEE795AAA5A07192095F6B4 ();
// 0x000004BD System.Void ControlUIEvent_<ClearText>d__33::System.IDisposable.Dispose()
extern void U3CClearTextU3Ed__33_System_IDisposable_Dispose_m6374DA1AECBB61043EEEEE524141F251A9DDD51F ();
// 0x000004BE System.Boolean ControlUIEvent_<ClearText>d__33::MoveNext()
extern void U3CClearTextU3Ed__33_MoveNext_mBE179BFCDA03171CFF08FA4EC30FE6D3773B97F0 ();
// 0x000004BF System.Object ControlUIEvent_<ClearText>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClearTextU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0691945032A56C5A4DADAA71DD7EAD51AAD563DF ();
// 0x000004C0 System.Void ControlUIEvent_<ClearText>d__33::System.Collections.IEnumerator.Reset()
extern void U3CClearTextU3Ed__33_System_Collections_IEnumerator_Reset_m9D58F82ACC8038CDFEE022ECA40AF1DD75028D41 ();
// 0x000004C1 System.Object ControlUIEvent_<ClearText>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CClearTextU3Ed__33_System_Collections_IEnumerator_get_Current_m438282C9DFF2FE65ABFF39463ECA3FC461697CE2 ();
// 0x000004C2 System.Void ControlUIInput_<ClearText>d__13::.ctor(System.Int32)
extern void U3CClearTextU3Ed__13__ctor_m6F6345A966FFFCA707EA50CD54A05F16527AC253 ();
// 0x000004C3 System.Void ControlUIInput_<ClearText>d__13::System.IDisposable.Dispose()
extern void U3CClearTextU3Ed__13_System_IDisposable_Dispose_m9D72A00A34470E2F4581609B2B310AE87BFEC14C ();
// 0x000004C4 System.Boolean ControlUIInput_<ClearText>d__13::MoveNext()
extern void U3CClearTextU3Ed__13_MoveNext_m3DDC80790B4810B781A1D1E2AC9688E064557E0C ();
// 0x000004C5 System.Object ControlUIInput_<ClearText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClearTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA512F4FC0909096C3AD9B352FFB418979C2E46A ();
// 0x000004C6 System.Void ControlUIInput_<ClearText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CClearTextU3Ed__13_System_Collections_IEnumerator_Reset_m1BA423BF7CF076A5B68E259769566621993ADABA ();
// 0x000004C7 System.Object ControlUIInput_<ClearText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CClearTextU3Ed__13_System_Collections_IEnumerator_get_Current_m98544CA1018CADC0D96D4314FEDA0F60C17CF8A6 ();
// 0x000004C8 System.Void FPSPlayerControl_<Flash>d__18::.ctor(System.Int32)
extern void U3CFlashU3Ed__18__ctor_m2048FB7C31C0C6D2A7DF6A4F287BF455DD3640CB ();
// 0x000004C9 System.Void FPSPlayerControl_<Flash>d__18::System.IDisposable.Dispose()
extern void U3CFlashU3Ed__18_System_IDisposable_Dispose_mB0D1776BC7590DB5A82222DCB36601631CA02A81 ();
// 0x000004CA System.Boolean FPSPlayerControl_<Flash>d__18::MoveNext()
extern void U3CFlashU3Ed__18_MoveNext_mBB2F395850ADF89B54766D0CE5D9E0B0A97A2ED4 ();
// 0x000004CB System.Object FPSPlayerControl_<Flash>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlashU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40B8884F47A1E46A4E2F8DA4C5B0B7D5872E0060 ();
// 0x000004CC System.Void FPSPlayerControl_<Flash>d__18::System.Collections.IEnumerator.Reset()
extern void U3CFlashU3Ed__18_System_Collections_IEnumerator_Reset_m4BA44E5145C94EB9E9A3955CF21D639CCBC1B490 ();
// 0x000004CD System.Object FPSPlayerControl_<Flash>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CFlashU3Ed__18_System_Collections_IEnumerator_get_Current_mEFB3E854FD3C9E6145E2C6C5A38F645E346285B9 ();
// 0x000004CE System.Void FPSPlayerControl_<Reload>d__19::.ctor(System.Int32)
extern void U3CReloadU3Ed__19__ctor_m10E3352782F6806CB8A611F5FDE1D70CC53DA04E ();
// 0x000004CF System.Void FPSPlayerControl_<Reload>d__19::System.IDisposable.Dispose()
extern void U3CReloadU3Ed__19_System_IDisposable_Dispose_m6E620D2F01D4DF357832C9B16CCE57CB4D74A47C ();
// 0x000004D0 System.Boolean FPSPlayerControl_<Reload>d__19::MoveNext()
extern void U3CReloadU3Ed__19_MoveNext_m48BC1BB30AC66A929406BB203A37EB0F470BF482 ();
// 0x000004D1 System.Object FPSPlayerControl_<Reload>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReloadU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2BAA1797804457231AC6380742E2FB5354AF31A ();
// 0x000004D2 System.Void FPSPlayerControl_<Reload>d__19::System.Collections.IEnumerator.Reset()
extern void U3CReloadU3Ed__19_System_Collections_IEnumerator_Reset_m405700A0EFA4C30A525242C9C0C7648DC8CB6734 ();
// 0x000004D3 System.Object FPSPlayerControl_<Reload>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CReloadU3Ed__19_System_Collections_IEnumerator_get_Current_m1A4C65E4696C11D6847C47C3E06EDFCDEDD9CE40 ();
// 0x000004D4 System.Void TouchPadUIEvent_<ClearText>d__6::.ctor(System.Int32)
extern void U3CClearTextU3Ed__6__ctor_m6DED0FCF7AA880B7C9590BD94C727608DA4BEC11 ();
// 0x000004D5 System.Void TouchPadUIEvent_<ClearText>d__6::System.IDisposable.Dispose()
extern void U3CClearTextU3Ed__6_System_IDisposable_Dispose_mDFA6A00493A07FF09E38926274770ED3598344D2 ();
// 0x000004D6 System.Boolean TouchPadUIEvent_<ClearText>d__6::MoveNext()
extern void U3CClearTextU3Ed__6_MoveNext_mB729781387B3CB735198CF956F6FB6D3CB664602 ();
// 0x000004D7 System.Object TouchPadUIEvent_<ClearText>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClearTextU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0B322F95DA3E3C97D451902AC656544EA82D755 ();
// 0x000004D8 System.Void TouchPadUIEvent_<ClearText>d__6::System.Collections.IEnumerator.Reset()
extern void U3CClearTextU3Ed__6_System_Collections_IEnumerator_Reset_mA0855FF5B0AE86DFCD68D9596DE7ACE0F1F17E5D ();
// 0x000004D9 System.Object TouchPadUIEvent_<ClearText>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CClearTextU3Ed__6_System_Collections_IEnumerator_get_Current_m88A2C0970266795788FA7B0D610FEEB7F63ED8CB ();
// 0x000004DA System.Void ETCBase_<UpdateVirtualControl>d__78::.ctor(System.Int32)
extern void U3CUpdateVirtualControlU3Ed__78__ctor_m72AEB99A78376FDE719F0314490D72F444BB1F25 ();
// 0x000004DB System.Void ETCBase_<UpdateVirtualControl>d__78::System.IDisposable.Dispose()
extern void U3CUpdateVirtualControlU3Ed__78_System_IDisposable_Dispose_m9E12504502CC46446F2678B480040D71217A40EE ();
// 0x000004DC System.Boolean ETCBase_<UpdateVirtualControl>d__78::MoveNext()
extern void U3CUpdateVirtualControlU3Ed__78_MoveNext_m4D9AE1427823CBA07775585380EC52D74C377052 ();
// 0x000004DD System.Object ETCBase_<UpdateVirtualControl>d__78::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateVirtualControlU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD211A1E514AE8B61BAF80864B08873507F7ECDE7 ();
// 0x000004DE System.Void ETCBase_<UpdateVirtualControl>d__78::System.Collections.IEnumerator.Reset()
extern void U3CUpdateVirtualControlU3Ed__78_System_Collections_IEnumerator_Reset_m4846B823F6D048179D18213C2A534139F85BE447 ();
// 0x000004DF System.Object ETCBase_<UpdateVirtualControl>d__78::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateVirtualControlU3Ed__78_System_Collections_IEnumerator_get_Current_m24FD56720237AEFB8EC085716079768896EA67E2 ();
// 0x000004E0 System.Void ETCBase_<FixedUpdateVirtualControl>d__79::.ctor(System.Int32)
extern void U3CFixedUpdateVirtualControlU3Ed__79__ctor_m01334AB807C6D4C2B8D9B4E59DA615BC0290A4FB ();
// 0x000004E1 System.Void ETCBase_<FixedUpdateVirtualControl>d__79::System.IDisposable.Dispose()
extern void U3CFixedUpdateVirtualControlU3Ed__79_System_IDisposable_Dispose_mAB7CE5FC00CC6AFD4C442C132B8B5A2759F59674 ();
// 0x000004E2 System.Boolean ETCBase_<FixedUpdateVirtualControl>d__79::MoveNext()
extern void U3CFixedUpdateVirtualControlU3Ed__79_MoveNext_mFE61B0DD5FE4B9A313C63B7E310B9523728CA4FC ();
// 0x000004E3 System.Object ETCBase_<FixedUpdateVirtualControl>d__79::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFixedUpdateVirtualControlU3Ed__79_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31C41CFC887DB825FD2A4AF21DEE6204C3F5ADD0 ();
// 0x000004E4 System.Void ETCBase_<FixedUpdateVirtualControl>d__79::System.Collections.IEnumerator.Reset()
extern void U3CFixedUpdateVirtualControlU3Ed__79_System_Collections_IEnumerator_Reset_m1B29D91DB7AE39401EBB581033FA381439E1C981 ();
// 0x000004E5 System.Object ETCBase_<FixedUpdateVirtualControl>d__79::System.Collections.IEnumerator.get_Current()
extern void U3CFixedUpdateVirtualControlU3Ed__79_System_Collections_IEnumerator_get_Current_mA79575D57F37076AD5623B46ED8297A858A723B0 ();
// 0x000004E6 System.Void ETCButton_OnDownHandler::.ctor()
extern void OnDownHandler__ctor_m93B6A51F50BD40566C1FE58990EB375ECA21298B ();
// 0x000004E7 System.Void ETCButton_OnPressedHandler::.ctor()
extern void OnPressedHandler__ctor_m28878A4BCF5FDDBF7D980DD5817B8BC77A1BE1FB ();
// 0x000004E8 System.Void ETCButton_OnPressedValueandler::.ctor()
extern void OnPressedValueandler__ctor_m21F1922ECC349714C94B1D822D11B119F12AC6A9 ();
// 0x000004E9 System.Void ETCButton_OnUPHandler::.ctor()
extern void OnUPHandler__ctor_m2D540E1E94E75D518B5A8B0E92258E2B034C5C0A ();
// 0x000004EA System.Void ETCDPad_OnMoveStartHandler::.ctor()
extern void OnMoveStartHandler__ctor_m7FC3F602C9C087D21D27655B813C9C9764BB487E ();
// 0x000004EB System.Void ETCDPad_OnMoveHandler::.ctor()
extern void OnMoveHandler__ctor_m5244788550A80DA961111BFE124EF1F83B66CDB7 ();
// 0x000004EC System.Void ETCDPad_OnMoveSpeedHandler::.ctor()
extern void OnMoveSpeedHandler__ctor_m66027EE94B25C09247F22CAAB2611C6F9AC16F07 ();
// 0x000004ED System.Void ETCDPad_OnMoveEndHandler::.ctor()
extern void OnMoveEndHandler__ctor_mD58DCEE10D0B0445FFA56722C6450A79DA65D45D ();
// 0x000004EE System.Void ETCDPad_OnTouchStartHandler::.ctor()
extern void OnTouchStartHandler__ctor_m3842E61288A0A3B09FD01311E0448DCA6FD2D0F9 ();
// 0x000004EF System.Void ETCDPad_OnTouchUPHandler::.ctor()
extern void OnTouchUPHandler__ctor_mC8BC8BC019F03E1E163CF82998C429273C8FD460 ();
// 0x000004F0 System.Void ETCDPad_OnDownUpHandler::.ctor()
extern void OnDownUpHandler__ctor_mC2B4C61B05060626BF59BCD6F089C98D070490D5 ();
// 0x000004F1 System.Void ETCDPad_OnDownDownHandler::.ctor()
extern void OnDownDownHandler__ctor_m2D00DB8AE032082B4AB482C71E160AFAFB105FCD ();
// 0x000004F2 System.Void ETCDPad_OnDownLeftHandler::.ctor()
extern void OnDownLeftHandler__ctor_m2595FD0934555F147F355C2214516CB4DBBFDC44 ();
// 0x000004F3 System.Void ETCDPad_OnDownRightHandler::.ctor()
extern void OnDownRightHandler__ctor_mDD7E4F1CB4852FD3BEE34B63672D3BF8552EF7CD ();
// 0x000004F4 System.Void ETCDPad_OnPressUpHandler::.ctor()
extern void OnPressUpHandler__ctor_m9F29A61660E6E08C50DDA41E92F99F19BD7BBB6A ();
// 0x000004F5 System.Void ETCDPad_OnPressDownHandler::.ctor()
extern void OnPressDownHandler__ctor_m022DB37BFF63C920BADF3D75C6F93FBE82ADF8FB ();
// 0x000004F6 System.Void ETCDPad_OnPressLeftHandler::.ctor()
extern void OnPressLeftHandler__ctor_m9436AB5B62DEEB59B2AEBC412D151649182C4D26 ();
// 0x000004F7 System.Void ETCDPad_OnPressRightHandler::.ctor()
extern void OnPressRightHandler__ctor_mB28FE4675BC2A131E7D41DED06D4878E08107F54 ();
// 0x000004F8 System.Void ETCJoystick_OnMoveStartHandler::.ctor()
extern void OnMoveStartHandler__ctor_m06FE580DBAF06C7D8E649610BE79799B09F4F599 ();
// 0x000004F9 System.Void ETCJoystick_OnMoveSpeedHandler::.ctor()
extern void OnMoveSpeedHandler__ctor_m5A49A40C138DECD2F02A1006C9A1D7D04B9882B8 ();
// 0x000004FA System.Void ETCJoystick_OnMoveHandler::.ctor()
extern void OnMoveHandler__ctor_mD597C433A8D8ED1359841A25E96B4C531A4D3249 ();
// 0x000004FB System.Void ETCJoystick_OnMoveEndHandler::.ctor()
extern void OnMoveEndHandler__ctor_m385C13126EF35D57F4F15E0713A9AFD512C69A93 ();
// 0x000004FC System.Void ETCJoystick_OnTouchStartHandler::.ctor()
extern void OnTouchStartHandler__ctor_mC93695E469FAC9337878F52A60717C6BD39A4A95 ();
// 0x000004FD System.Void ETCJoystick_OnTouchUpHandler::.ctor()
extern void OnTouchUpHandler__ctor_m52D55EB72C51CD19C65937291C87D67E5DC9C6BA ();
// 0x000004FE System.Void ETCJoystick_OnDownUpHandler::.ctor()
extern void OnDownUpHandler__ctor_m8926E1E460F857FB3AF6AA9940582F74D475A694 ();
// 0x000004FF System.Void ETCJoystick_OnDownDownHandler::.ctor()
extern void OnDownDownHandler__ctor_mBBD70B2DF201E88BA5F9EBA3E3862D065FC3B1F3 ();
// 0x00000500 System.Void ETCJoystick_OnDownLeftHandler::.ctor()
extern void OnDownLeftHandler__ctor_mB1293C417209A8489F276944131CCFB055B2347A ();
// 0x00000501 System.Void ETCJoystick_OnDownRightHandler::.ctor()
extern void OnDownRightHandler__ctor_m38998CAC7CE68B4C03CE31EDDD199BF268F99B84 ();
// 0x00000502 System.Void ETCJoystick_OnPressUpHandler::.ctor()
extern void OnPressUpHandler__ctor_m2B7507FF8C4677E43D725FBF18EB3F84D196F1D5 ();
// 0x00000503 System.Void ETCJoystick_OnPressDownHandler::.ctor()
extern void OnPressDownHandler__ctor_m286C6004E40A27683328D955F3103AC7EA77E0BF ();
// 0x00000504 System.Void ETCJoystick_OnPressLeftHandler::.ctor()
extern void OnPressLeftHandler__ctor_m433BD00D145FD6688E6327A05FDBB3B4ADFBA849 ();
// 0x00000505 System.Void ETCJoystick_OnPressRightHandler::.ctor()
extern void OnPressRightHandler__ctor_m57CEDF48C7DF8B606E6F0C71D0390D88DE57F53E ();
// 0x00000506 System.Void ETCTouchPad_OnMoveStartHandler::.ctor()
extern void OnMoveStartHandler__ctor_m99E8D97B4C663510E76BFF44A2CEB03C30BF99A0 ();
// 0x00000507 System.Void ETCTouchPad_OnMoveHandler::.ctor()
extern void OnMoveHandler__ctor_m2B46243974FC1117D47B64BDBBDE1D840BF0D9A3 ();
// 0x00000508 System.Void ETCTouchPad_OnMoveSpeedHandler::.ctor()
extern void OnMoveSpeedHandler__ctor_m4F01848068DB66578C96A0C1CF0414DBD208EFC5 ();
// 0x00000509 System.Void ETCTouchPad_OnMoveEndHandler::.ctor()
extern void OnMoveEndHandler__ctor_m89B6D0C441F303A45F12F91D165DA8C791410FE0 ();
// 0x0000050A System.Void ETCTouchPad_OnTouchStartHandler::.ctor()
extern void OnTouchStartHandler__ctor_m03B47CEE0965B21BF15B1B5BF2894F636B9D4D15 ();
// 0x0000050B System.Void ETCTouchPad_OnTouchUPHandler::.ctor()
extern void OnTouchUPHandler__ctor_mE3376869AE8BD288E3752158FED9438ED0F31011 ();
// 0x0000050C System.Void ETCTouchPad_OnDownUpHandler::.ctor()
extern void OnDownUpHandler__ctor_m4B9FDDB4714B8CDB13117E282A3D982A6EF20B58 ();
// 0x0000050D System.Void ETCTouchPad_OnDownDownHandler::.ctor()
extern void OnDownDownHandler__ctor_mF5A334B5BD0F79AC56F66B46A3F8EC8E237DA188 ();
// 0x0000050E System.Void ETCTouchPad_OnDownLeftHandler::.ctor()
extern void OnDownLeftHandler__ctor_m37D06E1D25C0B4E3F255F441139E3363ED208C71 ();
// 0x0000050F System.Void ETCTouchPad_OnDownRightHandler::.ctor()
extern void OnDownRightHandler__ctor_m2B270E98D1051F436995199D5F3D86CA1269E7A0 ();
// 0x00000510 System.Void ETCTouchPad_OnPressUpHandler::.ctor()
extern void OnPressUpHandler__ctor_mCD29BE9CBE99449746234B2BB10734DF0D13837B ();
// 0x00000511 System.Void ETCTouchPad_OnPressDownHandler::.ctor()
extern void OnPressDownHandler__ctor_mB99093FB2AAAF576BDE98E90D2D4969CE9353B89 ();
// 0x00000512 System.Void ETCTouchPad_OnPressLeftHandler::.ctor()
extern void OnPressLeftHandler__ctor_m284B395FB2769D5E7D0E824A8B5184CB1171686A ();
// 0x00000513 System.Void ETCTouchPad_OnPressRightHandler::.ctor()
extern void OnPressRightHandler__ctor_m0AEF16D293378F58AEBB351DFC3FE911241FA3B4 ();
// 0x00000514 System.Void DNAStrandManager_LevelInfo::.ctor()
extern void LevelInfo__ctor_mDB0BDCF0FD16842E6497774417DA959F7DC23D2A ();
// 0x00000515 System.Void DNAStrandManager_LevelNames::.ctor()
extern void LevelNames__ctor_m275A0B502D3249CA289866E5C2A87D528721CA39 ();
// 0x00000516 System.Void TouchControlsKit.AxesBasedController_<SmoothAxisX>d__18::.ctor(System.Int32)
extern void U3CSmoothAxisXU3Ed__18__ctor_m618185CD9E6F70E6643C4AEAB0BFFFD061915E98 ();
// 0x00000517 System.Void TouchControlsKit.AxesBasedController_<SmoothAxisX>d__18::System.IDisposable.Dispose()
extern void U3CSmoothAxisXU3Ed__18_System_IDisposable_Dispose_mF61EEE6F95BBAF717FF632F37E7B3899E5EB55EC ();
// 0x00000518 System.Boolean TouchControlsKit.AxesBasedController_<SmoothAxisX>d__18::MoveNext()
extern void U3CSmoothAxisXU3Ed__18_MoveNext_m592CFB1065FB407A98735AC5D5293D156B88CBE6 ();
// 0x00000519 System.Object TouchControlsKit.AxesBasedController_<SmoothAxisX>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSmoothAxisXU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BA4D63F2F0F1555190A094391F394EB06B66AF3 ();
// 0x0000051A System.Void TouchControlsKit.AxesBasedController_<SmoothAxisX>d__18::System.Collections.IEnumerator.Reset()
extern void U3CSmoothAxisXU3Ed__18_System_Collections_IEnumerator_Reset_m29726E161437933C24C5042042DF3532F3D5E25B ();
// 0x0000051B System.Object TouchControlsKit.AxesBasedController_<SmoothAxisX>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CSmoothAxisXU3Ed__18_System_Collections_IEnumerator_get_Current_mD14F11C68C8560FC905912B63FD64F1D924D885B ();
// 0x0000051C System.Void TouchControlsKit.AxesBasedController_<SmoothAxisY>d__19::.ctor(System.Int32)
extern void U3CSmoothAxisYU3Ed__19__ctor_m7981AF0BDF92A7C236A46CBA34EB9461C7C7EA4C ();
// 0x0000051D System.Void TouchControlsKit.AxesBasedController_<SmoothAxisY>d__19::System.IDisposable.Dispose()
extern void U3CSmoothAxisYU3Ed__19_System_IDisposable_Dispose_m6A8DABBD387113F750EC02D92B15F12E4CC2F4CA ();
// 0x0000051E System.Boolean TouchControlsKit.AxesBasedController_<SmoothAxisY>d__19::MoveNext()
extern void U3CSmoothAxisYU3Ed__19_MoveNext_mB822D7F4041307B51D41A5C0A08F4699FE9E8438 ();
// 0x0000051F System.Object TouchControlsKit.AxesBasedController_<SmoothAxisY>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSmoothAxisYU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14F78430D93C99FE40D28148E449E5E94B58F969 ();
// 0x00000520 System.Void TouchControlsKit.AxesBasedController_<SmoothAxisY>d__19::System.Collections.IEnumerator.Reset()
extern void U3CSmoothAxisYU3Ed__19_System_Collections_IEnumerator_Reset_mEA1053CB3B7BD323EB8CC8D889E0EED45CA9CC49 ();
// 0x00000521 System.Object TouchControlsKit.AxesBasedController_<SmoothAxisY>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CSmoothAxisYU3Ed__19_System_Collections_IEnumerator_get_Current_m0F32EE171EF0E993630179B65904C46445198C6A ();
// 0x00000522 System.Void TouchControlsKit.TCKJoystick_<SmoothReturnRun>d__21::.ctor(System.Int32)
extern void U3CSmoothReturnRunU3Ed__21__ctor_m8BE2DAEECAC80A77CEAB0A1B52D8A25AC9563A9B ();
// 0x00000523 System.Void TouchControlsKit.TCKJoystick_<SmoothReturnRun>d__21::System.IDisposable.Dispose()
extern void U3CSmoothReturnRunU3Ed__21_System_IDisposable_Dispose_mA64ED2CA149F10BBB6EC2AC8F6CDA2B363D9E8BB ();
// 0x00000524 System.Boolean TouchControlsKit.TCKJoystick_<SmoothReturnRun>d__21::MoveNext()
extern void U3CSmoothReturnRunU3Ed__21_MoveNext_mFB7EEEF064E187AA69ABE7144CB1692EC873019C ();
// 0x00000525 System.Object TouchControlsKit.TCKJoystick_<SmoothReturnRun>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSmoothReturnRunU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85E9D1569978E421AEC0762A2F8EB5FEA50EB88D ();
// 0x00000526 System.Void TouchControlsKit.TCKJoystick_<SmoothReturnRun>d__21::System.Collections.IEnumerator.Reset()
extern void U3CSmoothReturnRunU3Ed__21_System_Collections_IEnumerator_Reset_m079D62C652BF893D609D8C3BCD7132B27DB7CD3B ();
// 0x00000527 System.Object TouchControlsKit.TCKJoystick_<SmoothReturnRun>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CSmoothReturnRunU3Ed__21_System_Collections_IEnumerator_get_Current_mE75050DFCB0F2D534669FAB6E8F2DECCAA513A34 ();
// 0x00000528 System.Void TouchControlsKit.TCKTouchpad_<UpdateEndPosition>d__6::.ctor(System.Int32)
extern void U3CUpdateEndPositionU3Ed__6__ctor_m2E5DFD43D298A84FD0D3C08D242E35447977E994 ();
// 0x00000529 System.Void TouchControlsKit.TCKTouchpad_<UpdateEndPosition>d__6::System.IDisposable.Dispose()
extern void U3CUpdateEndPositionU3Ed__6_System_IDisposable_Dispose_m3C84CB69D2AE4D95C716CF71EE91BE6A6E3A7E5F ();
// 0x0000052A System.Boolean TouchControlsKit.TCKTouchpad_<UpdateEndPosition>d__6::MoveNext()
extern void U3CUpdateEndPositionU3Ed__6_MoveNext_mBE6C2DEAB7FF3D51E91AD942A85FF81598BBE9AB ();
// 0x0000052B System.Object TouchControlsKit.TCKTouchpad_<UpdateEndPosition>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateEndPositionU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87DBA62BF66BD675A98E06E13C972D32245598A3 ();
// 0x0000052C System.Void TouchControlsKit.TCKTouchpad_<UpdateEndPosition>d__6::System.Collections.IEnumerator.Reset()
extern void U3CUpdateEndPositionU3Ed__6_System_Collections_IEnumerator_Reset_m221DA8B3070CE1D740AB50816E8FD937E5D395C4 ();
// 0x0000052D System.Object TouchControlsKit.TCKTouchpad_<UpdateEndPosition>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateEndPositionU3Ed__6_System_Collections_IEnumerator_get_Current_mB5F565ECCF06B3CE1C7D40A7D3721C9D3EB7FB9C ();
// 0x0000052E System.Void TouchControlsKit.TCKInput_<>c::.cctor()
extern void U3CU3Ec__cctor_mF775470294DE784EF2B0F128293C99F2B432ED86 ();
// 0x0000052F System.Void TouchControlsKit.TCKInput_<>c::.ctor()
extern void U3CU3Ec__ctor_m3E9F8ED5C0EA18CDE608A8F04C6839D3A8DA4DD8 ();
// 0x00000530 System.Void TouchControlsKit.TCKInput_<>c::<Awake>b__17_0(TouchControlsKit.ControllerBase)
extern void U3CU3Ec_U3CAwakeU3Eb__17_0_mEF3312FEFA1E052097F9855D8E9F6351025BE0CF ();
// 0x00000531 System.Void TouchControlsKit.TCKInput_<>c__22`1::.cctor()
// 0x00000532 System.Void TouchControlsKit.TCKInput_<>c__22`1::.ctor()
// 0x00000533 System.Boolean TouchControlsKit.TCKInput_<>c__22`1::<GetControllers>b__22_0(TouchControlsKit.ControllerBase)
// 0x00000534 System.Void TouchControlsKit.TCKInput_<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m26A23380C9645C1240D4C62E91D798349A5308DB ();
// 0x00000535 System.Void TouchControlsKit.TCKInput_<>c__DisplayClass39_0::<ShowingTouchZone>b__0(TouchControlsKit.AxesBasedController)
extern void U3CU3Ec__DisplayClass39_0_U3CShowingTouchZoneU3Eb__0_mBF29D49498DFBFCE2A794E974F0E389113BDD2D2 ();
// 0x00000536 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver::.ctor()
extern void EasyTouchReceiver__ctor_m0FD7DE3C8AA4902F49B96D9FC510B384462249BA ();
// 0x00000537 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger_<>c__DisplayClass51_0::.ctor()
extern void U3CU3Ec__DisplayClass51_0__ctor_mC30F02F1BCFB600A75B173F89950824565C20D99 ();
// 0x00000538 System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger_<>c__DisplayClass51_0::<IsRecevier4>b__0(HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver)
extern void U3CU3Ec__DisplayClass51_0_U3CIsRecevier4U3Eb__0_m9ECEBB8B08A5BB32618966BFEF440191F57D22EC ();
// 0x00000539 System.Void HedgehogTeam.EasyTouch.EasyTouchTrigger_<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mA43075E01467504518AC88636257C296968FA73C ();
// 0x0000053A System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger_<>c__DisplayClass52_0::<GetTrigger>b__0(HedgehogTeam.EasyTouch.EasyTouchTrigger_EasyTouchReceiver)
extern void U3CU3Ec__DisplayClass52_0_U3CGetTriggerU3Eb__0_mA9E174B87F32BF11AC64E27CDDF4B7D7A052CF5E ();
// 0x0000053B System.Void HedgehogTeam.EasyTouch.QuickDrag_OnDragStart::.ctor()
extern void OnDragStart__ctor_m64100962F91435FA5EE6B0211688CCC91587CA9F ();
// 0x0000053C System.Void HedgehogTeam.EasyTouch.QuickDrag_OnDrag::.ctor()
extern void OnDrag__ctor_m496CFA691DA9DAFCD3A0DC4FA667252F8F299F7A ();
// 0x0000053D System.Void HedgehogTeam.EasyTouch.QuickDrag_OnDragEnd::.ctor()
extern void OnDragEnd__ctor_m4D4538B381B6CC94D770835AA23FE26A65BEB3D7 ();
// 0x0000053E System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchEnter::.ctor()
extern void OnTouchEnter__ctor_m4F208CB7C35A93FB7ED2030CC8F116850186BD99 ();
// 0x0000053F System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchOver::.ctor()
extern void OnTouchOver__ctor_m2A5FC73B81723DFC4BB3ABA30837E516821F1E1B ();
// 0x00000540 System.Void HedgehogTeam.EasyTouch.QuickEnterOverExist_OnTouchExit::.ctor()
extern void OnTouchExit__ctor_m21CA28B78875AC8A6F5A7E4B6DA073A1A75D73CD ();
// 0x00000541 System.Void HedgehogTeam.EasyTouch.QuickLongTap_OnLongTap::.ctor()
extern void OnLongTap__ctor_mF867810A202EDA7A0FF3EF4972A951587A9CDAE5 ();
// 0x00000542 System.Void HedgehogTeam.EasyTouch.QuickPinch_OnPinchAction::.ctor()
extern void OnPinchAction__ctor_mDF38FE4D8459744EF620C5D621B13A55661B3514 ();
// 0x00000543 System.Void HedgehogTeam.EasyTouch.QuickSwipe_OnSwipeAction::.ctor()
extern void OnSwipeAction__ctor_mFE8D67947899A186D68F52399E991438F26CB417 ();
// 0x00000544 System.Void HedgehogTeam.EasyTouch.QuickTap_OnTap::.ctor()
extern void OnTap__ctor_m89E87BA3B5DECBD98E37C13E4C59E3F6434D0A31 ();
// 0x00000545 System.Void HedgehogTeam.EasyTouch.QuickTouch_OnTouch::.ctor()
extern void OnTouch__ctor_m8F8C18DD02B542B09276497F66B3648E20B51073 ();
// 0x00000546 System.Void HedgehogTeam.EasyTouch.QuickTouch_OnTouchNotOverMe::.ctor()
extern void OnTouchNotOverMe__ctor_m98385DC531DA83F83FE4D3C6CF2C5F02C38FB813 ();
// 0x00000547 System.Void HedgehogTeam.EasyTouch.QuickTwist_OnTwistAction::.ctor()
extern void OnTwistAction__ctor_m7058379FFACE430A7D8D8EFE1F92A2CF2B2B595C ();
// 0x00000548 System.Void HedgehogTeam.EasyTouch.EasyTouch_DoubleTap::Stop()
extern void DoubleTap_Stop_mC1F788AD746449E6D1A88211AF32A257E51F356A ();
// 0x00000549 System.Void HedgehogTeam.EasyTouch.EasyTouch_DoubleTap::.ctor()
extern void DoubleTap__ctor_m8EAB63864802C41C9468A27B6D5DDCC8229822DA ();
// 0x0000054A System.Void HedgehogTeam.EasyTouch.EasyTouch_PickedObject::.ctor()
extern void PickedObject__ctor_m7A31B565DC8FCC44B180ED365EBCC5EE01C247DB ();
// 0x0000054B System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchCancelHandler::.ctor(System.Object,System.IntPtr)
extern void TouchCancelHandler__ctor_m8A5864CEA52285CAA90BE7E7927454288E26FC47 ();
// 0x0000054C System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchCancelHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TouchCancelHandler_Invoke_m3E0654AB5EF797BC7B8E643139CFF55A23A04223 ();
// 0x0000054D System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TouchCancelHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TouchCancelHandler_BeginInvoke_m82D62C670BFE670A9548FD745B6A465B91F3EC0D ();
// 0x0000054E System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchCancelHandler::EndInvoke(System.IAsyncResult)
extern void TouchCancelHandler_EndInvoke_mF56C48DE4255A404F329E8CC78B2C34ADA440094 ();
// 0x0000054F System.Void HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0 ();
// 0x00000550 System.Void HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void Cancel2FingersHandler_Invoke_mE59EB97814730B0D9EBEDE009CFA2AE3AF503674 ();
// 0x00000551 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void Cancel2FingersHandler_BeginInvoke_mB52C032299723741FE1300286AEBB0F70B4203BB ();
// 0x00000552 System.Void HedgehogTeam.EasyTouch.EasyTouch_Cancel2FingersHandler::EndInvoke(System.IAsyncResult)
extern void Cancel2FingersHandler_EndInvoke_mED45756BDF7AE509762211106BDE6C7D39CBA3A4 ();
// 0x00000553 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler::.ctor(System.Object,System.IntPtr)
extern void TouchStartHandler__ctor_m8B03D7113759D0BA11A017A3766C383F16B8D268 ();
// 0x00000554 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TouchStartHandler_Invoke_m5199345EE478AFE4CB78CCD2DB270BFBEFD69569 ();
// 0x00000555 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TouchStartHandler_BeginInvoke_mA449C714833BC8D211F40D94C46AC7914A02A833 ();
// 0x00000556 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchStartHandler::EndInvoke(System.IAsyncResult)
extern void TouchStartHandler_EndInvoke_m067CE9E9F54EF1BF5F401C83B8408C15E29DE673 ();
// 0x00000557 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler::.ctor(System.Object,System.IntPtr)
extern void TouchDownHandler__ctor_m266AB85478489C46AE8B9803142E973BB9679DA8 ();
// 0x00000558 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TouchDownHandler_Invoke_m38F8C448FB66C6C901A9668669552567824015FB ();
// 0x00000559 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TouchDownHandler_BeginInvoke_mFC20835BC0AED7533F20FCB24BF8C234DF034FB3 ();
// 0x0000055A System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchDownHandler::EndInvoke(System.IAsyncResult)
extern void TouchDownHandler_EndInvoke_m070F681CD8F4A044E3E3A10D97ABC41F33600FDD ();
// 0x0000055B System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler::.ctor(System.Object,System.IntPtr)
extern void TouchUpHandler__ctor_mFCAF0BE55F280AB1DD20F6AC020DE2E3B4A1E85F ();
// 0x0000055C System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TouchUpHandler_Invoke_mECF868AB2CBCEFC664CAFCCCB75DCCC9AB30B10A ();
// 0x0000055D System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TouchUpHandler_BeginInvoke_mB672605F50F31A185867ABE3961A5B7799CC9688 ();
// 0x0000055E System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchUpHandler::EndInvoke(System.IAsyncResult)
extern void TouchUpHandler_EndInvoke_mBA5A22B4E84DACEAEC78FFC695A9F4F15F710AF4 ();
// 0x0000055F System.Void HedgehogTeam.EasyTouch.EasyTouch_SimpleTapHandler::.ctor(System.Object,System.IntPtr)
extern void SimpleTapHandler__ctor_mA55446D2630A23367F1AB45C316320798972D5D8 ();
// 0x00000560 System.Void HedgehogTeam.EasyTouch.EasyTouch_SimpleTapHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void SimpleTapHandler_Invoke_mE4C1B89D74995327C1320ADAEE347D55628B8A44 ();
// 0x00000561 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_SimpleTapHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void SimpleTapHandler_BeginInvoke_mF08B6D5FC54B2706AE155E19A6EDBF5BB3CAD501 ();
// 0x00000562 System.Void HedgehogTeam.EasyTouch.EasyTouch_SimpleTapHandler::EndInvoke(System.IAsyncResult)
extern void SimpleTapHandler_EndInvoke_mA6A1D5041FB15352046DE29EFBE0E3E6AC177BF0 ();
// 0x00000563 System.Void HedgehogTeam.EasyTouch.EasyTouch_DoubleTapHandler::.ctor(System.Object,System.IntPtr)
extern void DoubleTapHandler__ctor_mA705BD33AE0BDFF6B579A2CC7CD5F06AED6995DA ();
// 0x00000564 System.Void HedgehogTeam.EasyTouch.EasyTouch_DoubleTapHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void DoubleTapHandler_Invoke_m5AD3BAEA046E13A13FFD156E57830B40D203F0E9 ();
// 0x00000565 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_DoubleTapHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void DoubleTapHandler_BeginInvoke_mC4008A27F61138FC7C46D734C94B6BF0855A74E9 ();
// 0x00000566 System.Void HedgehogTeam.EasyTouch.EasyTouch_DoubleTapHandler::EndInvoke(System.IAsyncResult)
extern void DoubleTapHandler_EndInvoke_mEC6D9474D1B4EC5ECBAE2A29C09D330D9F984354 ();
// 0x00000567 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapStartHandler::.ctor(System.Object,System.IntPtr)
extern void LongTapStartHandler__ctor_m098BC28395EFF78EA047197050E1F80C3C6ADAA7 ();
// 0x00000568 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapStartHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void LongTapStartHandler_Invoke_mE46CFE0BE063E13F67235336CE1D71EB06664C80 ();
// 0x00000569 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_LongTapStartHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void LongTapStartHandler_BeginInvoke_m1D28143DC7E9E9C8D30BCEC25DF9D649769262F3 ();
// 0x0000056A System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapStartHandler::EndInvoke(System.IAsyncResult)
extern void LongTapStartHandler_EndInvoke_m6EFA9794C89FD9B1F6E2CA2BC064B37811A78C02 ();
// 0x0000056B System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapHandler::.ctor(System.Object,System.IntPtr)
extern void LongTapHandler__ctor_m7328B96D97AAFF38107A81E1C7421FEB2B3A4F74 ();
// 0x0000056C System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void LongTapHandler_Invoke_m526023808514C2E5B401C2CF5A3F7BAABAE00F23 ();
// 0x0000056D System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_LongTapHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void LongTapHandler_BeginInvoke_m69C9B1AD0D0ECF51A54B703F90FEADE6F36A6B50 ();
// 0x0000056E System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapHandler::EndInvoke(System.IAsyncResult)
extern void LongTapHandler_EndInvoke_mB0B4210640A93FD7928014C947F3B169BA8ED9A3 ();
// 0x0000056F System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapEndHandler::.ctor(System.Object,System.IntPtr)
extern void LongTapEndHandler__ctor_mBD858D265F74CC3CA6722FD43FE590432D488C3D ();
// 0x00000570 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapEndHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void LongTapEndHandler_Invoke_m5EF0F923B54B1CA4E20CBFB162C55511ADA12D23 ();
// 0x00000571 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_LongTapEndHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void LongTapEndHandler_BeginInvoke_m172750E4F5F13865ACBD503ED68985E86F2BB8B4 ();
// 0x00000572 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapEndHandler::EndInvoke(System.IAsyncResult)
extern void LongTapEndHandler_EndInvoke_m8F51FFBA028E464DC82E764FBB663D9ED6137B7B ();
// 0x00000573 System.Void HedgehogTeam.EasyTouch.EasyTouch_DragStartHandler::.ctor(System.Object,System.IntPtr)
extern void DragStartHandler__ctor_mE611DE0BA8B6474E1FC82793012A8735A2556D99 ();
// 0x00000574 System.Void HedgehogTeam.EasyTouch.EasyTouch_DragStartHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void DragStartHandler_Invoke_m345A5650085AED28B0E0F942E2AFF536FCBEF5AC ();
// 0x00000575 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_DragStartHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void DragStartHandler_BeginInvoke_m4D408D1A0560B5EA026E426E7B94A62DCEF1B8D8 ();
// 0x00000576 System.Void HedgehogTeam.EasyTouch.EasyTouch_DragStartHandler::EndInvoke(System.IAsyncResult)
extern void DragStartHandler_EndInvoke_m6968781233FEE4337B70FB0F60F481596C1C32F8 ();
// 0x00000577 System.Void HedgehogTeam.EasyTouch.EasyTouch_DragHandler::.ctor(System.Object,System.IntPtr)
extern void DragHandler__ctor_mAAE60BE8ECE3CD6698B8A4603D140A32CF37F2B3 ();
// 0x00000578 System.Void HedgehogTeam.EasyTouch.EasyTouch_DragHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void DragHandler_Invoke_m9206AE28FC909A74DF9EDEEF6063ABC1A025A5FA ();
// 0x00000579 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_DragHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void DragHandler_BeginInvoke_m543EE0B4A52D8C088AE5F801F581EDCE2EBB39FB ();
// 0x0000057A System.Void HedgehogTeam.EasyTouch.EasyTouch_DragHandler::EndInvoke(System.IAsyncResult)
extern void DragHandler_EndInvoke_m27A3EF1E84A0E2B12D9936F7CDC3AE2073A83D25 ();
// 0x0000057B System.Void HedgehogTeam.EasyTouch.EasyTouch_DragEndHandler::.ctor(System.Object,System.IntPtr)
extern void DragEndHandler__ctor_m6FB60688875762340B4924F4243C7B96FE21B706 ();
// 0x0000057C System.Void HedgehogTeam.EasyTouch.EasyTouch_DragEndHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void DragEndHandler_Invoke_mA3FBEABEA201D3875774C2ED31C557707314265D ();
// 0x0000057D System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_DragEndHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void DragEndHandler_BeginInvoke_mCC7FCF79D977F7C1A18CEFFDD50A4F9B794B646F ();
// 0x0000057E System.Void HedgehogTeam.EasyTouch.EasyTouch_DragEndHandler::EndInvoke(System.IAsyncResult)
extern void DragEndHandler_EndInvoke_m31E46468A75561D38660E29CBE4487BDD8138325 ();
// 0x0000057F System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeStartHandler::.ctor(System.Object,System.IntPtr)
extern void SwipeStartHandler__ctor_mA1C7F567E55CAF871CCD63DCA14C2E8478503F47 ();
// 0x00000580 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeStartHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void SwipeStartHandler_Invoke_m1B43A735CECDE54F6E06CCE6DAF3589776CAB54E ();
// 0x00000581 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_SwipeStartHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void SwipeStartHandler_BeginInvoke_mE410318EE951D1628F254BF7DDBED973DDAAF8D5 ();
// 0x00000582 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeStartHandler::EndInvoke(System.IAsyncResult)
extern void SwipeStartHandler_EndInvoke_m52EC74B0960FFABA9E199101CFCDA585F6F604AF ();
// 0x00000583 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeHandler::.ctor(System.Object,System.IntPtr)
extern void SwipeHandler__ctor_m9FCE438A3E6B33D36C45514C085D3B7936D669E1 ();
// 0x00000584 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void SwipeHandler_Invoke_m9100C4F61F5382A2F43FDE4720E4DDC23FDA38E4 ();
// 0x00000585 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_SwipeHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void SwipeHandler_BeginInvoke_m11825638F6DBCD9AC3515CE70F0C04F25C1AE638 ();
// 0x00000586 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeHandler::EndInvoke(System.IAsyncResult)
extern void SwipeHandler_EndInvoke_mB1367E380FBF0F6BDC6D31B52600E3210C873A4A ();
// 0x00000587 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeEndHandler::.ctor(System.Object,System.IntPtr)
extern void SwipeEndHandler__ctor_mFE11C0F954D62D50B1248AD31E3EDDA7BB1136B9 ();
// 0x00000588 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeEndHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void SwipeEndHandler_Invoke_m73AA0A4D33FC100D35865630417F7EB3E8F9F5DC ();
// 0x00000589 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_SwipeEndHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void SwipeEndHandler_BeginInvoke_m2599A650EFE9B5067D89477E87D5AC9774008DCF ();
// 0x0000058A System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeEndHandler::EndInvoke(System.IAsyncResult)
extern void SwipeEndHandler_EndInvoke_m452EB8FD75DFB6C5DF57250E00B32D104F476373 ();
// 0x0000058B System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858 ();
// 0x0000058C System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TouchStart2FingersHandler_Invoke_mAACDCA5A2DF676134CF27B37F1A5789B1CC2F89E ();
// 0x0000058D System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TouchStart2FingersHandler_BeginInvoke_m05EB6B424D78161B129F6249D089CC97C3B5CC80 ();
// 0x0000058E System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchStart2FingersHandler::EndInvoke(System.IAsyncResult)
extern void TouchStart2FingersHandler_EndInvoke_mCEF01C300C2D89DBFA778A88CFDE3783A5B97DCC ();
// 0x0000058F System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void TouchDown2FingersHandler__ctor_m49EB38E582B3C5C3DC92D3F30D2097CE0EAE67A2 ();
// 0x00000590 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TouchDown2FingersHandler_Invoke_m941317814BC0857A920B0386FADE544F2492CF76 ();
// 0x00000591 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TouchDown2FingersHandler_BeginInvoke_m896BA057163BD6AF0D0906CA34A9A3C30418E808 ();
// 0x00000592 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchDown2FingersHandler::EndInvoke(System.IAsyncResult)
extern void TouchDown2FingersHandler_EndInvoke_m327ACAB2FE75F046233782BB14AB8F57238EE7F0 ();
// 0x00000593 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void TouchUp2FingersHandler__ctor_m5C3ACDED6D1F762F1B92E2D392B8612389D019C6 ();
// 0x00000594 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TouchUp2FingersHandler_Invoke_m3912D4F41096A7DFFE344641690D441360C0CBE2 ();
// 0x00000595 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TouchUp2FingersHandler_BeginInvoke_mA03D342F28921A2F3928F2026D24D402A9D4736E ();
// 0x00000596 System.Void HedgehogTeam.EasyTouch.EasyTouch_TouchUp2FingersHandler::EndInvoke(System.IAsyncResult)
extern void TouchUp2FingersHandler_EndInvoke_m079312B45F77E392745695084436856558F7520F ();
// 0x00000597 System.Void HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void SimpleTap2FingersHandler__ctor_m8AA13BBCDA12B5701474963698625B52D828586D ();
// 0x00000598 System.Void HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void SimpleTap2FingersHandler_Invoke_m32EFAAF89A211AFC23F021F2672B3428AB2B7A0D ();
// 0x00000599 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void SimpleTap2FingersHandler_BeginInvoke_m9AC4F42FC4BE5D6C5EB6B7852AC1DD3DED7AD208 ();
// 0x0000059A System.Void HedgehogTeam.EasyTouch.EasyTouch_SimpleTap2FingersHandler::EndInvoke(System.IAsyncResult)
extern void SimpleTap2FingersHandler_EndInvoke_m7C66CBFC58596FC5DF595F226008EE02CCEC84FA ();
// 0x0000059B System.Void HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void DoubleTap2FingersHandler__ctor_mCF4A990BD0889F2497F5FD37DB1478EE8D5B7CC6 ();
// 0x0000059C System.Void HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void DoubleTap2FingersHandler_Invoke_m5B3971ED7784B82419ACFC92D2CEE927E8253DCD ();
// 0x0000059D System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void DoubleTap2FingersHandler_BeginInvoke_m4677F668A0CD5ADEDBE284C6A05450B10167BFF8 ();
// 0x0000059E System.Void HedgehogTeam.EasyTouch.EasyTouch_DoubleTap2FingersHandler::EndInvoke(System.IAsyncResult)
extern void DoubleTap2FingersHandler_EndInvoke_mF28EF3B94D0AC1DB210118923E1F8681F64046EA ();
// 0x0000059F System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void LongTapStart2FingersHandler__ctor_mCEAE610D6ACD375506E88B77B757791DB6E9ECBF ();
// 0x000005A0 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void LongTapStart2FingersHandler_Invoke_mBAE911610FB1C863B0DC492E1DD30AC9F5B0BA69 ();
// 0x000005A1 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void LongTapStart2FingersHandler_BeginInvoke_m7F46A2E45BBD9495167DB0DA4262790EAC8B6439 ();
// 0x000005A2 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapStart2FingersHandler::EndInvoke(System.IAsyncResult)
extern void LongTapStart2FingersHandler_EndInvoke_m34A2181AC494E5833D7B1BCE18C73FF3F822A5EA ();
// 0x000005A3 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void LongTap2FingersHandler__ctor_m4962BF8633C3CBAE28F1D75AE74CC7329E59CFF8 ();
// 0x000005A4 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void LongTap2FingersHandler_Invoke_m4090B1761026226A9AB1763B3D69CDD9A30715D7 ();
// 0x000005A5 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void LongTap2FingersHandler_BeginInvoke_mDE601F29D9353B8C30D13981B78A7C44528A5251 ();
// 0x000005A6 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTap2FingersHandler::EndInvoke(System.IAsyncResult)
extern void LongTap2FingersHandler_EndInvoke_m4E9B4DB45505D038DAD2124B66E784F1756A8F64 ();
// 0x000005A7 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void LongTapEnd2FingersHandler__ctor_m855897087EE65EFC27D178330E0CD77AEB5A08F9 ();
// 0x000005A8 System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void LongTapEnd2FingersHandler_Invoke_m42A9800B4FEFC6AFC1DB927DEAFFEE0EACC8FE74 ();
// 0x000005A9 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void LongTapEnd2FingersHandler_BeginInvoke_mED5BD5792DEA9C35795FD43A9CA7D37880C42317 ();
// 0x000005AA System.Void HedgehogTeam.EasyTouch.EasyTouch_LongTapEnd2FingersHandler::EndInvoke(System.IAsyncResult)
extern void LongTapEnd2FingersHandler_EndInvoke_mA73C7C7D673050268A615FB3442EEB311EF6778B ();
// 0x000005AB System.Void HedgehogTeam.EasyTouch.EasyTouch_TwistHandler::.ctor(System.Object,System.IntPtr)
extern void TwistHandler__ctor_m6F700DD86D8607A52FB171EC6BDE3E36F8878C62 ();
// 0x000005AC System.Void HedgehogTeam.EasyTouch.EasyTouch_TwistHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TwistHandler_Invoke_mFA9DF10B5611E7186E50CD0E0C3696B8AC28E98A ();
// 0x000005AD System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TwistHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TwistHandler_BeginInvoke_mD208630675FDF308AC4AAA366F71CF3B6651CA22 ();
// 0x000005AE System.Void HedgehogTeam.EasyTouch.EasyTouch_TwistHandler::EndInvoke(System.IAsyncResult)
extern void TwistHandler_EndInvoke_m99A8454B89E4E0ABF5215452EA2A1547D8DE9CD4 ();
// 0x000005AF System.Void HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler::.ctor(System.Object,System.IntPtr)
extern void TwistEndHandler__ctor_m23A0EAEC768D7D9F26492DE6F184B39A4BF38DDE ();
// 0x000005B0 System.Void HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void TwistEndHandler_Invoke_m40DF7C46BCC948A9D3732978EC2007E56E42C83D ();
// 0x000005B1 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void TwistEndHandler_BeginInvoke_m79A9AA16B98824222B6F8170EDEFA9381DC0D00B ();
// 0x000005B2 System.Void HedgehogTeam.EasyTouch.EasyTouch_TwistEndHandler::EndInvoke(System.IAsyncResult)
extern void TwistEndHandler_EndInvoke_m25E7BA8A28D439FB1F80E884B55B33D003E28E74 ();
// 0x000005B3 System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchInHandler::.ctor(System.Object,System.IntPtr)
extern void PinchInHandler__ctor_mDEDDC6394CD2E1163DB89BF56FA54937156B5757 ();
// 0x000005B4 System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchInHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void PinchInHandler_Invoke_mDA17E4D55307EABA6016E6BABE27737E24FF248B ();
// 0x000005B5 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_PinchInHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void PinchInHandler_BeginInvoke_mFC9D26FBBC1934F710A4DD8D7A866DBD758353B3 ();
// 0x000005B6 System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchInHandler::EndInvoke(System.IAsyncResult)
extern void PinchInHandler_EndInvoke_mA9BACF0A8141783BDDCCE0A74EF63161C1370E0F ();
// 0x000005B7 System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchOutHandler::.ctor(System.Object,System.IntPtr)
extern void PinchOutHandler__ctor_mBCCE794792E19A5CBD71D7E1E96E35B9ABEB1CAC ();
// 0x000005B8 System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchOutHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void PinchOutHandler_Invoke_m8ED8D2B43115DD0C3B42C32036C6DED3FE8E071A ();
// 0x000005B9 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_PinchOutHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void PinchOutHandler_BeginInvoke_mF0AEDFD1274A82F47D31601448583AD3BD499716 ();
// 0x000005BA System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchOutHandler::EndInvoke(System.IAsyncResult)
extern void PinchOutHandler_EndInvoke_m41B8794D85C5EAC92EABE1F525E65C88F05C332F ();
// 0x000005BB System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchEndHandler::.ctor(System.Object,System.IntPtr)
extern void PinchEndHandler__ctor_m640C03B729088E330B629E4A6975F59BDFC97941 ();
// 0x000005BC System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchEndHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void PinchEndHandler_Invoke_m485731B20ABDE4DC1BE9BFA77F79C4D7B3D3D3DF ();
// 0x000005BD System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_PinchEndHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void PinchEndHandler_BeginInvoke_m71D6877B4E86F9471EDE65FFBF575A04907769A1 ();
// 0x000005BE System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchEndHandler::EndInvoke(System.IAsyncResult)
extern void PinchEndHandler_EndInvoke_m99A2D757909CE6653E77118A722B15EA5A760E63 ();
// 0x000005BF System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchHandler::.ctor(System.Object,System.IntPtr)
extern void PinchHandler__ctor_m56455C5FFAECE920632FE4C6D67A884CADDA6B03 ();
// 0x000005C0 System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void PinchHandler_Invoke_m9AB4431B1AF8FD6DBD8636F15C78DC5743606B4E ();
// 0x000005C1 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_PinchHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void PinchHandler_BeginInvoke_mB80571B2ED9EF8A200DC5337656FCB1534C97983 ();
// 0x000005C2 System.Void HedgehogTeam.EasyTouch.EasyTouch_PinchHandler::EndInvoke(System.IAsyncResult)
extern void PinchHandler_EndInvoke_mEA49BC6F3961711E38CC2C41E65E748F5FA6680B ();
// 0x000005C3 System.Void HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void DragStart2FingersHandler__ctor_m96F1FEE595DC4B38964C2CE9FFBF2D4577802249 ();
// 0x000005C4 System.Void HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void DragStart2FingersHandler_Invoke_m6D440071D765118B6BC606C4FD4887F16EFB0832 ();
// 0x000005C5 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void DragStart2FingersHandler_BeginInvoke_m2F6DB455D12EA9B4C2001CC3C3E92FF24BCE1878 ();
// 0x000005C6 System.Void HedgehogTeam.EasyTouch.EasyTouch_DragStart2FingersHandler::EndInvoke(System.IAsyncResult)
extern void DragStart2FingersHandler_EndInvoke_m613DC46DEE642EB25FDFEEA98D8617A2C1BAF496 ();
// 0x000005C7 System.Void HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void Drag2FingersHandler__ctor_mE97E75C61AC4B4C0A4AD34A67B30259DD0EAF444 ();
// 0x000005C8 System.Void HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void Drag2FingersHandler_Invoke_m51ADF190378DCF05889668EDA6D40851FE0CDEDA ();
// 0x000005C9 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void Drag2FingersHandler_BeginInvoke_mFF7DBC9623D2CEF95E84515303FA93B4473347EF ();
// 0x000005CA System.Void HedgehogTeam.EasyTouch.EasyTouch_Drag2FingersHandler::EndInvoke(System.IAsyncResult)
extern void Drag2FingersHandler_EndInvoke_m43E4080019553DEFE8AC75E5FBC99BEAB138ABD7 ();
// 0x000005CB System.Void HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void DragEnd2FingersHandler__ctor_mE400CA66F2292D2F12A1A5B67E79ABC72ABE16D7 ();
// 0x000005CC System.Void HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void DragEnd2FingersHandler_Invoke_m5DFC66D61D025266B7B33CB760015BE89EBADCA7 ();
// 0x000005CD System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void DragEnd2FingersHandler_BeginInvoke_m8553AE3C96821A9940034C4A93FF5C0D1F0D8AEA ();
// 0x000005CE System.Void HedgehogTeam.EasyTouch.EasyTouch_DragEnd2FingersHandler::EndInvoke(System.IAsyncResult)
extern void DragEnd2FingersHandler_EndInvoke_m363EFD1716FB7D3A15ACE6D3CA786D54EE79948D ();
// 0x000005CF System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void SwipeStart2FingersHandler__ctor_m4316083F72EA7135ACFFAFE6896C4743941B5AB2 ();
// 0x000005D0 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void SwipeStart2FingersHandler_Invoke_m6D9EE88D856FD25B6212B286138CC79E7E3C37CC ();
// 0x000005D1 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void SwipeStart2FingersHandler_BeginInvoke_m35E3A0AD1954AFAFA618C95FC8074EBF826FCE3C ();
// 0x000005D2 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeStart2FingersHandler::EndInvoke(System.IAsyncResult)
extern void SwipeStart2FingersHandler_EndInvoke_mE040834B6936A3322D0301EFD40C061DE89D7AA4 ();
// 0x000005D3 System.Void HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void Swipe2FingersHandler__ctor_m041539F2580BD4669C44668741A7506D0745A5F9 ();
// 0x000005D4 System.Void HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void Swipe2FingersHandler_Invoke_m9DA4F0016C8EF0E0E9B2A4D18BC162EE1092AE30 ();
// 0x000005D5 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void Swipe2FingersHandler_BeginInvoke_m8CE2E904785EDDC2E90AFA904CAE08B2C03824CB ();
// 0x000005D6 System.Void HedgehogTeam.EasyTouch.EasyTouch_Swipe2FingersHandler::EndInvoke(System.IAsyncResult)
extern void Swipe2FingersHandler_EndInvoke_m21B72C81262EEA213409F64B5BE2ECD6A11DA5A4 ();
// 0x000005D7 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern void SwipeEnd2FingersHandler__ctor_m7CF9535201124466FC7577CB425378F9A359BB92 ();
// 0x000005D8 System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void SwipeEnd2FingersHandler_Invoke_m88827E5D7D2475E8ACDCFB3DDB3F7FCF7CB8FCA7 ();
// 0x000005D9 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void SwipeEnd2FingersHandler_BeginInvoke_m3BE8EDEB46F9E178D8320F888066EAADBA940A52 ();
// 0x000005DA System.Void HedgehogTeam.EasyTouch.EasyTouch_SwipeEnd2FingersHandler::EndInvoke(System.IAsyncResult)
extern void SwipeEnd2FingersHandler_EndInvoke_mDCE7876B48AE246E4DA1324008C1899A96065F89 ();
// 0x000005DB System.Void HedgehogTeam.EasyTouch.EasyTouch_EasyTouchIsReadyHandler::.ctor(System.Object,System.IntPtr)
extern void EasyTouchIsReadyHandler__ctor_m2E2DDD1528BF06980E810F7AD40522BA26F2433B ();
// 0x000005DC System.Void HedgehogTeam.EasyTouch.EasyTouch_EasyTouchIsReadyHandler::Invoke()
extern void EasyTouchIsReadyHandler_Invoke_m8E135C5BB4444B7F7390517AC45D0CAD88E713E4 ();
// 0x000005DD System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_EasyTouchIsReadyHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern void EasyTouchIsReadyHandler_BeginInvoke_mBDC930ECC0E2BE8A010971AE0C5BFD5AB2C43654 ();
// 0x000005DE System.Void HedgehogTeam.EasyTouch.EasyTouch_EasyTouchIsReadyHandler::EndInvoke(System.IAsyncResult)
extern void EasyTouchIsReadyHandler_EndInvoke_m24E88D84D7ED48D3DE19749FE8104A54CA9837E6 ();
// 0x000005DF System.Void HedgehogTeam.EasyTouch.EasyTouch_OverUIElementHandler::.ctor(System.Object,System.IntPtr)
extern void OverUIElementHandler__ctor_mF2C60E858E59C4535684347D35701A67E4311B32 ();
// 0x000005E0 System.Void HedgehogTeam.EasyTouch.EasyTouch_OverUIElementHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void OverUIElementHandler_Invoke_m480DF4A635B1BCE164069BE31E495EB5C4DE09B7 ();
// 0x000005E1 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_OverUIElementHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void OverUIElementHandler_BeginInvoke_m4A57619288A06B8911AA0C3C206A07EAAF7ED04C ();
// 0x000005E2 System.Void HedgehogTeam.EasyTouch.EasyTouch_OverUIElementHandler::EndInvoke(System.IAsyncResult)
extern void OverUIElementHandler_EndInvoke_m9D48D23E3316E70E77078205470AB17005032A1B ();
// 0x000005E3 System.Void HedgehogTeam.EasyTouch.EasyTouch_UIElementTouchUpHandler::.ctor(System.Object,System.IntPtr)
extern void UIElementTouchUpHandler__ctor_m23B20253F7CCDA5F9370CBDE29DEF00B983E8824 ();
// 0x000005E4 System.Void HedgehogTeam.EasyTouch.EasyTouch_UIElementTouchUpHandler::Invoke(HedgehogTeam.EasyTouch.Gesture)
extern void UIElementTouchUpHandler_Invoke_mF3085E1B47CB1DFD212B7132E87DDDB5BEBC10BB ();
// 0x000005E5 System.IAsyncResult HedgehogTeam.EasyTouch.EasyTouch_UIElementTouchUpHandler::BeginInvoke(HedgehogTeam.EasyTouch.Gesture,System.AsyncCallback,System.Object)
extern void UIElementTouchUpHandler_BeginInvoke_m33ED64D5A18D82EF897AAC158CCE9EF5ECA2836C ();
// 0x000005E6 System.Void HedgehogTeam.EasyTouch.EasyTouch_UIElementTouchUpHandler::EndInvoke(System.IAsyncResult)
extern void UIElementTouchUpHandler_EndInvoke_m075C549595AED4EA4E172AEB6889551AA1D11A13 ();
// 0x000005E7 System.Void HedgehogTeam.EasyTouch.EasyTouch_<>c::.cctor()
extern void U3CU3Ec__cctor_m121627ACF36625C38CBB53C3BC3C23F137B7307F ();
// 0x000005E8 System.Void HedgehogTeam.EasyTouch.EasyTouch_<>c::.ctor()
extern void U3CU3Ec__ctor_mDF1191B9292A9D9617C4DDDE96CF6154D3428D52 ();
// 0x000005E9 System.Boolean HedgehogTeam.EasyTouch.EasyTouch_<>c::<Start>b__221_0(HedgehogTeam.EasyTouch.ECamera)
extern void U3CU3Ec_U3CStartU3Eb__221_0_m45F036D72B2F5C4025699A2D5AAC3D604131FEDA ();
// 0x000005EA System.Void HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::.ctor(System.Int32)
extern void U3CSingleOrDoubleU3Ed__229__ctor_mB4333383948A8ECCEDFCF40641F8AC071202D7A6 ();
// 0x000005EB System.Void HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::System.IDisposable.Dispose()
extern void U3CSingleOrDoubleU3Ed__229_System_IDisposable_Dispose_m3EBA43226484372780A4A3E4F57AE2C4CA013FD1 ();
// 0x000005EC System.Boolean HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::MoveNext()
extern void U3CSingleOrDoubleU3Ed__229_MoveNext_mAE776470B126028AD92016A0072D53DBC2EE75D1 ();
// 0x000005ED System.Object HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSingleOrDoubleU3Ed__229_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF450A5AFF7C114E4ED317EB7639B4D5A29CFB0A4 ();
// 0x000005EE System.Void HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::System.Collections.IEnumerator.Reset()
extern void U3CSingleOrDoubleU3Ed__229_System_Collections_IEnumerator_Reset_mD89AA06B498871C2C49D0FFAF242D6C50C5AE8B7 ();
// 0x000005EF System.Object HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble>d__229::System.Collections.IEnumerator.get_Current()
extern void U3CSingleOrDoubleU3Ed__229_System_Collections_IEnumerator_get_Current_m5629A6E9C71B4D3EB3B2E907CC513323B7093EED ();
// 0x000005F0 System.Void HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::.ctor(System.Int32)
extern void U3CSingleOrDouble2FingersU3Ed__235__ctor_m6F57A849F84E06164D13D96D443AA002048D489E ();
// 0x000005F1 System.Void HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::System.IDisposable.Dispose()
extern void U3CSingleOrDouble2FingersU3Ed__235_System_IDisposable_Dispose_m755BCCDA011958799CED29C78AD3DB112AACE6D0 ();
// 0x000005F2 System.Boolean HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::MoveNext()
extern void U3CSingleOrDouble2FingersU3Ed__235_MoveNext_mC7F7BE0616DD863EC465F83EBA4AE9793FC95CE9 ();
// 0x000005F3 System.Object HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSingleOrDouble2FingersU3Ed__235_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D64F0A43FE4786A6D4EE28202CC3D97E2514453 ();
// 0x000005F4 System.Void HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::System.Collections.IEnumerator.Reset()
extern void U3CSingleOrDouble2FingersU3Ed__235_System_Collections_IEnumerator_Reset_m4140DEEA55AD258A932821C7387A2B66E652DFF7 ();
// 0x000005F5 System.Object HedgehogTeam.EasyTouch.EasyTouch_<SingleOrDouble2Fingers>d__235::System.Collections.IEnumerator.get_Current()
extern void U3CSingleOrDouble2FingersU3Ed__235_System_Collections_IEnumerator_get_Current_m3D57C9C2B1B7BD65CE0E861AAD4F71B6EFE85592 ();
// 0x000005F6 System.Void HedgehogTeam.EasyTouch.EasyTouch_<>c__DisplayClass240_0::.ctor()
extern void U3CU3Ec__DisplayClass240_0__ctor_mFBBFBC99C488B01EBE82089F558370E574FE5B0A ();
// 0x000005F7 System.Boolean HedgehogTeam.EasyTouch.EasyTouch_<>c__DisplayClass240_0::<RaiseEvent>b__0(HedgehogTeam.EasyTouch.Gesture)
extern void U3CU3Ec__DisplayClass240_0_U3CRaiseEventU3Eb__0_mD1F006568BE77FDE8AB9789184DD1032B0C86ADD ();
// 0x000005F8 System.Void HedgehogTeam.EasyTouch.EasyTouch_<>c__DisplayClass273_0::.ctor()
extern void U3CU3Ec__DisplayClass273_0__ctor_m7BC082D8468BEB05DEE4F8D9270F493B01B468DB ();
// 0x000005F9 System.Boolean HedgehogTeam.EasyTouch.EasyTouch_<>c__DisplayClass273_0::<RemoveCamera>b__0(HedgehogTeam.EasyTouch.ECamera)
extern void U3CU3Ec__DisplayClass273_0_U3CRemoveCameraU3Eb__0_m9304F70819BE2710B16182B9CBDCAF5B9CDAEB83 ();
// 0x000005FA System.Void Btkalman.Unity.Util.Coroutines_<WaitForFrame>d__2::.ctor(System.Int32)
extern void U3CWaitForFrameU3Ed__2__ctor_m3393A075282B64FD849E7A2B221DA7E79CFE3E21 ();
// 0x000005FB System.Void Btkalman.Unity.Util.Coroutines_<WaitForFrame>d__2::System.IDisposable.Dispose()
extern void U3CWaitForFrameU3Ed__2_System_IDisposable_Dispose_m2484D8268E7FC0EAD9E66CE241345783419387A2 ();
// 0x000005FC System.Boolean Btkalman.Unity.Util.Coroutines_<WaitForFrame>d__2::MoveNext()
extern void U3CWaitForFrameU3Ed__2_MoveNext_m6DAF7132A332341B8A1416B618A5F9E58DFDEEDB ();
// 0x000005FD System.Object Btkalman.Unity.Util.Coroutines_<WaitForFrame>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForFrameU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4152F0CCD66669658CB639EEE19C9CF9647ABF1 ();
// 0x000005FE System.Void Btkalman.Unity.Util.Coroutines_<WaitForFrame>d__2::System.Collections.IEnumerator.Reset()
extern void U3CWaitForFrameU3Ed__2_System_Collections_IEnumerator_Reset_mF1339A9E5729157C32A022EC18436A36A4A73F6F ();
// 0x000005FF System.Object Btkalman.Unity.Util.Coroutines_<WaitForFrame>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForFrameU3Ed__2_System_Collections_IEnumerator_get_Current_m831DC4474E1D23A521A92AF05B1F4BF1220263D1 ();
// 0x00000600 System.Void Btkalman.Unity.Util.Coroutines_<WaitForSeconds>d__3::.ctor(System.Int32)
extern void U3CWaitForSecondsU3Ed__3__ctor_mE2F7F3F6830CB9899B721B8F78F77C22992F3E44 ();
// 0x00000601 System.Void Btkalman.Unity.Util.Coroutines_<WaitForSeconds>d__3::System.IDisposable.Dispose()
extern void U3CWaitForSecondsU3Ed__3_System_IDisposable_Dispose_m814138890A0775A2C84BA14C61C5E29D949B65F4 ();
// 0x00000602 System.Boolean Btkalman.Unity.Util.Coroutines_<WaitForSeconds>d__3::MoveNext()
extern void U3CWaitForSecondsU3Ed__3_MoveNext_m12BCB61E2553FEBB972C09146791FAE83A36F6CC ();
// 0x00000603 System.Object Btkalman.Unity.Util.Coroutines_<WaitForSeconds>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSecondsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m923520EA704767483CC224B27CC6A6537C202392 ();
// 0x00000604 System.Void Btkalman.Unity.Util.Coroutines_<WaitForSeconds>d__3::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSecondsU3Ed__3_System_Collections_IEnumerator_Reset_m21E66D1B7747AA873C81A06BDAE9A370F2FAA4AB ();
// 0x00000605 System.Object Btkalman.Unity.Util.Coroutines_<WaitForSeconds>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSecondsU3Ed__3_System_Collections_IEnumerator_get_Current_m88647E7460AD12D4E1B9B44DF85C49FC1284529F ();
// 0x00000606 System.Void Btkalman.Unity.Util.Coroutines_<WaitUntil>d__4::.ctor(System.Int32)
extern void U3CWaitUntilU3Ed__4__ctor_mE4F7210706628A296660F0104F8F89AEE034D9D6 ();
// 0x00000607 System.Void Btkalman.Unity.Util.Coroutines_<WaitUntil>d__4::System.IDisposable.Dispose()
extern void U3CWaitUntilU3Ed__4_System_IDisposable_Dispose_mFE38B4824B8F0AFF7A4C9C8F9572BF1A3B2E87AD ();
// 0x00000608 System.Boolean Btkalman.Unity.Util.Coroutines_<WaitUntil>d__4::MoveNext()
extern void U3CWaitUntilU3Ed__4_MoveNext_m6839F902C77A3C230300411DE871FDD74A2A87C6 ();
// 0x00000609 System.Object Btkalman.Unity.Util.Coroutines_<WaitUntil>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitUntilU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01FCD29668F1E8B6BF7178F95F9D66B6E8744EF5 ();
// 0x0000060A System.Void Btkalman.Unity.Util.Coroutines_<WaitUntil>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitUntilU3Ed__4_System_Collections_IEnumerator_Reset_m505A4F0E3572045D1E25808BF450542437F399D7 ();
// 0x0000060B System.Object Btkalman.Unity.Util.Coroutines_<WaitUntil>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitUntilU3Ed__4_System_Collections_IEnumerator_get_Current_m456B75C5925B3192644C6AA21537EC1022A45763 ();
// 0x0000060C System.Void Btkalman.Unity.Util.Coroutines_<Action>d__5::.ctor(System.Int32)
extern void U3CActionU3Ed__5__ctor_m57C6F972CE5E96C22D3D09ACD4110B44CB9AC009 ();
// 0x0000060D System.Void Btkalman.Unity.Util.Coroutines_<Action>d__5::System.IDisposable.Dispose()
extern void U3CActionU3Ed__5_System_IDisposable_Dispose_m9F0851F70ED64A448845630FD49CDB1605B9F30A ();
// 0x0000060E System.Boolean Btkalman.Unity.Util.Coroutines_<Action>d__5::MoveNext()
extern void U3CActionU3Ed__5_MoveNext_m5E444CB2C06BAD1672466066EB317288C080B5B2 ();
// 0x0000060F System.Object Btkalman.Unity.Util.Coroutines_<Action>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActionU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33DEEE9308FC72C73F1375ABF4069041C0CBC74E ();
// 0x00000610 System.Void Btkalman.Unity.Util.Coroutines_<Action>d__5::System.Collections.IEnumerator.Reset()
extern void U3CActionU3Ed__5_System_Collections_IEnumerator_Reset_mF2E541796E75F31FBB38CB35D0948537E4C694D5 ();
// 0x00000611 System.Object Btkalman.Unity.Util.Coroutines_<Action>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CActionU3Ed__5_System_Collections_IEnumerator_get_Current_m2FEC24310756FEDB927A4FE2FC5598B1D0512046 ();
// 0x00000612 System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonDown>d__6::.ctor(System.Int32)
extern void U3CWaitForButtonDownU3Ed__6__ctor_m1F2B6A6557D217EBE7E71C08C44625C2666DA77F ();
// 0x00000613 System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonDown>d__6::System.IDisposable.Dispose()
extern void U3CWaitForButtonDownU3Ed__6_System_IDisposable_Dispose_mCA6ACBDC4E87EED178DDCE011C94A8EB44F07D36 ();
// 0x00000614 System.Boolean Btkalman.Unity.Util.Coroutines_<WaitForButtonDown>d__6::MoveNext()
extern void U3CWaitForButtonDownU3Ed__6_MoveNext_mD2F606638C877BED3C1B948ECBD397BE5728D6AD ();
// 0x00000615 System.Object Btkalman.Unity.Util.Coroutines_<WaitForButtonDown>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForButtonDownU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m95055A159A3C4AB10545458A3E536FB82409A40A ();
// 0x00000616 System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonDown>d__6::System.Collections.IEnumerator.Reset()
extern void U3CWaitForButtonDownU3Ed__6_System_Collections_IEnumerator_Reset_mFD08246240DE7E9F9DDD4E153D4C2753E1804C52 ();
// 0x00000617 System.Object Btkalman.Unity.Util.Coroutines_<WaitForButtonDown>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForButtonDownU3Ed__6_System_Collections_IEnumerator_get_Current_m5885AA15C4B8D979579169536C3D77945CD364C9 ();
// 0x00000618 System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonUp>d__7::.ctor(System.Int32)
extern void U3CWaitForButtonUpU3Ed__7__ctor_mFEB3F94193FE387822D93895ECF98C62AC89923C ();
// 0x00000619 System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonUp>d__7::System.IDisposable.Dispose()
extern void U3CWaitForButtonUpU3Ed__7_System_IDisposable_Dispose_m628FAB931AA88D877C54BBC9647AB27457CECB44 ();
// 0x0000061A System.Boolean Btkalman.Unity.Util.Coroutines_<WaitForButtonUp>d__7::MoveNext()
extern void U3CWaitForButtonUpU3Ed__7_MoveNext_m1BDD17ED59F2D78A5DF8F7CB3826F6EC40F65CB8 ();
// 0x0000061B System.Object Btkalman.Unity.Util.Coroutines_<WaitForButtonUp>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForButtonUpU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m170C9F2B2AE070B873D2F6E21B87EC8C61618520 ();
// 0x0000061C System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonUp>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWaitForButtonUpU3Ed__7_System_Collections_IEnumerator_Reset_mE44B583FBCC13084C41CE2F8A8CAD8827720BF57 ();
// 0x0000061D System.Object Btkalman.Unity.Util.Coroutines_<WaitForButtonUp>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForButtonUpU3Ed__7_System_Collections_IEnumerator_get_Current_m1CCEC9336C06F50A4635CA897C27AA86DE8C6A59 ();
// 0x0000061E System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonPress>d__8::.ctor(System.Int32)
extern void U3CWaitForButtonPressU3Ed__8__ctor_mEA2D6C9710B871EC60F57E763C97427666EF9A8D ();
// 0x0000061F System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonPress>d__8::System.IDisposable.Dispose()
extern void U3CWaitForButtonPressU3Ed__8_System_IDisposable_Dispose_mFAC20D0D173D853F3CAFA23A291D1FECC62B3FD8 ();
// 0x00000620 System.Boolean Btkalman.Unity.Util.Coroutines_<WaitForButtonPress>d__8::MoveNext()
extern void U3CWaitForButtonPressU3Ed__8_MoveNext_mF914F1E59610B2198544EB5E393480C06C8B26BE ();
// 0x00000621 System.Object Btkalman.Unity.Util.Coroutines_<WaitForButtonPress>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForButtonPressU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8017717E9A1985AAAC40C26E75837E05429D6EAA ();
// 0x00000622 System.Void Btkalman.Unity.Util.Coroutines_<WaitForButtonPress>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWaitForButtonPressU3Ed__8_System_Collections_IEnumerator_Reset_mA0E24EC4237453D7104BC7C6631A345924192670 ();
// 0x00000623 System.Object Btkalman.Unity.Util.Coroutines_<WaitForButtonPress>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForButtonPressU3Ed__8_System_Collections_IEnumerator_get_Current_m8500CA27747A9BD2287476259E0FE375B63C4A5E ();
// 0x00000624 System.Void Btkalman.Unity.Util.Coroutines_<Race>d__9::.ctor(System.Int32)
extern void U3CRaceU3Ed__9__ctor_mD5897831FBAC0EFA18EFFFE58B76CEF2C0B7CFF1 ();
// 0x00000625 System.Void Btkalman.Unity.Util.Coroutines_<Race>d__9::System.IDisposable.Dispose()
extern void U3CRaceU3Ed__9_System_IDisposable_Dispose_mFF5E731FB438A0EF26880367BEB752ACFFE34931 ();
// 0x00000626 System.Boolean Btkalman.Unity.Util.Coroutines_<Race>d__9::MoveNext()
extern void U3CRaceU3Ed__9_MoveNext_m0FCABF5DC99E12C78EC82F92751F14A7CCC76C12 ();
// 0x00000627 System.Object Btkalman.Unity.Util.Coroutines_<Race>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRaceU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6CE7926D7A257BEAA5B770F6592C743BC664634 ();
// 0x00000628 System.Void Btkalman.Unity.Util.Coroutines_<Race>d__9::System.Collections.IEnumerator.Reset()
extern void U3CRaceU3Ed__9_System_Collections_IEnumerator_Reset_m0A7A77B905E276593FE4DAD5F61DC3DAD5D8DF51 ();
// 0x00000629 System.Object Btkalman.Unity.Util.Coroutines_<Race>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CRaceU3Ed__9_System_Collections_IEnumerator_get_Current_mF8429966439A6E12EA1A14732E67411AA527D0F3 ();
// 0x0000062A System.Void Btkalman.Unity.Util.Coroutines_<Race>d__10::.ctor(System.Int32)
extern void U3CRaceU3Ed__10__ctor_m88BA4CE76EDAFDF372977AC34B5D33F1CFAA7FEF ();
// 0x0000062B System.Void Btkalman.Unity.Util.Coroutines_<Race>d__10::System.IDisposable.Dispose()
extern void U3CRaceU3Ed__10_System_IDisposable_Dispose_m00D231915A594C6593DDCE04C3890CD937402248 ();
// 0x0000062C System.Boolean Btkalman.Unity.Util.Coroutines_<Race>d__10::MoveNext()
extern void U3CRaceU3Ed__10_MoveNext_m777F640A1EA2F44E214A3E011588008C73AF34F1 ();
// 0x0000062D System.Object Btkalman.Unity.Util.Coroutines_<Race>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRaceU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F6153F86644F3799A3347CD43B94AC9724B4246 ();
// 0x0000062E System.Void Btkalman.Unity.Util.Coroutines_<Race>d__10::System.Collections.IEnumerator.Reset()
extern void U3CRaceU3Ed__10_System_Collections_IEnumerator_Reset_m031E318B75C13C4FB68AC8E779F525CFC4E1121B ();
// 0x0000062F System.Object Btkalman.Unity.Util.Coroutines_<Race>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CRaceU3Ed__10_System_Collections_IEnumerator_get_Current_m7B9A001C52E2E9F54AD09EC9CA97DC5ABC0596E0 ();
// 0x00000630 System.Void Btkalman.Unity.Util.Coroutines_<Chain>d__11::.ctor(System.Int32)
extern void U3CChainU3Ed__11__ctor_m1FDACA750AA275A6AB59E8A9C22AD2F95BE07876 ();
// 0x00000631 System.Void Btkalman.Unity.Util.Coroutines_<Chain>d__11::System.IDisposable.Dispose()
extern void U3CChainU3Ed__11_System_IDisposable_Dispose_mE0001334A97EA15453CD5B764D8BA8EFD065EF03 ();
// 0x00000632 System.Boolean Btkalman.Unity.Util.Coroutines_<Chain>d__11::MoveNext()
extern void U3CChainU3Ed__11_MoveNext_mFA9F832AB0732FBF15AD53CF10CD1FC7A13D14CF ();
// 0x00000633 System.Object Btkalman.Unity.Util.Coroutines_<Chain>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChainU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD31FA606FFBAA36F3DA201220D5678E0022B81F5 ();
// 0x00000634 System.Void Btkalman.Unity.Util.Coroutines_<Chain>d__11::System.Collections.IEnumerator.Reset()
extern void U3CChainU3Ed__11_System_Collections_IEnumerator_Reset_mFD06748712BB79D5B6BD97942F10F0F8F8E72B3D ();
// 0x00000635 System.Object Btkalman.Unity.Util.Coroutines_<Chain>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CChainU3Ed__11_System_Collections_IEnumerator_get_Current_m412D4EF738160DA264D6ABF98B78ABA8375A9740 ();
// 0x00000636 System.Void Btkalman.Cinemachine.CinemachineReset_Listener::OnCinemachineReset()
// 0x00000637 System.Void Btkalman.Cinemachine.CinemachineReset_Before::Reset()
// 0x00000638 System.Void Btkalman.Cinemachine.CinemachineReset_BeforeImpl::Reset()
extern void BeforeImpl_Reset_mE2931C6CD0182BEF620574489FF5DA39AE5D6820 ();
// 0x00000639 System.Void Btkalman.Cinemachine.CinemachineReset_BeforeImpl::.ctor()
extern void BeforeImpl__ctor_mD24B1B211B479982143812B7DA3C3FB9908995C0 ();
static Il2CppMethodPointer s_methodPointers[1593] = 
{
	BlueCoinActivator_Start_m0F238FD4275D6F2E0DEFA4C09804BE24B9851178,
	BlueCoinActivator_Update_mBA2F05EF2A4D37DF4C37DC6061E31BC558899970,
	BlueCoinActivator_CheckCoins_m525D5AC5E9FC0611807ABCA39DFA5F0975321FBB,
	BlueCoinActivator_CheckCombo_mE8DD0E6CDD6C27892D9C7A114980D0666D3C7BE4,
	BlueCoinActivator__ctor_m02F1523DCF4481CCF43FC1244570C205A65DEE47,
	DnaEater_Start_m4B334D21181542A02ED5A784D9C2BC8F3FE2049A,
	DnaEater_Awake_m30A4C0DF09CA4394A9481959D7BA5C40F246CC5C,
	DnaEater_Update_m05B606042E8A6DD63924826540417A8DCE8DF948,
	DnaEater_UpdateAccelleration_m509536AB3DF3751860623708F8FC40BCCFF31819,
	DnaEater_Reset_m4478500CA01C757C662B0FE44618FEFB748FA06C,
	DnaEater_CatchUp_mBCE208D516E8BD2FA58988AE9E03B5EF499C19CD,
	DnaEater__ctor_m244F03631AB082CC71C584877974D0FA4B5E657C,
	DnaEater__cctor_mF1A51388B6A1857C7FC3F65F168A0832BBABBDF5,
	DnaInfo_Start_m69B4BEAF65BABFA9C7D645670A01F152A36FFADE,
	DnaInfo_Update_mD3574A42A39A0E00D7500A0C5E1FAAD6D634558D,
	DnaInfo__ctor_mAC35537741A4533C14DDE340CEC3277F04421F05,
	DriftScript_Start_mC0B4E5ECB165811EF4DB3FB52437A746384215D5,
	DriftScript_Awake_m14E893C28259A32A7BF84A80A6555D7C052AC71F,
	DriftScript_Update_mA7F0EFAB0064BE6A6732715751B5BC24BD1D7875,
	DriftScript_UpdateDrift_mB4DAA483A395C492EA120B35BEF906521A801F59,
	DriftScript__ctor_m568DC12B68F45D1F7E6086D519C19F0339BDD868,
	MultiLayerTouch_OnEnable_m18ABECD79B51E17A47C99FDDBD8DE36071558D03,
	MultiLayerTouch_OnDestroy_mDDBC581BD0A967C81F58BE8AFC2973BBBA605AC7,
	MultiLayerTouch_On_TouchDown_m6C55CE20CD5D15742DA48368138904FC16BEA440,
	MultiLayerTouch_On_TouchUp_m0B85E5D32DAEBD9A5057E84B1B91721E63A9D15E,
	MultiLayerTouch__ctor_mBCF2164FF90D51374CB31A1E941071A3489E9671,
	MultiLayerUI_SetAutoSelect_m361FCB01D6F2A7F5989FA346EBAB59DC2D7A8AB7,
	MultiLayerUI_SetAutoUpdate_m165800131C4964DB5BB7E301F09A71E96CE8DEC4,
	MultiLayerUI_Layer1_mBF42373F089EBFA64EF745FE616DAA2575DC4D3C,
	MultiLayerUI_Layer2_m5D4B226E371E3D788682F29CA3DB006284A669CD,
	MultiLayerUI_Layer3_m196EA62000B97225FE9913286BD9BD875CF28DB9,
	MultiLayerUI__ctor_m3B167593F50A91D4E7805891FF7F6408D36A526E,
	MultiCameraTouch_OnEnable_m8F6536E4A6BE03D7F559BC1259D63B0D2C2FA852,
	MultiCameraTouch_OnDestroy_m3102045E143234B9BCDEE95598D4C92BDC87F46A,
	MultiCameraTouch_On_TouchDown_mA9D78A14AA46D3DD202222F3579F92F2E57B9865,
	MultiCameraTouch_On_TouchUp_m14DC3BC21990DC31420276D32AC25B1F49D30B07,
	MultiCameraTouch__ctor_mF021A85E26ADFB48FA07A2CA0E929E6751A4E1CA,
	MultiCameraUI_AddCamera2_m7266009223CE15DC1BE8517D12BCE99702DB88D9,
	MultiCameraUI_AddCamera3_mE24FA9A3999F37CEC164070A64F2EDF84542EAC5,
	MultiCameraUI_AddCamera_m6DD1C72806928B2845529C34018DEB6BC1EFF5EC,
	MultiCameraUI__ctor_mB0829FAE56541297AF990D22FD121280C4730B9B,
	CubeSelect_OnEnable_m38FF3BDB5F99FADEAB1A239B95AC22DB4111EEE7,
	CubeSelect_OnDestroy_m7579A2F75BCA70E772B1E7A59EA6AB64143000EA,
	CubeSelect_Start_m63DF08877C35602F8D359684E5CCED1FD837F152,
	CubeSelect_On_SimpleTap_m8D59A3851F70A44762FB189271A458E2C1CAD820,
	CubeSelect_ResteColor_m9F79E704CD139A9C583E9A8CBF7E36019B6AEB4B,
	CubeSelect__ctor_m4856483EB7019362767791BB01AB963BE12D8594,
	RTSCamera_OnEnable_m9D7E68DFA0F5C2DA6C47478287900158625E7454,
	RTSCamera_On_Twist_m6B4B1DAC2F2DE6CF48B1410CBE5C0EC078351DEA,
	RTSCamera_OnDestroy_m041494B68E6EA892ED6CE2098A7A3D55842A01E9,
	RTSCamera_On_Drag_mE0A13F5EF8F9C904F5B1E84E3A703A6B590E291A,
	RTSCamera_On_Swipe_mA5AFE6113E1EAF9D03F028FF1945648072EAF88B,
	RTSCamera_On_Pinch_m57944272EC15749D9858BF7CB64E1EB623980AD2,
	RTSCamera__ctor_m648CF44113CAE61AA43CA417786095D38F8A814A,
	Ball__ctor_mCF954B56503D505E1FC04F582DD66B086098B67A,
	BallRunPlayer_OnEnable_m5F3703E9764FB560B60E4DB30C81C4B980BB72F0,
	BallRunPlayer_OnDestroy_m1172907FFC96014959E9FBE7B3D79A870716D125,
	BallRunPlayer_Start_m37EBD7254B861A2EEEFC924C11A54014EB2EA4F8,
	BallRunPlayer_Update_m72D95FE55CE3A4F62B89F7825BDD721791A3A28F,
	BallRunPlayer_OnCollision_m9BB037D5C3C510927BF7B99998B505823628C5F8,
	BallRunPlayer_On_SwipeEnd_mE332D52086C9FEC4148B13CF71C15703EB2D8E6D,
	BallRunPlayer_StartGame_m7710E645847D54F29F20B4D66D331C81ED973B3E,
	BallRunPlayer__ctor_mE0553449C98AFC29BE54AC628371194C63EB236C,
	ThirdPersonCamera_Start_mD22E58CB725E8543D94ACDB4482FC7F9BBE0413F,
	ThirdPersonCamera_LateUpdate_m288A921D34C2A80C295C66FAAD1CC7B10E180766,
	ThirdPersonCamera__ctor_m2815DED4895A74874732CAF0A1DC9B12685B4C3A,
	LoadExamples_LoadExample_m906BE36371874F3AF3866877F97B9840FACCB230,
	LoadExamples__ctor_mC9BFFCB3C15EF18DD5660891CC852E37B1108998,
	FingerTouch_OnEnable_m01912F265459CF43B54DE69F4F86B8C299B943BA,
	FingerTouch_OnDestroy_m7E0E44AD716806E9031DC93C12683D40B8E89681,
	FingerTouch_On_Drag_m69724AC042F6F549CE7235A0E194B0093D2CC1D5,
	FingerTouch_On_Swipe_mDE31BDE9A42F50006C454999847CBF149C08DC65,
	FingerTouch_On_TouchStart_mABE14F92A47920D285F05A5701230DBD75D17DFD,
	FingerTouch_On_TouchUp_mA92E0B105DA7DE69FE71F7B2DA6882F4767238CF,
	FingerTouch_InitTouch_mEF8867656598C4B1F0DD568F40E84A96FFF435B6,
	FingerTouch_On_DoubleTap_m53329B3640EDBE72D372D85AABE4D9A02BB719F4,
	FingerTouch__ctor_m8390586A711BF6B7CEA88940442365D858FFADBB,
	MutliFingersScreenTouch_OnEnable_m3593020B513B382D67162637E82DC40E822E2995,
	MutliFingersScreenTouch_OnDestroy_m0FBD8033CE1A4837B3D03779B008983A4FCAD382,
	MutliFingersScreenTouch_On_TouchStart_m1B224F6C8DD6EE96BAD4FF8083BCDDAD41A45A3E,
	MutliFingersScreenTouch__ctor_m96C172021FEF3C41F3C6EBADD28EA970B810079A,
	DoubleTapMe_OnEnable_m872EE06ADDEB105D79B13487525D37FAC45F846B,
	DoubleTapMe_OnDisable_m76037CB13D45135541CE24679AF93D04636A2D4E,
	DoubleTapMe_OnDestroy_m30BEE6402DE0A038BA43447364D42AF959D656B1,
	DoubleTapMe_UnsubscribeEvent_m8E50FA7F72C4CC2D796678D2515E75E1B73203EE,
	DoubleTapMe_On_DoubleTap_m45EED23522CB529540FC7E7110B5485ADEA21875,
	DoubleTapMe__ctor_mA878A175EF0D361BC359DC6E46EC26843EEF1A8C,
	DragMe_OnEnable_m437FFD62DFEDBD11D4BB2629D6F0C75A3F20164B,
	DragMe_OnDisable_mA84B75EE1AC36B8EDAE6EB094161FC9892C87BB3,
	DragMe_OnDestroy_m3FB91C6A43D2969A41CB07A25B957BA03CADD467,
	DragMe_UnsubscribeEvent_m39998A2771826394CB19EA547669720820881FC4,
	DragMe_Start_m06CB9ACAB54F91A8D02F8AE7EB2A3C223F46836B,
	DragMe_On_DragStart_m1E0014AE56BDB3E1657D7F6B33A6846D667DF780,
	DragMe_On_Drag_m853C3A8F075E36E1037FA4BF1C092C95F8AEFB00,
	DragMe_On_DragEnd_m237E31706F6BDE3790BEE1696C9885852FBA8F49,
	DragMe_RandomColor_mD433EA64B058D5F12BA04D08E51987E26747A20E,
	DragMe__ctor_mCC3B3FE01046220C730DF75620531B954379BC6C,
	LongTapMe_OnEnable_mEF7776B7B6D770186604AE635C483B25F17D9DD8,
	LongTapMe_OnDisable_m3796F96D0FD4CD23894425C9AE37643C91BA99BD,
	LongTapMe_OnDestroy_mBFB30DECA7DE75FD8AF54F760ADAF62EB7291E31,
	LongTapMe_UnsubscribeEvent_m52AFC82592F269EE203BBC3F24B9BE4C52D9302B,
	LongTapMe_Start_m51AA4A13FBEEB976829FE7C8B337017292DEE6B8,
	LongTapMe_On_LongTapStart_m582CB5DBB71356BCA87BACE1DCA5DF703FA803CA,
	LongTapMe_On_LongTap_m498E5EB14213C4AF10113A3D72AF08D7E90880D1,
	LongTapMe_On_LongTapEnd_m0506B87C0198871EC64CB7DEDB8159323792A469,
	LongTapMe_RandomColor_mC9EB633FE9F73577D22DF2018EA87212D675393F,
	LongTapMe__ctor_m6496AF92C1B15512436B2D1F4359D15AC872B5DD,
	Swipe_OnEnable_m701C4E847A4ACB5E1FB8A205508F09D4056CD329,
	Swipe_OnDisable_mADBE444EF54E865F15B42EC4E0EA7D39F8739D04,
	Swipe_OnDestroy_mE2A66E32FFC35413BD9DBBD8E367CA841505F17E,
	Swipe_UnsubscribeEvent_m810C4A8E1B2F56C8EFB12AF25CE91B132C6314F2,
	Swipe_On_SwipeStart_m62DD6BD2A7B3D8011D11E8059CA0FA31C13D0432,
	Swipe_On_Swipe_m452120CE24AABC459A73658269655FE7812D0F35,
	Swipe_On_SwipeEnd_m342AD97C1B11E4D8F15D7BE76B6D34F68074AB69,
	Swipe__ctor_mEDA6F2DED2025C742AA2029AD8896DB65C3146E5,
	TapMe_OnEnable_mBA52DAF5AC43235EDC05D2E7D7C2E9A98A14D2BE,
	TapMe_OnDisable_m9D5D587ED8C43F95C9D56B2DD366B6D1C7E43A61,
	TapMe_OnDestroy_m0AEA6D037CDEAF9C4344EB4B67F5F038A8EA0F6B,
	TapMe_UnsubscribeEvent_m67DF957DE130A08F2D1E89EE78FBB699B06E5F0F,
	TapMe_On_SimpleTap_m4A36A3515E9CC944DA57E96AD8A397BA8BA9E21D,
	TapMe__ctor_m7D6391ED786D5E92CC61307825E313FF91EF631A,
	TouchMe_OnEnable_m019FE783263BF7C692E304362C259C907D36A30F,
	TouchMe_OnDisable_m524C4771561149FB7218ADD0217274B09872B9A7,
	TouchMe_OnDestroy_m4C8EBF7F62CE31A52905377B14D0BB4B8C691824,
	TouchMe_UnsubscribeEvent_m31432F200DF166090AD601C0ECA684EDC872DF88,
	TouchMe_Start_mBCD3921FEBDF16C5D0078764FE8ECBD09169C258,
	TouchMe_On_TouchStart_m5D5F18D145B06E61E82CCF2308C6D34E60BFED2C,
	TouchMe_On_TouchDown_mBB5C13F97D2B25A6ECD4E533363567EB2B467CED,
	TouchMe_On_TouchUp_mF9F8108BF9727D348733472F38FE17AA24FABF88,
	TouchMe_RandomColor_m5270C32B484B8DAB294E808117526C492EE18AD2,
	TouchMe__ctor_mCF17EBB3CE54F25951D5C5C2EC0F9296987E957B,
	PinchMe_OnEnable_m169A20FD5C16579821FDD3E54F3F495BEBAB1899,
	PinchMe_OnDisable_m6989D877303244F2CAC32135CE20CAFB40A227C0,
	PinchMe_OnDestroy_m6D243F569C1937DF68D34734978B88F33D614990,
	PinchMe_UnsubscribeEvent_mF39844375DEEF16C725171545F3865DCB149FE5E,
	PinchMe_Start_mEA98E2DBF3ECEBA71FC7BA8998CCEF96B9B938E1,
	PinchMe_On_TouchStart2Fingers_m858F891D07436DE3EA0EE8D3FC114D3B5D9C033C,
	PinchMe_On_PinchIn_mCEEBEDA7A82C3A1F314D974E43FF893E61167925,
	PinchMe_On_PinchOut_mDC5A08B5C92C4EB20EA3985643A9181F7E295302,
	PinchMe_On_PinchEnd_mE08FBCCED6AC8474F1C50A997638A77E109B5605,
	PinchMe__ctor_mC148E8F1114DEF247041BFE6ED3F981AEA686461,
	TooglePickMethodUI_SetPickMethod2Finger_mCC5D850796239A401E4A04E6FAA17111869EA930,
	TooglePickMethodUI_SetPickMethod2Averager_m296B8E0802288A26941A226A9FC89DAC4C834E96,
	TooglePickMethodUI__ctor_mB1B383DAF1018561DAD7C1184D45FB4FFCD3E663,
	TwistMe_OnEnable_mC088E661BE5C529F317B51AA0B78AEC4ACFCB869,
	TwistMe_OnDisable_mD7F7DDD38EF945CB620ED4DFC10A349530986022,
	TwistMe_OnDestroy_mB8A5F5FEF51E82322ECEF08B1B82E3967062DB3E,
	TwistMe_UnsubscribeEvent_m7B4BCF9A689F41186C0AA9F958796037608C755F,
	TwistMe_Start_mC6A0F64168BF753E5AE9E3FB1D020E5FE655EB68,
	TwistMe_On_TouchStart2Fingers_mFFF75A1B207027E1C605C279618B75A31E2FFA44,
	TwistMe_On_Twist_mE89C6E729B06C376A893C3715B174FBAB9FB40F1,
	TwistMe_On_TwistEnd_m424E01AD6EF2387A76FABFF95CBAC054AA48C24A,
	TwistMe_On_Cancel2Fingers_mFD08D8ACA10F6A61B5455F67F7AB12162450DC7A,
	TwistMe__ctor_m06744BE7272661D1B810646B384755C1A4509651,
	TwoDoubleTapMe_OnEnable_m2B11FBB5777441BE991146F63778A1A570E1D1F6,
	TwoDoubleTapMe_OnDisable_mE0C828F1C208572A1D74337F9DAAE4003E04FDFF,
	TwoDoubleTapMe_OnDestroy_m0537A4F20B25F0CCB1235D7297D264944A1230A7,
	TwoDoubleTapMe_UnsubscribeEvent_mB7E8FD6ECF721C15D9228E09C7D32984FC069657,
	TwoDoubleTapMe_On_DoubleTap2Fingers_mD4B615169C0127E93616D25DDC9A9D0D738F4F5D,
	TwoDoubleTapMe__ctor_mD2BCCE139F26114852238D88404541A3B48BD474,
	TwoDragMe_OnEnable_m4D9291A4F42D0E4CCAD0A5E0A38C9530559F97BE,
	TwoDragMe_OnDisable_m5B906CF331E3D147D171A6BFE12EB394BC26E7E8,
	TwoDragMe_OnDestroy_mD49987B836652C20657BE68EB543623FB3095834,
	TwoDragMe_UnsubscribeEvent_m8528DD558E23FE55EC234587C2578D42840D507C,
	TwoDragMe_Start_m8299AE6AFFA99BDA8D0F51E23B9EF81A3D7C4BA2,
	TwoDragMe_On_DragStart2Fingers_m8245A5D5CCC2328E5E9A85EEFDC15E93A0AA0DC5,
	TwoDragMe_On_Drag2Fingers_m3D2B364638B7B17314181E863ED647082AE4BB88,
	TwoDragMe_On_DragEnd2Fingers_m4DDE0BAF6095656A0006854527036057C6DFA3A7,
	TwoDragMe_On_Cancel2Fingers_m08BF27F6834261DCC9C1DD6D02B890FB3E7DADEB,
	TwoDragMe_RandomColor_mF539871B48852F1E6407AC872917E58CAE62C031,
	TwoDragMe__ctor_mAB9F5C5DEE23B4AFA27EE46FC1A9DE277C27A44E,
	TwoLongTapMe_OnEnable_m24553C57C72307C05864F880DE2E06DF759B5179,
	TwoLongTapMe_OnDisable_m8A597E272F4A4DCA984CF98175BD334EF6D90D4C,
	TwoLongTapMe_OnDestroy_m834BA7E34436FBD3F1168C92BC41D8C806CE4DDC,
	TwoLongTapMe_UnsubscribeEvent_mD6FD63B41BD44D2F2EA9B2F8AD3A0147A13F332E,
	TwoLongTapMe_Start_m25B7567F5274044250351D028F3413A15B68DB13,
	TwoLongTapMe_On_LongTapStart2Fingers_mC7B363620D7F512BBE58F081E55C91DE7CB043FB,
	TwoLongTapMe_On_LongTap2Fingers_mABA3459A3EF0C678FC271172441CCD50269B35A0,
	TwoLongTapMe_On_LongTapEnd2Fingers_mF5A46F9D9B02CCC4BCB1926E6B4A3D7B4043FDDA,
	TwoLongTapMe_On_Cancel2Fingers_mF5F2AB348A2221D96B083F7492F673145E660BDA,
	TwoLongTapMe_RandomColor_m2EC75C8E459CEEF167AC03C733FBD6C8661C034F,
	TwoLongTapMe__ctor_m6BAB410EEAFED8FB06624FC8E66EA9506601D434,
	TwoSwipe_OnEnable_mD01720E2E3D1FB0F5F6485FDE2E89087724160D2,
	TwoSwipe_OnDisable_m072399D0FB362E3F9BAD878F4F9D30A5BDDD48A5,
	TwoSwipe_OnDestroy_m1F1BB31AECD941C287714260116BDC48312A0020,
	TwoSwipe_UnsubscribeEvent_m621E8D3873D00BD409B92D713D525AAD377B45DE,
	TwoSwipe_On_SwipeStart2Fingers_m996F4113B4E14D998D290641321014E90F14FD79,
	TwoSwipe_On_Swipe2Fingers_mFB811F96A67015B4782939BE6FEDE357D9A69895,
	TwoSwipe_On_SwipeEnd2Fingers_mDB441678D2EEBB257D550C07493925FDCE7985A0,
	TwoSwipe__ctor_mBD0137AA0BA8843547F3646F7B7D27C26DF5B255,
	TwoTapMe_OnEnable_mE11A33FC435C98E2DD8CAC8417E8CD0C181CBF8C,
	TwoTapMe_OnDisable_m090FCAF372A993258677033A9FA146AAC24DB1D8,
	TwoTapMe_OnDestroy_mC61C7535ACB2FCC296C68513E7907F72F7BC7201,
	TwoTapMe_UnsubscribeEvent_m507D3A6FC8FEBB6E4E54A4AEBC9EB6D2EE960C59,
	TwoTapMe_On_SimpleTap2Fingers_mFD1FDE5B180135498BB293517C2E60FB82869AAF,
	TwoTapMe_RandomColor_m9B8DADD685E5A235EDBD1CADBEDB76469B8EBDA7,
	TwoTapMe__ctor_mCBA3A52FFCF5F4A399A4CE9AD115DBBF3E8BC095,
	TwoTouchMe_OnEnable_mEADF58E4D9A10CE69524FAC5F05EEB0445DD6540,
	TwoTouchMe_OnDisable_mAA27BCB9D49F63DE42CD442377ACD14743F30E82,
	TwoTouchMe_OnDestroy_m154CB1223709052D3CF256B51017D289036F770E,
	TwoTouchMe_UnsubscribeEvent_mC4B6CE024DF6A020633D5A68813AB1A4DF9BCEEA,
	TwoTouchMe_Start_m29E68CF505FCC00AF0CF622AF832E2AE063D1DEC,
	TwoTouchMe_On_TouchStart2Fingers_m80A5B62BDEA9EBB501870D3515BEA904FE6D05F7,
	TwoTouchMe_On_TouchDown2Fingers_m5F2DBFD060A4B609ADEE268FB1A00B7F6EA5FCFE,
	TwoTouchMe_On_TouchUp2Fingers_m374E3C22AC1CF2DCAE946769B796391EC0A2FA06,
	TwoTouchMe_On_Cancel2Fingers_mC1A9360BED415B5C6B1C965B678E2BE310C12511,
	TwoTouchMe_RandomColor_m085E3A052F8C8A608915FAD9944ADE5DB04FE322,
	TwoTouchMe__ctor_mEB426FAC62BB58413573FBD998C5E81F89AE94BC,
	ETWindow_OnEnable_mB5D997AA20BC14C1B49B8EF371844C5EFE5E2BE7,
	ETWindow_OnDestroy_m97C3EF2E32CD74B617F0C3A2F60CAC4B72691AEE,
	ETWindow_On_TouchStart_mF9298CD7FEA2EA435C477B1720FBC5CBB3FA7690,
	ETWindow_On_TouchDown_mC980459D3E1017A72584FF41998E9C038E8D071F,
	ETWindow__ctor_m74F0467482D00E06A07E72F73F5D6F9D0E198486,
	GlobalEasyTouchEvent_OnEnable_mF64F27C95AD693F4BA6D8A651B204D8541F22892,
	GlobalEasyTouchEvent_OnDestroy_m642CAB87ED75B3736AA952EA27A8E118C57FE3A1,
	GlobalEasyTouchEvent_On_TouchDown_mCAA00AEF37E23149523C6E28C48A56EC406E77B6,
	GlobalEasyTouchEvent_On_OverUIElement_mC8EC40CD57C00D761986053300D206D880C5D8BA,
	GlobalEasyTouchEvent_On_UIElementTouchUp_mA2848C78736BAD880D4D0D836F365AA060B1A4F5,
	GlobalEasyTouchEvent_On_TouchUp_mC4CAC1719E04B1BB2CEE6F58E51BF2E365A427C3,
	GlobalEasyTouchEvent__ctor_mF96541D57730508FB87233D4B8D723D771D83558,
	UICompatibility_SetCompatibility_m469CB267AC36BBA0FB46FAA5DF5FE93BAC664ED9,
	UICompatibility__ctor_mE0E38E77CBFDEA33A1E3632FBE804F6064E03A73,
	UIWindow_OnDrag_m0C823B17A262F9D6294E0015EB1627EE246FD591,
	UIWindow_OnPointerDown_m58491AECE96A6331EC5C91614A1F78126F189AB0,
	UIWindow__ctor_m789C7BB02C86B2D6627A8A13CC4BBECF9D0207E8,
	UIDrag_OnEnable_m53F3D1A1B1141E2A30F7633C81224DEAE59EE620,
	UIDrag_OnDestroy_mDBA34F76DF1D49FC4EAB15E75C912C9B6D1A419B,
	UIDrag_On_TouchStart_m65ECE69393864CB1BAE99547E38BB74FCDF26A90,
	UIDrag_On_TouchDown_mDA6137F376EA0D51188523F0EB7F12A57862AFEF,
	UIDrag_On_TouchUp_mB610AEAAE2080078E5035964592074FED1AA317E,
	UIDrag_On_TouchStart2Fingers_m7E4609C89923E3E57F4F319306A870E6B8003A62,
	UIDrag_On_TouchDown2Fingers_m1D3686F31D71771CF4C04E43A1AD64C0200FF598,
	UIDrag_On_TouchUp2Fingers_m9835E0ACCB3C9E978D8A642C5EEDCC8C91689ACC,
	UIDrag__ctor_mA0B84C6372221FB61265BCDA320B577BE40C2A4B,
	UIPinch_OnEnable_m63DC2215BEC9FB17C5D4C3859FE58565DA8E3239,
	UIPinch_OnDestroy_mB910E8CCBD3760031F31C9EAA2B20F0E8CDF62EE,
	UIPinch_On_Pinch_m4EE743B766948A9807B7A008DE1EDB1D7219750C,
	UIPinch__ctor_m13DB91E62E69AAFDAE6272E07AD753438853B548,
	UITwist_OnEnable_m197511549C6C8217FDE1FA2264F5A0A25B1F0FC2,
	UITwist_OnDestroy_m183699EEB9681FC54940FB2114819D819E5B05DB,
	UITwist_On_Twist_mE02A43AECF2B05D0C6DD523F15CB8DE872575D63,
	UITwist__ctor_mE6B86357BEEF6CE575A51F4163A2263C9489ACC5,
	RTS_NewSyntaxe_Start_m7952442576B46EC4B8B1F36373AA360DC6AD9C3E,
	RTS_NewSyntaxe_Update_m646B814BD0D64888F94452B9F8E3A1C7E7B33B09,
	RTS_NewSyntaxe_ResteColor_m02294425582808D35870A04643F9740EA2E46124,
	RTS_NewSyntaxe__ctor_m9A3837AAE85A4A62F5C41885CCCFFFF00C33BFD5,
	SimpleActionExample_Start_m320F01A2C4A35902AE14A720EC03E9976D03B688,
	SimpleActionExample_ChangeColor_m3F2078AFB928FFE24E82778AD1A27F6FE6C42145,
	SimpleActionExample_TimePressed_mE13BBA99D2DAC7FD390EB1602BD1DE39872A7E1A,
	SimpleActionExample_DisplaySwipeAngle_m71627B252382DA87916DC91CB6763AA5DCEAF12D,
	SimpleActionExample_ChangeText_m324A813064BD45D1EED2F3A0DEAF1E7203540C3F,
	SimpleActionExample_ResetScale_m917CFAD531B9464E384DDCC95C6F3E6793F6D24C,
	SimpleActionExample_RandomColor_m646B92284C1C46EF2D6FFFA5777895A4A0B30B31,
	SimpleActionExample__ctor_mB16BE5538D9BE220D2D569230C499C7C576DDBE3,
	ETCSetDirectActionTransform_Start_m787EC67F4B8F298F397A5EAE7881AF0593B01891,
	ETCSetDirectActionTransform__ctor_m68B806C234EB7A1ED71327AAC437510FF18AE34F,
	ButtonInputUI_Update_m37F26A6E34E20DCDA49F963CBDB0748BB31C42C7,
	ButtonInputUI_ClearText_m190A836B685076A4057231633C733728B5824ECD,
	ButtonInputUI_SetSwipeIn_m9C2B071D39C3B27E129212281F658AB35CA9212A,
	ButtonInputUI_SetSwipeOut_m4478A95FE1B1B7EA5CC4DB6ECED6BE5740045559,
	ButtonInputUI_setTimePush_mF1D0BAE7ABCD052E2A3E7571433B3F6E916FBA15,
	ButtonInputUI__ctor_m9A933B71935FA104C17FC0CEE382D6873ABFA953,
	ButtonUIEvent_Down_m7B37FBBBCDE726B1CDAEAF7B6135FF0ACD4B4C19,
	ButtonUIEvent_Up_m38C58DE413B3DB6D5FFD1AA75D4BE41ADBC6ED1F,
	ButtonUIEvent_Press_m37A8B89B6553F1A3B5275DFA130A4743993577F7,
	ButtonUIEvent_PressValue_m253AA571B4BB196C6D977C931A343BD0307472B9,
	ButtonUIEvent_ClearText_m9A37084EF52E33F49DD49D5DAD2A96D73D8FF9A6,
	ButtonUIEvent__ctor_m8351A9DADA01FCD65B9D7DEC5403815274106676,
	ControlUIEvent_Update_m355F2AFDFEAE0B244B81B06483491215C2DFCACF,
	ControlUIEvent_MoveStart_m5FE3AFA1DB231EDE515FC390CD29FE65C002B680,
	ControlUIEvent_Move_m59A9D5E093DC0924DAF4E7EDD3B90C790A544ABC,
	ControlUIEvent_MoveSpeed_m0B56C4FF9583F0CB9AE875ACB5AC306180488F65,
	ControlUIEvent_MoveEnd_m21A02FF1675D31FABDB6F20DAEFB3F2408167643,
	ControlUIEvent_TouchStart_mCDE2D3554A6CEFB7252E8D0FA9B6C209A2231892,
	ControlUIEvent_TouchUp_m5CD3169D487B66AEC27D1F5CF1A6FF28C86B8B3A,
	ControlUIEvent_DownRight_mE61123CB414ACE01CEE7B37BF4AE06C673339103,
	ControlUIEvent_DownDown_m5F88D1AED44DF493ABE0CA3700D611D7110141F3,
	ControlUIEvent_DownLeft_mB52C736B52DCD4677A2ACADE0B5CD8F40208B925,
	ControlUIEvent_DownUp_mEDA5F6357A53AC3BF2401AE2778F4690999D9E0E,
	ControlUIEvent_Right_m03F3EA8631583EFAD860AEB9D54527F3134F2E92,
	ControlUIEvent_Down_m60CEB122C0322ABF30884110E011085885EBFB8F,
	ControlUIEvent_Left_mB740ABB49DC269F76167D97041457C6B3839AFD3,
	ControlUIEvent_Up_mBE430BF8A8D0C2B55BEFE052E4D25E5B006DE6A1,
	ControlUIEvent_ClearText_mC2A10439EE2B50D983EA96004841B0B040A2873A,
	ControlUIEvent__ctor_m54824CAAC9B5EC4670995E620CD688C85B1F9361,
	ControlUIInput_Update_mCED806C8DBE5CBE5348FB78A17242F742DDDDB10,
	ControlUIInput_ClearText_m7A57694C041703FD828F146991BD5E788AF75B77,
	ControlUIInput__ctor_mED47F31F89F8E9AA22BB7D7D64D694881E82068D,
	DPadParameterUI_SetClassicalInertia_mC56C691C6A1A05518078612A9BBE5E270F8D9020,
	DPadParameterUI_SetTimePushInertia_m271E2EBA9B61407C1EA61BA75088A135D1B0B601,
	DPadParameterUI_SetClassicalTwoAxesCount_m75493FF83663DA7225F11C6746ECA00D73EF7A9C,
	DPadParameterUI_SetClassicalFourAxesCount_m2FF4FF6F06E2B27ED21329098FEA100F6BA04E08,
	DPadParameterUI_SetTimePushTwoAxesCount_m902D3B36E4B41FF116E5D32A4875BA6A6A06BC14,
	DPadParameterUI_SetTimePushFourAxesCount_m827638037CE107FF7ECFC88419149A91DB516DD0,
	DPadParameterUI__ctor_mE2E796422671F940FF2F5BC2CC89043577E6A9F1,
	FPSPlayerControl_Awake_mAB80AF9A3CB050EF07935F1F242C71E17809FA67,
	FPSPlayerControl_Update_m70AC5CF9E2C826BAEE0B696783E85F8FBF03E194,
	FPSPlayerControl_MoveStart_m0A5946E0C4AD7AE31DF3D458183F0B6ED7B41689,
	FPSPlayerControl_MoveStop_m42847790894454BF4A28F27AD75515D410301E4E,
	FPSPlayerControl_GunFire_m355430928DD1EE8A7CF4B261245C74E41B19C618,
	FPSPlayerControl_TouchPadSwipe_mEB376362C5147F593A8F9B4E1F448A3E1E007197,
	FPSPlayerControl_Flash_m29B371AED745F7D1234A6ED2562C7C7405E2E0CE,
	FPSPlayerControl_Reload_m92254D38E482EC998BCCD83FC2497DBC4FA3C398,
	FPSPlayerControl__ctor_mEAECB3EFDE0DBBBBF74AC6E8F82E6123DF5607FF,
	ImpactEffect_Start_m2851D6A982D3328CBC3EDFC6E37883DA3EB6DD03,
	ImpactEffect_Update_m27A802F075CBB32E69A30ED17B331A3D37B06064,
	ImpactEffect__ctor_m8A2F621CE36895E5D1E98186031ADC199994B7CF,
	AxisXUi_ActivateAxisX_m2CD03246D94408476A6519DA8C3C3B3A19DABB96,
	AxisXUi_InvertedAxisX_m2CACCF93264FBB63D38D7DC00A2E5611694F7FD3,
	AxisXUi_DeadAxisX_m611EF3AE6005B4D73E93BEF548D70A5FED5E7546,
	AxisXUi_SpeedAxisX_mD3A276A758E8556C3870D520CCD5FFF91F2B6CFE,
	AxisXUi_IsInertiaX_mA7454219216A27467B2C73A36B9B6379FB67FCBD,
	AxisXUi_InertiaSpeedX_mDFB8D410260146841B07FFAE4AD4B6E3FF583D95,
	AxisXUi_ActivateAxisY_mC6CF5790DFF354E7EFEF0036E9D2D499D41987ED,
	AxisXUi_InvertedAxisY_m4A693E3BEAEA2ED92C37724CC17D6B0F01B1FD14,
	AxisXUi_DeadAxisY_m21A272462C85CF2B165A58782F3C5AE585FA2565,
	AxisXUi_SpeedAxisY_mE53C5CA0509E084511BCC33BD2A0845D51CE305A,
	AxisXUi_IsInertiaY_mF97C07B12CDE7AB761EC560735785947DAB7F691,
	AxisXUi_InertiaSpeedY_m287AFE40BA971185350CEA95E35A911924EA4C6C,
	AxisXUi__ctor_m77FEB3F0312C697AAFEC4491B3DD141343F880E3,
	LoadLevelScript_LoadMainMenu_m70C1C19730191EFBE232CD94CEAC04D233FD5F51,
	LoadLevelScript_LoadJoystickEvent_mDF330EC86C89AE3CA5382B8B466D3BC43F2D36F8,
	LoadLevelScript_LoadJoysticParameter_mD34065AB8E652E00811231FDAE32F0D56A3BAF67,
	LoadLevelScript_LoadDPadEvent_m87EBE9820724F667C4FD34697642E6E81B021481,
	LoadLevelScript_LoadDPadClassicalTime_m67AAE44F180BEBCC06C907B8ECE180A5A217E833,
	LoadLevelScript_LoadTouchPad_m6E330A7B07F8D9B891ABEB72361F872BDC8A50FE,
	LoadLevelScript_LoadButton_mE19C26B4CD1260C366A0FC51A3C0F676B25CDFAA,
	LoadLevelScript_LoadFPS_mDAF10CD6D6EC9476D4D26E46F7CFA565896FF521,
	LoadLevelScript_LoadThird_m6408479A75EDD4139A3003161056D0A785707DB2,
	LoadLevelScript_LoadThirddungeon_mC8C19490D26AA28BF522FB776B82A5FE0E4C8938,
	LoadLevelScript__ctor_m6A3AD55CE73FC214BE20B4FA990C4C5C9DDA4162,
	TouchPadUIEvent_TouchDown_m38CA3BCC215E0DC95B158C8D3BC97225745612EC,
	TouchPadUIEvent_TouchEvt_mF9416496C893E0C4E328770513D50B3CC0E77890,
	TouchPadUIEvent_TouchUp_m591BBF133B55261F98EAC73AEC2EB8F548914682,
	TouchPadUIEvent_ClearText_m2F56C2CD146F74128BF43D33E6FEC5D879E28374,
	TouchPadUIEvent__ctor_m3E1BD8B6B27E6C46D8010981FB3EDEE2210A562B,
	CharacterAnimation_Start_mEB7C3AD926CC597F6A8FD95735DF7B22D8FF5E06,
	CharacterAnimation_LateUpdate_m123CBF299A51B1C3BF2413B9CDA5F37AAABCE424,
	CharacterAnimation__ctor_m02CB4BD3E568E4A191B854F1916BFFFEC376CBEB,
	CharacterAnimationDungeon_Start_mF1A5028ABD6C34021DFDD9F44AEE2C494A25B9CE,
	CharacterAnimationDungeon_LateUpdate_mC7DA1C93E92A47DEC60BA592AF97C79AFAA0A15E,
	CharacterAnimationDungeon__ctor_m3679F4E917AA81FAC54B7F0E57F02E783E134436,
	SliderText_SetText_mFC24AC2F4A8A089AE8251C97A0F7925F6971990B,
	SliderText__ctor_mFC54ECE8C5F931E8DE1196A6395B8E2B7E7E8742,
	ComponentExtensions_rectTransform_mF088C3344E063DCA92A493483D0BD87F77DAF423,
	ComponentExtensions_Remap_mC4490DF86B8396580E71E523AC4D690A068A0075,
	ETCArea__ctor_m6E0601AC796B33B11564FF2E95097E2B550A3C59,
	ETCArea_Awake_m9B60FB2516B3218F290FF9B44724604DFD04160F,
	ETCArea_ApplyPreset_m6ABAE8611FBEB423D4581216CD5B9EC28C92A000,
	ETCAxis_get_directTransform_m81A3C04FA49EB7F28D4E29AD240E639578DA37E1,
	ETCAxis_set_directTransform_m49C57210686A1E7D958315C6DBEB0F819C0EB184,
	ETCAxis__ctor_m05B005D0EC30FCB465468B4A2787B57ABE1B5436,
	ETCAxis_InitAxis_m1068815F1388637523F3C91D223D11B5E8565163,
	ETCAxis_UpdateAxis_m0402F3C31F4BFDD0351CE8586858595DC2720AA6,
	ETCAxis_UpdateButton_mC242858FC4CA302E0A2FCACB1BD26889974C1C1F,
	ETCAxis_ResetAxis_mFFCD5D21A0C6155E4610782B479ADC72ECD977DF,
	ETCAxis_DoDirectAction_m36FDFC94ADF1E16A46A1A3CEF96E9E725A2A4F3D,
	ETCAxis_DoGravity_m220F56A3B663AF2E0268D406600B2BE4D7B55EA9,
	ETCAxis_ComputAxisValue_m1676411BB915BF9F2E02FDBFD6D82E8D1FCCA0FF,
	ETCAxis_GetInfluencedAxis_mA98AF53336C3C9053188E085BCC989EFF3B5921A,
	ETCAxis_GetAngle_mE1666B74F75D34558B26CEB22530BE605B22CEA3,
	ETCAxis_DoAutoStabilisation_m44449B1C5961AB6B5CB37A4516B8D147BC7ECA7F,
	ETCAxis_DoAngleLimitation_mE3A4668835BD4EC44969794B3748575B2464CC70,
	ETCAxis_InitDeadCurve_m792C52B4C1DC170EB2744EB38BF1386F2E434777,
	ETCBase_get_anchor_m5264815ABD81F40475A94DD8F22200966A6F5671,
	ETCBase_set_anchor_mAC12D428C2AD7202847E23C1874419F6D2DB8F37,
	ETCBase_get_anchorOffet_m45F590C803B5428363B2C991E4A5D6DD3F29EC23,
	ETCBase_set_anchorOffet_mFB2A1DD8C7A014BA8F4C75995DE57432D1DD0414,
	ETCBase_get_visible_m046CD11BFF4A77021DA78B68311DEE6EA0EE5AD3,
	ETCBase_set_visible_m00B2525EFBB28BC9EC0BA0280AB4F989B64C88EC,
	ETCBase_get_activated_mDC5DD764F14B57B3BE726B3F322B7D3F05CF3C5C,
	ETCBase_set_activated_m8D4CAE0E6EDDDBA21BB0521497A000C6D89F4945,
	ETCBase_Awake_mCE1D1FF2791F9453A6FCAD89188A875A905AC7FE,
	ETCBase_Start_m2B249AA08A8782BB725C038EA32E3080505A1BAD,
	ETCBase_OnEnable_m72AC9287FD616176EDAA65852FA20CA0EEC67F50,
	ETCBase_OnDisable_m003F87BCA1B9AC396423E49B7F66A582EC2AD131,
	ETCBase_OnDestroy_mB8AB4D7B7F75A76F8A7BE3C4DAD08B7AB2D15AE5,
	ETCBase_Update_m71F89C1AE64F58D8F069C1BBF2E26221A0EFBF48,
	ETCBase_FixedUpdate_mF87BEE97E4E5B698A2180F0581BD23B66A0492A1,
	ETCBase_LateUpdate_m010C5F0B0D7244A84AB0CD66C1E154D7BAAB6E1B,
	ETCBase_UpdateControlState_m2AACCB711D87DAF6AE02DE8C53BBC50C8456391F,
	ETCBase_SetVisible_mD93DBB871AA408D8E755201768ED21D08CCF0582,
	ETCBase_SetActivated_mDA97ED22B2F86E7B52D95830D16E74D297968D7A,
	ETCBase_SetAnchorPosition_m9A2C462A0F74849219E5F68DA9949A3634510939,
	ETCBase_GetFirstUIElement_m376A97048D47A0C5194ADB7001A370200BD28831,
	ETCBase_CameraSmoothFollow_m9C1431B401B4187E232F804760F5DF6D3C29CD33,
	ETCBase_CameraFollow_m392819CCD54211D83B191F849FF8669F1178D2C2,
	ETCBase_UpdateVirtualControl_m907E0DF0C53C330B03D59E3B6DE63DDB45DC8D98,
	ETCBase_FixedUpdateVirtualControl_m60E3C0834D3EC40F0C7B83A11C0EF561EAB5493C,
	ETCBase_DoActionBeforeEndOfFrame_mE6A885346C696EC5E4E2D8BE47BA629B62DBA252,
	ETCBase__ctor_mF5E1256874ADE07E24860A3AE37994F3C9896998,
	ETCButton__ctor_mDBCBF361BA3766C531D73494236CC8D3225E606B,
	ETCButton_Awake_m807702E4D5A199DA8C053D281B83DDB09EF9E7AB,
	ETCButton_Start_mDC30B8F1D2BE8074F9B6001AB0DE1A62DF4FDB3F,
	ETCButton_UpdateControlState_mA71E1F59EAD20D3A86A770017D62F2F1BB6F2D27,
	ETCButton_DoActionBeforeEndOfFrame_m5D1DB6621F5E041C61A8D7E0EA460273DB1F013E,
	ETCButton_OnPointerEnter_m16B469510021FD83EB55541ABED20F94A09ABA58,
	ETCButton_OnPointerDown_mA71620E4158C4D6B0D0C339A85A84D63975D48EC,
	ETCButton_OnPointerUp_m52E471EB0E1F532AFBCFF9334B122D0E1086C4D7,
	ETCButton_OnPointerExit_m664902F20FEBDCC099D3C3081D1F7ECB7C550864,
	ETCButton_UpdateButton_mD63ED06B84CBEB19FE795CA549C285FA905FEAE3,
	ETCButton_SetVisible_mBD6F54158DE6FA5F99F5E6F0DC11FF89EA121E8F,
	ETCButton_ApllyState_mF3F1BEFB29227DB99C1EE9C80E76F079B874FF73,
	ETCButton_SetActivated_m9B473F029A5CAA3754D59983882EF6E19F950734,
	ETCDPad__ctor_m97BDDD28C2742574EC6ED78D5C931F5F736F4392,
	ETCDPad_Start_m8A07615643ECFAE141BB529DB55BE35E08C6FEE3,
	ETCDPad_UpdateControlState_mA5B36B51EAF18060E8B29D8F19BA08F5452F5A09,
	ETCDPad_DoActionBeforeEndOfFrame_m47CF72131BF5D8179A2867CBA79A4A6CCE92858C,
	ETCDPad_OnPointerDown_mC4CFAE85F02ED3A3CF753260C40EAF32FA2C8E84,
	ETCDPad_OnDrag_mD0C2CDC27E4FD0A47842FAB474F8E859E85B1A2A,
	ETCDPad_OnPointerUp_m32BDE0C555AD90C4E343E34716FC068B7A189A64,
	ETCDPad_UpdateDPad_mE8C0C87F0967C24558FC0300A2846FDE4F2B3863,
	ETCDPad_SetVisible_mF862A5CE3E82BB669815AED0FAB76DD51F4785C1,
	ETCDPad_SetActivated_m7AD745D8C9215CE49888A55E96E02EE407E454B0,
	ETCDPad_GetTouchDirection_m6EF6C79B797101B8C95F67FEBAB3029ABE14D92C,
	ETCInput_get_instance_m9CD838DA676B36D5D4EF832B07636948D6403B9D,
	ETCInput_RegisterControl_m5A312482B738B12625AC973666B756DA229C8FCE,
	ETCInput_UnRegisterControl_m5146D877CEFA00F2703C93EEBF2FB29D08F96F64,
	ETCInput_Create_m2A4EA6298D8A95F2CA185032CB29FA50D8469F32,
	ETCInput_Register_m5B70537D14FB364A3EF577FDFF37B151A0389BF9,
	ETCInput_UnRegister_m85AB76BD77FDFC41C9C96B4443F4EB119F11D831,
	ETCInput_SetControlVisible_m04ECA8319B242A471F7E7A6657F9C70DF4344904,
	ETCInput_GetControlVisible_m1F6869013CFCD0444D92D5CBE9E5AE621B4019DF,
	ETCInput_SetControlActivated_mE08E99365F8AECC13FBCE3EB384B504F08D66E12,
	ETCInput_GetControlActivated_mF26D37234A7D9EBCC6EC99632B120DE65AE68AE3,
	ETCInput_SetControlSwipeIn_m29223017D1C737CEA2C15A5A940B7CEB9AF4CD3D,
	ETCInput_GetControlSwipeIn_m8163C29B98F8A06483CFB4E60C3C5D24E4C37072,
	ETCInput_SetControlSwipeOut_mAF89FB4658C1AC1316426A180947F0315F7BC8CC,
	ETCInput_GetControlSwipeOut_m96E2165FF278FF43FFC15D013803C11C77011882,
	ETCInput_SetDPadAxesCount_mFBB8C56E671E60AAE43CE19BC3A9AF711A8F7200,
	ETCInput_GetDPadAxesCount_mF5A3ACB2C943FB2641148F4BD6BCFFDE242C0241,
	ETCInput_GetControlJoystick_m6C99396CE1F3A2C71F9F73E8D58C396698532B1C,
	ETCInput_GetControlDPad_m3512F8CE59B2B996AB0F075E78E78830337BA41D,
	ETCInput_GetControlTouchPad_mA9CB8A16617247BD3E9BE0D1FED961BC74908FBD,
	ETCInput_GetControlButton_m5DBBF10FB44304BF294096A2F652BE1EE2B05AF1,
	ETCInput_SetControlSprite_mFB0FD1BE5689E49BDE7CDC1874B094D7C880905B,
	ETCInput_SetJoystickThumbSprite_m8FBD3CA4680E92652185387742005CEF2C2BEBE2,
	ETCInput_SetButtonSprite_m1E7E0CD5680F81B989FE9F057068DB1E097EEADE,
	ETCInput_SetAxisSpeed_m63501F05D8D4885D11742A14F1D07ED21C1F2FBF,
	ETCInput_SetAxisGravity_m7C5938D18630E9AA1C6963088B47E600E8682F23,
	ETCInput_SetTurnMoveSpeed_m59EF3FD689B58D1F829BC23356DBE40A5748A69B,
	ETCInput_ResetAxis_m9D287427DBB3A0D3695555790124B6DBA14E7684,
	ETCInput_SetAxisEnabled_mDE0FE8B3CE9AB70F46F22C04BA87D5C66EE73B96,
	ETCInput_GetAxisEnabled_mA16336CC21EDCABEADB2154526751506C9263609,
	ETCInput_SetAxisInverted_m16A56E626768ACBC225985E0D8D2D614266D6670,
	ETCInput_GetAxisInverted_mDE1DCC32119799008EB2A187FD6EB33536F9D875,
	ETCInput_SetAxisDeadValue_m3BBD1F5390E9B9858ACFD84AD38EA02765634C83,
	ETCInput_GetAxisDeadValue_m74B7B48696128E592B4C2BA85280F6C3A8F50EF0,
	ETCInput_SetAxisSensitivity_m952C10DEF9B92D0ACF0A485999BA5A3F619162B5,
	ETCInput_GetAxisSensitivity_m0A0F96810A806F5FB2B6BB10249C94C31EED638F,
	ETCInput_SetAxisThreshold_m3AE1F48AF73A6FCC3E3006CE2FA7CA2473811EC0,
	ETCInput_GetAxisThreshold_mBDEF6F2E9EF3D1C26F4AD74B6F328BD0D1CF21E6,
	ETCInput_SetAxisInertia_m6E2BB2406F83ECD09A0EF725643CF5F930D90AB9,
	ETCInput_GetAxisInertia_m43EA4C9D21F4834AD8C1D7889092D7C5AFEBCFC3,
	ETCInput_SetAxisInertiaSpeed_m37761EBA43DDC2B437D40A51473D809EA6B3DFBF,
	ETCInput_GetAxisInertiaSpeed_m9C41E015DDCD969DF91CDF46559A78BF4861DABF,
	ETCInput_SetAxisInertiaThreshold_mAB8B9C58A8D9CCD89CC2231993D4E3F4AA640A84,
	ETCInput_GetAxisInertiaThreshold_m2D15A8B708B54F04D07AC458A85EA28667B98779,
	ETCInput_SetAxisAutoStabilization_m0798915581DC1E2A20F6FCECF5A1286ACD8BBB85,
	ETCInput_GetAxisAutoStabilization_mC8CD3D0FAC37F07BC7397149394B4E5C5AD2CEAB,
	ETCInput_SetAxisAutoStabilizationSpeed_m42CCBC09FEA25A3CBF7593DBF4D9F0FCBEEA263E,
	ETCInput_GetAxisAutoStabilizationSpeed_m8EFA0AA2ECB938DC1FD12BFEFF99D497C3005AFE,
	ETCInput_SetAxisAutoStabilizationThreshold_m72E94D1E811AB0F70A4FFB0BDC338533EB2706BD,
	ETCInput_GetAxisAutoStabilizationThreshold_mDBC9BC599618FB0E9A297478BB4A34EDA86EBF35,
	ETCInput_SetAxisClampRotation_mBCE056504DEFA7B03B10BF7E55B2B42E49DA4427,
	ETCInput_GetAxisClampRotation_m3377294703CA91A7596B9371D7B616E974261D14,
	ETCInput_SetAxisClampRotationValue_m362EE5C71D25A7EF4A13C5DB767666A0ECA099A3,
	ETCInput_SetAxisClampRotationMinValue_m0525F1D62D4329E5FFCF9637A49192323C272F77,
	ETCInput_SetAxisClampRotationMaxValue_m63B4CAE7DFAABD121B974FF0C7AC8EF7A7424570,
	ETCInput_GetAxisClampRotationMinValue_mEA94004611E6D5F022B059B1F4CEBC7B4F17D7B0,
	ETCInput_GetAxisClampRotationMaxValue_mEEFEF0A44A5FA003FB31149F2CED90BDC73E71B4,
	ETCInput_SetAxisDirecTransform_mFF6445BE90A9B01D595E558C89D505728EDC5D0A,
	ETCInput_GetAxisDirectTransform_m7277917738E219C869139A5E1A17290153F5BE70,
	ETCInput_SetAxisDirectAction_m9525E9D86622A6DCB36BE17EDF409AB735A40E1A,
	ETCInput_GetAxisDirectAction_m77A172EF33A9DF3C31C7076359479D78249DE9D4,
	ETCInput_SetAxisAffectedAxis_m4765B52F252C8C3EB877294E5EDF17DB5FB703AE,
	ETCInput_GetAxisAffectedAxis_m55EC2EF12AFBFBB501083EAB2913F2EE4F6C74CB,
	ETCInput_SetAxisOverTime_mDCA45348296470D71D43DC64C11A484032E5C4FA,
	ETCInput_GetAxisOverTime_m23ECB76E72C76AAB7033CD6A925DED52DB59F9ED,
	ETCInput_SetAxisOverTimeStep_mD82A2C2EA6B8F1D247527A7B19D5DC222C15354D,
	ETCInput_GetAxisOverTimeStep_mBE2196554DFA81502C40621890C9695AA493C6CF,
	ETCInput_SetAxisOverTimeMaxValue_m9721597310E47BA6FD8F67D1398B8B7CB295E650,
	ETCInput_GetAxisOverTimeMaxValue_m364858C44036139AD6FA89E5D8ED8B595C8B1319,
	ETCInput_GetAxis_mD1ABC6B4866AD2318D3BF3800F1E4A205D905ADC,
	ETCInput_GetAxisSpeed_m46050DB636DAF7F2C08C1E7CACC8D04FB723251B,
	ETCInput_GetAxisDownUp_mCACCA58C011AA1AE66525FE9E63E8DE7FA98E760,
	ETCInput_GetAxisDownDown_mA48179A5A7296C155D8A1591C061C6E0FA329626,
	ETCInput_GetAxisDownRight_m8A02D4A250D9C998003461D4916EC0168A21A821,
	ETCInput_GetAxisDownLeft_m5B51CB9F33C048A3B76BEB9FAF3D1B7D0D3D840F,
	ETCInput_GetAxisPressedUp_mB4C7810CE00C2060AEC708DE8FBD7F3C81E56430,
	ETCInput_GetAxisPressedDown_m8321645C4928A00278F489108888EB84C67F7394,
	ETCInput_GetAxisPressedRight_m7A5451CB3AF12D5BF36C5AF9D2DEE0B9918F64E2,
	ETCInput_GetAxisPressedLeft_mEFD4326F4B09CB976DEB246C6798C0F80D70B7F0,
	ETCInput_GetButtonDown_mB743B8B6226FD9243F1258749D868B37BB5EC711,
	ETCInput_GetButton_m3349D8FA6F91C5324B786798D7D0361C776DAE73,
	ETCInput_GetButtonUp_m9F75E915B9C474B9F594855FF400E29CB0E0ED89,
	ETCInput_GetButtonValue_mD0958F928622F132F946EC03B75DB88364055863,
	ETCInput_RegisterAxis_m60EA1B99DA52D8EC63C46B0026C9EE72901E6EB0,
	ETCInput_UnRegisterAxis_mE6E1E5B9B81B599ED62B78DF5540C303AA5C5288,
	ETCInput__ctor_m46BDD6FFC063F4766C996CF78EE6F388BDFCCD52,
	ETCInput__cctor_m14DE50B9CF55516919E133AC30BC2038B3D84B3E,
	ETCJoystick_get_IsNoReturnThumb_m84B853A7A18F39C2E9FBAF9BC15767540BD56C24,
	ETCJoystick_set_IsNoReturnThumb_m7F2BDF26BC8029A5F3CAC31A1A647BC65E6798DA,
	ETCJoystick_get_IsNoOffsetThumb_m853FBAA23A336D03F40A59335CD126AE050166E1,
	ETCJoystick_set_IsNoOffsetThumb_m5C479CEEA3597C5C7CFFAED750DB17F7CADF9492,
	ETCJoystick__ctor_m04BA229A817ACF68AA5200D9D8FB5C6CBCE12FE7,
	ETCJoystick_Awake_m504ACE49B1B4247E4D3D74428C7D4F4FA15EE875,
	ETCJoystick_Start_m6F8C4F4F4458D5DCB09F491E90A0B51694F3AC75,
	ETCJoystick_Update_m882C84F88EE338A7C762D674F1612A0AB03926D4,
	ETCJoystick_LateUpdate_mD248E35D70897A64A7E1EC0B51FDF9E574075ED3,
	ETCJoystick_InitCameraLookAt_m6970412ED7F2273B4FDAA0E233E3086F5B9720BF,
	ETCJoystick_UpdateControlState_mE7F9B2FC5C19AEFBF85C96C5722DC554ACBA2FFF,
	ETCJoystick_OnPointerEnter_m8F54FE6621158782B02BBBD3B57D4CB2B109A752,
	ETCJoystick_OnPointerDown_m6373FDB24BCAE0530E06355C026A6C45BE3251B5,
	ETCJoystick_OnBeginDrag_m2F59226A2B693F1A80C736196D85F41687F16E0C,
	ETCJoystick_OnDrag_m91351068301671B274DBF7D93465372420792BE9,
	ETCJoystick_OnPointerUp_mBF67451AEB565B394F72D79170CC17446CE30590,
	ETCJoystick_OnUp_mE3400B8C1C7E81D1229E8A5F1625666A3565B68F,
	ETCJoystick_DoActionBeforeEndOfFrame_m028B30685DD59C8040713A206EABA4B9C98A825C,
	ETCJoystick_UpdateJoystick_mA530F42E68897F7CE8FBA5A98E2432A0E58C6466,
	ETCJoystick_isTouchOverJoystickArea_mBACB572D3CDACDAB0221E331B0571D4523C073E7,
	ETCJoystick_isScreenPointOverArea_m8ADDC7A67135E8C1F059FE39FDE31A6A527DC971,
	ETCJoystick_GetTouchCount_mDDA817D693DBA91C66A960024415B8441EB98C91,
	ETCJoystick_GetRadius_m32A263333C7EDC4F7D56492F61A109EC5339522C,
	ETCJoystick_SetActivated_m624984BFEA0EEFCBCEBA9BF6E99FC6228D400BE8,
	ETCJoystick_SetVisible_m7E09B2DEDD5F5A7318C208B42C999A2E3C1F65FA,
	ETCJoystick_DoTurnAndMove_mF8DA95ED4027E29812E16E80E9B536B1E082E799,
	ETCJoystick_InitCurve_mBAC68F5B60055E282C50F4518E5245FBC0B720CC,
	ETCJoystick_InitTurnMoveCurve_m8FDDF3006B95932DD2EDD41BB2CA89139637DA13,
	ETCTouchPad__ctor_mF1B52FA1E1EE26111A1FDD7A34957E5FFD0D3C44,
	ETCTouchPad_Awake_mC14785D63F7A1DBE25F260FAFC26BE95D500583F,
	ETCTouchPad_OnEnable_m21D562361A1FAD07E141BCD6530404BFF20913B2,
	ETCTouchPad_Start_m4D328BEDA29D50FF43148B0A2A73AA1A59514DDD,
	ETCTouchPad_UpdateControlState_m59AD53E74B1A43FAA9DDE142B26AC18B98CAB66C,
	ETCTouchPad_DoActionBeforeEndOfFrame_mE7C2E8F0CF0D2A53E995965F88E33C3C211F658D,
	ETCTouchPad_OnPointerEnter_m832FC3D243110D1062F053A9EA4C444DFD6B4D89,
	ETCTouchPad_OnBeginDrag_mE2E5817B425D3C3CA05E349316614933DC75A4D6,
	ETCTouchPad_OnDrag_m75C168E7D3915E6937582B88D737F75D9A4C3B17,
	ETCTouchPad_OnPointerDown_m65A43C1580C460E876D6BF2A14F658826CC62E4D,
	ETCTouchPad_OnPointerUp_mD8AC432E1B1F3E51E48B4DB265D09BAF6161289D,
	ETCTouchPad_OnPointerExit_mF7EC1FEB98948C8822F4C2264D2322D0CE6B2225,
	ETCTouchPad_UpdateTouchPad_m8D4DF59BA07000F3C50B2C37C8439A589E839B4E,
	ETCTouchPad_SetVisible_m7B36BF9D750C564AC5062A0D7DDA9CC665EA4B55,
	ETCTouchPad_SetActivated_mBB984EA6A0EE8E4FCF30A4FCDDDCD8CC94D65D34,
	RotationScript_Awake_m4CABA81305AEF3AFD1FA121FA5A7767610C4B97E,
	RotationScript_Start_mC6CD41E0258CD8FE249FB37C3AFA53A531F1CB53,
	RotationScript_Update_m1176401A78CD79210300333FBC316561D9D7E6E9,
	RotationScript_UpdateConstantRotation_mE94400C61A5087D5CD7D06E7E35197503696AABC,
	RotationScript__ctor_m38376ACF6849CB119931A0DAE31FE22CAD48D0BD,
	CinemachineController_Awake_m29EB2B6090A1C02A403861E8326CD15550BB94F3,
	CinemachineController_Start_m08177BEED6AB5F76422297F6D241FEDC5306D86C,
	CinemachineController_Update_m6CD69FA671B31AE6454622A476271C856518FB60,
	CinemachineController_UpdateFrameTimer_m9C2DB0B7176BB97FEAB93091D26EA53F2EEDB946,
	CinemachineController_FixedUpdate_m6586501D4F8DCBDCAC90DE196ECEAA4440C2B4CE,
	CinemachineController_UpdateCameraShake_m72F1C3B5EE5B5B2D8C8057B1DAFBA4E5BE29E9C4,
	CinemachineController__ctor_m2B6986946B1977B6FE7810557291323377A81626,
	Collectible_OnTriggerEnter2D_m0E7AB7E7FCD8F36E89AEDFAA299588E0E6984AA3,
	Collectible__ctor_mA82F242807011F3F131A51F9697889BF5B9C299B,
	CollectibleManager_Spawn_mDCBA20E523FFD9D4C7DBC46CD4414AD5C50ADD71,
	CollectibleManager_Awake_mE35FFD8380DFB797C6BF4DFD4EA4DCA5831B28B6,
	CollectibleManager_Start_mA60880CF54FEAEDB7104F4E9BB5C4302F17A492B,
	CollectibleManager_Update_m38DBB1F41C6345FF9EDF5D8CD599C3707F75D655,
	CollectibleManager_UpdateSpawn_m79329A2C9906A7DD31CA1713366E255587FFB614,
	CollectibleManager__ctor_mAA385632EA37E0B39327A2607145DF2DFF36B7DE,
	DNACollider_OnTriggerEnter2D_m40161A31E989C7FFFCA7F4F6B5402258F31843C1,
	DNACollider_OnTriggerStay2D_m3D10D593C3570BE9A80A4168F630C04083873ABF,
	DNACollider_OnTriggerExit2D_m0733C1A3358DDA0C78A2056960ACB93E4FD41695,
	DNACollider_GetPlayer_mC2ED7D9D623EDD2215F21698845B8CDC448CD45A,
	DNACollider__ctor_m2CB1A118C437FB40C4551E25CDAC4546173B8BB3,
	DNAStrandManager_Start_m536D70BC1EBD8015385051D43F3C40D178283248,
	DNAStrandManager_StartDNAStrand_mA67AAD76D0ADFE679F7DAA455F8BEE423EA80FB1,
	DNAStrandManager_ScoreIncrement_m04A36165A2EC86896C59BEB6881EB833380E8320,
	DNAStrandManager_SpawnNext_m1D5B0B2C347720E17FD8007F6C4FDA9E1D98BA58,
	DNAStrandManager_GetTrackGuides_m082E5E49709E9C40E064785030766B280A143B51,
	DNAStrandManager_ActivateWalls_m0A459CBD5625E78D21D8BA837B85948D1FD31984,
	DNAStrandManager_Reset_m8084C40FFAC60D17B93D2AA5FD8703093D491952,
	DNAStrandManager_populateDictionary_m22F5860567BC8C4ECDBDE99CC205644ADA09C569,
	DNAStrandManager_Awake_mE64B80623352245DA2CE201F3AB4987B6DCFFDA9,
	DNAStrandManager__ctor_m412EA62A47AAB1E9DFFA107BFEC80BB91DD5AC0B,
	FinishScreen_Visible_m5AAC449F98E3FF787A72802727C8958A8025590A,
	FinishScreen_TryAgain_m071DA818EAD58C808E7FA04B969B455651882042,
	FinishScreen_Win_m93C353B68584F1327A39B45EC4054AE152609567,
	FinishScreen_Lose_m0C3AEB0595AB6E34D91AE679A6EFFB2808E1C9CB,
	FinishScreen_Continue_m04769CB19C9FE2C0A3FDB8363291484A0BC6E72F,
	FinishScreen_Awake_m827778628FA7734D79BAA288752BBD8807B1356C,
	FinishScreen_Start_mC4BD4981443BED530FBE51B38FD5340D4C2D3E6A,
	FinishScreen_StorePlayer1Score_m4EFB9CCF8F51EC581BF211D6999BE47EA841C9C5,
	FinishScreen_StorePlayer2Score_m4DBA859485443D89234F68122B169E3825D2493E,
	FinishScreen_DisplayScores_m9063871C7739EBDD904F52C11FF005404D86DDEE,
	FinishScreen_FormatScore_mB50AF1B6ED34DC45A2DB39588926197BA266470C,
	FinishScreen__ctor_m5652F46083815215658CE8C5D8CADCD9ECBB919B,
	HUD_SetIsMultiplayer_mF8F4A47C57883D44CE28CDD458E4EB52C83206B4,
	HUD_setLives_m41EDC8B3B7EFAC6B839918D4CE94E6F98EDE83AF,
	HUD_SetEnabled_mAA9C06B49868DEC0D0198857696F0135C402D187,
	HUD_SetHealth_m9B311A06EF8ACE70901A44C9596C6BA2FD60665C,
	HUD_SetName_m897B1B5664EF2E48885B91653F8E172ED2479A94,
	HUD_SetScore_m31BD982AF4A3B2CAF9D89692F336F0C7D450E1E5,
	HUD_SetBoost_mE7E43957FD3F8A29F546B40F80D279D6DE463C35,
	HUD_SetBlue_mD147932CC2790A0ED7D5991D764AEF7CEE39CD28,
	HUD_GetPlayer_m455A9C7D6FAD83F454A068427C15347D8CD64B3D,
	HUD_Awake_mDF77764DC1D74224C4AF276B610B8F33E2E7A800,
	HUD_Start_m4BD3E094BF1DE8D743C70396024691DD0B0BB275,
	HUD_Update_mD5BD3824D8D13B56D9E833D398B957345025951F,
	HUD_ActivateNextLevelText_mF16AAD3C3AB525DD095F37F83808F894EAF7FDDA,
	HUD_ActivateWipeoutText_m62C15B074F8CEC5037F7D87E0D15D6126188DC74,
	HUD__ctor_m579520AFA3C6D8555D07E83C9EB3F140D8323CAE,
	NameGenerator_Name_m1DB4502AA0866B1682414DF243F75D143440878B,
	NameGenerator__ctor_m01231912A2381310BAA7442A95066502823A8B8F,
	NameGenerator__cctor_m452E4B6CFC2FE555C029F911022FA1828DC25B43,
	NewBehaviourScript_Start_mDB573B0B04591BBF1CDC10C7C835851EBF8D17F2,
	NewBehaviourScript_Update_mAA0BE51F329DE556FA585E93DD1B9CF6D917A85C,
	NewBehaviourScript__ctor_mC87DFFB91971C9C20A9487A744F5E68D74FB05FE,
	PlayerController_get_currentHealth_mA238FB34861E13044FF51984B124B6633AE1A17C,
	PlayerController_set_currentHealth_m12746B486DA77A161EAA05F3CAA2D18120762CFF,
	PlayerController_get_currentLives_m6CA3C8E9DA3BC58036F06237C1F208496C5CE3F5,
	PlayerController_set_currentLives_mBAED406A54F6ABCAAC7724394AE1B58211569ED9,
	PlayerController_get_playerNumber_m0A518CA5E2AB88B6183C2CBBBF8B86719706CD9F,
	PlayerController_set_playerNumber_mC39BB990846E9F677C2F7239C8247F6C9C56E464,
	PlayerController_get_playerName_mCD3F35E59BFFCBFB6011C24ED603A1A457EED4D5,
	PlayerController_set_playerName_m5C9797B9533C8F4C40C188C7C8972EE1A257485E,
	PlayerController_Collect_m1C88129453F8202810D59FA8AD10528D163855C0,
	PlayerController_exitLevel_m1DC20642A604B23CE776238A888520CEA634D149,
	PlayerController_SetHealth_mD18B26D0341F433B7D2F239614E62F355EF54C82,
	PlayerController_ChangeHealth_m23D95E59ED18ACBB6BCD9CB4DA5CDCF9E5117868,
	PlayerController_Awake_m118C1D205A374B627E1EC963E5837E23CF554573,
	PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF,
	PlayerController_Reset_m902A8C9D413CFF880212AB09556DC0D5EA73D9E2,
	PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33,
	PlayerController_setRotation_m83D5251B477A34971416C904BFA04C9F4D70AE37,
	PlayerController_pressLeftButton_mB96AF0C4F7FA64577407F6613E58879AF83B78A8,
	PlayerController_pressUseAllBoost_mEAC5B562BB6C2E5994F283662EB31A7552F4A9B8,
	PlayerController_releaseLeftButton_mC75C45BD0C5EF4AD1385B69DB860F4F8FC4B0066,
	PlayerController_pressRightButton_m978D262A8970A8F5E5984062CFA631E7A52D3350,
	PlayerController_releaseRightButton_m166C4B33D5BEBCEA59082E34FA8EA41EA187C1F2,
	PlayerController_UpdateInDistress_m2FB1DBE2FEF0F1E858770BC23AFDBD81541644B3,
	PlayerController_UpdateMirror_mBE5DD28C09E2D38A2DF182CC5D925EBFE6B95BF3,
	PlayerController_UpdateBoost_m366F78E6D8D96B1C7754197D21BFE14F4D3E0433,
	PlayerController_InDistress_m6F33784BF1F546379B6DA11AE0B34DFE727AF3BA,
	PlayerController_EndDistress_m6DEEABCB09434A19B426739EE48A68A761D268C6,
	PlayerController_SetAvailableBoost_mEA40B368E919A15342F04C4747035BB2084B9A7E,
	PlayerController_SetAvailableBlue_m434A4FBFC1CAA4F0506AE12D045D774C3E26855E,
	PlayerController_UpdateSailRotation_mE0796C61BAD4FE23ACB067B54616D17D24104CE6,
	PlayerController_UpdateAccelleration_mFFE774CC3FA7EEADB02B9B0BD9E6952FE3B0DEFC,
	PlayerController_UpdateTryAgain_mA889B13776CCAFF8A027857A4364670A7D575A59,
	PlayerController_UITryAgain_m4D3020B2D2E91EDC8FFE842434C2E3961F58AC81,
	PlayerController_LoadMenu_mB41AADDB5C5AFE2E615CF41B5B903C143504425A,
	PlayerController_turnOnVaccine_m776592BB3D73144737DB57460D3539E3C1BB4DE2,
	PlayerController_getVaccine_mB4C46F5CE4881DA40F5D4C96E108686869CA7AA0,
	PlayerController_OnTriggerEnter2D_m4BFC3BB96B7F16C5F0E6A7BE6795149716478A74,
	PlayerController_OnTriggerExit2D_m8E5A01B8FA14061A9E824E3B4AA6E702BC32A870,
	PlayerController_SetDrag_m56ED9520C66BE349A979B845F15BCA955EDA58CC,
	PlayerController_RestoreDrag_m5C7B2EE4DBA75736A7C154A176533E78E0B1220D,
	PlayerController_InputButton_mA163344F554E819C5D4AFA8DE26C4609200D96E3,
	PlayerController_InputAxisRaw_m412FC50032AE0B387781F445E17D4B8FD9381A42,
	PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60,
	PlayerController__cctor_mC7F091C693834BDDD5C2F37F8A8AF92350083357,
	MainMenuTurnSpeedSlider_Start_m85074A3BE0B6BAF3B4D5E53007EEB4DA0367DD55,
	MainMenuTurnSpeedSlider_UpdateTurnSpeed_mCB502E2E8ED9B243B223FDF0C2BA1A345ACDF4F6,
	MainMenuTurnSpeedSlider_OnDisable_m1E3D3F70FBB5088FE86F51250F2BCE0553DE79F2,
	MainMenuTurnSpeedSlider__ctor_m08D32BEC6F07B4063AC04E707AC573706C4830EC,
	ScoreGateController_StartPosition_m09C3F74DDF482C45F159699F63E3CDF9CB6F632E,
	ScoreGateController_OnTriggerEnter2D_m3E3F7616F2755E61FBCBE824F89A1468B2197939,
	ScoreGateController_Awake_m4A2A6EC6E31227C8053104A9EE5879170B9814B1,
	ScoreGateController__ctor_m2810289E215BD50F41D8E4D8BF0F4571D06C289A,
	ScoreManager_Boost_m98871E98B616CA1F9C2E25455F6E0537786FCE71,
	ScoreManager_CollectCoin_mAB62D8EBAB72C53F6BD31C89E430CEDBA8E7C790,
	ScoreManager_PassScoreGate_mA754233986BB162B4BC12D660D69128269C6F644,
	ScoreManager_Reset_m23F7E7B0959CAF8614B9DFC1AC0CAF2015C42483,
	ScoreManager_Awake_mC9372E5D9A3EEB1FC42D08450E813A6E01B3AC8F,
	ScoreManager_Start_m1BE9724ADAC2945CE6A29AB672FBB5DBE5BE96B5,
	ScoreManager_Update_m7C0C17E3C017FDFB4915824C59DEC5832A0A166F,
	ScoreManager_UpdatePeriodicScore_m1EBF757319C58F1CF4FF8234FC24853011DDB13A,
	ScoreManager_UpdateScoreText_m084C614C74288678E1D996CC280577D89B472EF8,
	ScoreManager_GetScore_mB5A1018BE3EF9F9E774E91CE8AB3E2A310ABE624,
	ScoreManager_resetCurrentLevel_m9FBF4ACFFCB546330C5CB54D38A60FDBC0FB37FD,
	ScoreManager_checkCompleteLevel_mE2FEEA2D3FC6072D028C0C5FAA99354E96EBDA8F,
	ScoreManager__ctor_mFA7EC3F474306DB06EAA63B19DEFA71F4E00B9E4,
	NULL,
	NULL,
	NULL,
	TargetFrameRate_Start_mB12212D0850E0C5C8153A58608E170DC2FD7B463,
	TargetFrameRate_Update_m685D371061CD7405EE649C669BDF67E77ACCD70C,
	TargetFrameRate__ctor_m549E35628533F771593486A4A0A696F66A4ECFE6,
	API_Demo_Update_mA48F15C091DF17F2437C1896D4D4EE09D93F150E,
	API_Demo_OnGUI_mB78DC161C2DC70637D87AC05891E566A07F9FD69,
	API_Demo_Sens_m3394080DFD1B35661AD2106641F657F6E798621C,
	API_Demo_Axes_m58AE74947AE5BC050A9A4EC8C8E38620C61B0657,
	API_Demo_SetGuiStyle_m960A735C08E4F30F8B8BE09076B5E907F59763B4,
	API_Demo_customSlider_m77455CD2F153C037F6A6E8931B41AD1544D9ADB6,
	API_Demo__ctor_mF3C2961812A81D482E0862748198AE8A7DFBCC82,
	mainmenu_Start_mDC81297C63144BAFB8DD830BC92F36D98A5AFB51,
	mainmenu_Update_mBFD24A0C2525F5F195934592A61148E37F2D6354,
	mainmenu_loadOnePlayerGameIntro1_m65D0F4C6598A5F7153F2CE5318DB1FA507EDB60A,
	mainmenu_loadOnePlayerGameIntro2_m4F09CF443D3295D3F483D9377046A0EFD5E19F36,
	mainmenu_loadOnePlayerGame_mEAA5970F5ECB291E05CADEA609FFFD38E7E01616,
	mainmenu_loadOnePlayerGameEasy_m117369FF13EF8FF89BD8FA57261CA49F14B4D033,
	mainmenu_loadOnePlayerGameHard_mB790347924211A7EE3EA17FA6FCACCB4710879AC,
	mainmenu_loadTwoPlayerGameIntro1_mF1899FED779D2EB45DC09478286AB041A41398C5,
	mainmenu_loadTwoPlayerGameIntro2_m5BA59B3C13EF7C8ED914FDC4CDB2747860BAC4F6,
	mainmenu_loadTwoPlayerGame_mBE4368A48A1E92E6D170AB618DFA134AFD8C680B,
	mainmenu__ctor_m49FD1EAC3055FBC9885EABB53E715B12EBD0936B,
	rotatorScript_Start_mF1D5803F10BA8630B167DBE734FE76EF412DF196,
	rotatorScript_Update_mD54FE3F99E6E2AF94F9F0D7E255124AD0FC70505,
	rotatorScript__ctor_m92241A9C114B4535C466686F17D85A9E18D9E856,
	ApplyMethodAttribute__ctor_m296E6B09B992262C44F658D22169D62A1023EE7C,
	LabelAttribute__ctor_mD9492F23B8F3EA05C8A1CEE399B4B4BBF3A95E28,
	LogicLabelAttribute__ctor_m1B723C7AD45E2D8CF0133350055706C0793D3135,
	GuiCamera_get_getCamera_mD1310EB3B41BAD6C3564D91469DE70229D834A26,
	GuiCamera_set_getCamera_m140E54BE2DF0723C0B0429736E50375A55768D33,
	GuiCamera_get_getTransform_mC266F3006763DE9A88FD00036FAE6DEFF88A0D5E,
	GuiCamera_set_getTransform_mA92D4DCD74CEC008CC9F6AB57FFE3225ED1004AF,
	GuiCamera_Awake_m18A1476EB47D6B9815495648C6A3F8532190A38C,
	GuiCamera_ScreenToWorldPoint_m2F019D2787CB5F7C11C6CD6BDCB5AD37A2C094BE,
	GuiCamera__ctor_m1741142A36BECC6603E9BD910C255B0B1E530A8B,
	Axis_get_value_m8A180723D48678894264701038F0BBC343640E47,
	Axis_set_value_mA9C3192DE8A1A2C3FD706608FCB46A84D31C7CAF,
	Axis_SetValue_m3F99072C1B9A593EC334B0B8AFCCEAC2647F009D,
	Axis__ctor_mE874128554829D17C871B1C9FBB775B223F3DAC6,
	AxesBasedController_get_ShowTouchZone_mF6912DF0B3EAADCA9294654E8B02A38B7E4FA642,
	AxesBasedController_set_ShowTouchZone_m84782525F3DF56A5369944505AC5B2543EF8A91A,
	AxesBasedController_OnApplyShowTouchZone_mFE45672990F5DFC0BCC7D5E0E709ED3736EE2E1D,
	AxesBasedController_OnApplyActiveColors_mF43C93B90202125B134048E9A726F0D19513451E,
	AxesBasedController_OnApplyVisible_m75F854BEA8B3F7668ED5A4CB45A14BB2A9468C7B,
	AxesBasedController_ResetAxes_m600249A15F091FB2ABD5DE3C3B5B815252C7E9E9,
	AxesBasedController_SetAxes_m5F89BE7F50AFB265ED0B7B57F95553918BE86AAB,
	AxesBasedController_SetAxes_m36D06AD499732F417BF513AF37FD0B6217A08162,
	AxesBasedController_SmoothAxisX_mCA175E2DA84EF66DB7E9987ACB64A039589E115D,
	AxesBasedController_SmoothAxisY_m4696B49D9184B972CE889D538D38CF12129B7C0E,
	AxesBasedController_ControlReset_m665C1235C98F939BA6C6E97BC07D4542A86F53A7,
	AxesBasedController__ctor_m75882BEF560E81CE2B035326BBF2BD4D1B5FED4B,
	ControllerBase_GetActiveColor_m2CB5BA88547CDAC0499A9895385EC383C370028B,
	ControllerBase_get_isEnable_m56794FD2588BC750EE510C45791B39EAC6B7DBDF,
	ControllerBase_set_isEnable_mA52409A32CE8E73258115CDE1B1CB701A2DB6976,
	ControllerBase_OnApplyEnable_mA7B37761C439CBB21C7995099D465F64CED934D2,
	ControllerBase_get_isActive_mD0A90584871B5973215E37D5F64AA6A55F8AB34A,
	ControllerBase_set_isActive_mF26CE3D98D6C5814E080B4DBCDD1ED6CBC294C12,
	ControllerBase_OnApplyActive_m8C6F1C13686045A65BD2FEC4C668F8397B0653BF,
	ControllerBase_OnApplyActiveColors_m038B03133484419441CAE25B4BAF9AB46D50A28E,
	ControllerBase_get_isVisible_m699AE27604C54E112C49F4AA90A5CEF45AD37603,
	ControllerBase_set_isVisible_m9B7C67302D5849B757D233658818F0CC54025012,
	ControllerBase_OnApplyVisible_m4D19B6D3E6B8E6898FAD67F27E6EE9AA2D5DE478,
	ControllerBase_OnAwake_m87AECBBDA5C4EDB74EE5C5AD159FA5DCB5CC95F0,
	ControllerBase_Update_mE98C96F4F96C299946D0DCD2FC71295B3445EC75,
	ControllerBase_LateUpdate_m1814B073344C4E51FE4865342BC20CFAE6C4A108,
	ControllerBase_FixedUpdate_m595260AAB40C9651900D0BC54BCA45566B0D28DA,
	ControllerBase_OnDisable_m633C0E158F84CF63F5F503E612BB2A1224C52C0A,
	ControllerBase_UpdateTouchPhase_mBE7297F1E53CCAF44DBF5E3E091545F785273E00,
	ControllerBase_UpdatePosition_mE31505EF07B5196E431D3D1258510A35343F6F2A,
	ControllerBase_ControlReset_m8F1BFC5D9CEB4FE2C37A0F106591828776BCB940,
	ControllerBase__ctor_m0B6BDD4E5E0A33736B860D03BA42A53D9B2875F4,
	TCKButton_get_isPRESSED_mA4DFE14807C4B9BE7A9A4F25C646694E9F23E679,
	TCKButton_get_isDOWN_m21A6B739592B2435994E4EB044AA73E3764FBF66,
	TCKButton_get_isUP_m030F8B6F0133E4810B1618BD727B8CB8B02A9066,
	TCKButton_get_isCLICK_m1C74D4394D82F7D66EABDCF71A336BB0A4DA1A24,
	TCKButton_UpdatePosition_m27C44C72A75DB8383D88CBA43107DAAE8B9C11DC,
	TCKButton_ButtonDown_m570FC008BCF8D2399B22E0FECE272FE61B94A37E,
	TCKButton_ButtonUp_mC2FF788E70391697A3B5786119C61256BE09024A,
	TCKButton_ControlReset_m87ACE3AAB4B3C226CB35DD298B6DE70BE2345F13,
	TCKButton_OnPointerDown_m65B3ADA6DB1AA89E941F4861EF30613024E1862D,
	TCKButton_OnDrag_m8C23768914FD492008AE0AA4C36CCA1781725AFF,
	TCKButton_OnPointerExit_m7BDEFF040A89BF7FE6731DE0A6D86C9DEE3CBBDF,
	TCKButton_OnPointerUp_mF1E05D892C07763F697F88ED2AC153901C7477BD,
	TCKButton_OnPointerClick_mAF0418C5F2B97C569EDF9BB81F6ABF21820B6A3A,
	TCKButton__ctor_mFFE37E008FC815B74E901998681ABD89F78A61A1,
	TCKJoystick_get_IsStatic_m5B3625BD29E3FC9A178D06D24CF798A27B1F5CD1,
	TCKJoystick_set_IsStatic_mC73AACE1ECE0C68D13E58CD5CDFDCBC6A7387ADE,
	TCKJoystick_OnAwake_m7DE43BC6180AB47CF910E336854ADD29E960084E,
	TCKJoystick_OnApplyEnable_mDC783B43E065AF3E1D757B90BC953A2B1AE43E4E,
	TCKJoystick_OnApplyActiveColors_m9DF053D0512882C707811A5CCBE79AA8F5D6F10F,
	TCKJoystick_OnApplyVisible_mF404470D980B6BE05CFCDE8D935851EA4269F664,
	TCKJoystick_UpdatePosition_mD675788CEA9548B749E0ECFA5625BEAD47BC5E1B,
	TCKJoystick_UpdateCurrentPosition_m8D6125168FC3E2C0CDF929EA803A095EF2AAEEFA,
	TCKJoystick_UpdateJoystickPosition_m7D5690C2864EE3F0F9EB69C7283343AC6BCF4883,
	TCKJoystick_UpdateTransparencyAndPosition_m64AFC17AED6D297ABC0A3497C39206BD5F093DBD,
	TCKJoystick_SmoothReturnRun_m263550D1BDB70F0BFC8B5B90895B48FE49216FE3,
	TCKJoystick_ControlReset_m07DADE3A97D5C42F18114D337F5EDCAA7CA50EB1,
	TCKJoystick_OnPointerDown_mD65A2BB5AC11011C3932B63A3057D7E744BEBF3B,
	TCKJoystick_OnDrag_m36F50B1D84852A80E6DA8E6E1B57230A9371AE2B,
	TCKJoystick_OnPointerUp_mC80EA08686365ABA1B809D9CA68E3B3EB33B19C2,
	TCKJoystick__ctor_m38C752E89491800B77D12AC6D715B62A29FAD0F4,
	TCKTouchpad_OnApplyVisible_m68D102D28CC07E3F760B8E8D7EF162C5B5F4EE91,
	TCKTouchpad_UpdatePosition_m72C696F70529BA924120454A549D35C4ECB697E0,
	TCKTouchpad_OnPointerEnter_m79FDA1AEBFEE77BF2923D962B8D2E9B2DCFC6E3D,
	TCKTouchpad_OnPointerDown_mC5C4A0E20F4F91F3F5AC254CF68501E58A4B73AC,
	TCKTouchpad_OnDrag_m131BD1ED57EF4214598C04B630EEE18E33E05DC7,
	TCKTouchpad_UpdateEndPosition_m4699CED0A47160E2F2CF20A3BF638526CD8A4A28,
	TCKTouchpad_OnPointerUp_mD7BD2FAAF8075E36CE7688B6B5D8EF204B554B05,
	TCKTouchpad__ctor_m01C8D7C078753D740131521A19A1443177C698B1,
	TCKInput_get_instance_m030CC4E39694281D15E16BFD91AC3E39F8F34EE7,
	TCKInput_get_isActive_mB0854A8A1424EC25A137877789B7A84984A10DB8,
	TCKInput_get_allControllers_mB8B54A73D6237F0BD4317BD68A712DE350CE29BB,
	TCKInput_get_abControllers_m968AF442F344D721A36024F5EC9944F7003700F2,
	TCKInput_get_buttons_mD997ED98A213FD3F5B2D71E2B086C2C5C1C2C1A0,
	TCKInput_CheckUIEventSystem_m49FCEB202FD99332483F1E8FF5F033814B6330E8,
	TCKInput_Awake_m03D7FCFC1DE7E3B863C707F7887C395465CE363A,
	TCKInput_OnDisable_m9B4921019B6E1C30F1927DD3DBAE82C0C7ED0BE3,
	TCKInput_OnDestroy_m5C364EBC121976FE23800995469455D9069D7CAD,
	TCKInput_OnReset_m9AC14D3ADC913F55F66B2D18567B6294E0189F90,
	TCKInput_InitControllers_mFAD19EB00743F5CA7B797D5381BF36E9F0116B56,
	NULL,
	TCKInput_SetActive_m046D9BEB11E5831368E45EABB49626803DE7EDC7,
	TCKInput_CheckController_mDEBFF59D19166F6C83A88274DACFAFFD06E2D79B,
	TCKInput_GetControllerEnable_m0B445D902AD39DFCCED919F3B631A2DE4AEB288A,
	TCKInput_SetControllerEnable_mE4E30714BDEB2AF5660CA789F2055EC08C387293,
	TCKInput_GetControllerActive_m25A474DDE8495E39CE7AD89345628C1EC9DABD0A,
	TCKInput_SetControllerActive_mF7FAE357420251729189095A29A499ECE8E291C3,
	TCKInput_GetControllerVisible_mF6F1782CA79EEC05185064117C62261EF8DE444C,
	TCKInput_SetControllerVisible_m203486B41CD0D023A542E44E05C8A3FC6FB527F0,
	TCKInput_GetAxis_m7DA7A595A620007C5E853EDF56E91B90548339A5,
	TCKInput_GetAxis_m373AE58DE90BC946C5CB7F6DFF282F6B1CA51E1D,
	TCKInput_GetAxisEnable_mD76609AB0E109C5FFD43B99BE200A9FF3D0AED40,
	TCKInput_SetAxisEnable_mBBEF1E459DEDFCAD886E75AFC5FBF72E17EBCF26,
	TCKInput_GetAxisInverse_m0ECF046E09BF703808619FB8D343D3154FEBC3C1,
	TCKInput_SetAxisInverse_mE51F4371C1EBD3E0F25C0608548F7553522461B4,
	TCKInput_GetSensitivity_mAA20F74A07855CF1A9C89D1F045F84ADAC0E4B35,
	TCKInput_SetSensitivity_m8B5F806113CD0D0E912EE3B64458F03F01DDA141,
	TCKInput_ShowingTouchZone_mB13BE0DDF92EA9D293E3D91ADA74546DEC9B8E69,
	TCKInput_GetAction_mA1F43D192959984AD33026D098E23C48426847FD,
	TCKInput_GetButtonDown_m5C88CFC3680E74B374CD427CF926E362A95706CE,
	TCKInput_GetButton_mFE6B58D6D9CEC14C05F2A5412909BE9191E40619,
	TCKInput_GetButtonUp_m1FDDED653FBFC4E5E68AB602E33435A2A46A9AF7,
	TCKInput_GetButtonClick_m54F0EBF08356BFC68A3C9BFB672FE2F17D18E156,
	TCKInput_GetEUpdateType_m1BB8FE16AF72ABE36A406E4D8B3AB9A0476AFF5D,
	TCKInput_SetEUpdateType_m7011876453070543E6FE8C059CD138B825B3CFB3,
	TCKInput_GetTouchPhase_m4F83327BD54711D7E28E592BAA1662B44A33709A,
	TCKInput__ctor_m154DE606DD19AA21C9B3203950456F85478CE8FC,
	FirstPersonExample_Awake_m6C15FF8324901B0B702EAC836B31FD87DFBB7D58,
	FirstPersonExample_Update_m1B331750FAC2CFAEE38A0D70A57E570614B2BD11,
	FirstPersonExample_FixedUpdate_m7128A17A4DD2E89D27F07F13D32EA3276782D489,
	FirstPersonExample_Jumping_mE106FC87A791E25766AA160E908D2225EC39E20E,
	FirstPersonExample_PlayerMovement_m95729B8554AB8EF472C4F8BA514FA53DF64BB103,
	FirstPersonExample_PlayerRotation_mDDD7E1E02FED0B7167CFDBA2D916D712083DFCB0,
	FirstPersonExample_PlayerFiring_mDC36CEA670704AC16ABC765A4A1C20ACD8CA31C5,
	FirstPersonExample_PlayerClicked_m1455834C7F8577A19A72968DA0141EAB4D898C64,
	FirstPersonExample__ctor_m914B8FBD2E47B35F74AEF776D7C468C4DFAD21E0,
	EasyTouchTrigger_Start_m711DFFD49A39C76354E4AF884837832A1FAC3BCB,
	EasyTouchTrigger_OnEnable_m61552C4FDC8D6A8384BF2B56D8141256D78E256C,
	EasyTouchTrigger_OnDisable_mBEAE9160ECC230A6A6D10A114E2FEFA429F62164,
	EasyTouchTrigger_OnDestroy_mCEE4817A00E1F63EE8CD74C3057BD5D64DF6C313,
	EasyTouchTrigger_SubscribeEasyTouchEvent_mF19355C4A4AE28D2B2930CA92AB7102BE6CC36E5,
	EasyTouchTrigger_UnsubscribeEasyTouchEvent_m00F8875F7E098744A212C95E03935A8E3CE4FB84,
	EasyTouchTrigger_On_TouchStart_mBE2F314F30DD09D28331EFACD56014ECA46171DF,
	EasyTouchTrigger_On_TouchDown_mFA13DC8CFE69F4EFAFDD8B8644A392E3A6A790AE,
	EasyTouchTrigger_On_TouchUp_mF120AE26739BAF7E77BD7792A5E258CAC4AD322A,
	EasyTouchTrigger_On_SimpleTap_mA709D5B7D1FF7B076F0B9CAE9148460F16CC26C7,
	EasyTouchTrigger_On_DoubleTap_m8020924B2DEA68CE37CA4C4EF3C6A44C9155E1C3,
	EasyTouchTrigger_On_LongTapStart_mC3B18CE1C5FDB5CA27D67D256CBAD63A585BB4ED,
	EasyTouchTrigger_On_LongTap_m1FFCEC9E68BF2DB70077EE6B7A774295EFE1B38C,
	EasyTouchTrigger_On_LongTapEnd_mC07993994E30CAB63359AAF898FA6CCD985637AA,
	EasyTouchTrigger_On_SwipeStart_m8AB6F05618EDFB931DAB501FF85A8A1C4836AAEB,
	EasyTouchTrigger_On_Swipe_mEA3686120C7684059B41962F531022F563893554,
	EasyTouchTrigger_On_SwipeEnd_mEDC32271BCBE699F24D0D8BC1751AE3D49C941BA,
	EasyTouchTrigger_On_DragStart_m740DDE3CADF433592E2543C296C2E1AAC167B136,
	EasyTouchTrigger_On_Drag_mE960E83E9511C4F8D40791C4F3A686920E7F7D66,
	EasyTouchTrigger_On_DragEnd_m107B02A41EF56A62A0C509B2BF0EDB48D46B9943,
	EasyTouchTrigger_On_Cancel_mCF059F25609C7CA53A18B1960EC54DE470347566,
	EasyTouchTrigger_On_TouchStart2Fingers_mED9EA412466D921C37EFA898F4759305A9E2D736,
	EasyTouchTrigger_On_TouchDown2Fingers_m8F2990E73692F51FC49078389BC315996A286C33,
	EasyTouchTrigger_On_TouchUp2Fingers_m21A93D70D6987B1B1A8C90986941EF50359E9CD5,
	EasyTouchTrigger_On_LongTapStart2Fingers_m3FAECD3D400D66AB6E1BF18C12E6ADDBC2901940,
	EasyTouchTrigger_On_LongTap2Fingers_m566D7816F5C5EEC8B6E4F6D9E9D76BA9624C2AF5,
	EasyTouchTrigger_On_LongTapEnd2Fingers_m9C2146DA7EEAA78E1C186D9B6DDEC43FEB05BBF2,
	EasyTouchTrigger_On_DragStart2Fingers_m5D3CA4E9B6EE613ED550BC376F2A932351AC21ED,
	EasyTouchTrigger_On_Drag2Fingers_mDD2C39858E72756AA231459AD9A479B0C26FE16B,
	EasyTouchTrigger_On_DragEnd2Fingers_mF0FE2C21B1F41F2AB15457AABE5A13EECE725D7E,
	EasyTouchTrigger_On_SwipeStart2Fingers_m5EB76F195BEE887E27AA52708A78FD80CF5E7759,
	EasyTouchTrigger_On_Swipe2Fingers_mD40D225719BD87605F38B1A66E8C59FE711D037C,
	EasyTouchTrigger_On_SwipeEnd2Fingers_mDC4B51476CB2A65561FB2A6A8F6BAE3EEE1F8260,
	EasyTouchTrigger_On_Twist_m7AF44FE52517312C014F59771DACD2CEA79A0C15,
	EasyTouchTrigger_On_TwistEnd_mEA5A698D69E659F04D39D87B199354E2E14E9290,
	EasyTouchTrigger_On_Pinch_m3814E9FC11484E90010510744268AC64BD6E7C22,
	EasyTouchTrigger_On_PinchOut_mD91A4A271C923EE6E87669FE37A21C1C77B4B7BF,
	EasyTouchTrigger_On_PinchIn_m6CC6C986D08EF95022ACB866E52ED99174D43CEC,
	EasyTouchTrigger_On_PinchEnd_mCFCC92D92A354C94DD502FA424E7CCF19ED8BB2A,
	EasyTouchTrigger_On_SimpleTap2Fingers_mBFD74A5641EE4F36A419928838C02E70C593FF9C,
	EasyTouchTrigger_On_DoubleTap2Fingers_m37FF870B26017DA5B5CD016157478D80F32BFAF2,
	EasyTouchTrigger_On_UIElementTouchUp_m59E963F792D0AFF69007370FCF7C515C4EA6D7B7,
	EasyTouchTrigger_On_OverUIElement_m55B286E7442D7115F6831D91020D050A48233D19,
	EasyTouchTrigger_AddTrigger_m27E4FACCF1F692C9BB24972C8D8C7AF713FF9274,
	EasyTouchTrigger_SetTriggerEnable_mFBD931A7FAEE6F6B256999BD7D8D30D5C647F453,
	EasyTouchTrigger_GetTriggerEnable_m58C9F8ECE31BB90311A556DF5D6D24C31EDB730A,
	EasyTouchTrigger_TriggerScheduler_m551750B30661A2EBA9C1773AAE26CCC49643E701,
	EasyTouchTrigger_IsRecevier4_mD4544DF3B62C6F4C3F85AAE327B86DC588DBC934,
	EasyTouchTrigger_GetTrigger_mD10D58C6EFE45AAB1246166E71D26DDACA6B8595,
	EasyTouchTrigger__ctor_m48F9CE5A67418AEFF9D62CE1ADAD4C2D4041D457,
	QuickBase_Awake_mE45ACA3A836B9CC9530195AC03E482CCD539A7C8,
	QuickBase_Start_mF31E321C7867827B90A454B84FB63589E202A30A,
	QuickBase_OnEnable_m229AB2AD87DA9C451DFDAC41F3C3068AD0850817,
	QuickBase_OnDisable_mE54B0741A8E06383C7FC0274951978E27546CB8B,
	QuickBase_GetInfluencedAxis_mB28CC2EB2CF21EBB99A59735697F66140B079F26,
	QuickBase_DoDirectAction_m4BD5573E2186CC349D4F7666270262488B53DA0A,
	QuickBase_EnabledQuickComponent_m9F479F1B7CBC49F63710F9DC0AA6C10DC0AD8C69,
	QuickBase_DisabledQuickComponent_m7433D42D2ED635019EAAC10C97F6EC22342C0D53,
	QuickBase_DisabledAllSwipeExcepted_m663F0B4F908235073A9DC24B6E25C55099B2F3DA,
	QuickBase__ctor_m72EF095BC9624BC5B806F8F0B9185937031156FF,
	QuickDrag__ctor_m15AD2A187771F3D204E48C81B499DF94E3B705CF,
	QuickDrag_OnEnable_mA71556956F1B88874865242FEBCD2BFE5EA4112E,
	QuickDrag_OnDisable_mB271040344F7972AE80F25949138F31818ABA3C2,
	QuickDrag_OnDestroy_mFFEFC1FD3F705845B642B40089AADD3AD27E7C57,
	QuickDrag_UnsubscribeEvent_m78A55B08AC3B7219AA07DFBE1F6D8C8DFE2DCEF8,
	QuickDrag_OnCollisionEnter_m9525B1CC52B5685C97DF6FB798BF0285BA999455,
	QuickDrag_On_TouchStart_m2AF99AA9A7F5AE904811439AA8BC1C0BC0967F8D,
	QuickDrag_On_TouchDown_m1B0D67493BF0D8A6299FBF6A9BE798D83B456BBE,
	QuickDrag_On_TouchUp_m3DFFC62B95EE1C8AFC8BE2E6E0BDA17599F07D83,
	QuickDrag_On_DragStart_m8BD3CF68062A4FB9A9AF96E973F6D60535F707E6,
	QuickDrag_On_Drag_mB9EE5722E773F2DBDE43F27254C5C4DAB706CBE2,
	QuickDrag_On_DragEnd_mCE34654E155520FC30304ADA65516B48433E2ED7,
	QuickDrag_GetPositionAxes_mB7B7BFF16546E8AEFD8D89F9210CB70B10CBF4D4,
	QuickDrag_StopDrag_m6873FF5D48404A5F6A274E2CB07FB482647AA581,
	QuickEnterOverExist__ctor_m45745191648DF5CA2B6CB59D10C880D474C78A9C,
	QuickEnterOverExist_Awake_mD43B550F7674D42063A0210EE6524EDBFCAB772A,
	QuickEnterOverExist_OnEnable_m543C5F88B64EDAE0003ED0F3F7D7ABE118D373D3,
	QuickEnterOverExist_OnDisable_m51E0DBA91617D2CD42EC515AD791EFA0F0EA54ED,
	QuickEnterOverExist_OnDestroy_m9E657D19F7F90868E03B73C893853C6789FB8330,
	QuickEnterOverExist_UnsubscribeEvent_mED5FE96400B9B6419788501B95D5D1DAC536F6F6,
	QuickEnterOverExist_On_TouchDown_m7B2694710E65BC8B0003053152ED229837255FCF,
	QuickEnterOverExist_On_TouchUp_m6B5864F962785D029F6B7D65BCAA3570350BF52C,
	QuickLongTap__ctor_mAFE9C84FDE30871F077AA01A7D7742D0C8244E3C,
	QuickLongTap_Update_m169423D6A4B878EC9B751B540697AB14F33DC842,
	QuickLongTap_DoAction_m6B871E605256A82D282ED7DF9CB9216FDD71CB40,
	QuickLongTap_IsOverMe_m84C139027D1AFE6C71025BBA2758C15341E097E3,
	QuickPinch__ctor_m8F793548382B7D49449030BC5455A433EDCC145B,
	QuickPinch_OnEnable_m36023D422F5AD602BB485B09AF1AE0502703426D,
	QuickPinch_OnDisable_mB0B2879F18F4965FABBC2886A33F6051159BF56C,
	QuickPinch_OnDestroy_mADFAC4A8B8C943B9034D053E0DE5155073F4679E,
	QuickPinch_UnsubscribeEvent_m8897B207A1EC343B14999DA19A1D55DA333B8966,
	QuickPinch_On_Pinch_mC647D6DBDA1B9C3419486F24552D60CBB024AB25,
	QuickPinch_On_PinchIn_mEF4525419DB887CF149FEEA839B12C77AE169CE7,
	QuickPinch_On_PinchOut_mAC09D4C91014F598924F31D33C2191B737B2FD4B,
	QuickPinch_On_PichEnd_m7E45F14CE8D8513B2E9DB00DFA0A5E1A86CA8F3D,
	QuickPinch_DoAction_m4A3D63434FB0C6B37F3C928120A2BC6264F3A158,
	QuickSwipe__ctor_m103A8C68349269611D91BDFF1E6AD2E35294EF50,
	QuickSwipe_OnEnable_mB29973394673BE23B0D859FE0C9BE23842815D25,
	QuickSwipe_OnDisable_m039EA35198A34D51ED7DD595973B70E48ED3FCD5,
	QuickSwipe_OnDestroy_m8306B7AC738B5A00D4979889A877E1F24403E678,
	QuickSwipe_UnsubscribeEvent_m8A5BDA871105F3001EED8C9F16CA44EACCB8EAFB,
	QuickSwipe_On_Swipe_mBB46FA3C72D3E3686FFC2E190B23157255A721FA,
	QuickSwipe_On_SwipeEnd_m6283D9F39D34244AA746DDE7DDA4768D6355BF79,
	QuickSwipe_On_DragEnd_m20EB1EEC594291E9A239C24CC69B4A19D411AD23,
	QuickSwipe_On_Drag_m3A19FFE4C70EC9AB043CBE8447D0FC5D7509B135,
	QuickSwipe_isRightDirection_m97109BB53C5F213D861165089E721339447D5EAA,
	QuickSwipe_DoAction_mD88BF66FA14D6275559DEB52F8C8F67FA8CF7874,
	QuickTap__ctor_m1387DACA0C92859479CDA62FC7A5B871EE75E8B1,
	QuickTap_Update_m68EE78BE4DA5D6A3F0EA62DB9F825FB8CAEB4D09,
	QuickTap_DoAction_m1D43666F9A68BE70722D7E301E8EA6B775827B85,
	QuickTouch__ctor_mFA53C0A1FE6D0E1A69282A21228C8BAEF819EE6B,
	QuickTouch_Update_m52C4F9B55578F6495AFFB9E945BBAE1B5AB02512,
	QuickTouch_DoAction_mF1E1FC42435E76F97BF5992B6035A19F8137F64D,
	QuickTouch_IsOverMe_m4E984CB8B62A7045B5AC6AC016DFEDBEF867D251,
	QuickTwist__ctor_m124B147FCFB74267018F7730FC130CF3E7A1A0D4,
	QuickTwist_OnEnable_m150C4CF1C506FF577C99254C13A2792F39DC292D,
	QuickTwist_OnDisable_mFB6F2B148F78C5200E7B62B45B844970ED5090F4,
	QuickTwist_OnDestroy_m8C74910DB2918A0DEBD24F0B6E74F25F6DB68B17,
	QuickTwist_UnsubscribeEvent_mFD4B62BD703DFDA7197EDADA6126B560C2AC4064,
	QuickTwist_On_Twist_mC5A58E17BE4D2B1ECC30ACCB61CC34FA2F3F1783,
	QuickTwist_On_TwistEnd_m9ECEF22E448E9998BD32E1BC07F9F64F2CE8FBB0,
	QuickTwist_IsRightRotation_m54783811E44FF06BF0E73A843FFB051B120024A9,
	QuickTwist_DoAction_mFD7D4EF117BF67A7D578868CE4F0551EED6F39BC,
	BaseFinger_GetGesture_mF7B1F71FAAA164D2923860675827C788AC97797F,
	BaseFinger__ctor_mD0C79ED74523CE5A7CF4B1989FB0BCC0DC424477,
	ECamera__ctor_mAEB9941B81F98EDEDE6B3B959E70ED4682D974BB,
	EasyTouch_add_On_Cancel_m4D5145DA0A8EA343EB856F5E0DCF407613027C59,
	EasyTouch_remove_On_Cancel_m262C3A4930C3BFFC5360906272C90E5095EE91A4,
	EasyTouch_add_On_Cancel2Fingers_mB18C68401E44AEE9AE6445D30BE84635112D939B,
	EasyTouch_remove_On_Cancel2Fingers_m22C78177398F197A13E7B33A6F752EFA0D548335,
	EasyTouch_add_On_TouchStart_m97F8CDF7FB22E6FB7515E5BB618C41D675A03BFA,
	EasyTouch_remove_On_TouchStart_mC872F67C12E8BEC695AF6214936B10D6BB8AA8DC,
	EasyTouch_add_On_TouchDown_m2D2D5E25BB869F6A6B394F3566EEBE4BAD522EEC,
	EasyTouch_remove_On_TouchDown_m700E4BBE8FFA99A81B81C8E89F213F1EC410A19C,
	EasyTouch_add_On_TouchUp_mA1891CE45733A36181535ABFE611B6FFC194FC6A,
	EasyTouch_remove_On_TouchUp_m0BBFA87933DF34D98AB73029DE067840F76F0E03,
	EasyTouch_add_On_SimpleTap_m1B25B3C1E31A7094B98F01BBEA5B1F45F49E4579,
	EasyTouch_remove_On_SimpleTap_mA776F5C495F3EBA34F60D4307AE0758E5CA54FA3,
	EasyTouch_add_On_DoubleTap_m0A710EFE4C192FB6F656C93333CA349B23B337D4,
	EasyTouch_remove_On_DoubleTap_m44A74B353A123E9B3F05E2A872E4058761170769,
	EasyTouch_add_On_LongTapStart_mF8AD6896FC1ED129681A0A50F51078241D5DA6D5,
	EasyTouch_remove_On_LongTapStart_m700757D9AD133688F0CFE9FE80AE78FEDF9A82C1,
	EasyTouch_add_On_LongTap_mA903F36D7BFFB45C746DAC633242938A61BF2F2E,
	EasyTouch_remove_On_LongTap_m0325ADBA5E88F4CF9CD718882B58FDC74E705EC3,
	EasyTouch_add_On_LongTapEnd_mD65B26DBF12E1397C495C3DDBB69983B66366CDC,
	EasyTouch_remove_On_LongTapEnd_m8E8330E95476B3849943009570935DE70CFD8952,
	EasyTouch_add_On_DragStart_mF0B097955BA74D6610087D5C46BBDE92B4BB6B98,
	EasyTouch_remove_On_DragStart_m92AE8749EB717A9A116196D70E09C7036A21B6CA,
	EasyTouch_add_On_Drag_mA55E17CD90BD1075069EF21247F1BCB0B10471CB,
	EasyTouch_remove_On_Drag_m40E4B7188D26CE7D4B6D22C708A8182A0DB2707B,
	EasyTouch_add_On_DragEnd_m3E9D4182FE045CD275F2B7290CFD077732031CB1,
	EasyTouch_remove_On_DragEnd_mFA68FDC4D3AF626D991B96184935B6B6C4B28F0C,
	EasyTouch_add_On_SwipeStart_mAA683134929C0EE754C0D14C977F153325F85511,
	EasyTouch_remove_On_SwipeStart_mE5FD140158400800623D7F8E39E2FED30D555626,
	EasyTouch_add_On_Swipe_m2576D5A79E42B54FC3696626945275237DFD4B95,
	EasyTouch_remove_On_Swipe_m318B258AD60224EBF8DD2ED6C5D5256C7DD911C1,
	EasyTouch_add_On_SwipeEnd_m542B113825441F578D5D86E8E53338D92FEBA493,
	EasyTouch_remove_On_SwipeEnd_mE4232BD4DF53D33D2B9869B165372B30E0BBBF2A,
	EasyTouch_add_On_TouchStart2Fingers_mD03C23CBCDA52A3698559EE3676449F23D9D0FC0,
	EasyTouch_remove_On_TouchStart2Fingers_m0EB5073AE67ADE0392A32028AA8EE2142E7EFC5F,
	EasyTouch_add_On_TouchDown2Fingers_mCEC9C49CDAC504B09D7E9BCDE555469C3DC7EBF8,
	EasyTouch_remove_On_TouchDown2Fingers_m8FC281BAD18B3C3C2B9A87424BB7573424CF122A,
	EasyTouch_add_On_TouchUp2Fingers_m039A663F83056984EA1F7C82CE246C3040CF6556,
	EasyTouch_remove_On_TouchUp2Fingers_m6E4F3B68FA23F90751EC9D4E5E90FF58ED717122,
	EasyTouch_add_On_SimpleTap2Fingers_mC19305DAB55E6BCFB82C8FB6E4123255B82E7738,
	EasyTouch_remove_On_SimpleTap2Fingers_m1FF34C9A9AEDAAD4145E7DE230A27009F229D680,
	EasyTouch_add_On_DoubleTap2Fingers_mA847C67B0A30C81BC9A12D4FD3046C8F094047E1,
	EasyTouch_remove_On_DoubleTap2Fingers_m4D6E01A1404E74A7C937B777B442320A9AD139BD,
	EasyTouch_add_On_LongTapStart2Fingers_mD6EAB281C23078CDBB179CF030DF06364730DCA2,
	EasyTouch_remove_On_LongTapStart2Fingers_mC01DA2731180F0C2282F828900820B9DA907D1AB,
	EasyTouch_add_On_LongTap2Fingers_mEEBDCAA62C01A97A50392545C95A826662D0B109,
	EasyTouch_remove_On_LongTap2Fingers_m45F4803E90895FAE4523272CA320188C07360A06,
	EasyTouch_add_On_LongTapEnd2Fingers_mD897971A4E5DA92088145FF69C32BCA7C5F16BE5,
	EasyTouch_remove_On_LongTapEnd2Fingers_m3BCFD86E017A40A1E7CF6F187EC582A0402428CF,
	EasyTouch_add_On_Twist_mE80E52A3010E98705B3319F87DDC34706601239A,
	EasyTouch_remove_On_Twist_mF6873B2804131E986AC0C40F390EC4E487A466D5,
	EasyTouch_add_On_TwistEnd_m46230202EA91287F5E52C55CDD43F0611B8C0166,
	EasyTouch_remove_On_TwistEnd_m27D30CE3446F715A85B737E2F6B9825D43DEB8C7,
	EasyTouch_add_On_Pinch_m3AB86DB5A8558B4BA19968EE71452F82CACA6352,
	EasyTouch_remove_On_Pinch_m0144B1E20FF8546AFF793BBDF6794007FD2E4BE1,
	EasyTouch_add_On_PinchIn_m54185BB2A30BE59E7293FF8080109CD9F0F8D369,
	EasyTouch_remove_On_PinchIn_m2633DDA06984988206E09C2A20B4753F16EAA81E,
	EasyTouch_add_On_PinchOut_mD86FF1D1540B2E52258D90B6125BB2C5F2A36787,
	EasyTouch_remove_On_PinchOut_m3C345E56635517A6E1473EE206A2310CCFC435B3,
	EasyTouch_add_On_PinchEnd_mCE516A6547AE177F70D6CC019679865ECBD7B750,
	EasyTouch_remove_On_PinchEnd_mC95CE1CCF992C035FD5F11F1EC580B002E71213B,
	EasyTouch_add_On_DragStart2Fingers_mDE91C6A5A0E98EF2EFCB655FAE2FBAA27DA3BB17,
	EasyTouch_remove_On_DragStart2Fingers_mEBEF7A5C1788FB0D372C873EE7D3CD04C514689E,
	EasyTouch_add_On_Drag2Fingers_mA7EB90CBF64A6EE2D985AC3DB16795DAD80E5113,
	EasyTouch_remove_On_Drag2Fingers_mC6D908A3893EF1F9E966C8AA502FD786B859D8C2,
	EasyTouch_add_On_DragEnd2Fingers_mE2E7397978E3B634279BDC927E61CB9B92887B0F,
	EasyTouch_remove_On_DragEnd2Fingers_m36A33E27ABD55E19913DA5C8FB24BA6CE4DE00D4,
	EasyTouch_add_On_SwipeStart2Fingers_mD066B4DF4DED0C2A5ECDC0B63E2E23F85970DF6B,
	EasyTouch_remove_On_SwipeStart2Fingers_mC014048396BCDB5E414C8CB51E2FAE989CD3FCDA,
	EasyTouch_add_On_Swipe2Fingers_m29F2B727DF6BB6F5DA51998D5F391A46CAB590AC,
	EasyTouch_remove_On_Swipe2Fingers_mE50D63E2CB409D988CF47532116E799801F7C753,
	EasyTouch_add_On_SwipeEnd2Fingers_mACCBCCA09EE9D34F296C454A4A7BB284E623A456,
	EasyTouch_remove_On_SwipeEnd2Fingers_m52A6FE0854E1376170C0128AD1240929AF3CE3AD,
	EasyTouch_add_On_EasyTouchIsReady_mF99BA5EC1FF2B801A27773C810930177677050CB,
	EasyTouch_remove_On_EasyTouchIsReady_m10DC1059571E752245E5AE35A75BB92DD96EF451,
	EasyTouch_add_On_OverUIElement_m3A660E8E80D2918A54B419709040A8644C4DC195,
	EasyTouch_remove_On_OverUIElement_m872CF9AFEA620D714F9FD66AAF1685FDBB1E55DF,
	EasyTouch_add_On_UIElementTouchUp_mF003C43244A08052314C00E9889D43598A9E6AA6,
	EasyTouch_remove_On_UIElementTouchUp_m796074782B0C81C05662957C991B7FA77F46F4DB,
	EasyTouch_get_instance_m4EC32DCD4E6D3467A303A43E0BAA580C89C62943,
	EasyTouch_get_current_m8C25C504633E944652E3554BE2990F60975047D3,
	EasyTouch__ctor_m824044C7397F0A86844B20D3167923B7AB57020F,
	EasyTouch_OnEnable_m4F120C46FCEA5247A811CD29DC492D1736F68815,
	EasyTouch_Awake_mD613403936D28738DC2DFA5697D9289340D54F13,
	EasyTouch_Start_m92693B8343FE8A80CE5C3F9E012DDF885F9059E9,
	EasyTouch_Init_m4D6ABC5FE7775C2D015DDB12BC1ED4ABFFF3055C,
	EasyTouch_OnDrawGizmos_m683D503BF36A525A68CF90FE155842EA867C7C55,
	EasyTouch_Update_mBF4D18A7FFABBF9FFEB4EC2BC0EE96E893D64610,
	EasyTouch_LateUpdate_m4207884C8CD599EA280F89A49C3F019EC3D5B696,
	EasyTouch_UpdateTouches_m99BA926BA28D6316F033DCCE4082068288F8014F,
	EasyTouch_ResetTouches_m4442CCA6BCE69F9FF5C3032B18754EA1DEE3EBC0,
	EasyTouch_OneFinger_m806337BECEFA4DC518E18003CAD5EC8A62CE7841,
	EasyTouch_SingleOrDouble_m996FA47004CD12E785054B3C38C4B1AA630B9145,
	EasyTouch_CreateGesture_mEE109C3593F2E4C759A65EB6D6316D275DC951A5,
	EasyTouch_TwoFinger_m9A8625AB639E6925EFAD3A5CD425F7DB9B76ACFA,
	EasyTouch_DetectPinch_m6650DFA2E65849D969D5123B9C7A62A1D427BBCA,
	EasyTouch_DetecTwist_m80B77AAE6D2BEB920C203CFDB75C8F5BFF3E4DEA,
	EasyTouch_CreateStateEnd2Fingers_m85DCDF49B29F87CF4976583CCAA020660D7A929A,
	EasyTouch_SingleOrDouble2Fingers_mD214C9F818629F80E85A1FF8FB52EDAF00ECEBAF,
	EasyTouch_CreateGesture2Finger_m85C3576FF6EBE5F0712DF9B89B82CEC1254C83C4,
	EasyTouch_GetTwoFinger_mD7E6D39FE16064E58F38CDA8E3DDCF7F07FEEBB2,
	EasyTouch_GetTwoFingerPickedObject_m0D0A8EDFFB1F511AD62493A2709B899C8B69A8CB,
	EasyTouch_GetTwoFingerPickedUIElement_m4CC3B79E8EEE4A8E27961B7CE992FD64D520D641,
	EasyTouch_RaiseEvent_m6D8729C26586C4869FA9B065AAB64C703810B01F,
	EasyTouch_GetPickedGameObject_mB62261DA4CF662819F8F6769FCC0B1EF9D5A895C,
	EasyTouch_GetGameObjectAt_m64D43562BBF1119733A60012FB154D784CDC95AA,
	EasyTouch_GetSwipe_m78BDEFB22EDEA16F70306BBC477ECB8DB6D6A678,
	EasyTouch_FingerInTolerance_m5E7C26AF42914EC105AE3E77E20AE1C503580C95,
	EasyTouch_IsTouchOverNGui_mCD9F37AC010E5F2FEDC9D14F490761278F575555,
	EasyTouch_GetFinger_m31FC29DC8EC219128C62F9E56EE050BF4AD0790B,
	EasyTouch_IsScreenPositionOverUI_mA9C4CE91D3E71A51291724D773918E9B7497D625,
	EasyTouch_GetFirstUIElementFromCache_mA1CFF57052EE8934FF6E7F02767BE2F18652F463,
	EasyTouch_GetFirstUIElement_m57BFC575A199CADEEA6956B38DD8B359EE779BFB,
	EasyTouch_IsFingerOverUIElement_mC9476A13C2709A91FEE5EE78493BE3B49ED17867,
	EasyTouch_GetCurrentPickedUIElement_mB74BE964259134F74D391B761B9B9B8D6305167B,
	EasyTouch_GetCurrentPickedObject_mD4CC0C83EF24E46C2C20A86D2A1E91524965131F,
	EasyTouch_GetGameObjectAt_mE60FDEE97CC006E13E3745D79F10AB85E7CDA5EB,
	EasyTouch_GetTouchCount_mE6679BE58C7B97EB10AA3E9A46A90E262E5DA8C7,
	EasyTouch_ResetTouch_mB3E1E99369B9B6066AEC529E6E88582540FC193A,
	EasyTouch_SetEnabled_m85A9A6BF4391266CA761756271CD1E55258189A8,
	EasyTouch_GetEnabled_m6DAC0A4CD5D581F18C9C4C65C2A3F2BD6AB55871,
	EasyTouch_SetEnableUIDetection_m64F49C1FDE241F2108DECF8A33FE16C58EBE6CB4,
	EasyTouch_GetEnableUIDetection_m2BB2C11AEA3BE7F4FFDB5B2C72692FB880C74C5E,
	EasyTouch_SetUICompatibily_m9DC3C352B33E7DF4B4DAED2FB5C49D6AEA94551E,
	EasyTouch_GetUIComptability_m78DC33E3DC502174F63540717F6279A7FB98F6CE,
	EasyTouch_SetAutoUpdateUI_m531462A1FD65B2D14A99498CF59FB1564B651315,
	EasyTouch_GetAutoUpdateUI_m75A374651F6D9A960D8415E2E7B28EFB92A5F20A,
	EasyTouch_SetNGUICompatibility_m51F6DAE53AFC53F2B490CCD1F954A57424DBA560,
	EasyTouch_GetNGUICompatibility_m3936DBC6E3571FB3FFC253B7CA939B01818E8C3E,
	EasyTouch_SetEnableAutoSelect_mBC19F466F18F576FACF7BE176C22ADFB4C4A73E3,
	EasyTouch_GetEnableAutoSelect_mFFDA2D74DD630D0F2480DB805E1C81F57A0B2F0F,
	EasyTouch_SetAutoUpdatePickedObject_mC8D1679142EE30BD01C7823CCF2732E5BB362A3B,
	EasyTouch_GetAutoUpdatePickedObject_mDF7C4BE1F832633FA734BCFEC4A58E8028BCF222,
	EasyTouch_Set3DPickableLayer_mCA629B084DCD7313141549CF0C2FD0FC5FDFED08,
	EasyTouch_Get3DPickableLayer_m249C0E5A794FFEAE5FE2F4CC57D09E8FE93DBDD4,
	EasyTouch_AddCamera_m0057E4A6D6BD214077BF5A3F262EB873121676A3,
	EasyTouch_RemoveCamera_m6075E81E6429E1E3F94AAC7955384126F2FEB415,
	EasyTouch_GetCamera_m4B1FD6D6C5AA8797F4B104F9DAE393194BB407B5,
	EasyTouch_SetEnable2DCollider_m3CED9249618DD25AC476D21995F118A21EB01256,
	EasyTouch_GetEnable2DCollider_m6FCFC0EA3C1CBCACD2B5C9544B038484DC61AE81,
	EasyTouch_Set2DPickableLayer_m724E88D020966DBC275B4BC2CB4AEEF10BDD2F8A,
	EasyTouch_Get2DPickableLayer_m97DEA9089481FA9510012506DE87AAC3E0F0EC9B,
	EasyTouch_SetGesturePriority_m4378BBF308B3A17CF3BCA736392E03C88C183FA7,
	EasyTouch_GetGesturePriority_m838B2DD8C6D1FB57C6DA91ACC946C7F68F5E92AF,
	EasyTouch_SetStationaryTolerance_mDD24F3EEB7F9A0E1AA8CC994650380057084DD00,
	EasyTouch_GetStationaryTolerance_m1823E6257577CB4FD1292E492E1165E3583FC032,
	EasyTouch_SetLongTapTime_mD1398D865EBB7E57540388CE4AA1DC11B4741EE3,
	EasyTouch_GetlongTapTime_mCEE03CE98A2FBCA016587DD572AA4D604A41CB32,
	EasyTouch_SetDoubleTapTime_m3E41B76C100E04E9A0A3F26730DF1FBB3F14C953,
	EasyTouch_GetDoubleTapTime_mB3D692A98C275C754F8B32E0C4D3DEB98BCF2F27,
	EasyTouch_SetDoubleTapMethod_m19F2B51AC054659EAC6FC1D55C2BBFF5B52781D6,
	EasyTouch_GetDoubleTapMethod_m3B741A2D2B448D128D64C4A70B5E96FF77746447,
	EasyTouch_SetSwipeTolerance_m6FCB6CA33E7EAEFF7A9CA80D87BF326DA51E9F62,
	EasyTouch_GetSwipeTolerance_mEA04C3331CD6516FFEA21A924EE2C9F38FBB0E10,
	EasyTouch_SetEnable2FingersGesture_mF8D1C4BB1782A80D57E3FFE35BCCB1D25E162E28,
	EasyTouch_GetEnable2FingersGesture_m7FA8C9DF21ED2B7B800202E9597182FA681D3324,
	EasyTouch_SetTwoFingerPickMethod_m5A0222B380E3DF3FB832BEAC13A7B697D8A014D2,
	EasyTouch_GetTwoFingerPickMethod_m487A50CB29623E9D140F881262E0779F3A2FB9F5,
	EasyTouch_SetEnablePinch_m97FE6AF8236A6A86E84A75A2EFEA216A26172102,
	EasyTouch_GetEnablePinch_m85E34EA117DA386F71BF49AB6D991E66AB7CC4AE,
	EasyTouch_SetMinPinchLength_m262017B03981C629A93B0CD7FDFB9FD99D5FF6EB,
	EasyTouch_GetMinPinchLength_m4B9B224EA407F22AA6085BBD72E9A1FC0D8D6617,
	EasyTouch_SetEnableTwist_m5F025CF421BE62026A1836F9122901D3AB90CC6D,
	EasyTouch_GetEnableTwist_m28E312D968424062299B918E84FE7E09D306A029,
	EasyTouch_SetMinTwistAngle_mBD36DD5F5278251CE74A5C11995C68709B0CACBB,
	EasyTouch_GetMinTwistAngle_mAA34620E29A3E8FDC3805D4BA3441544ACC9500A,
	EasyTouch_GetSecondeFingerSimulation_m6DD72025E65BDCC8769705F3FB2354BA037C54DB,
	EasyTouch_SetSecondFingerSimulation_mB4E1807A4CF6DD6C9A2D0296D9774DBF0F629326,
	EasyTouchInput_TouchCount_m1BE53940BF325EE91B5BF7D7E33BE40284B4655D,
	EasyTouchInput_getTouchCount_m3892F5EA856BF840F9848FD06D1EC2112EF03C29,
	EasyTouchInput_GetMouseTouch_m9CC7DE29A1CC986941E8EAE140A26BE6811D556B,
	EasyTouchInput_GetSecondFingerPosition_mC68C5BF9B51EA991DBD88B46B076144006FEF927,
	EasyTouchInput_GetPointerPosition_m09D05776571C3CFFB7A45C577FF15F82532E3A4E,
	EasyTouchInput_GetPinchTwist2Finger_m7567D733B56A48BD4978E713C2728FFE97398ACD,
	EasyTouchInput_GetComplex2finger_m4FAD9FD89F22A0D00C39AD3EA5B0B05991347E62,
	EasyTouchInput__ctor_m31B43CF5C38D9E3FF2850632217F1C97611A0127,
	Finger__ctor_mB66F5DA2B6C04E7D0FEB9013FDAFAA5F38FFE941,
	Gesture_Clone_mE8AAE298832ABE6C48BC21498002F3140AF95F60,
	Gesture_GetTouchToWorldPoint_mCEBE61ED58EC849C47A2C093A7BD71DA650BE72F,
	Gesture_GetTouchToWorldPoint_m976B9A97EA151D764514B372966D71E653072BB4,
	Gesture_GetSwipeOrDragAngle_m2F365EE39BDBCE136ABD4084BFA17E62AD9EEF85,
	Gesture_NormalizedPosition_m6C18796533A4088AAA243C4266568FBD563B1EBA,
	Gesture_IsOverUIElement_mA687B34D83B4834C16CF0844039D647DF8F3384F,
	Gesture_IsOverRectTransform_m88CA1E99841A4FA33F5F26D0984E285CA2F79CC0,
	Gesture_GetCurrentFirstPickedUIElement_m67F46284647CF6C76E8355AC5BA745CD85DF7FE7,
	Gesture_GetCurrentPickedObject_m7A51EB9A59A805659444032D910226D63AAB2D83,
	Gesture__ctor_mE9DC73D42CDA0F9A3ECE9F864350B3133C4CD0A0,
	TwoFingerGesture_ClearPickedObjectData_mA3EF8042C8B3FE7BF585E5A19A0145E18D60E6B2,
	TwoFingerGesture_ClearPickedUIData_m0B7DF8262E9E5FCC9B320E614722E87937D7ABC0,
	TwoFingerGesture__ctor_mC9619CF4B4951F2BF145D4F0B5915023DEA16368,
	Coroutines_WaitForFrame_mA0AA42FFF3F9A573764679D1558E5F7C2B5BA79F,
	Coroutines_WaitForSeconds_m0EB5337B82053D9EE204EC319FF70952975D4A5F,
	Coroutines_WaitUntil_mC7921F8BFDBDDC27E830B8D41D7BEB6B07EE07F7,
	Coroutines_Action_m2A6B8EFFEE0878FD1B00B495DD28DEF095171525,
	Coroutines_WaitForButtonDown_m27C67642D70FC23F11F74074F2074C978AC2A879,
	Coroutines_WaitForButtonUp_mBB542898CAFB3EC6AEDBA163637101E408899BE5,
	Coroutines_WaitForButtonPress_m0328F1F1D5F0DAC5DF50E655D45493788501F806,
	Coroutines_Race_m76670B85A403903E28908F6A71AA9DB2B005944C,
	Coroutines_Race_m4EA8E23EB30A16F8D8024C1219A3C0E2E0DB9F73,
	Coroutines_Chain_m87F3222221A696E5E8256CE31FC8B4665A6E6F49,
	Coroutines__ctor_m3600C6EDA1DEF571CA83FAAB82053DE042F7C954,
	Coroutines__cctor_m7E9488B6E6D7CA7A7A248D614DA3230200C9BAEA,
	Floats_Quantize_m0F30246CB323B5BE433CF457B05A5B6518F2E6F4,
	Floats_Compare_mF0B65F4315217757FB0AE141CA27B2674B9094B9,
	Floats__ctor_mB50F61E0C5D86218B7714BAA33A10860019DB820,
	NULL,
	NULL,
	GameObjects_GetObjectsWithTagInDescendents_m639E6A23D7DE8CF1CCCE2DEA45C172A3D63D5C25,
	GameObjects__ctor_mABD7C458DD3D0CF5798EE1FCFA0CAE6E7ADAEBF0,
	NULL,
	Singleton__ctor_mEFAE1584B158DD2EDF56FD7FA2520380E04F6798,
	NULL,
	NULL,
	NULL,
	Vectors_Abs_m0E280CD0217D2561AC52F7B3E2026168D0A3EFC4,
	Vectors_Round_m86C6EB0C43E45E3B504C3D3730737E8D7BD7FB72,
	Vectors_Quantize_mD25AD1802EF1A9DE187A6071CE3B858455A147B9,
	Vectors_Compare_m735595D7F044159571B9FED2766B00E5B166A1E3,
	Vectors_X_mBC6C45C90C043138CE002EE5BD3842FF2708D4B0,
	Vectors_X_m114ACA3723318C8B5760D60B9758A86379A97772,
	Vectors_Y_m386845081C8B59C3FAEA849DA586BEB37555A2DF,
	Vectors_Y_mF7FA97E2AFDBA3B22BE9047539907F8EC3F6941D,
	Vectors_Z_mB1BD638C3825FE966A6CDE53E1723248FF937D33,
	Vectors_Z_m2A6C68EE929324CA95C49B04FC6D9500D9FC53E2,
	Vectors__ctor_mC65DA9C5CBDC990DF6FB6C2A58AF886CAABFCC8C,
	CinemachineReset_BeforeReset_m1E3A111E56CCB75928CA2FAACCE7BA6DF0A75912,
	CinemachineReset_Reset_mF6D533C9094AF28176AC6B745E19512CE67BC9CA,
	CinemachineReset__ctor_m3B44817769546897220DDEFEAB667177E4012008,
	CinemachineScroller_PostPipelineStageCallback_m6F21765AA9A398E0FC2834760A387DD40686FD6F,
	CinemachineScroller_OnCinemachineReset_m7DB11DEC9E1A5446A09CC8D720D2A17D2CC501AA,
	CinemachineScroller__ctor_mEE57F9389BD7C0E144E862FDD691D0D525BC68F1,
	CinemachineCameraLock_Start_mF1D7757EF3895E18A56E1715EA241CB5AB0269D2,
	CinemachineCameraLock_PostPipelineStageCallback_m97B81EE4E9221DE7B4D5730D425A259438498346,
	CinemachineCameraLock_UpdateOrthoSize_m82B66A05455A15D8E1BD7B944148849EF3D9D498,
	CinemachineCameraLock__ctor_m4F7F997570161ACFBC5B3E93CC5970619354201F,
	CinemachineCameraLocked_Start_m85F94BBA8C742604B0A44C4A884AE9E997EF7CAD,
	CinemachineCameraLocked_PostPipelineStageCallback_m202090129AA16464C7E32E18919B4237A6960D67,
	CinemachineCameraLocked_UpdateOrthoSize_m6695B5590EEC0C7554E72E100A3CFB5D6D2C325D,
	CinemachineCameraLocked__ctor_m0F96ABCD2B982793B344193C0107412594ECE104,
	U3CClearTextU3Ed__5__ctor_mDA3220E75EEEAFEF440DA9015714488192941848,
	U3CClearTextU3Ed__5_System_IDisposable_Dispose_mA8D07EBAC38D595050B720D9F663A6BF6D9E3ABA,
	U3CClearTextU3Ed__5_MoveNext_m95C2615DEB29A7A6A97033C17350C8E87F7457B0,
	U3CClearTextU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCAE7B5E5A0EE41BE4C8FD7EAB527B097529D94AC,
	U3CClearTextU3Ed__5_System_Collections_IEnumerator_Reset_m9F6AE8BA6C20DFB7E83D36EB397AD89F4DBA93D0,
	U3CClearTextU3Ed__5_System_Collections_IEnumerator_get_Current_m2BBFD4DC767989F6178CB2ACE66AFB2D1483E027,
	U3CClearTextU3Ed__8__ctor_m955896B0FE659D09A669EC42B9442CEDC8438D5C,
	U3CClearTextU3Ed__8_System_IDisposable_Dispose_m6008363FC8044B7FF650806EC12DBDB547DA5164,
	U3CClearTextU3Ed__8_MoveNext_m75AEE5D44B18C9D823159ACBD50C82622657AE65,
	U3CClearTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDC2547C244C668B08D127C720A77AD3608133F9,
	U3CClearTextU3Ed__8_System_Collections_IEnumerator_Reset_m0D14AD5A2B1F9A696B73EB5D8B72F30242AD3AB0,
	U3CClearTextU3Ed__8_System_Collections_IEnumerator_get_Current_m996F24410DC74B3B5E2AD6AB3BDE35AABDE8EACD,
	U3CClearTextU3Ed__33__ctor_m5A0365988303938ACAEE795AAA5A07192095F6B4,
	U3CClearTextU3Ed__33_System_IDisposable_Dispose_m6374DA1AECBB61043EEEEE524141F251A9DDD51F,
	U3CClearTextU3Ed__33_MoveNext_mBE179BFCDA03171CFF08FA4EC30FE6D3773B97F0,
	U3CClearTextU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0691945032A56C5A4DADAA71DD7EAD51AAD563DF,
	U3CClearTextU3Ed__33_System_Collections_IEnumerator_Reset_m9D58F82ACC8038CDFEE022ECA40AF1DD75028D41,
	U3CClearTextU3Ed__33_System_Collections_IEnumerator_get_Current_m438282C9DFF2FE65ABFF39463ECA3FC461697CE2,
	U3CClearTextU3Ed__13__ctor_m6F6345A966FFFCA707EA50CD54A05F16527AC253,
	U3CClearTextU3Ed__13_System_IDisposable_Dispose_m9D72A00A34470E2F4581609B2B310AE87BFEC14C,
	U3CClearTextU3Ed__13_MoveNext_m3DDC80790B4810B781A1D1E2AC9688E064557E0C,
	U3CClearTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA512F4FC0909096C3AD9B352FFB418979C2E46A,
	U3CClearTextU3Ed__13_System_Collections_IEnumerator_Reset_m1BA423BF7CF076A5B68E259769566621993ADABA,
	U3CClearTextU3Ed__13_System_Collections_IEnumerator_get_Current_m98544CA1018CADC0D96D4314FEDA0F60C17CF8A6,
	U3CFlashU3Ed__18__ctor_m2048FB7C31C0C6D2A7DF6A4F287BF455DD3640CB,
	U3CFlashU3Ed__18_System_IDisposable_Dispose_mB0D1776BC7590DB5A82222DCB36601631CA02A81,
	U3CFlashU3Ed__18_MoveNext_mBB2F395850ADF89B54766D0CE5D9E0B0A97A2ED4,
	U3CFlashU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40B8884F47A1E46A4E2F8DA4C5B0B7D5872E0060,
	U3CFlashU3Ed__18_System_Collections_IEnumerator_Reset_m4BA44E5145C94EB9E9A3955CF21D639CCBC1B490,
	U3CFlashU3Ed__18_System_Collections_IEnumerator_get_Current_mEFB3E854FD3C9E6145E2C6C5A38F645E346285B9,
	U3CReloadU3Ed__19__ctor_m10E3352782F6806CB8A611F5FDE1D70CC53DA04E,
	U3CReloadU3Ed__19_System_IDisposable_Dispose_m6E620D2F01D4DF357832C9B16CCE57CB4D74A47C,
	U3CReloadU3Ed__19_MoveNext_m48BC1BB30AC66A929406BB203A37EB0F470BF482,
	U3CReloadU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2BAA1797804457231AC6380742E2FB5354AF31A,
	U3CReloadU3Ed__19_System_Collections_IEnumerator_Reset_m405700A0EFA4C30A525242C9C0C7648DC8CB6734,
	U3CReloadU3Ed__19_System_Collections_IEnumerator_get_Current_m1A4C65E4696C11D6847C47C3E06EDFCDEDD9CE40,
	U3CClearTextU3Ed__6__ctor_m6DED0FCF7AA880B7C9590BD94C727608DA4BEC11,
	U3CClearTextU3Ed__6_System_IDisposable_Dispose_mDFA6A00493A07FF09E38926274770ED3598344D2,
	U3CClearTextU3Ed__6_MoveNext_mB729781387B3CB735198CF956F6FB6D3CB664602,
	U3CClearTextU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0B322F95DA3E3C97D451902AC656544EA82D755,
	U3CClearTextU3Ed__6_System_Collections_IEnumerator_Reset_mA0855FF5B0AE86DFCD68D9596DE7ACE0F1F17E5D,
	U3CClearTextU3Ed__6_System_Collections_IEnumerator_get_Current_m88A2C0970266795788FA7B0D610FEEB7F63ED8CB,
	U3CUpdateVirtualControlU3Ed__78__ctor_m72AEB99A78376FDE719F0314490D72F444BB1F25,
	U3CUpdateVirtualControlU3Ed__78_System_IDisposable_Dispose_m9E12504502CC46446F2678B480040D71217A40EE,
	U3CUpdateVirtualControlU3Ed__78_MoveNext_m4D9AE1427823CBA07775585380EC52D74C377052,
	U3CUpdateVirtualControlU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD211A1E514AE8B61BAF80864B08873507F7ECDE7,
	U3CUpdateVirtualControlU3Ed__78_System_Collections_IEnumerator_Reset_m4846B823F6D048179D18213C2A534139F85BE447,
	U3CUpdateVirtualControlU3Ed__78_System_Collections_IEnumerator_get_Current_m24FD56720237AEFB8EC085716079768896EA67E2,
	U3CFixedUpdateVirtualControlU3Ed__79__ctor_m01334AB807C6D4C2B8D9B4E59DA615BC0290A4FB,
	U3CFixedUpdateVirtualControlU3Ed__79_System_IDisposable_Dispose_mAB7CE5FC00CC6AFD4C442C132B8B5A2759F59674,
	U3CFixedUpdateVirtualControlU3Ed__79_MoveNext_mFE61B0DD5FE4B9A313C63B7E310B9523728CA4FC,
	U3CFixedUpdateVirtualControlU3Ed__79_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31C41CFC887DB825FD2A4AF21DEE6204C3F5ADD0,
	U3CFixedUpdateVirtualControlU3Ed__79_System_Collections_IEnumerator_Reset_m1B29D91DB7AE39401EBB581033FA381439E1C981,
	U3CFixedUpdateVirtualControlU3Ed__79_System_Collections_IEnumerator_get_Current_mA79575D57F37076AD5623B46ED8297A858A723B0,
	OnDownHandler__ctor_m93B6A51F50BD40566C1FE58990EB375ECA21298B,
	OnPressedHandler__ctor_m28878A4BCF5FDDBF7D980DD5817B8BC77A1BE1FB,
	OnPressedValueandler__ctor_m21F1922ECC349714C94B1D822D11B119F12AC6A9,
	OnUPHandler__ctor_m2D540E1E94E75D518B5A8B0E92258E2B034C5C0A,
	OnMoveStartHandler__ctor_m7FC3F602C9C087D21D27655B813C9C9764BB487E,
	OnMoveHandler__ctor_m5244788550A80DA961111BFE124EF1F83B66CDB7,
	OnMoveSpeedHandler__ctor_m66027EE94B25C09247F22CAAB2611C6F9AC16F07,
	OnMoveEndHandler__ctor_mD58DCEE10D0B0445FFA56722C6450A79DA65D45D,
	OnTouchStartHandler__ctor_m3842E61288A0A3B09FD01311E0448DCA6FD2D0F9,
	OnTouchUPHandler__ctor_mC8BC8BC019F03E1E163CF82998C429273C8FD460,
	OnDownUpHandler__ctor_mC2B4C61B05060626BF59BCD6F089C98D070490D5,
	OnDownDownHandler__ctor_m2D00DB8AE032082B4AB482C71E160AFAFB105FCD,
	OnDownLeftHandler__ctor_m2595FD0934555F147F355C2214516CB4DBBFDC44,
	OnDownRightHandler__ctor_mDD7E4F1CB4852FD3BEE34B63672D3BF8552EF7CD,
	OnPressUpHandler__ctor_m9F29A61660E6E08C50DDA41E92F99F19BD7BBB6A,
	OnPressDownHandler__ctor_m022DB37BFF63C920BADF3D75C6F93FBE82ADF8FB,
	OnPressLeftHandler__ctor_m9436AB5B62DEEB59B2AEBC412D151649182C4D26,
	OnPressRightHandler__ctor_mB28FE4675BC2A131E7D41DED06D4878E08107F54,
	OnMoveStartHandler__ctor_m06FE580DBAF06C7D8E649610BE79799B09F4F599,
	OnMoveSpeedHandler__ctor_m5A49A40C138DECD2F02A1006C9A1D7D04B9882B8,
	OnMoveHandler__ctor_mD597C433A8D8ED1359841A25E96B4C531A4D3249,
	OnMoveEndHandler__ctor_m385C13126EF35D57F4F15E0713A9AFD512C69A93,
	OnTouchStartHandler__ctor_mC93695E469FAC9337878F52A60717C6BD39A4A95,
	OnTouchUpHandler__ctor_m52D55EB72C51CD19C65937291C87D67E5DC9C6BA,
	OnDownUpHandler__ctor_m8926E1E460F857FB3AF6AA9940582F74D475A694,
	OnDownDownHandler__ctor_mBBD70B2DF201E88BA5F9EBA3E3862D065FC3B1F3,
	OnDownLeftHandler__ctor_mB1293C417209A8489F276944131CCFB055B2347A,
	OnDownRightHandler__ctor_m38998CAC7CE68B4C03CE31EDDD199BF268F99B84,
	OnPressUpHandler__ctor_m2B7507FF8C4677E43D725FBF18EB3F84D196F1D5,
	OnPressDownHandler__ctor_m286C6004E40A27683328D955F3103AC7EA77E0BF,
	OnPressLeftHandler__ctor_m433BD00D145FD6688E6327A05FDBB3B4ADFBA849,
	OnPressRightHandler__ctor_m57CEDF48C7DF8B606E6F0C71D0390D88DE57F53E,
	OnMoveStartHandler__ctor_m99E8D97B4C663510E76BFF44A2CEB03C30BF99A0,
	OnMoveHandler__ctor_m2B46243974FC1117D47B64BDBBDE1D840BF0D9A3,
	OnMoveSpeedHandler__ctor_m4F01848068DB66578C96A0C1CF0414DBD208EFC5,
	OnMoveEndHandler__ctor_m89B6D0C441F303A45F12F91D165DA8C791410FE0,
	OnTouchStartHandler__ctor_m03B47CEE0965B21BF15B1B5BF2894F636B9D4D15,
	OnTouchUPHandler__ctor_mE3376869AE8BD288E3752158FED9438ED0F31011,
	OnDownUpHandler__ctor_m4B9FDDB4714B8CDB13117E282A3D982A6EF20B58,
	OnDownDownHandler__ctor_mF5A334B5BD0F79AC56F66B46A3F8EC8E237DA188,
	OnDownLeftHandler__ctor_m37D06E1D25C0B4E3F255F441139E3363ED208C71,
	OnDownRightHandler__ctor_m2B270E98D1051F436995199D5F3D86CA1269E7A0,
	OnPressUpHandler__ctor_mCD29BE9CBE99449746234B2BB10734DF0D13837B,
	OnPressDownHandler__ctor_mB99093FB2AAAF576BDE98E90D2D4969CE9353B89,
	OnPressLeftHandler__ctor_m284B395FB2769D5E7D0E824A8B5184CB1171686A,
	OnPressRightHandler__ctor_m0AEF16D293378F58AEBB351DFC3FE911241FA3B4,
	LevelInfo__ctor_mDB0BDCF0FD16842E6497774417DA959F7DC23D2A,
	LevelNames__ctor_m275A0B502D3249CA289866E5C2A87D528721CA39,
	U3CSmoothAxisXU3Ed__18__ctor_m618185CD9E6F70E6643C4AEAB0BFFFD061915E98,
	U3CSmoothAxisXU3Ed__18_System_IDisposable_Dispose_mF61EEE6F95BBAF717FF632F37E7B3899E5EB55EC,
	U3CSmoothAxisXU3Ed__18_MoveNext_m592CFB1065FB407A98735AC5D5293D156B88CBE6,
	U3CSmoothAxisXU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BA4D63F2F0F1555190A094391F394EB06B66AF3,
	U3CSmoothAxisXU3Ed__18_System_Collections_IEnumerator_Reset_m29726E161437933C24C5042042DF3532F3D5E25B,
	U3CSmoothAxisXU3Ed__18_System_Collections_IEnumerator_get_Current_mD14F11C68C8560FC905912B63FD64F1D924D885B,
	U3CSmoothAxisYU3Ed__19__ctor_m7981AF0BDF92A7C236A46CBA34EB9461C7C7EA4C,
	U3CSmoothAxisYU3Ed__19_System_IDisposable_Dispose_m6A8DABBD387113F750EC02D92B15F12E4CC2F4CA,
	U3CSmoothAxisYU3Ed__19_MoveNext_mB822D7F4041307B51D41A5C0A08F4699FE9E8438,
	U3CSmoothAxisYU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14F78430D93C99FE40D28148E449E5E94B58F969,
	U3CSmoothAxisYU3Ed__19_System_Collections_IEnumerator_Reset_mEA1053CB3B7BD323EB8CC8D889E0EED45CA9CC49,
	U3CSmoothAxisYU3Ed__19_System_Collections_IEnumerator_get_Current_m0F32EE171EF0E993630179B65904C46445198C6A,
	U3CSmoothReturnRunU3Ed__21__ctor_m8BE2DAEECAC80A77CEAB0A1B52D8A25AC9563A9B,
	U3CSmoothReturnRunU3Ed__21_System_IDisposable_Dispose_mA64ED2CA149F10BBB6EC2AC8F6CDA2B363D9E8BB,
	U3CSmoothReturnRunU3Ed__21_MoveNext_mFB7EEEF064E187AA69ABE7144CB1692EC873019C,
	U3CSmoothReturnRunU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85E9D1569978E421AEC0762A2F8EB5FEA50EB88D,
	U3CSmoothReturnRunU3Ed__21_System_Collections_IEnumerator_Reset_m079D62C652BF893D609D8C3BCD7132B27DB7CD3B,
	U3CSmoothReturnRunU3Ed__21_System_Collections_IEnumerator_get_Current_mE75050DFCB0F2D534669FAB6E8F2DECCAA513A34,
	U3CUpdateEndPositionU3Ed__6__ctor_m2E5DFD43D298A84FD0D3C08D242E35447977E994,
	U3CUpdateEndPositionU3Ed__6_System_IDisposable_Dispose_m3C84CB69D2AE4D95C716CF71EE91BE6A6E3A7E5F,
	U3CUpdateEndPositionU3Ed__6_MoveNext_mBE6C2DEAB7FF3D51E91AD942A85FF81598BBE9AB,
	U3CUpdateEndPositionU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87DBA62BF66BD675A98E06E13C972D32245598A3,
	U3CUpdateEndPositionU3Ed__6_System_Collections_IEnumerator_Reset_m221DA8B3070CE1D740AB50816E8FD937E5D395C4,
	U3CUpdateEndPositionU3Ed__6_System_Collections_IEnumerator_get_Current_mB5F565ECCF06B3CE1C7D40A7D3721C9D3EB7FB9C,
	U3CU3Ec__cctor_mF775470294DE784EF2B0F128293C99F2B432ED86,
	U3CU3Ec__ctor_m3E9F8ED5C0EA18CDE608A8F04C6839D3A8DA4DD8,
	U3CU3Ec_U3CAwakeU3Eb__17_0_mEF3312FEFA1E052097F9855D8E9F6351025BE0CF,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass39_0__ctor_m26A23380C9645C1240D4C62E91D798349A5308DB,
	U3CU3Ec__DisplayClass39_0_U3CShowingTouchZoneU3Eb__0_mBF29D49498DFBFCE2A794E974F0E389113BDD2D2,
	EasyTouchReceiver__ctor_m0FD7DE3C8AA4902F49B96D9FC510B384462249BA,
	U3CU3Ec__DisplayClass51_0__ctor_mC30F02F1BCFB600A75B173F89950824565C20D99,
	U3CU3Ec__DisplayClass51_0_U3CIsRecevier4U3Eb__0_m9ECEBB8B08A5BB32618966BFEF440191F57D22EC,
	U3CU3Ec__DisplayClass52_0__ctor_mA43075E01467504518AC88636257C296968FA73C,
	U3CU3Ec__DisplayClass52_0_U3CGetTriggerU3Eb__0_mA9E174B87F32BF11AC64E27CDDF4B7D7A052CF5E,
	OnDragStart__ctor_m64100962F91435FA5EE6B0211688CCC91587CA9F,
	OnDrag__ctor_m496CFA691DA9DAFCD3A0DC4FA667252F8F299F7A,
	OnDragEnd__ctor_m4D4538B381B6CC94D770835AA23FE26A65BEB3D7,
	OnTouchEnter__ctor_m4F208CB7C35A93FB7ED2030CC8F116850186BD99,
	OnTouchOver__ctor_m2A5FC73B81723DFC4BB3ABA30837E516821F1E1B,
	OnTouchExit__ctor_m21CA28B78875AC8A6F5A7E4B6DA073A1A75D73CD,
	OnLongTap__ctor_mF867810A202EDA7A0FF3EF4972A951587A9CDAE5,
	OnPinchAction__ctor_mDF38FE4D8459744EF620C5D621B13A55661B3514,
	OnSwipeAction__ctor_mFE8D67947899A186D68F52399E991438F26CB417,
	OnTap__ctor_m89E87BA3B5DECBD98E37C13E4C59E3F6434D0A31,
	OnTouch__ctor_m8F8C18DD02B542B09276497F66B3648E20B51073,
	OnTouchNotOverMe__ctor_m98385DC531DA83F83FE4D3C6CF2C5F02C38FB813,
	OnTwistAction__ctor_m7058379FFACE430A7D8D8EFE1F92A2CF2B2B595C,
	DoubleTap_Stop_mC1F788AD746449E6D1A88211AF32A257E51F356A,
	DoubleTap__ctor_m8EAB63864802C41C9468A27B6D5DDCC8229822DA,
	PickedObject__ctor_m7A31B565DC8FCC44B180ED365EBCC5EE01C247DB,
	TouchCancelHandler__ctor_m8A5864CEA52285CAA90BE7E7927454288E26FC47,
	TouchCancelHandler_Invoke_m3E0654AB5EF797BC7B8E643139CFF55A23A04223,
	TouchCancelHandler_BeginInvoke_m82D62C670BFE670A9548FD745B6A465B91F3EC0D,
	TouchCancelHandler_EndInvoke_mF56C48DE4255A404F329E8CC78B2C34ADA440094,
	Cancel2FingersHandler__ctor_m4BD46F0EDBD6066B918696ADEB35F6DE13038EF0,
	Cancel2FingersHandler_Invoke_mE59EB97814730B0D9EBEDE009CFA2AE3AF503674,
	Cancel2FingersHandler_BeginInvoke_mB52C032299723741FE1300286AEBB0F70B4203BB,
	Cancel2FingersHandler_EndInvoke_mED45756BDF7AE509762211106BDE6C7D39CBA3A4,
	TouchStartHandler__ctor_m8B03D7113759D0BA11A017A3766C383F16B8D268,
	TouchStartHandler_Invoke_m5199345EE478AFE4CB78CCD2DB270BFBEFD69569,
	TouchStartHandler_BeginInvoke_mA449C714833BC8D211F40D94C46AC7914A02A833,
	TouchStartHandler_EndInvoke_m067CE9E9F54EF1BF5F401C83B8408C15E29DE673,
	TouchDownHandler__ctor_m266AB85478489C46AE8B9803142E973BB9679DA8,
	TouchDownHandler_Invoke_m38F8C448FB66C6C901A9668669552567824015FB,
	TouchDownHandler_BeginInvoke_mFC20835BC0AED7533F20FCB24BF8C234DF034FB3,
	TouchDownHandler_EndInvoke_m070F681CD8F4A044E3E3A10D97ABC41F33600FDD,
	TouchUpHandler__ctor_mFCAF0BE55F280AB1DD20F6AC020DE2E3B4A1E85F,
	TouchUpHandler_Invoke_mECF868AB2CBCEFC664CAFCCCB75DCCC9AB30B10A,
	TouchUpHandler_BeginInvoke_mB672605F50F31A185867ABE3961A5B7799CC9688,
	TouchUpHandler_EndInvoke_mBA5A22B4E84DACEAEC78FFC695A9F4F15F710AF4,
	SimpleTapHandler__ctor_mA55446D2630A23367F1AB45C316320798972D5D8,
	SimpleTapHandler_Invoke_mE4C1B89D74995327C1320ADAEE347D55628B8A44,
	SimpleTapHandler_BeginInvoke_mF08B6D5FC54B2706AE155E19A6EDBF5BB3CAD501,
	SimpleTapHandler_EndInvoke_mA6A1D5041FB15352046DE29EFBE0E3E6AC177BF0,
	DoubleTapHandler__ctor_mA705BD33AE0BDFF6B579A2CC7CD5F06AED6995DA,
	DoubleTapHandler_Invoke_m5AD3BAEA046E13A13FFD156E57830B40D203F0E9,
	DoubleTapHandler_BeginInvoke_mC4008A27F61138FC7C46D734C94B6BF0855A74E9,
	DoubleTapHandler_EndInvoke_mEC6D9474D1B4EC5ECBAE2A29C09D330D9F984354,
	LongTapStartHandler__ctor_m098BC28395EFF78EA047197050E1F80C3C6ADAA7,
	LongTapStartHandler_Invoke_mE46CFE0BE063E13F67235336CE1D71EB06664C80,
	LongTapStartHandler_BeginInvoke_m1D28143DC7E9E9C8D30BCEC25DF9D649769262F3,
	LongTapStartHandler_EndInvoke_m6EFA9794C89FD9B1F6E2CA2BC064B37811A78C02,
	LongTapHandler__ctor_m7328B96D97AAFF38107A81E1C7421FEB2B3A4F74,
	LongTapHandler_Invoke_m526023808514C2E5B401C2CF5A3F7BAABAE00F23,
	LongTapHandler_BeginInvoke_m69C9B1AD0D0ECF51A54B703F90FEADE6F36A6B50,
	LongTapHandler_EndInvoke_mB0B4210640A93FD7928014C947F3B169BA8ED9A3,
	LongTapEndHandler__ctor_mBD858D265F74CC3CA6722FD43FE590432D488C3D,
	LongTapEndHandler_Invoke_m5EF0F923B54B1CA4E20CBFB162C55511ADA12D23,
	LongTapEndHandler_BeginInvoke_m172750E4F5F13865ACBD503ED68985E86F2BB8B4,
	LongTapEndHandler_EndInvoke_m8F51FFBA028E464DC82E764FBB663D9ED6137B7B,
	DragStartHandler__ctor_mE611DE0BA8B6474E1FC82793012A8735A2556D99,
	DragStartHandler_Invoke_m345A5650085AED28B0E0F942E2AFF536FCBEF5AC,
	DragStartHandler_BeginInvoke_m4D408D1A0560B5EA026E426E7B94A62DCEF1B8D8,
	DragStartHandler_EndInvoke_m6968781233FEE4337B70FB0F60F481596C1C32F8,
	DragHandler__ctor_mAAE60BE8ECE3CD6698B8A4603D140A32CF37F2B3,
	DragHandler_Invoke_m9206AE28FC909A74DF9EDEEF6063ABC1A025A5FA,
	DragHandler_BeginInvoke_m543EE0B4A52D8C088AE5F801F581EDCE2EBB39FB,
	DragHandler_EndInvoke_m27A3EF1E84A0E2B12D9936F7CDC3AE2073A83D25,
	DragEndHandler__ctor_m6FB60688875762340B4924F4243C7B96FE21B706,
	DragEndHandler_Invoke_mA3FBEABEA201D3875774C2ED31C557707314265D,
	DragEndHandler_BeginInvoke_mCC7FCF79D977F7C1A18CEFFDD50A4F9B794B646F,
	DragEndHandler_EndInvoke_m31E46468A75561D38660E29CBE4487BDD8138325,
	SwipeStartHandler__ctor_mA1C7F567E55CAF871CCD63DCA14C2E8478503F47,
	SwipeStartHandler_Invoke_m1B43A735CECDE54F6E06CCE6DAF3589776CAB54E,
	SwipeStartHandler_BeginInvoke_mE410318EE951D1628F254BF7DDBED973DDAAF8D5,
	SwipeStartHandler_EndInvoke_m52EC74B0960FFABA9E199101CFCDA585F6F604AF,
	SwipeHandler__ctor_m9FCE438A3E6B33D36C45514C085D3B7936D669E1,
	SwipeHandler_Invoke_m9100C4F61F5382A2F43FDE4720E4DDC23FDA38E4,
	SwipeHandler_BeginInvoke_m11825638F6DBCD9AC3515CE70F0C04F25C1AE638,
	SwipeHandler_EndInvoke_mB1367E380FBF0F6BDC6D31B52600E3210C873A4A,
	SwipeEndHandler__ctor_mFE11C0F954D62D50B1248AD31E3EDDA7BB1136B9,
	SwipeEndHandler_Invoke_m73AA0A4D33FC100D35865630417F7EB3E8F9F5DC,
	SwipeEndHandler_BeginInvoke_m2599A650EFE9B5067D89477E87D5AC9774008DCF,
	SwipeEndHandler_EndInvoke_m452EB8FD75DFB6C5DF57250E00B32D104F476373,
	TouchStart2FingersHandler__ctor_m5A57220DEE63F3CA8CE1B0292EDE272600335858,
	TouchStart2FingersHandler_Invoke_mAACDCA5A2DF676134CF27B37F1A5789B1CC2F89E,
	TouchStart2FingersHandler_BeginInvoke_m05EB6B424D78161B129F6249D089CC97C3B5CC80,
	TouchStart2FingersHandler_EndInvoke_mCEF01C300C2D89DBFA778A88CFDE3783A5B97DCC,
	TouchDown2FingersHandler__ctor_m49EB38E582B3C5C3DC92D3F30D2097CE0EAE67A2,
	TouchDown2FingersHandler_Invoke_m941317814BC0857A920B0386FADE544F2492CF76,
	TouchDown2FingersHandler_BeginInvoke_m896BA057163BD6AF0D0906CA34A9A3C30418E808,
	TouchDown2FingersHandler_EndInvoke_m327ACAB2FE75F046233782BB14AB8F57238EE7F0,
	TouchUp2FingersHandler__ctor_m5C3ACDED6D1F762F1B92E2D392B8612389D019C6,
	TouchUp2FingersHandler_Invoke_m3912D4F41096A7DFFE344641690D441360C0CBE2,
	TouchUp2FingersHandler_BeginInvoke_mA03D342F28921A2F3928F2026D24D402A9D4736E,
	TouchUp2FingersHandler_EndInvoke_m079312B45F77E392745695084436856558F7520F,
	SimpleTap2FingersHandler__ctor_m8AA13BBCDA12B5701474963698625B52D828586D,
	SimpleTap2FingersHandler_Invoke_m32EFAAF89A211AFC23F021F2672B3428AB2B7A0D,
	SimpleTap2FingersHandler_BeginInvoke_m9AC4F42FC4BE5D6C5EB6B7852AC1DD3DED7AD208,
	SimpleTap2FingersHandler_EndInvoke_m7C66CBFC58596FC5DF595F226008EE02CCEC84FA,
	DoubleTap2FingersHandler__ctor_mCF4A990BD0889F2497F5FD37DB1478EE8D5B7CC6,
	DoubleTap2FingersHandler_Invoke_m5B3971ED7784B82419ACFC92D2CEE927E8253DCD,
	DoubleTap2FingersHandler_BeginInvoke_m4677F668A0CD5ADEDBE284C6A05450B10167BFF8,
	DoubleTap2FingersHandler_EndInvoke_mF28EF3B94D0AC1DB210118923E1F8681F64046EA,
	LongTapStart2FingersHandler__ctor_mCEAE610D6ACD375506E88B77B757791DB6E9ECBF,
	LongTapStart2FingersHandler_Invoke_mBAE911610FB1C863B0DC492E1DD30AC9F5B0BA69,
	LongTapStart2FingersHandler_BeginInvoke_m7F46A2E45BBD9495167DB0DA4262790EAC8B6439,
	LongTapStart2FingersHandler_EndInvoke_m34A2181AC494E5833D7B1BCE18C73FF3F822A5EA,
	LongTap2FingersHandler__ctor_m4962BF8633C3CBAE28F1D75AE74CC7329E59CFF8,
	LongTap2FingersHandler_Invoke_m4090B1761026226A9AB1763B3D69CDD9A30715D7,
	LongTap2FingersHandler_BeginInvoke_mDE601F29D9353B8C30D13981B78A7C44528A5251,
	LongTap2FingersHandler_EndInvoke_m4E9B4DB45505D038DAD2124B66E784F1756A8F64,
	LongTapEnd2FingersHandler__ctor_m855897087EE65EFC27D178330E0CD77AEB5A08F9,
	LongTapEnd2FingersHandler_Invoke_m42A9800B4FEFC6AFC1DB927DEAFFEE0EACC8FE74,
	LongTapEnd2FingersHandler_BeginInvoke_mED5BD5792DEA9C35795FD43A9CA7D37880C42317,
	LongTapEnd2FingersHandler_EndInvoke_mA73C7C7D673050268A615FB3442EEB311EF6778B,
	TwistHandler__ctor_m6F700DD86D8607A52FB171EC6BDE3E36F8878C62,
	TwistHandler_Invoke_mFA9DF10B5611E7186E50CD0E0C3696B8AC28E98A,
	TwistHandler_BeginInvoke_mD208630675FDF308AC4AAA366F71CF3B6651CA22,
	TwistHandler_EndInvoke_m99A8454B89E4E0ABF5215452EA2A1547D8DE9CD4,
	TwistEndHandler__ctor_m23A0EAEC768D7D9F26492DE6F184B39A4BF38DDE,
	TwistEndHandler_Invoke_m40DF7C46BCC948A9D3732978EC2007E56E42C83D,
	TwistEndHandler_BeginInvoke_m79A9AA16B98824222B6F8170EDEFA9381DC0D00B,
	TwistEndHandler_EndInvoke_m25E7BA8A28D439FB1F80E884B55B33D003E28E74,
	PinchInHandler__ctor_mDEDDC6394CD2E1163DB89BF56FA54937156B5757,
	PinchInHandler_Invoke_mDA17E4D55307EABA6016E6BABE27737E24FF248B,
	PinchInHandler_BeginInvoke_mFC9D26FBBC1934F710A4DD8D7A866DBD758353B3,
	PinchInHandler_EndInvoke_mA9BACF0A8141783BDDCCE0A74EF63161C1370E0F,
	PinchOutHandler__ctor_mBCCE794792E19A5CBD71D7E1E96E35B9ABEB1CAC,
	PinchOutHandler_Invoke_m8ED8D2B43115DD0C3B42C32036C6DED3FE8E071A,
	PinchOutHandler_BeginInvoke_mF0AEDFD1274A82F47D31601448583AD3BD499716,
	PinchOutHandler_EndInvoke_m41B8794D85C5EAC92EABE1F525E65C88F05C332F,
	PinchEndHandler__ctor_m640C03B729088E330B629E4A6975F59BDFC97941,
	PinchEndHandler_Invoke_m485731B20ABDE4DC1BE9BFA77F79C4D7B3D3D3DF,
	PinchEndHandler_BeginInvoke_m71D6877B4E86F9471EDE65FFBF575A04907769A1,
	PinchEndHandler_EndInvoke_m99A2D757909CE6653E77118A722B15EA5A760E63,
	PinchHandler__ctor_m56455C5FFAECE920632FE4C6D67A884CADDA6B03,
	PinchHandler_Invoke_m9AB4431B1AF8FD6DBD8636F15C78DC5743606B4E,
	PinchHandler_BeginInvoke_mB80571B2ED9EF8A200DC5337656FCB1534C97983,
	PinchHandler_EndInvoke_mEA49BC6F3961711E38CC2C41E65E748F5FA6680B,
	DragStart2FingersHandler__ctor_m96F1FEE595DC4B38964C2CE9FFBF2D4577802249,
	DragStart2FingersHandler_Invoke_m6D440071D765118B6BC606C4FD4887F16EFB0832,
	DragStart2FingersHandler_BeginInvoke_m2F6DB455D12EA9B4C2001CC3C3E92FF24BCE1878,
	DragStart2FingersHandler_EndInvoke_m613DC46DEE642EB25FDFEEA98D8617A2C1BAF496,
	Drag2FingersHandler__ctor_mE97E75C61AC4B4C0A4AD34A67B30259DD0EAF444,
	Drag2FingersHandler_Invoke_m51ADF190378DCF05889668EDA6D40851FE0CDEDA,
	Drag2FingersHandler_BeginInvoke_mFF7DBC9623D2CEF95E84515303FA93B4473347EF,
	Drag2FingersHandler_EndInvoke_m43E4080019553DEFE8AC75E5FBC99BEAB138ABD7,
	DragEnd2FingersHandler__ctor_mE400CA66F2292D2F12A1A5B67E79ABC72ABE16D7,
	DragEnd2FingersHandler_Invoke_m5DFC66D61D025266B7B33CB760015BE89EBADCA7,
	DragEnd2FingersHandler_BeginInvoke_m8553AE3C96821A9940034C4A93FF5C0D1F0D8AEA,
	DragEnd2FingersHandler_EndInvoke_m363EFD1716FB7D3A15ACE6D3CA786D54EE79948D,
	SwipeStart2FingersHandler__ctor_m4316083F72EA7135ACFFAFE6896C4743941B5AB2,
	SwipeStart2FingersHandler_Invoke_m6D9EE88D856FD25B6212B286138CC79E7E3C37CC,
	SwipeStart2FingersHandler_BeginInvoke_m35E3A0AD1954AFAFA618C95FC8074EBF826FCE3C,
	SwipeStart2FingersHandler_EndInvoke_mE040834B6936A3322D0301EFD40C061DE89D7AA4,
	Swipe2FingersHandler__ctor_m041539F2580BD4669C44668741A7506D0745A5F9,
	Swipe2FingersHandler_Invoke_m9DA4F0016C8EF0E0E9B2A4D18BC162EE1092AE30,
	Swipe2FingersHandler_BeginInvoke_m8CE2E904785EDDC2E90AFA904CAE08B2C03824CB,
	Swipe2FingersHandler_EndInvoke_m21B72C81262EEA213409F64B5BE2ECD6A11DA5A4,
	SwipeEnd2FingersHandler__ctor_m7CF9535201124466FC7577CB425378F9A359BB92,
	SwipeEnd2FingersHandler_Invoke_m88827E5D7D2475E8ACDCFB3DDB3F7FCF7CB8FCA7,
	SwipeEnd2FingersHandler_BeginInvoke_m3BE8EDEB46F9E178D8320F888066EAADBA940A52,
	SwipeEnd2FingersHandler_EndInvoke_mDCE7876B48AE246E4DA1324008C1899A96065F89,
	EasyTouchIsReadyHandler__ctor_m2E2DDD1528BF06980E810F7AD40522BA26F2433B,
	EasyTouchIsReadyHandler_Invoke_m8E135C5BB4444B7F7390517AC45D0CAD88E713E4,
	EasyTouchIsReadyHandler_BeginInvoke_mBDC930ECC0E2BE8A010971AE0C5BFD5AB2C43654,
	EasyTouchIsReadyHandler_EndInvoke_m24E88D84D7ED48D3DE19749FE8104A54CA9837E6,
	OverUIElementHandler__ctor_mF2C60E858E59C4535684347D35701A67E4311B32,
	OverUIElementHandler_Invoke_m480DF4A635B1BCE164069BE31E495EB5C4DE09B7,
	OverUIElementHandler_BeginInvoke_m4A57619288A06B8911AA0C3C206A07EAAF7ED04C,
	OverUIElementHandler_EndInvoke_m9D48D23E3316E70E77078205470AB17005032A1B,
	UIElementTouchUpHandler__ctor_m23B20253F7CCDA5F9370CBDE29DEF00B983E8824,
	UIElementTouchUpHandler_Invoke_mF3085E1B47CB1DFD212B7132E87DDDB5BEBC10BB,
	UIElementTouchUpHandler_BeginInvoke_m33ED64D5A18D82EF897AAC158CCE9EF5ECA2836C,
	UIElementTouchUpHandler_EndInvoke_m075C549595AED4EA4E172AEB6889551AA1D11A13,
	U3CU3Ec__cctor_m121627ACF36625C38CBB53C3BC3C23F137B7307F,
	U3CU3Ec__ctor_mDF1191B9292A9D9617C4DDDE96CF6154D3428D52,
	U3CU3Ec_U3CStartU3Eb__221_0_m45F036D72B2F5C4025699A2D5AAC3D604131FEDA,
	U3CSingleOrDoubleU3Ed__229__ctor_mB4333383948A8ECCEDFCF40641F8AC071202D7A6,
	U3CSingleOrDoubleU3Ed__229_System_IDisposable_Dispose_m3EBA43226484372780A4A3E4F57AE2C4CA013FD1,
	U3CSingleOrDoubleU3Ed__229_MoveNext_mAE776470B126028AD92016A0072D53DBC2EE75D1,
	U3CSingleOrDoubleU3Ed__229_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF450A5AFF7C114E4ED317EB7639B4D5A29CFB0A4,
	U3CSingleOrDoubleU3Ed__229_System_Collections_IEnumerator_Reset_mD89AA06B498871C2C49D0FFAF242D6C50C5AE8B7,
	U3CSingleOrDoubleU3Ed__229_System_Collections_IEnumerator_get_Current_m5629A6E9C71B4D3EB3B2E907CC513323B7093EED,
	U3CSingleOrDouble2FingersU3Ed__235__ctor_m6F57A849F84E06164D13D96D443AA002048D489E,
	U3CSingleOrDouble2FingersU3Ed__235_System_IDisposable_Dispose_m755BCCDA011958799CED29C78AD3DB112AACE6D0,
	U3CSingleOrDouble2FingersU3Ed__235_MoveNext_mC7F7BE0616DD863EC465F83EBA4AE9793FC95CE9,
	U3CSingleOrDouble2FingersU3Ed__235_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D64F0A43FE4786A6D4EE28202CC3D97E2514453,
	U3CSingleOrDouble2FingersU3Ed__235_System_Collections_IEnumerator_Reset_m4140DEEA55AD258A932821C7387A2B66E652DFF7,
	U3CSingleOrDouble2FingersU3Ed__235_System_Collections_IEnumerator_get_Current_m3D57C9C2B1B7BD65CE0E861AAD4F71B6EFE85592,
	U3CU3Ec__DisplayClass240_0__ctor_mFBBFBC99C488B01EBE82089F558370E574FE5B0A,
	U3CU3Ec__DisplayClass240_0_U3CRaiseEventU3Eb__0_mD1F006568BE77FDE8AB9789184DD1032B0C86ADD,
	U3CU3Ec__DisplayClass273_0__ctor_m7BC082D8468BEB05DEE4F8D9270F493B01B468DB,
	U3CU3Ec__DisplayClass273_0_U3CRemoveCameraU3Eb__0_m9304F70819BE2710B16182B9CBDCAF5B9CDAEB83,
	U3CWaitForFrameU3Ed__2__ctor_m3393A075282B64FD849E7A2B221DA7E79CFE3E21,
	U3CWaitForFrameU3Ed__2_System_IDisposable_Dispose_m2484D8268E7FC0EAD9E66CE241345783419387A2,
	U3CWaitForFrameU3Ed__2_MoveNext_m6DAF7132A332341B8A1416B618A5F9E58DFDEEDB,
	U3CWaitForFrameU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4152F0CCD66669658CB639EEE19C9CF9647ABF1,
	U3CWaitForFrameU3Ed__2_System_Collections_IEnumerator_Reset_mF1339A9E5729157C32A022EC18436A36A4A73F6F,
	U3CWaitForFrameU3Ed__2_System_Collections_IEnumerator_get_Current_m831DC4474E1D23A521A92AF05B1F4BF1220263D1,
	U3CWaitForSecondsU3Ed__3__ctor_mE2F7F3F6830CB9899B721B8F78F77C22992F3E44,
	U3CWaitForSecondsU3Ed__3_System_IDisposable_Dispose_m814138890A0775A2C84BA14C61C5E29D949B65F4,
	U3CWaitForSecondsU3Ed__3_MoveNext_m12BCB61E2553FEBB972C09146791FAE83A36F6CC,
	U3CWaitForSecondsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m923520EA704767483CC224B27CC6A6537C202392,
	U3CWaitForSecondsU3Ed__3_System_Collections_IEnumerator_Reset_m21E66D1B7747AA873C81A06BDAE9A370F2FAA4AB,
	U3CWaitForSecondsU3Ed__3_System_Collections_IEnumerator_get_Current_m88647E7460AD12D4E1B9B44DF85C49FC1284529F,
	U3CWaitUntilU3Ed__4__ctor_mE4F7210706628A296660F0104F8F89AEE034D9D6,
	U3CWaitUntilU3Ed__4_System_IDisposable_Dispose_mFE38B4824B8F0AFF7A4C9C8F9572BF1A3B2E87AD,
	U3CWaitUntilU3Ed__4_MoveNext_m6839F902C77A3C230300411DE871FDD74A2A87C6,
	U3CWaitUntilU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01FCD29668F1E8B6BF7178F95F9D66B6E8744EF5,
	U3CWaitUntilU3Ed__4_System_Collections_IEnumerator_Reset_m505A4F0E3572045D1E25808BF450542437F399D7,
	U3CWaitUntilU3Ed__4_System_Collections_IEnumerator_get_Current_m456B75C5925B3192644C6AA21537EC1022A45763,
	U3CActionU3Ed__5__ctor_m57C6F972CE5E96C22D3D09ACD4110B44CB9AC009,
	U3CActionU3Ed__5_System_IDisposable_Dispose_m9F0851F70ED64A448845630FD49CDB1605B9F30A,
	U3CActionU3Ed__5_MoveNext_m5E444CB2C06BAD1672466066EB317288C080B5B2,
	U3CActionU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33DEEE9308FC72C73F1375ABF4069041C0CBC74E,
	U3CActionU3Ed__5_System_Collections_IEnumerator_Reset_mF2E541796E75F31FBB38CB35D0948537E4C694D5,
	U3CActionU3Ed__5_System_Collections_IEnumerator_get_Current_m2FEC24310756FEDB927A4FE2FC5598B1D0512046,
	U3CWaitForButtonDownU3Ed__6__ctor_m1F2B6A6557D217EBE7E71C08C44625C2666DA77F,
	U3CWaitForButtonDownU3Ed__6_System_IDisposable_Dispose_mCA6ACBDC4E87EED178DDCE011C94A8EB44F07D36,
	U3CWaitForButtonDownU3Ed__6_MoveNext_mD2F606638C877BED3C1B948ECBD397BE5728D6AD,
	U3CWaitForButtonDownU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m95055A159A3C4AB10545458A3E536FB82409A40A,
	U3CWaitForButtonDownU3Ed__6_System_Collections_IEnumerator_Reset_mFD08246240DE7E9F9DDD4E153D4C2753E1804C52,
	U3CWaitForButtonDownU3Ed__6_System_Collections_IEnumerator_get_Current_m5885AA15C4B8D979579169536C3D77945CD364C9,
	U3CWaitForButtonUpU3Ed__7__ctor_mFEB3F94193FE387822D93895ECF98C62AC89923C,
	U3CWaitForButtonUpU3Ed__7_System_IDisposable_Dispose_m628FAB931AA88D877C54BBC9647AB27457CECB44,
	U3CWaitForButtonUpU3Ed__7_MoveNext_m1BDD17ED59F2D78A5DF8F7CB3826F6EC40F65CB8,
	U3CWaitForButtonUpU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m170C9F2B2AE070B873D2F6E21B87EC8C61618520,
	U3CWaitForButtonUpU3Ed__7_System_Collections_IEnumerator_Reset_mE44B583FBCC13084C41CE2F8A8CAD8827720BF57,
	U3CWaitForButtonUpU3Ed__7_System_Collections_IEnumerator_get_Current_m1CCEC9336C06F50A4635CA897C27AA86DE8C6A59,
	U3CWaitForButtonPressU3Ed__8__ctor_mEA2D6C9710B871EC60F57E763C97427666EF9A8D,
	U3CWaitForButtonPressU3Ed__8_System_IDisposable_Dispose_mFAC20D0D173D853F3CAFA23A291D1FECC62B3FD8,
	U3CWaitForButtonPressU3Ed__8_MoveNext_mF914F1E59610B2198544EB5E393480C06C8B26BE,
	U3CWaitForButtonPressU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8017717E9A1985AAAC40C26E75837E05429D6EAA,
	U3CWaitForButtonPressU3Ed__8_System_Collections_IEnumerator_Reset_mA0E24EC4237453D7104BC7C6631A345924192670,
	U3CWaitForButtonPressU3Ed__8_System_Collections_IEnumerator_get_Current_m8500CA27747A9BD2287476259E0FE375B63C4A5E,
	U3CRaceU3Ed__9__ctor_mD5897831FBAC0EFA18EFFFE58B76CEF2C0B7CFF1,
	U3CRaceU3Ed__9_System_IDisposable_Dispose_mFF5E731FB438A0EF26880367BEB752ACFFE34931,
	U3CRaceU3Ed__9_MoveNext_m0FCABF5DC99E12C78EC82F92751F14A7CCC76C12,
	U3CRaceU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6CE7926D7A257BEAA5B770F6592C743BC664634,
	U3CRaceU3Ed__9_System_Collections_IEnumerator_Reset_m0A7A77B905E276593FE4DAD5F61DC3DAD5D8DF51,
	U3CRaceU3Ed__9_System_Collections_IEnumerator_get_Current_mF8429966439A6E12EA1A14732E67411AA527D0F3,
	U3CRaceU3Ed__10__ctor_m88BA4CE76EDAFDF372977AC34B5D33F1CFAA7FEF,
	U3CRaceU3Ed__10_System_IDisposable_Dispose_m00D231915A594C6593DDCE04C3890CD937402248,
	U3CRaceU3Ed__10_MoveNext_m777F640A1EA2F44E214A3E011588008C73AF34F1,
	U3CRaceU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F6153F86644F3799A3347CD43B94AC9724B4246,
	U3CRaceU3Ed__10_System_Collections_IEnumerator_Reset_m031E318B75C13C4FB68AC8E779F525CFC4E1121B,
	U3CRaceU3Ed__10_System_Collections_IEnumerator_get_Current_m7B9A001C52E2E9F54AD09EC9CA97DC5ABC0596E0,
	U3CChainU3Ed__11__ctor_m1FDACA750AA275A6AB59E8A9C22AD2F95BE07876,
	U3CChainU3Ed__11_System_IDisposable_Dispose_mE0001334A97EA15453CD5B764D8BA8EFD065EF03,
	U3CChainU3Ed__11_MoveNext_mFA9F832AB0732FBF15AD53CF10CD1FC7A13D14CF,
	U3CChainU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD31FA606FFBAA36F3DA201220D5678E0022B81F5,
	U3CChainU3Ed__11_System_Collections_IEnumerator_Reset_mFD06748712BB79D5B6BD97942F10F0F8F8E72B3D,
	U3CChainU3Ed__11_System_Collections_IEnumerator_get_Current_m412D4EF738160DA264D6ABF98B78ABA8375A9740,
	NULL,
	NULL,
	BeforeImpl_Reset_mE2931C6CD0182BEF620574489FF5DA39AE5D6820,
	BeforeImpl__ctor_mD24B1B211B479982143812B7DA3C3FB9908995C0,
};
static const int32_t s_InvokerIndices[1593] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	31,
	31,
	31,
	31,
	31,
	23,
	23,
	23,
	26,
	26,
	23,
	31,
	31,
	94,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	32,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	31,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	31,
	23,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	31,
	31,
	31,
	23,
	23,
	23,
	23,
	292,
	28,
	23,
	23,
	23,
	1047,
	1047,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	23,
	23,
	28,
	23,
	31,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	14,
	14,
	23,
	23,
	23,
	23,
	31,
	31,
	292,
	292,
	31,
	292,
	31,
	31,
	292,
	292,
	31,
	292,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1047,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	292,
	23,
	0,
	1671,
	23,
	23,
	32,
	14,
	26,
	26,
	23,
	1849,
	23,
	23,
	23,
	23,
	1850,
	1065,
	670,
	23,
	23,
	23,
	10,
	32,
	1046,
	1047,
	95,
	31,
	95,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	1851,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	31,
	23,
	1852,
	4,
	26,
	26,
	23,
	139,
	139,
	565,
	103,
	565,
	103,
	565,
	103,
	565,
	572,
	325,
	102,
	0,
	0,
	0,
	0,
	1853,
	1853,
	1854,
	1207,
	1207,
	1207,
	139,
	565,
	103,
	565,
	103,
	1207,
	1209,
	1207,
	1209,
	1207,
	1209,
	565,
	103,
	1207,
	1209,
	1207,
	1209,
	565,
	103,
	1207,
	1209,
	1207,
	1209,
	565,
	103,
	1855,
	1207,
	1207,
	1209,
	1209,
	121,
	0,
	325,
	102,
	325,
	102,
	565,
	103,
	1207,
	1209,
	1207,
	1209,
	1209,
	1209,
	103,
	103,
	103,
	103,
	103,
	103,
	103,
	103,
	103,
	103,
	103,
	1209,
	26,
	26,
	23,
	3,
	95,
	31,
	95,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	31,
	23,
	23,
	783,
	1856,
	10,
	670,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	1047,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	28,
	23,
	23,
	23,
	23,
	23,
	34,
	31,
	23,
	23,
	23,
	23,
	95,
	23,
	23,
	23,
	23,
	23,
	23,
	292,
	292,
	23,
	1526,
	23,
	31,
	32,
	1857,
	112,
	113,
	112,
	1409,
	1409,
	1858,
	23,
	23,
	23,
	26,
	23,
	23,
	4,
	23,
	3,
	23,
	23,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	26,
	26,
	23,
	32,
	32,
	23,
	23,
	31,
	23,
	292,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	292,
	292,
	23,
	23,
	23,
	23,
	23,
	23,
	95,
	26,
	26,
	868,
	26,
	9,
	201,
	23,
	3,
	23,
	23,
	23,
	23,
	1859,
	26,
	23,
	23,
	23,
	32,
	32,
	23,
	23,
	23,
	23,
	1860,
	32,
	1091,
	23,
	32,
	23,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	1861,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	180,
	4,
	139,
	4,
	139,
	23,
	1184,
	23,
	670,
	292,
	292,
	23,
	95,
	31,
	23,
	23,
	23,
	23,
	1047,
	1037,
	1526,
	1526,
	23,
	23,
	1862,
	95,
	31,
	23,
	95,
	31,
	23,
	23,
	95,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1047,
	23,
	23,
	95,
	95,
	95,
	95,
	1047,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	95,
	31,
	23,
	23,
	23,
	23,
	1047,
	1047,
	23,
	1047,
	14,
	23,
	26,
	26,
	26,
	23,
	23,
	1047,
	26,
	26,
	26,
	1851,
	26,
	23,
	4,
	49,
	4,
	4,
	4,
	3,
	23,
	23,
	23,
	3,
	3,
	-1,
	790,
	103,
	103,
	565,
	103,
	565,
	103,
	565,
	1863,
	1233,
	572,
	1477,
	572,
	1477,
	1209,
	1207,
	790,
	572,
	103,
	103,
	103,
	103,
	102,
	325,
	102,
	23,
	23,
	23,
	23,
	23,
	1037,
	1037,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	32,
	425,
	9,
	62,
	30,
	28,
	23,
	23,
	23,
	23,
	23,
	1065,
	292,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	1053,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	26,
	9,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	9,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	9,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	9,
	26,
	14,
	23,
	94,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	139,
	4,
	4,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	761,
	23,
	32,
	34,
	1864,
	23,
	292,
	1865,
	1866,
	14,
	1867,
	37,
	95,
	95,
	62,
	425,
	1868,
	1869,
	9,
	1870,
	34,
	1074,
	14,
	1851,
	46,
	740,
	740,
	1871,
	532,
	149,
	790,
	49,
	790,
	49,
	790,
	49,
	790,
	49,
	790,
	49,
	790,
	49,
	790,
	49,
	1872,
	1873,
	565,
	139,
	43,
	790,
	49,
	1872,
	1873,
	149,
	532,
	1344,
	1084,
	1344,
	1084,
	1344,
	1084,
	149,
	532,
	1344,
	1084,
	790,
	49,
	149,
	532,
	790,
	49,
	1344,
	1084,
	790,
	49,
	1344,
	1084,
	49,
	790,
	10,
	197,
	119,
	1046,
	1101,
	1798,
	1046,
	23,
	23,
	14,
	1070,
	1053,
	670,
	1046,
	95,
	127,
	106,
	106,
	23,
	23,
	23,
	23,
	4,
	89,
	0,
	0,
	0,
	0,
	1,
	1,
	0,
	0,
	23,
	3,
	390,
	1596,
	23,
	-1,
	-1,
	2,
	23,
	-1,
	23,
	-1,
	-1,
	-1,
	1081,
	1184,
	1185,
	1874,
	1160,
	1875,
	1160,
	1875,
	1160,
	1875,
	23,
	43,
	139,
	23,
	1605,
	23,
	23,
	23,
	1605,
	23,
	23,
	23,
	1605,
	23,
	23,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	3,
	23,
	26,
	-1,
	-1,
	-1,
	23,
	26,
	23,
	23,
	9,
	23,
	9,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	107,
	23,
	120,
	26,
	107,
	26,
	188,
	26,
	107,
	26,
	188,
	26,
	3,
	23,
	9,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	23,
	9,
	23,
	9,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x02000051, { 0, 22 } },
	{ 0x0200007C, { 34, 1 } },
	{ 0x020000D0, { 26, 4 } },
	{ 0x06000320, { 22, 4 } },
	{ 0x0600048E, { 30, 2 } },
	{ 0x0600048F, { 32, 1 } },
	{ 0x06000492, { 33, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[35] = 
{
	{ (Il2CppRGCTXDataType)3, 17466 },
	{ (Il2CppRGCTXDataType)3, 17467 },
	{ (Il2CppRGCTXDataType)3, 17468 },
	{ (Il2CppRGCTXDataType)3, 17469 },
	{ (Il2CppRGCTXDataType)3, 17470 },
	{ (Il2CppRGCTXDataType)3, 17471 },
	{ (Il2CppRGCTXDataType)3, 17472 },
	{ (Il2CppRGCTXDataType)3, 17473 },
	{ (Il2CppRGCTXDataType)3, 17474 },
	{ (Il2CppRGCTXDataType)2, 19421 },
	{ (Il2CppRGCTXDataType)3, 16675 },
	{ (Il2CppRGCTXDataType)3, 17475 },
	{ (Il2CppRGCTXDataType)3, 17476 },
	{ (Il2CppRGCTXDataType)3, 17477 },
	{ (Il2CppRGCTXDataType)3, 17478 },
	{ (Il2CppRGCTXDataType)3, 16671 },
	{ (Il2CppRGCTXDataType)2, 19422 },
	{ (Il2CppRGCTXDataType)3, 17479 },
	{ (Il2CppRGCTXDataType)2, 19423 },
	{ (Il2CppRGCTXDataType)3, 17480 },
	{ (Il2CppRGCTXDataType)3, 17481 },
	{ (Il2CppRGCTXDataType)2, 18645 },
	{ (Il2CppRGCTXDataType)2, 19424 },
	{ (Il2CppRGCTXDataType)3, 17482 },
	{ (Il2CppRGCTXDataType)3, 17483 },
	{ (Il2CppRGCTXDataType)3, 17484 },
	{ (Il2CppRGCTXDataType)2, 19425 },
	{ (Il2CppRGCTXDataType)3, 17485 },
	{ (Il2CppRGCTXDataType)2, 19425 },
	{ (Il2CppRGCTXDataType)2, 18723 },
	{ (Il2CppRGCTXDataType)3, 17486 },
	{ (Il2CppRGCTXDataType)2, 19063 },
	{ (Il2CppRGCTXDataType)3, 17487 },
	{ (Il2CppRGCTXDataType)2, 19068 },
	{ (Il2CppRGCTXDataType)2, 19072 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	1593,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	35,
	s_rgctxValues,
	NULL,
};
