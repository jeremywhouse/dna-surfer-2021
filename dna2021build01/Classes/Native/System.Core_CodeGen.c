﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 ();
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E ();
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 ();
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 ();
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC ();
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 ();
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 ();
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA ();
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 ();
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 ();
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 ();
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 ();
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 ();
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 ();
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 ();
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F ();
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C ();
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A ();
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 ();
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA ();
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B ();
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F ();
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA ();
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 ();
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 ();
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA ();
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F ();
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD ();
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA ();
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA ();
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 ();
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x0000002F System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000030 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000031 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000033 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
// 0x00000037 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000038 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000039 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003A System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000003D TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003E TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000003F System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000041 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000042 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000043 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000044 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000045 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000046 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000047 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000048 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000049 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000004A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000004B System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000004C System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004D System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000004E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000050 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000051 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000052 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000053 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000054 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000055 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000056 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000057 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000058 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000059 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000005B System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000005C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005E System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000005F System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000060 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000061 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000062 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000063 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000064 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000065 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000066 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000067 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000068 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000069 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006A System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000006B System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000006C System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000006E System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000006F System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000070 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000071 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000072 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::.ctor(System.Int32)
// 0x00000073 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.IDisposable.Dispose()
// 0x00000074 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__23`3::MoveNext()
// 0x00000075 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::<>m__Finally1()
// 0x00000076 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::<>m__Finally2()
// 0x00000077 TResult System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000078 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerator.Reset()
// 0x00000079 System.Object System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerator.get_Current()
// 0x0000007A System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000007B System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007C System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x0000007D System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x0000007E System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x0000007F System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000080 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000081 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000082 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000083 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000084 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000085 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000086 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000087 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000088 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000089 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000008A System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000008B System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000008C System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000008D System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x0000008E TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000008F System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000090 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000091 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000092 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000093 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000094 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000095 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000096 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000097 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000098 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000099 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000009A System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000009B System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000009C TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000009D System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000009E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000009F System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000A0 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000A1 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000A2 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000A3 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000A4 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000A5 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000A6 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000A7 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000A8 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000A9 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000AB System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000AC System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000AD System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000AE System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000AF System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000B0 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000B1 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000B2 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000B3 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000B4 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000B5 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000B6 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000B7 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000B8 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000B9 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000BA System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[186] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[186] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	120,
	14,
	120,
	31,
	23,
	23,
	23,
	23,
	23,
	120,
	120,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	851,
	27,
	37,
	180,
	180,
	3,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[44] = 
{
	{ 0x02000008, { 63, 4 } },
	{ 0x02000009, { 67, 9 } },
	{ 0x0200000A, { 78, 7 } },
	{ 0x0200000B, { 87, 10 } },
	{ 0x0200000C, { 99, 11 } },
	{ 0x0200000D, { 113, 9 } },
	{ 0x0200000E, { 125, 12 } },
	{ 0x0200000F, { 140, 1 } },
	{ 0x02000010, { 141, 2 } },
	{ 0x02000011, { 143, 13 } },
	{ 0x02000012, { 156, 6 } },
	{ 0x02000014, { 162, 3 } },
	{ 0x02000015, { 167, 5 } },
	{ 0x02000016, { 172, 7 } },
	{ 0x02000017, { 179, 3 } },
	{ 0x02000018, { 182, 7 } },
	{ 0x02000019, { 189, 4 } },
	{ 0x0200001A, { 193, 23 } },
	{ 0x0200001C, { 216, 2 } },
	{ 0x06000031, { 0, 10 } },
	{ 0x06000032, { 10, 10 } },
	{ 0x06000033, { 20, 5 } },
	{ 0x06000034, { 25, 5 } },
	{ 0x06000035, { 30, 1 } },
	{ 0x06000036, { 31, 2 } },
	{ 0x06000037, { 33, 2 } },
	{ 0x06000038, { 35, 1 } },
	{ 0x06000039, { 36, 3 } },
	{ 0x0600003A, { 39, 2 } },
	{ 0x0600003B, { 41, 2 } },
	{ 0x0600003C, { 43, 2 } },
	{ 0x0600003D, { 45, 4 } },
	{ 0x0600003E, { 49, 3 } },
	{ 0x0600003F, { 52, 1 } },
	{ 0x06000040, { 53, 3 } },
	{ 0x06000041, { 56, 2 } },
	{ 0x06000042, { 58, 5 } },
	{ 0x06000052, { 76, 2 } },
	{ 0x06000057, { 85, 2 } },
	{ 0x0600005C, { 97, 2 } },
	{ 0x06000062, { 110, 3 } },
	{ 0x06000067, { 122, 3 } },
	{ 0x0600006C, { 137, 3 } },
	{ 0x06000089, { 165, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[218] = 
{
	{ (Il2CppRGCTXDataType)2, 19228 },
	{ (Il2CppRGCTXDataType)3, 17027 },
	{ (Il2CppRGCTXDataType)2, 19229 },
	{ (Il2CppRGCTXDataType)2, 19230 },
	{ (Il2CppRGCTXDataType)3, 17028 },
	{ (Il2CppRGCTXDataType)2, 19231 },
	{ (Il2CppRGCTXDataType)2, 19232 },
	{ (Il2CppRGCTXDataType)3, 17029 },
	{ (Il2CppRGCTXDataType)2, 19233 },
	{ (Il2CppRGCTXDataType)3, 17030 },
	{ (Il2CppRGCTXDataType)2, 19234 },
	{ (Il2CppRGCTXDataType)3, 17031 },
	{ (Il2CppRGCTXDataType)2, 19235 },
	{ (Il2CppRGCTXDataType)2, 19236 },
	{ (Il2CppRGCTXDataType)3, 17032 },
	{ (Il2CppRGCTXDataType)2, 19237 },
	{ (Il2CppRGCTXDataType)2, 19238 },
	{ (Il2CppRGCTXDataType)3, 17033 },
	{ (Il2CppRGCTXDataType)2, 19239 },
	{ (Il2CppRGCTXDataType)3, 17034 },
	{ (Il2CppRGCTXDataType)2, 19240 },
	{ (Il2CppRGCTXDataType)3, 17035 },
	{ (Il2CppRGCTXDataType)3, 17036 },
	{ (Il2CppRGCTXDataType)2, 13683 },
	{ (Il2CppRGCTXDataType)3, 17037 },
	{ (Il2CppRGCTXDataType)2, 19241 },
	{ (Il2CppRGCTXDataType)3, 17038 },
	{ (Il2CppRGCTXDataType)3, 17039 },
	{ (Il2CppRGCTXDataType)2, 13690 },
	{ (Il2CppRGCTXDataType)3, 17040 },
	{ (Il2CppRGCTXDataType)3, 17041 },
	{ (Il2CppRGCTXDataType)2, 19242 },
	{ (Il2CppRGCTXDataType)3, 17042 },
	{ (Il2CppRGCTXDataType)2, 19243 },
	{ (Il2CppRGCTXDataType)3, 17043 },
	{ (Il2CppRGCTXDataType)3, 17044 },
	{ (Il2CppRGCTXDataType)2, 19244 },
	{ (Il2CppRGCTXDataType)3, 17045 },
	{ (Il2CppRGCTXDataType)3, 17046 },
	{ (Il2CppRGCTXDataType)2, 13721 },
	{ (Il2CppRGCTXDataType)3, 17047 },
	{ (Il2CppRGCTXDataType)2, 13722 },
	{ (Il2CppRGCTXDataType)3, 17048 },
	{ (Il2CppRGCTXDataType)2, 19245 },
	{ (Il2CppRGCTXDataType)3, 17049 },
	{ (Il2CppRGCTXDataType)2, 19246 },
	{ (Il2CppRGCTXDataType)2, 19247 },
	{ (Il2CppRGCTXDataType)2, 13726 },
	{ (Il2CppRGCTXDataType)2, 19248 },
	{ (Il2CppRGCTXDataType)2, 13728 },
	{ (Il2CppRGCTXDataType)2, 19249 },
	{ (Il2CppRGCTXDataType)3, 17050 },
	{ (Il2CppRGCTXDataType)2, 13731 },
	{ (Il2CppRGCTXDataType)2, 13733 },
	{ (Il2CppRGCTXDataType)2, 19250 },
	{ (Il2CppRGCTXDataType)3, 17051 },
	{ (Il2CppRGCTXDataType)2, 19251 },
	{ (Il2CppRGCTXDataType)3, 17052 },
	{ (Il2CppRGCTXDataType)3, 17053 },
	{ (Il2CppRGCTXDataType)2, 19252 },
	{ (Il2CppRGCTXDataType)2, 13738 },
	{ (Il2CppRGCTXDataType)2, 19253 },
	{ (Il2CppRGCTXDataType)2, 13740 },
	{ (Il2CppRGCTXDataType)3, 17054 },
	{ (Il2CppRGCTXDataType)3, 17055 },
	{ (Il2CppRGCTXDataType)2, 13743 },
	{ (Il2CppRGCTXDataType)3, 17056 },
	{ (Il2CppRGCTXDataType)3, 17057 },
	{ (Il2CppRGCTXDataType)2, 13755 },
	{ (Il2CppRGCTXDataType)2, 19254 },
	{ (Il2CppRGCTXDataType)3, 17058 },
	{ (Il2CppRGCTXDataType)3, 17059 },
	{ (Il2CppRGCTXDataType)2, 13757 },
	{ (Il2CppRGCTXDataType)2, 19132 },
	{ (Il2CppRGCTXDataType)3, 17060 },
	{ (Il2CppRGCTXDataType)3, 17061 },
	{ (Il2CppRGCTXDataType)2, 19255 },
	{ (Il2CppRGCTXDataType)3, 17062 },
	{ (Il2CppRGCTXDataType)3, 17063 },
	{ (Il2CppRGCTXDataType)2, 13767 },
	{ (Il2CppRGCTXDataType)2, 19256 },
	{ (Il2CppRGCTXDataType)3, 17064 },
	{ (Il2CppRGCTXDataType)3, 17065 },
	{ (Il2CppRGCTXDataType)3, 16545 },
	{ (Il2CppRGCTXDataType)3, 17066 },
	{ (Il2CppRGCTXDataType)2, 19257 },
	{ (Il2CppRGCTXDataType)3, 17067 },
	{ (Il2CppRGCTXDataType)3, 17068 },
	{ (Il2CppRGCTXDataType)2, 13779 },
	{ (Il2CppRGCTXDataType)2, 19258 },
	{ (Il2CppRGCTXDataType)3, 17069 },
	{ (Il2CppRGCTXDataType)3, 17070 },
	{ (Il2CppRGCTXDataType)3, 17071 },
	{ (Il2CppRGCTXDataType)3, 17072 },
	{ (Il2CppRGCTXDataType)3, 17073 },
	{ (Il2CppRGCTXDataType)3, 16551 },
	{ (Il2CppRGCTXDataType)3, 17074 },
	{ (Il2CppRGCTXDataType)2, 19259 },
	{ (Il2CppRGCTXDataType)3, 17075 },
	{ (Il2CppRGCTXDataType)3, 17076 },
	{ (Il2CppRGCTXDataType)2, 13792 },
	{ (Il2CppRGCTXDataType)2, 19260 },
	{ (Il2CppRGCTXDataType)3, 17077 },
	{ (Il2CppRGCTXDataType)3, 17078 },
	{ (Il2CppRGCTXDataType)2, 13794 },
	{ (Il2CppRGCTXDataType)2, 19261 },
	{ (Il2CppRGCTXDataType)3, 17079 },
	{ (Il2CppRGCTXDataType)3, 17080 },
	{ (Il2CppRGCTXDataType)2, 19262 },
	{ (Il2CppRGCTXDataType)3, 17081 },
	{ (Il2CppRGCTXDataType)3, 17082 },
	{ (Il2CppRGCTXDataType)2, 19263 },
	{ (Il2CppRGCTXDataType)3, 17083 },
	{ (Il2CppRGCTXDataType)3, 17084 },
	{ (Il2CppRGCTXDataType)2, 13809 },
	{ (Il2CppRGCTXDataType)2, 19264 },
	{ (Il2CppRGCTXDataType)3, 17085 },
	{ (Il2CppRGCTXDataType)3, 17086 },
	{ (Il2CppRGCTXDataType)3, 17087 },
	{ (Il2CppRGCTXDataType)3, 16562 },
	{ (Il2CppRGCTXDataType)2, 19265 },
	{ (Il2CppRGCTXDataType)3, 17088 },
	{ (Il2CppRGCTXDataType)3, 17089 },
	{ (Il2CppRGCTXDataType)2, 19266 },
	{ (Il2CppRGCTXDataType)3, 17090 },
	{ (Il2CppRGCTXDataType)3, 17091 },
	{ (Il2CppRGCTXDataType)2, 13825 },
	{ (Il2CppRGCTXDataType)2, 19267 },
	{ (Il2CppRGCTXDataType)3, 17092 },
	{ (Il2CppRGCTXDataType)3, 17093 },
	{ (Il2CppRGCTXDataType)3, 17094 },
	{ (Il2CppRGCTXDataType)3, 17095 },
	{ (Il2CppRGCTXDataType)3, 17096 },
	{ (Il2CppRGCTXDataType)3, 17097 },
	{ (Il2CppRGCTXDataType)3, 16568 },
	{ (Il2CppRGCTXDataType)2, 19268 },
	{ (Il2CppRGCTXDataType)3, 17098 },
	{ (Il2CppRGCTXDataType)3, 17099 },
	{ (Il2CppRGCTXDataType)2, 19269 },
	{ (Il2CppRGCTXDataType)3, 17100 },
	{ (Il2CppRGCTXDataType)3, 17101 },
	{ (Il2CppRGCTXDataType)3, 17102 },
	{ (Il2CppRGCTXDataType)3, 17103 },
	{ (Il2CppRGCTXDataType)3, 17104 },
	{ (Il2CppRGCTXDataType)3, 17105 },
	{ (Il2CppRGCTXDataType)2, 19270 },
	{ (Il2CppRGCTXDataType)2, 19271 },
	{ (Il2CppRGCTXDataType)3, 17106 },
	{ (Il2CppRGCTXDataType)2, 13860 },
	{ (Il2CppRGCTXDataType)2, 19272 },
	{ (Il2CppRGCTXDataType)3, 17107 },
	{ (Il2CppRGCTXDataType)3, 17108 },
	{ (Il2CppRGCTXDataType)2, 13853 },
	{ (Il2CppRGCTXDataType)2, 19273 },
	{ (Il2CppRGCTXDataType)3, 17109 },
	{ (Il2CppRGCTXDataType)3, 17110 },
	{ (Il2CppRGCTXDataType)3, 17111 },
	{ (Il2CppRGCTXDataType)2, 13872 },
	{ (Il2CppRGCTXDataType)3, 17112 },
	{ (Il2CppRGCTXDataType)2, 19274 },
	{ (Il2CppRGCTXDataType)3, 17113 },
	{ (Il2CppRGCTXDataType)3, 17114 },
	{ (Il2CppRGCTXDataType)2, 19275 },
	{ (Il2CppRGCTXDataType)3, 17115 },
	{ (Il2CppRGCTXDataType)3, 17116 },
	{ (Il2CppRGCTXDataType)2, 19276 },
	{ (Il2CppRGCTXDataType)3, 17117 },
	{ (Il2CppRGCTXDataType)2, 19277 },
	{ (Il2CppRGCTXDataType)3, 17118 },
	{ (Il2CppRGCTXDataType)3, 17119 },
	{ (Il2CppRGCTXDataType)3, 17120 },
	{ (Il2CppRGCTXDataType)2, 13898 },
	{ (Il2CppRGCTXDataType)3, 17121 },
	{ (Il2CppRGCTXDataType)2, 13906 },
	{ (Il2CppRGCTXDataType)3, 17122 },
	{ (Il2CppRGCTXDataType)2, 19278 },
	{ (Il2CppRGCTXDataType)2, 19279 },
	{ (Il2CppRGCTXDataType)3, 17123 },
	{ (Il2CppRGCTXDataType)3, 17124 },
	{ (Il2CppRGCTXDataType)3, 17125 },
	{ (Il2CppRGCTXDataType)3, 17126 },
	{ (Il2CppRGCTXDataType)3, 17127 },
	{ (Il2CppRGCTXDataType)3, 17128 },
	{ (Il2CppRGCTXDataType)2, 13922 },
	{ (Il2CppRGCTXDataType)2, 19280 },
	{ (Il2CppRGCTXDataType)3, 17129 },
	{ (Il2CppRGCTXDataType)3, 17130 },
	{ (Il2CppRGCTXDataType)2, 13926 },
	{ (Il2CppRGCTXDataType)3, 17131 },
	{ (Il2CppRGCTXDataType)2, 19281 },
	{ (Il2CppRGCTXDataType)2, 13936 },
	{ (Il2CppRGCTXDataType)2, 13934 },
	{ (Il2CppRGCTXDataType)2, 19282 },
	{ (Il2CppRGCTXDataType)3, 17132 },
	{ (Il2CppRGCTXDataType)2, 19283 },
	{ (Il2CppRGCTXDataType)3, 17133 },
	{ (Il2CppRGCTXDataType)3, 17134 },
	{ (Il2CppRGCTXDataType)3, 17135 },
	{ (Il2CppRGCTXDataType)2, 13939 },
	{ (Il2CppRGCTXDataType)3, 17136 },
	{ (Il2CppRGCTXDataType)3, 17137 },
	{ (Il2CppRGCTXDataType)2, 13942 },
	{ (Il2CppRGCTXDataType)3, 17138 },
	{ (Il2CppRGCTXDataType)1, 19284 },
	{ (Il2CppRGCTXDataType)2, 13941 },
	{ (Il2CppRGCTXDataType)3, 17139 },
	{ (Il2CppRGCTXDataType)1, 13941 },
	{ (Il2CppRGCTXDataType)1, 13939 },
	{ (Il2CppRGCTXDataType)2, 19285 },
	{ (Il2CppRGCTXDataType)2, 13941 },
	{ (Il2CppRGCTXDataType)2, 13944 },
	{ (Il2CppRGCTXDataType)2, 13943 },
	{ (Il2CppRGCTXDataType)3, 17140 },
	{ (Il2CppRGCTXDataType)3, 17141 },
	{ (Il2CppRGCTXDataType)3, 17142 },
	{ (Il2CppRGCTXDataType)2, 13940 },
	{ (Il2CppRGCTXDataType)3, 17143 },
	{ (Il2CppRGCTXDataType)2, 13954 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	186,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	44,
	s_rgctxIndices,
	218,
	s_rgctxValues,
	NULL,
};
