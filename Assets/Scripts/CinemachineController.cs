﻿using Btkalman.Unity.Util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemachineController : MonoBehaviour {
    public static CinemachineController i;

    [SerializeField] private float m_minCameraShake = 0.05f;
    [SerializeField] private float m_maxCameraShake = 0.1f;
    [SerializeField] private float m_cameraShakeDelay = 0.05f;

    private CinemachineCameraOffset m_cameraOffset;

    [HideInInspector] public float cameraShake;

    private float m_cameraShakeTimer = 0f;
    private float m_frameTimer = 0;
    private int m_frames = 0;
    private int m_physicsFrames = 0;

    private void Awake() {
        Singleton.Awake(this, ref i);
        m_cameraOffset = GetComponentInChildren<CinemachineCameraOffset>();
    }

    private void Start() {
        cameraShake = 0;
    }

    private void Update() {
        UpdateCameraShake();
        UpdateFrameTimer();
    }

    private void UpdateFrameTimer() {
        m_frames++;
        m_frameTimer += Time.deltaTime;
        while (m_frameTimer >= 1) {
            // Debug.LogFormat("{0} frames {1} physics frames", m_frames, m_physicsFrames);
            m_frames = 0;
            m_physicsFrames = 0;
            m_frameTimer -= 1;
        }
    }

    private void FixedUpdate() {
        m_physicsFrames++;
    }

    private void UpdateCameraShake() {
        if (cameraShake == 0) {
          //  m_cameraOffset.m_Offset = Vector3.zero;
            return;
        }

        m_cameraShakeTimer += Time.deltaTime;
        if (m_cameraShakeTimer < m_cameraShakeDelay) {
            return;
        }

        m_cameraShakeTimer = 0f;
        float cameraShakeRange = Mathf.Abs(m_maxCameraShake - m_minCameraShake);
        float cameraShakeAmount = m_minCameraShake + cameraShake * cameraShakeRange;
        m_cameraOffset.m_Offset = new Vector3(
            Random.Range(0f, cameraShakeAmount),
            Random.Range(0f, cameraShakeAmount),
            0f);

    }
}