﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <assembly>j__TPar <>f__AnonymousType0`2::get_assembly()
// 0x00000002 <type>j__TPar <>f__AnonymousType0`2::get_type()
// 0x00000003 System.Void <>f__AnonymousType0`2::.ctor(<assembly>j__TPar,<type>j__TPar)
// 0x00000004 System.Boolean <>f__AnonymousType0`2::Equals(System.Object)
// 0x00000005 System.Int32 <>f__AnonymousType0`2::GetHashCode()
// 0x00000006 System.String <>f__AnonymousType0`2::ToString()
// 0x00000007 System.Void ES3Serializable::.ctor()
extern void ES3Serializable__ctor_m5D9C652EB87EA77CA6C78136499DBBA3C480034B ();
// 0x00000008 System.Void ES3NonSerializable::.ctor()
extern void ES3NonSerializable__ctor_m770148BE2F0F069E29AEEA8BA01101BF1E2A5D8B ();
// 0x00000009 System.Void ES3AutoSave::Awake()
extern void ES3AutoSave_Awake_mB13DDD8E29A9C92CE52401792BCBCC733D817A06 ();
// 0x0000000A System.Void ES3AutoSave::OnApplicationQuit()
extern void ES3AutoSave_OnApplicationQuit_mE06C46B910E05DFB4420AA0625A2ABA07A238450 ();
// 0x0000000B System.Void ES3AutoSave::OnDestroy()
extern void ES3AutoSave_OnDestroy_mCF46A32C5EDC15E09B25B2D2FFBE7699445EF9C4 ();
// 0x0000000C System.Void ES3AutoSave::.ctor()
extern void ES3AutoSave__ctor_mC10C53CF07F0CAC7FDA095E4D07EB09E2056307E ();
// 0x0000000D ES3AutoSaveMgr ES3AutoSaveMgr::get_Current()
extern void ES3AutoSaveMgr_get_Current_mD91B835E85E7B47D6B24B212775038FCD2F40410 ();
// 0x0000000E ES3AutoSaveMgr ES3AutoSaveMgr::get_Instance()
extern void ES3AutoSaveMgr_get_Instance_m60E58681BBFDB0C3995900F4FFCA95C9F4B03D3A ();
// 0x0000000F System.Void ES3AutoSaveMgr::Save()
extern void ES3AutoSaveMgr_Save_mCBBC9405F826143E14D4C5A6AD2F77A329D978C4 ();
// 0x00000010 System.Void ES3AutoSaveMgr::Load()
extern void ES3AutoSaveMgr_Load_mC6EB59DCF8DD34E04581D68908CCB68ADAB83DE5 ();
// 0x00000011 System.Void ES3AutoSaveMgr::Start()
extern void ES3AutoSaveMgr_Start_mC41259CA90BE1FF92DD1015E92454B7113526222 ();
// 0x00000012 System.Void ES3AutoSaveMgr::Awake()
extern void ES3AutoSaveMgr_Awake_m28E5A2716D2EBBA4D82D715EA36FEF18EB438D37 ();
// 0x00000013 System.Void ES3AutoSaveMgr::OnApplicationQuit()
extern void ES3AutoSaveMgr_OnApplicationQuit_m54D6B426B7CA9A31A93F6F9AA23F47F83EF56D3D ();
// 0x00000014 System.Void ES3AutoSaveMgr::OnApplicationPause(System.Boolean)
extern void ES3AutoSaveMgr_OnApplicationPause_mC5B1023E70195C8CA595D113EF907FC0C747955A ();
// 0x00000015 System.Void ES3AutoSaveMgr::AddAutoSave(ES3AutoSave)
extern void ES3AutoSaveMgr_AddAutoSave_mAAA487209B0965D263555C2325EC36DADED766F9 ();
// 0x00000016 System.Void ES3AutoSaveMgr::RemoveAutoSave(ES3AutoSave)
extern void ES3AutoSaveMgr_RemoveAutoSave_m4D2A090CA6F4481054933A2BD6068FCF9D82647F ();
// 0x00000017 System.Void ES3AutoSaveMgr::.ctor()
extern void ES3AutoSaveMgr__ctor_m06FFA3D65BEFA50C35F1A531D18EE36AFCBEEF17 ();
// 0x00000018 System.Void ES3AutoSaveMgr::.cctor()
extern void ES3AutoSaveMgr__cctor_mC77A05DD53AA39A785CF2148AA5D5BE91756DFDD ();
// 0x00000019 System.Void ES3::Save(System.String,T)
// 0x0000001A System.Void ES3::Save(System.String,T,System.String)
// 0x0000001B System.Void ES3::Save(System.String,T,System.String,ES3Settings)
// 0x0000001C System.Void ES3::Save(System.String,T,ES3Settings)
// 0x0000001D System.Void ES3::SaveRaw(System.Byte[])
extern void ES3_SaveRaw_m185656881388B772C4BC0FBB0AEB098131A6A3CE ();
// 0x0000001E System.Void ES3::SaveRaw(System.Byte[],System.String)
extern void ES3_SaveRaw_m7A0C7CF36E49472EEF0F63DCCDDA381B39E49341 ();
// 0x0000001F System.Void ES3::SaveRaw(System.Byte[],System.String,ES3Settings)
extern void ES3_SaveRaw_m81B7550BFFE0E93277BA2C4375A8E02B699DEBE4 ();
// 0x00000020 System.Void ES3::SaveRaw(System.Byte[],ES3Settings)
extern void ES3_SaveRaw_m06CD032898697EC4E80D1C25617CBC6BC7260A03 ();
// 0x00000021 System.Void ES3::SaveRaw(System.String)
extern void ES3_SaveRaw_m63206DE41441B9FBF3337CB67DE55D4240DB2DF9 ();
// 0x00000022 System.Void ES3::SaveRaw(System.String,System.String)
extern void ES3_SaveRaw_m658F8E70EA66360A79F9FD2D47825B8B62970BF4 ();
// 0x00000023 System.Void ES3::SaveRaw(System.String,System.String,ES3Settings)
extern void ES3_SaveRaw_mFD121A037BF971843D26EE676C67E6DD8C7BC022 ();
// 0x00000024 System.Void ES3::SaveRaw(System.String,ES3Settings)
extern void ES3_SaveRaw_mA9AFA33465D87E3357E7AE1D5DECB5AAC3F3821C ();
// 0x00000025 System.Void ES3::AppendRaw(System.Byte[])
extern void ES3_AppendRaw_mAD54E86053DE6A6C7EEAC5918D90D5CBB7C5B96D ();
// 0x00000026 System.Void ES3::AppendRaw(System.Byte[],System.String,ES3Settings)
extern void ES3_AppendRaw_m59372737EB22B9D1776F508E42EBF7C2167F587D ();
// 0x00000027 System.Void ES3::AppendRaw(System.Byte[],ES3Settings)
extern void ES3_AppendRaw_mA5AE5120CBBCAD7188D170A46E1A01001C37F6B5 ();
// 0x00000028 System.Void ES3::AppendRaw(System.String)
extern void ES3_AppendRaw_mD092521126032026487990C028912D29D2FA184D ();
// 0x00000029 System.Void ES3::AppendRaw(System.String,System.String,ES3Settings)
extern void ES3_AppendRaw_m36915292D5E44A4D1A965A46250A81292FCA5002 ();
// 0x0000002A System.Void ES3::AppendRaw(System.String,ES3Settings)
extern void ES3_AppendRaw_mEC32507B7D5E2F84054329957F335890A0399ED6 ();
// 0x0000002B System.Void ES3::SaveImage(UnityEngine.Texture2D,System.String)
extern void ES3_SaveImage_m13730AC98D9B1FA084B18F0B9109A36802117D7F ();
// 0x0000002C System.Void ES3::SaveImage(UnityEngine.Texture2D,System.String,ES3Settings)
extern void ES3_SaveImage_mB688CC1BCAE83203E1344C7D9C729B60169A1D16 ();
// 0x0000002D System.Void ES3::SaveImage(UnityEngine.Texture2D,ES3Settings)
extern void ES3_SaveImage_m9BF2559B6AC239478719FF80F406E32D382D98F7 ();
// 0x0000002E System.Object ES3::Load(System.String)
extern void ES3_Load_m339B81BF72591C7323B1C7DD2E60DF9D4623BF1A ();
// 0x0000002F System.Object ES3::Load(System.String,System.String)
extern void ES3_Load_m67E1B05C0431EA59FD96CA89C48DE9F3CB14D15C ();
// 0x00000030 System.Object ES3::Load(System.String,System.String,ES3Settings)
extern void ES3_Load_m38521C42B16BA6502931A4637D98ADE12771CB9E ();
// 0x00000031 System.Object ES3::Load(System.String,ES3Settings)
extern void ES3_Load_m1ADFC6B3F68E1B711FDB8C77B83A34F132C391A5 ();
// 0x00000032 T ES3::Load(System.String)
// 0x00000033 T ES3::Load(System.String,System.String)
// 0x00000034 T ES3::Load(System.String,System.String,ES3Settings)
// 0x00000035 T ES3::Load(System.String,ES3Settings)
// 0x00000036 T ES3::Load(System.String,T)
// 0x00000037 T ES3::Load(System.String,System.String,T)
// 0x00000038 T ES3::Load(System.String,System.String,T,ES3Settings)
// 0x00000039 T ES3::Load(System.String,T,ES3Settings)
// 0x0000003A System.Void ES3::LoadInto(System.String,System.Object)
// 0x0000003B System.Void ES3::LoadInto(System.String,System.String,System.Object)
extern void ES3_LoadInto_m2DABB994D56C3C974C44B4C6D40A98A31828473E ();
// 0x0000003C System.Void ES3::LoadInto(System.String,System.String,System.Object,ES3Settings)
extern void ES3_LoadInto_m295A78B26C4BA83FDAF32630767BFD216CBF508C ();
// 0x0000003D System.Void ES3::LoadInto(System.String,System.Object,ES3Settings)
extern void ES3_LoadInto_m42501F26583EC1DC5B30C95F780D004BE31C48FF ();
// 0x0000003E System.Void ES3::LoadInto(System.String,T)
// 0x0000003F System.Void ES3::LoadInto(System.String,System.String,T)
// 0x00000040 System.Void ES3::LoadInto(System.String,System.String,T,ES3Settings)
// 0x00000041 System.Void ES3::LoadInto(System.String,T,ES3Settings)
// 0x00000042 System.String ES3::LoadString(System.String,System.String,System.String)
extern void ES3_LoadString_m03F2D57441A12134504E16444CD6A76909D0E3C9 ();
// 0x00000043 System.Byte[] ES3::LoadRawBytes()
extern void ES3_LoadRawBytes_m70C6CD8E4D303A66F5904584CB2834822E3D0676 ();
// 0x00000044 System.Byte[] ES3::LoadRawBytes(System.String)
extern void ES3_LoadRawBytes_mED1A50A53EA95B50FD0DAB28A10340AB0BF91565 ();
// 0x00000045 System.Byte[] ES3::LoadRawBytes(System.String,ES3Settings)
extern void ES3_LoadRawBytes_m397B818D37A9338B97D73A36C72ECA3C0FC6B255 ();
// 0x00000046 System.Byte[] ES3::LoadRawBytes(ES3Settings)
extern void ES3_LoadRawBytes_m74DA9B3D50DCC9F2193391350A54B5C41572321E ();
// 0x00000047 System.String ES3::LoadRawString()
extern void ES3_LoadRawString_mF4CC038ECD6DFF695175102BCA9B02F1F232DC80 ();
// 0x00000048 System.String ES3::LoadRawString(System.String)
extern void ES3_LoadRawString_m74B3A2630612B3AF21A7C44FCBAA08FE11E1B331 ();
// 0x00000049 System.String ES3::LoadRawString(System.String,ES3Settings)
extern void ES3_LoadRawString_m7DD8EE8CEF8D88D90A406F417BCF1FAA191ABFA9 ();
// 0x0000004A System.String ES3::LoadRawString(ES3Settings)
extern void ES3_LoadRawString_m5A4BDFBEF816DCE511668B68B11E6D8A6F76E2A1 ();
// 0x0000004B UnityEngine.Texture2D ES3::LoadImage(System.String)
extern void ES3_LoadImage_m075639268BFEC0D02B3E4E64F7AC00491ACA5997 ();
// 0x0000004C UnityEngine.Texture2D ES3::LoadImage(System.String,ES3Settings)
extern void ES3_LoadImage_m68648991A888381EECAAE213B36175ECCA62235D ();
// 0x0000004D UnityEngine.Texture2D ES3::LoadImage(ES3Settings)
extern void ES3_LoadImage_mA4B245149A427AE1A4312E45D379A81128929CF5 ();
// 0x0000004E UnityEngine.Texture2D ES3::LoadImage(System.Byte[])
extern void ES3_LoadImage_m0A9571ACB970E8D2680EA1173BD917337423016C ();
// 0x0000004F UnityEngine.AudioClip ES3::LoadAudio(System.String,UnityEngine.AudioType)
extern void ES3_LoadAudio_mCDD5DED765110C4BA0DB9C2AB47E0774FAE966F1 ();
// 0x00000050 UnityEngine.AudioClip ES3::LoadAudio(System.String,UnityEngine.AudioType,ES3Settings)
extern void ES3_LoadAudio_m2A0220549E9283A2552D27D7F84481A68E4F08A8 ();
// 0x00000051 System.Byte[] ES3::Serialize(T,ES3Settings)
// 0x00000052 T ES3::Deserialize(System.Byte[],ES3Settings)
// 0x00000053 System.Object ES3::Deserialize(ES3Types.ES3Type,System.Byte[],ES3Settings)
extern void ES3_Deserialize_m5C2795D200A761F69B2324DE85B4FFFE89E9D365 ();
// 0x00000054 System.Void ES3::DeserializeInto(System.Byte[],T,ES3Settings)
// 0x00000055 System.Void ES3::DeserializeInto(ES3Types.ES3Type,System.Byte[],T,ES3Settings)
// 0x00000056 System.Void ES3::DeleteFile()
extern void ES3_DeleteFile_mD0AA5F982AB14A6D6A4D709B7522DFF27C7AE126 ();
// 0x00000057 System.Void ES3::DeleteFile(System.String)
extern void ES3_DeleteFile_m05C7AF8C8CBEE98FFBBEF4F7779D8BB7FE052994 ();
// 0x00000058 System.Void ES3::DeleteFile(System.String,ES3Settings)
extern void ES3_DeleteFile_mE83B6DA8A3A1DBBF30C2A374C1A158F122DCDA0E ();
// 0x00000059 System.Void ES3::DeleteFile(ES3Settings)
extern void ES3_DeleteFile_mFDA84D97818D52B8C5C6740E59F74824231D8AF4 ();
// 0x0000005A System.Void ES3::CopyFile(System.String,System.String)
extern void ES3_CopyFile_m4ECD60DE8A21E19B6A7781725D245DDB688B5EAC ();
// 0x0000005B System.Void ES3::CopyFile(System.String,System.String,ES3Settings,ES3Settings)
extern void ES3_CopyFile_m14A5D59879FE5A73461B38D4B069AA781FB97C85 ();
// 0x0000005C System.Void ES3::CopyFile(ES3Settings,ES3Settings)
extern void ES3_CopyFile_m0A6F45C1AD9D8D948A42E5458E3CE9E9D2B6E7A0 ();
// 0x0000005D System.Void ES3::RenameFile(System.String,System.String)
extern void ES3_RenameFile_m03798E9AFA4F285D91FCE90E7AE43F8863C9515C ();
// 0x0000005E System.Void ES3::RenameFile(System.String,System.String,ES3Settings,ES3Settings)
extern void ES3_RenameFile_m92D79F6A616CB8E0EA30C113D350F6F612FCA2EF ();
// 0x0000005F System.Void ES3::RenameFile(ES3Settings,ES3Settings)
extern void ES3_RenameFile_m94BAFFCC6E8EB0748611476389A604EC3654B3AB ();
// 0x00000060 System.Void ES3::CopyDirectory(System.String,System.String)
extern void ES3_CopyDirectory_mF93A31FC462C1D3A592BD86D2338973F89D01C3D ();
// 0x00000061 System.Void ES3::CopyDirectory(System.String,System.String,ES3Settings,ES3Settings)
extern void ES3_CopyDirectory_m3C6A665350E03E7201D7E46A2E68602AE6826AEF ();
// 0x00000062 System.Void ES3::CopyDirectory(ES3Settings,ES3Settings)
extern void ES3_CopyDirectory_mA70B2CACC416B50F9017B6841FB9F39F49378415 ();
// 0x00000063 System.Void ES3::RenameDirectory(System.String,System.String)
extern void ES3_RenameDirectory_m74884C5021B47CD2F6157A2F4D85EAA73D7B4867 ();
// 0x00000064 System.Void ES3::RenameDirectory(System.String,System.String,ES3Settings,ES3Settings)
extern void ES3_RenameDirectory_mAF9A661E7AFBB16BBB8AD06527693AF1C5EB4F2C ();
// 0x00000065 System.Void ES3::RenameDirectory(ES3Settings,ES3Settings)
extern void ES3_RenameDirectory_m8D730B8743E23627632FA5431DA5E78AE8DFFAFE ();
// 0x00000066 System.Void ES3::DeleteDirectory(System.String)
extern void ES3_DeleteDirectory_m5BB80EA138F8EB18BAEBAA440A05C4600B07E08D ();
// 0x00000067 System.Void ES3::DeleteDirectory(System.String,ES3Settings)
extern void ES3_DeleteDirectory_mC524A5030C5F4D267D55A68886C03F4EB1E271F2 ();
// 0x00000068 System.Void ES3::DeleteDirectory(ES3Settings)
extern void ES3_DeleteDirectory_m1D42EF9D78F658D133148D8D193D59B2891D0B3C ();
// 0x00000069 System.Void ES3::DeleteKey(System.String)
extern void ES3_DeleteKey_m340CF506E4C74BAB82CA2214182F20AC4E16C3E3 ();
// 0x0000006A System.Void ES3::DeleteKey(System.String,System.String)
extern void ES3_DeleteKey_m5253D3018C8140B9F6714E156BBA73C7AB1FA2E3 ();
// 0x0000006B System.Void ES3::DeleteKey(System.String,System.String,ES3Settings)
extern void ES3_DeleteKey_mF86C97EEE580037681A721B0689E2EA84038E969 ();
// 0x0000006C System.Void ES3::DeleteKey(System.String,ES3Settings)
extern void ES3_DeleteKey_m1230F7DACFDEB6641DE2F0BC6A1B7B258C0EE798 ();
// 0x0000006D System.Boolean ES3::KeyExists(System.String)
extern void ES3_KeyExists_m9D8B8CA83BDF8F820D46D084647B432C8B0ED9F8 ();
// 0x0000006E System.Boolean ES3::KeyExists(System.String,System.String)
extern void ES3_KeyExists_m64329C74796BAAA23C6C75B1A56E7C9625C927F6 ();
// 0x0000006F System.Boolean ES3::KeyExists(System.String,System.String,ES3Settings)
extern void ES3_KeyExists_m9445454C892C5282AD69105DF0932D01D5FC6259 ();
// 0x00000070 System.Boolean ES3::KeyExists(System.String,ES3Settings)
extern void ES3_KeyExists_m5F8B2A90FDBF2447C4B1D736CAFA7C68F2683D71 ();
// 0x00000071 System.Boolean ES3::FileExists()
extern void ES3_FileExists_mDB951F12CBAF0341F430EAD71349B1EB94FB4F1D ();
// 0x00000072 System.Boolean ES3::FileExists(System.String)
extern void ES3_FileExists_mEE7CF88C89A73EEA89BBF882FB1E86BACD3D5403 ();
// 0x00000073 System.Boolean ES3::FileExists(System.String,ES3Settings)
extern void ES3_FileExists_m40193005B1DD5F178EDE24EC403A2D0B98B7831A ();
// 0x00000074 System.Boolean ES3::FileExists(ES3Settings)
extern void ES3_FileExists_mE2E9A0EC715448CD6FA0AC1588D51E8DB259D745 ();
// 0x00000075 System.Boolean ES3::DirectoryExists(System.String)
extern void ES3_DirectoryExists_mFDF2BE3DE73C19F304D5EFD5CD5045E15F2E6481 ();
// 0x00000076 System.Boolean ES3::DirectoryExists(System.String,ES3Settings)
extern void ES3_DirectoryExists_m6B8E797B63DC117F18387123021F15757F92955B ();
// 0x00000077 System.Boolean ES3::DirectoryExists(ES3Settings)
extern void ES3_DirectoryExists_m59913CE9E75DD0B0F3A7090630472CD225DE786E ();
// 0x00000078 System.String[] ES3::GetKeys()
extern void ES3_GetKeys_mC78A2482BAA8A941F0A4482575E6C02FF8E371FE ();
// 0x00000079 System.String[] ES3::GetKeys(System.String)
extern void ES3_GetKeys_m8EDA4C747C98BFFDDD45F95BB346CA07346712F1 ();
// 0x0000007A System.String[] ES3::GetKeys(System.String,ES3Settings)
extern void ES3_GetKeys_mAAE5F800EFC61EC4C238199A8E6235452ACEF981 ();
// 0x0000007B System.String[] ES3::GetKeys(ES3Settings)
extern void ES3_GetKeys_m4E4E7B1849CC0A9124F8298B21E5CD8E67BAF52B ();
// 0x0000007C System.String[] ES3::GetFiles()
extern void ES3_GetFiles_mD451CFE6D93B7A3A5DF72C25E0B25D2696C60A17 ();
// 0x0000007D System.String[] ES3::GetFiles(System.String)
extern void ES3_GetFiles_m7BCA658BE84BE317B2149176FD10957BDBE6F7C0 ();
// 0x0000007E System.String[] ES3::GetFiles(System.String,ES3Settings)
extern void ES3_GetFiles_mD92FFCDC3CC71549115D69FC35EBC4C6EEB4763B ();
// 0x0000007F System.String[] ES3::GetFiles(ES3Settings)
extern void ES3_GetFiles_m82DC58442713CE7EDE532BD85EDD35A17225FA57 ();
// 0x00000080 System.String[] ES3::GetDirectories()
extern void ES3_GetDirectories_m78437F8BD33070DACB9D2578ECA697364C8A1E7E ();
// 0x00000081 System.String[] ES3::GetDirectories(System.String)
extern void ES3_GetDirectories_m14428CF7C029AFE293C3FCCE36FA48F990108ABA ();
// 0x00000082 System.String[] ES3::GetDirectories(System.String,ES3Settings)
extern void ES3_GetDirectories_m8BC5EC01FEC3568DDAA9C570CE72821DFEB38E5B ();
// 0x00000083 System.String[] ES3::GetDirectories(ES3Settings)
extern void ES3_GetDirectories_mB0BE5EEC616E5AEE82C6C8BDE3411956A9E6BACA ();
// 0x00000084 System.Void ES3::CreateBackup()
extern void ES3_CreateBackup_m027B93C4329139D3E3007E80B00BB6A7BA057C2E ();
// 0x00000085 System.Void ES3::CreateBackup(System.String)
extern void ES3_CreateBackup_m2BD7925DBE59380958D8E07DD5103455E3C4DC20 ();
// 0x00000086 System.Void ES3::CreateBackup(System.String,ES3Settings)
extern void ES3_CreateBackup_m9151AC964D50C58528116DF964D84C4CF801BA65 ();
// 0x00000087 System.Void ES3::CreateBackup(ES3Settings)
extern void ES3_CreateBackup_mF99F5572D20AD03898E80AE177E3E6534AD06A8E ();
// 0x00000088 System.Boolean ES3::RestoreBackup(System.String)
extern void ES3_RestoreBackup_mF65BB014895598BBA7BD8EED19FE7741B10BB029 ();
// 0x00000089 System.Boolean ES3::RestoreBackup(System.String,ES3Settings)
extern void ES3_RestoreBackup_m598CF154231B2EBD6CA81DD7CD2635EBE5EC770C ();
// 0x0000008A System.Boolean ES3::RestoreBackup(ES3Settings)
extern void ES3_RestoreBackup_m82E0B77BADBBB9F3367F00DB706AC0F3134F40A4 ();
// 0x0000008B System.DateTime ES3::GetTimestamp()
extern void ES3_GetTimestamp_m2DB86061FCAC875539F86E9C5D6A2E05928A62EB ();
// 0x0000008C System.DateTime ES3::GetTimestamp(System.String)
extern void ES3_GetTimestamp_m5BF46845E352319EC7B9475F80C93277CE199BC7 ();
// 0x0000008D System.DateTime ES3::GetTimestamp(System.String,ES3Settings)
extern void ES3_GetTimestamp_m206942082E746E95819ED64FD8827C1ECD9D8EAA ();
// 0x0000008E System.DateTime ES3::GetTimestamp(ES3Settings)
extern void ES3_GetTimestamp_mD37C8B6C766A0CA806060B776B8B4B71CADC5937 ();
// 0x0000008F System.Void ES3::StoreCachedFile()
extern void ES3_StoreCachedFile_m1B34E1EFA9B2C40FB3720FC3AA11912FC56313BE ();
// 0x00000090 System.Void ES3::StoreCachedFile(System.String)
extern void ES3_StoreCachedFile_m5E12741050A1AD49707B005E100DF49793F1F131 ();
// 0x00000091 System.Void ES3::StoreCachedFile(System.String,ES3Settings)
extern void ES3_StoreCachedFile_m3B805435D9F10B69946D59603602077FF7B547E4 ();
// 0x00000092 System.Void ES3::StoreCachedFile(ES3Settings)
extern void ES3_StoreCachedFile_m99D4848127E6D4BA8CAE19A4584E9284CA8398F3 ();
// 0x00000093 System.Void ES3::CacheFile()
extern void ES3_CacheFile_m1C9CE72EDD36EDB772A57CAD43666656157D7660 ();
// 0x00000094 System.Void ES3::CacheFile(System.String)
extern void ES3_CacheFile_mE802BD63D3EBC826E64FE0C0B2E3435D36A4B4CA ();
// 0x00000095 System.Void ES3::CacheFile(System.String,ES3Settings)
extern void ES3_CacheFile_m9311BF777DCEFAC3B26ECDE282DC628B0D1F2B69 ();
// 0x00000096 System.Void ES3::CacheFile(ES3Settings)
extern void ES3_CacheFile_m2BD1B65E0535997821A85EBFBE4B34B76F93C2B7 ();
// 0x00000097 System.Void ES3::Init()
extern void ES3_Init_mEE11D38D7E3E7D35A33625FE9E4E873D2D105CB3 ();
// 0x00000098 System.Void ES3File::.ctor()
extern void ES3File__ctor_m780F5D3BFEB251899CC4EFB9ABDBBAA0F7A8D5BB ();
// 0x00000099 System.Void ES3File::.ctor(System.String)
extern void ES3File__ctor_m253D89DF972FBDAE4C235D80E61FD2627786C7FC ();
// 0x0000009A System.Void ES3File::.ctor(System.String,ES3Settings)
extern void ES3File__ctor_m2AFFEF7E039832C28591AA3BA528E095DCA34AE8 ();
// 0x0000009B System.Void ES3File::.ctor(ES3Settings)
extern void ES3File__ctor_mD741E25E2E4DACFC2AB59D94AA331EE0EFD0C64C ();
// 0x0000009C System.Void ES3File::.ctor(System.Boolean)
extern void ES3File__ctor_m96839D7D90D03187F2B31D8771C9FD3E8DA9F0AA ();
// 0x0000009D System.Void ES3File::.ctor(System.String,System.Boolean)
extern void ES3File__ctor_mC161D7F766C39658688E1C745B1998F2D7FF301A ();
// 0x0000009E System.Void ES3File::.ctor(System.String,ES3Settings,System.Boolean)
extern void ES3File__ctor_mBEFCE7F38FB753A43DF1DA8D986A0C557D830BF4 ();
// 0x0000009F System.Void ES3File::.ctor(ES3Settings,System.Boolean)
extern void ES3File__ctor_m885630A6272B8FFAF77D22C427CEA5D48665B84E ();
// 0x000000A0 System.Void ES3File::.ctor(System.Byte[],ES3Settings)
extern void ES3File__ctor_mBA2B6B4B21DA81390EBF4A4603D13B54C9C86F87 ();
// 0x000000A1 System.Void ES3File::Sync()
extern void ES3File_Sync_mB1C971B11BA2F18AD2448BF412E15234812F1C60 ();
// 0x000000A2 System.Void ES3File::Sync(System.String,ES3Settings)
extern void ES3File_Sync_mEE74FF3C19D663EC993237E42069D92617C03F3A ();
// 0x000000A3 System.Void ES3File::Sync(ES3Settings)
extern void ES3File_Sync_m8449CB9A818C32C5EA3437227D541BADBDFDB90E ();
// 0x000000A4 System.Void ES3File::Clear()
extern void ES3File_Clear_mC386010AADFDA8682674622657EF8C89662B51F6 ();
// 0x000000A5 System.String[] ES3File::GetKeys()
extern void ES3File_GetKeys_mE1B613E4C6485C5E3FF1C11DD2F837EAE80B1CFA ();
// 0x000000A6 System.Void ES3File::Save(System.String,T)
// 0x000000A7 System.Void ES3File::SaveRaw(System.Byte[],ES3Settings)
extern void ES3File_SaveRaw_m82962A9BCD9F4591A9AC6BE56B0C3139485B8309 ();
// 0x000000A8 System.Void ES3File::AppendRaw(System.Byte[],ES3Settings)
extern void ES3File_AppendRaw_m5ED3178112BA636EBAC5E23FDF4490C56DD54040 ();
// 0x000000A9 System.Object ES3File::Load(System.String)
extern void ES3File_Load_m0B84781A868397E22DF30C8F8D6D62A2CFF632E5 ();
// 0x000000AA System.Object ES3File::Load(System.String,System.Object)
extern void ES3File_Load_m0A9174DDBF9953D85AE73014B73FE53BFC1AF06F ();
// 0x000000AB T ES3File::Load(System.String)
// 0x000000AC T ES3File::Load(System.String,T)
// 0x000000AD System.Void ES3File::LoadInto(System.String,T)
// 0x000000AE System.Byte[] ES3File::LoadRawBytes()
extern void ES3File_LoadRawBytes_mBFC6623E89998B08EDB5BCD93F954BB042B893E0 ();
// 0x000000AF System.String ES3File::LoadRawString()
extern void ES3File_LoadRawString_m3058300E89B671BBCE2A21EE114890D1130E508A ();
// 0x000000B0 System.Void ES3File::DeleteKey(System.String)
extern void ES3File_DeleteKey_m4D1937BACC2C8BFC363958275224C03829CF9F50 ();
// 0x000000B1 System.Boolean ES3File::KeyExists(System.String)
extern void ES3File_KeyExists_m167B533F40F7D9B90481D5F483C1C1765EDDF2B9 ();
// 0x000000B2 System.Int32 ES3File::Size()
extern void ES3File_Size_m326EB59AC4F792236C0F0544C05AA045E3BEB7C2 ();
// 0x000000B3 System.Type ES3File::GetKeyType(System.String)
extern void ES3File_GetKeyType_mD92BFDE795AE2B9405665287FE32295622A6718B ();
// 0x000000B4 ES3File ES3File::GetOrCreateCachedFile(ES3Settings)
extern void ES3File_GetOrCreateCachedFile_m3904AD47E2CB689A23D201AD938F886AADDEF607 ();
// 0x000000B5 System.Void ES3File::CacheFile(ES3Settings)
extern void ES3File_CacheFile_mB60D1A5BDBE0BB7467C34F8EDCD48DC520FA20DC ();
// 0x000000B6 System.Void ES3File::Store(ES3Settings)
extern void ES3File_Store_m12A194B73BD11146F5EB3742F50079D41AD82540 ();
// 0x000000B7 System.Void ES3File::RemoveCachedFile(ES3Settings)
extern void ES3File_RemoveCachedFile_m6D7B49CD13CE276F2CD05C1C3B2C15E428CCB64F ();
// 0x000000B8 System.Void ES3File::CopyCachedFile(ES3Settings,ES3Settings)
extern void ES3File_CopyCachedFile_m121ED179C64CED9B0B49CD0A690B782F1FB1331D ();
// 0x000000B9 System.Void ES3File::DeleteKey(System.String,ES3Settings)
extern void ES3File_DeleteKey_m9DF15826BB45C3F550DED2D9A8AE3E3CD4B0D87B ();
// 0x000000BA System.Boolean ES3File::KeyExists(System.String,ES3Settings)
extern void ES3File_KeyExists_m835C0467FA805F83D574184D6A66DCC4A612E387 ();
// 0x000000BB System.Boolean ES3File::FileExists(ES3Settings)
extern void ES3File_FileExists_m4B0B846E932FBF3BB9802CEC772D25090D64E563 ();
// 0x000000BC System.String[] ES3File::GetKeys(ES3Settings)
extern void ES3File_GetKeys_mEB1772F6A72A36CB8B410E7642B175723AB5A185 ();
// 0x000000BD System.String[] ES3File::GetFiles()
extern void ES3File_GetFiles_m43F67ED3B2EF2A2C7C5CE0FAC98A71FCD2479D94 ();
// 0x000000BE System.DateTime ES3File::GetTimestamp(ES3Settings)
extern void ES3File_GetTimestamp_mFE50B38C30EB57E8E690A5EF77EF172404B872F0 ();
// 0x000000BF System.Void ES3File::.cctor()
extern void ES3File__cctor_mBDFB0090DED9748CE5E0BF88BB80860443D5093E ();
// 0x000000C0 System.Void ES3InspectorInfo::.ctor()
extern void ES3InspectorInfo__ctor_m7966EBF566674FC349B1837A9428EC86AFF034D1 ();
// 0x000000C1 System.Void ES3ReferenceMgr::.ctor()
extern void ES3ReferenceMgr__ctor_m1F32371F83DF571BEDCF59E71A3B92BB0412CAF3 ();
// 0x000000C2 System.Void ES3Spreadsheet::.ctor()
extern void ES3Spreadsheet__ctor_m9EC1620F5F2B923C5B68EAA6EF4A2308F9C5A3DA ();
// 0x000000C3 System.Int32 ES3Spreadsheet::get_ColumnCount()
extern void ES3Spreadsheet_get_ColumnCount_m4EFA161C29FC7771BB7130623EAC686372E31951 ();
// 0x000000C4 System.Int32 ES3Spreadsheet::get_RowCount()
extern void ES3Spreadsheet_get_RowCount_m11F511DEB209BDB0DD2C5B0092775B536A5CEFCD ();
// 0x000000C5 System.Void ES3Spreadsheet::SetCell(System.Int32,System.Int32,T)
// 0x000000C6 System.Void ES3Spreadsheet::SetCellString(System.Int32,System.Int32,System.String)
extern void ES3Spreadsheet_SetCellString_mE772C1C051311ECB879B25A2BB8F479F84693DE4 ();
// 0x000000C7 T ES3Spreadsheet::GetCell(System.Int32,System.Int32)
// 0x000000C8 System.Object ES3Spreadsheet::GetCell(System.Type,System.Int32,System.Int32)
extern void ES3Spreadsheet_GetCell_mE0010FBCF6761C97F87B9E70100DA04E841EE3E2 ();
// 0x000000C9 System.Void ES3Spreadsheet::Load(System.String)
extern void ES3Spreadsheet_Load_mF6BFAA30F1031CE98806E75BFA49EB19C69FDE7B ();
// 0x000000CA System.Void ES3Spreadsheet::Load(System.String,ES3Settings)
extern void ES3Spreadsheet_Load_m64FA8703A38554E7A667EB4529FEA0F088C279AE ();
// 0x000000CB System.Void ES3Spreadsheet::Load(ES3Settings)
extern void ES3Spreadsheet_Load_mAA7A582BEDEB539E6C6E5F06074C397B941CD65A ();
// 0x000000CC System.Void ES3Spreadsheet::LoadRaw(System.String)
extern void ES3Spreadsheet_LoadRaw_m7E42B1EE92FAA4E7961ABD3E8EEAD2226AB5C5F0 ();
// 0x000000CD System.Void ES3Spreadsheet::LoadRaw(System.String,ES3Settings)
extern void ES3Spreadsheet_LoadRaw_m74B91B28998D933DE4017696777FFF3B2634E714 ();
// 0x000000CE System.Void ES3Spreadsheet::Load(System.IO.Stream,ES3Settings)
extern void ES3Spreadsheet_Load_mA39FE17BAC9795948102396F65DB5206C1D09131 ();
// 0x000000CF System.Void ES3Spreadsheet::Save(System.String)
extern void ES3Spreadsheet_Save_m25BDB030B50D9AEC883B6EC3B3E13081D640EAD2 ();
// 0x000000D0 System.Void ES3Spreadsheet::Save(System.String,ES3Settings)
extern void ES3Spreadsheet_Save_m21E0D1571E6BE34C833F1783225014B74733A338 ();
// 0x000000D1 System.Void ES3Spreadsheet::Save(ES3Settings)
extern void ES3Spreadsheet_Save_m26CE88E7CF93EBF23A1C27FC2C79810C6FF1B0FA ();
// 0x000000D2 System.Void ES3Spreadsheet::Save(System.String,System.Boolean)
extern void ES3Spreadsheet_Save_mBEA2F09E65738879E1213EB2D5AC104EFB4D8704 ();
// 0x000000D3 System.Void ES3Spreadsheet::Save(System.String,ES3Settings,System.Boolean)
extern void ES3Spreadsheet_Save_mA30AFE3E9B0DB59FCC6D16F247EEB44A1911C216 ();
// 0x000000D4 System.Void ES3Spreadsheet::Save(ES3Settings,System.Boolean)
extern void ES3Spreadsheet_Save_mDAEB0EC6F58974F5409A2619B9799C4F0B1BD849 ();
// 0x000000D5 System.String ES3Spreadsheet::Escape(System.String,System.Boolean)
extern void ES3Spreadsheet_Escape_mD454AE36F544B23ABB68463970FDEA713A93DC8B ();
// 0x000000D6 System.String ES3Spreadsheet::Unescape(System.String)
extern void ES3Spreadsheet_Unescape_m6A87213236F367600B72EBCD9821229A5B615ADD ();
// 0x000000D7 System.String[0...,0...] ES3Spreadsheet::ToArray()
extern void ES3Spreadsheet_ToArray_m238C6E3665120E350182844D7971B57A7E504BBC ();
// 0x000000D8 System.Void ES3Spreadsheet::.cctor()
extern void ES3Spreadsheet__cctor_mAA735D6DB08517584921A85EE38A0BEE30C667E0 ();
// 0x000000D9 System.Int32 ES3Reader::Read_int()
// 0x000000DA System.Single ES3Reader::Read_float()
// 0x000000DB System.Boolean ES3Reader::Read_bool()
// 0x000000DC System.Char ES3Reader::Read_char()
// 0x000000DD System.Decimal ES3Reader::Read_decimal()
// 0x000000DE System.Double ES3Reader::Read_double()
// 0x000000DF System.Int64 ES3Reader::Read_long()
// 0x000000E0 System.UInt64 ES3Reader::Read_ulong()
// 0x000000E1 System.Byte ES3Reader::Read_byte()
// 0x000000E2 System.SByte ES3Reader::Read_sbyte()
// 0x000000E3 System.Int16 ES3Reader::Read_short()
// 0x000000E4 System.UInt16 ES3Reader::Read_ushort()
// 0x000000E5 System.UInt32 ES3Reader::Read_uint()
// 0x000000E6 System.String ES3Reader::Read_string()
// 0x000000E7 System.Byte[] ES3Reader::Read_byteArray()
// 0x000000E8 System.Int64 ES3Reader::Read_ref()
// 0x000000E9 System.String ES3Reader::ReadPropertyName()
// 0x000000EA System.Type ES3Reader::ReadKeyPrefix(System.Boolean)
// 0x000000EB System.Void ES3Reader::ReadKeySuffix()
// 0x000000EC System.Byte[] ES3Reader::ReadElement(System.Boolean)
// 0x000000ED System.Void ES3Reader::Dispose()
// 0x000000EE System.Boolean ES3Reader::Goto(System.String)
extern void ES3Reader_Goto_mECEF750439752C93AA41BED4CA3E2DFF47016D67 ();
// 0x000000EF System.Boolean ES3Reader::StartReadObject()
extern void ES3Reader_StartReadObject_mC1E9750C54C281CF465317BC7ABEEF060A4996AD ();
// 0x000000F0 System.Void ES3Reader::EndReadObject()
extern void ES3Reader_EndReadObject_mC2FA016C0528AEDDA9115E04FC113DBE74830EB7 ();
// 0x000000F1 System.Boolean ES3Reader::StartReadDictionary()
// 0x000000F2 System.Void ES3Reader::EndReadDictionary()
// 0x000000F3 System.Boolean ES3Reader::StartReadDictionaryKey()
// 0x000000F4 System.Void ES3Reader::EndReadDictionaryKey()
// 0x000000F5 System.Void ES3Reader::StartReadDictionaryValue()
// 0x000000F6 System.Boolean ES3Reader::EndReadDictionaryValue()
// 0x000000F7 System.Boolean ES3Reader::StartReadCollection()
// 0x000000F8 System.Void ES3Reader::EndReadCollection()
// 0x000000F9 System.Boolean ES3Reader::StartReadCollectionItem()
// 0x000000FA System.Boolean ES3Reader::EndReadCollectionItem()
// 0x000000FB System.Void ES3Reader::.ctor(ES3Settings,System.Boolean)
extern void ES3Reader__ctor_mFA5D8C47D30FB52BE25C949DF191C5510596E2AD ();
// 0x000000FC ES3Reader_ES3ReaderPropertyEnumerator ES3Reader::get_Properties()
extern void ES3Reader_get_Properties_m8928B9AB6B83F6681F12AB929DB97C0EA902728C ();
// 0x000000FD ES3Reader_ES3ReaderRawEnumerator ES3Reader::get_RawEnumerator()
extern void ES3Reader_get_RawEnumerator_mB24EF7B64CA00350FC75E62F286CC73A71B3ACA8 ();
// 0x000000FE System.Void ES3Reader::Skip()
extern void ES3Reader_Skip_mAD263093FD43CCDDE25772BCF5A5D96F668C2DD0 ();
// 0x000000FF T ES3Reader::Read()
// 0x00000100 System.Void ES3Reader::ReadInto(System.Object)
// 0x00000101 T ES3Reader::ReadProperty()
// 0x00000102 T ES3Reader::ReadProperty(ES3Types.ES3Type)
// 0x00000103 System.Int64 ES3Reader::ReadRefProperty()
extern void ES3Reader_ReadRefProperty_mF8B64EBB09BB2399E192329EC5974709E603231B ();
// 0x00000104 System.Type ES3Reader::ReadType()
extern void ES3Reader_ReadType_m2F367E59B15774FB53E3C4ED6DF0AEF4FC3F4958 ();
// 0x00000105 System.Void ES3Reader::SetPrivateProperty(System.String,System.Object,System.Object)
extern void ES3Reader_SetPrivateProperty_m208B026129F31DD900FD0B3EC32EF95F61AFA290 ();
// 0x00000106 System.Void ES3Reader::SetPrivateField(System.String,System.Object,System.Object)
extern void ES3Reader_SetPrivateField_m02248A47636EBB0A1593F89B5F9FE35F7358D624 ();
// 0x00000107 T ES3Reader::Read(System.String)
// 0x00000108 T ES3Reader::Read(System.String,T)
// 0x00000109 System.Void ES3Reader::ReadInto(System.String,T)
// 0x0000010A System.Void ES3Reader::ReadObject(System.Object,ES3Types.ES3Type)
// 0x0000010B T ES3Reader::ReadObject(ES3Types.ES3Type)
// 0x0000010C T ES3Reader::Read(ES3Types.ES3Type)
// 0x0000010D System.Void ES3Reader::ReadInto(System.Object,ES3Types.ES3Type)
// 0x0000010E System.Type ES3Reader::ReadTypeFromHeader()
// 0x0000010F ES3Reader ES3Reader::Create()
extern void ES3Reader_Create_m4DE19555F5B4D1ED7A534EC857C0BD21C4FC27C2 ();
// 0x00000110 ES3Reader ES3Reader::Create(System.String)
extern void ES3Reader_Create_mD6B83820A40E958B88788C41A452C789CD92290F ();
// 0x00000111 ES3Reader ES3Reader::Create(System.String,ES3Settings)
extern void ES3Reader_Create_m2BF9327FD7607F859EAC2F72EA8CFB0E249AA45E ();
// 0x00000112 ES3Reader ES3Reader::Create(ES3Settings)
extern void ES3Reader_Create_mDD80D8F8A3EF79D67C3CE9D5519580A3C6DBF8DE ();
// 0x00000113 ES3Reader ES3Reader::Create(System.Byte[])
extern void ES3Reader_Create_m6FE81E95A0CE7BFEF0596B880199D3D793976613 ();
// 0x00000114 ES3Reader ES3Reader::Create(System.Byte[],ES3Settings)
extern void ES3Reader_Create_mAA7D637CA4A36D277F8828030BD8AAA0D3C918DB ();
// 0x00000115 ES3Reader ES3Reader::Create(System.IO.Stream,ES3Settings)
extern void ES3Reader_Create_mA5DAFEB96B70AFB15DF8FC9DCD2E25582B62026A ();
// 0x00000116 ES3Reader ES3Reader::Create(System.IO.Stream,ES3Settings,System.Boolean)
extern void ES3Reader_Create_mF5DF1FD6E4650685509F53B7745E44C64D119652 ();
// 0x00000117 System.Void ES3XMLReader::.ctor()
extern void ES3XMLReader__ctor_mAEB5BA392DD5E005A1A6B575A111DF8C0D2439A1 ();
// 0x00000118 System.Void ES3Defaults::.ctor()
extern void ES3Defaults__ctor_mC15C214EE3FD7C5BC27683E1F9EAFF9E86AACD6B ();
// 0x00000119 ES3Defaults ES3Settings::get_defaultSettingsScriptableObject()
extern void ES3Settings_get_defaultSettingsScriptableObject_mE3CC92D7B03E640EF54DE8FC975F14CD6D7F87C8 ();
// 0x0000011A ES3Settings ES3Settings::get_defaultSettings()
extern void ES3Settings_get_defaultSettings_m7935DE01EA5292B39393693DD18519D3713E66D5 ();
// 0x0000011B ES3Settings ES3Settings::get_unencryptedUncompressedSettings()
extern void ES3Settings_get_unencryptedUncompressedSettings_m6CB5E48587262D9260A3A4A077D9C8EA33182B13 ();
// 0x0000011C ES3_Location ES3Settings::get_location()
extern void ES3Settings_get_location_m8E35806FDFECEAD31797C81FA6AF50283E97FEA6 ();
// 0x0000011D System.Void ES3Settings::set_location(ES3_Location)
extern void ES3Settings_set_location_mA044138052D94897B7C7BBAF2894C821C6744748 ();
// 0x0000011E System.String ES3Settings::get_FullPath()
extern void ES3Settings_get_FullPath_m0F490EE68D40BD397227296257729D6A4CF244D1 ();
// 0x0000011F System.Void ES3Settings::.ctor(System.String,ES3Settings)
extern void ES3Settings__ctor_m1B2CE5BD8E7129A9AA19221716A203B222313BC0 ();
// 0x00000120 System.Void ES3Settings::.ctor(System.String,System.Enum[])
extern void ES3Settings__ctor_mD79A3966C2AB80CD6811B0F156BFB6CE49FC536F ();
// 0x00000121 System.Void ES3Settings::.ctor(System.Enum[])
extern void ES3Settings__ctor_m428E5A1813662EF4BA2D8E184FB1770F2A622A75 ();
// 0x00000122 System.Void ES3Settings::.ctor(ES3_EncryptionType,System.String)
extern void ES3Settings__ctor_mC3CB3E40C73F375310C204D9BE86565A9266B9DA ();
// 0x00000123 System.Void ES3Settings::.ctor(System.String,ES3_EncryptionType,System.String,ES3Settings)
extern void ES3Settings__ctor_mB6E85691377444C5EA9138475738060D621013F3 ();
// 0x00000124 System.Void ES3Settings::.ctor(System.Boolean)
extern void ES3Settings__ctor_m2EB9C617958F0C2554F76B7F56E434C9B1D703E3 ();
// 0x00000125 System.Boolean ES3Settings::IsAbsolute(System.String)
extern void ES3Settings_IsAbsolute_mD0A27C2F34CAE0D9AABB090997D81F31D88E738C ();
// 0x00000126 System.Object ES3Settings::Clone()
extern void ES3Settings_Clone_m7C28E3FFE2301E690BB97D826BB24C6479CA778B ();
// 0x00000127 System.Void ES3Settings::CopyInto(ES3Settings)
extern void ES3Settings_CopyInto_m7025842713F68362DDC7B2493E43966C74830B56 ();
// 0x00000128 System.Void ES3Settings::.cctor()
extern void ES3Settings__cctor_mEBBCA0B1E301CE41B37295DC1773D37C981371A3 ();
// 0x00000129 System.Void ES3SerializableSettings::.ctor()
extern void ES3SerializableSettings__ctor_m7B19BA579AC8648C79D32F9B5BC32B8C10304555 ();
// 0x0000012A System.Void ES3SerializableSettings::.ctor(System.Boolean)
extern void ES3SerializableSettings__ctor_mBBEEC2192360AC655048F2625E059E0E4A62ADF6 ();
// 0x0000012B System.Void ES3SerializableSettings::.ctor(System.String)
extern void ES3SerializableSettings__ctor_m4E934731D916B440D6B7ABFFA1A745C941C78B0D ();
// 0x0000012C System.Void ES3SerializableSettings::.ctor(System.String,ES3_Location)
extern void ES3SerializableSettings__ctor_m3C22763E61F3469BECCB88E24FF6CA7BA8F0874F ();
// 0x0000012D System.Void ES3Ref::.ctor(System.Int64)
extern void ES3Ref__ctor_m87E69980DAA88B2D9BA9181A762EFEDE81C31459 ();
// 0x0000012E System.Void ES3Cloud::.ctor(System.String,System.String)
extern void ES3Cloud__ctor_m072D5FF64813D0BF5296CFD829B7B64F269ACB0F ();
// 0x0000012F System.Byte[] ES3Cloud::get_data()
extern void ES3Cloud_get_data_mAB407FDFA4FA3D5D0140D90A587A383E9C566FBD ();
// 0x00000130 System.String ES3Cloud::get_text()
extern void ES3Cloud_get_text_m60890F0C77E8CF50DBE91D2B081B2116CF4E83CF ();
// 0x00000131 System.String[] ES3Cloud::get_filenames()
extern void ES3Cloud_get_filenames_m313E6726F529E19FA4B7FE67CC4B698A281E1930 ();
// 0x00000132 System.DateTime ES3Cloud::get_timestamp()
extern void ES3Cloud_get_timestamp_m336FE907512134E0F9A2B7B41FDF98863F0608C6 ();
// 0x00000133 System.Collections.IEnumerator ES3Cloud::Sync()
extern void ES3Cloud_Sync_mAFF71A47D49921B03FB4518E57FB37766E9420AF ();
// 0x00000134 System.Collections.IEnumerator ES3Cloud::Sync(System.String)
extern void ES3Cloud_Sync_m7B3714CA1E3720C34F22D4C865E82E94E4D32851 ();
// 0x00000135 System.Collections.IEnumerator ES3Cloud::Sync(System.String,System.String)
extern void ES3Cloud_Sync_m0A9044CEE7ECF565C2E15ADF9653122F2F03CE90 ();
// 0x00000136 System.Collections.IEnumerator ES3Cloud::Sync(System.String,System.String,System.String)
extern void ES3Cloud_Sync_mAA17F0B41150D3D968263E2BCA57A7B7FFC1EA5D ();
// 0x00000137 System.Collections.IEnumerator ES3Cloud::Sync(System.String,ES3Settings)
extern void ES3Cloud_Sync_mF056DA7C1E14986EB1EF38C4CF162DEE0B9E8778 ();
// 0x00000138 System.Collections.IEnumerator ES3Cloud::Sync(System.String,System.String,ES3Settings)
extern void ES3Cloud_Sync_m499376332C418CEFBFD4125B8DF8E00C8C6CF0BE ();
// 0x00000139 System.Collections.IEnumerator ES3Cloud::Sync(System.String,System.String,System.String,ES3Settings)
extern void ES3Cloud_Sync_mB19FC8FA4A161ACA3B4CC333279B9E38A3020E71 ();
// 0x0000013A System.Collections.IEnumerator ES3Cloud::Sync(ES3Settings,System.String,System.String)
extern void ES3Cloud_Sync_mBC9B7F29C9C999AD6CF61D40B202F7B4A18DDFA2 ();
// 0x0000013B System.Collections.IEnumerator ES3Cloud::UploadFile()
extern void ES3Cloud_UploadFile_m6E32EC45CE84359DCBEBFB0C240F1192CFAA33C4 ();
// 0x0000013C System.Collections.IEnumerator ES3Cloud::UploadFile(System.String)
extern void ES3Cloud_UploadFile_m9A29F8AA221F4C22016DCF2205192B0418BCB80A ();
// 0x0000013D System.Collections.IEnumerator ES3Cloud::UploadFile(System.String,System.String)
extern void ES3Cloud_UploadFile_mCA820877D2C927BE3B0EC94B6045149B3DE38718 ();
// 0x0000013E System.Collections.IEnumerator ES3Cloud::UploadFile(System.String,System.String,System.String)
extern void ES3Cloud_UploadFile_m0FE9863888D3F6AA8D5CDD2CA87BCC9AEDE65614 ();
// 0x0000013F System.Collections.IEnumerator ES3Cloud::UploadFile(System.String,ES3Settings)
extern void ES3Cloud_UploadFile_m8C9779C4DA3B08BAABF52646E34CD50891D47C0E ();
// 0x00000140 System.Collections.IEnumerator ES3Cloud::UploadFile(System.String,System.String,ES3Settings)
extern void ES3Cloud_UploadFile_mF1C62E2DD407B5D30B12735C81E1F56C851F8947 ();
// 0x00000141 System.Collections.IEnumerator ES3Cloud::UploadFile(System.String,System.String,System.String,ES3Settings)
extern void ES3Cloud_UploadFile_m1CE23D99127093B3671E0C4E55C29A9A4F72BAFC ();
// 0x00000142 System.Collections.IEnumerator ES3Cloud::UploadFile(ES3File)
extern void ES3Cloud_UploadFile_mBF1C8E1CAABE09169EA6144C659BB090B8632341 ();
// 0x00000143 System.Collections.IEnumerator ES3Cloud::UploadFile(ES3File,System.String)
extern void ES3Cloud_UploadFile_mCBA08DE975F15E07A41EEA146951E44540EEFBF8 ();
// 0x00000144 System.Collections.IEnumerator ES3Cloud::UploadFile(ES3File,System.String,System.String)
extern void ES3Cloud_UploadFile_m48D3C117E03655CBA3B4EF904D245B1E9BF91A87 ();
// 0x00000145 System.Collections.IEnumerator ES3Cloud::UploadFile(ES3Settings,System.String,System.String)
extern void ES3Cloud_UploadFile_m713B67188204F3FF0C2BC4D1091292C0E21C458B ();
// 0x00000146 System.Collections.IEnumerator ES3Cloud::UploadFile(System.Byte[],ES3Settings,System.String,System.String)
extern void ES3Cloud_UploadFile_m932159E5998CE08FFB78C62471DAC57BCD230923 ();
// 0x00000147 System.Collections.IEnumerator ES3Cloud::UploadFile(System.Byte[],ES3Settings,System.String,System.String,System.Int64)
extern void ES3Cloud_UploadFile_m5390A39E5BE820257FB24BE236FCD3474AD44368 ();
// 0x00000148 System.Collections.IEnumerator ES3Cloud::DownloadFile()
extern void ES3Cloud_DownloadFile_mB65D1E34A9B8BF80B72B2819B5EA69A53D9A0743 ();
// 0x00000149 System.Collections.IEnumerator ES3Cloud::DownloadFile(System.String)
extern void ES3Cloud_DownloadFile_mEB3FB82F029E2418419B8D22E7579D93DEBCEE19 ();
// 0x0000014A System.Collections.IEnumerator ES3Cloud::DownloadFile(System.String,System.String)
extern void ES3Cloud_DownloadFile_mEE88FE09029589027EE8B278B90A7D111F25F8CC ();
// 0x0000014B System.Collections.IEnumerator ES3Cloud::DownloadFile(System.String,System.String,System.String)
extern void ES3Cloud_DownloadFile_mB351CCDDE17DA1FB6D5F6604511E1C7DAE5E2F0A ();
// 0x0000014C System.Collections.IEnumerator ES3Cloud::DownloadFile(System.String,ES3Settings)
extern void ES3Cloud_DownloadFile_m744737152224496ECC5F8CCC23BB4A8074418589 ();
// 0x0000014D System.Collections.IEnumerator ES3Cloud::DownloadFile(System.String,System.String,ES3Settings)
extern void ES3Cloud_DownloadFile_m8EBA3A0BE648E60B10E3E8D6BE20004CDD881903 ();
// 0x0000014E System.Collections.IEnumerator ES3Cloud::DownloadFile(System.String,System.String,System.String,ES3Settings)
extern void ES3Cloud_DownloadFile_m350F6BB180DE0DD099B04B0E8AE0CBBEC2596C89 ();
// 0x0000014F System.Collections.IEnumerator ES3Cloud::DownloadFile(ES3File)
extern void ES3Cloud_DownloadFile_m08680260C0C39E31540E6E62165FA8281CE352B5 ();
// 0x00000150 System.Collections.IEnumerator ES3Cloud::DownloadFile(ES3File,System.String)
extern void ES3Cloud_DownloadFile_m00409B6B79685F5FA64BE21B4D5236BFA1AB7D6F ();
// 0x00000151 System.Collections.IEnumerator ES3Cloud::DownloadFile(ES3File,System.String,System.String)
extern void ES3Cloud_DownloadFile_mDDD28A721F24A156BF5FDD64531E9640FEB489DE ();
// 0x00000152 System.Collections.IEnumerator ES3Cloud::DownloadFile(ES3File,System.String,System.String,System.Int64)
extern void ES3Cloud_DownloadFile_mF44F1FA229262AE9FFD6FA24A3205A2C9AC7D216 ();
// 0x00000153 System.Collections.IEnumerator ES3Cloud::DownloadFile(ES3Settings,System.String,System.String,System.Int64)
extern void ES3Cloud_DownloadFile_m546D47A2A65E1B60531320A1AB7D9A12AFAEB8A5 ();
// 0x00000154 System.Collections.IEnumerator ES3Cloud::DeleteFile()
extern void ES3Cloud_DeleteFile_m4A380F81BA33024C36B15951B0CCA6CF5AE63F29 ();
// 0x00000155 System.Collections.IEnumerator ES3Cloud::DeleteFile(System.String)
extern void ES3Cloud_DeleteFile_m0B30AA4AC8190E16BA95C12BD81C66449CD39197 ();
// 0x00000156 System.Collections.IEnumerator ES3Cloud::DeleteFile(System.String,System.String)
extern void ES3Cloud_DeleteFile_m7C0695B9AF1BF306CD5CEAE8DFD0EDABAD39228D ();
// 0x00000157 System.Collections.IEnumerator ES3Cloud::DeleteFile(System.String,System.String,System.String)
extern void ES3Cloud_DeleteFile_mFE6E0C4E2A6DC4A1A3C110B01221DEE8415498ED ();
// 0x00000158 System.Collections.IEnumerator ES3Cloud::DeleteFile(System.String,ES3Settings)
extern void ES3Cloud_DeleteFile_mA5D50E3CE25DBBE8F70928787D3859A25B756166 ();
// 0x00000159 System.Collections.IEnumerator ES3Cloud::DeleteFile(System.String,System.String,ES3Settings)
extern void ES3Cloud_DeleteFile_mC901638D3C65350485A212FE34FF4DFF81F6E0ED ();
// 0x0000015A System.Collections.IEnumerator ES3Cloud::DeleteFile(System.String,System.String,System.String,ES3Settings)
extern void ES3Cloud_DeleteFile_mFA5C7C45A38BBF1CC27762CAC3CFAD4662F8CDAF ();
// 0x0000015B System.Collections.IEnumerator ES3Cloud::DeleteFile(ES3Settings,System.String,System.String)
extern void ES3Cloud_DeleteFile_m4552A5AA7DBC7E807949ED52E404721887A6082E ();
// 0x0000015C System.Collections.IEnumerator ES3Cloud::RenameFile(System.String,System.String)
extern void ES3Cloud_RenameFile_m3576D8BC23EB65D05638767EAD47B58C672B2AB8 ();
// 0x0000015D System.Collections.IEnumerator ES3Cloud::RenameFile(System.String,System.String,System.String)
extern void ES3Cloud_RenameFile_mA811DCDFAAA4ACFA7CAE5F49F9AF79E76835D3F9 ();
// 0x0000015E System.Collections.IEnumerator ES3Cloud::RenameFile(System.String,System.String,System.String,System.String)
extern void ES3Cloud_RenameFile_m6DCA53F6E9C6014C8DCA1BFE22863824ED3F6D83 ();
// 0x0000015F System.Collections.IEnumerator ES3Cloud::RenameFile(System.String,System.String,ES3Settings)
extern void ES3Cloud_RenameFile_m28174978D360F572463731064423B74F8AE32DD4 ();
// 0x00000160 System.Collections.IEnumerator ES3Cloud::RenameFile(System.String,System.String,System.String,ES3Settings)
extern void ES3Cloud_RenameFile_mE917649F9AB64622C4C67936B2F0725A9AEAD8F4 ();
// 0x00000161 System.Collections.IEnumerator ES3Cloud::RenameFile(System.String,System.String,System.String,System.String,ES3Settings)
extern void ES3Cloud_RenameFile_m14CF4D0E172B998BAAA59C1955B071815951660A ();
// 0x00000162 System.Collections.IEnumerator ES3Cloud::RenameFile(ES3Settings,ES3Settings,System.String,System.String)
extern void ES3Cloud_RenameFile_m33D2812826427CEB0BCEDFD8A84C77993442338C ();
// 0x00000163 System.Collections.IEnumerator ES3Cloud::DownloadFilenames(System.String,System.String)
extern void ES3Cloud_DownloadFilenames_m0BC6065021079A5EC18B76B1E03BFA74E971139B ();
// 0x00000164 System.Collections.IEnumerator ES3Cloud::SearchFilenames(System.String,System.String,System.String)
extern void ES3Cloud_SearchFilenames_mE37F97ADB55960AC1CCD5A72605B35B08AD03041 ();
// 0x00000165 System.Collections.IEnumerator ES3Cloud::DownloadTimestamp()
extern void ES3Cloud_DownloadTimestamp_m0E596E8D543DD2519BD1705B0EEB9F3856E049E9 ();
// 0x00000166 System.Collections.IEnumerator ES3Cloud::DownloadTimestamp(System.String)
extern void ES3Cloud_DownloadTimestamp_m5C6325C3FFFE062D87B6C4D2A9D640B1FE08D087 ();
// 0x00000167 System.Collections.IEnumerator ES3Cloud::DownloadTimestamp(System.String,System.String)
extern void ES3Cloud_DownloadTimestamp_m56EFF21C3F882C5C747920E822AEF840E923B9DA ();
// 0x00000168 System.Collections.IEnumerator ES3Cloud::DownloadTimestamp(System.String,System.String,System.String)
extern void ES3Cloud_DownloadTimestamp_m701316872D74979E46CC9B50BFBC49844F96AB86 ();
// 0x00000169 System.Collections.IEnumerator ES3Cloud::DownloadTimestamp(System.String,ES3Settings)
extern void ES3Cloud_DownloadTimestamp_mCA8E8F290EDF7DF4C291D75C8EFBBF7F97CDB7A0 ();
// 0x0000016A System.Collections.IEnumerator ES3Cloud::DownloadTimestamp(System.String,System.String,ES3Settings)
extern void ES3Cloud_DownloadTimestamp_mE2B69499C715C252A12626DF00793838DDB2580D ();
// 0x0000016B System.Collections.IEnumerator ES3Cloud::DownloadTimestamp(System.String,System.String,System.String,ES3Settings)
extern void ES3Cloud_DownloadTimestamp_m8DF64717CCC4A51543F2A325C24E3D036E2287E9 ();
// 0x0000016C System.Collections.IEnumerator ES3Cloud::DownloadTimestamp(ES3Settings,System.String,System.String)
extern void ES3Cloud_DownloadTimestamp_mF716C5454907940EBDA2B26B9009070D4D0E1B0E ();
// 0x0000016D System.Int64 ES3Cloud::DateTimeToUnixTimestamp(System.DateTime)
extern void ES3Cloud_DateTimeToUnixTimestamp_m707B8B00877793F62AA3840CE57D5F00872EDE77 ();
// 0x0000016E System.Int64 ES3Cloud::GetFileTimestamp(ES3Settings)
extern void ES3Cloud_GetFileTimestamp_mD0B5C6DF14430675FD6496FD1869895F7DDF94F9 ();
// 0x0000016F System.Void ES3Cloud::Reset()
extern void ES3Cloud_Reset_m1301B0B621A18C13E13B866A6B7DB0E3D0EDF31C ();
// 0x00000170 System.Void ES3Writer::WriteNull()
// 0x00000171 System.Void ES3Writer::StartWriteFile()
extern void ES3Writer_StartWriteFile_m4A9558E362E4979381B099747A14477054D3D678 ();
// 0x00000172 System.Void ES3Writer::EndWriteFile()
extern void ES3Writer_EndWriteFile_m0BDD5927CA3AD2C33126186CA25EEF194D3EB50E ();
// 0x00000173 System.Void ES3Writer::StartWriteObject(System.String)
extern void ES3Writer_StartWriteObject_mEE85E07EB3920D8EE6FA16BC766C06865F88F879 ();
// 0x00000174 System.Void ES3Writer::EndWriteObject(System.String)
extern void ES3Writer_EndWriteObject_m38AA1FA1567CB7F1AEB46A5EFDCC986D5737A49E ();
// 0x00000175 System.Void ES3Writer::StartWriteProperty(System.String)
extern void ES3Writer_StartWriteProperty_mCBE70E6485B9267AEBBE6CCE787CDA183C027022 ();
// 0x00000176 System.Void ES3Writer::EndWriteProperty(System.String)
extern void ES3Writer_EndWriteProperty_m2D4F1791BBE89BB476ACAD369C21E81D1097BB50 ();
// 0x00000177 System.Void ES3Writer::StartWriteCollection()
extern void ES3Writer_StartWriteCollection_mA8D3B8871ADB3EED4ED6CD5A94948BD62947656F ();
// 0x00000178 System.Void ES3Writer::EndWriteCollection()
extern void ES3Writer_EndWriteCollection_m8EB3510B8C3D92BF128B47F5F517A9DEAE360CA5 ();
// 0x00000179 System.Void ES3Writer::StartWriteCollectionItem(System.Int32)
// 0x0000017A System.Void ES3Writer::EndWriteCollectionItem(System.Int32)
// 0x0000017B System.Void ES3Writer::StartWriteDictionary()
// 0x0000017C System.Void ES3Writer::EndWriteDictionary()
// 0x0000017D System.Void ES3Writer::StartWriteDictionaryKey(System.Int32)
// 0x0000017E System.Void ES3Writer::EndWriteDictionaryKey(System.Int32)
// 0x0000017F System.Void ES3Writer::StartWriteDictionaryValue(System.Int32)
// 0x00000180 System.Void ES3Writer::EndWriteDictionaryValue(System.Int32)
// 0x00000181 System.Void ES3Writer::Dispose()
// 0x00000182 System.Void ES3Writer::WriteRawProperty(System.String,System.Byte[])
// 0x00000183 System.Void ES3Writer::WritePrimitive(System.Int32)
// 0x00000184 System.Void ES3Writer::WritePrimitive(System.Single)
// 0x00000185 System.Void ES3Writer::WritePrimitive(System.Boolean)
// 0x00000186 System.Void ES3Writer::WritePrimitive(System.Decimal)
// 0x00000187 System.Void ES3Writer::WritePrimitive(System.Double)
// 0x00000188 System.Void ES3Writer::WritePrimitive(System.Int64)
// 0x00000189 System.Void ES3Writer::WritePrimitive(System.UInt64)
// 0x0000018A System.Void ES3Writer::WritePrimitive(System.UInt32)
// 0x0000018B System.Void ES3Writer::WritePrimitive(System.Byte)
// 0x0000018C System.Void ES3Writer::WritePrimitive(System.SByte)
// 0x0000018D System.Void ES3Writer::WritePrimitive(System.Int16)
// 0x0000018E System.Void ES3Writer::WritePrimitive(System.UInt16)
// 0x0000018F System.Void ES3Writer::WritePrimitive(System.Char)
// 0x00000190 System.Void ES3Writer::WritePrimitive(System.String)
// 0x00000191 System.Void ES3Writer::WritePrimitive(System.Byte[])
// 0x00000192 System.Void ES3Writer::.ctor(ES3Settings,System.Boolean,System.Boolean)
extern void ES3Writer__ctor_m40A8EF5042424F82BE2DD0449A1BF3954EAE39DA ();
// 0x00000193 System.Void ES3Writer::Write(System.String,System.Type,System.Byte[])
extern void ES3Writer_Write_m54C2EA500C058E8982599253E721D0E550804E67 ();
// 0x00000194 System.Void ES3Writer::Write(System.String,System.Object)
// 0x00000195 System.Void ES3Writer::Write(System.Type,System.String,System.Object)
extern void ES3Writer_Write_mE3278DE9E7B7069688D4B449521A19618BE5DA42 ();
// 0x00000196 System.Void ES3Writer::Write(System.Object,ES3_ReferenceMode)
extern void ES3Writer_Write_m2B0FFC0A8DB2401C7CC6938EF14F0A22CB41E30D ();
// 0x00000197 System.Void ES3Writer::Write(System.Object,ES3Types.ES3Type,ES3_ReferenceMode)
extern void ES3Writer_Write_m2111F8ADF9880838AE50D120AFDD307F0FF6BC40 ();
// 0x00000198 System.Void ES3Writer::WriteRef(UnityEngine.Object)
extern void ES3Writer_WriteRef_mED9AF6E95AE1984ECADF51934EC0D38DFFD5F939 ();
// 0x00000199 System.Void ES3Writer::WriteProperty(System.String,System.Object)
extern void ES3Writer_WriteProperty_m02AEA4AA6307F0D9B6E5B546A9E7890AC12097B6 ();
// 0x0000019A System.Void ES3Writer::WriteProperty(System.String,System.Object,ES3_ReferenceMode)
extern void ES3Writer_WriteProperty_m53FB2DD601FD74D2DB3E32090D78BAE53A31617A ();
// 0x0000019B System.Void ES3Writer::WriteProperty(System.String,System.Object)
// 0x0000019C System.Void ES3Writer::WriteProperty(System.String,System.Object,ES3Types.ES3Type)
extern void ES3Writer_WriteProperty_m297289B93F0C3C4C7A5B611346C52CC867D7DAE6 ();
// 0x0000019D System.Void ES3Writer::WriteProperty(System.String,System.Object,ES3Types.ES3Type,ES3_ReferenceMode)
extern void ES3Writer_WriteProperty_m63BE9A6C4A67D4EFBD2ABA201CBD9F8815D36DE3 ();
// 0x0000019E System.Void ES3Writer::WritePropertyByRef(System.String,UnityEngine.Object)
extern void ES3Writer_WritePropertyByRef_mBBBF59175AEE334D1CDFCBF672B564F0946AFB5A ();
// 0x0000019F System.Void ES3Writer::WritePrivateProperty(System.String,System.Object)
extern void ES3Writer_WritePrivateProperty_m965CC085DA3736F7AEC39D35080284C14DCD68C4 ();
// 0x000001A0 System.Void ES3Writer::WritePrivateField(System.String,System.Object)
extern void ES3Writer_WritePrivateField_m49CD30E6803CB527852527B714605A4395F2900C ();
// 0x000001A1 System.Void ES3Writer::WritePrivateProperty(System.String,System.Object,ES3Types.ES3Type)
extern void ES3Writer_WritePrivateProperty_mBB08F40928E0DBED2FB4E507EA51AFB99863C239 ();
// 0x000001A2 System.Void ES3Writer::WritePrivateField(System.String,System.Object,ES3Types.ES3Type)
extern void ES3Writer_WritePrivateField_m8B3EB35670B7137F11951494AEDDFA26DD9C1291 ();
// 0x000001A3 System.Void ES3Writer::WritePrivatePropertyByRef(System.String,System.Object)
extern void ES3Writer_WritePrivatePropertyByRef_m5DACC34AE51F3B52E6BD182F8CF145889AD5AEC7 ();
// 0x000001A4 System.Void ES3Writer::WritePrivateFieldByRef(System.String,System.Object)
extern void ES3Writer_WritePrivateFieldByRef_m13F98F4E1944DD6E902BB343A4F011B21B56A3DA ();
// 0x000001A5 System.Void ES3Writer::WriteType(System.Type)
extern void ES3Writer_WriteType_m082922ACB15EC0F069DB997F2D417F9CC4073275 ();
// 0x000001A6 ES3Writer ES3Writer::Create(System.String,ES3Settings)
extern void ES3Writer_Create_m9031A8B846FD09DF2EB9DCBE6C31F2C159349764 ();
// 0x000001A7 ES3Writer ES3Writer::Create(ES3Settings)
extern void ES3Writer_Create_mD97C535634FA6D5A62784EE7D71FF7D78F97822D ();
// 0x000001A8 ES3Writer ES3Writer::Create(ES3Settings,System.Boolean,System.Boolean,System.Boolean)
extern void ES3Writer_Create_mFA3C8FA39007FF3DCF9DD9EC23AF452A7FD52424 ();
// 0x000001A9 ES3Writer ES3Writer::Create(System.IO.Stream,ES3Settings,System.Boolean,System.Boolean)
extern void ES3Writer_Create_m854DAC5E022D7244231A37906B0DBA6217BDED41 ();
// 0x000001AA System.Boolean ES3Writer::SerializationDepthLimitExceeded()
extern void ES3Writer_SerializationDepthLimitExceeded_mF83656E02375E42A2450498D6C0D451438A64A8F ();
// 0x000001AB System.Void ES3Writer::MarkKeyForDeletion(System.String)
extern void ES3Writer_MarkKeyForDeletion_m85CABC52BF2C41379F44EEA82CE17D8388246A6C ();
// 0x000001AC System.Void ES3Writer::Merge()
extern void ES3Writer_Merge_m7CD4C2A7FDC2F4812C107DF5602B7C544E606231 ();
// 0x000001AD System.Void ES3Writer::Merge(ES3Reader)
extern void ES3Writer_Merge_m324EB02B2D3C621E84E4A1C5DF08BEBCBE1DB5A3 ();
// 0x000001AE System.Void ES3Writer::Save()
extern void ES3Writer_Save_mD751854BD8A5F1B2AB52926D12DBE0253D734416 ();
// 0x000001AF System.Void ES3Writer::Save(System.Boolean)
extern void ES3Writer_Save_m10CC2CDD279033A701E313A440D23D3A713BF77A ();
// 0x000001B0 System.Void ES3XMLWriter::.ctor()
extern void ES3XMLWriter__ctor_m515C7F651A44D4AF864F7D62B5B5A9D155616A91 ();
// 0x000001B1 System.Void ES3Types.ES3Type_ES3Prefab::.ctor()
extern void ES3Type_ES3Prefab__ctor_mC382EABDD13DAE44F18F2058C5973220ADB8A514 ();
// 0x000001B2 System.Void ES3Types.ES3Type_ES3Prefab::Write(System.Object,ES3Writer)
extern void ES3Type_ES3Prefab_Write_m94AE7C7FA96BF5506D094A5DBBE474F4E3E793F0 ();
// 0x000001B3 System.Object ES3Types.ES3Type_ES3Prefab::Read(ES3Reader)
// 0x000001B4 System.Void ES3Types.ES3Type_ES3Prefab::.cctor()
extern void ES3Type_ES3Prefab__cctor_m99AC978D69E1D23E7EA509886425A0F0A721CE8F ();
// 0x000001B5 System.Void ES3Types.ES3Type_ES3PrefabInternal::.ctor()
extern void ES3Type_ES3PrefabInternal__ctor_mBBD07E83D2E18994EA89C540C0407E9C3B91BA7C ();
// 0x000001B6 System.Void ES3Types.ES3Type_ES3PrefabInternal::Write(System.Object,ES3Writer)
extern void ES3Type_ES3PrefabInternal_Write_m501520A6FCAD05943ED777F6093CAF086408F9DB ();
// 0x000001B7 System.Object ES3Types.ES3Type_ES3PrefabInternal::Read(ES3Reader)
// 0x000001B8 System.Void ES3Types.ES3Type_ES3PrefabInternal::.cctor()
extern void ES3Type_ES3PrefabInternal__cctor_m3E078C37A0440B5045D5D53B101B03B7CA11054F ();
// 0x000001B9 System.Void ES3Types.ES32DArrayType::.ctor(System.Type)
extern void ES32DArrayType__ctor_mEE00FC72FF86DCE0C8273894772AEBF319DCE7C8 ();
// 0x000001BA System.Void ES3Types.ES32DArrayType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES32DArrayType_Write_m9D5A8F2AC8376A37A291E86AD4B108ADB9E4893A ();
// 0x000001BB System.Object ES3Types.ES32DArrayType::Read(ES3Reader)
// 0x000001BC System.Object ES3Types.ES32DArrayType::Read(ES3Reader)
extern void ES32DArrayType_Read_mA525DB06FDA85AC2542B75D3A1258F00B7115ADD ();
// 0x000001BD System.Void ES3Types.ES32DArrayType::ReadInto(ES3Reader,System.Object)
// 0x000001BE System.Void ES3Types.ES32DArrayType::ReadInto(ES3Reader,System.Object)
extern void ES32DArrayType_ReadInto_m0D7B554B96F2437159D4EABDF89D51DF1B59717D ();
// 0x000001BF System.Void ES3Types.ES33DArrayType::.ctor(System.Type)
extern void ES33DArrayType__ctor_m7DCE734BBA91DFC160C0FD735AD002767A40AD39 ();
// 0x000001C0 System.Void ES3Types.ES33DArrayType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES33DArrayType_Write_mD724D44F42A8561B78F1666CE35CC0176A8667C0 ();
// 0x000001C1 System.Object ES3Types.ES33DArrayType::Read(ES3Reader)
// 0x000001C2 System.Object ES3Types.ES33DArrayType::Read(ES3Reader)
extern void ES33DArrayType_Read_m8EE1219336E51FC952CC184A68891EEBECC46DA2 ();
// 0x000001C3 System.Void ES3Types.ES33DArrayType::ReadInto(ES3Reader,System.Object)
// 0x000001C4 System.Void ES3Types.ES33DArrayType::ReadInto(ES3Reader,System.Object)
extern void ES33DArrayType_ReadInto_mDF458F339875C41265C9ECA36E34A15A3A909ABF ();
// 0x000001C5 System.Void ES3Types.ES3ArrayType::.ctor(System.Type)
extern void ES3ArrayType__ctor_m8EE2D6B2A9CDBFE86CC445643D5004336631E7E9 ();
// 0x000001C6 System.Void ES3Types.ES3ArrayType::.ctor(System.Type,ES3Types.ES3Type)
extern void ES3ArrayType__ctor_mEA0F23FA8CF8EC6FD1A369CD2F636B68553421BE ();
// 0x000001C7 System.Void ES3Types.ES3ArrayType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3ArrayType_Write_m84B6BCE5067E444904EC44F8CCC0E3D62F133B92 ();
// 0x000001C8 System.Object ES3Types.ES3ArrayType::Read(ES3Reader)
extern void ES3ArrayType_Read_m6C523B6EC44CDD4861E4A08894ADBBFE1C3AF1E5 ();
// 0x000001C9 System.Object ES3Types.ES3ArrayType::Read(ES3Reader)
// 0x000001CA System.Void ES3Types.ES3ArrayType::ReadInto(ES3Reader,System.Object)
// 0x000001CB System.Void ES3Types.ES3ArrayType::ReadInto(ES3Reader,System.Object)
extern void ES3ArrayType_ReadInto_mF618476A6339B6EB4F65EA1A8C9CBA210644AB5C ();
// 0x000001CC System.Object ES3Types.ES3CollectionType::Read(ES3Reader)
// 0x000001CD System.Void ES3Types.ES3CollectionType::ReadInto(ES3Reader,System.Object)
// 0x000001CE System.Void ES3Types.ES3CollectionType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
// 0x000001CF System.Void ES3Types.ES3CollectionType::.ctor(System.Type)
extern void ES3CollectionType__ctor_mF904DB0113A81FD13B9C30350A58552AE3997CC9 ();
// 0x000001D0 System.Void ES3Types.ES3CollectionType::.ctor(System.Type,ES3Types.ES3Type)
extern void ES3CollectionType__ctor_mD4F0464DF0D38ED1B26A6A3081A7B3EA383D08CA ();
// 0x000001D1 System.Void ES3Types.ES3CollectionType::Write(System.Object,ES3Writer)
extern void ES3CollectionType_Write_mB9C5DDCD7BC918F47B54FC4831075153F5AF1680 ();
// 0x000001D2 System.Boolean ES3Types.ES3CollectionType::ReadICollection(ES3Reader,System.Collections.Generic.ICollection`1<T>,ES3Types.ES3Type)
// 0x000001D3 System.Void ES3Types.ES3CollectionType::ReadICollectionInto(ES3Reader,System.Collections.Generic.ICollection`1<T>,ES3Types.ES3Type)
// 0x000001D4 System.Void ES3Types.ES3CollectionType::ReadICollectionInto(ES3Reader,System.Collections.ICollection,ES3Types.ES3Type)
extern void ES3CollectionType_ReadICollectionInto_mB0251AE1E13CD925262D16DAD681B5ADC7514C74 ();
// 0x000001D5 System.Void ES3Types.ES3DictionaryType::.ctor(System.Type)
extern void ES3DictionaryType__ctor_m82AD6C47AD75B2FE6B85DBA8ED8324B9FBA32558 ();
// 0x000001D6 System.Void ES3Types.ES3DictionaryType::.ctor(System.Type,ES3Types.ES3Type,ES3Types.ES3Type)
extern void ES3DictionaryType__ctor_mFEC8022DDE5B7FCC75AFF2F5D306840F0FBF5D4D ();
// 0x000001D7 System.Void ES3Types.ES3DictionaryType::Write(System.Object,ES3Writer)
extern void ES3DictionaryType_Write_m46C7BECDB5FFEBA31A61AC4F58EB2134D61E169F ();
// 0x000001D8 System.Void ES3Types.ES3DictionaryType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3DictionaryType_Write_mC365B039AFD955CE6B38321BB28D32A110AFDDB9 ();
// 0x000001D9 System.Object ES3Types.ES3DictionaryType::Read(ES3Reader)
// 0x000001DA System.Void ES3Types.ES3DictionaryType::ReadInto(ES3Reader,System.Object)
// 0x000001DB System.Object ES3Types.ES3DictionaryType::Read(ES3Reader)
extern void ES3DictionaryType_Read_m587E2BF8F94F2A045AA7173017FA3EB5B3D8A302 ();
// 0x000001DC System.Void ES3Types.ES3DictionaryType::ReadInto(ES3Reader,System.Object)
extern void ES3DictionaryType_ReadInto_mD79066D37F150937BBE5AC80F8A8630694734996 ();
// 0x000001DD System.Void ES3Types.ES3HashSetType::.ctor(System.Type)
extern void ES3HashSetType__ctor_m1CCB7FECDB2AAE31C0F91D9099A8EFF3B77816AB ();
// 0x000001DE System.Void ES3Types.ES3HashSetType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3HashSetType_Write_mC5011A54411BFAA863B199FFE28C8299F5EB726C ();
// 0x000001DF System.Object ES3Types.ES3HashSetType::Read(ES3Reader)
// 0x000001E0 System.Object ES3Types.ES3HashSetType::Read(ES3Reader)
extern void ES3HashSetType_Read_m27E85F6DD6A8404E1F4817493CB6A2D9E94F75AF ();
// 0x000001E1 System.Void ES3Types.ES3HashSetType::ReadInto(ES3Reader,System.Object)
// 0x000001E2 System.Void ES3Types.ES3HashSetType::ReadInto(ES3Reader,System.Object)
extern void ES3HashSetType_ReadInto_m68C4FCD08860FF22984DA4F3A631B990E5E32EA7 ();
// 0x000001E3 System.Void ES3Types.ES3ListType::.ctor(System.Type)
extern void ES3ListType__ctor_m7CB3D8293ACC66031A9EAC1C706A5FE65D09D2A2 ();
// 0x000001E4 System.Void ES3Types.ES3ListType::.ctor(System.Type,ES3Types.ES3Type)
extern void ES3ListType__ctor_m929C68BF94A1BCB0EE95FDC83016E38188C7A8BF ();
// 0x000001E5 System.Void ES3Types.ES3ListType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3ListType_Write_m2705E26310FA670288CEE25899ABA6AF40499139 ();
// 0x000001E6 System.Object ES3Types.ES3ListType::Read(ES3Reader)
// 0x000001E7 System.Void ES3Types.ES3ListType::ReadInto(ES3Reader,System.Object)
// 0x000001E8 System.Object ES3Types.ES3ListType::Read(ES3Reader)
extern void ES3ListType_Read_mFC17880683C890D6CBE0AFDE743F8C29C270E7C5 ();
// 0x000001E9 System.Void ES3Types.ES3ListType::ReadInto(ES3Reader,System.Object)
extern void ES3ListType_ReadInto_m38C73DA7DF90D494357656553EFDD8CB1ECD6FE4 ();
// 0x000001EA System.Void ES3Types.ES3QueueType::.ctor(System.Type)
extern void ES3QueueType__ctor_mE124A6845E7493310C4778BAA6318E854BF06EBA ();
// 0x000001EB System.Void ES3Types.ES3QueueType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3QueueType_Write_mCF0088FE8AEACB82DE57F3572DE8D782110E3CBE ();
// 0x000001EC System.Object ES3Types.ES3QueueType::Read(ES3Reader)
// 0x000001ED System.Void ES3Types.ES3QueueType::ReadInto(ES3Reader,System.Object)
// 0x000001EE System.Object ES3Types.ES3QueueType::Read(ES3Reader)
extern void ES3QueueType_Read_mBFD3947A0B9A3204DEF1F3AACAC293B1B3966EF2 ();
// 0x000001EF System.Void ES3Types.ES3QueueType::ReadInto(ES3Reader,System.Object)
extern void ES3QueueType_ReadInto_mF79F71CF96C24D8D45E2B31A594120585EAA6C90 ();
// 0x000001F0 System.Void ES3Types.ES3StackType::.ctor(System.Type)
extern void ES3StackType__ctor_m4C47A278E9C819ECB087405B8A603835F9A25A0C ();
// 0x000001F1 System.Void ES3Types.ES3StackType::Write(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3StackType_Write_mD77C5F6D3C6D30F1BD241B9E815A94440646BCBE ();
// 0x000001F2 System.Object ES3Types.ES3StackType::Read(ES3Reader)
// 0x000001F3 System.Void ES3Types.ES3StackType::ReadInto(ES3Reader,System.Object)
// 0x000001F4 System.Object ES3Types.ES3StackType::Read(ES3Reader)
extern void ES3StackType_Read_m3596316D86568543979958A89E2EF67155606ACD ();
// 0x000001F5 System.Void ES3Types.ES3StackType::ReadInto(ES3Reader,System.Object)
extern void ES3StackType_ReadInto_m01955D15A95AA2E27683BE1A94D427ABE7154EA4 ();
// 0x000001F6 System.Void ES3Types.ES3ComponentType::.ctor(System.Type)
extern void ES3ComponentType__ctor_mFAADE03FCF8669787D2FBB33DFEE3277705C8631 ();
// 0x000001F7 System.Void ES3Types.ES3ComponentType::WriteComponent(System.Object,ES3Writer)
// 0x000001F8 System.Void ES3Types.ES3ComponentType::ReadComponent(ES3Reader,System.Object)
// 0x000001F9 System.Void ES3Types.ES3ComponentType::WriteUnityObject(System.Object,ES3Writer)
extern void ES3ComponentType_WriteUnityObject_m7EAC0E6D2EE350D45B0AEA540896CB282C5F9C40 ();
// 0x000001FA System.Void ES3Types.ES3ComponentType::ReadUnityObject(ES3Reader,System.Object)
// 0x000001FB System.Object ES3Types.ES3ComponentType::ReadUnityObject(ES3Reader)
// 0x000001FC System.Object ES3Types.ES3ComponentType::ReadObject(ES3Reader)
// 0x000001FD UnityEngine.Component ES3Types.ES3ComponentType::GetOrAddComponent(UnityEngine.GameObject,System.Type)
extern void ES3ComponentType_GetOrAddComponent_m7C8F6701E4A588D29830D2A294A04FD830F73CDE ();
// 0x000001FE UnityEngine.Component ES3Types.ES3ComponentType::CreateComponent(System.Type)
extern void ES3ComponentType_CreateComponent_m14BA12F0AD88A1C74679FDAD5FA0D6950813A942 ();
// 0x000001FF System.Void ES3Types.ES3ObjectType::.ctor(System.Type)
extern void ES3ObjectType__ctor_m6CF8C0823BE72726B2F24D179AD044C0388C26C7 ();
// 0x00000200 System.Void ES3Types.ES3ObjectType::WriteObject(System.Object,ES3Writer)
// 0x00000201 System.Object ES3Types.ES3ObjectType::ReadObject(ES3Reader)
// 0x00000202 System.Void ES3Types.ES3ObjectType::ReadObject(ES3Reader,System.Object)
// 0x00000203 System.Void ES3Types.ES3ObjectType::Write(System.Object,ES3Writer)
extern void ES3ObjectType_Write_mF0552F57FB8E470497445821DE5715BD43666538 ();
// 0x00000204 System.Object ES3Types.ES3ObjectType::Read(ES3Reader)
// 0x00000205 System.Void ES3Types.ES3ObjectType::ReadInto(ES3Reader,System.Object)
// 0x00000206 System.Void ES3Types.ES3ScriptableObjectType::.ctor(System.Type)
extern void ES3ScriptableObjectType__ctor_mDA7449A8FCCF24E8F6520EF3182E61694C636B21 ();
// 0x00000207 System.Void ES3Types.ES3ScriptableObjectType::WriteScriptableObject(System.Object,ES3Writer)
// 0x00000208 System.Void ES3Types.ES3ScriptableObjectType::ReadScriptableObject(ES3Reader,System.Object)
// 0x00000209 System.Void ES3Types.ES3ScriptableObjectType::WriteUnityObject(System.Object,ES3Writer)
extern void ES3ScriptableObjectType_WriteUnityObject_mB531FB539A50D2F2D43B96C832930DCE6A64FE80 ();
// 0x0000020A System.Void ES3Types.ES3ScriptableObjectType::ReadUnityObject(ES3Reader,System.Object)
// 0x0000020B System.Object ES3Types.ES3ScriptableObjectType::ReadUnityObject(ES3Reader)
// 0x0000020C System.Object ES3Types.ES3ScriptableObjectType::ReadObject(ES3Reader)
// 0x0000020D System.Void ES3Types.ES3Type::.ctor(System.Type)
extern void ES3Type__ctor_mE1C65E5D0EA2D6500FA0A888B1A9E1B57513F0C1 ();
// 0x0000020E System.Void ES3Types.ES3Type::Write(System.Object,ES3Writer)
// 0x0000020F System.Object ES3Types.ES3Type::Read(ES3Reader)
// 0x00000210 System.Void ES3Types.ES3Type::ReadInto(ES3Reader,System.Object)
// 0x00000211 System.Boolean ES3Types.ES3Type::WriteUsingDerivedType(System.Object,ES3Writer)
extern void ES3Type_WriteUsingDerivedType_m679E9EA5FA7A62C370941558DA930B7CDC5F898B ();
// 0x00000212 System.Void ES3Types.ES3Type::ReadUsingDerivedType(ES3Reader,System.Object)
// 0x00000213 System.String ES3Types.ES3Type::ReadPropertyName(ES3Reader)
extern void ES3Type_ReadPropertyName_mADECA3ECA85BF90F751A77E005C329DACA10C17E ();
// 0x00000214 System.Void ES3Types.ES3Type::WriteProperties(System.Object,ES3Writer)
extern void ES3Type_WriteProperties_m1A3AF5D202695C48D8270F7FC2C8C2AA969852D5 ();
// 0x00000215 System.Object ES3Types.ES3Type::ReadProperties(ES3Reader,System.Object)
extern void ES3Type_ReadProperties_m7874DFD8EA6B4B8365363C35042A91336AA8641A ();
// 0x00000216 System.Void ES3Types.ES3Type::GetMembers(System.Boolean)
extern void ES3Type_GetMembers_m26197DAA4F5A88CA39BFBBC6D0D4CC4450D2C620 ();
// 0x00000217 System.Void ES3Types.ES3Type::GetMembers(System.Boolean,System.String[])
extern void ES3Type_GetMembers_m765A8DAD51D4A7C95229D5098930A24529B0255C ();
// 0x00000218 System.Void ES3Types.ES3PropertiesAttribute::.ctor(System.String[])
extern void ES3PropertiesAttribute__ctor_mECA1DAF4495D8A437110C4CA5606C01467F326A6 ();
// 0x00000219 System.Void ES3Types.ES3UnityObjectType::.ctor(System.Type)
extern void ES3UnityObjectType__ctor_m32F9E90464288D451D14321A7FB8FEB4F1E2E677 ();
// 0x0000021A System.Void ES3Types.ES3UnityObjectType::WriteUnityObject(System.Object,ES3Writer)
// 0x0000021B System.Void ES3Types.ES3UnityObjectType::ReadUnityObject(ES3Reader,System.Object)
// 0x0000021C System.Object ES3Types.ES3UnityObjectType::ReadUnityObject(ES3Reader)
// 0x0000021D System.Void ES3Types.ES3UnityObjectType::WriteObject(System.Object,ES3Writer)
extern void ES3UnityObjectType_WriteObject_m9F8B2345A293644E321B63C464C6EAFCFB4466E6 ();
// 0x0000021E System.Void ES3Types.ES3UnityObjectType::WriteObject(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3UnityObjectType_WriteObject_m3662F3526215112CE4BC09F7B40EED1BA18F4554 ();
// 0x0000021F System.Void ES3Types.ES3UnityObjectType::ReadObject(ES3Reader,System.Object)
// 0x00000220 System.Object ES3Types.ES3UnityObjectType::ReadObject(ES3Reader)
// 0x00000221 System.Boolean ES3Types.ES3UnityObjectType::WriteUsingDerivedType(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3UnityObjectType_WriteUsingDerivedType_mA4CB44FF43AB89428DFA20E7D7D4B1EC75972D6F ();
// 0x00000222 System.Void ES3Types.ES3Type_Random::.ctor()
extern void ES3Type_Random__ctor_m8D6E16C28FAB0448920C82AB30EEFA6F595B433B ();
// 0x00000223 System.Void ES3Types.ES3Type_Random::WriteObject(System.Object,ES3Writer)
extern void ES3Type_Random_WriteObject_m325E3A123C425845A1FC24043860185E0CCB0131 ();
// 0x00000224 System.Void ES3Types.ES3Type_Random::ReadObject(ES3Reader,System.Object)
// 0x00000225 System.Object ES3Types.ES3Type_Random::ReadObject(ES3Reader)
// 0x00000226 System.Void ES3Types.ES3Type_Random::.cctor()
extern void ES3Type_Random__cctor_m8734D5A5E09A0CCF77F0A49A52942FD86A2478B3 ();
// 0x00000227 System.Void ES3Types.ES3Type_RandomArray::.ctor()
extern void ES3Type_RandomArray__ctor_m557E794D70052DB512565B78214303673B60B2FA ();
// 0x00000228 System.Void ES3Types.ES3Type_DateTime::.ctor()
extern void ES3Type_DateTime__ctor_m9C18CC743D1C085A74D5B04FB981397509CF0D3D ();
// 0x00000229 System.Void ES3Types.ES3Type_DateTime::Write(System.Object,ES3Writer)
extern void ES3Type_DateTime_Write_mECE5348F24FA3AE135CE4C8AD3D711E035F5BF8F ();
// 0x0000022A System.Object ES3Types.ES3Type_DateTime::Read(ES3Reader)
// 0x0000022B System.Void ES3Types.ES3Type_DateTime::.cctor()
extern void ES3Type_DateTime__cctor_m255B31D964DBA04AD4323A6E8F05C3CB3F010E89 ();
// 0x0000022C System.Void ES3Types.ES3Type_DateTimeArray::.ctor()
extern void ES3Type_DateTimeArray__ctor_mFCE6EEC75886B4BB693876B91BD7A6C5C8B80FCD ();
// 0x0000022D System.Void ES3Types.ES3Type_ES3Ref::.ctor()
extern void ES3Type_ES3Ref__ctor_m9D2F04A960C125B5B84C5C0897D93A4DA08C9197 ();
// 0x0000022E System.Void ES3Types.ES3Type_ES3Ref::Write(System.Object,ES3Writer)
extern void ES3Type_ES3Ref_Write_mD4BA048EA7830FA95C9D831C20459EEEC13A1B02 ();
// 0x0000022F System.Object ES3Types.ES3Type_ES3Ref::Read(ES3Reader)
// 0x00000230 System.Void ES3Types.ES3Type_ES3Ref::.cctor()
extern void ES3Type_ES3Ref__cctor_m81A4196E839A7B10FF06B53E79000CCEF25240BC ();
// 0x00000231 System.Void ES3Types.ES3Type_ES3RefArray::.ctor()
extern void ES3Type_ES3RefArray__ctor_mF9ABBE96F63A0A456AE588FDD23E9030E5DBE811 ();
// 0x00000232 System.Void ES3Types.ES3Type_ES3RefDictionary::.ctor()
extern void ES3Type_ES3RefDictionary__ctor_m0770EC845408062B421BDD02125CBD9936FD69D8 ();
// 0x00000233 System.Void ES3Types.ES3Type_UIntPtr::.ctor()
extern void ES3Type_UIntPtr__ctor_mE54EAE6B1E6E8559E78432A003C5850023DE616C ();
// 0x00000234 System.Void ES3Types.ES3Type_UIntPtr::Write(System.Object,ES3Writer)
extern void ES3Type_UIntPtr_Write_m93F37A6D79D65B718CC7B1B5D14A7B82CF2E5AD9 ();
// 0x00000235 System.Object ES3Types.ES3Type_UIntPtr::Read(ES3Reader)
// 0x00000236 System.Void ES3Types.ES3Type_UIntPtr::.cctor()
extern void ES3Type_UIntPtr__cctor_mBFF9E7C566DED024F8FDAE32C84A7FC0C447118B ();
// 0x00000237 System.Void ES3Types.ES3Type_UIntPtrArray::.ctor()
extern void ES3Type_UIntPtrArray__ctor_m0112CEA07A9EE264F9B0D58C4E70D3E38C6F79CA ();
// 0x00000238 System.Void ES3Types.ES3Type_bool::.ctor()
extern void ES3Type_bool__ctor_mF461278C26A423F6239919B7CFDC8D2E1A4F1D91 ();
// 0x00000239 System.Void ES3Types.ES3Type_bool::Write(System.Object,ES3Writer)
extern void ES3Type_bool_Write_mC69D943DA17E4864107F8A4B0AC2D38A2FA7AC41 ();
// 0x0000023A System.Object ES3Types.ES3Type_bool::Read(ES3Reader)
// 0x0000023B System.Void ES3Types.ES3Type_bool::.cctor()
extern void ES3Type_bool__cctor_mE00C5BD4368C5E260FF49362AE8DE455934E7C3B ();
// 0x0000023C System.Void ES3Types.ES3Type_boolArray::.ctor()
extern void ES3Type_boolArray__ctor_m66F7B29B92584C8AF3EE37A076C1CC27E4C21693 ();
// 0x0000023D System.Void ES3Types.ES3Type_byte::.ctor()
extern void ES3Type_byte__ctor_mE7DA116862C73104F76A1F6F9ED1EDF2567E70D9 ();
// 0x0000023E System.Void ES3Types.ES3Type_byte::Write(System.Object,ES3Writer)
extern void ES3Type_byte_Write_mBA0F9763D2DD7AC9FD317A65DD603BCA27C9040D ();
// 0x0000023F System.Object ES3Types.ES3Type_byte::Read(ES3Reader)
// 0x00000240 System.Void ES3Types.ES3Type_byte::.cctor()
extern void ES3Type_byte__cctor_mA0506463547BE9BF6C8CDD1706429381330CEB03 ();
// 0x00000241 System.Void ES3Types.ES3Type_byteArray::.ctor()
extern void ES3Type_byteArray__ctor_m0EE91527BFC43F0935B38EE897DEE8F4F6F6185A ();
// 0x00000242 System.Void ES3Types.ES3Type_byteArray::Write(System.Object,ES3Writer)
extern void ES3Type_byteArray_Write_mB90275FA0F19DFE6708FAA6B996E0D46B1C3D40E ();
// 0x00000243 System.Object ES3Types.ES3Type_byteArray::Read(ES3Reader)
// 0x00000244 System.Void ES3Types.ES3Type_byteArray::.cctor()
extern void ES3Type_byteArray__cctor_m6CC7CA895761C5BF32D6B14C3E1CDA1A48ACA71F ();
// 0x00000245 System.Void ES3Types.ES3Type_char::.ctor()
extern void ES3Type_char__ctor_m08E386C741FAC3D841ED084764AB40631CCBEF78 ();
// 0x00000246 System.Void ES3Types.ES3Type_char::Write(System.Object,ES3Writer)
extern void ES3Type_char_Write_mF0052E73DF43AD2AA13897DCD58C31562EE66C8A ();
// 0x00000247 System.Object ES3Types.ES3Type_char::Read(ES3Reader)
// 0x00000248 System.Void ES3Types.ES3Type_char::.cctor()
extern void ES3Type_char__cctor_mAB5ECAE75825EED720712B555A22DFB00F769696 ();
// 0x00000249 System.Void ES3Types.ES3Type_charArray::.ctor()
extern void ES3Type_charArray__ctor_mF70371A7B32EBEC3A3BEABDB171950791469EE17 ();
// 0x0000024A System.Void ES3Types.ES3Type_decimal::.ctor()
extern void ES3Type_decimal__ctor_m37F8F74775EA330EB8E5F67B4641BBB67A10DFB9 ();
// 0x0000024B System.Void ES3Types.ES3Type_decimal::Write(System.Object,ES3Writer)
extern void ES3Type_decimal_Write_mC806607596D9B9118843678CB5F11FDDEC5DF37E ();
// 0x0000024C System.Object ES3Types.ES3Type_decimal::Read(ES3Reader)
// 0x0000024D System.Void ES3Types.ES3Type_decimal::.cctor()
extern void ES3Type_decimal__cctor_m2780F4E1B896CAB6FEC861EB2161B8AB27A9C933 ();
// 0x0000024E System.Void ES3Types.ES3Type_decimalArray::.ctor()
extern void ES3Type_decimalArray__ctor_m1DEE941BA730E07E015D62BEB346C5278C39D0E9 ();
// 0x0000024F System.Void ES3Types.ES3Type_double::.ctor()
extern void ES3Type_double__ctor_m10C2504EF6D227ABB74FACEEE1B4172B0D728D9D ();
// 0x00000250 System.Void ES3Types.ES3Type_double::Write(System.Object,ES3Writer)
extern void ES3Type_double_Write_mCB6E2B18BFB2E755FD2392C97720DB56C24FFECB ();
// 0x00000251 System.Object ES3Types.ES3Type_double::Read(ES3Reader)
// 0x00000252 System.Void ES3Types.ES3Type_double::.cctor()
extern void ES3Type_double__cctor_mD3377BDA1841855B1930215018B46F458FC18214 ();
// 0x00000253 System.Void ES3Types.ES3Type_doubleArray::.ctor()
extern void ES3Type_doubleArray__ctor_mA23E81CF5C59E31109218C50E8D2BD525A714006 ();
// 0x00000254 System.Void ES3Types.ES3Type_enum::.ctor(System.Type)
extern void ES3Type_enum__ctor_m776DBBA523C193BD0F1171D99E422B3CB2428E12 ();
// 0x00000255 System.Void ES3Types.ES3Type_enum::Write(System.Object,ES3Writer)
extern void ES3Type_enum_Write_m000979BACB065282E624301E41FA5F56A881CF08 ();
// 0x00000256 System.Object ES3Types.ES3Type_enum::Read(ES3Reader)
// 0x00000257 System.Void ES3Types.ES3Type_enum::.cctor()
extern void ES3Type_enum__cctor_mB502B4CD7AC98934DC44CFD790E682AC8E11ACA1 ();
// 0x00000258 System.Void ES3Types.ES3Type_float::.ctor()
extern void ES3Type_float__ctor_m05CC0A8C2988F6E2C5070313E70F0AB6A591E7B9 ();
// 0x00000259 System.Void ES3Types.ES3Type_float::Write(System.Object,ES3Writer)
extern void ES3Type_float_Write_mC6F63490D42B229D1838995E6A502D03B85A764B ();
// 0x0000025A System.Object ES3Types.ES3Type_float::Read(ES3Reader)
// 0x0000025B System.Void ES3Types.ES3Type_float::.cctor()
extern void ES3Type_float__cctor_m19CB0F0D0B9688009BC363FA429B9A2FE2517787 ();
// 0x0000025C System.Void ES3Types.ES3Type_floatArray::.ctor()
extern void ES3Type_floatArray__ctor_mEFFAEF36F1A8CF0A3F68D17A3F298626E660C8E4 ();
// 0x0000025D System.Void ES3Types.ES3Type_int::.ctor()
extern void ES3Type_int__ctor_m377719C859759583124282B916C6983F6500ABB6 ();
// 0x0000025E System.Void ES3Types.ES3Type_int::Write(System.Object,ES3Writer)
extern void ES3Type_int_Write_m0207E425B784352433DF0D21089C7F4E310765F4 ();
// 0x0000025F System.Object ES3Types.ES3Type_int::Read(ES3Reader)
// 0x00000260 System.Void ES3Types.ES3Type_int::.cctor()
extern void ES3Type_int__cctor_m8ABA983C7268C852C46E5328CD67D648DA0B1BD5 ();
// 0x00000261 System.Void ES3Types.ES3Type_intArray::.ctor()
extern void ES3Type_intArray__ctor_mB9CFFE0DAC3BF124C5BC2A86C168762560E5AA18 ();
// 0x00000262 System.Void ES3Types.ES3Type_IntPtr::.ctor()
extern void ES3Type_IntPtr__ctor_m4458133A618E6CCC6A8C4B338AA9EFD63298B866 ();
// 0x00000263 System.Void ES3Types.ES3Type_IntPtr::Write(System.Object,ES3Writer)
extern void ES3Type_IntPtr_Write_m22817348510D94F1B712BACA6AAC28AD536FEDBA ();
// 0x00000264 System.Object ES3Types.ES3Type_IntPtr::Read(ES3Reader)
// 0x00000265 System.Void ES3Types.ES3Type_IntPtr::.cctor()
extern void ES3Type_IntPtr__cctor_m6F72C33C4920B2EBD9470990BB056AFADC1F074E ();
// 0x00000266 System.Void ES3Types.ES3Type_IntPtrArray::.ctor()
extern void ES3Type_IntPtrArray__ctor_m04E25AA0341A06F385BC3712593C5AFCA4E40CF8 ();
// 0x00000267 System.Void ES3Types.ES3Type_long::.ctor()
extern void ES3Type_long__ctor_m9B6CF7B704A92267767F537427E5F8CCC54EE0E9 ();
// 0x00000268 System.Void ES3Types.ES3Type_long::Write(System.Object,ES3Writer)
extern void ES3Type_long_Write_m0E55A24FA1C5BD219F284F04678D1888BF29BED2 ();
// 0x00000269 System.Object ES3Types.ES3Type_long::Read(ES3Reader)
// 0x0000026A System.Void ES3Types.ES3Type_long::.cctor()
extern void ES3Type_long__cctor_mCAE3110ECEAB7AC3D06853C436B2D2A1D32FE1D0 ();
// 0x0000026B System.Void ES3Types.ES3Type_longArray::.ctor()
extern void ES3Type_longArray__ctor_mC89B91FA7080A619CB50C72F26973C5A764AD37A ();
// 0x0000026C System.Void ES3Types.ES3Type_sbyte::.ctor()
extern void ES3Type_sbyte__ctor_m2E67AFCACBC5CBC8FC31F0B00A074C85D37B0DE9 ();
// 0x0000026D System.Void ES3Types.ES3Type_sbyte::Write(System.Object,ES3Writer)
extern void ES3Type_sbyte_Write_m8913966B1610B5AAAAAAFC0BFB3705C645004A88 ();
// 0x0000026E System.Object ES3Types.ES3Type_sbyte::Read(ES3Reader)
// 0x0000026F System.Void ES3Types.ES3Type_sbyte::.cctor()
extern void ES3Type_sbyte__cctor_m6FE88FBB7611D0B70BC7D8D60F43E6E9AB6093AF ();
// 0x00000270 System.Void ES3Types.ES3Type_sbyteArray::.ctor()
extern void ES3Type_sbyteArray__ctor_m4A53FBAEFB6CC43991204FDCB324480E171E80B2 ();
// 0x00000271 System.Void ES3Types.ES3Type_short::.ctor()
extern void ES3Type_short__ctor_mA9F4ADF2ABEA984E89C72BD54452658DD635779A ();
// 0x00000272 System.Void ES3Types.ES3Type_short::Write(System.Object,ES3Writer)
extern void ES3Type_short_Write_mE398125447D6F2FA7661307E5CFD57C4D5E16DEA ();
// 0x00000273 System.Object ES3Types.ES3Type_short::Read(ES3Reader)
// 0x00000274 System.Void ES3Types.ES3Type_short::.cctor()
extern void ES3Type_short__cctor_m7499BCF937CC3D6E3C32DF33720B5E6CD7E6772E ();
// 0x00000275 System.Void ES3Types.ES3Type_shortArray::.ctor()
extern void ES3Type_shortArray__ctor_mC5731A8AA14025E278762E26B68B912C64540F94 ();
// 0x00000276 System.Void ES3Types.ES3Type_string::.ctor()
extern void ES3Type_string__ctor_mBBC50231A331B512505007CE83D0D27FC1DAE929 ();
// 0x00000277 System.Void ES3Types.ES3Type_string::Write(System.Object,ES3Writer)
extern void ES3Type_string_Write_mE335ABDCC8F0FA54C0BA18AE73D82723E7CAE677 ();
// 0x00000278 System.Object ES3Types.ES3Type_string::Read(ES3Reader)
// 0x00000279 System.Void ES3Types.ES3Type_string::.cctor()
extern void ES3Type_string__cctor_m1A150BF757F3A51E058CE55D3AF98067C89FBF72 ();
// 0x0000027A System.Void ES3Types.ES3Type_StringArray::.ctor()
extern void ES3Type_StringArray__ctor_m7D3F2324B8779E075AEE391C7379D01644D7221D ();
// 0x0000027B System.Void ES3Types.ES3Type_uint::.ctor()
extern void ES3Type_uint__ctor_m0F6B7CEE0FB14AD38560DE0680B40C88308B2EFF ();
// 0x0000027C System.Void ES3Types.ES3Type_uint::Write(System.Object,ES3Writer)
extern void ES3Type_uint_Write_mBFB92390CFAB64CE3231FE14FF22EC1F992B9E39 ();
// 0x0000027D System.Object ES3Types.ES3Type_uint::Read(ES3Reader)
// 0x0000027E System.Void ES3Types.ES3Type_uint::.cctor()
extern void ES3Type_uint__cctor_m703A5D9B98A6615B603AD89DEBBEC139CB675206 ();
// 0x0000027F System.Void ES3Types.ES3Type_uintArray::.ctor()
extern void ES3Type_uintArray__ctor_m96FECC77B74E77FFB77005F2FFF80CAB6CA810BA ();
// 0x00000280 System.Void ES3Types.ES3Type_ulong::.ctor()
extern void ES3Type_ulong__ctor_m6169C3C5F8D0E2E9D091C3C542002F8D1489786C ();
// 0x00000281 System.Void ES3Types.ES3Type_ulong::Write(System.Object,ES3Writer)
extern void ES3Type_ulong_Write_m119A54060DC97C56F18B508C2A3CFE2EDE0C0AA9 ();
// 0x00000282 System.Object ES3Types.ES3Type_ulong::Read(ES3Reader)
// 0x00000283 System.Void ES3Types.ES3Type_ulong::.cctor()
extern void ES3Type_ulong__cctor_mAD1604A20A57FF0330F34FB2EFA8BA26FD1216D1 ();
// 0x00000284 System.Void ES3Types.ES3Type_ulongArray::.ctor()
extern void ES3Type_ulongArray__ctor_mC666EB10B7CC82DAEB25A9120E9B0C2F84C564C3 ();
// 0x00000285 System.Void ES3Types.ES3Type_ushort::.ctor()
extern void ES3Type_ushort__ctor_m1791120125ECD1FDEAB6C5B5A7901FA7BFC4573C ();
// 0x00000286 System.Void ES3Types.ES3Type_ushort::Write(System.Object,ES3Writer)
extern void ES3Type_ushort_Write_mBCB5E954DE1E1626B7091A09980BFFCA2E29FEC1 ();
// 0x00000287 System.Object ES3Types.ES3Type_ushort::Read(ES3Reader)
// 0x00000288 System.Void ES3Types.ES3Type_ushort::.cctor()
extern void ES3Type_ushort__cctor_mDB53D43D06AD63FA755E752D2B72F9E1FDA6E048 ();
// 0x00000289 System.Void ES3Types.ES3Type_ushortArray::.ctor()
extern void ES3Type_ushortArray__ctor_mE0AB32EC7E70AE12BDF4F689029F3BC5182D3A76 ();
// 0x0000028A System.Void ES3Types.ES3ReflectedComponentType::.ctor(System.Type)
extern void ES3ReflectedComponentType__ctor_m609E76B515B5A69BBB92B6A48A901D5C2C3EDA5D ();
// 0x0000028B System.Void ES3Types.ES3ReflectedComponentType::WriteComponent(System.Object,ES3Writer)
extern void ES3ReflectedComponentType_WriteComponent_m695DE406FECB8C3DF3B07941FDE58ADC9912A07B ();
// 0x0000028C System.Void ES3Types.ES3ReflectedComponentType::ReadComponent(ES3Reader,System.Object)
// 0x0000028D System.Void ES3Types.ES3ReflectedObjectType::.ctor(System.Type)
extern void ES3ReflectedObjectType__ctor_mF23E33D4DDEA06E403A314EAEED52A25863788BA ();
// 0x0000028E System.Void ES3Types.ES3ReflectedObjectType::WriteObject(System.Object,ES3Writer)
extern void ES3ReflectedObjectType_WriteObject_mD76EAD1F9CD470156CA0FB32F823845A61DC5942 ();
// 0x0000028F System.Object ES3Types.ES3ReflectedObjectType::ReadObject(ES3Reader)
// 0x00000290 System.Void ES3Types.ES3ReflectedObjectType::ReadObject(ES3Reader,System.Object)
// 0x00000291 System.Void ES3Types.ES3ReflectedScriptableObjectType::.ctor(System.Type)
extern void ES3ReflectedScriptableObjectType__ctor_mB2B13CFF3BD3F4CDDAB3A0C3BFEF13C4C7D717DB ();
// 0x00000292 System.Void ES3Types.ES3ReflectedScriptableObjectType::WriteScriptableObject(System.Object,ES3Writer)
extern void ES3ReflectedScriptableObjectType_WriteScriptableObject_m859A80339A1D392EFEBC33668ACE45E0BF3993F5 ();
// 0x00000293 System.Void ES3Types.ES3ReflectedScriptableObjectType::ReadScriptableObject(ES3Reader,System.Object)
// 0x00000294 System.Void ES3Types.ES3ReflectedType::.ctor(System.Type)
extern void ES3ReflectedType__ctor_mE2879DF382BE722B3CD61914B36E902299DFF32C ();
// 0x00000295 System.Void ES3Types.ES3ReflectedType::.ctor(System.Type,System.String[])
extern void ES3ReflectedType__ctor_mC83959EC1462CDECB0C8A23C7BE56A389110A116 ();
// 0x00000296 System.Void ES3Types.ES3ReflectedType::Write(System.Object,ES3Writer)
extern void ES3ReflectedType_Write_m72E9C60F513FAA7F37668F52811A9EE9FC3A5942 ();
// 0x00000297 System.Object ES3Types.ES3ReflectedType::Read(ES3Reader)
// 0x00000298 System.Void ES3Types.ES3ReflectedType::ReadInto(ES3Reader,System.Object)
// 0x00000299 System.Void ES3Types.ES3ReflectedUnityObjectType::.ctor(System.Type)
extern void ES3ReflectedUnityObjectType__ctor_mFCE3647F4101E29263E65FFBC5E7C91974A16C3C ();
// 0x0000029A System.Void ES3Types.ES3ReflectedUnityObjectType::WriteUnityObject(System.Object,ES3Writer)
extern void ES3ReflectedUnityObjectType_WriteUnityObject_m92E3BDAF3A8DC196E878DFAC5821AFA143E6BF12 ();
// 0x0000029B System.Object ES3Types.ES3ReflectedUnityObjectType::ReadUnityObject(ES3Reader)
// 0x0000029C System.Void ES3Types.ES3ReflectedUnityObjectType::ReadUnityObject(ES3Reader,System.Object)
// 0x0000029D System.Void ES3Types.ES3ReflectedValueType::.ctor(System.Type)
extern void ES3ReflectedValueType__ctor_m0D230E419ADCAADB53C3497781B14F90924E798E ();
// 0x0000029E System.Void ES3Types.ES3ReflectedValueType::Write(System.Object,ES3Writer)
extern void ES3ReflectedValueType_Write_m280E91FB066034BF45E93B336DC6FAC5E1AA188C ();
// 0x0000029F System.Object ES3Types.ES3ReflectedValueType::Read(ES3Reader)
// 0x000002A0 System.Void ES3Types.ES3ReflectedValueType::ReadInto(ES3Reader,System.Object)
// 0x000002A1 System.Void ES3Types.ES3Type_BoxCollider::.ctor()
extern void ES3Type_BoxCollider__ctor_m7F49B59EDF60F2E1F57FED5D31AB3BC2EF01DCC5 ();
// 0x000002A2 System.Void ES3Types.ES3Type_BoxCollider::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_BoxCollider_WriteComponent_mF6B9F0CC207AF73159780084A599176C57F96D05 ();
// 0x000002A3 System.Void ES3Types.ES3Type_BoxCollider::ReadComponent(ES3Reader,System.Object)
// 0x000002A4 System.Void ES3Types.ES3Type_BoxCollider::.cctor()
extern void ES3Type_BoxCollider__cctor_m7C917F11AEF9580E3DD9420C3815C29E1F6C2E0B ();
// 0x000002A5 System.Void ES3Types.ES3Type_BoxCollider2D::.ctor()
extern void ES3Type_BoxCollider2D__ctor_mA755D5969681F0E91AB4E82CBA4A1F0D2E5B1941 ();
// 0x000002A6 System.Void ES3Types.ES3Type_BoxCollider2D::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_BoxCollider2D_WriteComponent_m45AED18E2F1D3CDBBA4D007BCCFE1AA3ED3C6034 ();
// 0x000002A7 System.Void ES3Types.ES3Type_BoxCollider2D::ReadComponent(ES3Reader,System.Object)
// 0x000002A8 System.Void ES3Types.ES3Type_BoxCollider2D::.cctor()
extern void ES3Type_BoxCollider2D__cctor_m0528F04523FA4A5719858F2DD8F3CAE4BB602FD7 ();
// 0x000002A9 System.Void ES3Types.ES3Type_Camera::.ctor()
extern void ES3Type_Camera__ctor_mFD8408A3898E97207F171EA5D0363C16703CA741 ();
// 0x000002AA System.Void ES3Types.ES3Type_Camera::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_Camera_WriteComponent_mE6D85C8DF67178B6E94044112834A5784C9252B4 ();
// 0x000002AB System.Void ES3Types.ES3Type_Camera::ReadComponent(ES3Reader,System.Object)
// 0x000002AC System.Void ES3Types.ES3Type_Camera::.cctor()
extern void ES3Type_Camera__cctor_m5BB985DA17A43A6A810963148D7982FFB928728B ();
// 0x000002AD System.Void ES3Types.ES3Type_CapsuleCollider::.ctor()
extern void ES3Type_CapsuleCollider__ctor_mF6C53F1961FF1B8CC92B9C7FFE3DB8E336D381EF ();
// 0x000002AE System.Void ES3Types.ES3Type_CapsuleCollider::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_CapsuleCollider_WriteComponent_m5B76791A6BC0D1BEB7021006F6DEB90A44F2ACFE ();
// 0x000002AF System.Void ES3Types.ES3Type_CapsuleCollider::ReadComponent(ES3Reader,System.Object)
// 0x000002B0 System.Void ES3Types.ES3Type_CapsuleCollider::.cctor()
extern void ES3Type_CapsuleCollider__cctor_m582CFA8CD9086A86DF7756E855203B95B1A7102A ();
// 0x000002B1 System.Void ES3Types.ES3Type_EventSystem::.ctor()
extern void ES3Type_EventSystem__ctor_mA491EED2AF652B3296BC87C041DFFFE7C3D5005D ();
// 0x000002B2 System.Void ES3Types.ES3Type_EventSystem::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_EventSystem_WriteComponent_mDA81499D58230FA6A618FE1B7D528D4DDBC7B173 ();
// 0x000002B3 System.Void ES3Types.ES3Type_EventSystem::ReadComponent(ES3Reader,System.Object)
// 0x000002B4 System.Void ES3Types.ES3Type_EventSystem::.cctor()
extern void ES3Type_EventSystem__cctor_m1E6CA8F5DA897E0996AD3FD84D2FED0B93A22BC4 ();
// 0x000002B5 System.Void ES3Types.ES3Type_Image::.ctor()
extern void ES3Type_Image__ctor_m7942C8BAA691F3F1D4D8F1554FBA659A4A830DE9 ();
// 0x000002B6 System.Void ES3Types.ES3Type_Image::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_Image_WriteComponent_mD3959FE5C8A0D8DABB232B4CB11E9C2494EA8402 ();
// 0x000002B7 System.Void ES3Types.ES3Type_Image::ReadComponent(ES3Reader,System.Object)
// 0x000002B8 System.Void ES3Types.ES3Type_Image::.cctor()
extern void ES3Type_Image__cctor_mAA503144D6BFED66EE29FC72D434F16E27A7CD35 ();
// 0x000002B9 System.Void ES3Types.ES3Type_ImageArray::.ctor()
extern void ES3Type_ImageArray__ctor_m4C35367F0AED5C97A0CD47E596B72D6B76B64470 ();
// 0x000002BA System.Void ES3Types.ES3Type_MeshCollider::.ctor()
extern void ES3Type_MeshCollider__ctor_m6CF77304F6FF527271125BFDB70A0B6DDC770B64 ();
// 0x000002BB System.Void ES3Types.ES3Type_MeshCollider::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_MeshCollider_WriteComponent_m1CF7624475F71BCA68BA41F9CF81529DFCB39960 ();
// 0x000002BC System.Void ES3Types.ES3Type_MeshCollider::ReadComponent(ES3Reader,System.Object)
// 0x000002BD System.Void ES3Types.ES3Type_MeshCollider::.cctor()
extern void ES3Type_MeshCollider__cctor_m9C6AEE7EC4671F2774083809FBEA090EE9F89B8C ();
// 0x000002BE System.Void ES3Types.ES3Type_MeshColliderArray::.ctor()
extern void ES3Type_MeshColliderArray__ctor_m53B85EA5EFCB294BEC8C6A3D8A08832C283D49D5 ();
// 0x000002BF System.Void ES3Types.ES3Type_MeshFilter::.ctor()
extern void ES3Type_MeshFilter__ctor_mBF43F8E4BEF79EEE24ADB66B62E3058FC1C36AB2 ();
// 0x000002C0 System.Void ES3Types.ES3Type_MeshFilter::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_MeshFilter_WriteComponent_mA0846588E181E7C6B7E49C9BB48BFFA15B23208C ();
// 0x000002C1 System.Void ES3Types.ES3Type_MeshFilter::ReadComponent(ES3Reader,System.Object)
// 0x000002C2 System.Void ES3Types.ES3Type_MeshFilter::.cctor()
extern void ES3Type_MeshFilter__cctor_m68FBCF9BA1304FF3E56BFC82EC786F6617E46666 ();
// 0x000002C3 System.Void ES3Types.ES3Type_MeshFilterArray::.ctor()
extern void ES3Type_MeshFilterArray__ctor_m9D35E8990F1EFDF535B76C6ABE5DAA729E8B7CD8 ();
// 0x000002C4 System.Void ES3Types.ES3Type_MeshRenderer::.ctor()
extern void ES3Type_MeshRenderer__ctor_m51A57DB11F0D3FE472AB2FC9CE1E47AC254B043A ();
// 0x000002C5 System.Void ES3Types.ES3Type_MeshRenderer::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_MeshRenderer_WriteComponent_mC7A2CF00C1CECE6FCA608826D8D8A5DA64719047 ();
// 0x000002C6 System.Void ES3Types.ES3Type_MeshRenderer::ReadComponent(ES3Reader,System.Object)
// 0x000002C7 System.Void ES3Types.ES3Type_MeshRenderer::.cctor()
extern void ES3Type_MeshRenderer__cctor_m6313D20626DD63804E5399E3A2E03A57AB14321E ();
// 0x000002C8 System.Void ES3Types.ES3Type_MeshRendererArray::.ctor()
extern void ES3Type_MeshRendererArray__ctor_m61A76425226AE070A1758EB6AB03C282864A28AB ();
// 0x000002C9 System.Void ES3Types.ES3Type_ParticleSystem::.ctor()
extern void ES3Type_ParticleSystem__ctor_m238935593A40246A958912FCA794E966DE302718 ();
// 0x000002CA System.Void ES3Types.ES3Type_ParticleSystem::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_ParticleSystem_WriteComponent_m09E0CA7580B39CB8CEB78A7077C89D1DF3FE23A9 ();
// 0x000002CB System.Void ES3Types.ES3Type_ParticleSystem::ReadComponent(ES3Reader,System.Object)
// 0x000002CC System.Void ES3Types.ES3Type_ParticleSystem::.cctor()
extern void ES3Type_ParticleSystem__cctor_mA23926920DCA056DB5FBEEAA1782FA28A5BF2457 ();
// 0x000002CD System.Void ES3Types.ES3Type_PolygonCollider2D::.ctor()
extern void ES3Type_PolygonCollider2D__ctor_mBF2D4578296450B1AA14804DD6E86CE171F2007F ();
// 0x000002CE System.Void ES3Types.ES3Type_PolygonCollider2D::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_PolygonCollider2D_WriteComponent_mC6FF9AB99FA36F5C41351E984774E71F9C50074E ();
// 0x000002CF System.Void ES3Types.ES3Type_PolygonCollider2D::ReadComponent(ES3Reader,System.Object)
// 0x000002D0 System.Void ES3Types.ES3Type_PolygonCollider2D::.cctor()
extern void ES3Type_PolygonCollider2D__cctor_m71F762EC43F6FADFEAE9C84B66034033E6C5A344 ();
// 0x000002D1 System.Void ES3Types.ES3Type_PolygonCollider2DArray::.ctor()
extern void ES3Type_PolygonCollider2DArray__ctor_m4FCD8FD3D09C798E5B0E2F3913860F596548A494 ();
// 0x000002D2 System.Void ES3Types.ES3Type_RawImage::.ctor()
extern void ES3Type_RawImage__ctor_m1825BFAF73656982E97D030356A8597E30FACC1D ();
// 0x000002D3 System.Void ES3Types.ES3Type_RawImage::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_RawImage_WriteComponent_m49298958D623294BC3CE77632C529201E6C015A2 ();
// 0x000002D4 System.Void ES3Types.ES3Type_RawImage::ReadComponent(ES3Reader,System.Object)
// 0x000002D5 System.Void ES3Types.ES3Type_RawImage::.cctor()
extern void ES3Type_RawImage__cctor_m58A1787681A945783646D2B6AFA9B4274EA94D8C ();
// 0x000002D6 System.Void ES3Types.ES3Type_RawImageArray::.ctor()
extern void ES3Type_RawImageArray__ctor_mE7A7C55B138E1A692702F2333A8221030994887E ();
// 0x000002D7 System.Void ES3Types.ES3Type_SphereCollider::.ctor()
extern void ES3Type_SphereCollider__ctor_m6BDF2C71A82E8BF03B2784D793A870CAC6E02154 ();
// 0x000002D8 System.Void ES3Types.ES3Type_SphereCollider::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_SphereCollider_WriteComponent_mCF18EA8014E9C13A0A5509DA1D2B643FD6397F92 ();
// 0x000002D9 System.Void ES3Types.ES3Type_SphereCollider::ReadComponent(ES3Reader,System.Object)
// 0x000002DA System.Void ES3Types.ES3Type_SphereCollider::.cctor()
extern void ES3Type_SphereCollider__cctor_mD1060A8255549B8047F1FE9E5604627EBA795981 ();
// 0x000002DB System.Void ES3Types.ES3Type_Text::.ctor()
extern void ES3Type_Text__ctor_m91C0B12693722486BAC00A55907726AC133CEC44 ();
// 0x000002DC System.Void ES3Types.ES3Type_Text::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_Text_WriteComponent_m88F8B8D3897409DC8BD5E0F1FB16C6C1C9ED83D6 ();
// 0x000002DD System.Void ES3Types.ES3Type_Text::ReadComponent(ES3Reader,System.Object)
// 0x000002DE System.Void ES3Types.ES3Type_Text::.cctor()
extern void ES3Type_Text__cctor_m5CADB639D5CFF540729B9E9B60CF42F4C00E05EF ();
// 0x000002DF System.Void ES3Types.ES3Type_Transform::.ctor()
extern void ES3Type_Transform__ctor_m262A853CCEA7F0EA10B29A15684081B90F888B6E ();
// 0x000002E0 System.Void ES3Types.ES3Type_Transform::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_Transform_WriteComponent_mC672B2DAB50A5C7E3D54B7162FE06870005D6B5E ();
// 0x000002E1 System.Void ES3Types.ES3Type_Transform::ReadComponent(ES3Reader,System.Object)
// 0x000002E2 System.Void ES3Types.ES3Type_Transform::.cctor()
extern void ES3Type_Transform__cctor_mFDECB3EA95E1196431BDF779A2E0ACF8CEAAAA89 ();
// 0x000002E3 System.Void ES3Types.ES3Type_AnimationCurve::.ctor()
extern void ES3Type_AnimationCurve__ctor_m0BE1398D084B259EE6E2D41CF6CD41ED8DE42BE0 ();
// 0x000002E4 System.Void ES3Types.ES3Type_AnimationCurve::Write(System.Object,ES3Writer)
extern void ES3Type_AnimationCurve_Write_m2E11AC38A7C0C15129CAC4CBA3BD444B315BFF61 ();
// 0x000002E5 System.Object ES3Types.ES3Type_AnimationCurve::Read(ES3Reader)
// 0x000002E6 System.Void ES3Types.ES3Type_AnimationCurve::ReadInto(ES3Reader,System.Object)
// 0x000002E7 System.Void ES3Types.ES3Type_AnimationCurve::.cctor()
extern void ES3Type_AnimationCurve__cctor_m3740ED1A46FB71331D124F6016DD84D8594847E6 ();
// 0x000002E8 System.Void ES3Types.ES3Type_AudioClip::.ctor()
extern void ES3Type_AudioClip__ctor_m0BFF7D9AC3FA21AA7A443B02D61B5726DC4B07B5 ();
// 0x000002E9 System.Void ES3Types.ES3Type_AudioClip::WriteUnityObject(System.Object,ES3Writer)
extern void ES3Type_AudioClip_WriteUnityObject_mCB36C2145BFE332F0C76D5F51CDA63E0A403EEB4 ();
// 0x000002EA System.Void ES3Types.ES3Type_AudioClip::ReadUnityObject(ES3Reader,System.Object)
// 0x000002EB System.Object ES3Types.ES3Type_AudioClip::ReadUnityObject(ES3Reader)
// 0x000002EC System.Void ES3Types.ES3Type_AudioClip::.cctor()
extern void ES3Type_AudioClip__cctor_mA49F630ABDB12E3633906CED1CFCEBDF2861062B ();
// 0x000002ED System.Void ES3Types.ES3Type_AudioClipArray::.ctor()
extern void ES3Type_AudioClipArray__ctor_m4E3386E3826F128D0928BECCB80FC7B2F8799026 ();
// 0x000002EE System.Void ES3Types.ES3Type_BoneWeight::.ctor()
extern void ES3Type_BoneWeight__ctor_m6FC8C9557E73798EBFF47C21FDC8AC53E4D69BFD ();
// 0x000002EF System.Void ES3Types.ES3Type_BoneWeight::Write(System.Object,ES3Writer)
extern void ES3Type_BoneWeight_Write_m239F73EAE61ADB87C284FECAB413976C9F09D8BA ();
// 0x000002F0 System.Object ES3Types.ES3Type_BoneWeight::Read(ES3Reader)
// 0x000002F1 System.Void ES3Types.ES3Type_BoneWeight::.cctor()
extern void ES3Type_BoneWeight__cctor_mD53B23C18708B40647A3218C9D2918768F8727BC ();
// 0x000002F2 System.Void ES3Types.ES3Type_BoneWeightArray::.ctor()
extern void ES3Type_BoneWeightArray__ctor_m3D1240869A5A010B4C5EF29D5C3CA578A90FCC33 ();
// 0x000002F3 System.Void ES3Types.ES3Type_Bounds::.ctor()
extern void ES3Type_Bounds__ctor_m467FAE7E30D9509FD6005E6F3DFE87B89218C367 ();
// 0x000002F4 System.Void ES3Types.ES3Type_Bounds::Write(System.Object,ES3Writer)
extern void ES3Type_Bounds_Write_m644EB66E7EBA9C77134D5E6BF64F86285B9E6D37 ();
// 0x000002F5 System.Object ES3Types.ES3Type_Bounds::Read(ES3Reader)
// 0x000002F6 System.Void ES3Types.ES3Type_Bounds::.cctor()
extern void ES3Type_Bounds__cctor_mAD02B8AA057D2820A977F7F19369B21636D5FC64 ();
// 0x000002F7 System.Void ES3Types.ES3Type_BoundsArray::.ctor()
extern void ES3Type_BoundsArray__ctor_m41A7CB22D3A435C1B7FD805531CAA5944B4892A8 ();
// 0x000002F8 System.Void ES3Types.ES3Type_CollisionModule::.ctor()
extern void ES3Type_CollisionModule__ctor_mC98E478E95C6DBFAA754EA30A9D0D3D61F7F31D8 ();
// 0x000002F9 System.Void ES3Types.ES3Type_CollisionModule::Write(System.Object,ES3Writer)
extern void ES3Type_CollisionModule_Write_m8F77E870AA324AFBE500D786D46EBB5D8B655CD8 ();
// 0x000002FA System.Object ES3Types.ES3Type_CollisionModule::Read(ES3Reader)
// 0x000002FB System.Void ES3Types.ES3Type_CollisionModule::ReadInto(ES3Reader,System.Object)
// 0x000002FC System.Void ES3Types.ES3Type_CollisionModule::.cctor()
extern void ES3Type_CollisionModule__cctor_m187E9E43EE4C2E128F0EE6656A8C920665C338EE ();
// 0x000002FD System.Void ES3Types.ES3Type_Color::.ctor()
extern void ES3Type_Color__ctor_m7B3FC8676885FA18C577EB0F40973087DA101F95 ();
// 0x000002FE System.Void ES3Types.ES3Type_Color::Write(System.Object,ES3Writer)
extern void ES3Type_Color_Write_m1A511FD47F4548ABA13042CEF150FFB5712F5144 ();
// 0x000002FF System.Object ES3Types.ES3Type_Color::Read(ES3Reader)
// 0x00000300 System.Void ES3Types.ES3Type_Color::.cctor()
extern void ES3Type_Color__cctor_m9A958DA9E6DA45FD53BCEE39C0A3E8F4C4A312D4 ();
// 0x00000301 System.Void ES3Types.ES3Type_ColorArray::.ctor()
extern void ES3Type_ColorArray__ctor_mA03FE56FF7FD4472F43E887F303605E79B9E62EF ();
// 0x00000302 System.Void ES3Types.ES3Type_Color32::.ctor()
extern void ES3Type_Color32__ctor_m7E716D0E29DED5986CB93CED706AF3F524CD5DFB ();
// 0x00000303 System.Void ES3Types.ES3Type_Color32::Write(System.Object,ES3Writer)
extern void ES3Type_Color32_Write_mE941726E27729486FE248181AE8217F2982DBD60 ();
// 0x00000304 System.Object ES3Types.ES3Type_Color32::Read(ES3Reader)
// 0x00000305 System.Boolean ES3Types.ES3Type_Color32::Equals(UnityEngine.Color32,UnityEngine.Color32)
extern void ES3Type_Color32_Equals_m591DDDC7BA73FD82E4E720A5084791DED5D3C762 ();
// 0x00000306 System.Void ES3Types.ES3Type_Color32::.cctor()
extern void ES3Type_Color32__cctor_m483E3EA851B0096504EE6628535D105D4C228098 ();
// 0x00000307 System.Void ES3Types.ES3Type_Color32Array::.ctor()
extern void ES3Type_Color32Array__ctor_m821F3F5C1CFD5BC772E91ED9EBCBF41CB32AEA29 ();
// 0x00000308 System.Void ES3Types.ES3Type_ColorBySpeedModule::.ctor()
extern void ES3Type_ColorBySpeedModule__ctor_mE9A145C1B648716D367339A560E46E60ED49EC1A ();
// 0x00000309 System.Void ES3Types.ES3Type_ColorBySpeedModule::Write(System.Object,ES3Writer)
extern void ES3Type_ColorBySpeedModule_Write_m3DCD111A43B65F66C64EAD0F27903915F717DC56 ();
// 0x0000030A System.Object ES3Types.ES3Type_ColorBySpeedModule::Read(ES3Reader)
// 0x0000030B System.Void ES3Types.ES3Type_ColorBySpeedModule::ReadInto(ES3Reader,System.Object)
// 0x0000030C System.Void ES3Types.ES3Type_ColorBySpeedModule::.cctor()
extern void ES3Type_ColorBySpeedModule__cctor_m419B161CE477A47631DBCF5E0970D3B1D7D72B63 ();
// 0x0000030D System.Void ES3Types.ES3Type_ColorOverLifetimeModule::.ctor()
extern void ES3Type_ColorOverLifetimeModule__ctor_m0ADA954F52F1013D7CDA42ADE817A82A1B25E11D ();
// 0x0000030E System.Void ES3Types.ES3Type_ColorOverLifetimeModule::Write(System.Object,ES3Writer)
extern void ES3Type_ColorOverLifetimeModule_Write_m312B206CC2BB0B374CE0F48CF53EDCA635421AA6 ();
// 0x0000030F System.Object ES3Types.ES3Type_ColorOverLifetimeModule::Read(ES3Reader)
// 0x00000310 System.Void ES3Types.ES3Type_ColorOverLifetimeModule::ReadInto(ES3Reader,System.Object)
// 0x00000311 System.Void ES3Types.ES3Type_ColorOverLifetimeModule::.cctor()
extern void ES3Type_ColorOverLifetimeModule__cctor_mA0E9D70CB68675523255321F67F8C5B341835D58 ();
// 0x00000312 System.Void ES3Types.ES3Type_EmissionModule::.ctor()
extern void ES3Type_EmissionModule__ctor_m1049A5CB26F86A90109D202A0C3DD338129C6CF0 ();
// 0x00000313 System.Void ES3Types.ES3Type_EmissionModule::Write(System.Object,ES3Writer)
extern void ES3Type_EmissionModule_Write_mCDBEDF140170B9888332E50C925E5C2A2AD9FA08 ();
// 0x00000314 System.Object ES3Types.ES3Type_EmissionModule::Read(ES3Reader)
// 0x00000315 System.Void ES3Types.ES3Type_EmissionModule::ReadInto(ES3Reader,System.Object)
// 0x00000316 System.Void ES3Types.ES3Type_EmissionModule::.cctor()
extern void ES3Type_EmissionModule__cctor_mF63ADEA117495EF75E2939E6F8FD96F27D08A3B2 ();
// 0x00000317 System.Void ES3Types.ES3Type_ExternalForcesModule::.ctor()
extern void ES3Type_ExternalForcesModule__ctor_mD2A99AC305F61ECD698A1F606B1E79EFC9194A8C ();
// 0x00000318 System.Void ES3Types.ES3Type_ExternalForcesModule::Write(System.Object,ES3Writer)
extern void ES3Type_ExternalForcesModule_Write_mD28D7FC9AB8663878A032F45827C79AEADEAA5E1 ();
// 0x00000319 System.Object ES3Types.ES3Type_ExternalForcesModule::Read(ES3Reader)
// 0x0000031A System.Void ES3Types.ES3Type_ExternalForcesModule::ReadInto(ES3Reader,System.Object)
// 0x0000031B System.Void ES3Types.ES3Type_ExternalForcesModule::.cctor()
extern void ES3Type_ExternalForcesModule__cctor_mA82F08C97C3C6C3B80710A685C269576CD8C5EC0 ();
// 0x0000031C System.Void ES3Types.ES3Type_Flare::.ctor()
extern void ES3Type_Flare__ctor_mE60CA45525396C0D4E7B8A83B00C8064F97BEC7E ();
// 0x0000031D System.Void ES3Types.ES3Type_Flare::Write(System.Object,ES3Writer)
extern void ES3Type_Flare_Write_mFEE891F63625FFE58C1AF7638283B196710B47A8 ();
// 0x0000031E System.Object ES3Types.ES3Type_Flare::Read(ES3Reader)
// 0x0000031F System.Void ES3Types.ES3Type_Flare::ReadInto(ES3Reader,System.Object)
// 0x00000320 System.Void ES3Types.ES3Type_Flare::.cctor()
extern void ES3Type_Flare__cctor_mC2D99E91546BD589DE853922E029BC2C03F58648 ();
// 0x00000321 System.Void ES3Types.ES3Type_FlareArray::.ctor()
extern void ES3Type_FlareArray__ctor_m4FC90BA596A1315D036415CC9C5EDCE453B77E77 ();
// 0x00000322 System.Void ES3Types.ES3Type_Font::.ctor()
extern void ES3Type_Font__ctor_mA495381BA1AF96E050898C617AF085A9A2E6AC76 ();
// 0x00000323 System.Void ES3Types.ES3Type_Font::WriteUnityObject(System.Object,ES3Writer)
extern void ES3Type_Font_WriteUnityObject_m93678DFDF70FE41130F4DCC4D96697A997FCC38E ();
// 0x00000324 System.Void ES3Types.ES3Type_Font::ReadUnityObject(ES3Reader,System.Object)
// 0x00000325 System.Object ES3Types.ES3Type_Font::ReadUnityObject(ES3Reader)
// 0x00000326 System.Void ES3Types.ES3Type_Font::.cctor()
extern void ES3Type_Font__cctor_mA7A5DF91D6B44050C5CD12186B18EDE34D09A290 ();
// 0x00000327 System.Void ES3Types.ES3Type_FontArray::.ctor()
extern void ES3Type_FontArray__ctor_mEAF57272FFE55789E21FC656991A7CFAED06B9B9 ();
// 0x00000328 System.Void ES3Types.ES3Type_ForceOverLifetimeModule::.ctor()
extern void ES3Type_ForceOverLifetimeModule__ctor_m95BDC6C16741B0A107E3FC8DC5B5234D608E94A6 ();
// 0x00000329 System.Void ES3Types.ES3Type_ForceOverLifetimeModule::Write(System.Object,ES3Writer)
extern void ES3Type_ForceOverLifetimeModule_Write_mF16C5BC052DDCF7A699853BAEC0C976DD36DFC55 ();
// 0x0000032A System.Object ES3Types.ES3Type_ForceOverLifetimeModule::Read(ES3Reader)
// 0x0000032B System.Void ES3Types.ES3Type_ForceOverLifetimeModule::ReadInto(ES3Reader,System.Object)
// 0x0000032C System.Void ES3Types.ES3Type_ForceOverLifetimeModule::.cctor()
extern void ES3Type_ForceOverLifetimeModule__cctor_mB5B4DE504A3F5E545FFCB0EE951C07CA386C78A4 ();
// 0x0000032D System.Void ES3Types.ES3Type_GameObject::.ctor()
extern void ES3Type_GameObject__ctor_mAA52902D8800085EC2A6120FE21A1388481463DB ();
// 0x0000032E System.Void ES3Types.ES3Type_GameObject::WriteObject(System.Object,ES3Writer,ES3_ReferenceMode)
extern void ES3Type_GameObject_WriteObject_m31528FE033BE49814F486BF266E501776C0AB8F0 ();
// 0x0000032F System.Object ES3Types.ES3Type_GameObject::ReadObject(ES3Reader)
// 0x00000330 System.Void ES3Types.ES3Type_GameObject::ReadObject(ES3Reader,System.Object)
// 0x00000331 UnityEngine.GameObject ES3Types.ES3Type_GameObject::CreateNewGameObject(ES3Internal.ES3ReferenceMgrBase,System.Int64)
extern void ES3Type_GameObject_CreateNewGameObject_m5C123C7F04129E78FECFE61ECA28795FA4927540 ();
// 0x00000332 System.Collections.Generic.List`1<UnityEngine.GameObject> ES3Types.ES3Type_GameObject::GetChildren(UnityEngine.GameObject)
extern void ES3Type_GameObject_GetChildren_mF3227937B990C09C99DB8C035B3BDB952C277AD7 ();
// 0x00000333 System.Void ES3Types.ES3Type_GameObject::WriteUnityObject(System.Object,ES3Writer)
extern void ES3Type_GameObject_WriteUnityObject_mB1F056669DE8C316996C28D50E68E25B5068A3E0 ();
// 0x00000334 System.Void ES3Types.ES3Type_GameObject::ReadUnityObject(ES3Reader,System.Object)
// 0x00000335 System.Object ES3Types.ES3Type_GameObject::ReadUnityObject(ES3Reader)
// 0x00000336 System.Void ES3Types.ES3Type_GameObject::.cctor()
extern void ES3Type_GameObject__cctor_m4EAC0FCE242595F4ED8EEA945F4BA14A667360C3 ();
// 0x00000337 System.Void ES3Types.ES3Type_GameObjectArray::.ctor()
extern void ES3Type_GameObjectArray__ctor_m64F622D2DB65BABFA5F85E041FA764B099C07BCF ();
// 0x00000338 System.Void ES3Types.ES3Type_Gradient::.ctor()
extern void ES3Type_Gradient__ctor_m110AB9C9D615F9257F627E4866622CE45538A0A5 ();
// 0x00000339 System.Void ES3Types.ES3Type_Gradient::Write(System.Object,ES3Writer)
extern void ES3Type_Gradient_Write_m5B9859FED06F692F16AEA3576ADC4EE0B52D3ED9 ();
// 0x0000033A System.Object ES3Types.ES3Type_Gradient::Read(ES3Reader)
// 0x0000033B System.Void ES3Types.ES3Type_Gradient::ReadInto(ES3Reader,System.Object)
// 0x0000033C System.Void ES3Types.ES3Type_Gradient::.cctor()
extern void ES3Type_Gradient__cctor_mD97FA3F3940520E8687026F1F3CB1B4DF15FB757 ();
// 0x0000033D System.Void ES3Types.ES3Type_GradientAlphaKey::.ctor()
extern void ES3Type_GradientAlphaKey__ctor_m67A18247E478928C7500DF96D1FB1C11A93D3487 ();
// 0x0000033E System.Void ES3Types.ES3Type_GradientAlphaKey::Write(System.Object,ES3Writer)
extern void ES3Type_GradientAlphaKey_Write_m119804060007BAA96FE460A2B046B9E0C7EF91B6 ();
// 0x0000033F System.Object ES3Types.ES3Type_GradientAlphaKey::Read(ES3Reader)
// 0x00000340 System.Void ES3Types.ES3Type_GradientAlphaKey::.cctor()
extern void ES3Type_GradientAlphaKey__cctor_m86043A4A0F2F11A191B04B50A7D385F81E737F62 ();
// 0x00000341 System.Void ES3Types.ES3Type_GradientAlphaKeyArray::.ctor()
extern void ES3Type_GradientAlphaKeyArray__ctor_m08FBE2F639C7F368D25B0AC895BC213ABE688285 ();
// 0x00000342 System.Void ES3Types.ES3Type_GradientColorKey::.ctor()
extern void ES3Type_GradientColorKey__ctor_mCBE1AC34FC94FB80248B504C8FE41E55F02440DF ();
// 0x00000343 System.Void ES3Types.ES3Type_GradientColorKey::Write(System.Object,ES3Writer)
extern void ES3Type_GradientColorKey_Write_m3675CFEC69FFFB76F1CA49A764E96A30614EFD2E ();
// 0x00000344 System.Object ES3Types.ES3Type_GradientColorKey::Read(ES3Reader)
// 0x00000345 System.Void ES3Types.ES3Type_GradientColorKey::.cctor()
extern void ES3Type_GradientColorKey__cctor_m5F199C183F0077B6DFC7C883743458AD2F997AA7 ();
// 0x00000346 System.Void ES3Types.ES3Type_GradientColorKeyArray::.ctor()
extern void ES3Type_GradientColorKeyArray__ctor_m85A773B81720A4BA163AA595C1A655C827575B5B ();
// 0x00000347 System.Void ES3Types.ES3Type_InheritVelocityModule::.ctor()
extern void ES3Type_InheritVelocityModule__ctor_mFC1CA15E7FCC750BFA88FCCC95605E08EF0E6D48 ();
// 0x00000348 System.Void ES3Types.ES3Type_InheritVelocityModule::Write(System.Object,ES3Writer)
extern void ES3Type_InheritVelocityModule_Write_mCEE2906C60954D3420C3F659811312192D67DF91 ();
// 0x00000349 System.Object ES3Types.ES3Type_InheritVelocityModule::Read(ES3Reader)
// 0x0000034A System.Void ES3Types.ES3Type_InheritVelocityModule::ReadInto(ES3Reader,System.Object)
// 0x0000034B System.Void ES3Types.ES3Type_InheritVelocityModule::.cctor()
extern void ES3Type_InheritVelocityModule__cctor_mB9ED7F909E668D428413D23579B46F37E43A9EFB ();
// 0x0000034C System.Void ES3Types.ES3Type_Keyframe::.ctor()
extern void ES3Type_Keyframe__ctor_m3BE757B0448859C92D7F55ABEA33C264AD130798 ();
// 0x0000034D System.Void ES3Types.ES3Type_Keyframe::Write(System.Object,ES3Writer)
extern void ES3Type_Keyframe_Write_mC363B74D02BBBCAD9A2B04E1EAB6CE36DD604702 ();
// 0x0000034E System.Object ES3Types.ES3Type_Keyframe::Read(ES3Reader)
// 0x0000034F System.Void ES3Types.ES3Type_Keyframe::.cctor()
extern void ES3Type_Keyframe__cctor_mFD2212176E61D804E6D057C91095CCDCDC63E745 ();
// 0x00000350 System.Void ES3Types.ES3Type_KeyframeArray::.ctor()
extern void ES3Type_KeyframeArray__ctor_mA24398AFBE71AA7F80DF3187A082A9E1577B4BED ();
// 0x00000351 System.Void ES3Types.ES3Type_LayerMask::.ctor()
extern void ES3Type_LayerMask__ctor_m3BC49A2C5EE78F0B79ADFD2ED9E2DAA43240D708 ();
// 0x00000352 System.Void ES3Types.ES3Type_LayerMask::Write(System.Object,ES3Writer)
extern void ES3Type_LayerMask_Write_m0F67B59C69244C34C3C7A98904D6CDE294C4BFBD ();
// 0x00000353 System.Object ES3Types.ES3Type_LayerMask::Read(ES3Reader)
// 0x00000354 System.Void ES3Types.ES3Type_LayerMask::.cctor()
extern void ES3Type_LayerMask__cctor_mCDAFCD1C8C3CEB28896A0E363F532746A85ACF59 ();
// 0x00000355 System.Void ES3Types.ES3Type_Light::.ctor()
extern void ES3Type_Light__ctor_m3DD2E7B81E8BE81E282C095DF11DA7FF1B722AB8 ();
// 0x00000356 System.Void ES3Types.ES3Type_Light::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_Light_WriteComponent_m38DBB3D874F5F7E0E4DE1DD875184CAFDF9D2268 ();
// 0x00000357 System.Void ES3Types.ES3Type_Light::ReadComponent(ES3Reader,System.Object)
// 0x00000358 System.Void ES3Types.ES3Type_Light::.cctor()
extern void ES3Type_Light__cctor_mA3F974EA0F7E0FF6F2C8FB73AC56C5EA5BCC1298 ();
// 0x00000359 System.Void ES3Types.ES3Type_LightsModule::.ctor()
extern void ES3Type_LightsModule__ctor_mE4B0C8DFFF9D4D554A24673181E622B81A0C5C8E ();
// 0x0000035A System.Void ES3Types.ES3Type_LightsModule::Write(System.Object,ES3Writer)
extern void ES3Type_LightsModule_Write_mB591D4C1E27AEEB51D87D3F43D1068EC74750924 ();
// 0x0000035B System.Object ES3Types.ES3Type_LightsModule::Read(ES3Reader)
// 0x0000035C System.Void ES3Types.ES3Type_LightsModule::ReadInto(ES3Reader,System.Object)
// 0x0000035D System.Void ES3Types.ES3Type_LightsModule::.cctor()
extern void ES3Type_LightsModule__cctor_m2A7BCCE48370B5D4B25192B8EBD4C2920DF6C6B4 ();
// 0x0000035E System.Void ES3Types.ES3Type_LimitVelocityOverLifetimeModule::.ctor()
extern void ES3Type_LimitVelocityOverLifetimeModule__ctor_m0D7438AAFCF943A66A0130E29DA683FA5D9C8CD7 ();
// 0x0000035F System.Void ES3Types.ES3Type_LimitVelocityOverLifetimeModule::Write(System.Object,ES3Writer)
extern void ES3Type_LimitVelocityOverLifetimeModule_Write_m53A1CE287F6CA4AB3A5A006A44BA2AE5AEC314B9 ();
// 0x00000360 System.Object ES3Types.ES3Type_LimitVelocityOverLifetimeModule::Read(ES3Reader)
// 0x00000361 System.Void ES3Types.ES3Type_LimitVelocityOverLifetimeModule::ReadInto(ES3Reader,System.Object)
// 0x00000362 System.Void ES3Types.ES3Type_LimitVelocityOverLifetimeModule::.cctor()
extern void ES3Type_LimitVelocityOverLifetimeModule__cctor_mBAEE3265788AC0F0E82F0A5346D9BCA88C368FF0 ();
// 0x00000363 System.Void ES3Types.ES3Type_MainModule::.ctor()
extern void ES3Type_MainModule__ctor_mBDB335067E4046173B0DFA634FC2BF7B5ECC7971 ();
// 0x00000364 System.Void ES3Types.ES3Type_MainModule::Write(System.Object,ES3Writer)
extern void ES3Type_MainModule_Write_m1C9D9BEBD977536A37FA793BF43D6680C6B04779 ();
// 0x00000365 System.Object ES3Types.ES3Type_MainModule::Read(ES3Reader)
// 0x00000366 System.Void ES3Types.ES3Type_MainModule::ReadInto(ES3Reader,System.Object)
// 0x00000367 System.Void ES3Types.ES3Type_MainModule::.cctor()
extern void ES3Type_MainModule__cctor_mD775812049E7BFE51A7C679751B163BB2891CEA5 ();
// 0x00000368 System.Void ES3Types.ES3Type_Material::.ctor()
extern void ES3Type_Material__ctor_m78AEBBD7DBA1E10F01370EBB0F13DEAA543A78CE ();
// 0x00000369 System.Void ES3Types.ES3Type_Material::WriteUnityObject(System.Object,ES3Writer)
extern void ES3Type_Material_WriteUnityObject_m84329BCE685179E49BC8950B799308285DEC89C9 ();
// 0x0000036A System.Object ES3Types.ES3Type_Material::ReadUnityObject(ES3Reader)
// 0x0000036B System.Void ES3Types.ES3Type_Material::ReadUnityObject(ES3Reader,System.Object)
// 0x0000036C System.Void ES3Types.ES3Type_Material::.cctor()
extern void ES3Type_Material__cctor_mF1E38ABDF2971076FEAAF35D42765AE7162377E9 ();
// 0x0000036D System.Void ES3Types.ES3Type_MaterialArray::.ctor()
extern void ES3Type_MaterialArray__ctor_m46C16208AB51FB217D04ADF5E820A8FBA6EBB67B ();
// 0x0000036E System.Void ES3Types.ES3Type_Matrix4x4::.ctor()
extern void ES3Type_Matrix4x4__ctor_mC82D113B62080CC10EAB8FD98133C5BA767EE7CD ();
// 0x0000036F System.Void ES3Types.ES3Type_Matrix4x4::Write(System.Object,ES3Writer)
extern void ES3Type_Matrix4x4_Write_m684712A7D37DC3FBC1A6304B4DFDD26CF531EA56 ();
// 0x00000370 System.Object ES3Types.ES3Type_Matrix4x4::Read(ES3Reader)
// 0x00000371 System.Void ES3Types.ES3Type_Matrix4x4::.cctor()
extern void ES3Type_Matrix4x4__cctor_m48595FE31662A303C329274871462EC8BEBB7EEA ();
// 0x00000372 System.Void ES3Types.ES3Type_Matrix4x4Array::.ctor()
extern void ES3Type_Matrix4x4Array__ctor_mCF02AB264EF543DF556339BCD38D41A3687D0A10 ();
// 0x00000373 System.Void ES3Types.ES3Type_Mesh::.ctor()
extern void ES3Type_Mesh__ctor_mB842D25B6FD16BFD816B0E380D5CB1E6049B900C ();
// 0x00000374 System.Void ES3Types.ES3Type_Mesh::WriteUnityObject(System.Object,ES3Writer)
extern void ES3Type_Mesh_WriteUnityObject_m5462E4C396CFEED903B9BC64C65CA0E9658D5AF1 ();
// 0x00000375 System.Object ES3Types.ES3Type_Mesh::ReadUnityObject(ES3Reader)
// 0x00000376 System.Void ES3Types.ES3Type_Mesh::ReadUnityObject(ES3Reader,System.Object)
// 0x00000377 System.Void ES3Types.ES3Type_Mesh::.cctor()
extern void ES3Type_Mesh__cctor_m3415CBAFFF26967A2F46494806C421C57EA6C1D5 ();
// 0x00000378 System.Void ES3Types.ES3Type_MinMaxCurve::.ctor()
extern void ES3Type_MinMaxCurve__ctor_m0D22187676E6CBD8CD6615E3E841BFBE5E10B613 ();
// 0x00000379 System.Void ES3Types.ES3Type_MinMaxCurve::Write(System.Object,ES3Writer)
extern void ES3Type_MinMaxCurve_Write_m6505BD9D23BB31DFE8161810A81C9E467D43C764 ();
// 0x0000037A System.Object ES3Types.ES3Type_MinMaxCurve::Read(ES3Reader)
// 0x0000037B System.Void ES3Types.ES3Type_MinMaxCurve::ReadInto(ES3Reader,System.Object)
// 0x0000037C System.Void ES3Types.ES3Type_MinMaxCurve::.cctor()
extern void ES3Type_MinMaxCurve__cctor_m5D350DDEF19E41CAE0502B6C9F0C784754604ECF ();
// 0x0000037D System.Void ES3Types.ES3Type_MinMaxGradient::.ctor()
extern void ES3Type_MinMaxGradient__ctor_mE27422408BB947FB80C047A3B34E7178DB11C606 ();
// 0x0000037E System.Void ES3Types.ES3Type_MinMaxGradient::Write(System.Object,ES3Writer)
extern void ES3Type_MinMaxGradient_Write_m6EAFBDDEEABB94515F4B01BAF91F71F0F0F23323 ();
// 0x0000037F System.Object ES3Types.ES3Type_MinMaxGradient::Read(ES3Reader)
// 0x00000380 System.Void ES3Types.ES3Type_MinMaxGradient::.cctor()
extern void ES3Type_MinMaxGradient__cctor_m2CED6CABDD4AB43993BCA15531A6266D873E1BE7 ();
// 0x00000381 System.Void ES3Types.ES3Type_NoiseModule::.ctor()
extern void ES3Type_NoiseModule__ctor_m13B9423D29205C0BB952F75FFCD5D19BD8E1F020 ();
// 0x00000382 System.Void ES3Types.ES3Type_NoiseModule::Write(System.Object,ES3Writer)
extern void ES3Type_NoiseModule_Write_m34C034E1E1D729BC9CFA550F2023410E629D8F20 ();
// 0x00000383 System.Object ES3Types.ES3Type_NoiseModule::Read(ES3Reader)
// 0x00000384 System.Void ES3Types.ES3Type_NoiseModule::ReadInto(ES3Reader,System.Object)
// 0x00000385 System.Void ES3Types.ES3Type_NoiseModule::.cctor()
extern void ES3Type_NoiseModule__cctor_m1ECDB2649C7B01622E20C54F861045B874487BCE ();
// 0x00000386 System.Void ES3Types.ES3Type_PhysicMaterial::.ctor()
extern void ES3Type_PhysicMaterial__ctor_m35825A289AA97B9E5F829328DF0C388EEF39994C ();
// 0x00000387 System.Void ES3Types.ES3Type_PhysicMaterial::WriteObject(System.Object,ES3Writer)
extern void ES3Type_PhysicMaterial_WriteObject_m7A3943B9335E7A1B337F081ADED2C40F89F82168 ();
// 0x00000388 System.Void ES3Types.ES3Type_PhysicMaterial::ReadObject(ES3Reader,System.Object)
// 0x00000389 System.Object ES3Types.ES3Type_PhysicMaterial::ReadObject(ES3Reader)
// 0x0000038A System.Void ES3Types.ES3Type_PhysicMaterial::.cctor()
extern void ES3Type_PhysicMaterial__cctor_m07D0663CC61161779CD021ADCBD28947F1155AA0 ();
// 0x0000038B System.Void ES3Types.ES3Type_PhysicMaterialArray::.ctor()
extern void ES3Type_PhysicMaterialArray__ctor_mE3F39A13477458BD76F87C0140B54F5D2575498B ();
// 0x0000038C System.Void ES3Types.ES3Type_PhysicsMaterial2D::.ctor()
extern void ES3Type_PhysicsMaterial2D__ctor_m7DACAF9E497EB14E28601B72F69C25947695AE5D ();
// 0x0000038D System.Void ES3Types.ES3Type_PhysicsMaterial2D::WriteObject(System.Object,ES3Writer)
extern void ES3Type_PhysicsMaterial2D_WriteObject_mAD59E7A4291FF1E3D3D5EFE190C37B56223113B1 ();
// 0x0000038E System.Void ES3Types.ES3Type_PhysicsMaterial2D::ReadObject(ES3Reader,System.Object)
// 0x0000038F System.Object ES3Types.ES3Type_PhysicsMaterial2D::ReadObject(ES3Reader)
// 0x00000390 System.Void ES3Types.ES3Type_PhysicsMaterial2D::.cctor()
extern void ES3Type_PhysicsMaterial2D__cctor_mC237C5B979125BE2FA5B003EA3C354B94CD059D8 ();
// 0x00000391 System.Void ES3Types.ES3Type_PhysicsMaterial2DArray::.ctor()
extern void ES3Type_PhysicsMaterial2DArray__ctor_m905FD6AE1EE306CC001C517CAD64ADF3CB304137 ();
// 0x00000392 System.Void ES3Types.ES3Type_Quaternion::.ctor()
extern void ES3Type_Quaternion__ctor_m6220CA0258FE427536005C13C6A1F922BAFE8486 ();
// 0x00000393 System.Void ES3Types.ES3Type_Quaternion::Write(System.Object,ES3Writer)
extern void ES3Type_Quaternion_Write_mFA1BAEE914E8988A6734E3AD665F64FB8DB8EDA4 ();
// 0x00000394 System.Object ES3Types.ES3Type_Quaternion::Read(ES3Reader)
// 0x00000395 System.Void ES3Types.ES3Type_Quaternion::.cctor()
extern void ES3Type_Quaternion__cctor_m47D0BDC9E6C0354EF1AD4858EF281992DA192302 ();
// 0x00000396 System.Void ES3Types.ES3Type_QuaternionArray::.ctor()
extern void ES3Type_QuaternionArray__ctor_m689487BD82EEA2E1F952DE559BA40E93B24026C8 ();
// 0x00000397 System.Void ES3Types.ES3Type_Rect::.ctor()
extern void ES3Type_Rect__ctor_m4D363C11EDBDCAC1C2ACA14515304BDC0FC91983 ();
// 0x00000398 System.Void ES3Types.ES3Type_Rect::Write(System.Object,ES3Writer)
extern void ES3Type_Rect_Write_m8786EDF1FA710954952EEB76BE0AE38D72144F2A ();
// 0x00000399 System.Object ES3Types.ES3Type_Rect::Read(ES3Reader)
// 0x0000039A System.Void ES3Types.ES3Type_Rect::.cctor()
extern void ES3Type_Rect__cctor_m5363881021C14A3211DA61CC2B38AC7F76D813E7 ();
// 0x0000039B System.Void ES3Types.ES3Type_RectTransform::.ctor()
extern void ES3Type_RectTransform__ctor_m0177971461DDFD79A892F1CAA1E162368FB55017 ();
// 0x0000039C System.Void ES3Types.ES3Type_RectTransform::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_RectTransform_WriteComponent_mE5566261EF742B5C2DF1B1828D90045369B44672 ();
// 0x0000039D System.Void ES3Types.ES3Type_RectTransform::ReadComponent(ES3Reader,System.Object)
// 0x0000039E System.Void ES3Types.ES3Type_RectTransform::.cctor()
extern void ES3Type_RectTransform__cctor_mF3089825534CF4B9AB472DE5A7E25B3CC1389E5E ();
// 0x0000039F System.Void ES3Types.ES3Type_RotationBySpeedModule::.ctor()
extern void ES3Type_RotationBySpeedModule__ctor_mD7D75E9BC0F4205FBFD409F88488D9D4F41ED7ED ();
// 0x000003A0 System.Void ES3Types.ES3Type_RotationBySpeedModule::Write(System.Object,ES3Writer)
extern void ES3Type_RotationBySpeedModule_Write_m58F70BEBD0E667B3C8AC1632409B82C47ADE4D13 ();
// 0x000003A1 System.Object ES3Types.ES3Type_RotationBySpeedModule::Read(ES3Reader)
// 0x000003A2 System.Void ES3Types.ES3Type_RotationBySpeedModule::ReadInto(ES3Reader,System.Object)
// 0x000003A3 System.Void ES3Types.ES3Type_RotationBySpeedModule::.cctor()
extern void ES3Type_RotationBySpeedModule__cctor_m45D15D48B0C206E01A606796A5F288881F14CDD0 ();
// 0x000003A4 System.Void ES3Types.ES3Type_RotationOverLifetimeModule::.ctor()
extern void ES3Type_RotationOverLifetimeModule__ctor_m4BC62B634B258087C236E65CCD57FAEB89F63A2D ();
// 0x000003A5 System.Void ES3Types.ES3Type_RotationOverLifetimeModule::Write(System.Object,ES3Writer)
extern void ES3Type_RotationOverLifetimeModule_Write_mD06EB4D3B252901A99C563A992100DA296D2A590 ();
// 0x000003A6 System.Object ES3Types.ES3Type_RotationOverLifetimeModule::Read(ES3Reader)
// 0x000003A7 System.Void ES3Types.ES3Type_RotationOverLifetimeModule::ReadInto(ES3Reader,System.Object)
// 0x000003A8 System.Void ES3Types.ES3Type_RotationOverLifetimeModule::.cctor()
extern void ES3Type_RotationOverLifetimeModule__cctor_mFC2C6D16AF41E2170C6AA4844C10BE0F710F7F93 ();
// 0x000003A9 System.Void ES3Types.ES3Type_Shader::.ctor()
extern void ES3Type_Shader__ctor_m184228173F234807AAE8F29F7C30D7AC63C00021 ();
// 0x000003AA System.Void ES3Types.ES3Type_Shader::Write(System.Object,ES3Writer)
extern void ES3Type_Shader_Write_m2A41ACD16BC5AEBADCD4D5BC445658ACDD2856E1 ();
// 0x000003AB System.Object ES3Types.ES3Type_Shader::Read(ES3Reader)
// 0x000003AC System.Void ES3Types.ES3Type_Shader::ReadInto(ES3Reader,System.Object)
// 0x000003AD System.Void ES3Types.ES3Type_Shader::.cctor()
extern void ES3Type_Shader__cctor_mEA0E4BC73C37DE82376ABA38CA291D26409B9638 ();
// 0x000003AE System.Void ES3Types.ES3Type_ShaderArray::.ctor()
extern void ES3Type_ShaderArray__ctor_m9F418B10CB57EA26D68BA4D8ECC273A140E6E882 ();
// 0x000003AF System.Void ES3Types.ES3Type_ShapeModule::.ctor()
extern void ES3Type_ShapeModule__ctor_mE1206D7CFB317044DD32CC1AC38F4E797973A097 ();
// 0x000003B0 System.Void ES3Types.ES3Type_ShapeModule::Write(System.Object,ES3Writer)
extern void ES3Type_ShapeModule_Write_m45098488B29CB93233C8FCD10C223C309DAA3A52 ();
// 0x000003B1 System.Object ES3Types.ES3Type_ShapeModule::Read(ES3Reader)
// 0x000003B2 System.Void ES3Types.ES3Type_ShapeModule::ReadInto(ES3Reader,System.Object)
// 0x000003B3 System.Void ES3Types.ES3Type_ShapeModule::.cctor()
extern void ES3Type_ShapeModule__cctor_mB095CD24DB760978E53C81A81A73CCB21E4A4099 ();
// 0x000003B4 System.Void ES3Types.ES3Type_SizeBySpeedModule::.ctor()
extern void ES3Type_SizeBySpeedModule__ctor_mF3EB07C4141EB15C6CA08C3C8902647EF60CC384 ();
// 0x000003B5 System.Void ES3Types.ES3Type_SizeBySpeedModule::Write(System.Object,ES3Writer)
extern void ES3Type_SizeBySpeedModule_Write_m8223285FDEF4A32ACC92CED2AABB1CF48079617C ();
// 0x000003B6 System.Object ES3Types.ES3Type_SizeBySpeedModule::Read(ES3Reader)
// 0x000003B7 System.Void ES3Types.ES3Type_SizeBySpeedModule::ReadInto(ES3Reader,System.Object)
// 0x000003B8 System.Void ES3Types.ES3Type_SizeBySpeedModule::.cctor()
extern void ES3Type_SizeBySpeedModule__cctor_m5CEEFD445337663CD93C7AE53FCDD14BB02E4C4D ();
// 0x000003B9 System.Void ES3Types.ES3Type_SizeOverLifetimeModule::.ctor()
extern void ES3Type_SizeOverLifetimeModule__ctor_m3BF12F1F2691CAA7B340D6AC69648EC6ECA3C367 ();
// 0x000003BA System.Void ES3Types.ES3Type_SizeOverLifetimeModule::Write(System.Object,ES3Writer)
extern void ES3Type_SizeOverLifetimeModule_Write_m8355CB911DCBF412D61A817D9543AE4FC5B19205 ();
// 0x000003BB System.Object ES3Types.ES3Type_SizeOverLifetimeModule::Read(ES3Reader)
// 0x000003BC System.Void ES3Types.ES3Type_SizeOverLifetimeModule::ReadInto(ES3Reader,System.Object)
// 0x000003BD System.Void ES3Types.ES3Type_SizeOverLifetimeModule::.cctor()
extern void ES3Type_SizeOverLifetimeModule__cctor_m019611468D9C7A9D7CD0615EE79E9AEAE052A628 ();
// 0x000003BE System.Void ES3Types.ES3Type_SkinnedMeshRenderer::.ctor()
extern void ES3Type_SkinnedMeshRenderer__ctor_m6E962E3F9801B2784B0D37AB8F9AAEF1374D83DF ();
// 0x000003BF System.Void ES3Types.ES3Type_SkinnedMeshRenderer::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_SkinnedMeshRenderer_WriteComponent_m07EF5271929295CC224FB1E282DCF2CFFBE54699 ();
// 0x000003C0 System.Void ES3Types.ES3Type_SkinnedMeshRenderer::ReadComponent(ES3Reader,System.Object)
// 0x000003C1 System.Void ES3Types.ES3Type_SkinnedMeshRenderer::.cctor()
extern void ES3Type_SkinnedMeshRenderer__cctor_mE5707A7412ABE805618326AEADB71CE81E428EFA ();
// 0x000003C2 System.Void ES3Types.ES3Type_SkinnedMeshRendererArray::.ctor()
extern void ES3Type_SkinnedMeshRendererArray__ctor_mCAF302C8F53FE94E2D7AB1D303A183DE58D40B42 ();
// 0x000003C3 System.Void ES3Types.ES3Type_Sprite::.ctor()
extern void ES3Type_Sprite__ctor_m89015E5BFBDB29039B63AD0C673C670C9BCF310C ();
// 0x000003C4 System.Void ES3Types.ES3Type_Sprite::WriteUnityObject(System.Object,ES3Writer)
extern void ES3Type_Sprite_WriteUnityObject_m87455FE051A3F76B0A2D58C8A4368E81FB45AC45 ();
// 0x000003C5 System.Void ES3Types.ES3Type_Sprite::ReadUnityObject(ES3Reader,System.Object)
// 0x000003C6 System.Object ES3Types.ES3Type_Sprite::ReadUnityObject(ES3Reader)
// 0x000003C7 System.Void ES3Types.ES3Type_Sprite::.cctor()
extern void ES3Type_Sprite__cctor_m4FE9D599B813B2072BD15A3C9083B5D995BFA64E ();
// 0x000003C8 System.Void ES3Types.ES3Type_SpriteRenderer::.ctor()
extern void ES3Type_SpriteRenderer__ctor_m287A19497A77D1AF6445FF14DDF8D92489B367F3 ();
// 0x000003C9 System.Void ES3Types.ES3Type_SpriteRenderer::WriteComponent(System.Object,ES3Writer)
extern void ES3Type_SpriteRenderer_WriteComponent_mC62C01406041595A364181A8884548842125B0E9 ();
// 0x000003CA System.Void ES3Types.ES3Type_SpriteRenderer::ReadComponent(ES3Reader,System.Object)
// 0x000003CB System.Void ES3Types.ES3Type_SpriteRenderer::.cctor()
extern void ES3Type_SpriteRenderer__cctor_mD60057B703BA03FE38F1AB578FD595CCCB293829 ();
// 0x000003CC System.Void ES3Types.ES3Type_SpriteRendererArray::.ctor()
extern void ES3Type_SpriteRendererArray__ctor_m5F14EF74558191ED02A7D7C8CDE63BA5A4BF6E87 ();
// 0x000003CD System.Void ES3Types.ES3Type_SubEmittersModule::.ctor()
extern void ES3Type_SubEmittersModule__ctor_m7BEBB80F026B6E6019BCE601C3DFC1A56AAA6287 ();
// 0x000003CE System.Void ES3Types.ES3Type_SubEmittersModule::Write(System.Object,ES3Writer)
extern void ES3Type_SubEmittersModule_Write_mC02863802E98F67A046D88801E1CC4D481E73D09 ();
// 0x000003CF System.Object ES3Types.ES3Type_SubEmittersModule::Read(ES3Reader)
// 0x000003D0 System.Void ES3Types.ES3Type_SubEmittersModule::ReadInto(ES3Reader,System.Object)
// 0x000003D1 System.Void ES3Types.ES3Type_SubEmittersModule::.cctor()
extern void ES3Type_SubEmittersModule__cctor_mBC0957D231233E5DB00149202DADA7F638675A71 ();
// 0x000003D2 System.Void ES3Types.ES3Type_Texture::.ctor()
extern void ES3Type_Texture__ctor_m575E7A1AED5146965A411CB7C0A1E9A5F72414D3 ();
// 0x000003D3 System.Void ES3Types.ES3Type_Texture::Write(System.Object,ES3Writer)
extern void ES3Type_Texture_Write_m80E826C76B2A2C9A668CC9F126C1A18A76D202AA ();
// 0x000003D4 System.Void ES3Types.ES3Type_Texture::ReadInto(ES3Reader,System.Object)
// 0x000003D5 System.Object ES3Types.ES3Type_Texture::Read(ES3Reader)
// 0x000003D6 System.Void ES3Types.ES3Type_Texture::.cctor()
extern void ES3Type_Texture__cctor_m75F27A34C6E1D8BD2E23F36469D00B5F270D086E ();
// 0x000003D7 System.Void ES3Types.ES3Type_TextureArray::.ctor()
extern void ES3Type_TextureArray__ctor_m7C5B02564127EEA9E4966225E3CB72816A280FBB ();
// 0x000003D8 System.Void ES3Types.ES3Type_Texture2D::.ctor()
extern void ES3Type_Texture2D__ctor_m3BABC7A255BF766360CB763D3B61B073945F95E7 ();
// 0x000003D9 System.Void ES3Types.ES3Type_Texture2D::WriteUnityObject(System.Object,ES3Writer)
extern void ES3Type_Texture2D_WriteUnityObject_mD123C9020BDF1F5C7EA3724BDE5F91EE8AC25503 ();
// 0x000003DA System.Void ES3Types.ES3Type_Texture2D::ReadUnityObject(ES3Reader,System.Object)
// 0x000003DB System.Object ES3Types.ES3Type_Texture2D::ReadUnityObject(ES3Reader)
// 0x000003DC System.Boolean ES3Types.ES3Type_Texture2D::IsReadable(UnityEngine.Texture2D)
extern void ES3Type_Texture2D_IsReadable_m95F6497DE57EF3390D2A614B91637E8C6C24DE25 ();
// 0x000003DD System.Void ES3Types.ES3Type_Texture2D::.cctor()
extern void ES3Type_Texture2D__cctor_m4BAAE048CA19A2583C6AE4AE3FED5EC017EDBC98 ();
// 0x000003DE System.Void ES3Types.ES3Type_Texture2DArray::.ctor()
extern void ES3Type_Texture2DArray__ctor_m8923DBA70FE6E2089412F1E5CC61198260B8CCFD ();
// 0x000003DF System.Void ES3Types.ES3Type_TextureSheetAnimationModule::.ctor()
extern void ES3Type_TextureSheetAnimationModule__ctor_m63ACF4715E13911E7251AF79264D7CAF53F1456C ();
// 0x000003E0 System.Void ES3Types.ES3Type_TextureSheetAnimationModule::Write(System.Object,ES3Writer)
extern void ES3Type_TextureSheetAnimationModule_Write_m8783EBFC26F9AAE1035CDCE4F26927BDCC88ACBE ();
// 0x000003E1 System.Object ES3Types.ES3Type_TextureSheetAnimationModule::Read(ES3Reader)
// 0x000003E2 System.Void ES3Types.ES3Type_TextureSheetAnimationModule::ReadInto(ES3Reader,System.Object)
// 0x000003E3 System.Void ES3Types.ES3Type_TextureSheetAnimationModule::.cctor()
extern void ES3Type_TextureSheetAnimationModule__cctor_mBCB3BAAC27A9C7E85B8BB2B7EE4C8F1B4620B2EF ();
// 0x000003E4 System.Void ES3Types.ES3Type_TrailModule::.ctor()
extern void ES3Type_TrailModule__ctor_mFE44C0359B0FFAEB74B536A328FCAC8B0B2A9382 ();
// 0x000003E5 System.Void ES3Types.ES3Type_TrailModule::Write(System.Object,ES3Writer)
extern void ES3Type_TrailModule_Write_mCED5039A74BF9C7DBB32D6BA4D9BC40C28644A2F ();
// 0x000003E6 System.Object ES3Types.ES3Type_TrailModule::Read(ES3Reader)
// 0x000003E7 System.Void ES3Types.ES3Type_TrailModule::ReadInto(ES3Reader,System.Object)
// 0x000003E8 System.Void ES3Types.ES3Type_TrailModule::.cctor()
extern void ES3Type_TrailModule__cctor_m2993517CE074536DE4CDB121A705B764D9CC163F ();
// 0x000003E9 System.Void ES3Types.ES3Type_TriggerModule::.ctor()
extern void ES3Type_TriggerModule__ctor_m7EC4E96F96388AA336D3E382CAD7194CE2CE2075 ();
// 0x000003EA System.Void ES3Types.ES3Type_TriggerModule::Write(System.Object,ES3Writer)
extern void ES3Type_TriggerModule_Write_m2DE5C2E6A95FDA3709D190AF3288CE92E6501852 ();
// 0x000003EB System.Object ES3Types.ES3Type_TriggerModule::Read(ES3Reader)
// 0x000003EC System.Void ES3Types.ES3Type_TriggerModule::ReadInto(ES3Reader,System.Object)
// 0x000003ED System.Void ES3Types.ES3Type_TriggerModule::.cctor()
extern void ES3Type_TriggerModule__cctor_m17AE6580D682B8F5B63F6A3D6F5DA50CF7DBA04B ();
// 0x000003EE System.Void ES3Types.ES3Type_Vector2::.ctor()
extern void ES3Type_Vector2__ctor_m4CF1FAE34C6F1024D7E79B81931963A3ECD17953 ();
// 0x000003EF System.Void ES3Types.ES3Type_Vector2::Write(System.Object,ES3Writer)
extern void ES3Type_Vector2_Write_m3D44D5043AEE1FB56AE1552CD2FE626B6607F011 ();
// 0x000003F0 System.Object ES3Types.ES3Type_Vector2::Read(ES3Reader)
// 0x000003F1 System.Void ES3Types.ES3Type_Vector2::.cctor()
extern void ES3Type_Vector2__cctor_mE1276FF4874CB05EABAD7689AB3DE49112D248A1 ();
// 0x000003F2 System.Void ES3Types.ES3Type_Vector2Array::.ctor()
extern void ES3Type_Vector2Array__ctor_m0B5980492D4756C0620D21599BD0D3E962780D30 ();
// 0x000003F3 System.Void ES3Types.ES3Type_Vector2Int::.ctor()
extern void ES3Type_Vector2Int__ctor_mDF9045B451096E73C41B0FB1F6EC54B7FC4BFE67 ();
// 0x000003F4 System.Void ES3Types.ES3Type_Vector2Int::Write(System.Object,ES3Writer)
extern void ES3Type_Vector2Int_Write_mAAA5C18513BC345A530A46035A38E1EEAA18A9B6 ();
// 0x000003F5 System.Object ES3Types.ES3Type_Vector2Int::Read(ES3Reader)
// 0x000003F6 System.Void ES3Types.ES3Type_Vector2Int::.cctor()
extern void ES3Type_Vector2Int__cctor_mC9A2863F60E99223B35325D43C4EA29B11F2E0B3 ();
// 0x000003F7 System.Void ES3Types.ES3Type_Vector2IntArray::.ctor()
extern void ES3Type_Vector2IntArray__ctor_mA438889206B21979FFF8C158015A94A12C7E2321 ();
// 0x000003F8 System.Void ES3Types.ES3Type_Vector3::.ctor()
extern void ES3Type_Vector3__ctor_m7EDC5B207AE8B67A5D2788F71AFDE9D054EE0472 ();
// 0x000003F9 System.Void ES3Types.ES3Type_Vector3::Write(System.Object,ES3Writer)
extern void ES3Type_Vector3_Write_m76AEA0845E34F13C33EA5779AE38664111EAD921 ();
// 0x000003FA System.Object ES3Types.ES3Type_Vector3::Read(ES3Reader)
// 0x000003FB System.Void ES3Types.ES3Type_Vector3::.cctor()
extern void ES3Type_Vector3__cctor_m25669987B1D861BE3CA3DDB1DD0545DE11D0736D ();
// 0x000003FC System.Void ES3Types.ES3Type_Vector3Array::.ctor()
extern void ES3Type_Vector3Array__ctor_mDF32324C7243E8B546D8FB2A407232434B71E1CC ();
// 0x000003FD System.Void ES3Types.ES3Type_Vector3Int::.ctor()
extern void ES3Type_Vector3Int__ctor_m23AFF374E7F2C9EF9F7C13AEB0642DE17BC88AF7 ();
// 0x000003FE System.Void ES3Types.ES3Type_Vector3Int::Write(System.Object,ES3Writer)
extern void ES3Type_Vector3Int_Write_m618B770521CF19276FFF81EAC0561F81DF331BCC ();
// 0x000003FF System.Object ES3Types.ES3Type_Vector3Int::Read(ES3Reader)
// 0x00000400 System.Void ES3Types.ES3Type_Vector3Int::.cctor()
extern void ES3Type_Vector3Int__cctor_m8714EA08E874B125AF6139468BB1D7445DC46BCA ();
// 0x00000401 System.Void ES3Types.ES3Type_Vector3IntArray::.ctor()
extern void ES3Type_Vector3IntArray__ctor_mB188F701ABDEBB75427D1421113368169A206089 ();
// 0x00000402 System.Void ES3Types.ES3Type_Vector4::.ctor()
extern void ES3Type_Vector4__ctor_mA2A6D8214BB0EED7AC9770340E5915225BA67A69 ();
// 0x00000403 System.Void ES3Types.ES3Type_Vector4::Write(System.Object,ES3Writer)
extern void ES3Type_Vector4_Write_m02DDF5D37D292F2DA9C225637B57278B5894CD05 ();
// 0x00000404 System.Object ES3Types.ES3Type_Vector4::Read(ES3Reader)
// 0x00000405 System.Boolean ES3Types.ES3Type_Vector4::Equals(UnityEngine.Vector4,UnityEngine.Vector4)
extern void ES3Type_Vector4_Equals_m2DDC340C5C5615FCA19FBDE470A58790870AA4FB ();
// 0x00000406 System.Void ES3Types.ES3Type_Vector4::.cctor()
extern void ES3Type_Vector4__cctor_m77FDBE72C206DF0FF25FFBCE9A9FBCDC5D28BFAA ();
// 0x00000407 System.Void ES3Types.ES3Type_Vector4Array::.ctor()
extern void ES3Type_Vector4Array__ctor_m341AE128A36D1682F2AA7E6E3ED7A08A9F4F2BBA ();
// 0x00000408 System.Void ES3Types.ES3Type_VelocityOverLifetimeModule::.ctor()
extern void ES3Type_VelocityOverLifetimeModule__ctor_m36062CF3E34DF22640EDC1838DA43CB044D77058 ();
// 0x00000409 System.Void ES3Types.ES3Type_VelocityOverLifetimeModule::Write(System.Object,ES3Writer)
extern void ES3Type_VelocityOverLifetimeModule_Write_mB3B793E195D390747AFD12047C836DF76E7A04A7 ();
// 0x0000040A System.Object ES3Types.ES3Type_VelocityOverLifetimeModule::Read(ES3Reader)
// 0x0000040B System.Void ES3Types.ES3Type_VelocityOverLifetimeModule::ReadInto(ES3Reader,System.Object)
// 0x0000040C System.Void ES3Types.ES3Type_VelocityOverLifetimeModule::.cctor()
extern void ES3Type_VelocityOverLifetimeModule__cctor_m80DBBD4A9714AC5E64F0E80F7DCF3CCCD3F025B7 ();
// 0x0000040D System.Void ES3Internal.ES3Debug::Log(System.String,UnityEngine.Object,System.Int32)
extern void ES3Debug_Log_mCD7C61578DD7976547700C80316C41CCBD0DE7AC ();
// 0x0000040E System.Void ES3Internal.ES3Debug::LogWarning(System.String,UnityEngine.Object,System.Int32)
extern void ES3Debug_LogWarning_mA02F0B62778A55BD061330D7FB01ABE09547A0E1 ();
// 0x0000040F System.Void ES3Internal.ES3Debug::LogError(System.String,UnityEngine.Object,System.Int32)
extern void ES3Debug_LogError_m6C096EC10B905B1FAA7F736F9949640FCEEB6D21 ();
// 0x00000410 System.String ES3Internal.ES3Debug::Indent(System.Int32)
extern void ES3Debug_Indent_mCA5244FDB6E90BB5585AD21470ACE2AE0195DA6B ();
// 0x00000411 System.String ES3Internal.ES3Hash::SHA1Hash(System.String)
extern void ES3Hash_SHA1Hash_mED3D75B460E3DAF52049CE048EE4F4E5E6D0BB00 ();
// 0x00000412 System.Void ES3Internal.EncryptionAlgorithm::Encrypt(System.IO.Stream,System.IO.Stream,System.String,System.Int32)
// 0x00000413 System.Void ES3Internal.EncryptionAlgorithm::Decrypt(System.IO.Stream,System.IO.Stream,System.String,System.Int32)
// 0x00000414 System.Void ES3Internal.EncryptionAlgorithm::CopyStream(System.IO.Stream,System.IO.Stream,System.Int32)
extern void EncryptionAlgorithm_CopyStream_m3B2007F7657F9A17A48DAC2CA2373BA1438749F2 ();
// 0x00000415 System.Void ES3Internal.EncryptionAlgorithm::.ctor()
extern void EncryptionAlgorithm__ctor_m756DE35582AAE650CEA7EADD2798B19D86C34CF4 ();
// 0x00000416 System.Void ES3Internal.AESEncryptionAlgorithm::Encrypt(System.IO.Stream,System.IO.Stream,System.String,System.Int32)
extern void AESEncryptionAlgorithm_Encrypt_m6AA3CD54A1C43E95788852276B65B6024C660430 ();
// 0x00000417 System.Void ES3Internal.AESEncryptionAlgorithm::Decrypt(System.IO.Stream,System.IO.Stream,System.String,System.Int32)
extern void AESEncryptionAlgorithm_Decrypt_m8685A3291C0D95A4B102D5ADC85167A7C66878F4 ();
// 0x00000418 System.Void ES3Internal.AESEncryptionAlgorithm::.ctor()
extern void AESEncryptionAlgorithm__ctor_mC7E03A00030639CA552110368B093C1DE57C49C0 ();
// 0x00000419 System.Void ES3Internal.UnbufferedCryptoStream::.ctor(System.IO.Stream,System.Boolean,System.String,System.Int32,ES3Internal.EncryptionAlgorithm)
extern void UnbufferedCryptoStream__ctor_m78F855CABFD60A1BC0716BEC65501ACD0A6CC53F ();
// 0x0000041A System.Void ES3Internal.UnbufferedCryptoStream::Dispose(System.Boolean)
extern void UnbufferedCryptoStream_Dispose_m809D283D2CCA55B5D193704DE8DBE0F857BE46E8 ();
// 0x0000041B System.Void ES3Internal.ES3Data::.ctor(System.Type,System.Byte[])
extern void ES3Data__ctor_m16AF906373359317ADEB63838FC1630EA2969C14_AdjustorThunk ();
// 0x0000041C System.Void ES3Internal.ES3Data::.ctor(ES3Types.ES3Type,System.Byte[])
extern void ES3Data__ctor_m1CF0FDCFDA248E120941C48F1CCD29A4A7A22B0C_AdjustorThunk ();
// 0x0000041D System.DateTime ES3Internal.ES3IO::GetTimestamp(System.String)
extern void ES3IO_GetTimestamp_m953CB1ABC0D95994E8E9C2BCD924E6A1B6066CD2 ();
// 0x0000041E System.String ES3Internal.ES3IO::GetExtension(System.String)
extern void ES3IO_GetExtension_m95CF0B403A1A46DB06AE99B3A51801810711F4C6 ();
// 0x0000041F System.Void ES3Internal.ES3IO::DeleteFile(System.String)
extern void ES3IO_DeleteFile_m6D735FF7C91F96CE3CB570D3CB2980D4162C7512 ();
// 0x00000420 System.Boolean ES3Internal.ES3IO::FileExists(System.String)
extern void ES3IO_FileExists_mD330225B2159D0F32949109EA25FAF122E0B94C3 ();
// 0x00000421 System.Void ES3Internal.ES3IO::MoveFile(System.String,System.String)
extern void ES3IO_MoveFile_mD8CEE6ACC05835575A7F4EBA455F9A08AF9CE565 ();
// 0x00000422 System.Void ES3Internal.ES3IO::CopyFile(System.String,System.String)
extern void ES3IO_CopyFile_mD9A1D5E2EA769034E4E4C5F093FC9C40436B8ADD ();
// 0x00000423 System.Void ES3Internal.ES3IO::MoveDirectory(System.String,System.String)
extern void ES3IO_MoveDirectory_mF4BEC0A7E128ACD28718F612EB506742A10EA3C4 ();
// 0x00000424 System.Void ES3Internal.ES3IO::CreateDirectory(System.String)
extern void ES3IO_CreateDirectory_mA759449C8C7A4E36F31210D8851B9E763D97AE29 ();
// 0x00000425 System.Boolean ES3Internal.ES3IO::DirectoryExists(System.String)
extern void ES3IO_DirectoryExists_mC4CEA972AE143FF64E296C8A6C3017BD5AA91478 ();
// 0x00000426 System.String ES3Internal.ES3IO::GetDirectoryPath(System.String,System.Char)
extern void ES3IO_GetDirectoryPath_m60479932D52DB290DA79FB6E0877D678C89ABF6A ();
// 0x00000427 System.Boolean ES3Internal.ES3IO::UsesForwardSlash(System.String)
extern void ES3IO_UsesForwardSlash_m6DECE9532F0AF85DBDD7E5E14C3214F8E982CAA3 ();
// 0x00000428 System.String ES3Internal.ES3IO::CombinePathAndFilename(System.String,System.String)
extern void ES3IO_CombinePathAndFilename_mC313E2941A4F0BD4B939B82DA805047E59251BB6 ();
// 0x00000429 System.String[] ES3Internal.ES3IO::GetDirectories(System.String,System.Boolean)
extern void ES3IO_GetDirectories_mFC556886532E91AFA8A64A1D7079F0C9A63BDE28 ();
// 0x0000042A System.Void ES3Internal.ES3IO::DeleteDirectory(System.String)
extern void ES3IO_DeleteDirectory_m5E64CF3F72C3A4F0E8354F77EDA84E614B031AA6 ();
// 0x0000042B System.String[] ES3Internal.ES3IO::GetFiles(System.String,System.Boolean)
extern void ES3IO_GetFiles_mCBE0FA87B89E4ED543F722C7A6D2172409C873CF ();
// 0x0000042C System.Byte[] ES3Internal.ES3IO::ReadAllBytes(System.String)
extern void ES3IO_ReadAllBytes_m3013044D9A9D8AAB00FA2566B44402A83257CB83 ();
// 0x0000042D System.Void ES3Internal.ES3IO::WriteAllBytes(System.String,System.Byte[])
extern void ES3IO_WriteAllBytes_m47F7E3AA7778AAA049D6F180AE26A800A523663A ();
// 0x0000042E System.Void ES3Internal.ES3IO::CommitBackup(ES3Settings)
extern void ES3IO_CommitBackup_m1B9D0812093513607AA08762F27D74D968CE1408 ();
// 0x0000042F System.Void ES3Internal.ES3IO::.cctor()
extern void ES3IO__cctor_m6ED0F50AA3C6806652ACA8D74FFC00DF02418B25 ();
// 0x00000430 System.Void ES3Internal.ES3Prefab::Awake()
extern void ES3Prefab_Awake_mCF33EFFF279D947A0DAE9C17672FBE13329FD7AF ();
// 0x00000431 System.Int64 ES3Internal.ES3Prefab::Get(UnityEngine.Object)
extern void ES3Prefab_Get_mB219B4C2214EFCC2852AB6E62B4AD635F2A2EC0F ();
// 0x00000432 System.Int64 ES3Internal.ES3Prefab::Add(UnityEngine.Object)
extern void ES3Prefab_Add_mB3816D5DDECE6B902A7B34B56526D4D813B9A3F8 ();
// 0x00000433 System.Collections.Generic.Dictionary`2<System.String,System.String> ES3Internal.ES3Prefab::GetReferences()
extern void ES3Prefab_GetReferences_m976D68FAD98F7C4AC3A165E643DBA963F94DB04F ();
// 0x00000434 System.Void ES3Internal.ES3Prefab::ApplyReferences(System.Collections.Generic.Dictionary`2<System.Int64,System.Int64>)
extern void ES3Prefab_ApplyReferences_m8FFFD996F5A0E03CCBC379636F04D9FEADEC008E ();
// 0x00000435 System.Int64 ES3Internal.ES3Prefab::GetNewRefID()
extern void ES3Prefab_GetNewRefID_m1DF307C928C48C9A30EBB94C91FC77B6A29C67C5 ();
// 0x00000436 System.Void ES3Internal.ES3Prefab::.ctor()
extern void ES3Prefab__ctor_m4575F4EA31D2CF4D5699E686D603AFEDD9D623FE ();
// 0x00000437 ES3Internal.ES3ReferenceMgrBase ES3Internal.ES3ReferenceMgrBase::get_Current()
extern void ES3ReferenceMgrBase_get_Current_m43C6D8A5A18699219B471A291607A0E4BB4A38D8 ();
// 0x00000438 System.Boolean ES3Internal.ES3ReferenceMgrBase::get_IsInitialised()
extern void ES3ReferenceMgrBase_get_IsInitialised_m434C632F8827D6F31953E41770C3D143239C018A ();
// 0x00000439 ES3Internal.ES3RefIdDictionary ES3Internal.ES3ReferenceMgrBase::get_refId()
extern void ES3ReferenceMgrBase_get_refId_m793808277E67D2DB0C05B8A392A4DA33F099FC1D ();
// 0x0000043A System.Void ES3Internal.ES3ReferenceMgrBase::set_refId(ES3Internal.ES3RefIdDictionary)
extern void ES3ReferenceMgrBase_set_refId_mE4EC83174096DADBAA00518A51D222338D0EB2A5 ();
// 0x0000043B ES3Internal.ES3GlobalReferences ES3Internal.ES3ReferenceMgrBase::get_GlobalReferences()
extern void ES3ReferenceMgrBase_get_GlobalReferences_mC6C222787A8E5775CF75DD23E0E2AA10D89B69D5 ();
// 0x0000043C System.Void ES3Internal.ES3ReferenceMgrBase::Awake()
extern void ES3ReferenceMgrBase_Awake_mD42487217B99905D36DE308D812155FB15D1365F ();
// 0x0000043D System.Void ES3Internal.ES3ReferenceMgrBase::Merge(ES3Internal.ES3ReferenceMgrBase)
extern void ES3ReferenceMgrBase_Merge_m5E8755D54A7533433459891E4BC861BEE9297E2A ();
// 0x0000043E System.Int64 ES3Internal.ES3ReferenceMgrBase::Get(UnityEngine.Object)
extern void ES3ReferenceMgrBase_Get_mA655CB517BB9811528FFD1AB206888FB3E8B8088 ();
// 0x0000043F UnityEngine.Object ES3Internal.ES3ReferenceMgrBase::Get(System.Int64,System.Type)
extern void ES3ReferenceMgrBase_Get_m6810353CD7411FC7D36E4AB66CD268F2DDB29403 ();
// 0x00000440 UnityEngine.Object ES3Internal.ES3ReferenceMgrBase::Get(System.Int64,System.Boolean)
extern void ES3ReferenceMgrBase_Get_m1A862965D6A63E8A54D837B84D4A947000634F76 ();
// 0x00000441 ES3Internal.ES3Prefab ES3Internal.ES3ReferenceMgrBase::GetPrefab(System.Int64,System.Boolean)
extern void ES3ReferenceMgrBase_GetPrefab_m4F2F30E338F375E942B55D80E7DEF3810FCDF633 ();
// 0x00000442 System.Int64 ES3Internal.ES3ReferenceMgrBase::GetPrefab(ES3Internal.ES3Prefab,System.Boolean)
extern void ES3ReferenceMgrBase_GetPrefab_mA14FB7D11BAB040FBD21C62B66FDF022AF191E52 ();
// 0x00000443 System.Int64 ES3Internal.ES3ReferenceMgrBase::Add(UnityEngine.Object)
extern void ES3ReferenceMgrBase_Add_m450969F3F9A70AB1F5D46AFEAF38C3412B32322E ();
// 0x00000444 System.Int64 ES3Internal.ES3ReferenceMgrBase::Add(UnityEngine.Object,System.Int64)
extern void ES3ReferenceMgrBase_Add_mEB259F54F9A3909455EF088F43887A0874024AB8 ();
// 0x00000445 System.Boolean ES3Internal.ES3ReferenceMgrBase::AddPrefab(ES3Internal.ES3Prefab)
extern void ES3ReferenceMgrBase_AddPrefab_m3A25F33ED0295B15C4C45ECCE7E774DE6E3DE9D9 ();
// 0x00000446 System.Void ES3Internal.ES3ReferenceMgrBase::Remove(UnityEngine.Object)
extern void ES3ReferenceMgrBase_Remove_mF60FFAD74BB7FC4634AC0FF1FBCAAB41E044A439 ();
// 0x00000447 System.Void ES3Internal.ES3ReferenceMgrBase::Remove(System.Int64)
extern void ES3ReferenceMgrBase_Remove_m672A8D0E3ED156358B6F8FBB803E8569ED8A1FF1 ();
// 0x00000448 System.Void ES3Internal.ES3ReferenceMgrBase::RemoveNullValues()
extern void ES3ReferenceMgrBase_RemoveNullValues_mD43BA38F919FB0C65AB43BD85BF640914FC5A505 ();
// 0x00000449 System.Void ES3Internal.ES3ReferenceMgrBase::Clear()
extern void ES3ReferenceMgrBase_Clear_mA4379A68B0414B23B0262F77504736ED643138E0 ();
// 0x0000044A System.Boolean ES3Internal.ES3ReferenceMgrBase::Contains(UnityEngine.Object)
extern void ES3ReferenceMgrBase_Contains_mF0B981E5278FEA20EBC957319F11CDE34B199398 ();
// 0x0000044B System.Boolean ES3Internal.ES3ReferenceMgrBase::Contains(System.Int64)
extern void ES3ReferenceMgrBase_Contains_m3F4272814DFB74EEB4C41CAE41A2F90CCC34161E ();
// 0x0000044C System.Void ES3Internal.ES3ReferenceMgrBase::ChangeId(System.Int64,System.Int64)
extern void ES3ReferenceMgrBase_ChangeId_m3FD0FA230B58D2B06BEE596AC65519F9CA1AD460 ();
// 0x0000044D System.Int64 ES3Internal.ES3ReferenceMgrBase::GetNewRefID()
extern void ES3ReferenceMgrBase_GetNewRefID_mC9521DAC9296396E3057D7251446A77BCBC3E4B2 ();
// 0x0000044E System.Boolean ES3Internal.ES3ReferenceMgrBase::CanBeSaved(UnityEngine.Object)
extern void ES3ReferenceMgrBase_CanBeSaved_m5A848AE9D162BB0D75934E25E73C91B78F63E8C6 ();
// 0x0000044F System.Void ES3Internal.ES3ReferenceMgrBase::.ctor()
extern void ES3ReferenceMgrBase__ctor_m0D02EBDFBA8614B971735F706FCF23E6B0984F22 ();
// 0x00000450 System.Void ES3Internal.ES3ReferenceMgrBase::.cctor()
extern void ES3ReferenceMgrBase__cctor_m83C43956082B3F62377D329D791E3F2E007E2DE4 ();
// 0x00000451 System.Boolean ES3Internal.ES3IdRefDictionary::KeysAreEqual(System.Int64,System.Int64)
extern void ES3IdRefDictionary_KeysAreEqual_m196F94FEF321BB11B8622977E07003B9BCEB9EA1 ();
// 0x00000452 System.Boolean ES3Internal.ES3IdRefDictionary::ValuesAreEqual(UnityEngine.Object,UnityEngine.Object)
extern void ES3IdRefDictionary_ValuesAreEqual_mBB921B64217E471DA46E5AB91EE2F0C5ACF7B910 ();
// 0x00000453 System.Void ES3Internal.ES3IdRefDictionary::.ctor()
extern void ES3IdRefDictionary__ctor_mB5761C7BED0AA6E6000FFE256A31A8ED0F45C7B1 ();
// 0x00000454 System.Boolean ES3Internal.ES3RefIdDictionary::KeysAreEqual(UnityEngine.Object,UnityEngine.Object)
extern void ES3RefIdDictionary_KeysAreEqual_m337DB8921BD226CB54A4BD49EF209978EB1FC243 ();
// 0x00000455 System.Boolean ES3Internal.ES3RefIdDictionary::ValuesAreEqual(System.Int64,System.Int64)
extern void ES3RefIdDictionary_ValuesAreEqual_m87F54EB05F71ACDB59A3A2744EAA2AE6E0AE5C34 ();
// 0x00000456 System.Void ES3Internal.ES3RefIdDictionary::.ctor()
extern void ES3RefIdDictionary__ctor_m7314193B2379F50798FA2799985946D549011194 ();
// 0x00000457 System.Reflection.Assembly[] ES3Internal.ES3Reflection::get_Assemblies()
extern void ES3Reflection_get_Assemblies_m95641BA9F385C479DE736B7CD5E12F208C1E5A77 ();
// 0x00000458 System.Type[] ES3Internal.ES3Reflection::GetElementTypes(System.Type)
extern void ES3Reflection_GetElementTypes_mAE3793A4EADE9BF0937C291548D24452CE135B2A ();
// 0x00000459 System.Collections.Generic.List`1<System.Reflection.FieldInfo> ES3Internal.ES3Reflection::GetSerializableFields(System.Type,System.Collections.Generic.List`1<System.Reflection.FieldInfo>,System.Boolean,System.String[],System.Reflection.BindingFlags)
extern void ES3Reflection_GetSerializableFields_m8CB43337BC9CE52ED84D9D43AC32EBE897204410 ();
// 0x0000045A System.Collections.Generic.List`1<System.Reflection.PropertyInfo> ES3Internal.ES3Reflection::GetSerializableProperties(System.Type,System.Collections.Generic.List`1<System.Reflection.PropertyInfo>,System.Boolean,System.String[],System.Reflection.BindingFlags)
extern void ES3Reflection_GetSerializableProperties_m5BE897CDF1906A63C393BDE7C6EC2A05264F6A1B ();
// 0x0000045B System.Boolean ES3Internal.ES3Reflection::TypeIsSerializable(System.Type)
extern void ES3Reflection_TypeIsSerializable_m0DC2675A282B19DBC45BCB25153FB0608E758921 ();
// 0x0000045C System.Object ES3Internal.ES3Reflection::CreateInstance(System.Type)
extern void ES3Reflection_CreateInstance_m0F629B05F6270A65C1B75F61467E067E4A446D0D ();
// 0x0000045D System.Object ES3Internal.ES3Reflection::CreateInstance(System.Type,System.Object[])
extern void ES3Reflection_CreateInstance_m321DDE80D918CA837ACCE30EF2966CED7DA417E6 ();
// 0x0000045E System.Array ES3Internal.ES3Reflection::ArrayCreateInstance(System.Type,System.Int32)
extern void ES3Reflection_ArrayCreateInstance_m2C85F9809EC815E4CAAA28B1C88FF34EC3AE3DE2 ();
// 0x0000045F System.Array ES3Internal.ES3Reflection::ArrayCreateInstance(System.Type,System.Int32[])
extern void ES3Reflection_ArrayCreateInstance_m7D394D08688933F1D86AAF9A1A66AA9D6179A2E9 ();
// 0x00000460 System.Type ES3Internal.ES3Reflection::MakeGenericType(System.Type,System.Type)
extern void ES3Reflection_MakeGenericType_mDA23AC8E19770FF2BC513C93172757B7C1F9E7DD ();
// 0x00000461 ES3Internal.ES3Reflection_ES3ReflectedMember[] ES3Internal.ES3Reflection::GetSerializableMembers(System.Type,System.Boolean,System.String[])
extern void ES3Reflection_GetSerializableMembers_m6A5EB263DA4B623AC452070364BC7C295DB0D88F ();
// 0x00000462 ES3Internal.ES3Reflection_ES3ReflectedMember ES3Internal.ES3Reflection::GetES3ReflectedProperty(System.Type,System.String)
extern void ES3Reflection_GetES3ReflectedProperty_m9A290BF5A32EFD8F23A9915B42A3A92A9896358D ();
// 0x00000463 ES3Internal.ES3Reflection_ES3ReflectedMember ES3Internal.ES3Reflection::GetES3ReflectedMember(System.Type,System.String)
extern void ES3Reflection_GetES3ReflectedMember_mDC552FB4A61F95E802BB74AC75B12668B43C8171 ();
// 0x00000464 System.Collections.Generic.IList`1<T> ES3Internal.ES3Reflection::GetInstances()
// 0x00000465 System.Collections.Generic.IList`1<System.Type> ES3Internal.ES3Reflection::GetDerivedTypes(System.Type)
extern void ES3Reflection_GetDerivedTypes_m82C9908F6A233680255180A18A9A59C871945FEA ();
// 0x00000466 System.Boolean ES3Internal.ES3Reflection::IsAssignableFrom(System.Type,System.Type)
extern void ES3Reflection_IsAssignableFrom_m66D083548EB379C7917F67557F92AE1D623B047E ();
// 0x00000467 System.Type ES3Internal.ES3Reflection::GetGenericTypeDefinition(System.Type)
extern void ES3Reflection_GetGenericTypeDefinition_mF65BEA8BAA08FDC02C8F791533296DC9B532FBB8 ();
// 0x00000468 System.Type[] ES3Internal.ES3Reflection::GetGenericArguments(System.Type)
extern void ES3Reflection_GetGenericArguments_m571B8F57FC8E3550F53E25FB4C94FC5F51CEFAA5 ();
// 0x00000469 System.Int32 ES3Internal.ES3Reflection::GetArrayRank(System.Type)
extern void ES3Reflection_GetArrayRank_mFCD4995C675D8CADC2ABB1378E7B678CC34FF023 ();
// 0x0000046A System.String ES3Internal.ES3Reflection::GetAssemblyQualifiedName(System.Type)
extern void ES3Reflection_GetAssemblyQualifiedName_m14B7FF29572E5B64385BE2C8D6A0B8733DDEE299 ();
// 0x0000046B ES3Internal.ES3Reflection_ES3ReflectedMethod ES3Internal.ES3Reflection::GetMethod(System.Type,System.String,System.Type[],System.Type[])
extern void ES3Reflection_GetMethod_mFFDC5C5E30E4CE9D4B8C36C0B2823520744D2DF2 ();
// 0x0000046C System.Boolean ES3Internal.ES3Reflection::TypeIsArray(System.Type)
extern void ES3Reflection_TypeIsArray_m0D95AF29EB18D34FD4E9EC335287484B378C0C9E ();
// 0x0000046D System.Type ES3Internal.ES3Reflection::GetElementType(System.Type)
extern void ES3Reflection_GetElementType_m05C766434786917BBBCF0B2F3C442F0E48972E9C ();
// 0x0000046E System.Boolean ES3Internal.ES3Reflection::IsAbstract(System.Type)
extern void ES3Reflection_IsAbstract_m909CF9A4CE05EEBE9DFE97951DFC95C5F7A842A7 ();
// 0x0000046F System.Boolean ES3Internal.ES3Reflection::IsInterface(System.Type)
extern void ES3Reflection_IsInterface_m5832C600B7D9E36965E4746D197C145D4B06551E ();
// 0x00000470 System.Boolean ES3Internal.ES3Reflection::IsGenericType(System.Type)
extern void ES3Reflection_IsGenericType_m30C061375DC2C81AEFA9C9382490AE6730E2D6A3 ();
// 0x00000471 System.Boolean ES3Internal.ES3Reflection::IsValueType(System.Type)
extern void ES3Reflection_IsValueType_m417BB61457D7F32C55043397AB547D6F8E4B54E1 ();
// 0x00000472 System.Boolean ES3Internal.ES3Reflection::IsEnum(System.Type)
extern void ES3Reflection_IsEnum_m6D3F1BCBDC12EAA142CDBA3C737B12DB74F2A393 ();
// 0x00000473 System.Boolean ES3Internal.ES3Reflection::HasParameterlessConstructor(System.Type)
extern void ES3Reflection_HasParameterlessConstructor_mEA957F24BC52B30EFF3AF8B8663F5D8AE0975842 ();
// 0x00000474 System.Reflection.ConstructorInfo ES3Internal.ES3Reflection::GetParameterlessConstructor(System.Type)
extern void ES3Reflection_GetParameterlessConstructor_mA5EE2D389D290A028E694E1C9FAE6CE7E7CFB658 ();
// 0x00000475 System.String ES3Internal.ES3Reflection::GetShortAssemblyQualifiedName(System.Type)
extern void ES3Reflection_GetShortAssemblyQualifiedName_m0769DFFE3ACA80B1FD2B817893C8ACEE78D13033 ();
// 0x00000476 System.Reflection.PropertyInfo ES3Internal.ES3Reflection::GetProperty(System.Type,System.String)
extern void ES3Reflection_GetProperty_m49A06B87835EC1E013050F9DA43839017F086CD5 ();
// 0x00000477 System.Reflection.FieldInfo ES3Internal.ES3Reflection::GetField(System.Type,System.String)
extern void ES3Reflection_GetField_m8B7ED9BD79822B7D9A964A75C30E503D372FEB19 ();
// 0x00000478 System.Boolean ES3Internal.ES3Reflection::IsPrimitive(System.Type)
extern void ES3Reflection_IsPrimitive_m01B03272A5FCEB3F6D85DC50D6FB16890CFD74DD ();
// 0x00000479 System.Boolean ES3Internal.ES3Reflection::AttributeIsDefined(System.Reflection.MemberInfo,System.Type)
extern void ES3Reflection_AttributeIsDefined_mC8040D763AA804D5883E48F1CAFF6AB6C0D79F99 ();
// 0x0000047A System.Boolean ES3Internal.ES3Reflection::AttributeIsDefined(System.Type,System.Type)
extern void ES3Reflection_AttributeIsDefined_mB7BE1445B3B98997456B30B46F4FAC5FB5BFE426 ();
// 0x0000047B System.Boolean ES3Internal.ES3Reflection::ImplementsInterface(System.Type,System.Type)
extern void ES3Reflection_ImplementsInterface_m118CCD1E37AEC0D9856BDD3225CE5F5EFAF4CF00 ();
// 0x0000047C System.Type ES3Internal.ES3Reflection::BaseType(System.Type)
extern void ES3Reflection_BaseType_m2992AA1E7B25B9BC795F83FBBCC46A75CDE81C71 ();
// 0x0000047D System.Void ES3Internal.ES3Reflection::.cctor()
extern void ES3Reflection__cctor_mB45B691E9BFF37DA2D44CA343D80FA85D0F6D854 ();
// 0x0000047E System.Boolean ES3Internal.ES3SerializableDictionary`2::KeysAreEqual(TKey,TKey)
// 0x0000047F System.Boolean ES3Internal.ES3SerializableDictionary`2::ValuesAreEqual(TVal,TVal)
// 0x00000480 System.Void ES3Internal.ES3SerializableDictionary`2::OnBeforeSerialize()
// 0x00000481 System.Void ES3Internal.ES3SerializableDictionary`2::OnAfterDeserialize()
// 0x00000482 System.Int32 ES3Internal.ES3SerializableDictionary`2::RemoveNullValues()
// 0x00000483 System.Boolean ES3Internal.ES3SerializableDictionary`2::ChangeKey(TKey,TKey)
// 0x00000484 System.Void ES3Internal.ES3SerializableDictionary`2::.ctor()
// 0x00000485 System.Void ES3Internal.ES3BinaryReader::.ctor(System.IO.Stream,ES3Settings,System.Boolean)
extern void ES3BinaryReader__ctor_mA5254CF3C090456BAF5EEC582BB0BE1B76FDF4C0 ();
// 0x00000486 System.String ES3Internal.ES3BinaryReader::ReadPropertyName()
extern void ES3BinaryReader_ReadPropertyName_m7D3732CC7F91A3BE113CE2E272DBC85D1684C950 ();
// 0x00000487 System.Type ES3Internal.ES3BinaryReader::ReadKeyPrefix(System.Boolean)
extern void ES3BinaryReader_ReadKeyPrefix_m4AA8CE72F164A597D10F5F13948E9F6910DFD9F8 ();
// 0x00000488 System.Void ES3Internal.ES3BinaryReader::ReadKeySuffix()
extern void ES3BinaryReader_ReadKeySuffix_mBE33D588D0EB68E4AC8E92EA952C51CDE473F056 ();
// 0x00000489 System.Boolean ES3Internal.ES3BinaryReader::StartReadObject()
extern void ES3BinaryReader_StartReadObject_mBBAA5C6D9A35D95F89463F66677B1D4258B1BC12 ();
// 0x0000048A System.Void ES3Internal.ES3BinaryReader::EndReadObject()
extern void ES3BinaryReader_EndReadObject_m8DBB3AFA047AD749F8ACD94C270D437168D760E0 ();
// 0x0000048B System.Boolean ES3Internal.ES3BinaryReader::StartReadDictionary()
extern void ES3BinaryReader_StartReadDictionary_m1FF9F5D0923FB699FC4591BFA47ED9950C167DA3 ();
// 0x0000048C System.Void ES3Internal.ES3BinaryReader::EndReadDictionary()
extern void ES3BinaryReader_EndReadDictionary_mADC02A398CB37F0826FD6D5F2D3DF673F66EBD47 ();
// 0x0000048D System.Boolean ES3Internal.ES3BinaryReader::StartReadDictionaryKey()
extern void ES3BinaryReader_StartReadDictionaryKey_m0A5C9B43DD6D1B143026ACF92E885958A681B5A0 ();
// 0x0000048E System.Void ES3Internal.ES3BinaryReader::EndReadDictionaryKey()
extern void ES3BinaryReader_EndReadDictionaryKey_mEA3F22EFEF8E44F8268CDB45F00C0FA00E7DDA03 ();
// 0x0000048F System.Void ES3Internal.ES3BinaryReader::StartReadDictionaryValue()
extern void ES3BinaryReader_StartReadDictionaryValue_mB95905486B24B45F52ADB81E681FCBC29F584972 ();
// 0x00000490 System.Boolean ES3Internal.ES3BinaryReader::EndReadDictionaryValue()
extern void ES3BinaryReader_EndReadDictionaryValue_m4E5F2333D466629A53EC9F5133BDDD781522CFC9 ();
// 0x00000491 System.Boolean ES3Internal.ES3BinaryReader::StartReadCollection()
extern void ES3BinaryReader_StartReadCollection_m0DECECC7F8806A416835BD0DCF5609F7703C21A6 ();
// 0x00000492 System.Void ES3Internal.ES3BinaryReader::EndReadCollection()
extern void ES3BinaryReader_EndReadCollection_m5EF6D97A805952B02126D950948A7187718BF220 ();
// 0x00000493 System.Boolean ES3Internal.ES3BinaryReader::StartReadCollectionItem()
extern void ES3BinaryReader_StartReadCollectionItem_m0FC683EC642F7C55C1601E97C632F6EDE4F7C844 ();
// 0x00000494 System.Boolean ES3Internal.ES3BinaryReader::EndReadCollectionItem()
extern void ES3BinaryReader_EndReadCollectionItem_m9BD3DFB1568075EBCBCA4695837002A947CC5EB2 ();
// 0x00000495 System.Byte[] ES3Internal.ES3BinaryReader::ReadElement(System.Boolean)
extern void ES3BinaryReader_ReadElement_m64E69139784B7097355ACFBE195481D1E355B4A7 ();
// 0x00000496 System.Void ES3Internal.ES3BinaryReader::ReadElement(System.IO.BinaryWriter,System.Boolean)
extern void ES3BinaryReader_ReadElement_mEC416B96785BE6CB352AB96E9FF78E6202C4E093 ();
// 0x00000497 System.Int64 ES3Internal.ES3BinaryReader::Read_ref()
extern void ES3BinaryReader_Read_ref_mC6FD95A88EA7F7DE8846A7A3AA31618164D874A5 ();
// 0x00000498 System.String ES3Internal.ES3BinaryReader::Read_string()
extern void ES3BinaryReader_Read_string_m6EEB579F9045C9B4D10CE9D0F79F321F3FA71A47 ();
// 0x00000499 System.Char ES3Internal.ES3BinaryReader::Read_char()
extern void ES3BinaryReader_Read_char_mB949F97EBFD4FF9F503EC84AF52F5C5C97423D91 ();
// 0x0000049A System.Single ES3Internal.ES3BinaryReader::Read_float()
extern void ES3BinaryReader_Read_float_m0DE630FC8C7C982EA59FD11FD085F655B5CC1E26 ();
// 0x0000049B System.Int32 ES3Internal.ES3BinaryReader::Read_int()
extern void ES3BinaryReader_Read_int_m2D22C0F8AD8E9AB744CEAAB13C0B0B7D82C52656 ();
// 0x0000049C System.Boolean ES3Internal.ES3BinaryReader::Read_bool()
extern void ES3BinaryReader_Read_bool_mA22C1C5192CF276C18E14B6EE4F014928F0E8D34 ();
// 0x0000049D System.Decimal ES3Internal.ES3BinaryReader::Read_decimal()
extern void ES3BinaryReader_Read_decimal_mCE54426C00EC6AF0FB279CD52C6411022413859D ();
// 0x0000049E System.Double ES3Internal.ES3BinaryReader::Read_double()
extern void ES3BinaryReader_Read_double_mC896C9E968A5564D079A10BEB084513275A4DAF1 ();
// 0x0000049F System.Int64 ES3Internal.ES3BinaryReader::Read_long()
extern void ES3BinaryReader_Read_long_mC3D4774490F1CBD9E855F2DE6C3836EADF401AD3 ();
// 0x000004A0 System.UInt64 ES3Internal.ES3BinaryReader::Read_ulong()
extern void ES3BinaryReader_Read_ulong_m657F5D91A6B7891B85342AC8F3A371B10111F05F ();
// 0x000004A1 System.UInt32 ES3Internal.ES3BinaryReader::Read_uint()
extern void ES3BinaryReader_Read_uint_mE1D863393F5D5A5241A37D8EADE399C9A7A12018 ();
// 0x000004A2 System.Byte ES3Internal.ES3BinaryReader::Read_byte()
extern void ES3BinaryReader_Read_byte_m738496AAF2624E01849283F3D862F07DE23CDBC1 ();
// 0x000004A3 System.SByte ES3Internal.ES3BinaryReader::Read_sbyte()
extern void ES3BinaryReader_Read_sbyte_mFB7D36C9AFAD7B4B12D7BCDD82B52A2329133082 ();
// 0x000004A4 System.Int16 ES3Internal.ES3BinaryReader::Read_short()
extern void ES3BinaryReader_Read_short_mD7E1EE8350724BBFD983C1173308EF8160CC9DCE ();
// 0x000004A5 System.UInt16 ES3Internal.ES3BinaryReader::Read_ushort()
extern void ES3BinaryReader_Read_ushort_mB7F8EBF976099D293EFBF9C0607DAA5EC45B8639 ();
// 0x000004A6 System.Byte[] ES3Internal.ES3BinaryReader::Read_byteArray()
extern void ES3BinaryReader_Read_byteArray_mADAC229FD4ABD3B7760186CB029909F34ECA6A31 ();
// 0x000004A7 T ES3Internal.ES3BinaryReader::Read(ES3Types.ES3Type)
// 0x000004A8 System.Void ES3Internal.ES3BinaryReader::ReadInto(System.Object,ES3Types.ES3Type)
// 0x000004A9 System.Int32 ES3Internal.ES3BinaryReader::Read7BitEncodedInt()
extern void ES3BinaryReader_Read7BitEncodedInt_m9060182E013F05FD7FCB464B418F5EE299757B47 ();
// 0x000004AA System.Void ES3Internal.ES3BinaryReader::Dispose()
extern void ES3BinaryReader_Dispose_m9A3CA9FEE7FA74DB91C37419BD9E9FA5B7AE6B53 ();
// 0x000004AB System.Void ES3Internal.ES3JSONReader::.ctor(System.IO.Stream,ES3Settings,System.Boolean)
extern void ES3JSONReader__ctor_m17BBE79306A7F76FFD453D10534A9A71902E3948 ();
// 0x000004AC System.String ES3Internal.ES3JSONReader::ReadPropertyName()
extern void ES3JSONReader_ReadPropertyName_mE35A2A2A36E2F352B13241F9C6E5D4A2C75C90BA ();
// 0x000004AD System.Type ES3Internal.ES3JSONReader::ReadKeyPrefix(System.Boolean)
extern void ES3JSONReader_ReadKeyPrefix_m91AC1CB9CE4C4FAB57CD30EA7EE75CE3FC49FBAA ();
// 0x000004AE System.Void ES3Internal.ES3JSONReader::ReadKeySuffix()
extern void ES3JSONReader_ReadKeySuffix_m32DFE6E473510C3B9AF5672976F69E009A76A3B9 ();
// 0x000004AF System.Boolean ES3Internal.ES3JSONReader::StartReadObject()
extern void ES3JSONReader_StartReadObject_mE49545C2A93FD39830820E8E50651A85257550C5 ();
// 0x000004B0 System.Void ES3Internal.ES3JSONReader::EndReadObject()
extern void ES3JSONReader_EndReadObject_m81F9629949691C631566F353205469A258649D78 ();
// 0x000004B1 System.Boolean ES3Internal.ES3JSONReader::StartReadDictionary()
extern void ES3JSONReader_StartReadDictionary_m3046125695D5A8B2297D8E4393400D54807C3A10 ();
// 0x000004B2 System.Void ES3Internal.ES3JSONReader::EndReadDictionary()
extern void ES3JSONReader_EndReadDictionary_mC688E98CA35C68173C640E496105CBDEBEC50B33 ();
// 0x000004B3 System.Boolean ES3Internal.ES3JSONReader::StartReadDictionaryKey()
extern void ES3JSONReader_StartReadDictionaryKey_mB6574B702DC1C61A225C5B7064E8D769AD9A7158 ();
// 0x000004B4 System.Void ES3Internal.ES3JSONReader::EndReadDictionaryKey()
extern void ES3JSONReader_EndReadDictionaryKey_mDC30B3DCD746BCEB2A7128CA30B1949A72240DF0 ();
// 0x000004B5 System.Void ES3Internal.ES3JSONReader::StartReadDictionaryValue()
extern void ES3JSONReader_StartReadDictionaryValue_m49335F35F9C5B4970003772EEE339DAB4B513C7A ();
// 0x000004B6 System.Boolean ES3Internal.ES3JSONReader::EndReadDictionaryValue()
extern void ES3JSONReader_EndReadDictionaryValue_mBF27C1B659610A2B8057652A813246AF88AB0E41 ();
// 0x000004B7 System.Boolean ES3Internal.ES3JSONReader::StartReadCollection()
extern void ES3JSONReader_StartReadCollection_mE3CE6154B07D519215A21F5AC5691BB8B66FD5A9 ();
// 0x000004B8 System.Void ES3Internal.ES3JSONReader::EndReadCollection()
extern void ES3JSONReader_EndReadCollection_m16F1C75BCD481CB80A30F6338B9F087B044A8ED2 ();
// 0x000004B9 System.Boolean ES3Internal.ES3JSONReader::StartReadCollectionItem()
extern void ES3JSONReader_StartReadCollectionItem_m859937748AD554D9B85B8D6B6B7917A691A5D90F ();
// 0x000004BA System.Boolean ES3Internal.ES3JSONReader::EndReadCollectionItem()
extern void ES3JSONReader_EndReadCollectionItem_mCC631036ABA5519D7436B404E2F56E4E8DC47A55 ();
// 0x000004BB System.Void ES3Internal.ES3JSONReader::ReadString(System.IO.StreamWriter,System.Boolean)
extern void ES3JSONReader_ReadString_mEAC4B995FE66B0A4AD27CF11E917369563C01B73 ();
// 0x000004BC System.Byte[] ES3Internal.ES3JSONReader::ReadElement(System.Boolean)
extern void ES3JSONReader_ReadElement_mFEA0BF6473883663D07FCBCA5BD37415978FB70B ();
// 0x000004BD System.Char ES3Internal.ES3JSONReader::ReadOrSkipChar(System.IO.StreamWriter,System.Boolean)
extern void ES3JSONReader_ReadOrSkipChar_mFC2E6EE6592ED19B95F83E62887AC914CE9BD82A ();
// 0x000004BE System.Char ES3Internal.ES3JSONReader::ReadCharIgnoreWhitespace(System.Boolean)
extern void ES3JSONReader_ReadCharIgnoreWhitespace_m613187290BC4F2DFEE20DDBACC078D4B9F522411 ();
// 0x000004BF System.Boolean ES3Internal.ES3JSONReader::ReadNullOrCharIgnoreWhitespace(System.Char)
extern void ES3JSONReader_ReadNullOrCharIgnoreWhitespace_m34DA622081FFF53C157674198C8F51FCC3186DEB ();
// 0x000004C0 System.Char ES3Internal.ES3JSONReader::ReadCharIgnoreWhitespace(System.Char)
extern void ES3JSONReader_ReadCharIgnoreWhitespace_mB4E5F2BA0542D1AFF8C8DADD1D7601C530132180 ();
// 0x000004C1 System.Boolean ES3Internal.ES3JSONReader::ReadQuotationMarkOrNullIgnoreWhitespace()
extern void ES3JSONReader_ReadQuotationMarkOrNullIgnoreWhitespace_m3A101EEADF8978B96DE0910DBD4AC6D13C60B09D ();
// 0x000004C2 System.Char ES3Internal.ES3JSONReader::PeekCharIgnoreWhitespace(System.Char)
extern void ES3JSONReader_PeekCharIgnoreWhitespace_m486C52A1E1B66C018FD1FAEAAFA8EE585D5485C3 ();
// 0x000004C3 System.Char ES3Internal.ES3JSONReader::PeekCharIgnoreWhitespace()
extern void ES3JSONReader_PeekCharIgnoreWhitespace_m7D659DBA4244BBD5DAB7CB5C3264351CE2A3E4F5 ();
// 0x000004C4 System.Void ES3Internal.ES3JSONReader::SkipWhiteSpace()
extern void ES3JSONReader_SkipWhiteSpace_m1D8147243750528B9A7F3696FC11D160031E3996 ();
// 0x000004C5 System.Void ES3Internal.ES3JSONReader::SkipOpeningBraceOfFile()
extern void ES3JSONReader_SkipOpeningBraceOfFile_m3B2F3DF54830973042E482A1A3FA90FED5567609 ();
// 0x000004C6 System.Boolean ES3Internal.ES3JSONReader::IsWhiteSpace(System.Char)
extern void ES3JSONReader_IsWhiteSpace_mF068999016A5B3B9EB1E3BB40F823440F411D656 ();
// 0x000004C7 System.Boolean ES3Internal.ES3JSONReader::IsOpeningBrace(System.Char)
extern void ES3JSONReader_IsOpeningBrace_mC0A0982F4570833EDCE1B3BFEFA5E4DBCEE83587 ();
// 0x000004C8 System.Boolean ES3Internal.ES3JSONReader::IsEndOfValue(System.Char)
extern void ES3JSONReader_IsEndOfValue_m028234CF74097B1B0BCCB9685CB2C93697F4418B ();
// 0x000004C9 System.Boolean ES3Internal.ES3JSONReader::IsTerminator(System.Char)
extern void ES3JSONReader_IsTerminator_m2A8BBA5D74AE0C0BDFA4509512E89CFDFBC85234 ();
// 0x000004CA System.Boolean ES3Internal.ES3JSONReader::IsQuotationMark(System.Char)
extern void ES3JSONReader_IsQuotationMark_mE69B98ABAC02EC697C9FF9B3C4D0665A2A012C54 ();
// 0x000004CB System.Boolean ES3Internal.ES3JSONReader::IsEndOfStream(System.Char)
extern void ES3JSONReader_IsEndOfStream_mEDD321EAA75C90A41AC43513F83E5EF0D8371AA5 ();
// 0x000004CC System.String ES3Internal.ES3JSONReader::GetValueString()
extern void ES3JSONReader_GetValueString_mED0C1E5FF644942D52408405A8892A6D0425E359 ();
// 0x000004CD System.String ES3Internal.ES3JSONReader::Read_string()
extern void ES3JSONReader_Read_string_mA6ECF193D04F8A4199F987B278B03114014224BA ();
// 0x000004CE System.Int64 ES3Internal.ES3JSONReader::Read_ref()
extern void ES3JSONReader_Read_ref_mBC08ABAB9B5782046796C1C642102FA174C849F2 ();
// 0x000004CF System.Char ES3Internal.ES3JSONReader::Read_char()
extern void ES3JSONReader_Read_char_m670DC7FF32269B77AAFEA4945A899B450DBA594D ();
// 0x000004D0 System.Single ES3Internal.ES3JSONReader::Read_float()
extern void ES3JSONReader_Read_float_m0350D39C3DE6E73354A5DC045AFE5BD81D4AD32A ();
// 0x000004D1 System.Int32 ES3Internal.ES3JSONReader::Read_int()
extern void ES3JSONReader_Read_int_mC82580D0075D4414E09F5FBCD9132B5766382915 ();
// 0x000004D2 System.Boolean ES3Internal.ES3JSONReader::Read_bool()
extern void ES3JSONReader_Read_bool_mB38E66913DB1E0176DD81AAD6EE86F2BA0D4352B ();
// 0x000004D3 System.Decimal ES3Internal.ES3JSONReader::Read_decimal()
extern void ES3JSONReader_Read_decimal_mB0384B301953D92E23452A4DE947509CAF8B36EC ();
// 0x000004D4 System.Double ES3Internal.ES3JSONReader::Read_double()
extern void ES3JSONReader_Read_double_m83DCF0C216FE209C616DEFBE454783172B994633 ();
// 0x000004D5 System.Int64 ES3Internal.ES3JSONReader::Read_long()
extern void ES3JSONReader_Read_long_mDA1AB3B15DC0456B8C7F0BD661944CFE483C302C ();
// 0x000004D6 System.UInt64 ES3Internal.ES3JSONReader::Read_ulong()
extern void ES3JSONReader_Read_ulong_m0A0D598839DCE3007F74C69AA25EF3B0FC196E16 ();
// 0x000004D7 System.UInt32 ES3Internal.ES3JSONReader::Read_uint()
extern void ES3JSONReader_Read_uint_m237DCA3C23AB651B073BD6B3224F25C6EA594E9F ();
// 0x000004D8 System.Byte ES3Internal.ES3JSONReader::Read_byte()
extern void ES3JSONReader_Read_byte_m1DE5AA8E18D2CCAB62D3FABF7146567493A919EF ();
// 0x000004D9 System.SByte ES3Internal.ES3JSONReader::Read_sbyte()
extern void ES3JSONReader_Read_sbyte_mCE70FFF4BB6093344482640BA5E9986383F9555C ();
// 0x000004DA System.Int16 ES3Internal.ES3JSONReader::Read_short()
extern void ES3JSONReader_Read_short_m871E07D5E5C3A8DF19C7736D3E4C69CEC94D2220 ();
// 0x000004DB System.UInt16 ES3Internal.ES3JSONReader::Read_ushort()
extern void ES3JSONReader_Read_ushort_m1C4961563B5EFBEFF8E15A08075E6C9CAEC89E7A ();
// 0x000004DC System.Byte[] ES3Internal.ES3JSONReader::Read_byteArray()
extern void ES3JSONReader_Read_byteArray_mE5E1CDECD0502718666D6D74FE43C7FD7223FD4A ();
// 0x000004DD System.Void ES3Internal.ES3JSONReader::Dispose()
extern void ES3JSONReader_Dispose_mD7E105CB37536BBE761EC7439D790C31E7DD98CF ();
// 0x000004DE ES3Internal.ES3GlobalReferences ES3Internal.ES3GlobalReferences::get_Instance()
extern void ES3GlobalReferences_get_Instance_m766FE68364B27CB850DFD80C71A530F4AE6EDF8F ();
// 0x000004DF UnityEngine.Object ES3Internal.ES3GlobalReferences::Get(System.Int64)
extern void ES3GlobalReferences_Get_mE658C144069C6AEF3598ED042AABB08071FA1CC5 ();
// 0x000004E0 System.Int64 ES3Internal.ES3GlobalReferences::GetOrAdd(UnityEngine.Object)
extern void ES3GlobalReferences_GetOrAdd_m58437E89A839FAA4DD7B9F8B249AA2C54DE45779 ();
// 0x000004E1 System.Void ES3Internal.ES3GlobalReferences::RemoveInvalidKeys()
extern void ES3GlobalReferences_RemoveInvalidKeys_mB20453A7D1EB908FE55CC99500B565D8FC9B1630 ();
// 0x000004E2 System.Void ES3Internal.ES3GlobalReferences::.ctor()
extern void ES3GlobalReferences__ctor_m0390F1C12488C42FE9CB2214D9698AE4A4B8F15E ();
// 0x000004E3 System.Void ES3Internal.ES3DefaultSettings::.ctor()
extern void ES3DefaultSettings__ctor_mBA46628BE904D31DCF4EB4D0446B33A5597849CA ();
// 0x000004E4 System.Void ES3Internal.ES3FileStream::.ctor(System.String,ES3Internal.ES3FileMode,System.Int32,System.Boolean)
extern void ES3FileStream__ctor_mEACE9EA7E564180B300AC5D87EFB986A199E47D3 ();
// 0x000004E5 System.String ES3Internal.ES3FileStream::GetPath(System.String,ES3Internal.ES3FileMode)
extern void ES3FileStream_GetPath_mD4366B8DB076240B689951B8D126F4FE6D203984 ();
// 0x000004E6 System.IO.FileMode ES3Internal.ES3FileStream::GetFileMode(ES3Internal.ES3FileMode)
extern void ES3FileStream_GetFileMode_mE8548879A5E45E24A91F31B63B6144888A7AD600 ();
// 0x000004E7 System.IO.FileAccess ES3Internal.ES3FileStream::GetFileAccess(ES3Internal.ES3FileMode)
extern void ES3FileStream_GetFileAccess_m52D95CEE89B7F3BA7D44C7BA7DD4566A590027FC ();
// 0x000004E8 System.Void ES3Internal.ES3FileStream::Dispose(System.Boolean)
extern void ES3FileStream_Dispose_m3FFBB0FB4E174D4483B1BA57E241708C43EE38B6 ();
// 0x000004E9 System.Void ES3Internal.ES3PlayerPrefsStream::.ctor(System.String)
extern void ES3PlayerPrefsStream__ctor_m6EFEE86C38048737A87198A2F7F98FEF265FF3AC ();
// 0x000004EA System.Void ES3Internal.ES3PlayerPrefsStream::.ctor(System.String,System.Int32,System.Boolean)
extern void ES3PlayerPrefsStream__ctor_mA22DA68788346E7C730FCFC5CD854329508AD0CA ();
// 0x000004EB System.Byte[] ES3Internal.ES3PlayerPrefsStream::GetData(System.String,System.Boolean)
extern void ES3PlayerPrefsStream_GetData_m12E47B97AA884204E117EFDEE02C6454A1B99FD3 ();
// 0x000004EC System.Void ES3Internal.ES3PlayerPrefsStream::Dispose(System.Boolean)
extern void ES3PlayerPrefsStream_Dispose_m70F322AE1B857EFB7565AD68EB2EB24F94BE4C98 ();
// 0x000004ED System.Boolean ES3Internal.ES3ResourcesStream::get_Exists()
extern void ES3ResourcesStream_get_Exists_m3FBA3F32F22290B0BAC766889D766E211B99A43C ();
// 0x000004EE System.Void ES3Internal.ES3ResourcesStream::.ctor(System.String)
extern void ES3ResourcesStream__ctor_mD26ECC2BB989E4347638D30BDFA108BA0326F627 ();
// 0x000004EF System.Byte[] ES3Internal.ES3ResourcesStream::GetData(System.String)
extern void ES3ResourcesStream_GetData_m30D607DF8947B04CE01095A9317BBE0437B19A6C ();
// 0x000004F0 System.Void ES3Internal.ES3ResourcesStream::Dispose(System.Boolean)
extern void ES3ResourcesStream_Dispose_m86982FCA7263346F686EFA558B29AB6EF698DCA1 ();
// 0x000004F1 System.IO.Stream ES3Internal.ES3Stream::CreateStream(ES3Settings,ES3Internal.ES3FileMode)
extern void ES3Stream_CreateStream_mFDC6B85322CEBE34237BB1462A7652ABF7E2269E ();
// 0x000004F2 System.IO.Stream ES3Internal.ES3Stream::CreateStream(System.IO.Stream,ES3Settings,ES3Internal.ES3FileMode)
extern void ES3Stream_CreateStream_m0B1C3520964988A81086D1A3B145D17AB31AC04B ();
// 0x000004F3 System.Void ES3Internal.ES3Stream::CopyTo(System.IO.Stream,System.IO.Stream)
extern void ES3Stream_CopyTo_m941AE086EE5848C4C5A7444085221F1424A4E52B ();
// 0x000004F4 System.Void ES3Internal.ES3Member::.ctor(System.String,System.Type,System.Boolean)
extern void ES3Member__ctor_mA3BBD7A646ACC43FEBF7734118CB1EDA967951F7 ();
// 0x000004F5 System.Void ES3Internal.ES3Member::.ctor(ES3Internal.ES3Reflection_ES3ReflectedMember)
extern void ES3Member__ctor_mE663398D3D9F366EA5FBFD57334DD8DBCD0BBC1A ();
// 0x000004F6 ES3Types.ES3Type ES3Internal.ES3TypeMgr::GetOrCreateES3Type(System.Type,System.Boolean)
extern void ES3TypeMgr_GetOrCreateES3Type_mB12E0FD8E92BFCEFD9D6814B428A1ABCF22F7983 ();
// 0x000004F7 ES3Types.ES3Type ES3Internal.ES3TypeMgr::GetES3Type(System.Type)
extern void ES3TypeMgr_GetES3Type_m9777E8D44A268633ED1FAB91C70C985568D77E35 ();
// 0x000004F8 System.Void ES3Internal.ES3TypeMgr::Add(System.Type,ES3Types.ES3Type)
extern void ES3TypeMgr_Add_m99EE8C7C46508C087CEB5C32C255F8B36E8DD23F ();
// 0x000004F9 ES3Types.ES3Type ES3Internal.ES3TypeMgr::CreateES3Type(System.Type,System.Boolean)
extern void ES3TypeMgr_CreateES3Type_m6F10A6BC4B879AF12C24DE29A406C7A21E644477 ();
// 0x000004FA System.Void ES3Internal.ES3TypeMgr::Init()
extern void ES3TypeMgr_Init_mA59BACCA4C7636A2DB8779EAF61FD7F57F20CBB6 ();
// 0x000004FB System.Void ES3Internal.ES3TypeMgr::.cctor()
extern void ES3TypeMgr__cctor_m29195A7EF386968BB04AE098C2549C9B23B423CB ();
// 0x000004FC System.Single ES3Internal.ES3WebClass::get_uploadProgress()
extern void ES3WebClass_get_uploadProgress_m829D20BFDC7EE47B8124615FDCD442BEFE248DCB ();
// 0x000004FD System.Single ES3Internal.ES3WebClass::get_downloadProgress()
extern void ES3WebClass_get_downloadProgress_mFE19F25A4A34CFA036E8A68C6331A086611FAB3C ();
// 0x000004FE System.Boolean ES3Internal.ES3WebClass::get_isError()
extern void ES3WebClass_get_isError_m94BAE9F2CC51A9AC95AA171A3B639FD4CF9833A1 ();
// 0x000004FF System.Void ES3Internal.ES3WebClass::.ctor(System.String,System.String)
extern void ES3WebClass__ctor_m89BF498B9078CBA7732974C1E242B88FB1AB6B81 ();
// 0x00000500 System.Void ES3Internal.ES3WebClass::AddPOSTField(System.String,System.String)
extern void ES3WebClass_AddPOSTField_m4F00FA4F204A6427B0AC21DEBDD7649F369E7B70 ();
// 0x00000501 System.String ES3Internal.ES3WebClass::GetUser(System.String,System.String)
extern void ES3WebClass_GetUser_m173A42EE49D85BF99EFC839529588DC460BD9801 ();
// 0x00000502 UnityEngine.WWWForm ES3Internal.ES3WebClass::CreateWWWForm()
extern void ES3WebClass_CreateWWWForm_m827392153AF52E4CAE56D10AEF9D6990F53D22D8 ();
// 0x00000503 System.Boolean ES3Internal.ES3WebClass::HandleError(UnityEngine.Networking.UnityWebRequest,System.Boolean)
extern void ES3WebClass_HandleError_m4E83B48A57CED39B45A208CB3FBBEA9F60112868 ();
// 0x00000504 System.Collections.IEnumerator ES3Internal.ES3WebClass::SendWebRequest(UnityEngine.Networking.UnityWebRequest)
extern void ES3WebClass_SendWebRequest_m154095E6ECB3E73C0C98D467F8D57D946F853E5D ();
// 0x00000505 System.Void ES3Internal.ES3WebClass::Reset()
extern void ES3WebClass_Reset_mA9CF44B06F57729C51151B33C550B642E24DDC2A ();
// 0x00000506 ES3Internal.ES3SpecialByte ES3Internal.ES3Binary::TypeToByte(System.Type)
extern void ES3Binary_TypeToByte_m15C22426AB52571E1322EA4009904C5D95634AD1 ();
// 0x00000507 System.Type ES3Internal.ES3Binary::ByteToType(ES3Internal.ES3SpecialByte)
extern void ES3Binary_ByteToType_mED465B8A24191A11568B809B9C658B5C0DD0019A ();
// 0x00000508 System.Type ES3Internal.ES3Binary::ByteToType(System.Byte)
extern void ES3Binary_ByteToType_mBAF5026EF62837CFC5BC5E089C6E4CA52A15D5FE ();
// 0x00000509 System.Boolean ES3Internal.ES3Binary::IsPrimitive(ES3Internal.ES3SpecialByte)
extern void ES3Binary_IsPrimitive_mCC6E86CCB6D25FED0FFACA48870DE18CC9816D82 ();
// 0x0000050A System.Void ES3Internal.ES3Binary::.cctor()
extern void ES3Binary__cctor_m32D22BC51927075991C544FEAD48D5DEA8671AAA ();
// 0x0000050B System.Void ES3Internal.ES3BinaryWriter::.ctor(System.IO.Stream,ES3Settings)
extern void ES3BinaryWriter__ctor_mCD32D7C605DC6410A307A0E05D2314161EE9BD20 ();
// 0x0000050C System.Void ES3Internal.ES3BinaryWriter::.ctor(System.IO.Stream,ES3Settings,System.Boolean,System.Boolean)
extern void ES3BinaryWriter__ctor_mDFB75C3395A4FC19D452FFC8899028AE8698CC07 ();
// 0x0000050D System.Void ES3Internal.ES3BinaryWriter::Write(System.String,System.Type,System.Byte[])
extern void ES3BinaryWriter_Write_m23800B7118246269FEB1433C095BF0803B19FFA8 ();
// 0x0000050E System.Void ES3Internal.ES3BinaryWriter::Write(System.Type,System.String,System.Object)
extern void ES3BinaryWriter_Write_m4B21A1D7822BCA3AAE719807B711847BDC975C36 ();
// 0x0000050F System.Void ES3Internal.ES3BinaryWriter::WriteProperty(System.String,System.Object,ES3_ReferenceMode)
extern void ES3BinaryWriter_WriteProperty_m71E6364DFB7F86D3DF36C77B35A2F2F6F69684E6 ();
// 0x00000510 System.Void ES3Internal.ES3BinaryWriter::WriteProperty(System.String,System.Object,ES3Types.ES3Type,ES3_ReferenceMode)
extern void ES3BinaryWriter_WriteProperty_m04FAB9385FF9E76FED6FE914CC95FBDD6363AC7E ();
// 0x00000511 System.Void ES3Internal.ES3BinaryWriter::WritePropertyByRef(System.String,UnityEngine.Object)
extern void ES3BinaryWriter_WritePropertyByRef_mE48FEBCECE097B7E6DD5E264DE2FB12D89CBD8BC ();
// 0x00000512 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Int32)
extern void ES3BinaryWriter_WritePrimitive_m32638A0EAEE4332CF14308616625D328D8E9EEF8 ();
// 0x00000513 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Single)
extern void ES3BinaryWriter_WritePrimitive_m91399BF01D0E9B2C0DA5F75BE53D7D4DADCC870A ();
// 0x00000514 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Boolean)
extern void ES3BinaryWriter_WritePrimitive_m5A649AB0E886C74CB13A1C88DDBA0B38D5E52A88 ();
// 0x00000515 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Decimal)
extern void ES3BinaryWriter_WritePrimitive_m2934092A51C31D82D5A596F45900A1D6A46B4D4A ();
// 0x00000516 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Double)
extern void ES3BinaryWriter_WritePrimitive_mA494B6C45F97AA5FF3639AD018586432ABE7DB2B ();
// 0x00000517 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Int64)
extern void ES3BinaryWriter_WritePrimitive_m744285FC58F1402AA216EB15E7C6734C081B30B6 ();
// 0x00000518 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.UInt64)
extern void ES3BinaryWriter_WritePrimitive_m44A986B67AA170C0CAE06760D448B7F0DFD52BEF ();
// 0x00000519 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.UInt32)
extern void ES3BinaryWriter_WritePrimitive_mE95088D5CA8F162F6B6DF45E3C2CD6C7B78B98F5 ();
// 0x0000051A System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Byte)
extern void ES3BinaryWriter_WritePrimitive_m82398FF7C812FDA39545E8F47B835F8C81BF6F5F ();
// 0x0000051B System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.SByte)
extern void ES3BinaryWriter_WritePrimitive_mA180BAC92CB41AF61850EF69B75FF9791238C119 ();
// 0x0000051C System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Int16)
extern void ES3BinaryWriter_WritePrimitive_m6548A9B2E4CE55E4D9C939F11EC19BF7E7146935 ();
// 0x0000051D System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.UInt16)
extern void ES3BinaryWriter_WritePrimitive_m9982ADCC03014EAB18D010D5F42DE21EFDEBE340 ();
// 0x0000051E System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Char)
extern void ES3BinaryWriter_WritePrimitive_m370363C5F0FD1035567B6ACE47C731D7D047544B ();
// 0x0000051F System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.Byte[])
extern void ES3BinaryWriter_WritePrimitive_m6202C1537EE619D997F194A45E278C4EDE858BAF ();
// 0x00000520 System.Void ES3Internal.ES3BinaryWriter::WritePrimitive(System.String)
extern void ES3BinaryWriter_WritePrimitive_m5FE89041CD48EE26571414E1D532C1388DAF8657 ();
// 0x00000521 System.Void ES3Internal.ES3BinaryWriter::Write7BitEncodedInt(System.Int32)
extern void ES3BinaryWriter_Write7BitEncodedInt_m67C13143C874F2E498933131DA6FECDEC7A9CC88 ();
// 0x00000522 System.Void ES3Internal.ES3BinaryWriter::WriteNull()
extern void ES3BinaryWriter_WriteNull_m2B682B1F0A321FB92E237BEF783CCC405FDCB729 ();
// 0x00000523 System.Void ES3Internal.ES3BinaryWriter::WriteRawProperty(System.String,System.Byte[])
extern void ES3BinaryWriter_WriteRawProperty_m513EB75D1C8C9717C3FEA4D7BB763B116E909271 ();
// 0x00000524 System.Void ES3Internal.ES3BinaryWriter::StartWriteFile()
extern void ES3BinaryWriter_StartWriteFile_m407CA8E9C9297091A471EEE6FD6BAAEDCE57BD7E ();
// 0x00000525 System.Void ES3Internal.ES3BinaryWriter::EndWriteFile()
extern void ES3BinaryWriter_EndWriteFile_m87EF1E8E4609CE3094552BF16D07A9106E9E6008 ();
// 0x00000526 System.Void ES3Internal.ES3BinaryWriter::StartWriteProperty(System.String)
extern void ES3BinaryWriter_StartWriteProperty_mE63104F2B0B69363085483393A54D3B93C68E616 ();
// 0x00000527 System.Void ES3Internal.ES3BinaryWriter::EndWriteProperty(System.String)
extern void ES3BinaryWriter_EndWriteProperty_m64AF2449E3B2C038A4914E2C41D19CE56C495442 ();
// 0x00000528 System.Void ES3Internal.ES3BinaryWriter::StartWriteObject(System.String)
extern void ES3BinaryWriter_StartWriteObject_m76C0130D84EADF77255AA11E89C517E8362ADDDC ();
// 0x00000529 System.Void ES3Internal.ES3BinaryWriter::EndWriteObject(System.String)
extern void ES3BinaryWriter_EndWriteObject_mB5E7E0201B5405F34829296D892BF6F3BEBF17B8 ();
// 0x0000052A System.Void ES3Internal.ES3BinaryWriter::StartWriteCollection()
extern void ES3BinaryWriter_StartWriteCollection_m859866296D67BB4F3E2BB38A8F19DCB78776C44C ();
// 0x0000052B System.Void ES3Internal.ES3BinaryWriter::EndWriteCollection()
extern void ES3BinaryWriter_EndWriteCollection_mE7F1F47176AD668099E8A590E33713D1C7D04502 ();
// 0x0000052C System.Void ES3Internal.ES3BinaryWriter::StartWriteCollectionItem(System.Int32)
extern void ES3BinaryWriter_StartWriteCollectionItem_m47137F7DE32B2B46DC483569966AFDAC645246CA ();
// 0x0000052D System.Void ES3Internal.ES3BinaryWriter::EndWriteCollectionItem(System.Int32)
extern void ES3BinaryWriter_EndWriteCollectionItem_m87CE09614BD2C80D65AB08E94761268C469D94D7 ();
// 0x0000052E System.Void ES3Internal.ES3BinaryWriter::StartWriteDictionary()
extern void ES3BinaryWriter_StartWriteDictionary_m99BD8879438466BC8A86C0C9307D2AE1DEFE34AE ();
// 0x0000052F System.Void ES3Internal.ES3BinaryWriter::EndWriteDictionary()
extern void ES3BinaryWriter_EndWriteDictionary_m9EA46EC3CC57E2053F343AE19C302DECD9FBF778 ();
// 0x00000530 System.Void ES3Internal.ES3BinaryWriter::StartWriteDictionaryKey(System.Int32)
extern void ES3BinaryWriter_StartWriteDictionaryKey_m565689C1B56196E3B7BA95D6EC07180C8E31CD76 ();
// 0x00000531 System.Void ES3Internal.ES3BinaryWriter::EndWriteDictionaryKey(System.Int32)
extern void ES3BinaryWriter_EndWriteDictionaryKey_m2D38FEBE3B11B4E83353A4ADC142BC0950150BCA ();
// 0x00000532 System.Void ES3Internal.ES3BinaryWriter::StartWriteDictionaryValue(System.Int32)
extern void ES3BinaryWriter_StartWriteDictionaryValue_m9F86A7405950E505F3C16B67CDFCC59D528C4436 ();
// 0x00000533 System.Void ES3Internal.ES3BinaryWriter::EndWriteDictionaryValue(System.Int32)
extern void ES3BinaryWriter_EndWriteDictionaryValue_m1F6AFBDD30BCAFBB6E629E60A090701CD2C4CA59 ();
// 0x00000534 System.Void ES3Internal.ES3BinaryWriter::Dispose()
extern void ES3BinaryWriter_Dispose_m7874F6A77B8D6AA26B145B60CAD79A2F892A9467 ();
// 0x00000535 System.Void ES3Internal.ES3CacheWriter::.ctor(ES3Settings,System.Boolean,System.Boolean)
extern void ES3CacheWriter__ctor_mAF0394327812069DF8953208C8303548D34823F1 ();
// 0x00000536 System.Void ES3Internal.ES3CacheWriter::Write(System.String,System.Object)
// 0x00000537 System.Void ES3Internal.ES3CacheWriter::Write(System.String,System.Type,System.Byte[])
extern void ES3CacheWriter_Write_m3A637364239D8C34D32B0F9F4710949BBBE163FE ();
// 0x00000538 System.Void ES3Internal.ES3CacheWriter::Write(System.Type,System.String,System.Object)
extern void ES3CacheWriter_Write_mDE5FB776751EE78508222F1934B758A4158D5FE1 ();
// 0x00000539 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Int32)
extern void ES3CacheWriter_WritePrimitive_m9FD3E4CC7B137D360B53FE1F0412EB88B0A1FA47 ();
// 0x0000053A System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Single)
extern void ES3CacheWriter_WritePrimitive_mF98BBEC778EB29E0F30064816205F1DFDB7E4132 ();
// 0x0000053B System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Boolean)
extern void ES3CacheWriter_WritePrimitive_mC10FCC7DBF1FCD88C30196F46AC669712CC96324 ();
// 0x0000053C System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Decimal)
extern void ES3CacheWriter_WritePrimitive_m2216BF4089A528BDA565331CE7695B95BDED9E73 ();
// 0x0000053D System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Double)
extern void ES3CacheWriter_WritePrimitive_m7565F04B31002AB5501EB4CCC4E02194B787B0B4 ();
// 0x0000053E System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Int64)
extern void ES3CacheWriter_WritePrimitive_m2709FB7E8387D6621493C8BF9E7DB520998DDAE9 ();
// 0x0000053F System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.UInt64)
extern void ES3CacheWriter_WritePrimitive_mE258E5EFE472D4A94A9037FFDC6121E8495401B7 ();
// 0x00000540 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.UInt32)
extern void ES3CacheWriter_WritePrimitive_mBEE7E75447B691BBA3C94A726801C282545B886E ();
// 0x00000541 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Byte)
extern void ES3CacheWriter_WritePrimitive_mFDA10194DD6C69A380C4319BC7C6EFBDB28836A9 ();
// 0x00000542 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.SByte)
extern void ES3CacheWriter_WritePrimitive_mBF464BA1645F726264AF5199D41B9B0BA3C2226D ();
// 0x00000543 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Int16)
extern void ES3CacheWriter_WritePrimitive_mD7C4D126FCA06CADE7FF85A28663B8F4D57B4CE7 ();
// 0x00000544 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.UInt16)
extern void ES3CacheWriter_WritePrimitive_m4179168292BED2AEF688D0C19B8A27ED68FB08B6 ();
// 0x00000545 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Char)
extern void ES3CacheWriter_WritePrimitive_mD8D85B826E90C4E3386E486F59481CE4FADDCAF6 ();
// 0x00000546 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.Byte[])
extern void ES3CacheWriter_WritePrimitive_m33805F4F58E92CCBC8D452F654BCD6A252FFD515 ();
// 0x00000547 System.Void ES3Internal.ES3CacheWriter::WritePrimitive(System.String)
extern void ES3CacheWriter_WritePrimitive_m7E8C628C2EF9340894CD8AD388AD4F7646067210 ();
// 0x00000548 System.Void ES3Internal.ES3CacheWriter::WriteNull()
extern void ES3CacheWriter_WriteNull_mEF2257501C552A13CBFD17A0EA7EC5482D12E345 ();
// 0x00000549 System.Boolean ES3Internal.ES3CacheWriter::CharacterRequiresEscaping(System.Char)
extern void ES3CacheWriter_CharacterRequiresEscaping_mC32069913D2E70BDBD1314C831EEE44330076FA9 ();
// 0x0000054A System.Void ES3Internal.ES3CacheWriter::WriteCommaIfRequired()
extern void ES3CacheWriter_WriteCommaIfRequired_mE834D5046882E87E5FA6AA90E3CF86EDBFEBC618 ();
// 0x0000054B System.Void ES3Internal.ES3CacheWriter::WriteRawProperty(System.String,System.Byte[])
extern void ES3CacheWriter_WriteRawProperty_m1C0EF635BBDF6AE5516CD549CF25C6410BE2175F ();
// 0x0000054C System.Void ES3Internal.ES3CacheWriter::StartWriteFile()
extern void ES3CacheWriter_StartWriteFile_mF401D10B8B50FEF784EE6645C435BCC017E33F00 ();
// 0x0000054D System.Void ES3Internal.ES3CacheWriter::EndWriteFile()
extern void ES3CacheWriter_EndWriteFile_m8C9FCA9BFB1D89A208FD850B622C8BA24899B12C ();
// 0x0000054E System.Void ES3Internal.ES3CacheWriter::StartWriteProperty(System.String)
extern void ES3CacheWriter_StartWriteProperty_m62F85B781903DD8AE585BA93832EA21E1CA61DFD ();
// 0x0000054F System.Void ES3Internal.ES3CacheWriter::EndWriteProperty(System.String)
extern void ES3CacheWriter_EndWriteProperty_m5F3D987C16C4164C4F4B0C60A1152D01B6AC3920 ();
// 0x00000550 System.Void ES3Internal.ES3CacheWriter::StartWriteObject(System.String)
extern void ES3CacheWriter_StartWriteObject_mC0B025FE509B9CE38BE13F33ABE8C2EC44259C4B ();
// 0x00000551 System.Void ES3Internal.ES3CacheWriter::EndWriteObject(System.String)
extern void ES3CacheWriter_EndWriteObject_mFF1AF66972DE025D13ECFAE4C1B3ED34537D4331 ();
// 0x00000552 System.Void ES3Internal.ES3CacheWriter::StartWriteCollection()
extern void ES3CacheWriter_StartWriteCollection_mB84F5D82F29DB2F1B1324F460F07C31FCDFBBF8F ();
// 0x00000553 System.Void ES3Internal.ES3CacheWriter::EndWriteCollection()
extern void ES3CacheWriter_EndWriteCollection_m9622711518FCBE71595C4FE4FECF84CD9A9CB253 ();
// 0x00000554 System.Void ES3Internal.ES3CacheWriter::StartWriteCollectionItem(System.Int32)
extern void ES3CacheWriter_StartWriteCollectionItem_m24A15597B7B391CB98C8FFFB49988E625FD2EF22 ();
// 0x00000555 System.Void ES3Internal.ES3CacheWriter::EndWriteCollectionItem(System.Int32)
extern void ES3CacheWriter_EndWriteCollectionItem_m4C193478B00B42779A0BB21CC8A2D6EBA87CCB7B ();
// 0x00000556 System.Void ES3Internal.ES3CacheWriter::StartWriteDictionary()
extern void ES3CacheWriter_StartWriteDictionary_m4CB81A1D15F09AEEF01A022D47C2C35BEED58CB9 ();
// 0x00000557 System.Void ES3Internal.ES3CacheWriter::EndWriteDictionary()
extern void ES3CacheWriter_EndWriteDictionary_mA50077F206D72AF0FD50F5282153A0AF2D4100F7 ();
// 0x00000558 System.Void ES3Internal.ES3CacheWriter::StartWriteDictionaryKey(System.Int32)
extern void ES3CacheWriter_StartWriteDictionaryKey_mE50497F85F915C8FBAE1845F1343D9ECBFA9E3F5 ();
// 0x00000559 System.Void ES3Internal.ES3CacheWriter::EndWriteDictionaryKey(System.Int32)
extern void ES3CacheWriter_EndWriteDictionaryKey_m30EA1080F7F54DE3FF767A62649BC05E36E08E37 ();
// 0x0000055A System.Void ES3Internal.ES3CacheWriter::StartWriteDictionaryValue(System.Int32)
extern void ES3CacheWriter_StartWriteDictionaryValue_m84CA400E0395F9E71134C762C3BE94F3AFAEF695 ();
// 0x0000055B System.Void ES3Internal.ES3CacheWriter::EndWriteDictionaryValue(System.Int32)
extern void ES3CacheWriter_EndWriteDictionaryValue_m2CBF9F3968CDD0A084F79D1A1EEBAE1374DD1169 ();
// 0x0000055C System.Void ES3Internal.ES3CacheWriter::Dispose()
extern void ES3CacheWriter_Dispose_mC71481FC1BC698818532CEBCE5A92A0B0A1B3C6D ();
// 0x0000055D System.Void ES3Internal.ES3JSONWriter::.ctor(System.IO.Stream,ES3Settings)
extern void ES3JSONWriter__ctor_m45591E20452A0FAAA92DE008CA5AC909472B69EF ();
// 0x0000055E System.Void ES3Internal.ES3JSONWriter::.ctor(System.IO.Stream,ES3Settings,System.Boolean,System.Boolean)
extern void ES3JSONWriter__ctor_mF094A8A112A6456314425DB40081ABDA81ADE640 ();
// 0x0000055F System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Int32)
extern void ES3JSONWriter_WritePrimitive_m61CDCE9FEB51B59C14BF7701AFED4483BC37897D ();
// 0x00000560 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Single)
extern void ES3JSONWriter_WritePrimitive_m78DCBB21C01934C6BDB9A66081CEB2853D2E217E ();
// 0x00000561 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Boolean)
extern void ES3JSONWriter_WritePrimitive_mE706727663FE7A4E64813E439B60B5D5FBC3C770 ();
// 0x00000562 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Decimal)
extern void ES3JSONWriter_WritePrimitive_mACA27E18EF546576E037D4FF8EAE33CD5F8D3A0B ();
// 0x00000563 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Double)
extern void ES3JSONWriter_WritePrimitive_m26AD40B7F3E2CC978EA4DFD9FBCD0C8E9F7CB668 ();
// 0x00000564 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Int64)
extern void ES3JSONWriter_WritePrimitive_mB81DF912883CDF4AFE1FCF774DA0F09A7905D4D5 ();
// 0x00000565 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.UInt64)
extern void ES3JSONWriter_WritePrimitive_m3EFE07EAD33AECAF701FF8814B01A6406F2785F6 ();
// 0x00000566 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.UInt32)
extern void ES3JSONWriter_WritePrimitive_mF1DC896A95FE1CA7A5A1D25AB3669EEA36CE1AE1 ();
// 0x00000567 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Byte)
extern void ES3JSONWriter_WritePrimitive_m8141231866DAB515D36614EBF3B902BF1BBEB6B7 ();
// 0x00000568 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.SByte)
extern void ES3JSONWriter_WritePrimitive_m44C9B844A6CA64D769799F3BCA99FFD1DB3F448A ();
// 0x00000569 System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Int16)
extern void ES3JSONWriter_WritePrimitive_mA80EDB378C7C6A3B05756BC02B5231B6652923A6 ();
// 0x0000056A System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.UInt16)
extern void ES3JSONWriter_WritePrimitive_mF775B7D75A96B9BEB9C5BA8D23DEB11180CF822F ();
// 0x0000056B System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Char)
extern void ES3JSONWriter_WritePrimitive_m86BC936955EBAFCEAB4E9CFD05FAE9086DC789DC ();
// 0x0000056C System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.Byte[])
extern void ES3JSONWriter_WritePrimitive_m3075CF381ABBC1ADE3A607D24EB0147425699D73 ();
// 0x0000056D System.Void ES3Internal.ES3JSONWriter::WritePrimitive(System.String)
extern void ES3JSONWriter_WritePrimitive_m72A3614C5905C12CBEC7059DD1E7C837312D76BC ();
// 0x0000056E System.Void ES3Internal.ES3JSONWriter::WriteNull()
extern void ES3JSONWriter_WriteNull_m3458C81460E2FA2E6DC3BF0983E6A9AEDC86BCEA ();
// 0x0000056F System.Boolean ES3Internal.ES3JSONWriter::CharacterRequiresEscaping(System.Char)
extern void ES3JSONWriter_CharacterRequiresEscaping_m89763B269EF6A293C9A880C0FAD1FA0BD52914D8 ();
// 0x00000570 System.Void ES3Internal.ES3JSONWriter::WriteCommaIfRequired()
extern void ES3JSONWriter_WriteCommaIfRequired_m0544390F30EC0B272B576DEF99A675AA9423CB9D ();
// 0x00000571 System.Void ES3Internal.ES3JSONWriter::WriteRawProperty(System.String,System.Byte[])
extern void ES3JSONWriter_WriteRawProperty_mFB5673B8CC4DF8F6F4BF60B2FD91112E10EB4C01 ();
// 0x00000572 System.Void ES3Internal.ES3JSONWriter::StartWriteFile()
extern void ES3JSONWriter_StartWriteFile_m5D5BA46D3FB313BFDE8D1FBB17617FC11548F89D ();
// 0x00000573 System.Void ES3Internal.ES3JSONWriter::EndWriteFile()
extern void ES3JSONWriter_EndWriteFile_mE8F43869E2C94DFB42771B8CC439E3EBF57AC8CD ();
// 0x00000574 System.Void ES3Internal.ES3JSONWriter::StartWriteProperty(System.String)
extern void ES3JSONWriter_StartWriteProperty_mD4E597E7BCA9B56050670108473CD7DFBFC6FBF6 ();
// 0x00000575 System.Void ES3Internal.ES3JSONWriter::EndWriteProperty(System.String)
extern void ES3JSONWriter_EndWriteProperty_mEBCB3F8D4C68150A2BFEAFBE445B1BDB158C29E6 ();
// 0x00000576 System.Void ES3Internal.ES3JSONWriter::StartWriteObject(System.String)
extern void ES3JSONWriter_StartWriteObject_mD817BFA247573CCB3EEDFFC1C5C3111C15D11A7D ();
// 0x00000577 System.Void ES3Internal.ES3JSONWriter::EndWriteObject(System.String)
extern void ES3JSONWriter_EndWriteObject_m4832C63F19827E166ACB3E6FA6541201B738397D ();
// 0x00000578 System.Void ES3Internal.ES3JSONWriter::StartWriteCollection()
extern void ES3JSONWriter_StartWriteCollection_mC2380EC023B8856B06A419219769A517E4CFBFDF ();
// 0x00000579 System.Void ES3Internal.ES3JSONWriter::EndWriteCollection()
extern void ES3JSONWriter_EndWriteCollection_m49C5FDD746173EA44D17DEBC91152A17F4A36F66 ();
// 0x0000057A System.Void ES3Internal.ES3JSONWriter::StartWriteCollectionItem(System.Int32)
extern void ES3JSONWriter_StartWriteCollectionItem_mEC8B0AB3632E9DF0EA9F9829B587F6E7F758C6E3 ();
// 0x0000057B System.Void ES3Internal.ES3JSONWriter::EndWriteCollectionItem(System.Int32)
extern void ES3JSONWriter_EndWriteCollectionItem_m16ED92B5E285A44467CAF861235EC08FC24CB4EE ();
// 0x0000057C System.Void ES3Internal.ES3JSONWriter::StartWriteDictionary()
extern void ES3JSONWriter_StartWriteDictionary_m5F1AD1666D99627DCCE164029338C4D7DD289CB2 ();
// 0x0000057D System.Void ES3Internal.ES3JSONWriter::EndWriteDictionary()
extern void ES3JSONWriter_EndWriteDictionary_m9333430C80ECEC683604A8B80EDE44310C4994C8 ();
// 0x0000057E System.Void ES3Internal.ES3JSONWriter::StartWriteDictionaryKey(System.Int32)
extern void ES3JSONWriter_StartWriteDictionaryKey_mE80668823B753404A9664867BC8F9D4563946958 ();
// 0x0000057F System.Void ES3Internal.ES3JSONWriter::EndWriteDictionaryKey(System.Int32)
extern void ES3JSONWriter_EndWriteDictionaryKey_m154079C6F41DF8F1F04E0664001794C3DF6D9A9E ();
// 0x00000580 System.Void ES3Internal.ES3JSONWriter::StartWriteDictionaryValue(System.Int32)
extern void ES3JSONWriter_StartWriteDictionaryValue_mAEA527056D20E3C292F02E6FFADD16C8BFC401EC ();
// 0x00000581 System.Void ES3Internal.ES3JSONWriter::EndWriteDictionaryValue(System.Int32)
extern void ES3JSONWriter_EndWriteDictionaryValue_mD40DBBFB9D9819728682085CFD1B59A73B310C7E ();
// 0x00000582 System.Void ES3Internal.ES3JSONWriter::Dispose()
extern void ES3JSONWriter_Dispose_mB9EC5937CE8DD248205E615FCFAEE910E55C2462 ();
// 0x00000583 System.Void ES3Internal.ES3JSONWriter::WriteNewlineAndTabs()
extern void ES3JSONWriter_WriteNewlineAndTabs_mBDBEA833734A66C3D2F33369714046121AC83178 ();
// 0x00000584 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m9FD9D5C30B989EC8C2B53EC7131DE8D4320E5813 ();
// 0x00000585 System.Void ES3Spreadsheet_Index::.ctor(System.Int32,System.Int32)
extern void Index__ctor_m7E89106F6BE1B37FB8E74AA2451A9795CA38A04E_AdjustorThunk ();
// 0x00000586 System.Void ES3Reader_ES3ReaderPropertyEnumerator::.ctor(ES3Reader)
extern void ES3ReaderPropertyEnumerator__ctor_mF089B29C6D5707CB3EE1D2AA408D91B005558421 ();
// 0x00000587 System.Collections.IEnumerator ES3Reader_ES3ReaderPropertyEnumerator::GetEnumerator()
extern void ES3ReaderPropertyEnumerator_GetEnumerator_m5BECF82CB1697BE11395A321D30EB015FA9D3095 ();
// 0x00000588 System.Void ES3Reader_ES3ReaderRawEnumerator::.ctor(ES3Reader)
extern void ES3ReaderRawEnumerator__ctor_m97B888FCB98A953D9B0F3195CF983712693B94C6 ();
// 0x00000589 System.Collections.IEnumerator ES3Reader_ES3ReaderRawEnumerator::GetEnumerator()
extern void ES3ReaderRawEnumerator_GetEnumerator_mA90016431197C25720481FAA8B7099B9AB163544 ();
// 0x0000058A System.Void ES3Cloud_<Sync>d__18::.ctor(System.Int32)
extern void U3CSyncU3Ed__18__ctor_m033D6727BB00DF555FE81821570BEDA7DF44939B ();
// 0x0000058B System.Void ES3Cloud_<Sync>d__18::System.IDisposable.Dispose()
extern void U3CSyncU3Ed__18_System_IDisposable_Dispose_m7C90F62D311E884A689CC34C525722E094EAE440 ();
// 0x0000058C System.Boolean ES3Cloud_<Sync>d__18::MoveNext()
extern void U3CSyncU3Ed__18_MoveNext_mB96C6E234525B590D2483C66516F9E1465AEE1B2 ();
// 0x0000058D System.Object ES3Cloud_<Sync>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSyncU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1734D8E0C1FC387ED53AEF2036E7FB3519ABCCE8 ();
// 0x0000058E System.Void ES3Cloud_<Sync>d__18::System.Collections.IEnumerator.Reset()
extern void U3CSyncU3Ed__18_System_Collections_IEnumerator_Reset_m4D596E71E411E3C586E2C933B587D45CD93A802A ();
// 0x0000058F System.Object ES3Cloud_<Sync>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CSyncU3Ed__18_System_Collections_IEnumerator_get_Current_m701280AB2A2696136B8E02246C37FD786E418E52 ();
// 0x00000590 System.Void ES3Cloud_<UploadFile>d__31::.ctor(System.Int32)
extern void U3CUploadFileU3Ed__31__ctor_m0BDC3981FB003514C431862B9CFA3903A9EAA5DC ();
// 0x00000591 System.Void ES3Cloud_<UploadFile>d__31::System.IDisposable.Dispose()
extern void U3CUploadFileU3Ed__31_System_IDisposable_Dispose_m91654840B549D1E707F9BE6C4DF643C749941B75 ();
// 0x00000592 System.Boolean ES3Cloud_<UploadFile>d__31::MoveNext()
extern void U3CUploadFileU3Ed__31_MoveNext_mFD4B4F05BF6BB72FD33BBBC7FC21A3DFBA7DAB56 ();
// 0x00000593 System.Void ES3Cloud_<UploadFile>d__31::<>m__Finally1()
extern void U3CUploadFileU3Ed__31_U3CU3Em__Finally1_mEE21CD0EA6E3281B43DBDAFA088D476AD6F37100 ();
// 0x00000594 System.Object ES3Cloud_<UploadFile>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUploadFileU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8891471CD6B44634D22883E4CF14FEDD2E0CA50D ();
// 0x00000595 System.Void ES3Cloud_<UploadFile>d__31::System.Collections.IEnumerator.Reset()
extern void U3CUploadFileU3Ed__31_System_Collections_IEnumerator_Reset_mE0E509354C35A0F0EA095D0080EF76E984E61EA6 ();
// 0x00000596 System.Object ES3Cloud_<UploadFile>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CUploadFileU3Ed__31_System_Collections_IEnumerator_get_Current_m0EDBA1EF5B08EA88F64F4E6487B0AC5446A9ECC8 ();
// 0x00000597 System.Void ES3Cloud_<DownloadFile>d__42::.ctor(System.Int32)
extern void U3CDownloadFileU3Ed__42__ctor_m8F1A0B87ECAD159883BC8AA73A763456006DB800 ();
// 0x00000598 System.Void ES3Cloud_<DownloadFile>d__42::System.IDisposable.Dispose()
extern void U3CDownloadFileU3Ed__42_System_IDisposable_Dispose_mFB91B69ACF8248533A696FBCAA22EA710900595D ();
// 0x00000599 System.Boolean ES3Cloud_<DownloadFile>d__42::MoveNext()
extern void U3CDownloadFileU3Ed__42_MoveNext_mE93BA1311E386EB3C6ED559A892DFD6BEFB1B760 ();
// 0x0000059A System.Void ES3Cloud_<DownloadFile>d__42::<>m__Finally1()
extern void U3CDownloadFileU3Ed__42_U3CU3Em__Finally1_m61140B2C3AE08CD1CE000449F62937B70015C4AE ();
// 0x0000059B System.Object ES3Cloud_<DownloadFile>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadFileU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF29DBB02369A72FD5973DD8D59D5D7140D1F80AE ();
// 0x0000059C System.Void ES3Cloud_<DownloadFile>d__42::System.Collections.IEnumerator.Reset()
extern void U3CDownloadFileU3Ed__42_System_Collections_IEnumerator_Reset_m86A1B8EFE7F5D2C13251D8FD5DE62DEC1BE6578E ();
// 0x0000059D System.Object ES3Cloud_<DownloadFile>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadFileU3Ed__42_System_Collections_IEnumerator_get_Current_mDDD750C12464F1BB617FAB8B039C4B0E2F2BC2F3 ();
// 0x0000059E System.Void ES3Cloud_<DownloadFile>d__43::.ctor(System.Int32)
extern void U3CDownloadFileU3Ed__43__ctor_mA783E6486D61A17B9B300C0136F32EFFBD642B80 ();
// 0x0000059F System.Void ES3Cloud_<DownloadFile>d__43::System.IDisposable.Dispose()
extern void U3CDownloadFileU3Ed__43_System_IDisposable_Dispose_m1C1DF73874C2563AF61972DBDF6746520DC2882E ();
// 0x000005A0 System.Boolean ES3Cloud_<DownloadFile>d__43::MoveNext()
extern void U3CDownloadFileU3Ed__43_MoveNext_mC394FCFEE9549E286170D007E8EC064C0969C4A0 ();
// 0x000005A1 System.Void ES3Cloud_<DownloadFile>d__43::<>m__Finally1()
extern void U3CDownloadFileU3Ed__43_U3CU3Em__Finally1_mB8C4212B0995DB7773F735FCD1917A7C32542099 ();
// 0x000005A2 System.Object ES3Cloud_<DownloadFile>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadFileU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00E135313B70E3B073141F7C639F832D71DD1A55 ();
// 0x000005A3 System.Void ES3Cloud_<DownloadFile>d__43::System.Collections.IEnumerator.Reset()
extern void U3CDownloadFileU3Ed__43_System_Collections_IEnumerator_Reset_m524A6492CE168E85CC0AEB5551DABD8CF712C027 ();
// 0x000005A4 System.Object ES3Cloud_<DownloadFile>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadFileU3Ed__43_System_Collections_IEnumerator_get_Current_m33F93786F723D0AEBAFF37AE651918A0819F21B5 ();
// 0x000005A5 System.Void ES3Cloud_<DeleteFile>d__51::.ctor(System.Int32)
extern void U3CDeleteFileU3Ed__51__ctor_mDD7A4AAC957798706FBA4FB792BD6411B6C6581E ();
// 0x000005A6 System.Void ES3Cloud_<DeleteFile>d__51::System.IDisposable.Dispose()
extern void U3CDeleteFileU3Ed__51_System_IDisposable_Dispose_m29CF210E1B76494F96E641BDBD282FEC0D7FA875 ();
// 0x000005A7 System.Boolean ES3Cloud_<DeleteFile>d__51::MoveNext()
extern void U3CDeleteFileU3Ed__51_MoveNext_m86992BA94E91618C156FF01F1A9D890EA078D739 ();
// 0x000005A8 System.Void ES3Cloud_<DeleteFile>d__51::<>m__Finally1()
extern void U3CDeleteFileU3Ed__51_U3CU3Em__Finally1_mCB3152F7603B2AF8C8F474ED6AF2533A1855E4F1 ();
// 0x000005A9 System.Object ES3Cloud_<DeleteFile>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeleteFileU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m023054DDDE86D46F1A20B37C57942913390352E2 ();
// 0x000005AA System.Void ES3Cloud_<DeleteFile>d__51::System.Collections.IEnumerator.Reset()
extern void U3CDeleteFileU3Ed__51_System_Collections_IEnumerator_Reset_mFEDE20E0A4D2927FE2E3A366DEAB3E1C795C1313 ();
// 0x000005AB System.Object ES3Cloud_<DeleteFile>d__51::System.Collections.IEnumerator.get_Current()
extern void U3CDeleteFileU3Ed__51_System_Collections_IEnumerator_get_Current_m3A284BC43C616BC210C3BE957142D1D06261F93D ();
// 0x000005AC System.Void ES3Cloud_<RenameFile>d__58::.ctor(System.Int32)
extern void U3CRenameFileU3Ed__58__ctor_mFE6B9003E529F060E59D50B71308D842C929C98E ();
// 0x000005AD System.Void ES3Cloud_<RenameFile>d__58::System.IDisposable.Dispose()
extern void U3CRenameFileU3Ed__58_System_IDisposable_Dispose_mE8B903E35131A23DA279BD0A80629412A5A8EB87 ();
// 0x000005AE System.Boolean ES3Cloud_<RenameFile>d__58::MoveNext()
extern void U3CRenameFileU3Ed__58_MoveNext_m9F0A6DE9DF65B6951C04195C4062B395D31FF9A3 ();
// 0x000005AF System.Void ES3Cloud_<RenameFile>d__58::<>m__Finally1()
extern void U3CRenameFileU3Ed__58_U3CU3Em__Finally1_m36CB320A284D555E2A03DE708966AABA6228D547 ();
// 0x000005B0 System.Object ES3Cloud_<RenameFile>d__58::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRenameFileU3Ed__58_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BFB03E2854D939939EC05E5581CE501BEBF6FA6 ();
// 0x000005B1 System.Void ES3Cloud_<RenameFile>d__58::System.Collections.IEnumerator.Reset()
extern void U3CRenameFileU3Ed__58_System_Collections_IEnumerator_Reset_m7A416560E4F4E558E94B8E4B01C2BEE03B75CD55 ();
// 0x000005B2 System.Object ES3Cloud_<RenameFile>d__58::System.Collections.IEnumerator.get_Current()
extern void U3CRenameFileU3Ed__58_System_Collections_IEnumerator_get_Current_m5F940600D20B101D0B9B04E46A61AC6192C4091F ();
// 0x000005B3 System.Void ES3Cloud_<DownloadFilenames>d__59::.ctor(System.Int32)
extern void U3CDownloadFilenamesU3Ed__59__ctor_m3F2DAC7F103EED57C560376A033A2F7F7B1E125D ();
// 0x000005B4 System.Void ES3Cloud_<DownloadFilenames>d__59::System.IDisposable.Dispose()
extern void U3CDownloadFilenamesU3Ed__59_System_IDisposable_Dispose_mEA8654680F35F4B1D11BFC0B1274EF54D69B86A5 ();
// 0x000005B5 System.Boolean ES3Cloud_<DownloadFilenames>d__59::MoveNext()
extern void U3CDownloadFilenamesU3Ed__59_MoveNext_m8CD8035730FD31F56203439DC3B486B3B37CB600 ();
// 0x000005B6 System.Void ES3Cloud_<DownloadFilenames>d__59::<>m__Finally1()
extern void U3CDownloadFilenamesU3Ed__59_U3CU3Em__Finally1_mF5D579A5E5F75A79F276E3B69156ABCE3D2BC2EF ();
// 0x000005B7 System.Object ES3Cloud_<DownloadFilenames>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadFilenamesU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0031A49181BEE6D5E5B05CCAF2F0A0D879EBF8D8 ();
// 0x000005B8 System.Void ES3Cloud_<DownloadFilenames>d__59::System.Collections.IEnumerator.Reset()
extern void U3CDownloadFilenamesU3Ed__59_System_Collections_IEnumerator_Reset_mA40B1BE32DFC81340E03B02224820EA0F712F212 ();
// 0x000005B9 System.Object ES3Cloud_<DownloadFilenames>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadFilenamesU3Ed__59_System_Collections_IEnumerator_get_Current_m634EE639A9A5D5BA5E0E7631DAA494E1B71250B8 ();
// 0x000005BA System.Void ES3Cloud_<SearchFilenames>d__60::.ctor(System.Int32)
extern void U3CSearchFilenamesU3Ed__60__ctor_m3A62B58C7239A715F52E9B53BB7435EEB860CD68 ();
// 0x000005BB System.Void ES3Cloud_<SearchFilenames>d__60::System.IDisposable.Dispose()
extern void U3CSearchFilenamesU3Ed__60_System_IDisposable_Dispose_m8DFE76AFA0858CCC058B072775738D02B612B338 ();
// 0x000005BC System.Boolean ES3Cloud_<SearchFilenames>d__60::MoveNext()
extern void U3CSearchFilenamesU3Ed__60_MoveNext_mF522B34C6025B147E2EEC6F0A710A36F5914A563 ();
// 0x000005BD System.Void ES3Cloud_<SearchFilenames>d__60::<>m__Finally1()
extern void U3CSearchFilenamesU3Ed__60_U3CU3Em__Finally1_m3F0BF052C0F4B983D5310A2AD3DE2B98D3D1047F ();
// 0x000005BE System.Object ES3Cloud_<SearchFilenames>d__60::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSearchFilenamesU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60577A520C64B4B398D594DFE499CF19DD3EA0BF ();
// 0x000005BF System.Void ES3Cloud_<SearchFilenames>d__60::System.Collections.IEnumerator.Reset()
extern void U3CSearchFilenamesU3Ed__60_System_Collections_IEnumerator_Reset_m15034D944E5814A47E2625612A4C2D46CEB0CDD3 ();
// 0x000005C0 System.Object ES3Cloud_<SearchFilenames>d__60::System.Collections.IEnumerator.get_Current()
extern void U3CSearchFilenamesU3Ed__60_System_Collections_IEnumerator_get_Current_m1DEC95B8F870DEA8C3033301870A136A605FFECF ();
// 0x000005C1 System.Void ES3Cloud_<DownloadTimestamp>d__68::.ctor(System.Int32)
extern void U3CDownloadTimestampU3Ed__68__ctor_m96F8A4F61225CFB202CA4172ECB68C1D485FA9DE ();
// 0x000005C2 System.Void ES3Cloud_<DownloadTimestamp>d__68::System.IDisposable.Dispose()
extern void U3CDownloadTimestampU3Ed__68_System_IDisposable_Dispose_mB52E838EA4C081177C88D11D5D971706D8758AC3 ();
// 0x000005C3 System.Boolean ES3Cloud_<DownloadTimestamp>d__68::MoveNext()
extern void U3CDownloadTimestampU3Ed__68_MoveNext_mEBE146A1FB0F381143A37B131B68F9FAD5BD6B61 ();
// 0x000005C4 System.Void ES3Cloud_<DownloadTimestamp>d__68::<>m__Finally1()
extern void U3CDownloadTimestampU3Ed__68_U3CU3Em__Finally1_mEA369E9545E0FCB177CC68A3612A7FBABF1F9A7D ();
// 0x000005C5 System.Object ES3Cloud_<DownloadTimestamp>d__68::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadTimestampU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC1FE395C56C63D70A83A63D965DC6F7018EF596 ();
// 0x000005C6 System.Void ES3Cloud_<DownloadTimestamp>d__68::System.Collections.IEnumerator.Reset()
extern void U3CDownloadTimestampU3Ed__68_System_Collections_IEnumerator_Reset_m48D46BA0A5086D948C8633BB85CA182A62F1D6E1 ();
// 0x000005C7 System.Object ES3Cloud_<DownloadTimestamp>d__68::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadTimestampU3Ed__68_System_Collections_IEnumerator_get_Current_m3C054DCAC693818678C229A665F5D3B94A3C1328 ();
// 0x000005C8 System.Void ES3Internal.ES3ReferenceMgrBase_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m0867AC1EE4584E3D72A584FFC2C59A2BAFB5AA5D ();
// 0x000005C9 System.Boolean ES3Internal.ES3ReferenceMgrBase_<>c__DisplayClass27_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.Int64,UnityEngine.Object>)
extern void U3CU3Ec__DisplayClass27_0_U3CRemoveU3Eb__0_m7DDF79982240485EC42F30A03B5E235CD11E293D ();
// 0x000005CA System.Void ES3Internal.ES3ReferenceMgrBase_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m0533AF6441844F93A6FD749A1712440D67AA1B55 ();
// 0x000005CB System.Boolean ES3Internal.ES3ReferenceMgrBase_<>c__DisplayClass28_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<UnityEngine.Object,System.Int64>)
extern void U3CU3Ec__DisplayClass28_0_U3CRemoveU3Eb__0_m7ED2D7D13DA77CB4668356F33E17512AB3FCF878 ();
// 0x000005CC System.Void ES3Internal.ES3ReferenceMgrBase_<>c::.cctor()
extern void U3CU3Ec__cctor_mCBAE2CBA3484F6F26D7CFAD443DA421869565DA8 ();
// 0x000005CD System.Void ES3Internal.ES3ReferenceMgrBase_<>c::.ctor()
extern void U3CU3Ec__ctor_mF7E534B55FA357C4B8A838CF4A0534866FC072AE ();
// 0x000005CE System.Boolean ES3Internal.ES3ReferenceMgrBase_<>c::<RemoveNullValues>b__29_0(System.Collections.Generic.KeyValuePair`2<System.Int64,UnityEngine.Object>)
extern void U3CU3Ec_U3CRemoveNullValuesU3Eb__29_0_mD8771B2EF5C61F784A08A9DDC5E3FAD2ADD863D5 ();
// 0x000005CF System.Int64 ES3Internal.ES3ReferenceMgrBase_<>c::<RemoveNullValues>b__29_1(System.Collections.Generic.KeyValuePair`2<System.Int64,UnityEngine.Object>)
extern void U3CU3Ec_U3CRemoveNullValuesU3Eb__29_1_m67907F7BBEE3B2C66F3251FAD56398623C0C75F4 ();
// 0x000005D0 System.Boolean ES3Internal.ES3Reflection_ES3ReflectedMember::get_IsNull()
extern void ES3ReflectedMember_get_IsNull_mA0586EAA02E91291B1C3DA3292834DA7B717224C_AdjustorThunk ();
// 0x000005D1 System.String ES3Internal.ES3Reflection_ES3ReflectedMember::get_Name()
extern void ES3ReflectedMember_get_Name_m65DCB779409BD0CE4E307F35EEED95E2C5161032_AdjustorThunk ();
// 0x000005D2 System.Type ES3Internal.ES3Reflection_ES3ReflectedMember::get_MemberType()
extern void ES3ReflectedMember_get_MemberType_m4DA31D17F188C2583A6F9814B71801F08B9B5C05_AdjustorThunk ();
// 0x000005D3 System.Boolean ES3Internal.ES3Reflection_ES3ReflectedMember::get_IsPublic()
extern void ES3ReflectedMember_get_IsPublic_mDF765A42E542465F1770698DBCCDF4BE665D18A7_AdjustorThunk ();
// 0x000005D4 System.Boolean ES3Internal.ES3Reflection_ES3ReflectedMember::get_IsProtected()
extern void ES3ReflectedMember_get_IsProtected_mA1BB184327C8280A2AEDE5235535DB261E7745B7_AdjustorThunk ();
// 0x000005D5 System.Boolean ES3Internal.ES3Reflection_ES3ReflectedMember::get_IsStatic()
extern void ES3ReflectedMember_get_IsStatic_m90CF30A3C1370CF1701B4471D2ACA95836EB9F2F_AdjustorThunk ();
// 0x000005D6 System.Void ES3Internal.ES3Reflection_ES3ReflectedMember::.ctor(System.Object)
extern void ES3ReflectedMember__ctor_m2C0D87EE6D7B6F42E1ED321266692B4FF2466B1A_AdjustorThunk ();
// 0x000005D7 System.Void ES3Internal.ES3Reflection_ES3ReflectedMember::SetValue(System.Object,System.Object)
extern void ES3ReflectedMember_SetValue_m10D81415A1791D2CF480D379933AFA313566116D_AdjustorThunk ();
// 0x000005D8 System.Object ES3Internal.ES3Reflection_ES3ReflectedMember::GetValue(System.Object)
extern void ES3ReflectedMember_GetValue_m0C7ADDD5B7D7E20C53978C0CBDA389CC3CABC13C_AdjustorThunk ();
// 0x000005D9 System.Void ES3Internal.ES3Reflection_ES3ReflectedMethod::.ctor(System.Type,System.String,System.Type[],System.Type[])
extern void ES3ReflectedMethod__ctor_m7B8C634DB7388FD2460B19675B5A6DD0880D2A16 ();
// 0x000005DA System.Void ES3Internal.ES3Reflection_ES3ReflectedMethod::.ctor(System.Type,System.String,System.Type[],System.Type[],System.Reflection.BindingFlags)
extern void ES3ReflectedMethod__ctor_mA248731652730737F805D06E0CDF4D5A8BA81EF3 ();
// 0x000005DB System.Object ES3Internal.ES3Reflection_ES3ReflectedMethod::Invoke(System.Object,System.Object[])
extern void ES3ReflectedMethod_Invoke_mD25A58365F5CAED89E376CF7FDCC0BBA9F9AA4F0 ();
// 0x000005DC System.Void ES3Internal.ES3Reflection_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_mA3FE7301EA70CB2B7AEFD7662268C71F49A0E3CC ();
// 0x000005DD System.Boolean ES3Internal.ES3Reflection_<>c__DisplayClass27_0::<GetDerivedTypes>b__2(<>f__AnonymousType0`2<System.Reflection.Assembly,System.Type>)
extern void U3CU3Ec__DisplayClass27_0_U3CGetDerivedTypesU3Eb__2_mE0B72CB0B8528A2C997F1B03733982435309A0CF ();
// 0x000005DE System.Void ES3Internal.ES3Reflection_<>c::.cctor()
extern void U3CU3Ec__cctor_m8D6A6BE41FF6776BC085382A9937868B22843B13 ();
// 0x000005DF System.Void ES3Internal.ES3Reflection_<>c::.ctor()
extern void U3CU3Ec__ctor_m6BB75E5970728EFC0D8ABEF6119D8C6B6BC38AD6 ();
// 0x000005E0 System.Collections.Generic.IEnumerable`1<System.Type> ES3Internal.ES3Reflection_<>c::<GetDerivedTypes>b__27_0(System.Reflection.Assembly)
extern void U3CU3Ec_U3CGetDerivedTypesU3Eb__27_0_m94A7E5D4961403CA6DDCEAB62790C98083AB3971 ();
// 0x000005E1 <>f__AnonymousType0`2<System.Reflection.Assembly,System.Type> ES3Internal.ES3Reflection_<>c::<GetDerivedTypes>b__27_1(System.Reflection.Assembly,System.Type)
extern void U3CU3Ec_U3CGetDerivedTypesU3Eb__27_1_m4FDB7C171EA88089E8E393A4CC3AB0024A99B4F2 ();
// 0x000005E2 System.Type ES3Internal.ES3Reflection_<>c::<GetDerivedTypes>b__27_3(<>f__AnonymousType0`2<System.Reflection.Assembly,System.Type>)
extern void U3CU3Ec_U3CGetDerivedTypesU3Eb__27_3_m2F8A024770EF84BD5D9C12293C17324777A60752 ();
// 0x000005E3 System.Void ES3Internal.ES3SerializableDictionary`2_<>c::.cctor()
// 0x000005E4 System.Void ES3Internal.ES3SerializableDictionary`2_<>c::.ctor()
// 0x000005E5 System.Boolean ES3Internal.ES3SerializableDictionary`2_<>c::<RemoveNullValues>b__6_0(System.Collections.Generic.KeyValuePair`2<TKey,TVal>)
// 0x000005E6 TKey ES3Internal.ES3SerializableDictionary`2_<>c::<RemoveNullValues>b__6_1(System.Collections.Generic.KeyValuePair`2<TKey,TVal>)
// 0x000005E7 System.Void ES3Internal.ES3WebClass_<SendWebRequest>d__18::.ctor(System.Int32)
extern void U3CSendWebRequestU3Ed__18__ctor_m05B84CA1C5473BA276D3C99D5AD78C2669A199B3 ();
// 0x000005E8 System.Void ES3Internal.ES3WebClass_<SendWebRequest>d__18::System.IDisposable.Dispose()
extern void U3CSendWebRequestU3Ed__18_System_IDisposable_Dispose_m10166695D948E49245C9B0909B38641838287AF5 ();
// 0x000005E9 System.Boolean ES3Internal.ES3WebClass_<SendWebRequest>d__18::MoveNext()
extern void U3CSendWebRequestU3Ed__18_MoveNext_mA3DF809A3EAC4CF487A3B52981B0A9E9130BCB9C ();
// 0x000005EA System.Object ES3Internal.ES3WebClass_<SendWebRequest>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendWebRequestU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7A624745C7225D39F9C30FFEB5470D19451631C ();
// 0x000005EB System.Void ES3Internal.ES3WebClass_<SendWebRequest>d__18::System.Collections.IEnumerator.Reset()
extern void U3CSendWebRequestU3Ed__18_System_Collections_IEnumerator_Reset_m781E5086D81D1AA4EC286A94B97EF1728FB7FCCF ();
// 0x000005EC System.Object ES3Internal.ES3WebClass_<SendWebRequest>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CSendWebRequestU3Ed__18_System_Collections_IEnumerator_get_Current_m86EF2247AD1C02C2539A77CC1521647A55136E0A ();
// 0x000005ED System.Void ES3Reader_ES3ReaderPropertyEnumerator_<GetEnumerator>d__2::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__2__ctor_m464CFA360A550AED379A3B0D3BD22CF52F746E48 ();
// 0x000005EE System.Void ES3Reader_ES3ReaderPropertyEnumerator_<GetEnumerator>d__2::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_mE5B52E1B648670786BD3C93FD43D4A26DB5D733D ();
// 0x000005EF System.Boolean ES3Reader_ES3ReaderPropertyEnumerator_<GetEnumerator>d__2::MoveNext()
extern void U3CGetEnumeratorU3Ed__2_MoveNext_m04543E1EED6E78CE553F13D2724BB3501766D6D3 ();
// 0x000005F0 System.Object ES3Reader_ES3ReaderPropertyEnumerator_<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA55C586C11378B7EED5AF43876F23B4BF62B28AE ();
// 0x000005F1 System.Void ES3Reader_ES3ReaderPropertyEnumerator_<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mEB70CC2FC3965462D3A483EA2D97085680F60053 ();
// 0x000005F2 System.Object ES3Reader_ES3ReaderPropertyEnumerator_<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_m0905980F66AFCF0B861B5BCC9543D803EC83A018 ();
// 0x000005F3 System.Void ES3Reader_ES3ReaderRawEnumerator_<GetEnumerator>d__2::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__2__ctor_m108CEEB4722EC20E8690E60483B8F788D9473399 ();
// 0x000005F4 System.Void ES3Reader_ES3ReaderRawEnumerator_<GetEnumerator>d__2::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_mC59944AB864257A23D362A3CBB5D364608FF87FC ();
// 0x000005F5 System.Boolean ES3Reader_ES3ReaderRawEnumerator_<GetEnumerator>d__2::MoveNext()
extern void U3CGetEnumeratorU3Ed__2_MoveNext_mCC3F59F02C720D03382D8465D4024AD22A7233D0 ();
// 0x000005F6 System.Object ES3Reader_ES3ReaderRawEnumerator_<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F6AF4BB7179E618AD4DD6493C76944BD0688AD6 ();
// 0x000005F7 System.Void ES3Reader_ES3ReaderRawEnumerator_<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_m77C2404A32B22B3CB1550EF77420F68A4167B096 ();
// 0x000005F8 System.Object ES3Reader_ES3ReaderRawEnumerator_<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_mD7FC926CB8A73A7A6DCA13DBB13402050845B3BE ();
static Il2CppMethodPointer s_methodPointers[1528] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3Serializable__ctor_m5D9C652EB87EA77CA6C78136499DBBA3C480034B,
	ES3NonSerializable__ctor_m770148BE2F0F069E29AEEA8BA01101BF1E2A5D8B,
	ES3AutoSave_Awake_mB13DDD8E29A9C92CE52401792BCBCC733D817A06,
	ES3AutoSave_OnApplicationQuit_mE06C46B910E05DFB4420AA0625A2ABA07A238450,
	ES3AutoSave_OnDestroy_mCF46A32C5EDC15E09B25B2D2FFBE7699445EF9C4,
	ES3AutoSave__ctor_mC10C53CF07F0CAC7FDA095E4D07EB09E2056307E,
	ES3AutoSaveMgr_get_Current_mD91B835E85E7B47D6B24B212775038FCD2F40410,
	ES3AutoSaveMgr_get_Instance_m60E58681BBFDB0C3995900F4FFCA95C9F4B03D3A,
	ES3AutoSaveMgr_Save_mCBBC9405F826143E14D4C5A6AD2F77A329D978C4,
	ES3AutoSaveMgr_Load_mC6EB59DCF8DD34E04581D68908CCB68ADAB83DE5,
	ES3AutoSaveMgr_Start_mC41259CA90BE1FF92DD1015E92454B7113526222,
	ES3AutoSaveMgr_Awake_m28E5A2716D2EBBA4D82D715EA36FEF18EB438D37,
	ES3AutoSaveMgr_OnApplicationQuit_m54D6B426B7CA9A31A93F6F9AA23F47F83EF56D3D,
	ES3AutoSaveMgr_OnApplicationPause_mC5B1023E70195C8CA595D113EF907FC0C747955A,
	ES3AutoSaveMgr_AddAutoSave_mAAA487209B0965D263555C2325EC36DADED766F9,
	ES3AutoSaveMgr_RemoveAutoSave_m4D2A090CA6F4481054933A2BD6068FCF9D82647F,
	ES3AutoSaveMgr__ctor_m06FFA3D65BEFA50C35F1A531D18EE36AFCBEEF17,
	ES3AutoSaveMgr__cctor_mC77A05DD53AA39A785CF2148AA5D5BE91756DFDD,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3_SaveRaw_m185656881388B772C4BC0FBB0AEB098131A6A3CE,
	ES3_SaveRaw_m7A0C7CF36E49472EEF0F63DCCDDA381B39E49341,
	ES3_SaveRaw_m81B7550BFFE0E93277BA2C4375A8E02B699DEBE4,
	ES3_SaveRaw_m06CD032898697EC4E80D1C25617CBC6BC7260A03,
	ES3_SaveRaw_m63206DE41441B9FBF3337CB67DE55D4240DB2DF9,
	ES3_SaveRaw_m658F8E70EA66360A79F9FD2D47825B8B62970BF4,
	ES3_SaveRaw_mFD121A037BF971843D26EE676C67E6DD8C7BC022,
	ES3_SaveRaw_mA9AFA33465D87E3357E7AE1D5DECB5AAC3F3821C,
	ES3_AppendRaw_mAD54E86053DE6A6C7EEAC5918D90D5CBB7C5B96D,
	ES3_AppendRaw_m59372737EB22B9D1776F508E42EBF7C2167F587D,
	ES3_AppendRaw_mA5AE5120CBBCAD7188D170A46E1A01001C37F6B5,
	ES3_AppendRaw_mD092521126032026487990C028912D29D2FA184D,
	ES3_AppendRaw_m36915292D5E44A4D1A965A46250A81292FCA5002,
	ES3_AppendRaw_mEC32507B7D5E2F84054329957F335890A0399ED6,
	ES3_SaveImage_m13730AC98D9B1FA084B18F0B9109A36802117D7F,
	ES3_SaveImage_mB688CC1BCAE83203E1344C7D9C729B60169A1D16,
	ES3_SaveImage_m9BF2559B6AC239478719FF80F406E32D382D98F7,
	ES3_Load_m339B81BF72591C7323B1C7DD2E60DF9D4623BF1A,
	ES3_Load_m67E1B05C0431EA59FD96CA89C48DE9F3CB14D15C,
	ES3_Load_m38521C42B16BA6502931A4637D98ADE12771CB9E,
	ES3_Load_m1ADFC6B3F68E1B711FDB8C77B83A34F132C391A5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3_LoadInto_m2DABB994D56C3C974C44B4C6D40A98A31828473E,
	ES3_LoadInto_m295A78B26C4BA83FDAF32630767BFD216CBF508C,
	ES3_LoadInto_m42501F26583EC1DC5B30C95F780D004BE31C48FF,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3_LoadString_m03F2D57441A12134504E16444CD6A76909D0E3C9,
	ES3_LoadRawBytes_m70C6CD8E4D303A66F5904584CB2834822E3D0676,
	ES3_LoadRawBytes_mED1A50A53EA95B50FD0DAB28A10340AB0BF91565,
	ES3_LoadRawBytes_m397B818D37A9338B97D73A36C72ECA3C0FC6B255,
	ES3_LoadRawBytes_m74DA9B3D50DCC9F2193391350A54B5C41572321E,
	ES3_LoadRawString_mF4CC038ECD6DFF695175102BCA9B02F1F232DC80,
	ES3_LoadRawString_m74B3A2630612B3AF21A7C44FCBAA08FE11E1B331,
	ES3_LoadRawString_m7DD8EE8CEF8D88D90A406F417BCF1FAA191ABFA9,
	ES3_LoadRawString_m5A4BDFBEF816DCE511668B68B11E6D8A6F76E2A1,
	ES3_LoadImage_m075639268BFEC0D02B3E4E64F7AC00491ACA5997,
	ES3_LoadImage_m68648991A888381EECAAE213B36175ECCA62235D,
	ES3_LoadImage_mA4B245149A427AE1A4312E45D379A81128929CF5,
	ES3_LoadImage_m0A9571ACB970E8D2680EA1173BD917337423016C,
	ES3_LoadAudio_mCDD5DED765110C4BA0DB9C2AB47E0774FAE966F1,
	ES3_LoadAudio_m2A0220549E9283A2552D27D7F84481A68E4F08A8,
	NULL,
	NULL,
	ES3_Deserialize_m5C2795D200A761F69B2324DE85B4FFFE89E9D365,
	NULL,
	NULL,
	ES3_DeleteFile_mD0AA5F982AB14A6D6A4D709B7522DFF27C7AE126,
	ES3_DeleteFile_m05C7AF8C8CBEE98FFBBEF4F7779D8BB7FE052994,
	ES3_DeleteFile_mE83B6DA8A3A1DBBF30C2A374C1A158F122DCDA0E,
	ES3_DeleteFile_mFDA84D97818D52B8C5C6740E59F74824231D8AF4,
	ES3_CopyFile_m4ECD60DE8A21E19B6A7781725D245DDB688B5EAC,
	ES3_CopyFile_m14A5D59879FE5A73461B38D4B069AA781FB97C85,
	ES3_CopyFile_m0A6F45C1AD9D8D948A42E5458E3CE9E9D2B6E7A0,
	ES3_RenameFile_m03798E9AFA4F285D91FCE90E7AE43F8863C9515C,
	ES3_RenameFile_m92D79F6A616CB8E0EA30C113D350F6F612FCA2EF,
	ES3_RenameFile_m94BAFFCC6E8EB0748611476389A604EC3654B3AB,
	ES3_CopyDirectory_mF93A31FC462C1D3A592BD86D2338973F89D01C3D,
	ES3_CopyDirectory_m3C6A665350E03E7201D7E46A2E68602AE6826AEF,
	ES3_CopyDirectory_mA70B2CACC416B50F9017B6841FB9F39F49378415,
	ES3_RenameDirectory_m74884C5021B47CD2F6157A2F4D85EAA73D7B4867,
	ES3_RenameDirectory_mAF9A661E7AFBB16BBB8AD06527693AF1C5EB4F2C,
	ES3_RenameDirectory_m8D730B8743E23627632FA5431DA5E78AE8DFFAFE,
	ES3_DeleteDirectory_m5BB80EA138F8EB18BAEBAA440A05C4600B07E08D,
	ES3_DeleteDirectory_mC524A5030C5F4D267D55A68886C03F4EB1E271F2,
	ES3_DeleteDirectory_m1D42EF9D78F658D133148D8D193D59B2891D0B3C,
	ES3_DeleteKey_m340CF506E4C74BAB82CA2214182F20AC4E16C3E3,
	ES3_DeleteKey_m5253D3018C8140B9F6714E156BBA73C7AB1FA2E3,
	ES3_DeleteKey_mF86C97EEE580037681A721B0689E2EA84038E969,
	ES3_DeleteKey_m1230F7DACFDEB6641DE2F0BC6A1B7B258C0EE798,
	ES3_KeyExists_m9D8B8CA83BDF8F820D46D084647B432C8B0ED9F8,
	ES3_KeyExists_m64329C74796BAAA23C6C75B1A56E7C9625C927F6,
	ES3_KeyExists_m9445454C892C5282AD69105DF0932D01D5FC6259,
	ES3_KeyExists_m5F8B2A90FDBF2447C4B1D736CAFA7C68F2683D71,
	ES3_FileExists_mDB951F12CBAF0341F430EAD71349B1EB94FB4F1D,
	ES3_FileExists_mEE7CF88C89A73EEA89BBF882FB1E86BACD3D5403,
	ES3_FileExists_m40193005B1DD5F178EDE24EC403A2D0B98B7831A,
	ES3_FileExists_mE2E9A0EC715448CD6FA0AC1588D51E8DB259D745,
	ES3_DirectoryExists_mFDF2BE3DE73C19F304D5EFD5CD5045E15F2E6481,
	ES3_DirectoryExists_m6B8E797B63DC117F18387123021F15757F92955B,
	ES3_DirectoryExists_m59913CE9E75DD0B0F3A7090630472CD225DE786E,
	ES3_GetKeys_mC78A2482BAA8A941F0A4482575E6C02FF8E371FE,
	ES3_GetKeys_m8EDA4C747C98BFFDDD45F95BB346CA07346712F1,
	ES3_GetKeys_mAAE5F800EFC61EC4C238199A8E6235452ACEF981,
	ES3_GetKeys_m4E4E7B1849CC0A9124F8298B21E5CD8E67BAF52B,
	ES3_GetFiles_mD451CFE6D93B7A3A5DF72C25E0B25D2696C60A17,
	ES3_GetFiles_m7BCA658BE84BE317B2149176FD10957BDBE6F7C0,
	ES3_GetFiles_mD92FFCDC3CC71549115D69FC35EBC4C6EEB4763B,
	ES3_GetFiles_m82DC58442713CE7EDE532BD85EDD35A17225FA57,
	ES3_GetDirectories_m78437F8BD33070DACB9D2578ECA697364C8A1E7E,
	ES3_GetDirectories_m14428CF7C029AFE293C3FCCE36FA48F990108ABA,
	ES3_GetDirectories_m8BC5EC01FEC3568DDAA9C570CE72821DFEB38E5B,
	ES3_GetDirectories_mB0BE5EEC616E5AEE82C6C8BDE3411956A9E6BACA,
	ES3_CreateBackup_m027B93C4329139D3E3007E80B00BB6A7BA057C2E,
	ES3_CreateBackup_m2BD7925DBE59380958D8E07DD5103455E3C4DC20,
	ES3_CreateBackup_m9151AC964D50C58528116DF964D84C4CF801BA65,
	ES3_CreateBackup_mF99F5572D20AD03898E80AE177E3E6534AD06A8E,
	ES3_RestoreBackup_mF65BB014895598BBA7BD8EED19FE7741B10BB029,
	ES3_RestoreBackup_m598CF154231B2EBD6CA81DD7CD2635EBE5EC770C,
	ES3_RestoreBackup_m82E0B77BADBBB9F3367F00DB706AC0F3134F40A4,
	ES3_GetTimestamp_m2DB86061FCAC875539F86E9C5D6A2E05928A62EB,
	ES3_GetTimestamp_m5BF46845E352319EC7B9475F80C93277CE199BC7,
	ES3_GetTimestamp_m206942082E746E95819ED64FD8827C1ECD9D8EAA,
	ES3_GetTimestamp_mD37C8B6C766A0CA806060B776B8B4B71CADC5937,
	ES3_StoreCachedFile_m1B34E1EFA9B2C40FB3720FC3AA11912FC56313BE,
	ES3_StoreCachedFile_m5E12741050A1AD49707B005E100DF49793F1F131,
	ES3_StoreCachedFile_m3B805435D9F10B69946D59603602077FF7B547E4,
	ES3_StoreCachedFile_m99D4848127E6D4BA8CAE19A4584E9284CA8398F3,
	ES3_CacheFile_m1C9CE72EDD36EDB772A57CAD43666656157D7660,
	ES3_CacheFile_mE802BD63D3EBC826E64FE0C0B2E3435D36A4B4CA,
	ES3_CacheFile_m9311BF777DCEFAC3B26ECDE282DC628B0D1F2B69,
	ES3_CacheFile_m2BD1B65E0535997821A85EBFBE4B34B76F93C2B7,
	ES3_Init_mEE11D38D7E3E7D35A33625FE9E4E873D2D105CB3,
	ES3File__ctor_m780F5D3BFEB251899CC4EFB9ABDBBAA0F7A8D5BB,
	ES3File__ctor_m253D89DF972FBDAE4C235D80E61FD2627786C7FC,
	ES3File__ctor_m2AFFEF7E039832C28591AA3BA528E095DCA34AE8,
	ES3File__ctor_mD741E25E2E4DACFC2AB59D94AA331EE0EFD0C64C,
	ES3File__ctor_m96839D7D90D03187F2B31D8771C9FD3E8DA9F0AA,
	ES3File__ctor_mC161D7F766C39658688E1C745B1998F2D7FF301A,
	ES3File__ctor_mBEFCE7F38FB753A43DF1DA8D986A0C557D830BF4,
	ES3File__ctor_m885630A6272B8FFAF77D22C427CEA5D48665B84E,
	ES3File__ctor_mBA2B6B4B21DA81390EBF4A4603D13B54C9C86F87,
	ES3File_Sync_mB1C971B11BA2F18AD2448BF412E15234812F1C60,
	ES3File_Sync_mEE74FF3C19D663EC993237E42069D92617C03F3A,
	ES3File_Sync_m8449CB9A818C32C5EA3437227D541BADBDFDB90E,
	ES3File_Clear_mC386010AADFDA8682674622657EF8C89662B51F6,
	ES3File_GetKeys_mE1B613E4C6485C5E3FF1C11DD2F837EAE80B1CFA,
	NULL,
	ES3File_SaveRaw_m82962A9BCD9F4591A9AC6BE56B0C3139485B8309,
	ES3File_AppendRaw_m5ED3178112BA636EBAC5E23FDF4490C56DD54040,
	ES3File_Load_m0B84781A868397E22DF30C8F8D6D62A2CFF632E5,
	ES3File_Load_m0A9174DDBF9953D85AE73014B73FE53BFC1AF06F,
	NULL,
	NULL,
	NULL,
	ES3File_LoadRawBytes_mBFC6623E89998B08EDB5BCD93F954BB042B893E0,
	ES3File_LoadRawString_m3058300E89B671BBCE2A21EE114890D1130E508A,
	ES3File_DeleteKey_m4D1937BACC2C8BFC363958275224C03829CF9F50,
	ES3File_KeyExists_m167B533F40F7D9B90481D5F483C1C1765EDDF2B9,
	ES3File_Size_m326EB59AC4F792236C0F0544C05AA045E3BEB7C2,
	ES3File_GetKeyType_mD92BFDE795AE2B9405665287FE32295622A6718B,
	ES3File_GetOrCreateCachedFile_m3904AD47E2CB689A23D201AD938F886AADDEF607,
	ES3File_CacheFile_mB60D1A5BDBE0BB7467C34F8EDCD48DC520FA20DC,
	ES3File_Store_m12A194B73BD11146F5EB3742F50079D41AD82540,
	ES3File_RemoveCachedFile_m6D7B49CD13CE276F2CD05C1C3B2C15E428CCB64F,
	ES3File_CopyCachedFile_m121ED179C64CED9B0B49CD0A690B782F1FB1331D,
	ES3File_DeleteKey_m9DF15826BB45C3F550DED2D9A8AE3E3CD4B0D87B,
	ES3File_KeyExists_m835C0467FA805F83D574184D6A66DCC4A612E387,
	ES3File_FileExists_m4B0B846E932FBF3BB9802CEC772D25090D64E563,
	ES3File_GetKeys_mEB1772F6A72A36CB8B410E7642B175723AB5A185,
	ES3File_GetFiles_m43F67ED3B2EF2A2C7C5CE0FAC98A71FCD2479D94,
	ES3File_GetTimestamp_mFE50B38C30EB57E8E690A5EF77EF172404B872F0,
	ES3File__cctor_mBDFB0090DED9748CE5E0BF88BB80860443D5093E,
	ES3InspectorInfo__ctor_m7966EBF566674FC349B1837A9428EC86AFF034D1,
	ES3ReferenceMgr__ctor_m1F32371F83DF571BEDCF59E71A3B92BB0412CAF3,
	ES3Spreadsheet__ctor_m9EC1620F5F2B923C5B68EAA6EF4A2308F9C5A3DA,
	ES3Spreadsheet_get_ColumnCount_m4EFA161C29FC7771BB7130623EAC686372E31951,
	ES3Spreadsheet_get_RowCount_m11F511DEB209BDB0DD2C5B0092775B536A5CEFCD,
	NULL,
	ES3Spreadsheet_SetCellString_mE772C1C051311ECB879B25A2BB8F479F84693DE4,
	NULL,
	ES3Spreadsheet_GetCell_mE0010FBCF6761C97F87B9E70100DA04E841EE3E2,
	ES3Spreadsheet_Load_mF6BFAA30F1031CE98806E75BFA49EB19C69FDE7B,
	ES3Spreadsheet_Load_m64FA8703A38554E7A667EB4529FEA0F088C279AE,
	ES3Spreadsheet_Load_mAA7A582BEDEB539E6C6E5F06074C397B941CD65A,
	ES3Spreadsheet_LoadRaw_m7E42B1EE92FAA4E7961ABD3E8EEAD2226AB5C5F0,
	ES3Spreadsheet_LoadRaw_m74B91B28998D933DE4017696777FFF3B2634E714,
	ES3Spreadsheet_Load_mA39FE17BAC9795948102396F65DB5206C1D09131,
	ES3Spreadsheet_Save_m25BDB030B50D9AEC883B6EC3B3E13081D640EAD2,
	ES3Spreadsheet_Save_m21E0D1571E6BE34C833F1783225014B74733A338,
	ES3Spreadsheet_Save_m26CE88E7CF93EBF23A1C27FC2C79810C6FF1B0FA,
	ES3Spreadsheet_Save_mBEA2F09E65738879E1213EB2D5AC104EFB4D8704,
	ES3Spreadsheet_Save_mA30AFE3E9B0DB59FCC6D16F247EEB44A1911C216,
	ES3Spreadsheet_Save_mDAEB0EC6F58974F5409A2619B9799C4F0B1BD849,
	ES3Spreadsheet_Escape_mD454AE36F544B23ABB68463970FDEA713A93DC8B,
	ES3Spreadsheet_Unescape_m6A87213236F367600B72EBCD9821229A5B615ADD,
	ES3Spreadsheet_ToArray_m238C6E3665120E350182844D7971B57A7E504BBC,
	ES3Spreadsheet__cctor_mAA735D6DB08517584921A85EE38A0BEE30C667E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3Reader_Goto_mECEF750439752C93AA41BED4CA3E2DFF47016D67,
	ES3Reader_StartReadObject_mC1E9750C54C281CF465317BC7ABEEF060A4996AD,
	ES3Reader_EndReadObject_mC2FA016C0528AEDDA9115E04FC113DBE74830EB7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3Reader__ctor_mFA5D8C47D30FB52BE25C949DF191C5510596E2AD,
	ES3Reader_get_Properties_m8928B9AB6B83F6681F12AB929DB97C0EA902728C,
	ES3Reader_get_RawEnumerator_mB24EF7B64CA00350FC75E62F286CC73A71B3ACA8,
	ES3Reader_Skip_mAD263093FD43CCDDE25772BCF5A5D96F668C2DD0,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3Reader_ReadRefProperty_mF8B64EBB09BB2399E192329EC5974709E603231B,
	ES3Reader_ReadType_m2F367E59B15774FB53E3C4ED6DF0AEF4FC3F4958,
	ES3Reader_SetPrivateProperty_m208B026129F31DD900FD0B3EC32EF95F61AFA290,
	ES3Reader_SetPrivateField_m02248A47636EBB0A1593F89B5F9FE35F7358D624,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3Reader_Create_m4DE19555F5B4D1ED7A534EC857C0BD21C4FC27C2,
	ES3Reader_Create_mD6B83820A40E958B88788C41A452C789CD92290F,
	ES3Reader_Create_m2BF9327FD7607F859EAC2F72EA8CFB0E249AA45E,
	ES3Reader_Create_mDD80D8F8A3EF79D67C3CE9D5519580A3C6DBF8DE,
	ES3Reader_Create_m6FE81E95A0CE7BFEF0596B880199D3D793976613,
	ES3Reader_Create_mAA7D637CA4A36D277F8828030BD8AAA0D3C918DB,
	ES3Reader_Create_mA5DAFEB96B70AFB15DF8FC9DCD2E25582B62026A,
	ES3Reader_Create_mF5DF1FD6E4650685509F53B7745E44C64D119652,
	ES3XMLReader__ctor_mAEB5BA392DD5E005A1A6B575A111DF8C0D2439A1,
	ES3Defaults__ctor_mC15C214EE3FD7C5BC27683E1F9EAFF9E86AACD6B,
	ES3Settings_get_defaultSettingsScriptableObject_mE3CC92D7B03E640EF54DE8FC975F14CD6D7F87C8,
	ES3Settings_get_defaultSettings_m7935DE01EA5292B39393693DD18519D3713E66D5,
	ES3Settings_get_unencryptedUncompressedSettings_m6CB5E48587262D9260A3A4A077D9C8EA33182B13,
	ES3Settings_get_location_m8E35806FDFECEAD31797C81FA6AF50283E97FEA6,
	ES3Settings_set_location_mA044138052D94897B7C7BBAF2894C821C6744748,
	ES3Settings_get_FullPath_m0F490EE68D40BD397227296257729D6A4CF244D1,
	ES3Settings__ctor_m1B2CE5BD8E7129A9AA19221716A203B222313BC0,
	ES3Settings__ctor_mD79A3966C2AB80CD6811B0F156BFB6CE49FC536F,
	ES3Settings__ctor_m428E5A1813662EF4BA2D8E184FB1770F2A622A75,
	ES3Settings__ctor_mC3CB3E40C73F375310C204D9BE86565A9266B9DA,
	ES3Settings__ctor_mB6E85691377444C5EA9138475738060D621013F3,
	ES3Settings__ctor_m2EB9C617958F0C2554F76B7F56E434C9B1D703E3,
	ES3Settings_IsAbsolute_mD0A27C2F34CAE0D9AABB090997D81F31D88E738C,
	ES3Settings_Clone_m7C28E3FFE2301E690BB97D826BB24C6479CA778B,
	ES3Settings_CopyInto_m7025842713F68362DDC7B2493E43966C74830B56,
	ES3Settings__cctor_mEBBCA0B1E301CE41B37295DC1773D37C981371A3,
	ES3SerializableSettings__ctor_m7B19BA579AC8648C79D32F9B5BC32B8C10304555,
	ES3SerializableSettings__ctor_mBBEEC2192360AC655048F2625E059E0E4A62ADF6,
	ES3SerializableSettings__ctor_m4E934731D916B440D6B7ABFFA1A745C941C78B0D,
	ES3SerializableSettings__ctor_m3C22763E61F3469BECCB88E24FF6CA7BA8F0874F,
	ES3Ref__ctor_m87E69980DAA88B2D9BA9181A762EFEDE81C31459,
	ES3Cloud__ctor_m072D5FF64813D0BF5296CFD829B7B64F269ACB0F,
	ES3Cloud_get_data_mAB407FDFA4FA3D5D0140D90A587A383E9C566FBD,
	ES3Cloud_get_text_m60890F0C77E8CF50DBE91D2B081B2116CF4E83CF,
	ES3Cloud_get_filenames_m313E6726F529E19FA4B7FE67CC4B698A281E1930,
	ES3Cloud_get_timestamp_m336FE907512134E0F9A2B7B41FDF98863F0608C6,
	ES3Cloud_Sync_mAFF71A47D49921B03FB4518E57FB37766E9420AF,
	ES3Cloud_Sync_m7B3714CA1E3720C34F22D4C865E82E94E4D32851,
	ES3Cloud_Sync_m0A9044CEE7ECF565C2E15ADF9653122F2F03CE90,
	ES3Cloud_Sync_mAA17F0B41150D3D968263E2BCA57A7B7FFC1EA5D,
	ES3Cloud_Sync_mF056DA7C1E14986EB1EF38C4CF162DEE0B9E8778,
	ES3Cloud_Sync_m499376332C418CEFBFD4125B8DF8E00C8C6CF0BE,
	ES3Cloud_Sync_mB19FC8FA4A161ACA3B4CC333279B9E38A3020E71,
	ES3Cloud_Sync_mBC9B7F29C9C999AD6CF61D40B202F7B4A18DDFA2,
	ES3Cloud_UploadFile_m6E32EC45CE84359DCBEBFB0C240F1192CFAA33C4,
	ES3Cloud_UploadFile_m9A29F8AA221F4C22016DCF2205192B0418BCB80A,
	ES3Cloud_UploadFile_mCA820877D2C927BE3B0EC94B6045149B3DE38718,
	ES3Cloud_UploadFile_m0FE9863888D3F6AA8D5CDD2CA87BCC9AEDE65614,
	ES3Cloud_UploadFile_m8C9779C4DA3B08BAABF52646E34CD50891D47C0E,
	ES3Cloud_UploadFile_mF1C62E2DD407B5D30B12735C81E1F56C851F8947,
	ES3Cloud_UploadFile_m1CE23D99127093B3671E0C4E55C29A9A4F72BAFC,
	ES3Cloud_UploadFile_mBF1C8E1CAABE09169EA6144C659BB090B8632341,
	ES3Cloud_UploadFile_mCBA08DE975F15E07A41EEA146951E44540EEFBF8,
	ES3Cloud_UploadFile_m48D3C117E03655CBA3B4EF904D245B1E9BF91A87,
	ES3Cloud_UploadFile_m713B67188204F3FF0C2BC4D1091292C0E21C458B,
	ES3Cloud_UploadFile_m932159E5998CE08FFB78C62471DAC57BCD230923,
	ES3Cloud_UploadFile_m5390A39E5BE820257FB24BE236FCD3474AD44368,
	ES3Cloud_DownloadFile_mB65D1E34A9B8BF80B72B2819B5EA69A53D9A0743,
	ES3Cloud_DownloadFile_mEB3FB82F029E2418419B8D22E7579D93DEBCEE19,
	ES3Cloud_DownloadFile_mEE88FE09029589027EE8B278B90A7D111F25F8CC,
	ES3Cloud_DownloadFile_mB351CCDDE17DA1FB6D5F6604511E1C7DAE5E2F0A,
	ES3Cloud_DownloadFile_m744737152224496ECC5F8CCC23BB4A8074418589,
	ES3Cloud_DownloadFile_m8EBA3A0BE648E60B10E3E8D6BE20004CDD881903,
	ES3Cloud_DownloadFile_m350F6BB180DE0DD099B04B0E8AE0CBBEC2596C89,
	ES3Cloud_DownloadFile_m08680260C0C39E31540E6E62165FA8281CE352B5,
	ES3Cloud_DownloadFile_m00409B6B79685F5FA64BE21B4D5236BFA1AB7D6F,
	ES3Cloud_DownloadFile_mDDD28A721F24A156BF5FDD64531E9640FEB489DE,
	ES3Cloud_DownloadFile_mF44F1FA229262AE9FFD6FA24A3205A2C9AC7D216,
	ES3Cloud_DownloadFile_m546D47A2A65E1B60531320A1AB7D9A12AFAEB8A5,
	ES3Cloud_DeleteFile_m4A380F81BA33024C36B15951B0CCA6CF5AE63F29,
	ES3Cloud_DeleteFile_m0B30AA4AC8190E16BA95C12BD81C66449CD39197,
	ES3Cloud_DeleteFile_m7C0695B9AF1BF306CD5CEAE8DFD0EDABAD39228D,
	ES3Cloud_DeleteFile_mFE6E0C4E2A6DC4A1A3C110B01221DEE8415498ED,
	ES3Cloud_DeleteFile_mA5D50E3CE25DBBE8F70928787D3859A25B756166,
	ES3Cloud_DeleteFile_mC901638D3C65350485A212FE34FF4DFF81F6E0ED,
	ES3Cloud_DeleteFile_mFA5C7C45A38BBF1CC27762CAC3CFAD4662F8CDAF,
	ES3Cloud_DeleteFile_m4552A5AA7DBC7E807949ED52E404721887A6082E,
	ES3Cloud_RenameFile_m3576D8BC23EB65D05638767EAD47B58C672B2AB8,
	ES3Cloud_RenameFile_mA811DCDFAAA4ACFA7CAE5F49F9AF79E76835D3F9,
	ES3Cloud_RenameFile_m6DCA53F6E9C6014C8DCA1BFE22863824ED3F6D83,
	ES3Cloud_RenameFile_m28174978D360F572463731064423B74F8AE32DD4,
	ES3Cloud_RenameFile_mE917649F9AB64622C4C67936B2F0725A9AEAD8F4,
	ES3Cloud_RenameFile_m14CF4D0E172B998BAAA59C1955B071815951660A,
	ES3Cloud_RenameFile_m33D2812826427CEB0BCEDFD8A84C77993442338C,
	ES3Cloud_DownloadFilenames_m0BC6065021079A5EC18B76B1E03BFA74E971139B,
	ES3Cloud_SearchFilenames_mE37F97ADB55960AC1CCD5A72605B35B08AD03041,
	ES3Cloud_DownloadTimestamp_m0E596E8D543DD2519BD1705B0EEB9F3856E049E9,
	ES3Cloud_DownloadTimestamp_m5C6325C3FFFE062D87B6C4D2A9D640B1FE08D087,
	ES3Cloud_DownloadTimestamp_m56EFF21C3F882C5C747920E822AEF840E923B9DA,
	ES3Cloud_DownloadTimestamp_m701316872D74979E46CC9B50BFBC49844F96AB86,
	ES3Cloud_DownloadTimestamp_mCA8E8F290EDF7DF4C291D75C8EFBBF7F97CDB7A0,
	ES3Cloud_DownloadTimestamp_mE2B69499C715C252A12626DF00793838DDB2580D,
	ES3Cloud_DownloadTimestamp_m8DF64717CCC4A51543F2A325C24E3D036E2287E9,
	ES3Cloud_DownloadTimestamp_mF716C5454907940EBDA2B26B9009070D4D0E1B0E,
	ES3Cloud_DateTimeToUnixTimestamp_m707B8B00877793F62AA3840CE57D5F00872EDE77,
	ES3Cloud_GetFileTimestamp_mD0B5C6DF14430675FD6496FD1869895F7DDF94F9,
	ES3Cloud_Reset_m1301B0B621A18C13E13B866A6B7DB0E3D0EDF31C,
	NULL,
	ES3Writer_StartWriteFile_m4A9558E362E4979381B099747A14477054D3D678,
	ES3Writer_EndWriteFile_m0BDD5927CA3AD2C33126186CA25EEF194D3EB50E,
	ES3Writer_StartWriteObject_mEE85E07EB3920D8EE6FA16BC766C06865F88F879,
	ES3Writer_EndWriteObject_m38AA1FA1567CB7F1AEB46A5EFDCC986D5737A49E,
	ES3Writer_StartWriteProperty_mCBE70E6485B9267AEBBE6CCE787CDA183C027022,
	ES3Writer_EndWriteProperty_m2D4F1791BBE89BB476ACAD369C21E81D1097BB50,
	ES3Writer_StartWriteCollection_mA8D3B8871ADB3EED4ED6CD5A94948BD62947656F,
	ES3Writer_EndWriteCollection_m8EB3510B8C3D92BF128B47F5F517A9DEAE360CA5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3Writer__ctor_m40A8EF5042424F82BE2DD0449A1BF3954EAE39DA,
	ES3Writer_Write_m54C2EA500C058E8982599253E721D0E550804E67,
	NULL,
	ES3Writer_Write_mE3278DE9E7B7069688D4B449521A19618BE5DA42,
	ES3Writer_Write_m2B0FFC0A8DB2401C7CC6938EF14F0A22CB41E30D,
	ES3Writer_Write_m2111F8ADF9880838AE50D120AFDD307F0FF6BC40,
	ES3Writer_WriteRef_mED9AF6E95AE1984ECADF51934EC0D38DFFD5F939,
	ES3Writer_WriteProperty_m02AEA4AA6307F0D9B6E5B546A9E7890AC12097B6,
	ES3Writer_WriteProperty_m53FB2DD601FD74D2DB3E32090D78BAE53A31617A,
	NULL,
	ES3Writer_WriteProperty_m297289B93F0C3C4C7A5B611346C52CC867D7DAE6,
	ES3Writer_WriteProperty_m63BE9A6C4A67D4EFBD2ABA201CBD9F8815D36DE3,
	ES3Writer_WritePropertyByRef_mBBBF59175AEE334D1CDFCBF672B564F0946AFB5A,
	ES3Writer_WritePrivateProperty_m965CC085DA3736F7AEC39D35080284C14DCD68C4,
	ES3Writer_WritePrivateField_m49CD30E6803CB527852527B714605A4395F2900C,
	ES3Writer_WritePrivateProperty_mBB08F40928E0DBED2FB4E507EA51AFB99863C239,
	ES3Writer_WritePrivateField_m8B3EB35670B7137F11951494AEDDFA26DD9C1291,
	ES3Writer_WritePrivatePropertyByRef_m5DACC34AE51F3B52E6BD182F8CF145889AD5AEC7,
	ES3Writer_WritePrivateFieldByRef_m13F98F4E1944DD6E902BB343A4F011B21B56A3DA,
	ES3Writer_WriteType_m082922ACB15EC0F069DB997F2D417F9CC4073275,
	ES3Writer_Create_m9031A8B846FD09DF2EB9DCBE6C31F2C159349764,
	ES3Writer_Create_mD97C535634FA6D5A62784EE7D71FF7D78F97822D,
	ES3Writer_Create_mFA3C8FA39007FF3DCF9DD9EC23AF452A7FD52424,
	ES3Writer_Create_m854DAC5E022D7244231A37906B0DBA6217BDED41,
	ES3Writer_SerializationDepthLimitExceeded_mF83656E02375E42A2450498D6C0D451438A64A8F,
	ES3Writer_MarkKeyForDeletion_m85CABC52BF2C41379F44EEA82CE17D8388246A6C,
	ES3Writer_Merge_m7CD4C2A7FDC2F4812C107DF5602B7C544E606231,
	ES3Writer_Merge_m324EB02B2D3C621E84E4A1C5DF08BEBCBE1DB5A3,
	ES3Writer_Save_mD751854BD8A5F1B2AB52926D12DBE0253D734416,
	ES3Writer_Save_m10CC2CDD279033A701E313A440D23D3A713BF77A,
	ES3XMLWriter__ctor_m515C7F651A44D4AF864F7D62B5B5A9D155616A91,
	ES3Type_ES3Prefab__ctor_mC382EABDD13DAE44F18F2058C5973220ADB8A514,
	ES3Type_ES3Prefab_Write_m94AE7C7FA96BF5506D094A5DBBE474F4E3E793F0,
	NULL,
	ES3Type_ES3Prefab__cctor_m99AC978D69E1D23E7EA509886425A0F0A721CE8F,
	ES3Type_ES3PrefabInternal__ctor_mBBD07E83D2E18994EA89C540C0407E9C3B91BA7C,
	ES3Type_ES3PrefabInternal_Write_m501520A6FCAD05943ED777F6093CAF086408F9DB,
	NULL,
	ES3Type_ES3PrefabInternal__cctor_m3E078C37A0440B5045D5D53B101B03B7CA11054F,
	ES32DArrayType__ctor_mEE00FC72FF86DCE0C8273894772AEBF319DCE7C8,
	ES32DArrayType_Write_m9D5A8F2AC8376A37A291E86AD4B108ADB9E4893A,
	NULL,
	ES32DArrayType_Read_mA525DB06FDA85AC2542B75D3A1258F00B7115ADD,
	NULL,
	ES32DArrayType_ReadInto_m0D7B554B96F2437159D4EABDF89D51DF1B59717D,
	ES33DArrayType__ctor_m7DCE734BBA91DFC160C0FD735AD002767A40AD39,
	ES33DArrayType_Write_mD724D44F42A8561B78F1666CE35CC0176A8667C0,
	NULL,
	ES33DArrayType_Read_m8EE1219336E51FC952CC184A68891EEBECC46DA2,
	NULL,
	ES33DArrayType_ReadInto_mDF458F339875C41265C9ECA36E34A15A3A909ABF,
	ES3ArrayType__ctor_m8EE2D6B2A9CDBFE86CC445643D5004336631E7E9,
	ES3ArrayType__ctor_mEA0F23FA8CF8EC6FD1A369CD2F636B68553421BE,
	ES3ArrayType_Write_m84B6BCE5067E444904EC44F8CCC0E3D62F133B92,
	ES3ArrayType_Read_m6C523B6EC44CDD4861E4A08894ADBBFE1C3AF1E5,
	NULL,
	NULL,
	ES3ArrayType_ReadInto_mF618476A6339B6EB4F65EA1A8C9CBA210644AB5C,
	NULL,
	NULL,
	NULL,
	ES3CollectionType__ctor_mF904DB0113A81FD13B9C30350A58552AE3997CC9,
	ES3CollectionType__ctor_mD4F0464DF0D38ED1B26A6A3081A7B3EA383D08CA,
	ES3CollectionType_Write_mB9C5DDCD7BC918F47B54FC4831075153F5AF1680,
	NULL,
	NULL,
	ES3CollectionType_ReadICollectionInto_mB0251AE1E13CD925262D16DAD681B5ADC7514C74,
	ES3DictionaryType__ctor_m82AD6C47AD75B2FE6B85DBA8ED8324B9FBA32558,
	ES3DictionaryType__ctor_mFEC8022DDE5B7FCC75AFF2F5D306840F0FBF5D4D,
	ES3DictionaryType_Write_m46C7BECDB5FFEBA31A61AC4F58EB2134D61E169F,
	ES3DictionaryType_Write_mC365B039AFD955CE6B38321BB28D32A110AFDDB9,
	NULL,
	NULL,
	ES3DictionaryType_Read_m587E2BF8F94F2A045AA7173017FA3EB5B3D8A302,
	ES3DictionaryType_ReadInto_mD79066D37F150937BBE5AC80F8A8630694734996,
	ES3HashSetType__ctor_m1CCB7FECDB2AAE31C0F91D9099A8EFF3B77816AB,
	ES3HashSetType_Write_mC5011A54411BFAA863B199FFE28C8299F5EB726C,
	NULL,
	ES3HashSetType_Read_m27E85F6DD6A8404E1F4817493CB6A2D9E94F75AF,
	NULL,
	ES3HashSetType_ReadInto_m68C4FCD08860FF22984DA4F3A631B990E5E32EA7,
	ES3ListType__ctor_m7CB3D8293ACC66031A9EAC1C706A5FE65D09D2A2,
	ES3ListType__ctor_m929C68BF94A1BCB0EE95FDC83016E38188C7A8BF,
	ES3ListType_Write_m2705E26310FA670288CEE25899ABA6AF40499139,
	NULL,
	NULL,
	ES3ListType_Read_mFC17880683C890D6CBE0AFDE743F8C29C270E7C5,
	ES3ListType_ReadInto_m38C73DA7DF90D494357656553EFDD8CB1ECD6FE4,
	ES3QueueType__ctor_mE124A6845E7493310C4778BAA6318E854BF06EBA,
	ES3QueueType_Write_mCF0088FE8AEACB82DE57F3572DE8D782110E3CBE,
	NULL,
	NULL,
	ES3QueueType_Read_mBFD3947A0B9A3204DEF1F3AACAC293B1B3966EF2,
	ES3QueueType_ReadInto_mF79F71CF96C24D8D45E2B31A594120585EAA6C90,
	ES3StackType__ctor_m4C47A278E9C819ECB087405B8A603835F9A25A0C,
	ES3StackType_Write_mD77C5F6D3C6D30F1BD241B9E815A94440646BCBE,
	NULL,
	NULL,
	ES3StackType_Read_m3596316D86568543979958A89E2EF67155606ACD,
	ES3StackType_ReadInto_m01955D15A95AA2E27683BE1A94D427ABE7154EA4,
	ES3ComponentType__ctor_mFAADE03FCF8669787D2FBB33DFEE3277705C8631,
	NULL,
	NULL,
	ES3ComponentType_WriteUnityObject_m7EAC0E6D2EE350D45B0AEA540896CB282C5F9C40,
	NULL,
	NULL,
	NULL,
	ES3ComponentType_GetOrAddComponent_m7C8F6701E4A588D29830D2A294A04FD830F73CDE,
	ES3ComponentType_CreateComponent_m14BA12F0AD88A1C74679FDAD5FA0D6950813A942,
	ES3ObjectType__ctor_m6CF8C0823BE72726B2F24D179AD044C0388C26C7,
	NULL,
	NULL,
	NULL,
	ES3ObjectType_Write_mF0552F57FB8E470497445821DE5715BD43666538,
	NULL,
	NULL,
	ES3ScriptableObjectType__ctor_mDA7449A8FCCF24E8F6520EF3182E61694C636B21,
	NULL,
	NULL,
	ES3ScriptableObjectType_WriteUnityObject_mB531FB539A50D2F2D43B96C832930DCE6A64FE80,
	NULL,
	NULL,
	NULL,
	ES3Type__ctor_mE1C65E5D0EA2D6500FA0A888B1A9E1B57513F0C1,
	NULL,
	NULL,
	NULL,
	ES3Type_WriteUsingDerivedType_m679E9EA5FA7A62C370941558DA930B7CDC5F898B,
	NULL,
	ES3Type_ReadPropertyName_mADECA3ECA85BF90F751A77E005C329DACA10C17E,
	ES3Type_WriteProperties_m1A3AF5D202695C48D8270F7FC2C8C2AA969852D5,
	ES3Type_ReadProperties_m7874DFD8EA6B4B8365363C35042A91336AA8641A,
	ES3Type_GetMembers_m26197DAA4F5A88CA39BFBBC6D0D4CC4450D2C620,
	ES3Type_GetMembers_m765A8DAD51D4A7C95229D5098930A24529B0255C,
	ES3PropertiesAttribute__ctor_mECA1DAF4495D8A437110C4CA5606C01467F326A6,
	ES3UnityObjectType__ctor_m32F9E90464288D451D14321A7FB8FEB4F1E2E677,
	NULL,
	NULL,
	NULL,
	ES3UnityObjectType_WriteObject_m9F8B2345A293644E321B63C464C6EAFCFB4466E6,
	ES3UnityObjectType_WriteObject_m3662F3526215112CE4BC09F7B40EED1BA18F4554,
	NULL,
	NULL,
	ES3UnityObjectType_WriteUsingDerivedType_mA4CB44FF43AB89428DFA20E7D7D4B1EC75972D6F,
	ES3Type_Random__ctor_m8D6E16C28FAB0448920C82AB30EEFA6F595B433B,
	ES3Type_Random_WriteObject_m325E3A123C425845A1FC24043860185E0CCB0131,
	NULL,
	NULL,
	ES3Type_Random__cctor_m8734D5A5E09A0CCF77F0A49A52942FD86A2478B3,
	ES3Type_RandomArray__ctor_m557E794D70052DB512565B78214303673B60B2FA,
	ES3Type_DateTime__ctor_m9C18CC743D1C085A74D5B04FB981397509CF0D3D,
	ES3Type_DateTime_Write_mECE5348F24FA3AE135CE4C8AD3D711E035F5BF8F,
	NULL,
	ES3Type_DateTime__cctor_m255B31D964DBA04AD4323A6E8F05C3CB3F010E89,
	ES3Type_DateTimeArray__ctor_mFCE6EEC75886B4BB693876B91BD7A6C5C8B80FCD,
	ES3Type_ES3Ref__ctor_m9D2F04A960C125B5B84C5C0897D93A4DA08C9197,
	ES3Type_ES3Ref_Write_mD4BA048EA7830FA95C9D831C20459EEEC13A1B02,
	NULL,
	ES3Type_ES3Ref__cctor_m81A4196E839A7B10FF06B53E79000CCEF25240BC,
	ES3Type_ES3RefArray__ctor_mF9ABBE96F63A0A456AE588FDD23E9030E5DBE811,
	ES3Type_ES3RefDictionary__ctor_m0770EC845408062B421BDD02125CBD9936FD69D8,
	ES3Type_UIntPtr__ctor_mE54EAE6B1E6E8559E78432A003C5850023DE616C,
	ES3Type_UIntPtr_Write_m93F37A6D79D65B718CC7B1B5D14A7B82CF2E5AD9,
	NULL,
	ES3Type_UIntPtr__cctor_mBFF9E7C566DED024F8FDAE32C84A7FC0C447118B,
	ES3Type_UIntPtrArray__ctor_m0112CEA07A9EE264F9B0D58C4E70D3E38C6F79CA,
	ES3Type_bool__ctor_mF461278C26A423F6239919B7CFDC8D2E1A4F1D91,
	ES3Type_bool_Write_mC69D943DA17E4864107F8A4B0AC2D38A2FA7AC41,
	NULL,
	ES3Type_bool__cctor_mE00C5BD4368C5E260FF49362AE8DE455934E7C3B,
	ES3Type_boolArray__ctor_m66F7B29B92584C8AF3EE37A076C1CC27E4C21693,
	ES3Type_byte__ctor_mE7DA116862C73104F76A1F6F9ED1EDF2567E70D9,
	ES3Type_byte_Write_mBA0F9763D2DD7AC9FD317A65DD603BCA27C9040D,
	NULL,
	ES3Type_byte__cctor_mA0506463547BE9BF6C8CDD1706429381330CEB03,
	ES3Type_byteArray__ctor_m0EE91527BFC43F0935B38EE897DEE8F4F6F6185A,
	ES3Type_byteArray_Write_mB90275FA0F19DFE6708FAA6B996E0D46B1C3D40E,
	NULL,
	ES3Type_byteArray__cctor_m6CC7CA895761C5BF32D6B14C3E1CDA1A48ACA71F,
	ES3Type_char__ctor_m08E386C741FAC3D841ED084764AB40631CCBEF78,
	ES3Type_char_Write_mF0052E73DF43AD2AA13897DCD58C31562EE66C8A,
	NULL,
	ES3Type_char__cctor_mAB5ECAE75825EED720712B555A22DFB00F769696,
	ES3Type_charArray__ctor_mF70371A7B32EBEC3A3BEABDB171950791469EE17,
	ES3Type_decimal__ctor_m37F8F74775EA330EB8E5F67B4641BBB67A10DFB9,
	ES3Type_decimal_Write_mC806607596D9B9118843678CB5F11FDDEC5DF37E,
	NULL,
	ES3Type_decimal__cctor_m2780F4E1B896CAB6FEC861EB2161B8AB27A9C933,
	ES3Type_decimalArray__ctor_m1DEE941BA730E07E015D62BEB346C5278C39D0E9,
	ES3Type_double__ctor_m10C2504EF6D227ABB74FACEEE1B4172B0D728D9D,
	ES3Type_double_Write_mCB6E2B18BFB2E755FD2392C97720DB56C24FFECB,
	NULL,
	ES3Type_double__cctor_mD3377BDA1841855B1930215018B46F458FC18214,
	ES3Type_doubleArray__ctor_mA23E81CF5C59E31109218C50E8D2BD525A714006,
	ES3Type_enum__ctor_m776DBBA523C193BD0F1171D99E422B3CB2428E12,
	ES3Type_enum_Write_m000979BACB065282E624301E41FA5F56A881CF08,
	NULL,
	ES3Type_enum__cctor_mB502B4CD7AC98934DC44CFD790E682AC8E11ACA1,
	ES3Type_float__ctor_m05CC0A8C2988F6E2C5070313E70F0AB6A591E7B9,
	ES3Type_float_Write_mC6F63490D42B229D1838995E6A502D03B85A764B,
	NULL,
	ES3Type_float__cctor_m19CB0F0D0B9688009BC363FA429B9A2FE2517787,
	ES3Type_floatArray__ctor_mEFFAEF36F1A8CF0A3F68D17A3F298626E660C8E4,
	ES3Type_int__ctor_m377719C859759583124282B916C6983F6500ABB6,
	ES3Type_int_Write_m0207E425B784352433DF0D21089C7F4E310765F4,
	NULL,
	ES3Type_int__cctor_m8ABA983C7268C852C46E5328CD67D648DA0B1BD5,
	ES3Type_intArray__ctor_mB9CFFE0DAC3BF124C5BC2A86C168762560E5AA18,
	ES3Type_IntPtr__ctor_m4458133A618E6CCC6A8C4B338AA9EFD63298B866,
	ES3Type_IntPtr_Write_m22817348510D94F1B712BACA6AAC28AD536FEDBA,
	NULL,
	ES3Type_IntPtr__cctor_m6F72C33C4920B2EBD9470990BB056AFADC1F074E,
	ES3Type_IntPtrArray__ctor_m04E25AA0341A06F385BC3712593C5AFCA4E40CF8,
	ES3Type_long__ctor_m9B6CF7B704A92267767F537427E5F8CCC54EE0E9,
	ES3Type_long_Write_m0E55A24FA1C5BD219F284F04678D1888BF29BED2,
	NULL,
	ES3Type_long__cctor_mCAE3110ECEAB7AC3D06853C436B2D2A1D32FE1D0,
	ES3Type_longArray__ctor_mC89B91FA7080A619CB50C72F26973C5A764AD37A,
	ES3Type_sbyte__ctor_m2E67AFCACBC5CBC8FC31F0B00A074C85D37B0DE9,
	ES3Type_sbyte_Write_m8913966B1610B5AAAAAAFC0BFB3705C645004A88,
	NULL,
	ES3Type_sbyte__cctor_m6FE88FBB7611D0B70BC7D8D60F43E6E9AB6093AF,
	ES3Type_sbyteArray__ctor_m4A53FBAEFB6CC43991204FDCB324480E171E80B2,
	ES3Type_short__ctor_mA9F4ADF2ABEA984E89C72BD54452658DD635779A,
	ES3Type_short_Write_mE398125447D6F2FA7661307E5CFD57C4D5E16DEA,
	NULL,
	ES3Type_short__cctor_m7499BCF937CC3D6E3C32DF33720B5E6CD7E6772E,
	ES3Type_shortArray__ctor_mC5731A8AA14025E278762E26B68B912C64540F94,
	ES3Type_string__ctor_mBBC50231A331B512505007CE83D0D27FC1DAE929,
	ES3Type_string_Write_mE335ABDCC8F0FA54C0BA18AE73D82723E7CAE677,
	NULL,
	ES3Type_string__cctor_m1A150BF757F3A51E058CE55D3AF98067C89FBF72,
	ES3Type_StringArray__ctor_m7D3F2324B8779E075AEE391C7379D01644D7221D,
	ES3Type_uint__ctor_m0F6B7CEE0FB14AD38560DE0680B40C88308B2EFF,
	ES3Type_uint_Write_mBFB92390CFAB64CE3231FE14FF22EC1F992B9E39,
	NULL,
	ES3Type_uint__cctor_m703A5D9B98A6615B603AD89DEBBEC139CB675206,
	ES3Type_uintArray__ctor_m96FECC77B74E77FFB77005F2FFF80CAB6CA810BA,
	ES3Type_ulong__ctor_m6169C3C5F8D0E2E9D091C3C542002F8D1489786C,
	ES3Type_ulong_Write_m119A54060DC97C56F18B508C2A3CFE2EDE0C0AA9,
	NULL,
	ES3Type_ulong__cctor_mAD1604A20A57FF0330F34FB2EFA8BA26FD1216D1,
	ES3Type_ulongArray__ctor_mC666EB10B7CC82DAEB25A9120E9B0C2F84C564C3,
	ES3Type_ushort__ctor_m1791120125ECD1FDEAB6C5B5A7901FA7BFC4573C,
	ES3Type_ushort_Write_mBCB5E954DE1E1626B7091A09980BFFCA2E29FEC1,
	NULL,
	ES3Type_ushort__cctor_mDB53D43D06AD63FA755E752D2B72F9E1FDA6E048,
	ES3Type_ushortArray__ctor_mE0AB32EC7E70AE12BDF4F689029F3BC5182D3A76,
	ES3ReflectedComponentType__ctor_m609E76B515B5A69BBB92B6A48A901D5C2C3EDA5D,
	ES3ReflectedComponentType_WriteComponent_m695DE406FECB8C3DF3B07941FDE58ADC9912A07B,
	NULL,
	ES3ReflectedObjectType__ctor_mF23E33D4DDEA06E403A314EAEED52A25863788BA,
	ES3ReflectedObjectType_WriteObject_mD76EAD1F9CD470156CA0FB32F823845A61DC5942,
	NULL,
	NULL,
	ES3ReflectedScriptableObjectType__ctor_mB2B13CFF3BD3F4CDDAB3A0C3BFEF13C4C7D717DB,
	ES3ReflectedScriptableObjectType_WriteScriptableObject_m859A80339A1D392EFEBC33668ACE45E0BF3993F5,
	NULL,
	ES3ReflectedType__ctor_mE2879DF382BE722B3CD61914B36E902299DFF32C,
	ES3ReflectedType__ctor_mC83959EC1462CDECB0C8A23C7BE56A389110A116,
	ES3ReflectedType_Write_m72E9C60F513FAA7F37668F52811A9EE9FC3A5942,
	NULL,
	NULL,
	ES3ReflectedUnityObjectType__ctor_mFCE3647F4101E29263E65FFBC5E7C91974A16C3C,
	ES3ReflectedUnityObjectType_WriteUnityObject_m92E3BDAF3A8DC196E878DFAC5821AFA143E6BF12,
	NULL,
	NULL,
	ES3ReflectedValueType__ctor_m0D230E419ADCAADB53C3497781B14F90924E798E,
	ES3ReflectedValueType_Write_m280E91FB066034BF45E93B336DC6FAC5E1AA188C,
	NULL,
	NULL,
	ES3Type_BoxCollider__ctor_m7F49B59EDF60F2E1F57FED5D31AB3BC2EF01DCC5,
	ES3Type_BoxCollider_WriteComponent_mF6B9F0CC207AF73159780084A599176C57F96D05,
	NULL,
	ES3Type_BoxCollider__cctor_m7C917F11AEF9580E3DD9420C3815C29E1F6C2E0B,
	ES3Type_BoxCollider2D__ctor_mA755D5969681F0E91AB4E82CBA4A1F0D2E5B1941,
	ES3Type_BoxCollider2D_WriteComponent_m45AED18E2F1D3CDBBA4D007BCCFE1AA3ED3C6034,
	NULL,
	ES3Type_BoxCollider2D__cctor_m0528F04523FA4A5719858F2DD8F3CAE4BB602FD7,
	ES3Type_Camera__ctor_mFD8408A3898E97207F171EA5D0363C16703CA741,
	ES3Type_Camera_WriteComponent_mE6D85C8DF67178B6E94044112834A5784C9252B4,
	NULL,
	ES3Type_Camera__cctor_m5BB985DA17A43A6A810963148D7982FFB928728B,
	ES3Type_CapsuleCollider__ctor_mF6C53F1961FF1B8CC92B9C7FFE3DB8E336D381EF,
	ES3Type_CapsuleCollider_WriteComponent_m5B76791A6BC0D1BEB7021006F6DEB90A44F2ACFE,
	NULL,
	ES3Type_CapsuleCollider__cctor_m582CFA8CD9086A86DF7756E855203B95B1A7102A,
	ES3Type_EventSystem__ctor_mA491EED2AF652B3296BC87C041DFFFE7C3D5005D,
	ES3Type_EventSystem_WriteComponent_mDA81499D58230FA6A618FE1B7D528D4DDBC7B173,
	NULL,
	ES3Type_EventSystem__cctor_m1E6CA8F5DA897E0996AD3FD84D2FED0B93A22BC4,
	ES3Type_Image__ctor_m7942C8BAA691F3F1D4D8F1554FBA659A4A830DE9,
	ES3Type_Image_WriteComponent_mD3959FE5C8A0D8DABB232B4CB11E9C2494EA8402,
	NULL,
	ES3Type_Image__cctor_mAA503144D6BFED66EE29FC72D434F16E27A7CD35,
	ES3Type_ImageArray__ctor_m4C35367F0AED5C97A0CD47E596B72D6B76B64470,
	ES3Type_MeshCollider__ctor_m6CF77304F6FF527271125BFDB70A0B6DDC770B64,
	ES3Type_MeshCollider_WriteComponent_m1CF7624475F71BCA68BA41F9CF81529DFCB39960,
	NULL,
	ES3Type_MeshCollider__cctor_m9C6AEE7EC4671F2774083809FBEA090EE9F89B8C,
	ES3Type_MeshColliderArray__ctor_m53B85EA5EFCB294BEC8C6A3D8A08832C283D49D5,
	ES3Type_MeshFilter__ctor_mBF43F8E4BEF79EEE24ADB66B62E3058FC1C36AB2,
	ES3Type_MeshFilter_WriteComponent_mA0846588E181E7C6B7E49C9BB48BFFA15B23208C,
	NULL,
	ES3Type_MeshFilter__cctor_m68FBCF9BA1304FF3E56BFC82EC786F6617E46666,
	ES3Type_MeshFilterArray__ctor_m9D35E8990F1EFDF535B76C6ABE5DAA729E8B7CD8,
	ES3Type_MeshRenderer__ctor_m51A57DB11F0D3FE472AB2FC9CE1E47AC254B043A,
	ES3Type_MeshRenderer_WriteComponent_mC7A2CF00C1CECE6FCA608826D8D8A5DA64719047,
	NULL,
	ES3Type_MeshRenderer__cctor_m6313D20626DD63804E5399E3A2E03A57AB14321E,
	ES3Type_MeshRendererArray__ctor_m61A76425226AE070A1758EB6AB03C282864A28AB,
	ES3Type_ParticleSystem__ctor_m238935593A40246A958912FCA794E966DE302718,
	ES3Type_ParticleSystem_WriteComponent_m09E0CA7580B39CB8CEB78A7077C89D1DF3FE23A9,
	NULL,
	ES3Type_ParticleSystem__cctor_mA23926920DCA056DB5FBEEAA1782FA28A5BF2457,
	ES3Type_PolygonCollider2D__ctor_mBF2D4578296450B1AA14804DD6E86CE171F2007F,
	ES3Type_PolygonCollider2D_WriteComponent_mC6FF9AB99FA36F5C41351E984774E71F9C50074E,
	NULL,
	ES3Type_PolygonCollider2D__cctor_m71F762EC43F6FADFEAE9C84B66034033E6C5A344,
	ES3Type_PolygonCollider2DArray__ctor_m4FCD8FD3D09C798E5B0E2F3913860F596548A494,
	ES3Type_RawImage__ctor_m1825BFAF73656982E97D030356A8597E30FACC1D,
	ES3Type_RawImage_WriteComponent_m49298958D623294BC3CE77632C529201E6C015A2,
	NULL,
	ES3Type_RawImage__cctor_m58A1787681A945783646D2B6AFA9B4274EA94D8C,
	ES3Type_RawImageArray__ctor_mE7A7C55B138E1A692702F2333A8221030994887E,
	ES3Type_SphereCollider__ctor_m6BDF2C71A82E8BF03B2784D793A870CAC6E02154,
	ES3Type_SphereCollider_WriteComponent_mCF18EA8014E9C13A0A5509DA1D2B643FD6397F92,
	NULL,
	ES3Type_SphereCollider__cctor_mD1060A8255549B8047F1FE9E5604627EBA795981,
	ES3Type_Text__ctor_m91C0B12693722486BAC00A55907726AC133CEC44,
	ES3Type_Text_WriteComponent_m88F8B8D3897409DC8BD5E0F1FB16C6C1C9ED83D6,
	NULL,
	ES3Type_Text__cctor_m5CADB639D5CFF540729B9E9B60CF42F4C00E05EF,
	ES3Type_Transform__ctor_m262A853CCEA7F0EA10B29A15684081B90F888B6E,
	ES3Type_Transform_WriteComponent_mC672B2DAB50A5C7E3D54B7162FE06870005D6B5E,
	NULL,
	ES3Type_Transform__cctor_mFDECB3EA95E1196431BDF779A2E0ACF8CEAAAA89,
	ES3Type_AnimationCurve__ctor_m0BE1398D084B259EE6E2D41CF6CD41ED8DE42BE0,
	ES3Type_AnimationCurve_Write_m2E11AC38A7C0C15129CAC4CBA3BD444B315BFF61,
	NULL,
	NULL,
	ES3Type_AnimationCurve__cctor_m3740ED1A46FB71331D124F6016DD84D8594847E6,
	ES3Type_AudioClip__ctor_m0BFF7D9AC3FA21AA7A443B02D61B5726DC4B07B5,
	ES3Type_AudioClip_WriteUnityObject_mCB36C2145BFE332F0C76D5F51CDA63E0A403EEB4,
	NULL,
	NULL,
	ES3Type_AudioClip__cctor_mA49F630ABDB12E3633906CED1CFCEBDF2861062B,
	ES3Type_AudioClipArray__ctor_m4E3386E3826F128D0928BECCB80FC7B2F8799026,
	ES3Type_BoneWeight__ctor_m6FC8C9557E73798EBFF47C21FDC8AC53E4D69BFD,
	ES3Type_BoneWeight_Write_m239F73EAE61ADB87C284FECAB413976C9F09D8BA,
	NULL,
	ES3Type_BoneWeight__cctor_mD53B23C18708B40647A3218C9D2918768F8727BC,
	ES3Type_BoneWeightArray__ctor_m3D1240869A5A010B4C5EF29D5C3CA578A90FCC33,
	ES3Type_Bounds__ctor_m467FAE7E30D9509FD6005E6F3DFE87B89218C367,
	ES3Type_Bounds_Write_m644EB66E7EBA9C77134D5E6BF64F86285B9E6D37,
	NULL,
	ES3Type_Bounds__cctor_mAD02B8AA057D2820A977F7F19369B21636D5FC64,
	ES3Type_BoundsArray__ctor_m41A7CB22D3A435C1B7FD805531CAA5944B4892A8,
	ES3Type_CollisionModule__ctor_mC98E478E95C6DBFAA754EA30A9D0D3D61F7F31D8,
	ES3Type_CollisionModule_Write_m8F77E870AA324AFBE500D786D46EBB5D8B655CD8,
	NULL,
	NULL,
	ES3Type_CollisionModule__cctor_m187E9E43EE4C2E128F0EE6656A8C920665C338EE,
	ES3Type_Color__ctor_m7B3FC8676885FA18C577EB0F40973087DA101F95,
	ES3Type_Color_Write_m1A511FD47F4548ABA13042CEF150FFB5712F5144,
	NULL,
	ES3Type_Color__cctor_m9A958DA9E6DA45FD53BCEE39C0A3E8F4C4A312D4,
	ES3Type_ColorArray__ctor_mA03FE56FF7FD4472F43E887F303605E79B9E62EF,
	ES3Type_Color32__ctor_m7E716D0E29DED5986CB93CED706AF3F524CD5DFB,
	ES3Type_Color32_Write_mE941726E27729486FE248181AE8217F2982DBD60,
	NULL,
	ES3Type_Color32_Equals_m591DDDC7BA73FD82E4E720A5084791DED5D3C762,
	ES3Type_Color32__cctor_m483E3EA851B0096504EE6628535D105D4C228098,
	ES3Type_Color32Array__ctor_m821F3F5C1CFD5BC772E91ED9EBCBF41CB32AEA29,
	ES3Type_ColorBySpeedModule__ctor_mE9A145C1B648716D367339A560E46E60ED49EC1A,
	ES3Type_ColorBySpeedModule_Write_m3DCD111A43B65F66C64EAD0F27903915F717DC56,
	NULL,
	NULL,
	ES3Type_ColorBySpeedModule__cctor_m419B161CE477A47631DBCF5E0970D3B1D7D72B63,
	ES3Type_ColorOverLifetimeModule__ctor_m0ADA954F52F1013D7CDA42ADE817A82A1B25E11D,
	ES3Type_ColorOverLifetimeModule_Write_m312B206CC2BB0B374CE0F48CF53EDCA635421AA6,
	NULL,
	NULL,
	ES3Type_ColorOverLifetimeModule__cctor_mA0E9D70CB68675523255321F67F8C5B341835D58,
	ES3Type_EmissionModule__ctor_m1049A5CB26F86A90109D202A0C3DD338129C6CF0,
	ES3Type_EmissionModule_Write_mCDBEDF140170B9888332E50C925E5C2A2AD9FA08,
	NULL,
	NULL,
	ES3Type_EmissionModule__cctor_mF63ADEA117495EF75E2939E6F8FD96F27D08A3B2,
	ES3Type_ExternalForcesModule__ctor_mD2A99AC305F61ECD698A1F606B1E79EFC9194A8C,
	ES3Type_ExternalForcesModule_Write_mD28D7FC9AB8663878A032F45827C79AEADEAA5E1,
	NULL,
	NULL,
	ES3Type_ExternalForcesModule__cctor_mA82F08C97C3C6C3B80710A685C269576CD8C5EC0,
	ES3Type_Flare__ctor_mE60CA45525396C0D4E7B8A83B00C8064F97BEC7E,
	ES3Type_Flare_Write_mFEE891F63625FFE58C1AF7638283B196710B47A8,
	NULL,
	NULL,
	ES3Type_Flare__cctor_mC2D99E91546BD589DE853922E029BC2C03F58648,
	ES3Type_FlareArray__ctor_m4FC90BA596A1315D036415CC9C5EDCE453B77E77,
	ES3Type_Font__ctor_mA495381BA1AF96E050898C617AF085A9A2E6AC76,
	ES3Type_Font_WriteUnityObject_m93678DFDF70FE41130F4DCC4D96697A997FCC38E,
	NULL,
	NULL,
	ES3Type_Font__cctor_mA7A5DF91D6B44050C5CD12186B18EDE34D09A290,
	ES3Type_FontArray__ctor_mEAF57272FFE55789E21FC656991A7CFAED06B9B9,
	ES3Type_ForceOverLifetimeModule__ctor_m95BDC6C16741B0A107E3FC8DC5B5234D608E94A6,
	ES3Type_ForceOverLifetimeModule_Write_mF16C5BC052DDCF7A699853BAEC0C976DD36DFC55,
	NULL,
	NULL,
	ES3Type_ForceOverLifetimeModule__cctor_mB5B4DE504A3F5E545FFCB0EE951C07CA386C78A4,
	ES3Type_GameObject__ctor_mAA52902D8800085EC2A6120FE21A1388481463DB,
	ES3Type_GameObject_WriteObject_m31528FE033BE49814F486BF266E501776C0AB8F0,
	NULL,
	NULL,
	ES3Type_GameObject_CreateNewGameObject_m5C123C7F04129E78FECFE61ECA28795FA4927540,
	ES3Type_GameObject_GetChildren_mF3227937B990C09C99DB8C035B3BDB952C277AD7,
	ES3Type_GameObject_WriteUnityObject_mB1F056669DE8C316996C28D50E68E25B5068A3E0,
	NULL,
	NULL,
	ES3Type_GameObject__cctor_m4EAC0FCE242595F4ED8EEA945F4BA14A667360C3,
	ES3Type_GameObjectArray__ctor_m64F622D2DB65BABFA5F85E041FA764B099C07BCF,
	ES3Type_Gradient__ctor_m110AB9C9D615F9257F627E4866622CE45538A0A5,
	ES3Type_Gradient_Write_m5B9859FED06F692F16AEA3576ADC4EE0B52D3ED9,
	NULL,
	NULL,
	ES3Type_Gradient__cctor_mD97FA3F3940520E8687026F1F3CB1B4DF15FB757,
	ES3Type_GradientAlphaKey__ctor_m67A18247E478928C7500DF96D1FB1C11A93D3487,
	ES3Type_GradientAlphaKey_Write_m119804060007BAA96FE460A2B046B9E0C7EF91B6,
	NULL,
	ES3Type_GradientAlphaKey__cctor_m86043A4A0F2F11A191B04B50A7D385F81E737F62,
	ES3Type_GradientAlphaKeyArray__ctor_m08FBE2F639C7F368D25B0AC895BC213ABE688285,
	ES3Type_GradientColorKey__ctor_mCBE1AC34FC94FB80248B504C8FE41E55F02440DF,
	ES3Type_GradientColorKey_Write_m3675CFEC69FFFB76F1CA49A764E96A30614EFD2E,
	NULL,
	ES3Type_GradientColorKey__cctor_m5F199C183F0077B6DFC7C883743458AD2F997AA7,
	ES3Type_GradientColorKeyArray__ctor_m85A773B81720A4BA163AA595C1A655C827575B5B,
	ES3Type_InheritVelocityModule__ctor_mFC1CA15E7FCC750BFA88FCCC95605E08EF0E6D48,
	ES3Type_InheritVelocityModule_Write_mCEE2906C60954D3420C3F659811312192D67DF91,
	NULL,
	NULL,
	ES3Type_InheritVelocityModule__cctor_mB9ED7F909E668D428413D23579B46F37E43A9EFB,
	ES3Type_Keyframe__ctor_m3BE757B0448859C92D7F55ABEA33C264AD130798,
	ES3Type_Keyframe_Write_mC363B74D02BBBCAD9A2B04E1EAB6CE36DD604702,
	NULL,
	ES3Type_Keyframe__cctor_mFD2212176E61D804E6D057C91095CCDCDC63E745,
	ES3Type_KeyframeArray__ctor_mA24398AFBE71AA7F80DF3187A082A9E1577B4BED,
	ES3Type_LayerMask__ctor_m3BC49A2C5EE78F0B79ADFD2ED9E2DAA43240D708,
	ES3Type_LayerMask_Write_m0F67B59C69244C34C3C7A98904D6CDE294C4BFBD,
	NULL,
	ES3Type_LayerMask__cctor_mCDAFCD1C8C3CEB28896A0E363F532746A85ACF59,
	ES3Type_Light__ctor_m3DD2E7B81E8BE81E282C095DF11DA7FF1B722AB8,
	ES3Type_Light_WriteComponent_m38DBB3D874F5F7E0E4DE1DD875184CAFDF9D2268,
	NULL,
	ES3Type_Light__cctor_mA3F974EA0F7E0FF6F2C8FB73AC56C5EA5BCC1298,
	ES3Type_LightsModule__ctor_mE4B0C8DFFF9D4D554A24673181E622B81A0C5C8E,
	ES3Type_LightsModule_Write_mB591D4C1E27AEEB51D87D3F43D1068EC74750924,
	NULL,
	NULL,
	ES3Type_LightsModule__cctor_m2A7BCCE48370B5D4B25192B8EBD4C2920DF6C6B4,
	ES3Type_LimitVelocityOverLifetimeModule__ctor_m0D7438AAFCF943A66A0130E29DA683FA5D9C8CD7,
	ES3Type_LimitVelocityOverLifetimeModule_Write_m53A1CE287F6CA4AB3A5A006A44BA2AE5AEC314B9,
	NULL,
	NULL,
	ES3Type_LimitVelocityOverLifetimeModule__cctor_mBAEE3265788AC0F0E82F0A5346D9BCA88C368FF0,
	ES3Type_MainModule__ctor_mBDB335067E4046173B0DFA634FC2BF7B5ECC7971,
	ES3Type_MainModule_Write_m1C9D9BEBD977536A37FA793BF43D6680C6B04779,
	NULL,
	NULL,
	ES3Type_MainModule__cctor_mD775812049E7BFE51A7C679751B163BB2891CEA5,
	ES3Type_Material__ctor_m78AEBBD7DBA1E10F01370EBB0F13DEAA543A78CE,
	ES3Type_Material_WriteUnityObject_m84329BCE685179E49BC8950B799308285DEC89C9,
	NULL,
	NULL,
	ES3Type_Material__cctor_mF1E38ABDF2971076FEAAF35D42765AE7162377E9,
	ES3Type_MaterialArray__ctor_m46C16208AB51FB217D04ADF5E820A8FBA6EBB67B,
	ES3Type_Matrix4x4__ctor_mC82D113B62080CC10EAB8FD98133C5BA767EE7CD,
	ES3Type_Matrix4x4_Write_m684712A7D37DC3FBC1A6304B4DFDD26CF531EA56,
	NULL,
	ES3Type_Matrix4x4__cctor_m48595FE31662A303C329274871462EC8BEBB7EEA,
	ES3Type_Matrix4x4Array__ctor_mCF02AB264EF543DF556339BCD38D41A3687D0A10,
	ES3Type_Mesh__ctor_mB842D25B6FD16BFD816B0E380D5CB1E6049B900C,
	ES3Type_Mesh_WriteUnityObject_m5462E4C396CFEED903B9BC64C65CA0E9658D5AF1,
	NULL,
	NULL,
	ES3Type_Mesh__cctor_m3415CBAFFF26967A2F46494806C421C57EA6C1D5,
	ES3Type_MinMaxCurve__ctor_m0D22187676E6CBD8CD6615E3E841BFBE5E10B613,
	ES3Type_MinMaxCurve_Write_m6505BD9D23BB31DFE8161810A81C9E467D43C764,
	NULL,
	NULL,
	ES3Type_MinMaxCurve__cctor_m5D350DDEF19E41CAE0502B6C9F0C784754604ECF,
	ES3Type_MinMaxGradient__ctor_mE27422408BB947FB80C047A3B34E7178DB11C606,
	ES3Type_MinMaxGradient_Write_m6EAFBDDEEABB94515F4B01BAF91F71F0F0F23323,
	NULL,
	ES3Type_MinMaxGradient__cctor_m2CED6CABDD4AB43993BCA15531A6266D873E1BE7,
	ES3Type_NoiseModule__ctor_m13B9423D29205C0BB952F75FFCD5D19BD8E1F020,
	ES3Type_NoiseModule_Write_m34C034E1E1D729BC9CFA550F2023410E629D8F20,
	NULL,
	NULL,
	ES3Type_NoiseModule__cctor_m1ECDB2649C7B01622E20C54F861045B874487BCE,
	ES3Type_PhysicMaterial__ctor_m35825A289AA97B9E5F829328DF0C388EEF39994C,
	ES3Type_PhysicMaterial_WriteObject_m7A3943B9335E7A1B337F081ADED2C40F89F82168,
	NULL,
	NULL,
	ES3Type_PhysicMaterial__cctor_m07D0663CC61161779CD021ADCBD28947F1155AA0,
	ES3Type_PhysicMaterialArray__ctor_mE3F39A13477458BD76F87C0140B54F5D2575498B,
	ES3Type_PhysicsMaterial2D__ctor_m7DACAF9E497EB14E28601B72F69C25947695AE5D,
	ES3Type_PhysicsMaterial2D_WriteObject_mAD59E7A4291FF1E3D3D5EFE190C37B56223113B1,
	NULL,
	NULL,
	ES3Type_PhysicsMaterial2D__cctor_mC237C5B979125BE2FA5B003EA3C354B94CD059D8,
	ES3Type_PhysicsMaterial2DArray__ctor_m905FD6AE1EE306CC001C517CAD64ADF3CB304137,
	ES3Type_Quaternion__ctor_m6220CA0258FE427536005C13C6A1F922BAFE8486,
	ES3Type_Quaternion_Write_mFA1BAEE914E8988A6734E3AD665F64FB8DB8EDA4,
	NULL,
	ES3Type_Quaternion__cctor_m47D0BDC9E6C0354EF1AD4858EF281992DA192302,
	ES3Type_QuaternionArray__ctor_m689487BD82EEA2E1F952DE559BA40E93B24026C8,
	ES3Type_Rect__ctor_m4D363C11EDBDCAC1C2ACA14515304BDC0FC91983,
	ES3Type_Rect_Write_m8786EDF1FA710954952EEB76BE0AE38D72144F2A,
	NULL,
	ES3Type_Rect__cctor_m5363881021C14A3211DA61CC2B38AC7F76D813E7,
	ES3Type_RectTransform__ctor_m0177971461DDFD79A892F1CAA1E162368FB55017,
	ES3Type_RectTransform_WriteComponent_mE5566261EF742B5C2DF1B1828D90045369B44672,
	NULL,
	ES3Type_RectTransform__cctor_mF3089825534CF4B9AB472DE5A7E25B3CC1389E5E,
	ES3Type_RotationBySpeedModule__ctor_mD7D75E9BC0F4205FBFD409F88488D9D4F41ED7ED,
	ES3Type_RotationBySpeedModule_Write_m58F70BEBD0E667B3C8AC1632409B82C47ADE4D13,
	NULL,
	NULL,
	ES3Type_RotationBySpeedModule__cctor_m45D15D48B0C206E01A606796A5F288881F14CDD0,
	ES3Type_RotationOverLifetimeModule__ctor_m4BC62B634B258087C236E65CCD57FAEB89F63A2D,
	ES3Type_RotationOverLifetimeModule_Write_mD06EB4D3B252901A99C563A992100DA296D2A590,
	NULL,
	NULL,
	ES3Type_RotationOverLifetimeModule__cctor_mFC2C6D16AF41E2170C6AA4844C10BE0F710F7F93,
	ES3Type_Shader__ctor_m184228173F234807AAE8F29F7C30D7AC63C00021,
	ES3Type_Shader_Write_m2A41ACD16BC5AEBADCD4D5BC445658ACDD2856E1,
	NULL,
	NULL,
	ES3Type_Shader__cctor_mEA0E4BC73C37DE82376ABA38CA291D26409B9638,
	ES3Type_ShaderArray__ctor_m9F418B10CB57EA26D68BA4D8ECC273A140E6E882,
	ES3Type_ShapeModule__ctor_mE1206D7CFB317044DD32CC1AC38F4E797973A097,
	ES3Type_ShapeModule_Write_m45098488B29CB93233C8FCD10C223C309DAA3A52,
	NULL,
	NULL,
	ES3Type_ShapeModule__cctor_mB095CD24DB760978E53C81A81A73CCB21E4A4099,
	ES3Type_SizeBySpeedModule__ctor_mF3EB07C4141EB15C6CA08C3C8902647EF60CC384,
	ES3Type_SizeBySpeedModule_Write_m8223285FDEF4A32ACC92CED2AABB1CF48079617C,
	NULL,
	NULL,
	ES3Type_SizeBySpeedModule__cctor_m5CEEFD445337663CD93C7AE53FCDD14BB02E4C4D,
	ES3Type_SizeOverLifetimeModule__ctor_m3BF12F1F2691CAA7B340D6AC69648EC6ECA3C367,
	ES3Type_SizeOverLifetimeModule_Write_m8355CB911DCBF412D61A817D9543AE4FC5B19205,
	NULL,
	NULL,
	ES3Type_SizeOverLifetimeModule__cctor_m019611468D9C7A9D7CD0615EE79E9AEAE052A628,
	ES3Type_SkinnedMeshRenderer__ctor_m6E962E3F9801B2784B0D37AB8F9AAEF1374D83DF,
	ES3Type_SkinnedMeshRenderer_WriteComponent_m07EF5271929295CC224FB1E282DCF2CFFBE54699,
	NULL,
	ES3Type_SkinnedMeshRenderer__cctor_mE5707A7412ABE805618326AEADB71CE81E428EFA,
	ES3Type_SkinnedMeshRendererArray__ctor_mCAF302C8F53FE94E2D7AB1D303A183DE58D40B42,
	ES3Type_Sprite__ctor_m89015E5BFBDB29039B63AD0C673C670C9BCF310C,
	ES3Type_Sprite_WriteUnityObject_m87455FE051A3F76B0A2D58C8A4368E81FB45AC45,
	NULL,
	NULL,
	ES3Type_Sprite__cctor_m4FE9D599B813B2072BD15A3C9083B5D995BFA64E,
	ES3Type_SpriteRenderer__ctor_m287A19497A77D1AF6445FF14DDF8D92489B367F3,
	ES3Type_SpriteRenderer_WriteComponent_mC62C01406041595A364181A8884548842125B0E9,
	NULL,
	ES3Type_SpriteRenderer__cctor_mD60057B703BA03FE38F1AB578FD595CCCB293829,
	ES3Type_SpriteRendererArray__ctor_m5F14EF74558191ED02A7D7C8CDE63BA5A4BF6E87,
	ES3Type_SubEmittersModule__ctor_m7BEBB80F026B6E6019BCE601C3DFC1A56AAA6287,
	ES3Type_SubEmittersModule_Write_mC02863802E98F67A046D88801E1CC4D481E73D09,
	NULL,
	NULL,
	ES3Type_SubEmittersModule__cctor_mBC0957D231233E5DB00149202DADA7F638675A71,
	ES3Type_Texture__ctor_m575E7A1AED5146965A411CB7C0A1E9A5F72414D3,
	ES3Type_Texture_Write_m80E826C76B2A2C9A668CC9F126C1A18A76D202AA,
	NULL,
	NULL,
	ES3Type_Texture__cctor_m75F27A34C6E1D8BD2E23F36469D00B5F270D086E,
	ES3Type_TextureArray__ctor_m7C5B02564127EEA9E4966225E3CB72816A280FBB,
	ES3Type_Texture2D__ctor_m3BABC7A255BF766360CB763D3B61B073945F95E7,
	ES3Type_Texture2D_WriteUnityObject_mD123C9020BDF1F5C7EA3724BDE5F91EE8AC25503,
	NULL,
	NULL,
	ES3Type_Texture2D_IsReadable_m95F6497DE57EF3390D2A614B91637E8C6C24DE25,
	ES3Type_Texture2D__cctor_m4BAAE048CA19A2583C6AE4AE3FED5EC017EDBC98,
	ES3Type_Texture2DArray__ctor_m8923DBA70FE6E2089412F1E5CC61198260B8CCFD,
	ES3Type_TextureSheetAnimationModule__ctor_m63ACF4715E13911E7251AF79264D7CAF53F1456C,
	ES3Type_TextureSheetAnimationModule_Write_m8783EBFC26F9AAE1035CDCE4F26927BDCC88ACBE,
	NULL,
	NULL,
	ES3Type_TextureSheetAnimationModule__cctor_mBCB3BAAC27A9C7E85B8BB2B7EE4C8F1B4620B2EF,
	ES3Type_TrailModule__ctor_mFE44C0359B0FFAEB74B536A328FCAC8B0B2A9382,
	ES3Type_TrailModule_Write_mCED5039A74BF9C7DBB32D6BA4D9BC40C28644A2F,
	NULL,
	NULL,
	ES3Type_TrailModule__cctor_m2993517CE074536DE4CDB121A705B764D9CC163F,
	ES3Type_TriggerModule__ctor_m7EC4E96F96388AA336D3E382CAD7194CE2CE2075,
	ES3Type_TriggerModule_Write_m2DE5C2E6A95FDA3709D190AF3288CE92E6501852,
	NULL,
	NULL,
	ES3Type_TriggerModule__cctor_m17AE6580D682B8F5B63F6A3D6F5DA50CF7DBA04B,
	ES3Type_Vector2__ctor_m4CF1FAE34C6F1024D7E79B81931963A3ECD17953,
	ES3Type_Vector2_Write_m3D44D5043AEE1FB56AE1552CD2FE626B6607F011,
	NULL,
	ES3Type_Vector2__cctor_mE1276FF4874CB05EABAD7689AB3DE49112D248A1,
	ES3Type_Vector2Array__ctor_m0B5980492D4756C0620D21599BD0D3E962780D30,
	ES3Type_Vector2Int__ctor_mDF9045B451096E73C41B0FB1F6EC54B7FC4BFE67,
	ES3Type_Vector2Int_Write_mAAA5C18513BC345A530A46035A38E1EEAA18A9B6,
	NULL,
	ES3Type_Vector2Int__cctor_mC9A2863F60E99223B35325D43C4EA29B11F2E0B3,
	ES3Type_Vector2IntArray__ctor_mA438889206B21979FFF8C158015A94A12C7E2321,
	ES3Type_Vector3__ctor_m7EDC5B207AE8B67A5D2788F71AFDE9D054EE0472,
	ES3Type_Vector3_Write_m76AEA0845E34F13C33EA5779AE38664111EAD921,
	NULL,
	ES3Type_Vector3__cctor_m25669987B1D861BE3CA3DDB1DD0545DE11D0736D,
	ES3Type_Vector3Array__ctor_mDF32324C7243E8B546D8FB2A407232434B71E1CC,
	ES3Type_Vector3Int__ctor_m23AFF374E7F2C9EF9F7C13AEB0642DE17BC88AF7,
	ES3Type_Vector3Int_Write_m618B770521CF19276FFF81EAC0561F81DF331BCC,
	NULL,
	ES3Type_Vector3Int__cctor_m8714EA08E874B125AF6139468BB1D7445DC46BCA,
	ES3Type_Vector3IntArray__ctor_mB188F701ABDEBB75427D1421113368169A206089,
	ES3Type_Vector4__ctor_mA2A6D8214BB0EED7AC9770340E5915225BA67A69,
	ES3Type_Vector4_Write_m02DDF5D37D292F2DA9C225637B57278B5894CD05,
	NULL,
	ES3Type_Vector4_Equals_m2DDC340C5C5615FCA19FBDE470A58790870AA4FB,
	ES3Type_Vector4__cctor_m77FDBE72C206DF0FF25FFBCE9A9FBCDC5D28BFAA,
	ES3Type_Vector4Array__ctor_m341AE128A36D1682F2AA7E6E3ED7A08A9F4F2BBA,
	ES3Type_VelocityOverLifetimeModule__ctor_m36062CF3E34DF22640EDC1838DA43CB044D77058,
	ES3Type_VelocityOverLifetimeModule_Write_mB3B793E195D390747AFD12047C836DF76E7A04A7,
	NULL,
	NULL,
	ES3Type_VelocityOverLifetimeModule__cctor_m80DBBD4A9714AC5E64F0E80F7DCF3CCCD3F025B7,
	ES3Debug_Log_mCD7C61578DD7976547700C80316C41CCBD0DE7AC,
	ES3Debug_LogWarning_mA02F0B62778A55BD061330D7FB01ABE09547A0E1,
	ES3Debug_LogError_m6C096EC10B905B1FAA7F736F9949640FCEEB6D21,
	ES3Debug_Indent_mCA5244FDB6E90BB5585AD21470ACE2AE0195DA6B,
	ES3Hash_SHA1Hash_mED3D75B460E3DAF52049CE048EE4F4E5E6D0BB00,
	NULL,
	NULL,
	EncryptionAlgorithm_CopyStream_m3B2007F7657F9A17A48DAC2CA2373BA1438749F2,
	EncryptionAlgorithm__ctor_m756DE35582AAE650CEA7EADD2798B19D86C34CF4,
	AESEncryptionAlgorithm_Encrypt_m6AA3CD54A1C43E95788852276B65B6024C660430,
	AESEncryptionAlgorithm_Decrypt_m8685A3291C0D95A4B102D5ADC85167A7C66878F4,
	AESEncryptionAlgorithm__ctor_mC7E03A00030639CA552110368B093C1DE57C49C0,
	UnbufferedCryptoStream__ctor_m78F855CABFD60A1BC0716BEC65501ACD0A6CC53F,
	UnbufferedCryptoStream_Dispose_m809D283D2CCA55B5D193704DE8DBE0F857BE46E8,
	ES3Data__ctor_m16AF906373359317ADEB63838FC1630EA2969C14_AdjustorThunk,
	ES3Data__ctor_m1CF0FDCFDA248E120941C48F1CCD29A4A7A22B0C_AdjustorThunk,
	ES3IO_GetTimestamp_m953CB1ABC0D95994E8E9C2BCD924E6A1B6066CD2,
	ES3IO_GetExtension_m95CF0B403A1A46DB06AE99B3A51801810711F4C6,
	ES3IO_DeleteFile_m6D735FF7C91F96CE3CB570D3CB2980D4162C7512,
	ES3IO_FileExists_mD330225B2159D0F32949109EA25FAF122E0B94C3,
	ES3IO_MoveFile_mD8CEE6ACC05835575A7F4EBA455F9A08AF9CE565,
	ES3IO_CopyFile_mD9A1D5E2EA769034E4E4C5F093FC9C40436B8ADD,
	ES3IO_MoveDirectory_mF4BEC0A7E128ACD28718F612EB506742A10EA3C4,
	ES3IO_CreateDirectory_mA759449C8C7A4E36F31210D8851B9E763D97AE29,
	ES3IO_DirectoryExists_mC4CEA972AE143FF64E296C8A6C3017BD5AA91478,
	ES3IO_GetDirectoryPath_m60479932D52DB290DA79FB6E0877D678C89ABF6A,
	ES3IO_UsesForwardSlash_m6DECE9532F0AF85DBDD7E5E14C3214F8E982CAA3,
	ES3IO_CombinePathAndFilename_mC313E2941A4F0BD4B939B82DA805047E59251BB6,
	ES3IO_GetDirectories_mFC556886532E91AFA8A64A1D7079F0C9A63BDE28,
	ES3IO_DeleteDirectory_m5E64CF3F72C3A4F0E8354F77EDA84E614B031AA6,
	ES3IO_GetFiles_mCBE0FA87B89E4ED543F722C7A6D2172409C873CF,
	ES3IO_ReadAllBytes_m3013044D9A9D8AAB00FA2566B44402A83257CB83,
	ES3IO_WriteAllBytes_m47F7E3AA7778AAA049D6F180AE26A800A523663A,
	ES3IO_CommitBackup_m1B9D0812093513607AA08762F27D74D968CE1408,
	ES3IO__cctor_m6ED0F50AA3C6806652ACA8D74FFC00DF02418B25,
	ES3Prefab_Awake_mCF33EFFF279D947A0DAE9C17672FBE13329FD7AF,
	ES3Prefab_Get_mB219B4C2214EFCC2852AB6E62B4AD635F2A2EC0F,
	ES3Prefab_Add_mB3816D5DDECE6B902A7B34B56526D4D813B9A3F8,
	ES3Prefab_GetReferences_m976D68FAD98F7C4AC3A165E643DBA963F94DB04F,
	ES3Prefab_ApplyReferences_m8FFFD996F5A0E03CCBC379636F04D9FEADEC008E,
	ES3Prefab_GetNewRefID_m1DF307C928C48C9A30EBB94C91FC77B6A29C67C5,
	ES3Prefab__ctor_m4575F4EA31D2CF4D5699E686D603AFEDD9D623FE,
	ES3ReferenceMgrBase_get_Current_m43C6D8A5A18699219B471A291607A0E4BB4A38D8,
	ES3ReferenceMgrBase_get_IsInitialised_m434C632F8827D6F31953E41770C3D143239C018A,
	ES3ReferenceMgrBase_get_refId_m793808277E67D2DB0C05B8A392A4DA33F099FC1D,
	ES3ReferenceMgrBase_set_refId_mE4EC83174096DADBAA00518A51D222338D0EB2A5,
	ES3ReferenceMgrBase_get_GlobalReferences_mC6C222787A8E5775CF75DD23E0E2AA10D89B69D5,
	ES3ReferenceMgrBase_Awake_mD42487217B99905D36DE308D812155FB15D1365F,
	ES3ReferenceMgrBase_Merge_m5E8755D54A7533433459891E4BC861BEE9297E2A,
	ES3ReferenceMgrBase_Get_mA655CB517BB9811528FFD1AB206888FB3E8B8088,
	ES3ReferenceMgrBase_Get_m6810353CD7411FC7D36E4AB66CD268F2DDB29403,
	ES3ReferenceMgrBase_Get_m1A862965D6A63E8A54D837B84D4A947000634F76,
	ES3ReferenceMgrBase_GetPrefab_m4F2F30E338F375E942B55D80E7DEF3810FCDF633,
	ES3ReferenceMgrBase_GetPrefab_mA14FB7D11BAB040FBD21C62B66FDF022AF191E52,
	ES3ReferenceMgrBase_Add_m450969F3F9A70AB1F5D46AFEAF38C3412B32322E,
	ES3ReferenceMgrBase_Add_mEB259F54F9A3909455EF088F43887A0874024AB8,
	ES3ReferenceMgrBase_AddPrefab_m3A25F33ED0295B15C4C45ECCE7E774DE6E3DE9D9,
	ES3ReferenceMgrBase_Remove_mF60FFAD74BB7FC4634AC0FF1FBCAAB41E044A439,
	ES3ReferenceMgrBase_Remove_m672A8D0E3ED156358B6F8FBB803E8569ED8A1FF1,
	ES3ReferenceMgrBase_RemoveNullValues_mD43BA38F919FB0C65AB43BD85BF640914FC5A505,
	ES3ReferenceMgrBase_Clear_mA4379A68B0414B23B0262F77504736ED643138E0,
	ES3ReferenceMgrBase_Contains_mF0B981E5278FEA20EBC957319F11CDE34B199398,
	ES3ReferenceMgrBase_Contains_m3F4272814DFB74EEB4C41CAE41A2F90CCC34161E,
	ES3ReferenceMgrBase_ChangeId_m3FD0FA230B58D2B06BEE596AC65519F9CA1AD460,
	ES3ReferenceMgrBase_GetNewRefID_mC9521DAC9296396E3057D7251446A77BCBC3E4B2,
	ES3ReferenceMgrBase_CanBeSaved_m5A848AE9D162BB0D75934E25E73C91B78F63E8C6,
	ES3ReferenceMgrBase__ctor_m0D02EBDFBA8614B971735F706FCF23E6B0984F22,
	ES3ReferenceMgrBase__cctor_m83C43956082B3F62377D329D791E3F2E007E2DE4,
	ES3IdRefDictionary_KeysAreEqual_m196F94FEF321BB11B8622977E07003B9BCEB9EA1,
	ES3IdRefDictionary_ValuesAreEqual_mBB921B64217E471DA46E5AB91EE2F0C5ACF7B910,
	ES3IdRefDictionary__ctor_mB5761C7BED0AA6E6000FFE256A31A8ED0F45C7B1,
	ES3RefIdDictionary_KeysAreEqual_m337DB8921BD226CB54A4BD49EF209978EB1FC243,
	ES3RefIdDictionary_ValuesAreEqual_m87F54EB05F71ACDB59A3A2744EAA2AE6E0AE5C34,
	ES3RefIdDictionary__ctor_m7314193B2379F50798FA2799985946D549011194,
	ES3Reflection_get_Assemblies_m95641BA9F385C479DE736B7CD5E12F208C1E5A77,
	ES3Reflection_GetElementTypes_mAE3793A4EADE9BF0937C291548D24452CE135B2A,
	ES3Reflection_GetSerializableFields_m8CB43337BC9CE52ED84D9D43AC32EBE897204410,
	ES3Reflection_GetSerializableProperties_m5BE897CDF1906A63C393BDE7C6EC2A05264F6A1B,
	ES3Reflection_TypeIsSerializable_m0DC2675A282B19DBC45BCB25153FB0608E758921,
	ES3Reflection_CreateInstance_m0F629B05F6270A65C1B75F61467E067E4A446D0D,
	ES3Reflection_CreateInstance_m321DDE80D918CA837ACCE30EF2966CED7DA417E6,
	ES3Reflection_ArrayCreateInstance_m2C85F9809EC815E4CAAA28B1C88FF34EC3AE3DE2,
	ES3Reflection_ArrayCreateInstance_m7D394D08688933F1D86AAF9A1A66AA9D6179A2E9,
	ES3Reflection_MakeGenericType_mDA23AC8E19770FF2BC513C93172757B7C1F9E7DD,
	ES3Reflection_GetSerializableMembers_m6A5EB263DA4B623AC452070364BC7C295DB0D88F,
	ES3Reflection_GetES3ReflectedProperty_m9A290BF5A32EFD8F23A9915B42A3A92A9896358D,
	ES3Reflection_GetES3ReflectedMember_mDC552FB4A61F95E802BB74AC75B12668B43C8171,
	NULL,
	ES3Reflection_GetDerivedTypes_m82C9908F6A233680255180A18A9A59C871945FEA,
	ES3Reflection_IsAssignableFrom_m66D083548EB379C7917F67557F92AE1D623B047E,
	ES3Reflection_GetGenericTypeDefinition_mF65BEA8BAA08FDC02C8F791533296DC9B532FBB8,
	ES3Reflection_GetGenericArguments_m571B8F57FC8E3550F53E25FB4C94FC5F51CEFAA5,
	ES3Reflection_GetArrayRank_mFCD4995C675D8CADC2ABB1378E7B678CC34FF023,
	ES3Reflection_GetAssemblyQualifiedName_m14B7FF29572E5B64385BE2C8D6A0B8733DDEE299,
	ES3Reflection_GetMethod_mFFDC5C5E30E4CE9D4B8C36C0B2823520744D2DF2,
	ES3Reflection_TypeIsArray_m0D95AF29EB18D34FD4E9EC335287484B378C0C9E,
	ES3Reflection_GetElementType_m05C766434786917BBBCF0B2F3C442F0E48972E9C,
	ES3Reflection_IsAbstract_m909CF9A4CE05EEBE9DFE97951DFC95C5F7A842A7,
	ES3Reflection_IsInterface_m5832C600B7D9E36965E4746D197C145D4B06551E,
	ES3Reflection_IsGenericType_m30C061375DC2C81AEFA9C9382490AE6730E2D6A3,
	ES3Reflection_IsValueType_m417BB61457D7F32C55043397AB547D6F8E4B54E1,
	ES3Reflection_IsEnum_m6D3F1BCBDC12EAA142CDBA3C737B12DB74F2A393,
	ES3Reflection_HasParameterlessConstructor_mEA957F24BC52B30EFF3AF8B8663F5D8AE0975842,
	ES3Reflection_GetParameterlessConstructor_mA5EE2D389D290A028E694E1C9FAE6CE7E7CFB658,
	ES3Reflection_GetShortAssemblyQualifiedName_m0769DFFE3ACA80B1FD2B817893C8ACEE78D13033,
	ES3Reflection_GetProperty_m49A06B87835EC1E013050F9DA43839017F086CD5,
	ES3Reflection_GetField_m8B7ED9BD79822B7D9A964A75C30E503D372FEB19,
	ES3Reflection_IsPrimitive_m01B03272A5FCEB3F6D85DC50D6FB16890CFD74DD,
	ES3Reflection_AttributeIsDefined_mC8040D763AA804D5883E48F1CAFF6AB6C0D79F99,
	ES3Reflection_AttributeIsDefined_mB7BE1445B3B98997456B30B46F4FAC5FB5BFE426,
	ES3Reflection_ImplementsInterface_m118CCD1E37AEC0D9856BDD3225CE5F5EFAF4CF00,
	ES3Reflection_BaseType_m2992AA1E7B25B9BC795F83FBBCC46A75CDE81C71,
	ES3Reflection__cctor_mB45B691E9BFF37DA2D44CA343D80FA85D0F6D854,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ES3BinaryReader__ctor_mA5254CF3C090456BAF5EEC582BB0BE1B76FDF4C0,
	ES3BinaryReader_ReadPropertyName_m7D3732CC7F91A3BE113CE2E272DBC85D1684C950,
	ES3BinaryReader_ReadKeyPrefix_m4AA8CE72F164A597D10F5F13948E9F6910DFD9F8,
	ES3BinaryReader_ReadKeySuffix_mBE33D588D0EB68E4AC8E92EA952C51CDE473F056,
	ES3BinaryReader_StartReadObject_mBBAA5C6D9A35D95F89463F66677B1D4258B1BC12,
	ES3BinaryReader_EndReadObject_m8DBB3AFA047AD749F8ACD94C270D437168D760E0,
	ES3BinaryReader_StartReadDictionary_m1FF9F5D0923FB699FC4591BFA47ED9950C167DA3,
	ES3BinaryReader_EndReadDictionary_mADC02A398CB37F0826FD6D5F2D3DF673F66EBD47,
	ES3BinaryReader_StartReadDictionaryKey_m0A5C9B43DD6D1B143026ACF92E885958A681B5A0,
	ES3BinaryReader_EndReadDictionaryKey_mEA3F22EFEF8E44F8268CDB45F00C0FA00E7DDA03,
	ES3BinaryReader_StartReadDictionaryValue_mB95905486B24B45F52ADB81E681FCBC29F584972,
	ES3BinaryReader_EndReadDictionaryValue_m4E5F2333D466629A53EC9F5133BDDD781522CFC9,
	ES3BinaryReader_StartReadCollection_m0DECECC7F8806A416835BD0DCF5609F7703C21A6,
	ES3BinaryReader_EndReadCollection_m5EF6D97A805952B02126D950948A7187718BF220,
	ES3BinaryReader_StartReadCollectionItem_m0FC683EC642F7C55C1601E97C632F6EDE4F7C844,
	ES3BinaryReader_EndReadCollectionItem_m9BD3DFB1568075EBCBCA4695837002A947CC5EB2,
	ES3BinaryReader_ReadElement_m64E69139784B7097355ACFBE195481D1E355B4A7,
	ES3BinaryReader_ReadElement_mEC416B96785BE6CB352AB96E9FF78E6202C4E093,
	ES3BinaryReader_Read_ref_mC6FD95A88EA7F7DE8846A7A3AA31618164D874A5,
	ES3BinaryReader_Read_string_m6EEB579F9045C9B4D10CE9D0F79F321F3FA71A47,
	ES3BinaryReader_Read_char_mB949F97EBFD4FF9F503EC84AF52F5C5C97423D91,
	ES3BinaryReader_Read_float_m0DE630FC8C7C982EA59FD11FD085F655B5CC1E26,
	ES3BinaryReader_Read_int_m2D22C0F8AD8E9AB744CEAAB13C0B0B7D82C52656,
	ES3BinaryReader_Read_bool_mA22C1C5192CF276C18E14B6EE4F014928F0E8D34,
	ES3BinaryReader_Read_decimal_mCE54426C00EC6AF0FB279CD52C6411022413859D,
	ES3BinaryReader_Read_double_mC896C9E968A5564D079A10BEB084513275A4DAF1,
	ES3BinaryReader_Read_long_mC3D4774490F1CBD9E855F2DE6C3836EADF401AD3,
	ES3BinaryReader_Read_ulong_m657F5D91A6B7891B85342AC8F3A371B10111F05F,
	ES3BinaryReader_Read_uint_mE1D863393F5D5A5241A37D8EADE399C9A7A12018,
	ES3BinaryReader_Read_byte_m738496AAF2624E01849283F3D862F07DE23CDBC1,
	ES3BinaryReader_Read_sbyte_mFB7D36C9AFAD7B4B12D7BCDD82B52A2329133082,
	ES3BinaryReader_Read_short_mD7E1EE8350724BBFD983C1173308EF8160CC9DCE,
	ES3BinaryReader_Read_ushort_mB7F8EBF976099D293EFBF9C0607DAA5EC45B8639,
	ES3BinaryReader_Read_byteArray_mADAC229FD4ABD3B7760186CB029909F34ECA6A31,
	NULL,
	NULL,
	ES3BinaryReader_Read7BitEncodedInt_m9060182E013F05FD7FCB464B418F5EE299757B47,
	ES3BinaryReader_Dispose_m9A3CA9FEE7FA74DB91C37419BD9E9FA5B7AE6B53,
	ES3JSONReader__ctor_m17BBE79306A7F76FFD453D10534A9A71902E3948,
	ES3JSONReader_ReadPropertyName_mE35A2A2A36E2F352B13241F9C6E5D4A2C75C90BA,
	ES3JSONReader_ReadKeyPrefix_m91AC1CB9CE4C4FAB57CD30EA7EE75CE3FC49FBAA,
	ES3JSONReader_ReadKeySuffix_m32DFE6E473510C3B9AF5672976F69E009A76A3B9,
	ES3JSONReader_StartReadObject_mE49545C2A93FD39830820E8E50651A85257550C5,
	ES3JSONReader_EndReadObject_m81F9629949691C631566F353205469A258649D78,
	ES3JSONReader_StartReadDictionary_m3046125695D5A8B2297D8E4393400D54807C3A10,
	ES3JSONReader_EndReadDictionary_mC688E98CA35C68173C640E496105CBDEBEC50B33,
	ES3JSONReader_StartReadDictionaryKey_mB6574B702DC1C61A225C5B7064E8D769AD9A7158,
	ES3JSONReader_EndReadDictionaryKey_mDC30B3DCD746BCEB2A7128CA30B1949A72240DF0,
	ES3JSONReader_StartReadDictionaryValue_m49335F35F9C5B4970003772EEE339DAB4B513C7A,
	ES3JSONReader_EndReadDictionaryValue_mBF27C1B659610A2B8057652A813246AF88AB0E41,
	ES3JSONReader_StartReadCollection_mE3CE6154B07D519215A21F5AC5691BB8B66FD5A9,
	ES3JSONReader_EndReadCollection_m16F1C75BCD481CB80A30F6338B9F087B044A8ED2,
	ES3JSONReader_StartReadCollectionItem_m859937748AD554D9B85B8D6B6B7917A691A5D90F,
	ES3JSONReader_EndReadCollectionItem_mCC631036ABA5519D7436B404E2F56E4E8DC47A55,
	ES3JSONReader_ReadString_mEAC4B995FE66B0A4AD27CF11E917369563C01B73,
	ES3JSONReader_ReadElement_mFEA0BF6473883663D07FCBCA5BD37415978FB70B,
	ES3JSONReader_ReadOrSkipChar_mFC2E6EE6592ED19B95F83E62887AC914CE9BD82A,
	ES3JSONReader_ReadCharIgnoreWhitespace_m613187290BC4F2DFEE20DDBACC078D4B9F522411,
	ES3JSONReader_ReadNullOrCharIgnoreWhitespace_m34DA622081FFF53C157674198C8F51FCC3186DEB,
	ES3JSONReader_ReadCharIgnoreWhitespace_mB4E5F2BA0542D1AFF8C8DADD1D7601C530132180,
	ES3JSONReader_ReadQuotationMarkOrNullIgnoreWhitespace_m3A101EEADF8978B96DE0910DBD4AC6D13C60B09D,
	ES3JSONReader_PeekCharIgnoreWhitespace_m486C52A1E1B66C018FD1FAEAAFA8EE585D5485C3,
	ES3JSONReader_PeekCharIgnoreWhitespace_m7D659DBA4244BBD5DAB7CB5C3264351CE2A3E4F5,
	ES3JSONReader_SkipWhiteSpace_m1D8147243750528B9A7F3696FC11D160031E3996,
	ES3JSONReader_SkipOpeningBraceOfFile_m3B2F3DF54830973042E482A1A3FA90FED5567609,
	ES3JSONReader_IsWhiteSpace_mF068999016A5B3B9EB1E3BB40F823440F411D656,
	ES3JSONReader_IsOpeningBrace_mC0A0982F4570833EDCE1B3BFEFA5E4DBCEE83587,
	ES3JSONReader_IsEndOfValue_m028234CF74097B1B0BCCB9685CB2C93697F4418B,
	ES3JSONReader_IsTerminator_m2A8BBA5D74AE0C0BDFA4509512E89CFDFBC85234,
	ES3JSONReader_IsQuotationMark_mE69B98ABAC02EC697C9FF9B3C4D0665A2A012C54,
	ES3JSONReader_IsEndOfStream_mEDD321EAA75C90A41AC43513F83E5EF0D8371AA5,
	ES3JSONReader_GetValueString_mED0C1E5FF644942D52408405A8892A6D0425E359,
	ES3JSONReader_Read_string_mA6ECF193D04F8A4199F987B278B03114014224BA,
	ES3JSONReader_Read_ref_mBC08ABAB9B5782046796C1C642102FA174C849F2,
	ES3JSONReader_Read_char_m670DC7FF32269B77AAFEA4945A899B450DBA594D,
	ES3JSONReader_Read_float_m0350D39C3DE6E73354A5DC045AFE5BD81D4AD32A,
	ES3JSONReader_Read_int_mC82580D0075D4414E09F5FBCD9132B5766382915,
	ES3JSONReader_Read_bool_mB38E66913DB1E0176DD81AAD6EE86F2BA0D4352B,
	ES3JSONReader_Read_decimal_mB0384B301953D92E23452A4DE947509CAF8B36EC,
	ES3JSONReader_Read_double_m83DCF0C216FE209C616DEFBE454783172B994633,
	ES3JSONReader_Read_long_mDA1AB3B15DC0456B8C7F0BD661944CFE483C302C,
	ES3JSONReader_Read_ulong_m0A0D598839DCE3007F74C69AA25EF3B0FC196E16,
	ES3JSONReader_Read_uint_m237DCA3C23AB651B073BD6B3224F25C6EA594E9F,
	ES3JSONReader_Read_byte_m1DE5AA8E18D2CCAB62D3FABF7146567493A919EF,
	ES3JSONReader_Read_sbyte_mCE70FFF4BB6093344482640BA5E9986383F9555C,
	ES3JSONReader_Read_short_m871E07D5E5C3A8DF19C7736D3E4C69CEC94D2220,
	ES3JSONReader_Read_ushort_m1C4961563B5EFBEFF8E15A08075E6C9CAEC89E7A,
	ES3JSONReader_Read_byteArray_mE5E1CDECD0502718666D6D74FE43C7FD7223FD4A,
	ES3JSONReader_Dispose_mD7E105CB37536BBE761EC7439D790C31E7DD98CF,
	ES3GlobalReferences_get_Instance_m766FE68364B27CB850DFD80C71A530F4AE6EDF8F,
	ES3GlobalReferences_Get_mE658C144069C6AEF3598ED042AABB08071FA1CC5,
	ES3GlobalReferences_GetOrAdd_m58437E89A839FAA4DD7B9F8B249AA2C54DE45779,
	ES3GlobalReferences_RemoveInvalidKeys_mB20453A7D1EB908FE55CC99500B565D8FC9B1630,
	ES3GlobalReferences__ctor_m0390F1C12488C42FE9CB2214D9698AE4A4B8F15E,
	ES3DefaultSettings__ctor_mBA46628BE904D31DCF4EB4D0446B33A5597849CA,
	ES3FileStream__ctor_mEACE9EA7E564180B300AC5D87EFB986A199E47D3,
	ES3FileStream_GetPath_mD4366B8DB076240B689951B8D126F4FE6D203984,
	ES3FileStream_GetFileMode_mE8548879A5E45E24A91F31B63B6144888A7AD600,
	ES3FileStream_GetFileAccess_m52D95CEE89B7F3BA7D44C7BA7DD4566A590027FC,
	ES3FileStream_Dispose_m3FFBB0FB4E174D4483B1BA57E241708C43EE38B6,
	ES3PlayerPrefsStream__ctor_m6EFEE86C38048737A87198A2F7F98FEF265FF3AC,
	ES3PlayerPrefsStream__ctor_mA22DA68788346E7C730FCFC5CD854329508AD0CA,
	ES3PlayerPrefsStream_GetData_m12E47B97AA884204E117EFDEE02C6454A1B99FD3,
	ES3PlayerPrefsStream_Dispose_m70F322AE1B857EFB7565AD68EB2EB24F94BE4C98,
	ES3ResourcesStream_get_Exists_m3FBA3F32F22290B0BAC766889D766E211B99A43C,
	ES3ResourcesStream__ctor_mD26ECC2BB989E4347638D30BDFA108BA0326F627,
	ES3ResourcesStream_GetData_m30D607DF8947B04CE01095A9317BBE0437B19A6C,
	ES3ResourcesStream_Dispose_m86982FCA7263346F686EFA558B29AB6EF698DCA1,
	ES3Stream_CreateStream_mFDC6B85322CEBE34237BB1462A7652ABF7E2269E,
	ES3Stream_CreateStream_m0B1C3520964988A81086D1A3B145D17AB31AC04B,
	ES3Stream_CopyTo_m941AE086EE5848C4C5A7444085221F1424A4E52B,
	ES3Member__ctor_mA3BBD7A646ACC43FEBF7734118CB1EDA967951F7,
	ES3Member__ctor_mE663398D3D9F366EA5FBFD57334DD8DBCD0BBC1A,
	ES3TypeMgr_GetOrCreateES3Type_mB12E0FD8E92BFCEFD9D6814B428A1ABCF22F7983,
	ES3TypeMgr_GetES3Type_m9777E8D44A268633ED1FAB91C70C985568D77E35,
	ES3TypeMgr_Add_m99EE8C7C46508C087CEB5C32C255F8B36E8DD23F,
	ES3TypeMgr_CreateES3Type_m6F10A6BC4B879AF12C24DE29A406C7A21E644477,
	ES3TypeMgr_Init_mA59BACCA4C7636A2DB8779EAF61FD7F57F20CBB6,
	ES3TypeMgr__cctor_m29195A7EF386968BB04AE098C2549C9B23B423CB,
	ES3WebClass_get_uploadProgress_m829D20BFDC7EE47B8124615FDCD442BEFE248DCB,
	ES3WebClass_get_downloadProgress_mFE19F25A4A34CFA036E8A68C6331A086611FAB3C,
	ES3WebClass_get_isError_m94BAE9F2CC51A9AC95AA171A3B639FD4CF9833A1,
	ES3WebClass__ctor_m89BF498B9078CBA7732974C1E242B88FB1AB6B81,
	ES3WebClass_AddPOSTField_m4F00FA4F204A6427B0AC21DEBDD7649F369E7B70,
	ES3WebClass_GetUser_m173A42EE49D85BF99EFC839529588DC460BD9801,
	ES3WebClass_CreateWWWForm_m827392153AF52E4CAE56D10AEF9D6990F53D22D8,
	ES3WebClass_HandleError_m4E83B48A57CED39B45A208CB3FBBEA9F60112868,
	ES3WebClass_SendWebRequest_m154095E6ECB3E73C0C98D467F8D57D946F853E5D,
	ES3WebClass_Reset_mA9CF44B06F57729C51151B33C550B642E24DDC2A,
	ES3Binary_TypeToByte_m15C22426AB52571E1322EA4009904C5D95634AD1,
	ES3Binary_ByteToType_mED465B8A24191A11568B809B9C658B5C0DD0019A,
	ES3Binary_ByteToType_mBAF5026EF62837CFC5BC5E089C6E4CA52A15D5FE,
	ES3Binary_IsPrimitive_mCC6E86CCB6D25FED0FFACA48870DE18CC9816D82,
	ES3Binary__cctor_m32D22BC51927075991C544FEAD48D5DEA8671AAA,
	ES3BinaryWriter__ctor_mCD32D7C605DC6410A307A0E05D2314161EE9BD20,
	ES3BinaryWriter__ctor_mDFB75C3395A4FC19D452FFC8899028AE8698CC07,
	ES3BinaryWriter_Write_m23800B7118246269FEB1433C095BF0803B19FFA8,
	ES3BinaryWriter_Write_m4B21A1D7822BCA3AAE719807B711847BDC975C36,
	ES3BinaryWriter_WriteProperty_m71E6364DFB7F86D3DF36C77B35A2F2F6F69684E6,
	ES3BinaryWriter_WriteProperty_m04FAB9385FF9E76FED6FE914CC95FBDD6363AC7E,
	ES3BinaryWriter_WritePropertyByRef_mE48FEBCECE097B7E6DD5E264DE2FB12D89CBD8BC,
	ES3BinaryWriter_WritePrimitive_m32638A0EAEE4332CF14308616625D328D8E9EEF8,
	ES3BinaryWriter_WritePrimitive_m91399BF01D0E9B2C0DA5F75BE53D7D4DADCC870A,
	ES3BinaryWriter_WritePrimitive_m5A649AB0E886C74CB13A1C88DDBA0B38D5E52A88,
	ES3BinaryWriter_WritePrimitive_m2934092A51C31D82D5A596F45900A1D6A46B4D4A,
	ES3BinaryWriter_WritePrimitive_mA494B6C45F97AA5FF3639AD018586432ABE7DB2B,
	ES3BinaryWriter_WritePrimitive_m744285FC58F1402AA216EB15E7C6734C081B30B6,
	ES3BinaryWriter_WritePrimitive_m44A986B67AA170C0CAE06760D448B7F0DFD52BEF,
	ES3BinaryWriter_WritePrimitive_mE95088D5CA8F162F6B6DF45E3C2CD6C7B78B98F5,
	ES3BinaryWriter_WritePrimitive_m82398FF7C812FDA39545E8F47B835F8C81BF6F5F,
	ES3BinaryWriter_WritePrimitive_mA180BAC92CB41AF61850EF69B75FF9791238C119,
	ES3BinaryWriter_WritePrimitive_m6548A9B2E4CE55E4D9C939F11EC19BF7E7146935,
	ES3BinaryWriter_WritePrimitive_m9982ADCC03014EAB18D010D5F42DE21EFDEBE340,
	ES3BinaryWriter_WritePrimitive_m370363C5F0FD1035567B6ACE47C731D7D047544B,
	ES3BinaryWriter_WritePrimitive_m6202C1537EE619D997F194A45E278C4EDE858BAF,
	ES3BinaryWriter_WritePrimitive_m5FE89041CD48EE26571414E1D532C1388DAF8657,
	ES3BinaryWriter_Write7BitEncodedInt_m67C13143C874F2E498933131DA6FECDEC7A9CC88,
	ES3BinaryWriter_WriteNull_m2B682B1F0A321FB92E237BEF783CCC405FDCB729,
	ES3BinaryWriter_WriteRawProperty_m513EB75D1C8C9717C3FEA4D7BB763B116E909271,
	ES3BinaryWriter_StartWriteFile_m407CA8E9C9297091A471EEE6FD6BAAEDCE57BD7E,
	ES3BinaryWriter_EndWriteFile_m87EF1E8E4609CE3094552BF16D07A9106E9E6008,
	ES3BinaryWriter_StartWriteProperty_mE63104F2B0B69363085483393A54D3B93C68E616,
	ES3BinaryWriter_EndWriteProperty_m64AF2449E3B2C038A4914E2C41D19CE56C495442,
	ES3BinaryWriter_StartWriteObject_m76C0130D84EADF77255AA11E89C517E8362ADDDC,
	ES3BinaryWriter_EndWriteObject_mB5E7E0201B5405F34829296D892BF6F3BEBF17B8,
	ES3BinaryWriter_StartWriteCollection_m859866296D67BB4F3E2BB38A8F19DCB78776C44C,
	ES3BinaryWriter_EndWriteCollection_mE7F1F47176AD668099E8A590E33713D1C7D04502,
	ES3BinaryWriter_StartWriteCollectionItem_m47137F7DE32B2B46DC483569966AFDAC645246CA,
	ES3BinaryWriter_EndWriteCollectionItem_m87CE09614BD2C80D65AB08E94761268C469D94D7,
	ES3BinaryWriter_StartWriteDictionary_m99BD8879438466BC8A86C0C9307D2AE1DEFE34AE,
	ES3BinaryWriter_EndWriteDictionary_m9EA46EC3CC57E2053F343AE19C302DECD9FBF778,
	ES3BinaryWriter_StartWriteDictionaryKey_m565689C1B56196E3B7BA95D6EC07180C8E31CD76,
	ES3BinaryWriter_EndWriteDictionaryKey_m2D38FEBE3B11B4E83353A4ADC142BC0950150BCA,
	ES3BinaryWriter_StartWriteDictionaryValue_m9F86A7405950E505F3C16B67CDFCC59D528C4436,
	ES3BinaryWriter_EndWriteDictionaryValue_m1F6AFBDD30BCAFBB6E629E60A090701CD2C4CA59,
	ES3BinaryWriter_Dispose_m7874F6A77B8D6AA26B145B60CAD79A2F892A9467,
	ES3CacheWriter__ctor_mAF0394327812069DF8953208C8303548D34823F1,
	NULL,
	ES3CacheWriter_Write_m3A637364239D8C34D32B0F9F4710949BBBE163FE,
	ES3CacheWriter_Write_mDE5FB776751EE78508222F1934B758A4158D5FE1,
	ES3CacheWriter_WritePrimitive_m9FD3E4CC7B137D360B53FE1F0412EB88B0A1FA47,
	ES3CacheWriter_WritePrimitive_mF98BBEC778EB29E0F30064816205F1DFDB7E4132,
	ES3CacheWriter_WritePrimitive_mC10FCC7DBF1FCD88C30196F46AC669712CC96324,
	ES3CacheWriter_WritePrimitive_m2216BF4089A528BDA565331CE7695B95BDED9E73,
	ES3CacheWriter_WritePrimitive_m7565F04B31002AB5501EB4CCC4E02194B787B0B4,
	ES3CacheWriter_WritePrimitive_m2709FB7E8387D6621493C8BF9E7DB520998DDAE9,
	ES3CacheWriter_WritePrimitive_mE258E5EFE472D4A94A9037FFDC6121E8495401B7,
	ES3CacheWriter_WritePrimitive_mBEE7E75447B691BBA3C94A726801C282545B886E,
	ES3CacheWriter_WritePrimitive_mFDA10194DD6C69A380C4319BC7C6EFBDB28836A9,
	ES3CacheWriter_WritePrimitive_mBF464BA1645F726264AF5199D41B9B0BA3C2226D,
	ES3CacheWriter_WritePrimitive_mD7C4D126FCA06CADE7FF85A28663B8F4D57B4CE7,
	ES3CacheWriter_WritePrimitive_m4179168292BED2AEF688D0C19B8A27ED68FB08B6,
	ES3CacheWriter_WritePrimitive_mD8D85B826E90C4E3386E486F59481CE4FADDCAF6,
	ES3CacheWriter_WritePrimitive_m33805F4F58E92CCBC8D452F654BCD6A252FFD515,
	ES3CacheWriter_WritePrimitive_m7E8C628C2EF9340894CD8AD388AD4F7646067210,
	ES3CacheWriter_WriteNull_mEF2257501C552A13CBFD17A0EA7EC5482D12E345,
	ES3CacheWriter_CharacterRequiresEscaping_mC32069913D2E70BDBD1314C831EEE44330076FA9,
	ES3CacheWriter_WriteCommaIfRequired_mE834D5046882E87E5FA6AA90E3CF86EDBFEBC618,
	ES3CacheWriter_WriteRawProperty_m1C0EF635BBDF6AE5516CD549CF25C6410BE2175F,
	ES3CacheWriter_StartWriteFile_mF401D10B8B50FEF784EE6645C435BCC017E33F00,
	ES3CacheWriter_EndWriteFile_m8C9FCA9BFB1D89A208FD850B622C8BA24899B12C,
	ES3CacheWriter_StartWriteProperty_m62F85B781903DD8AE585BA93832EA21E1CA61DFD,
	ES3CacheWriter_EndWriteProperty_m5F3D987C16C4164C4F4B0C60A1152D01B6AC3920,
	ES3CacheWriter_StartWriteObject_mC0B025FE509B9CE38BE13F33ABE8C2EC44259C4B,
	ES3CacheWriter_EndWriteObject_mFF1AF66972DE025D13ECFAE4C1B3ED34537D4331,
	ES3CacheWriter_StartWriteCollection_mB84F5D82F29DB2F1B1324F460F07C31FCDFBBF8F,
	ES3CacheWriter_EndWriteCollection_m9622711518FCBE71595C4FE4FECF84CD9A9CB253,
	ES3CacheWriter_StartWriteCollectionItem_m24A15597B7B391CB98C8FFFB49988E625FD2EF22,
	ES3CacheWriter_EndWriteCollectionItem_m4C193478B00B42779A0BB21CC8A2D6EBA87CCB7B,
	ES3CacheWriter_StartWriteDictionary_m4CB81A1D15F09AEEF01A022D47C2C35BEED58CB9,
	ES3CacheWriter_EndWriteDictionary_mA50077F206D72AF0FD50F5282153A0AF2D4100F7,
	ES3CacheWriter_StartWriteDictionaryKey_mE50497F85F915C8FBAE1845F1343D9ECBFA9E3F5,
	ES3CacheWriter_EndWriteDictionaryKey_m30EA1080F7F54DE3FF767A62649BC05E36E08E37,
	ES3CacheWriter_StartWriteDictionaryValue_m84CA400E0395F9E71134C762C3BE94F3AFAEF695,
	ES3CacheWriter_EndWriteDictionaryValue_m2CBF9F3968CDD0A084F79D1A1EEBAE1374DD1169,
	ES3CacheWriter_Dispose_mC71481FC1BC698818532CEBCE5A92A0B0A1B3C6D,
	ES3JSONWriter__ctor_m45591E20452A0FAAA92DE008CA5AC909472B69EF,
	ES3JSONWriter__ctor_mF094A8A112A6456314425DB40081ABDA81ADE640,
	ES3JSONWriter_WritePrimitive_m61CDCE9FEB51B59C14BF7701AFED4483BC37897D,
	ES3JSONWriter_WritePrimitive_m78DCBB21C01934C6BDB9A66081CEB2853D2E217E,
	ES3JSONWriter_WritePrimitive_mE706727663FE7A4E64813E439B60B5D5FBC3C770,
	ES3JSONWriter_WritePrimitive_mACA27E18EF546576E037D4FF8EAE33CD5F8D3A0B,
	ES3JSONWriter_WritePrimitive_m26AD40B7F3E2CC978EA4DFD9FBCD0C8E9F7CB668,
	ES3JSONWriter_WritePrimitive_mB81DF912883CDF4AFE1FCF774DA0F09A7905D4D5,
	ES3JSONWriter_WritePrimitive_m3EFE07EAD33AECAF701FF8814B01A6406F2785F6,
	ES3JSONWriter_WritePrimitive_mF1DC896A95FE1CA7A5A1D25AB3669EEA36CE1AE1,
	ES3JSONWriter_WritePrimitive_m8141231866DAB515D36614EBF3B902BF1BBEB6B7,
	ES3JSONWriter_WritePrimitive_m44C9B844A6CA64D769799F3BCA99FFD1DB3F448A,
	ES3JSONWriter_WritePrimitive_mA80EDB378C7C6A3B05756BC02B5231B6652923A6,
	ES3JSONWriter_WritePrimitive_mF775B7D75A96B9BEB9C5BA8D23DEB11180CF822F,
	ES3JSONWriter_WritePrimitive_m86BC936955EBAFCEAB4E9CFD05FAE9086DC789DC,
	ES3JSONWriter_WritePrimitive_m3075CF381ABBC1ADE3A607D24EB0147425699D73,
	ES3JSONWriter_WritePrimitive_m72A3614C5905C12CBEC7059DD1E7C837312D76BC,
	ES3JSONWriter_WriteNull_m3458C81460E2FA2E6DC3BF0983E6A9AEDC86BCEA,
	ES3JSONWriter_CharacterRequiresEscaping_m89763B269EF6A293C9A880C0FAD1FA0BD52914D8,
	ES3JSONWriter_WriteCommaIfRequired_m0544390F30EC0B272B576DEF99A675AA9423CB9D,
	ES3JSONWriter_WriteRawProperty_mFB5673B8CC4DF8F6F4BF60B2FD91112E10EB4C01,
	ES3JSONWriter_StartWriteFile_m5D5BA46D3FB313BFDE8D1FBB17617FC11548F89D,
	ES3JSONWriter_EndWriteFile_mE8F43869E2C94DFB42771B8CC439E3EBF57AC8CD,
	ES3JSONWriter_StartWriteProperty_mD4E597E7BCA9B56050670108473CD7DFBFC6FBF6,
	ES3JSONWriter_EndWriteProperty_mEBCB3F8D4C68150A2BFEAFBE445B1BDB158C29E6,
	ES3JSONWriter_StartWriteObject_mD817BFA247573CCB3EEDFFC1C5C3111C15D11A7D,
	ES3JSONWriter_EndWriteObject_m4832C63F19827E166ACB3E6FA6541201B738397D,
	ES3JSONWriter_StartWriteCollection_mC2380EC023B8856B06A419219769A517E4CFBFDF,
	ES3JSONWriter_EndWriteCollection_m49C5FDD746173EA44D17DEBC91152A17F4A36F66,
	ES3JSONWriter_StartWriteCollectionItem_mEC8B0AB3632E9DF0EA9F9829B587F6E7F758C6E3,
	ES3JSONWriter_EndWriteCollectionItem_m16ED92B5E285A44467CAF861235EC08FC24CB4EE,
	ES3JSONWriter_StartWriteDictionary_m5F1AD1666D99627DCCE164029338C4D7DD289CB2,
	ES3JSONWriter_EndWriteDictionary_m9333430C80ECEC683604A8B80EDE44310C4994C8,
	ES3JSONWriter_StartWriteDictionaryKey_mE80668823B753404A9664867BC8F9D4563946958,
	ES3JSONWriter_EndWriteDictionaryKey_m154079C6F41DF8F1F04E0664001794C3DF6D9A9E,
	ES3JSONWriter_StartWriteDictionaryValue_mAEA527056D20E3C292F02E6FFADD16C8BFC401EC,
	ES3JSONWriter_EndWriteDictionaryValue_mD40DBBFB9D9819728682085CFD1B59A73B310C7E,
	ES3JSONWriter_Dispose_mB9EC5937CE8DD248205E615FCFAEE910E55C2462,
	ES3JSONWriter_WriteNewlineAndTabs_mBDBEA833734A66C3D2F33369714046121AC83178,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m9FD9D5C30B989EC8C2B53EC7131DE8D4320E5813,
	Index__ctor_m7E89106F6BE1B37FB8E74AA2451A9795CA38A04E_AdjustorThunk,
	ES3ReaderPropertyEnumerator__ctor_mF089B29C6D5707CB3EE1D2AA408D91B005558421,
	ES3ReaderPropertyEnumerator_GetEnumerator_m5BECF82CB1697BE11395A321D30EB015FA9D3095,
	ES3ReaderRawEnumerator__ctor_m97B888FCB98A953D9B0F3195CF983712693B94C6,
	ES3ReaderRawEnumerator_GetEnumerator_mA90016431197C25720481FAA8B7099B9AB163544,
	U3CSyncU3Ed__18__ctor_m033D6727BB00DF555FE81821570BEDA7DF44939B,
	U3CSyncU3Ed__18_System_IDisposable_Dispose_m7C90F62D311E884A689CC34C525722E094EAE440,
	U3CSyncU3Ed__18_MoveNext_mB96C6E234525B590D2483C66516F9E1465AEE1B2,
	U3CSyncU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1734D8E0C1FC387ED53AEF2036E7FB3519ABCCE8,
	U3CSyncU3Ed__18_System_Collections_IEnumerator_Reset_m4D596E71E411E3C586E2C933B587D45CD93A802A,
	U3CSyncU3Ed__18_System_Collections_IEnumerator_get_Current_m701280AB2A2696136B8E02246C37FD786E418E52,
	U3CUploadFileU3Ed__31__ctor_m0BDC3981FB003514C431862B9CFA3903A9EAA5DC,
	U3CUploadFileU3Ed__31_System_IDisposable_Dispose_m91654840B549D1E707F9BE6C4DF643C749941B75,
	U3CUploadFileU3Ed__31_MoveNext_mFD4B4F05BF6BB72FD33BBBC7FC21A3DFBA7DAB56,
	U3CUploadFileU3Ed__31_U3CU3Em__Finally1_mEE21CD0EA6E3281B43DBDAFA088D476AD6F37100,
	U3CUploadFileU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8891471CD6B44634D22883E4CF14FEDD2E0CA50D,
	U3CUploadFileU3Ed__31_System_Collections_IEnumerator_Reset_mE0E509354C35A0F0EA095D0080EF76E984E61EA6,
	U3CUploadFileU3Ed__31_System_Collections_IEnumerator_get_Current_m0EDBA1EF5B08EA88F64F4E6487B0AC5446A9ECC8,
	U3CDownloadFileU3Ed__42__ctor_m8F1A0B87ECAD159883BC8AA73A763456006DB800,
	U3CDownloadFileU3Ed__42_System_IDisposable_Dispose_mFB91B69ACF8248533A696FBCAA22EA710900595D,
	U3CDownloadFileU3Ed__42_MoveNext_mE93BA1311E386EB3C6ED559A892DFD6BEFB1B760,
	U3CDownloadFileU3Ed__42_U3CU3Em__Finally1_m61140B2C3AE08CD1CE000449F62937B70015C4AE,
	U3CDownloadFileU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF29DBB02369A72FD5973DD8D59D5D7140D1F80AE,
	U3CDownloadFileU3Ed__42_System_Collections_IEnumerator_Reset_m86A1B8EFE7F5D2C13251D8FD5DE62DEC1BE6578E,
	U3CDownloadFileU3Ed__42_System_Collections_IEnumerator_get_Current_mDDD750C12464F1BB617FAB8B039C4B0E2F2BC2F3,
	U3CDownloadFileU3Ed__43__ctor_mA783E6486D61A17B9B300C0136F32EFFBD642B80,
	U3CDownloadFileU3Ed__43_System_IDisposable_Dispose_m1C1DF73874C2563AF61972DBDF6746520DC2882E,
	U3CDownloadFileU3Ed__43_MoveNext_mC394FCFEE9549E286170D007E8EC064C0969C4A0,
	U3CDownloadFileU3Ed__43_U3CU3Em__Finally1_mB8C4212B0995DB7773F735FCD1917A7C32542099,
	U3CDownloadFileU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00E135313B70E3B073141F7C639F832D71DD1A55,
	U3CDownloadFileU3Ed__43_System_Collections_IEnumerator_Reset_m524A6492CE168E85CC0AEB5551DABD8CF712C027,
	U3CDownloadFileU3Ed__43_System_Collections_IEnumerator_get_Current_m33F93786F723D0AEBAFF37AE651918A0819F21B5,
	U3CDeleteFileU3Ed__51__ctor_mDD7A4AAC957798706FBA4FB792BD6411B6C6581E,
	U3CDeleteFileU3Ed__51_System_IDisposable_Dispose_m29CF210E1B76494F96E641BDBD282FEC0D7FA875,
	U3CDeleteFileU3Ed__51_MoveNext_m86992BA94E91618C156FF01F1A9D890EA078D739,
	U3CDeleteFileU3Ed__51_U3CU3Em__Finally1_mCB3152F7603B2AF8C8F474ED6AF2533A1855E4F1,
	U3CDeleteFileU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m023054DDDE86D46F1A20B37C57942913390352E2,
	U3CDeleteFileU3Ed__51_System_Collections_IEnumerator_Reset_mFEDE20E0A4D2927FE2E3A366DEAB3E1C795C1313,
	U3CDeleteFileU3Ed__51_System_Collections_IEnumerator_get_Current_m3A284BC43C616BC210C3BE957142D1D06261F93D,
	U3CRenameFileU3Ed__58__ctor_mFE6B9003E529F060E59D50B71308D842C929C98E,
	U3CRenameFileU3Ed__58_System_IDisposable_Dispose_mE8B903E35131A23DA279BD0A80629412A5A8EB87,
	U3CRenameFileU3Ed__58_MoveNext_m9F0A6DE9DF65B6951C04195C4062B395D31FF9A3,
	U3CRenameFileU3Ed__58_U3CU3Em__Finally1_m36CB320A284D555E2A03DE708966AABA6228D547,
	U3CRenameFileU3Ed__58_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BFB03E2854D939939EC05E5581CE501BEBF6FA6,
	U3CRenameFileU3Ed__58_System_Collections_IEnumerator_Reset_m7A416560E4F4E558E94B8E4B01C2BEE03B75CD55,
	U3CRenameFileU3Ed__58_System_Collections_IEnumerator_get_Current_m5F940600D20B101D0B9B04E46A61AC6192C4091F,
	U3CDownloadFilenamesU3Ed__59__ctor_m3F2DAC7F103EED57C560376A033A2F7F7B1E125D,
	U3CDownloadFilenamesU3Ed__59_System_IDisposable_Dispose_mEA8654680F35F4B1D11BFC0B1274EF54D69B86A5,
	U3CDownloadFilenamesU3Ed__59_MoveNext_m8CD8035730FD31F56203439DC3B486B3B37CB600,
	U3CDownloadFilenamesU3Ed__59_U3CU3Em__Finally1_mF5D579A5E5F75A79F276E3B69156ABCE3D2BC2EF,
	U3CDownloadFilenamesU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0031A49181BEE6D5E5B05CCAF2F0A0D879EBF8D8,
	U3CDownloadFilenamesU3Ed__59_System_Collections_IEnumerator_Reset_mA40B1BE32DFC81340E03B02224820EA0F712F212,
	U3CDownloadFilenamesU3Ed__59_System_Collections_IEnumerator_get_Current_m634EE639A9A5D5BA5E0E7631DAA494E1B71250B8,
	U3CSearchFilenamesU3Ed__60__ctor_m3A62B58C7239A715F52E9B53BB7435EEB860CD68,
	U3CSearchFilenamesU3Ed__60_System_IDisposable_Dispose_m8DFE76AFA0858CCC058B072775738D02B612B338,
	U3CSearchFilenamesU3Ed__60_MoveNext_mF522B34C6025B147E2EEC6F0A710A36F5914A563,
	U3CSearchFilenamesU3Ed__60_U3CU3Em__Finally1_m3F0BF052C0F4B983D5310A2AD3DE2B98D3D1047F,
	U3CSearchFilenamesU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60577A520C64B4B398D594DFE499CF19DD3EA0BF,
	U3CSearchFilenamesU3Ed__60_System_Collections_IEnumerator_Reset_m15034D944E5814A47E2625612A4C2D46CEB0CDD3,
	U3CSearchFilenamesU3Ed__60_System_Collections_IEnumerator_get_Current_m1DEC95B8F870DEA8C3033301870A136A605FFECF,
	U3CDownloadTimestampU3Ed__68__ctor_m96F8A4F61225CFB202CA4172ECB68C1D485FA9DE,
	U3CDownloadTimestampU3Ed__68_System_IDisposable_Dispose_mB52E838EA4C081177C88D11D5D971706D8758AC3,
	U3CDownloadTimestampU3Ed__68_MoveNext_mEBE146A1FB0F381143A37B131B68F9FAD5BD6B61,
	U3CDownloadTimestampU3Ed__68_U3CU3Em__Finally1_mEA369E9545E0FCB177CC68A3612A7FBABF1F9A7D,
	U3CDownloadTimestampU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC1FE395C56C63D70A83A63D965DC6F7018EF596,
	U3CDownloadTimestampU3Ed__68_System_Collections_IEnumerator_Reset_m48D46BA0A5086D948C8633BB85CA182A62F1D6E1,
	U3CDownloadTimestampU3Ed__68_System_Collections_IEnumerator_get_Current_m3C054DCAC693818678C229A665F5D3B94A3C1328,
	U3CU3Ec__DisplayClass27_0__ctor_m0867AC1EE4584E3D72A584FFC2C59A2BAFB5AA5D,
	U3CU3Ec__DisplayClass27_0_U3CRemoveU3Eb__0_m7DDF79982240485EC42F30A03B5E235CD11E293D,
	U3CU3Ec__DisplayClass28_0__ctor_m0533AF6441844F93A6FD749A1712440D67AA1B55,
	U3CU3Ec__DisplayClass28_0_U3CRemoveU3Eb__0_m7ED2D7D13DA77CB4668356F33E17512AB3FCF878,
	U3CU3Ec__cctor_mCBAE2CBA3484F6F26D7CFAD443DA421869565DA8,
	U3CU3Ec__ctor_mF7E534B55FA357C4B8A838CF4A0534866FC072AE,
	U3CU3Ec_U3CRemoveNullValuesU3Eb__29_0_mD8771B2EF5C61F784A08A9DDC5E3FAD2ADD863D5,
	U3CU3Ec_U3CRemoveNullValuesU3Eb__29_1_m67907F7BBEE3B2C66F3251FAD56398623C0C75F4,
	ES3ReflectedMember_get_IsNull_mA0586EAA02E91291B1C3DA3292834DA7B717224C_AdjustorThunk,
	ES3ReflectedMember_get_Name_m65DCB779409BD0CE4E307F35EEED95E2C5161032_AdjustorThunk,
	ES3ReflectedMember_get_MemberType_m4DA31D17F188C2583A6F9814B71801F08B9B5C05_AdjustorThunk,
	ES3ReflectedMember_get_IsPublic_mDF765A42E542465F1770698DBCCDF4BE665D18A7_AdjustorThunk,
	ES3ReflectedMember_get_IsProtected_mA1BB184327C8280A2AEDE5235535DB261E7745B7_AdjustorThunk,
	ES3ReflectedMember_get_IsStatic_m90CF30A3C1370CF1701B4471D2ACA95836EB9F2F_AdjustorThunk,
	ES3ReflectedMember__ctor_m2C0D87EE6D7B6F42E1ED321266692B4FF2466B1A_AdjustorThunk,
	ES3ReflectedMember_SetValue_m10D81415A1791D2CF480D379933AFA313566116D_AdjustorThunk,
	ES3ReflectedMember_GetValue_m0C7ADDD5B7D7E20C53978C0CBDA389CC3CABC13C_AdjustorThunk,
	ES3ReflectedMethod__ctor_m7B8C634DB7388FD2460B19675B5A6DD0880D2A16,
	ES3ReflectedMethod__ctor_mA248731652730737F805D06E0CDF4D5A8BA81EF3,
	ES3ReflectedMethod_Invoke_mD25A58365F5CAED89E376CF7FDCC0BBA9F9AA4F0,
	U3CU3Ec__DisplayClass27_0__ctor_mA3FE7301EA70CB2B7AEFD7662268C71F49A0E3CC,
	U3CU3Ec__DisplayClass27_0_U3CGetDerivedTypesU3Eb__2_mE0B72CB0B8528A2C997F1B03733982435309A0CF,
	U3CU3Ec__cctor_m8D6A6BE41FF6776BC085382A9937868B22843B13,
	U3CU3Ec__ctor_m6BB75E5970728EFC0D8ABEF6119D8C6B6BC38AD6,
	U3CU3Ec_U3CGetDerivedTypesU3Eb__27_0_m94A7E5D4961403CA6DDCEAB62790C98083AB3971,
	U3CU3Ec_U3CGetDerivedTypesU3Eb__27_1_m4FDB7C171EA88089E8E393A4CC3AB0024A99B4F2,
	U3CU3Ec_U3CGetDerivedTypesU3Eb__27_3_m2F8A024770EF84BD5D9C12293C17324777A60752,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CSendWebRequestU3Ed__18__ctor_m05B84CA1C5473BA276D3C99D5AD78C2669A199B3,
	U3CSendWebRequestU3Ed__18_System_IDisposable_Dispose_m10166695D948E49245C9B0909B38641838287AF5,
	U3CSendWebRequestU3Ed__18_MoveNext_mA3DF809A3EAC4CF487A3B52981B0A9E9130BCB9C,
	U3CSendWebRequestU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7A624745C7225D39F9C30FFEB5470D19451631C,
	U3CSendWebRequestU3Ed__18_System_Collections_IEnumerator_Reset_m781E5086D81D1AA4EC286A94B97EF1728FB7FCCF,
	U3CSendWebRequestU3Ed__18_System_Collections_IEnumerator_get_Current_m86EF2247AD1C02C2539A77CC1521647A55136E0A,
	U3CGetEnumeratorU3Ed__2__ctor_m464CFA360A550AED379A3B0D3BD22CF52F746E48,
	U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_mE5B52E1B648670786BD3C93FD43D4A26DB5D733D,
	U3CGetEnumeratorU3Ed__2_MoveNext_m04543E1EED6E78CE553F13D2724BB3501766D6D3,
	U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA55C586C11378B7EED5AF43876F23B4BF62B28AE,
	U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mEB70CC2FC3965462D3A483EA2D97085680F60053,
	U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_m0905980F66AFCF0B861B5BCC9543D803EC83A018,
	U3CGetEnumeratorU3Ed__2__ctor_m108CEEB4722EC20E8690E60483B8F788D9473399,
	U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_mC59944AB864257A23D362A3CBB5D364608FF87FC,
	U3CGetEnumeratorU3Ed__2_MoveNext_mCC3F59F02C720D03382D8465D4024AD22A7233D0,
	U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F6AF4BB7179E618AD4DD6493C76944BD0688AD6,
	U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_m77C2404A32B22B3CB1550EF77420F68A4167B096,
	U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_mD7FC926CB8A73A7A6DCA13DBB13402050845B3BE,
};
static const int32_t s_InvokerIndices[1528] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	4,
	4,
	23,
	23,
	23,
	23,
	23,
	31,
	139,
	139,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	139,
	121,
	169,
	121,
	139,
	121,
	169,
	121,
	139,
	169,
	121,
	139,
	169,
	121,
	121,
	169,
	121,
	0,
	1,
	2,
	1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	169,
	1843,
	169,
	-1,
	-1,
	-1,
	-1,
	2,
	4,
	0,
	1,
	0,
	4,
	0,
	1,
	0,
	0,
	1,
	0,
	0,
	115,
	221,
	-1,
	-1,
	2,
	-1,
	-1,
	3,
	139,
	121,
	139,
	121,
	1843,
	121,
	121,
	1843,
	121,
	121,
	1843,
	121,
	121,
	1843,
	121,
	139,
	121,
	139,
	139,
	121,
	169,
	121,
	103,
	118,
	313,
	118,
	49,
	103,
	118,
	103,
	103,
	118,
	103,
	4,
	0,
	1,
	0,
	4,
	0,
	1,
	0,
	4,
	0,
	1,
	0,
	3,
	139,
	121,
	139,
	103,
	118,
	103,
	285,
	689,
	264,
	689,
	3,
	139,
	121,
	139,
	3,
	139,
	121,
	139,
	3,
	23,
	26,
	27,
	26,
	31,
	94,
	132,
	94,
	27,
	23,
	27,
	26,
	23,
	14,
	-1,
	27,
	27,
	28,
	120,
	-1,
	-1,
	-1,
	14,
	14,
	26,
	9,
	10,
	28,
	0,
	139,
	139,
	139,
	121,
	121,
	118,
	103,
	0,
	4,
	689,
	3,
	23,
	23,
	23,
	10,
	10,
	-1,
	566,
	-1,
	54,
	26,
	27,
	26,
	26,
	27,
	27,
	26,
	27,
	26,
	94,
	132,
	94,
	138,
	0,
	14,
	3,
	10,
	670,
	95,
	218,
	671,
	407,
	155,
	155,
	95,
	95,
	218,
	218,
	10,
	14,
	14,
	155,
	14,
	106,
	23,
	106,
	23,
	9,
	95,
	23,
	95,
	23,
	95,
	23,
	23,
	95,
	95,
	23,
	95,
	95,
	94,
	14,
	14,
	23,
	-1,
	-1,
	-1,
	-1,
	155,
	14,
	180,
	180,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	0,
	1,
	0,
	0,
	1,
	1,
	189,
	23,
	23,
	4,
	4,
	4,
	10,
	32,
	14,
	27,
	27,
	26,
	62,
	1844,
	31,
	103,
	14,
	26,
	3,
	23,
	31,
	26,
	113,
	183,
	27,
	14,
	14,
	14,
	284,
	14,
	28,
	120,
	188,
	120,
	188,
	108,
	188,
	14,
	28,
	120,
	188,
	120,
	188,
	108,
	28,
	120,
	188,
	188,
	108,
	1845,
	14,
	28,
	120,
	188,
	120,
	188,
	108,
	28,
	120,
	188,
	1846,
	1846,
	14,
	28,
	120,
	188,
	120,
	188,
	108,
	188,
	120,
	188,
	108,
	188,
	108,
	1633,
	108,
	120,
	188,
	14,
	28,
	120,
	188,
	120,
	188,
	108,
	188,
	1847,
	200,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	32,
	32,
	23,
	23,
	32,
	32,
	32,
	32,
	23,
	27,
	32,
	292,
	31,
	672,
	293,
	183,
	183,
	32,
	31,
	31,
	557,
	557,
	557,
	26,
	26,
	745,
	180,
	-1,
	180,
	113,
	104,
	26,
	27,
	104,
	-1,
	180,
	526,
	27,
	27,
	27,
	180,
	180,
	27,
	27,
	26,
	1,
	0,
	100,
	1848,
	95,
	26,
	23,
	26,
	23,
	31,
	23,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	26,
	104,
	-1,
	28,
	-1,
	27,
	26,
	104,
	-1,
	28,
	-1,
	27,
	26,
	27,
	104,
	28,
	-1,
	-1,
	27,
	28,
	27,
	104,
	26,
	27,
	27,
	-1,
	-1,
	180,
	26,
	180,
	27,
	104,
	-1,
	-1,
	28,
	27,
	26,
	104,
	-1,
	28,
	-1,
	27,
	26,
	27,
	104,
	-1,
	-1,
	28,
	27,
	26,
	104,
	-1,
	-1,
	28,
	27,
	26,
	104,
	-1,
	-1,
	28,
	27,
	26,
	27,
	-1,
	27,
	-1,
	-1,
	-1,
	1,
	0,
	26,
	27,
	-1,
	-1,
	27,
	-1,
	-1,
	26,
	27,
	-1,
	27,
	-1,
	-1,
	-1,
	26,
	27,
	-1,
	-1,
	127,
	-1,
	28,
	27,
	120,
	31,
	947,
	26,
	26,
	27,
	-1,
	-1,
	27,
	104,
	-1,
	-1,
	67,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	26,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	26,
	27,
	-1,
	26,
	27,
	-1,
	-1,
	26,
	27,
	-1,
	26,
	27,
	27,
	-1,
	-1,
	26,
	27,
	-1,
	-1,
	26,
	27,
	-1,
	-1,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	1836,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	104,
	-1,
	-1,
	1849,
	0,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	23,
	27,
	-1,
	-1,
	9,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	-1,
	3,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	3,
	23,
	23,
	27,
	-1,
	1205,
	3,
	23,
	23,
	27,
	-1,
	-1,
	3,
	178,
	178,
	178,
	43,
	0,
	526,
	526,
	178,
	23,
	526,
	526,
	23,
	1850,
	31,
	27,
	27,
	689,
	0,
	139,
	103,
	121,
	121,
	121,
	139,
	103,
	322,
	103,
	1,
	138,
	139,
	138,
	0,
	121,
	139,
	3,
	23,
	200,
	200,
	14,
	26,
	135,
	23,
	4,
	95,
	14,
	26,
	14,
	23,
	26,
	200,
	1851,
	1852,
	1852,
	1853,
	200,
	1854,
	9,
	26,
	183,
	23,
	23,
	9,
	382,
	911,
	135,
	103,
	23,
	3,
	1858,
	127,
	23,
	127,
	1858,
	23,
	4,
	0,
	1859,
	1859,
	103,
	0,
	1,
	115,
	1,
	1,
	1011,
	1860,
	1860,
	-1,
	0,
	118,
	0,
	0,
	102,
	0,
	311,
	103,
	0,
	103,
	103,
	103,
	103,
	103,
	103,
	0,
	0,
	1,
	1,
	103,
	118,
	118,
	118,
	0,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	132,
	14,
	106,
	23,
	95,
	23,
	95,
	23,
	95,
	23,
	23,
	95,
	95,
	23,
	95,
	95,
	106,
	94,
	155,
	14,
	218,
	670,
	10,
	95,
	671,
	407,
	155,
	155,
	10,
	95,
	95,
	218,
	218,
	14,
	-1,
	-1,
	10,
	23,
	132,
	14,
	106,
	23,
	95,
	23,
	95,
	23,
	95,
	23,
	23,
	95,
	95,
	23,
	95,
	95,
	94,
	106,
	1862,
	1863,
	211,
	603,
	95,
	603,
	218,
	23,
	23,
	48,
	48,
	48,
	48,
	48,
	48,
	14,
	14,
	155,
	218,
	670,
	10,
	95,
	671,
	407,
	155,
	155,
	10,
	95,
	95,
	218,
	218,
	14,
	23,
	4,
	157,
	200,
	23,
	23,
	23,
	1864,
	115,
	21,
	21,
	31,
	26,
	314,
	138,
	31,
	95,
	26,
	0,
	31,
	115,
	503,
	121,
	132,
	1865,
	138,
	0,
	121,
	138,
	3,
	3,
	670,
	670,
	95,
	27,
	27,
	120,
	14,
	425,
	28,
	23,
	103,
	756,
	756,
	5,
	3,
	27,
	1866,
	180,
	180,
	104,
	526,
	27,
	32,
	292,
	31,
	672,
	293,
	183,
	183,
	32,
	31,
	31,
	557,
	557,
	557,
	26,
	26,
	32,
	23,
	27,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	32,
	32,
	23,
	23,
	32,
	32,
	32,
	32,
	23,
	745,
	-1,
	180,
	180,
	32,
	292,
	31,
	672,
	293,
	183,
	183,
	32,
	31,
	31,
	557,
	557,
	557,
	26,
	26,
	23,
	48,
	23,
	27,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	32,
	32,
	23,
	23,
	32,
	32,
	32,
	32,
	23,
	27,
	1866,
	32,
	292,
	31,
	672,
	293,
	183,
	183,
	32,
	31,
	31,
	557,
	557,
	557,
	26,
	26,
	23,
	48,
	23,
	27,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	32,
	32,
	23,
	23,
	32,
	32,
	32,
	32,
	23,
	23,
	102,
	112,
	26,
	14,
	26,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	23,
	1855,
	23,
	1856,
	3,
	23,
	1855,
	1857,
	95,
	14,
	14,
	95,
	95,
	95,
	26,
	27,
	28,
	391,
	1861,
	120,
	23,
	9,
	3,
	23,
	28,
	120,
	28,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[114] = 
{
	{ 0x02000002, { 0, 11 } },
	{ 0x020000C8, { 169, 39 } },
	{ 0x020000F7, { 208, 6 } },
	{ 0x06000019, { 11, 1 } },
	{ 0x0600001A, { 12, 1 } },
	{ 0x0600001B, { 13, 1 } },
	{ 0x0600001C, { 14, 3 } },
	{ 0x06000032, { 17, 1 } },
	{ 0x06000033, { 18, 1 } },
	{ 0x06000034, { 19, 1 } },
	{ 0x06000035, { 20, 2 } },
	{ 0x06000036, { 22, 1 } },
	{ 0x06000037, { 23, 1 } },
	{ 0x06000038, { 24, 1 } },
	{ 0x06000039, { 25, 2 } },
	{ 0x0600003E, { 27, 1 } },
	{ 0x0600003F, { 28, 1 } },
	{ 0x06000040, { 29, 1 } },
	{ 0x06000041, { 30, 2 } },
	{ 0x06000051, { 32, 2 } },
	{ 0x06000052, { 34, 2 } },
	{ 0x06000054, { 36, 2 } },
	{ 0x06000055, { 38, 2 } },
	{ 0x060000A6, { 40, 2 } },
	{ 0x060000AB, { 42, 3 } },
	{ 0x060000AC, { 45, 3 } },
	{ 0x060000AD, { 48, 3 } },
	{ 0x060000C5, { 51, 1 } },
	{ 0x060000C7, { 52, 2 } },
	{ 0x060000FF, { 54, 2 } },
	{ 0x06000100, { 56, 2 } },
	{ 0x06000101, { 58, 2 } },
	{ 0x06000102, { 60, 1 } },
	{ 0x06000107, { 61, 2 } },
	{ 0x06000108, { 63, 2 } },
	{ 0x06000109, { 65, 3 } },
	{ 0x0600010A, { 68, 1 } },
	{ 0x0600010B, { 69, 2 } },
	{ 0x0600010C, { 71, 3 } },
	{ 0x0600010D, { 74, 1 } },
	{ 0x0600010E, { 75, 1 } },
	{ 0x06000194, { 76, 1 } },
	{ 0x0600019B, { 77, 1 } },
	{ 0x060001D2, { 78, 2 } },
	{ 0x060001D3, { 80, 1 } },
	{ 0x060001DF, { 81, 1 } },
	{ 0x060001ED, { 82, 8 } },
	{ 0x060001F3, { 90, 8 } },
	{ 0x060001FA, { 98, 1 } },
	{ 0x060001FC, { 99, 1 } },
	{ 0x06000204, { 100, 2 } },
	{ 0x06000205, { 102, 2 } },
	{ 0x0600020A, { 104, 1 } },
	{ 0x0600020C, { 105, 1 } },
	{ 0x06000212, { 106, 1 } },
	{ 0x0600021F, { 107, 1 } },
	{ 0x06000220, { 108, 2 } },
	{ 0x06000225, { 110, 1 } },
	{ 0x0600022F, { 111, 1 } },
	{ 0x0600023A, { 112, 1 } },
	{ 0x0600023F, { 113, 1 } },
	{ 0x06000243, { 114, 1 } },
	{ 0x06000247, { 115, 1 } },
	{ 0x0600024C, { 116, 1 } },
	{ 0x06000251, { 117, 1 } },
	{ 0x0600025A, { 118, 1 } },
	{ 0x0600025F, { 119, 1 } },
	{ 0x06000264, { 120, 1 } },
	{ 0x06000269, { 121, 1 } },
	{ 0x0600026E, { 122, 1 } },
	{ 0x06000273, { 123, 1 } },
	{ 0x0600027D, { 124, 1 } },
	{ 0x06000282, { 125, 1 } },
	{ 0x06000287, { 126, 1 } },
	{ 0x06000297, { 127, 1 } },
	{ 0x06000298, { 128, 1 } },
	{ 0x060002E5, { 129, 1 } },
	{ 0x060002FA, { 130, 1 } },
	{ 0x0600030A, { 131, 1 } },
	{ 0x0600030F, { 132, 1 } },
	{ 0x06000314, { 133, 1 } },
	{ 0x06000319, { 134, 1 } },
	{ 0x0600031E, { 135, 1 } },
	{ 0x06000325, { 136, 1 } },
	{ 0x0600032A, { 137, 1 } },
	{ 0x0600032F, { 138, 2 } },
	{ 0x0600033A, { 140, 1 } },
	{ 0x06000349, { 141, 1 } },
	{ 0x0600035B, { 142, 1 } },
	{ 0x06000360, { 143, 1 } },
	{ 0x06000365, { 144, 1 } },
	{ 0x0600036A, { 145, 1 } },
	{ 0x06000375, { 146, 1 } },
	{ 0x06000383, { 147, 1 } },
	{ 0x06000389, { 148, 1 } },
	{ 0x0600038F, { 149, 1 } },
	{ 0x060003A1, { 150, 1 } },
	{ 0x060003A6, { 151, 1 } },
	{ 0x060003AB, { 152, 1 } },
	{ 0x060003B1, { 153, 1 } },
	{ 0x060003B6, { 154, 1 } },
	{ 0x060003BB, { 155, 1 } },
	{ 0x060003CF, { 156, 1 } },
	{ 0x060003D4, { 157, 1 } },
	{ 0x060003D5, { 158, 1 } },
	{ 0x060003DB, { 159, 1 } },
	{ 0x060003E1, { 160, 1 } },
	{ 0x060003E6, { 161, 1 } },
	{ 0x060003EB, { 162, 1 } },
	{ 0x0600040A, { 163, 1 } },
	{ 0x06000464, { 164, 5 } },
	{ 0x060004A7, { 214, 1 } },
	{ 0x060004A8, { 215, 1 } },
	{ 0x06000536, { 216, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[218] = 
{
	{ (Il2CppRGCTXDataType)2, 19446 },
	{ (Il2CppRGCTXDataType)3, 17489 },
	{ (Il2CppRGCTXDataType)2, 19447 },
	{ (Il2CppRGCTXDataType)3, 17490 },
	{ (Il2CppRGCTXDataType)3, 17491 },
	{ (Il2CppRGCTXDataType)2, 19448 },
	{ (Il2CppRGCTXDataType)3, 17492 },
	{ (Il2CppRGCTXDataType)3, 17493 },
	{ (Il2CppRGCTXDataType)3, 17494 },
	{ (Il2CppRGCTXDataType)2, 17727 },
	{ (Il2CppRGCTXDataType)2, 17728 },
	{ (Il2CppRGCTXDataType)3, 17495 },
	{ (Il2CppRGCTXDataType)3, 17496 },
	{ (Il2CppRGCTXDataType)3, 17497 },
	{ (Il2CppRGCTXDataType)3, 17498 },
	{ (Il2CppRGCTXDataType)2, 17753 },
	{ (Il2CppRGCTXDataType)3, 17499 },
	{ (Il2CppRGCTXDataType)3, 17500 },
	{ (Il2CppRGCTXDataType)3, 17501 },
	{ (Il2CppRGCTXDataType)3, 17502 },
	{ (Il2CppRGCTXDataType)3, 17503 },
	{ (Il2CppRGCTXDataType)3, 17504 },
	{ (Il2CppRGCTXDataType)3, 17505 },
	{ (Il2CppRGCTXDataType)3, 17506 },
	{ (Il2CppRGCTXDataType)3, 17507 },
	{ (Il2CppRGCTXDataType)3, 17508 },
	{ (Il2CppRGCTXDataType)3, 17509 },
	{ (Il2CppRGCTXDataType)3, 17510 },
	{ (Il2CppRGCTXDataType)3, 17511 },
	{ (Il2CppRGCTXDataType)3, 17512 },
	{ (Il2CppRGCTXDataType)3, 17513 },
	{ (Il2CppRGCTXDataType)3, 17514 },
	{ (Il2CppRGCTXDataType)2, 17766 },
	{ (Il2CppRGCTXDataType)1, 17766 },
	{ (Il2CppRGCTXDataType)1, 17768 },
	{ (Il2CppRGCTXDataType)2, 17768 },
	{ (Il2CppRGCTXDataType)1, 17769 },
	{ (Il2CppRGCTXDataType)3, 17515 },
	{ (Il2CppRGCTXDataType)2, 17770 },
	{ (Il2CppRGCTXDataType)3, 17516 },
	{ (Il2CppRGCTXDataType)1, 17790 },
	{ (Il2CppRGCTXDataType)3, 17517 },
	{ (Il2CppRGCTXDataType)1, 17791 },
	{ (Il2CppRGCTXDataType)2, 17791 },
	{ (Il2CppRGCTXDataType)3, 17518 },
	{ (Il2CppRGCTXDataType)1, 17792 },
	{ (Il2CppRGCTXDataType)2, 17792 },
	{ (Il2CppRGCTXDataType)3, 17519 },
	{ (Il2CppRGCTXDataType)1, 17793 },
	{ (Il2CppRGCTXDataType)3, 17520 },
	{ (Il2CppRGCTXDataType)3, 17521 },
	{ (Il2CppRGCTXDataType)2, 17803 },
	{ (Il2CppRGCTXDataType)1, 17804 },
	{ (Il2CppRGCTXDataType)2, 17804 },
	{ (Il2CppRGCTXDataType)1, 17813 },
	{ (Il2CppRGCTXDataType)3, 17522 },
	{ (Il2CppRGCTXDataType)1, 19449 },
	{ (Il2CppRGCTXDataType)3, 17523 },
	{ (Il2CppRGCTXDataType)1, 17814 },
	{ (Il2CppRGCTXDataType)3, 17524 },
	{ (Il2CppRGCTXDataType)3, 17525 },
	{ (Il2CppRGCTXDataType)3, 17526 },
	{ (Il2CppRGCTXDataType)3, 17527 },
	{ (Il2CppRGCTXDataType)3, 17528 },
	{ (Il2CppRGCTXDataType)3, 17529 },
	{ (Il2CppRGCTXDataType)3, 17530 },
	{ (Il2CppRGCTXDataType)2, 17818 },
	{ (Il2CppRGCTXDataType)3, 17531 },
	{ (Il2CppRGCTXDataType)3, 17532 },
	{ (Il2CppRGCTXDataType)3, 17533 },
	{ (Il2CppRGCTXDataType)2, 17819 },
	{ (Il2CppRGCTXDataType)3, 17534 },
	{ (Il2CppRGCTXDataType)2, 17820 },
	{ (Il2CppRGCTXDataType)3, 17535 },
	{ (Il2CppRGCTXDataType)3, 17536 },
	{ (Il2CppRGCTXDataType)1, 19452 },
	{ (Il2CppRGCTXDataType)1, 19453 },
	{ (Il2CppRGCTXDataType)1, 19454 },
	{ (Il2CppRGCTXDataType)3, 17537 },
	{ (Il2CppRGCTXDataType)2, 17887 },
	{ (Il2CppRGCTXDataType)3, 17538 },
	{ (Il2CppRGCTXDataType)2, 19455 },
	{ (Il2CppRGCTXDataType)2, 19456 },
	{ (Il2CppRGCTXDataType)3, 17539 },
	{ (Il2CppRGCTXDataType)3, 17540 },
	{ (Il2CppRGCTXDataType)2, 19457 },
	{ (Il2CppRGCTXDataType)3, 17541 },
	{ (Il2CppRGCTXDataType)3, 17542 },
	{ (Il2CppRGCTXDataType)3, 17543 },
	{ (Il2CppRGCTXDataType)2, 19458 },
	{ (Il2CppRGCTXDataType)2, 19459 },
	{ (Il2CppRGCTXDataType)3, 17544 },
	{ (Il2CppRGCTXDataType)3, 17545 },
	{ (Il2CppRGCTXDataType)2, 19460 },
	{ (Il2CppRGCTXDataType)3, 17546 },
	{ (Il2CppRGCTXDataType)3, 17547 },
	{ (Il2CppRGCTXDataType)3, 17548 },
	{ (Il2CppRGCTXDataType)2, 19461 },
	{ (Il2CppRGCTXDataType)3, 17549 },
	{ (Il2CppRGCTXDataType)3, 17550 },
	{ (Il2CppRGCTXDataType)3, 17551 },
	{ (Il2CppRGCTXDataType)3, 17552 },
	{ (Il2CppRGCTXDataType)3, 17553 },
	{ (Il2CppRGCTXDataType)3, 17554 },
	{ (Il2CppRGCTXDataType)3, 17555 },
	{ (Il2CppRGCTXDataType)3, 17556 },
	{ (Il2CppRGCTXDataType)3, 17557 },
	{ (Il2CppRGCTXDataType)3, 17558 },
	{ (Il2CppRGCTXDataType)3, 17559 },
	{ (Il2CppRGCTXDataType)3, 17560 },
	{ (Il2CppRGCTXDataType)3, 17561 },
	{ (Il2CppRGCTXDataType)2, 19472 },
	{ (Il2CppRGCTXDataType)2, 19473 },
	{ (Il2CppRGCTXDataType)2, 19474 },
	{ (Il2CppRGCTXDataType)2, 19475 },
	{ (Il2CppRGCTXDataType)2, 19476 },
	{ (Il2CppRGCTXDataType)2, 19477 },
	{ (Il2CppRGCTXDataType)2, 19478 },
	{ (Il2CppRGCTXDataType)2, 19479 },
	{ (Il2CppRGCTXDataType)2, 19480 },
	{ (Il2CppRGCTXDataType)2, 19481 },
	{ (Il2CppRGCTXDataType)2, 19482 },
	{ (Il2CppRGCTXDataType)2, 19483 },
	{ (Il2CppRGCTXDataType)2, 19484 },
	{ (Il2CppRGCTXDataType)2, 19485 },
	{ (Il2CppRGCTXDataType)2, 19486 },
	{ (Il2CppRGCTXDataType)2, 19487 },
	{ (Il2CppRGCTXDataType)3, 17562 },
	{ (Il2CppRGCTXDataType)3, 17563 },
	{ (Il2CppRGCTXDataType)3, 17564 },
	{ (Il2CppRGCTXDataType)3, 17565 },
	{ (Il2CppRGCTXDataType)3, 17566 },
	{ (Il2CppRGCTXDataType)3, 17567 },
	{ (Il2CppRGCTXDataType)3, 17568 },
	{ (Il2CppRGCTXDataType)3, 17569 },
	{ (Il2CppRGCTXDataType)3, 17570 },
	{ (Il2CppRGCTXDataType)3, 17571 },
	{ (Il2CppRGCTXDataType)3, 17572 },
	{ (Il2CppRGCTXDataType)3, 17573 },
	{ (Il2CppRGCTXDataType)3, 17574 },
	{ (Il2CppRGCTXDataType)3, 17575 },
	{ (Il2CppRGCTXDataType)3, 17576 },
	{ (Il2CppRGCTXDataType)3, 17577 },
	{ (Il2CppRGCTXDataType)3, 17578 },
	{ (Il2CppRGCTXDataType)3, 17579 },
	{ (Il2CppRGCTXDataType)3, 17580 },
	{ (Il2CppRGCTXDataType)3, 17581 },
	{ (Il2CppRGCTXDataType)3, 17582 },
	{ (Il2CppRGCTXDataType)3, 17583 },
	{ (Il2CppRGCTXDataType)3, 17584 },
	{ (Il2CppRGCTXDataType)3, 17585 },
	{ (Il2CppRGCTXDataType)3, 17586 },
	{ (Il2CppRGCTXDataType)3, 17587 },
	{ (Il2CppRGCTXDataType)3, 17588 },
	{ (Il2CppRGCTXDataType)3, 17589 },
	{ (Il2CppRGCTXDataType)3, 17590 },
	{ (Il2CppRGCTXDataType)3, 17591 },
	{ (Il2CppRGCTXDataType)3, 17592 },
	{ (Il2CppRGCTXDataType)3, 17593 },
	{ (Il2CppRGCTXDataType)3, 17594 },
	{ (Il2CppRGCTXDataType)3, 17595 },
	{ (Il2CppRGCTXDataType)3, 17596 },
	{ (Il2CppRGCTXDataType)3, 17597 },
	{ (Il2CppRGCTXDataType)3, 17598 },
	{ (Il2CppRGCTXDataType)2, 19524 },
	{ (Il2CppRGCTXDataType)3, 17599 },
	{ (Il2CppRGCTXDataType)1, 18264 },
	{ (Il2CppRGCTXDataType)2, 18264 },
	{ (Il2CppRGCTXDataType)3, 17600 },
	{ (Il2CppRGCTXDataType)2, 19525 },
	{ (Il2CppRGCTXDataType)3, 17601 },
	{ (Il2CppRGCTXDataType)2, 19526 },
	{ (Il2CppRGCTXDataType)3, 17602 },
	{ (Il2CppRGCTXDataType)3, 17603 },
	{ (Il2CppRGCTXDataType)3, 17604 },
	{ (Il2CppRGCTXDataType)3, 17605 },
	{ (Il2CppRGCTXDataType)3, 17606 },
	{ (Il2CppRGCTXDataType)3, 17607 },
	{ (Il2CppRGCTXDataType)3, 17608 },
	{ (Il2CppRGCTXDataType)3, 17609 },
	{ (Il2CppRGCTXDataType)2, 19527 },
	{ (Il2CppRGCTXDataType)3, 16818 },
	{ (Il2CppRGCTXDataType)3, 17610 },
	{ (Il2CppRGCTXDataType)3, 17611 },
	{ (Il2CppRGCTXDataType)3, 17612 },
	{ (Il2CppRGCTXDataType)2, 18285 },
	{ (Il2CppRGCTXDataType)3, 17613 },
	{ (Il2CppRGCTXDataType)3, 16814 },
	{ (Il2CppRGCTXDataType)2, 19528 },
	{ (Il2CppRGCTXDataType)3, 17614 },
	{ (Il2CppRGCTXDataType)2, 19529 },
	{ (Il2CppRGCTXDataType)3, 17615 },
	{ (Il2CppRGCTXDataType)3, 17616 },
	{ (Il2CppRGCTXDataType)3, 17617 },
	{ (Il2CppRGCTXDataType)2, 19530 },
	{ (Il2CppRGCTXDataType)3, 17618 },
	{ (Il2CppRGCTXDataType)3, 17619 },
	{ (Il2CppRGCTXDataType)3, 17620 },
	{ (Il2CppRGCTXDataType)3, 17621 },
	{ (Il2CppRGCTXDataType)3, 17622 },
	{ (Il2CppRGCTXDataType)3, 16836 },
	{ (Il2CppRGCTXDataType)3, 17623 },
	{ (Il2CppRGCTXDataType)2, 19531 },
	{ (Il2CppRGCTXDataType)3, 17624 },
	{ (Il2CppRGCTXDataType)3, 16833 },
	{ (Il2CppRGCTXDataType)3, 16834 },
	{ (Il2CppRGCTXDataType)3, 17625 },
	{ (Il2CppRGCTXDataType)2, 18284 },
	{ (Il2CppRGCTXDataType)2, 19532 },
	{ (Il2CppRGCTXDataType)3, 17626 },
	{ (Il2CppRGCTXDataType)2, 19532 },
	{ (Il2CppRGCTXDataType)3, 17627 },
	{ (Il2CppRGCTXDataType)2, 18293 },
	{ (Il2CppRGCTXDataType)3, 17628 },
	{ (Il2CppRGCTXDataType)3, 17629 },
	{ (Il2CppRGCTXDataType)3, 17630 },
	{ (Il2CppRGCTXDataType)2, 19534 },
	{ (Il2CppRGCTXDataType)3, 17631 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	1528,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	114,
	s_rgctxIndices,
	218,
	s_rgctxValues,
	NULL,
};
